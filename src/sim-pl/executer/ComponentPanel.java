/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import icons.IconManager;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.BeanInfo;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;

import nbit.NumberFormat;
import util.ExceptionHandler;
import util.GUIHelper;
import util.ImageSource;
import util.graphics.ImageOutputter;

import compiler.wisent.DataSource;
import compiler.wisent.Instance_Simple;
import compiler.wisent.UndefinedIdentifierException;

import databinding.BasicEditorNode;
import databinding.Input;
import databinding.Memory;
import databinding.Pin;
import databinding.PinLabel;
import databinding.Storage;
import databinding.SubComponent;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.wires.Node;
import databinding.wires.Span;
import databinding.wires.Wire;
import editor.RenderAdapter.NodeDist;
import editor.RenderAdapter.NodeRepresentantMapping;
import editor.RenderContext;
import executer.Simulator.Instance_Complex;
import executer.instanceeditor.MemoryEditorDialog;
import executer.timesequencediagram.TimeSequenceDiagram;
import executer.timetable.TimeTable;




public class ComponentPanel extends JPanel implements TimeTableListener, Scrollable
{
/*
The getNormalizingTransform dilemma.
 * we can't ask the Graphics2D object, since that is sometimes an offscreen image
 * we can't ask the AWT object, since the second screen's graphicsConfiguration
   has bogus values.

 As a temporary solution, we use the default of the primary monitor.
 This is not unreasonable.
*/

	private static final AffineTransform normTrans = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();


	// margin is currently in device units (pixels)
	// TODO: transform it into meters
	public static final int MARGIN = 40;

	final WorkSheet    listener;
	final Instance     instance;

	/** The bound of the component IN COMPONENT COORDINATES */
	private     Rectangle2D         bounds;

	/** The bound of the component IN PIXELS. Always has origin (0,0) */
	private     Rectangle           pixelBound;



	public      boolean             viewValues =    true;
	public      boolean             viewLabels =    true;
	public      boolean             drawPins =    true;


	RenderContext rc;
	CompiledComponent<?> cc;

	final List<Probe> probes = new ArrayList<Probe>();

	double zoom = 1.0;

	public double getZoom() { return zoom; }

	public void setZoom(double zoom) {
		this.zoom = zoom;
		rc.getRenderRoot().deepInvalidate();
		if (super.isDisplayable()) recomputeBounds();
	}
	
	public void zoom(double clicks) {
		setZoom(getZoom() * Math.pow(1.25, clicks));
	}


	public Dimension getPreferredSize() {
		if (bounds == null) recomputeBounds();
		return super.getPreferredSize();
	}



	public Graphics2D getComponentGrahpics() {
		Graphics2D g = (Graphics2D)getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.translate(MARGIN, MARGIN);
		g.transform(normTrans);
		g.scale(zoom, zoom);
		g.translate(-bounds.getX(), -bounds.getY());
		return g;
	}


	final MouseMotionAdapter ma1 = new MouseMotionAdapter() {
		public void mouseMoved(MouseEvent e) {
			Object o = getObjectUnderCursor(getComponentGrahpics(), e.getPoint());
			if (o != null) {
				if (o instanceof Instance) {
					setToolTipText(o.toString());
					setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
				} else if (o instanceof Instance_Pin) {
					setToolTipText( ( (Instance_Pin)o).getFullString(listener.
						getNumberFormat()));
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				} else if (o instanceof LocalWireInstance) {
					LocalWireInstance lws = (LocalWireInstance)o;
					setToolTipText(lws.getFullString(listener.getNumberFormat()));
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				} else
					throw new ExceptionHandler("Unexpected thing: " + o +
											   " of class " + o.getClass());
			} else {
				setCursor(Cursor.getDefaultCursor());
				setToolTipText(null);
			}
		}
	};


	final MouseAdapter ma2 = new MouseAdapter() {

		private void askForInput(Frame parent, Instance_Pin ip, Point p, DataSource d) {
			ValueDialog vd = new ValueDialog(parent);
			ComponentPanel.this.getLocation();
			vd.setRepresentation(ComponentPanel.this.listener.getNumberFormat());
			Point sp = new Point(p);
			SwingUtilities.convertPointToScreen(sp, ComponentPanel.this);
			Instance_Simple r = vd.ask(sp, ip.pin.getName(), ((Instance_Simple)d.getValue()));
			if (r == null) return;
			try {
				ComponentPanel.this.listener.tt.changeInputTo(ip, r);
			} catch (Exception ex) {
				ex.printStackTrace();
				ExceptionHandler.handleException(ex);
			}
		}

			private void popUpOver(final MouseEvent e, Object o) {

				int iconStyle = BeanInfo.ICON_COLOR_32x32;

				if (o != null) {
				    //					System.err.println("Popup on " + o + "/" + o.getClass().getName());
					JPopupMenu popup = new JPopupMenu(o.toString());
					if (o instanceof Instance) {
						final Instance i = (Instance)o;
						if (o instanceof Simulator.Instance_Simple) {
							final Simulator.Instance_Simple is = (Simulator.Instance_Simple) i;
							for (final Storage s : is.getComponent().getMemory().getStorages()) {
								popup.add(new AbstractAction("Show " + s.getName(), IconManager.chargeIcon(Memory.class, iconStyle) ) {
									public void actionPerformed(ActionEvent ae) {
										listener.addDialog(e.getPoint(), new MemoryEditorDialog(listener, is, s));
									}
								});
							}
						}
						// show component in its own window
//						popup.add(new AbstractAction("Show " + i.getFullName(), IconManager.chargeIcon(Simple.class, iconStyle) ) {
//							public void actionPerformed(ActionEvent ae) {
//								listener.addDialog(e.getPoint(), new ComponentView(listener, i));
//							}
//						});
					} else if (o instanceof Instance_Pin) {
						try {
							final Instance_Pin ip = (Instance_Pin)o;
							final DataSource d = ip.instance.getNamespace().get(ip.pin.getName());
							if (d.isValueEditable()) {
								final Icon icon = IconManager.chargeIcon(Input.class, iconStyle);
								popup.add(new AbstractAction("Change input to " + ip.pin.getName(), icon) {
									public void actionPerformed(ActionEvent ae) {
										Frame parent = (Frame)ComponentPanel.this.getTopLevelAncestor();
										askForInput(parent, ip, e.getPoint(), d);
									}
								});
							}
						} catch (UndefinedIdentifierException ex) { throw new Error(ex); }
					}

					if (o instanceof ValuedObject) {
						final ValuedObject vo = (ValuedObject)o;
						final JCheckBoxMenuItem jcb = new JCheckBoxMenuItem(
											  "Graph " + vo.getFullName(),
											  new ImageIcon(TimeSequenceDiagram.icon.getScaledInstance(32,32,Image.SCALE_SMOOTH)),
											  listener.tsd.contains(vo)
						);
						jcb.addActionListener(new ActionListener()  {
							public void actionPerformed(ActionEvent ae) {
								if (jcb.isSelected()) listener.tsd.addPin(vo);
								else                  listener.tsd.removePin(vo);
								listener.tsd.refresh();
							}
						});

						popup.add(jcb);
					}

					popup.setBorderPainted(true);
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			private void regularSelect(MouseEvent e, Object o) {
				if (o != null)
				{
					if (o instanceof Instance) {
						listener.fireSelectInstance((Instance)o);
					}
					else if (o instanceof Instance_Pin) {
						try {
							final Instance_Pin ip = (Instance_Pin)o;
							final DataSource d = ip.instance.getNamespace().get(ip.pin.getName());
							if (d.isValueEditable()) {
								Frame parent = (Frame)ComponentPanel.this.getTopLevelAncestor();
								assert e.getSource() == ComponentPanel.this;
								askForInput(parent, ip, e.getPoint(), d);
							}
						} catch (UndefinedIdentifierException ex) { throw new Error(ex); }
					} else if (o instanceof LocalWireInstance) {
						// do nothing when user clicks wire
					}
					else throw new ExceptionHandler("Unexpected thing: " + o + " of class " + o.getClass());
				}
				else {
					listener.fireSelectInstance(null);
				}
			}


			public void mousePressed(MouseEvent e)
			{
				requestFocusInWindow();
				Object o = getObjectUnderCursor(getComponentGrahpics(), e.getPoint());

				if (e.isPopupTrigger()) {
					popUpOver(e, o);
				} else {
					regularSelect(e, o);
				}

			}

			// we also check popup for releases for multi-platform compatibility
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					requestFocusInWindow();
					popUpOver(e, getObjectUnderCursor(getComponentGrahpics(), e.getPoint()));
				}
			}
		};
		
	final MouseWheelListener ma3 = new MouseWheelListener() {
		public void mouseWheelMoved(MouseWheelEvent e) {
			if (e.isControlDown()) {
				// zoom
				zoom(-e.getWheelRotation());
			} else {
				getParent().dispatchEvent(e);
			}
		}
	};
		

    public ComponentPanel(WorkSheet listener, Instance instance)
	{
		this.listener = listener;
		this.setBackground(Color.white);

		this.instance = instance;
		listener.tt.getEffector().collectProbes(new AffineTransform(), probes, instance);
		rc = new RenderContext(instance.getComponent(), viewLabels, false);
		cc = ((Simulator)listener.tt.getEffector()).getCompiledComponent();

		this.addMouseListener(ma2);
		this.addMouseMotionListener(ma1);
		this.addMouseWheelListener(ma3);

//		recomputeBounds();
    }


	static class InstanceSelector implements NodeRepresentantMapping<Object> {
		public final Instance i;

		public InstanceSelector(Instance i) {
			this.i = i;
		}

		public InstanceSelector getSubComponentMapping(SubComponent s) {
			try {
				return new InstanceSelector( ( (Instance_Complex)i).
											getSubComponentInstance(s.getName()));
			} catch (Exception ex) { throw new Error(ex); }
		}

		public Object getRepresentant(BasicEditorNode<?> node) {
			if (node instanceof Pin)      return new Instance_Pin(i, (Pin)node);
			if (node instanceof PinLabel) return new Instance_Pin(i, ((PinLabel)node).getParent());

			if (node instanceof databinding.wires.Probe)   node = ((databinding.wires.Probe)node).getParent();
			if (node instanceof Node)    node = ((Node)node).getParent().getParent();
			if (node instanceof Span)    node = ((Span)node).getParent().getParent();
			if (node instanceof Wire)    return new LocalWireInstance((Wire)node, ((Instance_Complex)i).w2wi.get(node), (Instance_Complex)i);
			return i;
		}

	}

	public static class LocalWireInstance implements ValuedObject {
		final Wire w;
		final Instance_Wire wi;
		final Instance_Complex ic;

		public LocalWireInstance(Wire w, Instance_Wire wi, Instance_Complex ic) {
			assert ic.w2wi.get(w) == wi;
			this.w = w;
			this.wi = wi;
			this.ic = ic;
		}

		public Instance_Complex getParent() { return ic; }

		public String getFullString(NumberFormat format) {
			return w.getName() + ": " + wi.value.toFormattedString(format);
		}

		public String getLocalName() {
			return w.getName();
		}

		public String getFullName() {
			return ic.getHybridName(w.getName());
		}

		public Instance_Simple getValue() {
			return wi.value;
		}

		public boolean equals(Object o) {
			LocalWireInstance lwi = (LocalWireInstance)o;
			assert !(lwi.ic == ic && lwi.w == w) || (lwi.w == w);
			return lwi.ic == ic && lwi.w == w;
		}

		public int hashCode() {
			return ic.hashCode() + w.hashCode();
		}
	}


	/** Need to call this method with g already transformed into COMPONENT coordinates */
	public Object getObjectUnderCursor(Graphics2D g, Point cursor)
	{
		try {
				AffineTransform t = g.getTransform();
//				g.getDeviceConfiguration().getNormalizingTransform();
//				t.translate( -bounds.getX(), -bounds.getY());
				Point2D pos = t.inverseTransform(cursor, null);

				NodeDist<Probe> nd_p = new NodeDist<Probe>();
				if (viewValues) {
					for (Probe p : probes) {
						double d = p.getDist(pos, g, listener.getNumberFormat());
						if (d < nd_p.d) {
							nd_p.d = d;
							nd_p.node = p;
						}
					}
				}
				NodeDist<Object> nd_o = rc.getRenderRoot().getNode(pos, g, new InstanceSelector(instance));

				if (nd_o.node == null && nd_p.node == null) return null;

				// only threshold here
				return  nd_o.d < nd_p.d
				? nd_o.d < 5 ? nd_o.node : null
				: nd_p.d < 5 ? nd_p.node.getUnderlyingNode() : null;

//			Object o = listener.tt.getEffector().getInstance().getObject(pos, (Graphics2D)getGraphics(), viewLabels);
//			if (o != null) return o;
		} catch (NoninvertibleTransformException ex) { throw new Error(ex); }
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D)g;

		// TODO: make ComponentPanel transformation-aware
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		AffineTransform t = g2.getTransform();
		g2.translate(MARGIN, MARGIN);
		g2.transform(normTrans);
		g2.scale(zoom, zoom);
		g2.translate(-bounds.getX(), -bounds.getY());


		rc.getRenderRoot().render(g2, cc);

		if (viewValues) {
			g2.setStroke(GUIHelper.unitStroke);
			for (Probe p : probes) {
				p.render(g2, listener.getNumberFormat());
			}
		}
//			rc.getRenderRoot().renderValues(g2, listener.tt.getEffector().getInstance());
//			Component c = listener.tt.getEffector().getComponent();
//			if (this != null) throw new Error("Fix next lines");
//			c.draw(offset, g2, drawPins, true);
//			if (viewLabels) c.drawLabels(offset, g2);
//			if (viewValues) c.drawValues(offset, g2, true, listener.tt.getEffector().getInstance(), listener.getNumberFormat());

		g2.setTransform(t);
	}

	public ImageSource getIS(final boolean renderLabels, final boolean renderValues) {
		return new ImageOutputter(rc.getRoot(), cc, renderLabels, false) {
			public void drawImage(Graphics2D g) {
				super.drawImage(g);
				if (renderValues) {
					g.setStroke(GUIHelper.unitStroke);
					for (Probe p : probes) {
						p.render(g, listener.getNumberFormat());
					}
				}
			}
		};
	}


//	public Rectangle getBounds(Graphics2D g2) {
//		return pixelBound;
////		throw new Error("Correct these lines");
//		//return bounds; // this works because we use the origin (instead of offset) in drawImage
//	}
//
//	public void drawImage(Graphics2D g2) {
//		Rectangle bounds = getBounds(g2);
//		g2.setColor(Color.white);
//		g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
//
//		Component c = listener.tt.getEffector().getComponent();
//
//		Point origin = new Point(0,0);
//		if (this != null) throw new Error("Fix next lines");
////		c.draw(origin, g2, drawPins, true);
////		if (viewLabels) c.drawLabels(origin, g2);
////		if (viewValues) c.drawValues(origin, g2, true, listener.tt.getEffector().getInstance(), listener.getNumberFormat());
//	}
//
//	public BufferedImage getPicture()
//	{
//		throw new Error("Correct these lines");
////		BufferedImage im = new BufferedImage(bounds.width, bounds.height, BufferedImage.TYPE_INT_RGB);
////    	Graphics2D g2 = (Graphics2D)im.getGraphics();
////		g2.setColor(Color.white);
////		g2.fillRect(0, 0, bounds.width, bounds.height);
////
////		Component c = listener.tt.getEffector().getComponent();
////		if (this != null) throw new Error("Fix next lines");
////		c.draw(offset, g2, drawPins, true);
////		if (viewLabels) c.drawLabels(offset, g2);
////		if (viewValues) c.drawValues(offset, g2, true, listener.tt.getEffector().getInstance(), listener.getNumberFormat());
////		return im;
//	}

	private void recomputeBounds() {
		Graphics2D  g2 =    (Graphics2D)getGraphics();
		assert g2 != null;
		assert rc != null;

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		AffineTransform f = new AffineTransform(normTrans);
		f.scale(zoom, zoom);
		g2.transform(f);

		Rectangle2D r = rc.getRenderRoot().getDeepBound(g2);
		if (r == null) r = new Rectangle2D.Double(); // can happen !!!
//		r.add(0,0); // include origin;

		bounds = r; //GUIHelper.grow(MARGIN, r, null);
		pixelBound = GUIHelper.transform(f, bounds).getBounds();
		// grow MARGIN in device space
		GUIHelper.grow(MARGIN, pixelBound, pixelBound);
		pixelBound.translate(-pixelBound.x, -pixelBound.y);
		Dimension size = pixelBound.getSize();
		setPreferredSize(size);
		revalidate();
		repaint();
	}




    public void cycleLengthChanged(TimeTable tt)    {    }
    public void timeTableCleared(TimeTable tt)    {
		repaint(pixelBound);
    }

	public void eventSequenceCompleted(TimeTable tt)    {
//		System.out.println("repaint: " + pixelBound);
		repaint(pixelBound);
    }

    public void clockFlank(TimeTable tt, boolean rising)    {    }
    public void valuesChanged(TimeTable tt)    {    }

	public void initialisationProgramChanged(TimeTable tt) {}

	public void setViewLabels(boolean b) {
		this.viewLabels = b;
		rc = new RenderContext(instance.getComponent(), viewLabels, false);
		if (super.isDisplayable()) recomputeBounds(); // take labels into/out of account
		this.repaint();
	}
	
	
	
	
	/* ================== scrollable stuff ==================  */
	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}
	public boolean getScrollableTracksViewportHeight() { 
		return getPreferredSize().height < getParent().getHeight();
	}
	

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 20;
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 100;
	}
	
}
