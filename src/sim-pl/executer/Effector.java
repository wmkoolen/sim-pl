/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.awt.geom.AffineTransform;
import java.io.File;
import java.util.List;

import databinding.Component;
import databinding.compiled.Compiled.CompiledComponent;
import executer.timetable.TimeTable;

public interface Effector
{
	public Component            getComponent();
	public CompiledComponent<?> getCompiledComponent();
	public Instance             getInstance();
	public File                 getComponentFile();


	/** reset all values to initial state (all ones) */
	public void         clear()                                         throws Exception;
	public void         onInit(TimeTable tt)                            throws Exception;

	public void         onClockRising(TimeTable tt)                     throws Exception;
	public void         onClockFalling(TimeTable tt)                    throws Exception;

	public void         recalculateForComponent(Instance i, TimeTable tt) throws Exception;
	public void         onClose() throws Exception;

	public void         collectProbes(AffineTransform f, List<Probe> probes, Instance instance);
}
