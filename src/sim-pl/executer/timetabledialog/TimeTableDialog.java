/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.timetabledialog;

import icons.IconManager;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import nbit.NumberFormat;
import util.ExceptionHandler;
import executer.ExecuterDialog;
import executer.NamespaceUpdate;
import executer.TimeTableListener;
import executer.WorkSheet;
import executer.timetable.ContentChangeEvent;
import executer.timetable.TimeTable;

class ClockUpdate extends NamespaceUpdate
{
	boolean isRising;

	public ClockUpdate(int clockTime, boolean isRising)
	{
		super(new ContentChangeEvent(null, clockTime));
		this.isRising = isRising;
	}

	public String getInstanceString()
	{
		return "All components";
	}

	public String getUpdateString(NumberFormat representation)
	{
		return isRising ? "CLOCK_RISING" : "CLOCK_FALLING";
	}

}

class TimeTableModel extends AbstractTableModel
{
	NamespaceUpdate [] events = new NamespaceUpdate[0];
	WorkSheet listener;

	public TimeTableModel(WorkSheet listener)
	{
		this.listener = listener;
	}

	public void refresh()
	{
		TimeTable tt = listener.tt.clone();
		events = new NamespaceUpdate[tt.size() + 1]; // Maak ruimte voor clock event

		int i = 0;

		int nextClockTime = tt.getNextClockTime();


		// Eerst alle events voor de clock event
		while (tt.size() != 0 && tt.getNextEventTime() < nextClockTime) {
			events[ i++] = tt.deleteMin();
		}

		// Dan de clock event
		events[i++] = new ClockUpdate(nextClockTime, tt.isNextClockRising());

		// Dan alle events na de clock event
		while (tt.size() != 0) {
			events[ i++] = tt.deleteMin();
		}

		fireTableDataChanged();
	}


	static final String [] names  = {"Time", "Instance", "Update"};

	public String getColumnName(int col)
	{
		return names[col];
	}

	public int getColumnCount()
	{
		return names.length;
	}

	public int getRowCount()
	{
		return events.length;
	}

	public Object getValueAt(int row, int col)
	{
		switch (col)
		{
			case 0: return new Integer(events[row].cce.time);
			case 1: return events[row].getInstanceString();
			case 2: return events[row].getUpdateString(listener.getNumberFormat());
			default: throw new ExceptionHandler("TimeTableDialog::getValue Error");
		}
	}
}


public class TimeTableDialog extends ExecuterDialog implements TimeTableListener
{

    JTable          jTable1;
	JScrollPane     scroll =            new JScrollPane();
    BorderLayout    borderLayout1 =     new BorderLayout();

	TimeTableModel  ttm;

	public TimeTableDialog(WorkSheet listener)
    {
		super(listener, "Time Table");

		ttm = new TimeTableModel(listener);
		jTable1 = new JTable(ttm);

		jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		jTable1.getColumnModel().getColumn(0).setPreferredWidth(75);
		jTable1.getColumnModel().getColumn(1).setPreferredWidth(150);
		jTable1.getColumnModel().getColumn(2).setPreferredWidth(2000);

		timeTableChanged();

		jbInit();
    }

	public String getID() { return "TimeTableDialog"; }

//	public void setCycleLength(int cycleLength)
//	{
//		listener.tt.setCycleLength(cycleLength);
//	//		this.refreshTitle();
//	}
//
//	public int getCycleLength()
//	{
//		return listener.tt.getCycleLength();
//	}

//	public void Init()
//    {
//		try
//		{
//			System.gc();
//	//			listener.tsd.clearStoredSignals();
//
//			listener.tt.clear();
//	//			listener.tt.getEffector().init(listener.tt);
//
//	//			listener.fireTimeTableChanged(true);
//		}
//		catch (Exception ex)
//		{
//			ExceptionHandler.handleException(ex);
//		}
//    }

//    public void Step()
//    {
//		try
//		{
//	//			System.gc();
//
//		    listener.tt.step();
//
//	//			listener.fireTimeTableChanged(false);
//		}
//		catch (Exception ex)
//		{
//			ExceptionHandler.handleException(ex);
//		}
//    }
//
//
//
//    private void cycle(int cycles)
//    {
//		try
//		{
//	//			System.gc();
//
//			for (int i = 0; i < cycles; i++)
//			{
//				listener.tt.cycle();
//			}
//
//	//			listener.fireTimeTableChanged(false);
//		}
//		catch (Exception ex)
//		{
//			ExceptionHandler.handleException(ex);
//		}
//    }
//
//
//    void RunCycles()
//    {
//		String  cycleStr =   JOptionPane.showInputDialog(this, "How many cycles?", "Run cylces", JOptionPane.QUESTION_MESSAGE);
//		if (cycleStr != null)
//		{
//		    int     cycleInt;
//
//		    try
//		    {
//			    try                                 {cycleInt =  Integer.parseInt(cycleStr);}
//			    catch (NumberFormatException ex)    { throw new Exception("'" + cycleStr + "' is not a valid cycle length"); }
//			    if (cycleInt < 0) throw new Exception("You can not reverse time!");
//
//		        cycle(cycleInt);
//		    }
//		    catch (Exception ex)
//		    {
//			    ExceptionHandler.handleException(ex);
//		    }
//		}
//    }

	public void refreshTitle(){
		this.setTitle("Time Table" + ": current time is " + listener.tt.getCurrentTime());
	}

	public void timeTableChanged()
	{
		refreshTitle();
		ttm.refresh();
	}


	public void representationChanged()
	{
		ttm.refresh();
	}

    private void jbInit()
    {
		this.getContentPane().setLayout(borderLayout1);
        this.getContentPane().add(scroll,  BorderLayout.CENTER);

		scroll.getViewport().add(jTable1);

		setIconImage(IconManager.loadImage(TimeTableDialog.class, "TTD.png"));
    }

	public void initialisationProgramChanged(TimeTable tt) {}
    public void clockFlank(TimeTable tt, boolean rising)    {    }
    public void valuesChanged(TimeTable tt) {}

    public void cycleLengthChanged(TimeTable tt)    {
		refreshTitle();
    }

	public void timeTableCleared(TimeTable tt) {
		refreshTitle();
		timeTableChanged();
	}

    public void eventSequenceCompleted(TimeTable tt) {
		timeTableChanged();
	}

	public KidPOD getPOD() {
		throw new Error("Not Implemented");
	}

}
