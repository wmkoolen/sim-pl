/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import nbit.NumberFormat;
import util.ExceptionHandler;

import compiler.wisent.Instance_Simple;
import compiler.wisent.UndefinedIdentifierException;

import databinding.Pin;

public class Instance_Pin implements ValuedObject
{
	public final Instance  instance;
	public final Pin       pin;

    public Instance_Pin(Instance instance, Pin pin)
    {
		if (instance == null || pin == null) throw new ExceptionHandler("Invalid arguments");
		this.instance = instance;
		this.pin =      pin;
    }

	public String toString()
	{
		return instance.getHybridName(pin.getName());
	}

	public Instance_Simple getValue() {
		try {
			return (Instance_Simple)instance.getNamespace().get(pin.getName()).getValue();
		} catch (UndefinedIdentifierException ex) {
			throw new ExceptionHandler(ex);
		}
	}

	public String getFullString(NumberFormat format)
	{
		return this + ": " + getValue().toFormattedString(format);
	}

	public boolean equals(Object o)	{
		Instance_Pin p = (Instance_Pin)o;
		return p.instance == instance && p.pin == pin;
	}

	public int hashCode() {
		return instance.hashCode() + pin.hashCode();
	}

	public String getLocalName() { return pin.getName(); }
	public String getFullName()  { return instance.getHybridName(pin.getName()); }
}
