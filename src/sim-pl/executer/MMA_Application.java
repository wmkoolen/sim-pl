/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import util.VersionManager;
import util.WAction;
import about.AboutWindow;

import com.centerkey.utils.BareBonesBrowserLaunch;


/** Multi Monitor Aware Application
 */

public abstract class MMA_Application extends JFrame
{
	public abstract String getProgramName();

	protected final WAction aAbout = new WAction(
		   "About",
		   ExecuterIconManager.ABOUT_SMALL, ExecuterIconManager.ABOUT_LARGE,
		   "Information about SIM-PL",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_A) {
		public void actionPerformed(ActionEvent e) {
			AboutWindow about = new AboutWindow(MMA_Application.this, getProgramName());
			about.setVisible(true);
			about.dispose();
		}
	};


	protected final WAction aWebPage = new WAction(
		   "Web Page",
		   ExecuterIconManager.ABOUT_SMALL, ExecuterIconManager.ABOUT_LARGE,
		   "Go to " + VersionManager.currentURL,
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_W) {
		public void actionPerformed(ActionEvent e) {
			BareBonesBrowserLaunch.openURL(VersionManager.currentURL);
		}
	};

}
