/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.instanceeditor;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

import nbit.NumberFormat;

import compiler.wisent.Instance_Simple;

public class Instance_SimpleRenderer extends JTextField implements TableCellRenderer
{
	public static Class forClass()
	{

		return Instance_Simple.class;
	}


	Color       background =        this.getBackground();
	Color       foreground =        this.getForeground();

	NumberFormat        representation =   NumberFormat.HEXADECIMAL;
	Instance_Simple     value =            null;


	public Instance_SimpleRenderer()
	{
		setHorizontalAlignment(LEFT);
		this.setEnabled(true);
		this.setOpaque(true);
		this.setEditable(false);
	}

	public void setRepresentation(NumberFormat representation)
	{
		this.representation =   representation;
		if (value != null)      super.setText(value.toFormattedString(representation));
	}

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	{
		assert representation != null;
		setForeground(isSelected ? table.getSelectionForeground() : foreground);
		setBackground(isSelected ? table.getSelectionBackground() : background);
		this.value = (Instance_Simple)value;
		super.setText(this.value.toFormattedString(representation));
        return this;
    }
}
