/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.instanceeditor;

import icons.IconManager;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import databinding.HPL;
import databinding.InvalidHPLException;
import databinding.Memory;
import databinding.ResolutionException;
import databinding.Simple;
import databinding.Storage;
import databinding.wires.Node;
import executer.ExecuterDialog;
import executer.Simulator.Instance_Simple;
import executer.TimeTableListener;
import executer.WorkSheet;
import executer.timetable.AliasResolver;
import executer.timetable.InstanceAliasResolver;
import executer.timetable.TimeTable;


public class MemoryEditorDialog extends ExecuterDialog implements TimeTableListener
{
	final Instance_Simple instance;
	final Storage         s;
	final InstanceTable   itm;


	public MemoryEditorDialog(WorkSheet listener, POD p) throws InvalidHPLException, ResolutionException {
		this(listener,
			 (Instance_Simple)listener.tt.getEffector().getInstance().resolve(new HPL(p.instance)),
			 p.name);
	}

	private MemoryEditorDialog(WorkSheet listener, Instance_Simple instance, String storage) throws ResolutionException {
		this(listener, instance, resolve(instance.getComponent().getMemory(), storage));
	}

	private static Storage resolve(Memory m, String storage) throws ResolutionException {
		Storage s = m.getByName(storage);
		if (s == null) throw new ResolutionException(m,storage);
		return s;
	}

	public MemoryEditorDialog(WorkSheet listener, Instance_Simple instance, Storage s)
	{
		super(listener, "Memory Editor");
		this.instance = instance;
		this.s = s;

		setIconImage(IconManager.loadImage(MemoryEditorDialog.class, "IE.png"));

		setTitle("Memory Editor: " + instance.getHybridName(null) + ":" + s.getName());

		Simple cs = instance.getComponent();

		assert cs.getMemory().getStorages().contains(s);

		AliasResolver ar = listener.tt.getAliasResolver();
		InstanceAliasResolver iar = ar == null ? null : ar.getAliases(instance);

		itm = new InstanceTable(new InstanceTableModel_Mem(instance, s, iar));
		this.getContentPane().add(new JScrollPane(itm), BorderLayout.CENTER);

		// Fake a representationChanged event so all tables have right numerical format
		representationChanged();
	}

	String id = Node.IDs.getNewID();
	public String getID() { return  id; }


	public void representationChanged() {
		itm.setRepresentation(listener.getNumberFormat());
	}

	public void reLoad(boolean dataOnly) {
		itm.reLoad(dataOnly ? null : listener.tt);
	}


	public void cycleLengthChanged(TimeTable tt)    {    }
	public void timeTableCleared(TimeTable tt)    {
		// aliases can have changed by a compile before this 'init'
		reLoad(false);
	}
	public void clockFlank(TimeTable tt, boolean rising)    {    }
	public void valuesChanged(TimeTable tt)    {    }

	public void eventSequenceCompleted(TimeTable tt)    {
		reLoad(true);
	}

	public void initialisationProgramChanged(TimeTable tt) {}

	public POD getPOD() {
		return new POD(getID(), instance.getFullName(), s.getName());
	}

	public static class POD implements KidPOD {
		public String id;
		public String instance;
		public String name;

		public POD() {}
		public POD(String id, String instance, String name) {
			this.id = id;
			this.instance = instance;
			this.name = name;
		}

		public ExecuterDialog createDialog(WorkSheet w) throws ResolutionException, InvalidHPLException {
			return new MemoryEditorDialog(w, this);
		}

		public String getID() {
			return id;
		}

	}

}
