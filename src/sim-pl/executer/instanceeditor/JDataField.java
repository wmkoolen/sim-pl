/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.instanceeditor;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import nbit.NumberFormat;
import nbit.nBit;

import compiler.wisent.Instance_Simple;
import compiler.wisent.Type_Simple;

/** DO NOT USE A DOCUMENTLISTENER ON THIS TEXT FIELD'S DOCUMENT, WE USE THE
 * DOCUMENT EVENT MECHANISM OURSELVES.  IF YOU WANT TO KEEP INFORMED ABOUT THE
 * STATE, REGISTER A CHANGE LISTENER INSTEAD
 */

public class JDataField extends JTextField {
	public static final int OK =       0;
	public static final int OVERFLOW = 1;
	public static final int INVALID =  2;

	private static final Color [] colors = {Color.black, Color.cyan, Color.red};

	Instance_Simple     value =             null;
	Type_Simple         type =              null;
	NumberFormat        representation =    NumberFormat.DECIMAL;
	int                 state =             INVALID;
	ChangeListener      cl;

	public JDataField() {
		setHorizontalAlignment(LEFT);
//		this.setDocument(new DataDocument());

		this.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				updateColor();
			}

			public void removeUpdate(DocumentEvent e) {
				updateColor();
			}

			public void changedUpdate(DocumentEvent e) {
				updateColor();
			}
		});
	}

	public void setRepresentation(NumberFormat representation) {
		this.representation = representation;
		if (value != null)
			super.setText(value.toFormattedString(representation));
	}

	public void setValue(Instance_Simple value) {
		this.value = value;
		this.type = value.type;
		super.setText(value.toFormattedString(representation));
	}

	public Instance_Simple getValue() {
		assert state != INVALID;
		return this.value;
	}

	public int getState() {
		return state;
	}


	private void updateColor() {
		assert type != null;
		int state = OK;

		try
		{
			nBit fullValue =    NumberFormat.parse_nBit(this.getText(),representation);
			value =             new Instance_Simple(type, fullValue);

			if (type.signed)
			{
				nBit maxValue = nBit.allOnes(type.bits-1).cast(type.bits).NOT_BIT(); // 0b100000....
//				System.out.println(" Full value: " + fullValue.toString(nBit.HEXADECIMAL, true));
//				System.out.println("-Full value: " + fullValue.NEG().toString(nBit.HEXADECIMAL, true));
//				System.out.println(" Max  value: " + maxValue. toString(nBit.HEXADECIMAL, true));

				if (fullValue.getBit(fullValue.bits-1)) // user input is negative
				{
					if (fullValue.NEG().compareTo(maxValue) > 0) state = OVERFLOW;
				}
				else // user input is positive
				{
					if (fullValue.compareTo(maxValue) >= 0) state = OVERFLOW;
				}
			}
			else
			{
				if (fullValue.compareTo(value.value) != 0)   state = OVERFLOW;
			}
		}
		catch (Exception ex)
		{
			value = null;
			state = INVALID;
		}

		if (this.state != state) {
			this.state = state;
			this.setForeground(colors[state]);
			if (cl != null) cl.stateChanged(new ChangeEvent(this));
		}

		assert (state == INVALID) == (value == null);
	}

	public void addChangeListener(ChangeListener cl) {
		assert this.cl == null;
		this.cl = cl;
	}



//	class DataDocument extends PlainDocument
//	{
//		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
//		{
//			super.insertString(offs, str, a);
//			updateColor();
//		}
//
//		public void remove(int offs, int len) throws BadLocationException
//		{
//			super.remove(offs, len);
//			updateColor();
//		}
//	}
}
