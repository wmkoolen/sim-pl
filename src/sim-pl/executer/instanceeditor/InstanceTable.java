/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.instanceeditor;

import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import nbit.NumberFormat;
import util.ExceptionHandler;
import util.MyFormatter;

import compiler.wisent.Instance_Array;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Type_Array;
import compiler.wisent.UndefinedIdentifierException;

import databinding.Storage;
import executer.Instance;
import executer.timetable.AliasResolver;
import executer.timetable.InstanceAliasResolver;
import executer.timetable.TimeTable;

class InstanceTable extends JTable
{
	final Instance_SimpleEditor     ise = new Instance_SimpleEditor();
	final Instance_SimpleRenderer   isr = new Instance_SimpleRenderer();

	public InstanceTable(InstanceTableModel tm)
	{
		super(tm);
	    setDefaultEditor  (ise.forClass(), ise);
	    setDefaultRenderer(isr.forClass(), isr);
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	public TableCellEditor getCellEditor(int row, int col)
	{
		return getDefaultEditor(this.getValueAt(row, col).getClass());
	}

	public TableCellRenderer getCellRenderer(int row, int col)
	{
		return getDefaultRenderer(getValueAt(row, col).getClass());
	}

	public void setRepresentation(NumberFormat representation)
	{
		ise.setRepresentation(representation);
		isr.setRepresentation(representation);
		((InstanceTableModel)this.getModel()).reLoad(null);
	}

	public void reLoad(TimeTable tt) {
		((InstanceTableModel)getModel()).reLoad(tt);
	}
}

abstract interface InstanceTableModel extends TableModel {
	public abstract void reLoad(TimeTable tt);
}


class InstanceTableModel_Mem extends AbstractTableModel implements InstanceTableModel
{
	executer.Instance        instance;
	Type_Array      ta;
	Instance_Array  ia;
	Storage         storage;
	InstanceAliasResolver iar;
	static final String [] columnNames = {"Index", "Alias", "Value"};

	public InstanceTableModel_Mem(executer.Instance instance, Storage storage, InstanceAliasResolver iar) {
		this.iar = iar;
		this.instance = instance;
		try {
			this.storage = storage;
			ia = (Instance_Array) (instance.getNamespace().get(storage.getName())).getValue();
			ta = (Type_Array)ia.getType();
		} catch (UndefinedIdentifierException ex) {
			throw new ExceptionHandler(ex);
		}
	}

	public int getRowCount() {
		return ia.size();
	}

	public int getColumnCount() {
		return 3;
	}


	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex == 0) return new Integer(rowIndex);
		if (columnIndex == 1) {
			if (iar != null) {
				List<String> names = iar.resolveAliases(rowIndex);
				if (names != null)
					return MyFormatter.join(",", names);
			}
			return "";
		}
		assert columnIndex == 2;
		return ia.getElement(rowIndex);
	}

	public boolean isCellEditable(int row, int col)	{
		return col == 2;
	}

	public void reLoad(TimeTable tt) {
		if (tt != null) {
			// we update iar too, to have new aliases after a compile-init sequence
			AliasResolver ar = tt.getAliasResolver();
			iar = ar == null ? null : ar.getAliases(instance);
		}

		try {
			ia = (Instance_Array) ( instance.getNamespace().get(storage.getName())).getValue();
		} catch (UndefinedIdentifierException ex) { throw new ExceptionHandler(ex); }

		this.fireTableRowsUpdated(0, ia.size()-1);
	}

	public void applyChanges(TimeTable tt, Instance instance) {
		System.err.println("TODO");
	}

	public void setValueAt(Object value, int row, int col)  {
		assert col == 2;
		ia.getElement(row).changeInto((Instance_Simple)value);
	}
}

