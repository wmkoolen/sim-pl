/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JOptionPane;

import namespace.ComponentTable;
import nbit.NumberFormat;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.DockingWindowAdapter;
import net.infonode.docking.DockingWindowListener;
import net.infonode.docking.FloatingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.View;
import net.infonode.docking.ViewSerializer;
import net.infonode.docking.WindowBar;
import net.infonode.docking.properties.RootWindowProperties;
import net.infonode.docking.theme.DockingWindowsTheme;
import net.infonode.docking.util.MixedViewHandler;
import net.infonode.docking.util.PropertiesUtil;
import net.infonode.docking.util.StringViewMap;
import net.infonode.util.Direction;

import org.apache.commons.codec.binary.Base64;
import org.castor.util.ConfigKeys;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.util.Configuration;
import org.exolab.castor.util.LocalConfiguration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.XMLMappingLoader;

import util.RelativeDirectoryResolver;
import databinding.Component;
import executer.ExecuterDialog.KidPOD;
import executer.programeditor.ProgramEditorDialog2;
import executer.timesequencediagram.TimeSequenceDiagram;
import executer.timetable.TimeTable;
import executer.timetabledialog.TimeTableDialog;

public class WorkSheet implements TimeTableListener {
	private static final boolean DEBUG = false;

	public RootWindow rootWindow;

	private Executer executer;
	// most important of all, the simulator core
	public final TimeTable tt;

	// the following can only be initialised after the time table is defined
	// the fixed dialogs
	public final ComponentView         componentPanel;
	public final TimeSequenceDiagram   tsd;
	public final TimeTableDialog       ttd;
	public final ProgramEditorDialog2  ped;

	private final ExecuterDialog []    fixDialogs;

	// the fixed (above four) and variable dialogs (for ease of iteration)
	private final ArrayList<ExecuterDialog> allDialogs = new ArrayList<ExecuterDialog>();
	// the variable dialogs
	private final ArrayList<ExecuterDialog> varDialogs = new ArrayList<ExecuterDialog>();

	private NumberFormat numberformat = NumberFormat.DECIMAL;


//	public static interface ExecuterDialogListener extends EventListener {
//		public void edEvent(ExecuterDialogEvent e);
//	}
//
//	public static class ExecuterDialogEvent {
//		public final ExecuterDialog ed;
//		public final boolean opened;
//
//
//		public ExecuterDialogEvent(ExecuterDialog ed, boolean opened) {
//			this.ed = ed;
//			this.opened = opened;
//		}
//	}

//	EventDispatchManager<ExecuterDialogEvent,ExecuterDialogListener> edm = new EventDispatchManager<ExecuterDialogEvent,ExecuterDialogListener>() {
//		protected void dispatchEvent(ExecuterDialogListener listener, ExecuterDialogEvent event) {
//			listener.edEvent(event);
//		}
//	};

	private final DockingWindowListener dwl = new DockingWindowAdapter() {
		public void windowClosed(DockingWindow w) {
			if (DEBUG) System.out.println("Dialog closed and forgotten");
			ExecuterDialog ed = (ExecuterDialog)((View)w).getComponent();
			assert varDialogs.contains(ed);
			assert allDialogs.contains(ed);
			varDialogs.remove(ed);
			allDialogs.remove(ed);
			tt.removeTimeTableListener(ed);
			w.removeListener(dwl);
			rootWindow.removeView((View)w);
		}
	};


//	private final WindowListener wl = new WindowAdapter() {
//		public void windowClosed(WindowEvent e) {
//			FloatingWindow fw = (FloatingWindow)((JDialog)e.getWindow()).getContentPane();
//			fw.close();
//		}
//	};

	private final DockingWindowListener dwl2 = new DockingWindowAdapter() {
		public void windowRestored(DockingWindow w) {
			if (DEBUG) System.out.println("windowRestored " + w+"/"+w.getClass());
		}


		// close children of floating window while the floating window still is child of its parent
		public void windowClosing(DockingWindow w) {
			if (DEBUG) System.out.println("windowClosing " + w+"/"+w.getClass());
			if (w instanceof FloatingWindow) {
				ArrayList<DockingWindow> subtree = new ArrayList<DockingWindow>();
				getRec(((FloatingWindow)w).getWindow(), subtree);
				for (DockingWindow dw : subtree) dw.close();
			}
		}

		private void getRec(DockingWindow w, ArrayList<DockingWindow> subtree) {
			subtree.add(w);
			for (int i = 0; i < w.getChildWindowCount(); ++i) {
				getRec(w.getChildWindow(i), subtree);
			}
		}

		public void windowClosed(DockingWindow w) {
			if (DEBUG) System.out.println("windowClosed " + w+"/"+w.getClass());
		}
		public void windowUndocked(DockingWindow w) {
			if (DEBUG) System.out.println("windowUndocked " + w+"/"+w.getClass());
		}
		public void windowDocked(DockingWindow w) {
			if (DEBUG) System.out.println("windowDocked " + w+"/"+w.getClass());
		}
		public void windowAdded(DockingWindow p, DockingWindow w) {
			if (DEBUG) System.out.println("windowAdded " + w+"/"+w.getClass() + " to " + p+"/"+p.getClass());
		}
		public void windowRemoved(DockingWindow p, DockingWindow w) {
			if (DEBUG) System.out.println("windowRemoved " + w+"/"+w.getClass() + " from " + p+"/"+p.getClass());
		}
	};



	private WorkSheet(File parentDir, Executer executer, POD p ) throws Exception {
		File f = new File(p.componentFile);
		if (!f.isAbsolute()) f = new File(parentDir, p.componentFile);
		Component c = ComponentTable.load(f);
		this.tt = new TimeTable(new Simulator(c, f));
		this.executer = executer;

//		edm.addListener(executer);

		// initialse fixed dialogs from data
		tsd = new TimeSequenceDiagram(this, p.tsd);
		ttd = new TimeTableDialog(this);
		ped = new ProgramEditorDialog2(this);
		componentPanel = new ComponentView(this, p.cp);

		fixDialogs = new ExecuterDialog[] {componentPanel, tsd, ttd, ped};
		allDialogs.addAll(Arrays.asList(fixDialogs));

		// initialse variable dialogs from data
		if (p.kids != null) for (KidPOD kp : p.kids) {
			try {
				ExecuterDialog ed = kp.createDialog(this);
				insertVariableDialog(ed);
				dynamicMap.addKids(kp.getID(), ed);
			} catch (Exception ex) {
				if (JOptionPane.showConfirmDialog(null, "Could not create window:\n" + ex.getMessage() + "\n\nChoose 'Yes' to skip this window, 'No' to abort", "Window creation error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) == JOptionPane.NO_OPTION) throw ex;
			}
		}

		// take care of sub-windows and their relation to the Window menu

		StringViewMap staticMap = new StringViewMap();

		for (ExecuterDialog ed : fixDialogs) {
			staticMap.addView(ed.getID(), ed.v);
		}

		MixedViewHandler viewMap = new MixedViewHandler(staticMap, dynamicMap);

		rootWindow = new RootWindow(viewMap);
		rootWindow.addListener(dwl2);
		theme(rootWindow);

		// read view from layout string

		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(Base64.decodeBase64(p.layout.replaceAll("\\n","\r\n").getBytes())));
		rootWindow.read(ois);
		ois.close();

		tt.addTimeTableListener(this);
		for (ExecuterDialog d : fixDialogs) {
			tt.addTimeTableListener(d);
//			edm.fireEvent(new ExecuterDialogEvent(d, true));
		}

		// loading fixed dialogs sets their titles. Here we post-correct that.
		ped.refreshTitle();
		// tsd only has "Time Sequence Diagram" as title
		ttd.refreshTitle();
		// componentPanel sets title itself (it is modular)
	}


	private static void theme(RootWindow rootWindow) {
		// Set theme
		DockingWindowsTheme theme = new net.infonode.docking.theme.ClassicDockingTheme();
		rootWindow.getRootWindowProperties().addSuperObject(theme.getRootWindowProperties());

		// Enable title bars
		RootWindowProperties titleBarStyleProperties = PropertiesUtil.createTitleBarStyleRootWindowProperties();
		rootWindow.getRootWindowProperties().addSuperObject(titleBarStyleProperties);

		// Enable Window bar
		rootWindow.getWindowBar(Direction.DOWN).setEnabled(true);
	}


	class InfoNodeToXMLSerialiser implements ViewSerializer {
		HashMap<String, ExecuterDialog> kids = new HashMap<String,ExecuterDialog>();

		public void addKids(String kid, ExecuterDialog ed) {
			if (DEBUG) System.out.println("Map id " + kid + " to " +  ed.getClass().getName());
			kids.put(kid, ed);
		}

		public void writeView(View view, ObjectOutputStream objectOutputStream) throws IOException {
			ExecuterDialog ed = (ExecuterDialog)view.getComponent();
			if (DEBUG) System.out.println("Write " + ed.getClass().getName() + " as ID " + ed.getID());
			objectOutputStream.writeUTF(ed.getID());
		}

		public View readView(ObjectInputStream objectInputStream) throws IOException {
			String id = objectInputStream.readUTF();
			if (DEBUG) System.out.println("Request ID " + id);
			ExecuterDialog ed = kids.get(id);
			if (ed == null) {
				System.err.println("Non-existing id " + id +"\nWorkaround for nasty closed-views-saved-in-layout-but-not-xml bug triggered.");
				return null;
			}
			return ed.v;
		}
	}

	private final InfoNodeToXMLSerialiser dynamicMap = new InfoNodeToXMLSerialiser();



	public WorkSheet(Executer executer, Effector effector) throws Exception {
		this.executer = executer;
		this.tt = new TimeTable(effector);

//		edm.addListener(executer);

		componentPanel = new ComponentView(this, effector.getInstance());
		tsd = new TimeSequenceDiagram(this);
		ttd = new TimeTableDialog(this);
		ped = new ProgramEditorDialog2(this);
		fixDialogs = new ExecuterDialog[] {componentPanel, tsd, ttd, ped};
		allDialogs.addAll(Arrays.asList(fixDialogs));

		// take care of sub-windows and their relation to the Window menu

		StringViewMap staticMap = new StringViewMap();

		for (ExecuterDialog ed : fixDialogs) {
			staticMap.addView(ed.getID(), ed.v);
		}

		MixedViewHandler viewMap = new MixedViewHandler(staticMap, dynamicMap);


		// Use static layout
		DockingWindow myLayout =
			new SplitWindow(true, 2 / 3f,
							new SplitWindow(false, 2 / 3f, componentPanel.v, tsd.v),
							new SplitWindow(false, 1 / 3f, ttd.v, ped.v)
			);

		rootWindow = new RootWindow(false, viewMap, myLayout);
		rootWindow.addListener(dwl2);
		theme(rootWindow);


		tt.addTimeTableListener(this);
		for (ExecuterDialog d : fixDialogs) {
			tt.addTimeTableListener(d);
//			edm.fireEvent(new ExecuterDialogEvent(d, true));
		}
		ttd.v.close();
	}



	public void fireSelectInstance(Instance instance)
	{
		for (ExecuterDialog d : allDialogs)
			d.selectionChanged(instance);
	}

	public void fireSelectVN(ValuedObject pin)
	{
		if (tsd.contains(pin))  { tsd.removePin(pin);   tsd.refresh(); }
		else                    { tsd.addPin(pin);      tsd.refresh(); }
	}



	public void setNumberFormat(NumberFormat numberformat)
	{
		assert numberformat != null;
		this.numberformat = numberformat;

		NumberFormat [] formats = NumberFormat.getAvailableFormats();
		for (int i = 0; i < formats.length; i++)
		{
			if (formats[i].equals(numberformat))
			{
				executer.rbNF[i].setSelected(true);
				break;
			}
		}

		for (ExecuterDialog d : allDialogs)
			d.representationChanged();
	}

	public NumberFormat getNumberFormat()
	{
		return numberformat;
	}


	public boolean tryCloseWithUserCancel()
	{
		// DONT Ask if user sure  (user is not dumb)
//		if (JOptionPane.showConfirmDialog(this, "End current execution?", "Execution", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) return false;

		// Check for unsaved programs
		if (!this.ped.tryCloseWithUserCancel()) return false;

		for (ExecuterDialog ed : new ArrayList<ExecuterDialog>(allDialogs)) {
			ed.v.close();
		}

		for (int i = 0; i < rootWindow.getChildWindowCount(); i++) {
			assert rootWindow.getChildWindow(i) instanceof WindowBar;
		}

		assert varDialogs.isEmpty() : varDialogs;
		assert allDialogs.equals(Arrays.asList(fixDialogs));
		return true;
	}

	public void initialisationProgramChanged(TimeTable tt) {}
	public void cycleLengthChanged(TimeTable e)         {    }
	public void timeTableCleared(TimeTable e)           {    }
	public void eventSequenceCompleted(TimeTable e)     {    }
	public void clockFlank(TimeTable tt, boolean rising){    }
	public void valuesChanged(TimeTable e)              {    }


	private void insertVariableDialog(ExecuterDialog ed) {
		allDialogs.add(ed);
		varDialogs.add(ed);
		ed.v.addListener(dwl);
		tt.addTimeTableListener(ed);
	}


	public void addDialog(Point d, ExecuterDialog ed) {
		FloatingWindow fw = rootWindow.createFloatingWindow(d, new Dimension(10,10), ed.v);
		((Window)fw.getTopLevelAncestor()).pack();
		fw.getTopLevelAncestor().setVisible(true);
		insertVariableDialog(ed);

		// only fire ExecuterDialogEvent for fixed dialogs
	}


	private POD getPOD(File parentDir) {
		ArrayList<KidPOD> kids = new ArrayList<KidPOD>();
		for (ExecuterDialog d : varDialogs) {
			kids.add(d.getPOD());
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			rootWindow.write(oos);
			oos.close();
		} catch (IOException ex) { throw new Error(ex); }

		return new POD(RelativeDirectoryResolver.getRelativePath(parentDir,tt.getEffector().getComponentFile()),
					   "\n" + new String(Base64.encodeBase64(bos.toByteArray(), true)).replaceAll("\\r\\n", "\n"),
					   tt.getCycleLength(),
					   componentPanel.getPOD(),
					   tsd.getPOD(),
					   kids);
	}

	public void save(File f) throws Exception {
		getPOD(f.getParentFile()).save(f);
	}


	public static class POD {
		public String componentFile;
		public String layout;
		public Integer cycleLength;
		public ComponentView.POD cp;
		public TimeSequenceDiagram.POD tsd;
		public ArrayList<KidPOD> kids;


		public POD() {} // from XML constructor

		public POD(String componentFile, String layout, Integer cycleLength,
				   ComponentView.POD cp,
				   TimeSequenceDiagram.POD tsd,
				   ArrayList<KidPOD> kids) {
			this.componentFile = componentFile;
			this.layout = layout;
			this.cycleLength = cycleLength;
			this.cp = cp;
			this.tsd = tsd;
			this.kids = kids;
		}

		public void save(File f) throws Exception {
			Writer w = new OutputStreamWriter(new FileOutputStream(f),
											  ComponentTable.encoding);
			try {
				Marshaller marshaller = new Marshaller(w);
				marshaller.setEncoding(ComponentTable.encoding);
				marshaller.setMapping(map);
				marshaller.setMarshalExtendedType(true);
				marshaller.setMarshalAsDocument(true);
				marshaller.setLogWriter(new PrintWriter(System.err));
				marshaller.marshal(this);
			} finally {
				w.close();
			}
		}

		public static WorkSheet load(Executer e, File f) throws Exception {
			Reader reader = new FileReader(f);
			try {
				Unmarshaller unmarshaller = new Unmarshaller(map);
				unmarshaller.setClearCollections(true);

				// TODO: set to false to be really strict
				unmarshaller.setIgnoreExtraAttributes(true);
				unmarshaller.setIgnoreExtraElements(false);
				unmarshaller.setWhitespacePreserve(true);
				POD c = (POD)unmarshaller.unmarshal(reader);
				return new WorkSheet(f.getParentFile(), e, c);
			} finally {
				reader.close();
			}
		}

		/** The (global) castor xml mapping  */
		private static Mapping map = new Mapping();

		// static initialiser
		static {
			try {
//			Properties p_org_castor = org.castor.util.Configuration.getInstance().getProperties();
//			p_org_castor.setProperty(ConfigKeys.MAPPING_LOADER_FACTORIES, XMLMappingLoaderFactory.class.getCanonicalName());

				Properties p_org_castor = org.castor.util.Configuration.
					getInstance().getProperties();
				p_org_castor.setProperty(ConfigKeys.MAPPING_LOADERS,
										 XMLMappingLoader.class.
										 getCanonicalName());

				Properties p_org_exolab = LocalConfiguration.getInstance().
					getProperties();
				p_org_exolab.setProperty(Configuration.Property.Indent, "true");
				URL u = Executer.class.getResource("WorkSheetMapping.xml");
//				System.out.println(u);
				map.loadMapping(u);
			} catch (Exception ex) {
				throw new Error(ex);
			}
		}
	}

	public ExecuterDialog [] getFixedDialogs() {
		return fixDialogs;
	}


	private boolean changed;
	public void setChanged(boolean b) {
		if (b != changed) {
			changed = b;
		}
	}
}
