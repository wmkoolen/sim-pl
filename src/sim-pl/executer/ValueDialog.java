/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;
import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.BeanInfo;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import nbit.NumberFormat;
import util.DialogHelper;

import compiler.wisent.Instance_Simple;

import databinding.Input;
import executer.instanceeditor.JDataField;

public class ValueDialog extends JDialog {

	public ValueDialog() { // JBuilder default constructor
		this(null);
	}

	public ValueDialog(Frame owner) {
		super(owner, "Input value", true);
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private boolean okPressed;

	public Instance_Simple ask(Point p, String name, Instance_Simple v) {
		okPressed = false;
		bOK.setEnabled(false);
		jTextField1.setValue(v); // normally enables bOK
		jTextField1.selectAll();
		jLabel1.setText(name);
		pack();
		setLocation(p);
		setVisible(true);
		dispose();
		assert !okPressed || jTextField1.getState() == JDataField.OK;
		return okPressed ? jTextField1.getValue() : null;
	}


//	public static void main(String [] args) throws Exception {
//		ValueDialog vd = new ValueDialog();
//		System.out.println(vd.ask("SomePin", new Instance_Simple(new Type_Simple(5, false), new nBit(5, 6))));
//	}


	private void jbInit() throws Exception {
		DialogHelper.performOnEscapeKey(this, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); // escape == setVisible(false)
			}
		});

		getRootPane().setDefaultButton(bOK);

		bOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okPressed = true;
				setVisible(false);
			}
		});

		bCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});

		jTextField1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				bOK.setEnabled((jTextField1.getState() == JDataField.OK));
			}
		});

		jTextField1.setColumns(10);
		jPanel1.setLayout(gridBagLayout1);
		jLabel1.setText("jLabel1");
		lIcon.setOpaque(true);
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		bCancel.setText("Cancel");
		bOK.setText("OK");
		jToolBar1.add(bOK);
		jToolBar1.add(bCancel);
		this.getContentPane().add(jToolBar1, java.awt.BorderLayout.SOUTH);
		jPanel1.add(lIcon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
												  , GridBagConstraints.CENTER,
												  GridBagConstraints.VERTICAL,
												  new Insets(20, 20, 20, 20), 0,
												  0));
		jPanel1.add(jLabel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 10, 0));
		jPanel1.add(jTextField1, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 10), 0, 0));
		lIcon.setIcon(IconManager.chargeIcon(Input.class, BeanInfo.ICON_COLOR_32x32));
	}

	JPanel jPanel1 = new JPanel();
	JButton bCancel = new JButton();
	JButton bOK = new JButton();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JDataField jTextField1 = new JDataField();
	JLabel lIcon = new JLabel();
	JPanel jToolBar1 = new JPanel();

	public void setRepresentation(NumberFormat numberFormat) {
		jTextField1.setRepresentation(numberFormat);
	}
}
