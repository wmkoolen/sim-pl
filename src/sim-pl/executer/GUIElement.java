/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;


public abstract class GUIElement
{
	public static  void drawEncadredString(Graphics2D g, Point center, String s, Color background, Color cadre, Color text) {
		Rectangle   bound = g.getFontMetrics().getStringBounds(s, g).getBounds();
		int dy = g.getFontMetrics().getAscent()/2;
		int dx = -bound.width/2;

		g.setColor(background);
		g.fillRect(center.x + bound.x + dx -2, center.y + bound.y + dy-1 , bound.width + 3, bound.height+1);
		g.setColor(cadre);
		g.drawRect(center.x + bound.x + dx -2, center.y + bound.y + dy-1 , bound.width+3, bound.height+1);
		g.setColor(text);
		g.drawString(s, center.x + dx, center.y + dy);
	}

	public static Rectangle getEncadredStringBounds(Graphics2D g, Point center, String s)
	{
		Rectangle   bound = g.getFontMetrics().getStringBounds(s, g).getBounds();
		bound.x +=  center.x - bound.width/2 - 2;
		bound.y +=  center.y + g.getFontMetrics().getAscent()/2 - 1;
		bound.width += 3;
		bound.height += 1;
		return bound;
	}
}
