/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import namespace.ComponentTable;
import nbit.NumberFormat;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.DockingWindowAdapter;
import net.infonode.docking.View;
import util.CustomizableToolbar;
import util.CustomizableToolbarButton;
import util.ExceptionHandler;
import util.FileManager;
import util.ScreenShot;
import util.Terminator;
import util.VersionManager;
import util.WAction;
import util.truthtable.TruthTableView;
import about.AboutWindow;
import about.LogoPanel;
import executer.timetable.TimeTable;

public class Executer extends MMA_Application implements TimeTableListener
{
//	final EffectorFactoryMenuItem [] effectorfactories  = { new SimulatorFactory("New Simulation...", this) /*, new RemoteControllerFactory(this) */};
	final LogoPanel       logoPanel = new LogoPanel();


	WorkSheet worksheet;
	// worksheet file. can be null if worksheet has never been saved
	File      worksheetFile;



    final BorderLayout borderLayout1 = new BorderLayout();
    final JMenuBar MainMenuBar = new JMenuBar();
    final JMenu menuFile = new JMenu();
	final JMenu menuTools = new JMenu();

    final JMenu menuHelp = new JMenu();
    final JMenu menuNumberFormat = new JMenu();

	JRadioButtonMenuItem [] rbNF;

    final ButtonGroup buttonGroup_NF = new ButtonGroup();

    final JMenu menuWindow = new JMenu();
	final JMenu menuSimulate = new JMenu();
	final JMenu menuSettings = new JMenu();

    JMenuItem mCycleLength = new JMenuItem();


	final WAction aNew = new WAction(
		   "New",
		   true,
		   ExecuterIconManager.FILE_NEW_SMALL, ExecuterIconManager.FILE_NEW_LARGE,
		   "Create a new worksheet",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK),
		   KeyEvent.VK_N) {
		public void actionPerformed(ActionEvent e) {
			File_New();
		}
	};


	final WAction aOpen = new WAction(
		   "Open",
		   true,
		   ExecuterIconManager.FILE_OPEN_SMALL, ExecuterIconManager.FILE_OPEN_LARGE,
		   "Open an existing worksheet",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK),
		   KeyEvent.VK_O) {
		public void actionPerformed(ActionEvent e) {
			File_Open();
		}
	};


	final WAction aSave = new WAction(
		   "Save",
		   ExecuterIconManager.FILE_SAVE_SMALL, ExecuterIconManager.FILE_SAVE_LARGE,
		   "Save the current worksheet",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		   KeyEvent.VK_S) {
		public void actionPerformed(ActionEvent e) {
			File_Save();
		}
	};

	final WAction aSaveAs = new WAction(
		   "Save as ...",
		   ExecuterIconManager.FILE_SAVEAS_SMALL, ExecuterIconManager.FILE_SAVEAS_LARGE,
		   "Save the current worksheet in a different file",
		   null,
		   null,
		   KeyEvent.VK_V) {
		public void actionPerformed(ActionEvent e) {
			File_SaveAs();
		}
	};



	final WAction aClose = new WAction(
		   "Close",
		   ExecuterIconManager.FILE_CLOSE_SMALL, ExecuterIconManager.FILE_CLOSE_LARGE,
		   "Close the current worksheet, or exit",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK),
		   KeyEvent.VK_C) {
		public void actionPerformed(ActionEvent e) {
			File_Close();
		}
	};


	final WAction aExit = new WAction(
		   "Exit",
		   ExecuterIconManager.EXIT_SMALL, ExecuterIconManager.EXIT_LARGE,
		   "Quit the application",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK),
		   KeyEvent.VK_X) {
		public void actionPerformed(ActionEvent e) {
			File_Exit();
		}
	};

	final WAction aInit = new WAction(
		   "Init",
		   ExecuterIconManager.INIT_SMALL, ExecuterIconManager.INIT_LARGE,
		   "Restart the simulation",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_I) {
		public void actionPerformed(ActionEvent e) {
			Init(e);
		}
	};

	final WAction aInitProgram = new WAction(
		   "Init",
		   ExecuterIconManager.INIT_PROGRAM_SMALL, ExecuterIconManager.INIT_PROGRAM_LARGE,
		   "Restart the simulation and load the program",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_I) {
		public void actionPerformed(ActionEvent e) {
			Init(e);
		}
	};


	final WAction aStep = new WAction(
		   "Step",
		   ExecuterIconManager.STEP_SMALL, ExecuterIconManager.STEP_LARGE,
		   "Perform a single simulation step",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.CTRL_MASK),
		   KeyEvent.VK_T) {
		public void actionPerformed(ActionEvent e) {
			Step(e);
		}
	};

	final WAction aCycle = new WAction(
		   "Cycle",
		   ExecuterIconManager.CYCLE_SMALL, ExecuterIconManager.CYCLE_LARGE,
		   "Simulate an entire cycle",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_C) {
		public void actionPerformed(ActionEvent e) {
			Cycle(e);
		}
	};

	final WAction aCycles = new WAction(
		   "Cycles",
		   true,
		   ExecuterIconManager.CYCLES_SMALL, ExecuterIconManager.CYCLES_LARGE,
		   "Simulate a number of cycles",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_Y) {
		public void actionPerformed(ActionEvent e) {
			Cycles(e);
		}
	};




	final WAction aTruthTable = new WAction(
		   "Show Truth Table",
		   ExecuterIconManager.TRUTH_TABLE_SMALL, ExecuterIconManager.TRUTH_TABLE_LARGE,
		   "Generate the Truth Table of the current component",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_MASK),
		   KeyEvent.VK_S) {

		public void actionPerformed(ActionEvent e) {
			try {
				JFrame f = new TruthTableView(worksheet.tt.getEffector().getCompiledComponent());
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.pack();
				f.setLocationRelativeTo(getContentPane());
				f.setVisible(true);
			} catch (Exception ex) {
				ExceptionHandler.handleException(ex);
			}
		}
	};


    final CustomizableToolbar jToolBar1 = new CustomizableToolbar();

	final AbstractButton [] buttonsDisableable = {menuNumberFormat, mCycleLength};
	final AbstractAction [] actionsDisable = {aInit, aStep, aCycle, aCycles, aTruthTable, aSave, aSaveAs};

	public Executer() {
        jbInit();
		setWorkSheet(null, null);
	}

//	private void openFile(String loadFromStr)
//	{
//	    try
//	    {
//			File loadFrom = loadFromStr == null ? null : new File(loadFromStr);
//
//			if (loadFrom == null)   {
//				setWorkSheet(null, null);
//			}
//			else
//			{
////				ComponentTable.clear();
//				databinding.Component c = ComponentTable.load(loadFrom);
//				setWorkSheetFromComponent(new Simulator(c, loadFrom), null);
//			}
//	    }
//		catch (Exception ex)
//		{
//			setWorkSheet(null, null);
//			ExceptionHandler.handleException(ex);
//		}
//	}

	JMenuItem mInit;
	CustomizableToolbarButton bInit;

    private void jbInit()
    {
        this.getContentPane().setLayout(borderLayout1);
        menuFile.setMnemonic('F');
        menuFile.setText("File");
        menuSettings.setMnemonic('e');
        menuSettings.setText("Settings");
		menuTools.setMnemonic('T');
		menuTools.setText("Tools");
        menuHelp.setMnemonic('H');
        menuHelp.setText("Help");

		setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));

		menuFile.add(aNew);        jToolBar1.add(aNew);
		menuFile.add(aOpen);       jToolBar1.add(aOpen);
		menuFile.add(aSave);       jToolBar1.add(aSave);
		menuFile.add(aSaveAs);
		menuFile.add(aClose);      // WAction.deployToolbar(jToolBar1, aExport);
		menuFile.addSeparator();           jToolBar1.addSeparator();
		menuFile.add(aExit);

		mInit=menuSimulate.add(aInit);   bInit=jToolBar1.add(aInit);
		menuSimulate.add(aStep);   jToolBar1.add(aStep);
		menuSimulate.add(aCycle);  jToolBar1.add(aCycle);
		menuSimulate.add(aCycles); jToolBar1.add(aCycles);


		menuTools.add(aTruthTable);

		menuHelp.add(aAbout);
		menuHelp.add(aWebPage);

        menuNumberFormat.setText("Number Format");

        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setJMenuBar(MainMenuBar);
        this.setTitle(getProgramName());
        this.addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                File_Exit();
            }
        });
        menuWindow.setMnemonic('W');
        menuWindow.setText("Window");

		menuSimulate.setText("Simulate");
		menuSimulate.setMnemonic('S');

        mCycleLength.setText("Cycle Length...");
        mCycleLength.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CycleLength(e);
            }
        });
        this.getContentPane().add(jToolBar1, BorderLayout.NORTH);


		// Generated menu items -----------------
		// NumberFormat

		NumberFormat [] formats = NumberFormat.getAvailableFormats();
		rbNF = new JRadioButtonMenuItem [formats.length];

		for (int i = 0; i < formats.length; i++)
		{
			final NumberFormat format = formats[i];
			rbNF[i] = new JRadioButtonMenuItem(format.toString());
			menuNumberFormat.add(rbNF[i]);
			buttonGroup_NF.add(rbNF[i]);
			rbNF[i].addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
				    worksheet.setNumberFormat(format);
                }
            });
		}

		getContentPane().add(logoPanel, BorderLayout.CENTER);




//		// Window dialogs
//		for (int i = 0; i < dialogs.length; i++) menuWindow.add(dialogs[i].cb);

		// Layout menu(item)
//		menuWindow.addSeparator();
//		menuWindow.add(super.layout);

        MainMenuBar.add(menuFile);
		MainMenuBar.add(menuSimulate);
		MainMenuBar.add(menuTools);

        MainMenuBar.add(menuSettings);
        MainMenuBar.add(menuWindow);
        MainMenuBar.add(menuHelp);
//        menuHelp.add(menuAbout);
        menuSettings.add(menuNumberFormat);
		menuSimulate.add(mCycleLength);
		mCycleLength.setIcon(ExecuterIconManager.CYCLE_LENGTH_SMALL);
//        menuFile.add(menuNew);
//        menuFile.add(menuitem_export_graphics);
//        menuFile.add(file_close);
//        menuFile.addSeparator();
//        menuFile.add(file_exit);
//        jToolBar1.add(bInit, null);
//        jToolBar1.add(bStep, null);
//        jToolBar1.add(bCycle, null);
//        jToolBar1.add(bCycles, null);

		// what about this guy here?
		//worksheet.setNumberFormat(nbit.nBit.PREFIXFREE);

		jToolBar1.correctToolbarLook();

		// set up print screen action
		ScreenShot.installHook(getRootPane());

//		for (int i = 0; i < dialogs.length; i++) {
//			ScreenShot.installHook(dialogs[i].getRootPane());
//		}
    }



//	public void disconnectNOW()
//	{
//		this.effector = null; // No disconnect code called by setEffector (in next line)
//		this.setEffector(null);
//	}





//	public void fireTimeTableChanged(boolean reinited)
//	{
//		componentPanel.repaint();
//		ttd.timeTableChanged();
//		if (reinited)
//		{
//			id.reLoad();
//			tsd.getValues();
//		}
//		else
//		{
//		    id.repaint();
//		}
//		tsd.refresh();
//	}




   /**
	* Open a component, and create a fresh worksheet from it
	*/
   private final void File_New() {

	   File loadFrom = null;
	   FileManager fm = FileManager.getFileManager();
	   loadFrom = fm.askForFile(this, "Open Component File", loadFrom, "Open", FileManager.filter_anycmp, FileManager.filter_sim_pl, FileManager.filter_xml);
	   if (loadFrom == null) return;

	   try
	   {
		   if (tryCloseWithUserCancel()) {
			   assert worksheet == null;
			   ComponentTable.clear();
			   databinding.Component component = ComponentTable.load(loadFrom);
			   Effector newEffector =  new Simulator(component, loadFrom);

			   assert (newEffector != null);
			   setWorkSheetFromComponent(newEffector, null);
		   }
	   }
	   catch (Exception ex)
	   {
		   if (ex.getMessage().equals("The class for the root element 'WORKSHEET' could not be found.")) {
			   JOptionPane.showMessageDialog(Executer.this,
				   "I expected a component file, but you selected a worksheet file.\nYou can open a worksheet using File->Open.",
				   "Not a component file",
				   JOptionPane.ERROR_MESSAGE);
		   } else {
			   ExceptionHandler.handleException(ex);
		   }
	   }
   }


   /**
	* Re-open a previously saved worksheet
	*/
   private final void File_Open() {

		File loadFrom = null;
		FileManager fm = FileManager.getFileManager();
		loadFrom = fm.askForFile(this, "Open Worksheet", loadFrom, "Open", FileManager.filter_worksheet);
		if (loadFrom == null)   return;

		try {
			if (tryCloseWithUserCancel()) {
				assert worksheet == null;
				ComponentTable.clear();
				setWorkSheet(WorkSheet.POD.load(this, loadFrom), loadFrom);
			}
		} catch (Exception ex) {

			if (ex.getMessage().equals("The class for the root element 'SIMPLE' could not be found.") ||
				ex.getMessage().equals("The class for the root element 'COMPLEX' could not be found.")) {
				JOptionPane.showMessageDialog(Executer.this,
											  "I expected a worksheet file, but you selected a component file.\nYou can create a worksheet using File->New.",
					"Not a worksheet file",
					JOptionPane.ERROR_MESSAGE);
			} else {
				ExceptionHandler.handleException(ex);
			}
		}
	}

	/**
	 * @param f File
	 * @return boolean false if user cancelled
	 */
	private final boolean write(File f) {
		if (f == null) {
			FileManager fm = FileManager.getFileManager();
			do {
				f = new File(worksheet.tt.getEffector().getComponent().getName());
				f = fm.askForFile(this, "Save Worksheet As", f, "Save",
								  FileManager.filter_worksheet);
				if (f == null) return false;
			} while(f.exists() &&
					JOptionPane.showConfirmDialog(this, "File exists.\nOverwrite?",
												  "Overwrite question",
												  JOptionPane.YES_NO_OPTION,
												  JOptionPane.QUESTION_MESSAGE) !=
					JOptionPane.YES_OPTION);
		}
		assert f != null;

		try {
			worksheet.save(f);
			this.worksheetFile = f;
			refreshTitle();
			return true;
		} catch(Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Error saving " + f, JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	private final boolean File_Save() {
		return write(worksheetFile);
	}

	private final boolean File_SaveAs() {
		return write(null);
	}

	private final void File_Close() {
		if (worksheet == null) File_Exit();
		else                   tryCloseWithUserCancel();
    }


	private final void File_Exit() {
		if (tryCloseWithUserCancel()) System.exit(0);
    }



	public boolean tryCloseWithUserCancel() {
		if (worksheet == null) return true;

//      WE DO NOT ASK USER WETHER THE WORK NEEDS TO BE SAVED
//      THE RATIONALE HERE IS THAT IT ALMOST NEVER MUST BE
//		switch (JOptionPane.showConfirmDialog(this, "Save worksheet?", "Save on close", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE)) {
//			case JOptionPane.YES_OPTION:
//				if (!File_Save()) return false;
//				else break;
//			case JOptionPane.NO_OPTION:
//				// just carry on
//				break;
//			case JOptionPane.CANCEL_OPTION:
//				return false; // bail this function entirely
//			default:
//				throw new Error("What?");
//		}

		if (!worksheet.tryCloseWithUserCancel()) return false;
		setWorkSheet(null, null);
		assert worksheet == null;
		assert worksheetFile == null;
		return true;
	}


//	private final void newWorkSheet_Threaded(final File f) {
//		new Thread() {
//			public void run() {
//				try {
//					final ProgressMonitor m = new ProgressMonitor(Executer.this, "Loading " + f, "", 0, 4);
//
//					m.setNote("Load file");
//					Component component = ComponentTable.load(f);
//					m.setProgress(1);
//					if (m.isCanceled()) return;
//
//					m.setNote("Create simulator");
//					Simulator effector = new Simulator(component, f);
//					m.setProgress(2);
//					if (m.isCanceled()) return;
//
//					m.setNote("Create worksheet");
//					final WorkSheet w = new WorkSheet(Executer.this, effector);
//					m.setProgress(3);
//					if (m.isCanceled()) return;
//
//					SwingUtilities.invokeAndWait(new Runnable() {
//						public void run() {
//							m.setNote("Show worksheet");
//							setWorkSheet(w, null);
//							m.setProgress(4);
//						}
//					});
//					m.close();
//				} catch (Exception ex) {
//					ex.printStackTrace();
//					JOptionPane.showMessageDialog(Executer.this, ex.getMessage(), "Error loading file", JOptionPane.ERROR_MESSAGE);
//				}
//			}
//		}.start();
//	}


	private final void loadThreaded(final File loadFrom) {
		final ProgressMonitor m = new ProgressMonitor(Executer.this, "Loading " + loadFrom, "", 0, 3);
		m.setMillisToDecideToPopup(0);
		m.setMillisToPopup(0);

		new Thread() {
			public void run() {
				try {
					m.setNote("Initialise");
					// nothing really to do, but we need to set progress to make monitor appear
					m.setProgress(1);

					m.setNote("Load worksheet");
					final WorkSheet w = WorkSheet.POD.load(Executer.this, loadFrom);
					m.setProgress(2);
					if (m.isCanceled()) return;

					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							m.setNote("Show worksheet");
							setWorkSheet(w, loadFrom);
							m.setProgress(3);
						}
					});
					// m automatically closed here, as progress == maxprogress.
				} catch (Exception ex) {
					ex.printStackTrace();
					m.close();
					JOptionPane.showMessageDialog(Executer.this, ex.getMessage(), "Error loading file", JOptionPane.ERROR_MESSAGE);
				}
			}
		}.start();
	}



    public static void main(final String[] args) {
		if (args.length >= 2) {
			System.err.println("Usage: " + Executer.class.getName() + " [filename]");
			return;
		}
	        Thread.setDefaultUncaughtExceptionHandler(new Terminator());
		final Executer e = new Executer();

		e.pack(); // need a realised frame for successful maximisation

		e.setSize(1000,800);
		e.setLocationRelativeTo(e.getOwner());

		// nice if maximisation can be done
		if (e.getToolkit().isFrameStateSupported(JFrame.MAXIMIZED_BOTH)) {
			e.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}

		if (args.length == 1) {
			e.loadThreaded(new File(args[0]).getAbsoluteFile());
		}
		e.setVisible(true);
    }



	public void setWorkSheetFromComponent(Effector effector, File workSheetFile) {
		try {
			setWorkSheet(new WorkSheet(this, effector), workSheetFile);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Could not create worksheet", JOptionPane.ERROR_MESSAGE);
		}
	}


	private final void refreshTitle() {
		String title = getProgramName() + " - " + VersionManager.currentVersion;

		if (worksheet != null) {
			title += " (" + (worksheetFile == null ? "Untitled" : worksheetFile.getName()) + ")";
		}

		super.setTitle(title);
	}


    public void setWorkSheet(WorkSheet ws, File wsFile)    {
		final Container cpane = getContentPane();

		if (worksheet != null) {

			// Try to close old Effector. If that fails (networkd dead or something like that) just continue.
			Effector oldEffector = worksheet.tt.getEffector();
			try {
				if (oldEffector != null)
					oldEffector.onClose();
			} catch (Exception ex) {
				ExceptionHandler.handleException(ex);
			}
			assert cpane.isAncestorOf(worksheet.rootWindow);
			cpane.remove(worksheet.rootWindow);
			worksheet.tt.removeTimeTableListener(this);

			menuWindow.removeAll();

		} else {
			assert cpane.isAncestorOf(logoPanel);
			cpane.remove(logoPanel);
		}

		assert menuWindow.getComponentCount() == 0;

		// here cpane's center slot is unoccupied

		this.worksheet = ws;
		this.worksheetFile = wsFile;

		final boolean enabled = ws != null;

		if (enabled) {
			databinding.Component c = ws.tt.getEffector().getComponent();
			NumberFormat preferred = c.getPreferredNumberFormat();
			worksheet.setNumberFormat(preferred == null ? nbit.NumberFormat.HEXADECIMAL : preferred);
			cpane.add(worksheet.rootWindow, BorderLayout.CENTER);
			worksheet.tt.addTimeTableListener(this);

			for (final ExecuterDialog ed : worksheet.getFixedDialogs()) {
				String menuItemText = ed.menuCheckBoxName;
				final WindowAction a = new WindowAction(menuItemText, ed.v.getIcon(), ed.v);
				ed.v.addListener(new DockingWindowAdapter() {
					public void windowRestored(DockingWindow w) {
						assert w == ed.v;
						a.setEnabled(false);
					}

					public void windowClosed(DockingWindow w) {
						assert w == ed.v;
						a.setEnabled(true);
					}
				});
				a.setEnabled(!ed.v.isShowing());
				menuWindow.add(new JMenuItem(a));
			}
		} else {
			cpane.add(logoPanel, BorderLayout.CENTER);
		}

		refreshTitle();
		cpane.validate();
		cpane.repaint();

		for (int i = 0; i < buttonsDisableable.length; i++)   buttonsDisableable[i].setEnabled(enabled);
		for (int i = 0; i < actionsDisable    .length; i++)   actionsDisable    [i].setEnabled(enabled);
    }

    public void cycleLengthChanged(TimeTable e)         {    }
    public void timeTableCleared(TimeTable e)           {    }
    public void eventSequenceCompleted(TimeTable e)     {    }
    public void clockFlank(TimeTable tt, boolean rising){    }
    public void valuesChanged(TimeTable e)              {    }

	public void initialisationProgramChanged(TimeTable tt) {
		Action initAction = tt.getInitialisationProgram() != null ? aInitProgram : aInit;
		mInit.setAction(initAction);
		jToolBar1.setAction(bInit, initAction);
	}


	void Init(ActionEvent e) {
		try {
	       worksheet.tt.init();
        }
		catch (Exception ex) { throw new ExceptionHandler(ex); }
    }

    void Step(ActionEvent e) {
		try { worksheet.tt.step(); }
		catch (Exception ex) { throw new ExceptionHandler(ex); }
    }

    void Cycle(ActionEvent e) {
		try { worksheet.tt.cycle(); }
		catch (Exception ex) { throw new ExceptionHandler(ex); }
    }

    void Cycles(ActionEvent e) {
		java.awt.Component parent = (java.awt.Component)e.getSource();
		String answer = JOptionPane.showInputDialog(parent, "How many cycles", "");
		if (answer == null) return; // user cancelled
		try {
			int cycles = Integer.parseInt(answer);
			if (cycles < 0) throw new NumberFormatException();
			worksheet.tt.cycle(cycles);
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(parent, "Invalid cycle count: " + answer, "Error", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception ex) { throw new ExceptionHandler(ex); }
    }

    void CycleLength(ActionEvent e) {
		String answer = JOptionPane.showInputDialog(this, "New cycle length", Integer.toString(worksheet.tt.getCycleLength()));
		if (answer == null) return; // user cancelled
		try {
			int cycleLength = Integer.parseInt(answer);
			if (cycleLength < 0) throw new NumberFormatException();
			worksheet.tt.setCycleLength(cycleLength);
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "Invalid cycle length: " + answer, "Error", JOptionPane.ERROR_MESSAGE);
		}
    }

	public final String getProgramName() { return "Executer"; }


	private static class WindowAction extends AbstractAction {
		public final View v;
		public WindowAction(String text, Icon icon, View v) {
			super(text, icon);
			this.v = v;
		}

		public void actionPerformed(ActionEvent ae) {
			v.restore();
		}
	}
}


