/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.timesequencediagram;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.InputVerifier;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nbit.nBit;
import util.ExceptionHandler;
import util.ImageSource;
import util.WAction;
import util.graphics.GraphicsExporter;
import databinding.HPL;
import databinding.InvalidHPLException;
import databinding.Pin;
import databinding.ResolutionException;
import databinding.wires.Wire;
import executer.ComponentPanel.LocalWireInstance;
import executer.Effector;
import executer.ExecuterDialog;
import executer.ExecuterIconManager;
import executer.Instance;
import executer.Instance_Pin;
import executer.Simulator.Instance_Complex;
import executer.Simulator.Instance_Component;
import executer.TimeTableListener;
import executer.ValuedObject;
import executer.WorkSheet;
import executer.timetable.TimeTable;


interface ValueIterator
{
	public  boolean hasNext();
	int     getNextMoment();
	int     getNextHeight();
}



class Duo
{
	ValuedObject            ip;
	NameLabel               nl;
	TimeDiagram             td;
	TimeTable               tt;
	TimeSequenceDiagram     tsd;

	/**
	 * Time2value ONLY records CHANGES
	 * This means in particular that tt.currentTime >= time2value.lastKey()
	 * We have to accomodate for this in drawing
	 */
	SortedMap<Integer,nBit>       time2value = new TreeMap<Integer,nBit>();

	public Duo(ValuedObject ip, TimeTable tt, TimeSequenceDiagram tsd, boolean viewFullName)
	{
		this.ip = ip;
		this.tt = tt;
		this.tsd = tsd;
		nl = new NameLabel(this, viewFullName);
		td = new TimeDiagram(this);
		time2value.put(new Integer(tt.getCurrentTime()), new nBit(scanValue()));
	}

	public void empty()
	{
		time2value.clear();
		td.repaint();
	}

	public nBit scanValue() {
		return ip.getValue().getValue();
	}

	public void getValue()
	{
		nBit sv = scanValue();

		// only add something on a REAL change
		if (time2value.size() == 0 || !time2value.get(time2value.lastKey()).equals(sv))
		{
			time2value.put(new Integer(tt.getCurrentTime()), new nBit(sv));
		}
	}
}


class NameLabel extends JLabel
{
	Duo duo;

	static final int MINIMUMHEIGHT = 20;

	public NameLabel(Duo duo, boolean viewFullName)
	{
		this.duo = duo;
		super.setForeground(Color.black);
		super.setOpaque(false);
		this.viewFullName(viewFullName);
	}

	public void viewFullName(boolean viewFullName) {
		setText(" " + (viewFullName ? duo.ip.getFullName() : duo.ip.getLocalName()) + " ");
	}

	public Dimension getPreferredSize()
	{
		Dimension d = super.getPreferredSize();
		d.height = Math.max(d.height, MINIMUMHEIGHT);
		return d;
	}

	public Dimension getMinimumSize()
	{
		Dimension d = super.getMinimumSize();
		d.height = Math.max(d.height, MINIMUMHEIGHT);
		return d;
	}

	public Dimension getMaximumSize()
	{
		Dimension d = super.getMaximumSize();
		d.height = Math.max(d.height, MINIMUMHEIGHT);
		return d;
	}
}

class TimeDiagram extends JPanel implements MouseMotionListener
{
	public static final int MARGIN = 20;
	Duo duo;

	public TimeDiagram(Duo duo)
	{
		this.duo = duo;
		super.setOpaque(false);
		this.addMouseMotionListener(this);
	}


	public void mouseMoved(MouseEvent e)
	{
		Integer time =      new Integer((int)(e.getPoint().x / duo.tsd.zoomFactor));
		nBit    value =     null;


		if (time.intValue() <= duo.tsd.listener.tt.getCurrentTime())
		{
			SortedMap<Integer,nBit> m = duo.time2value.tailMap(time);


			if (m.size() > 0)
			{
				Integer largerorequal = m.firstKey();
				if (largerorequal.intValue() == time.intValue())
				{
					value = m.get(largerorequal);
				}
			}

			if (value == null)
			{
				SortedMap<Integer,nBit>  b = duo.time2value.headMap(time);
				if (b.size() > 0)
			    {
					value = b.get(b.lastKey());
			    }
			}
		}

		try
		{
			String tooltip = duo.nl.getText() + " (" + time.intValue() + ", ";
			if (value != null) tooltip +=  value.toString(duo.tsd.listener.getNumberFormat(), duo.ip.getValue().type.signed);
			else               tooltip += "NO VALUE AVAILABLE";
			tooltip += ")";
			setToolTipText(tooltip);
		}
		catch (Exception ex) { throw new ExceptionHandler(ex); }
	}

	public void mouseDragged(MouseEvent e) {}


	public final static int BEFORE =    0;
	public final static int IN =        1;
	public final static int AFTER =     2;
	public final static int DONE =      3;



	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		try
   		{
			g.setColor(Color.black);

			final Rectangle clip =    g.getClipBounds();

			final Dimension d =       getSize();
			final int useableHeight = (d.height*1)/2;
			final int offset =        (d.height*3)/4;
			final nBit height =       new nBit(nBit.bitsUsed(useableHeight), useableHeight);


			ValueIterator i = new ValueIterator()
			{
				final Integer fromX =       new Integer((int)(clip.x / duo.tsd.zoomFactor));
				final Integer toX   =       new Integer((int)((clip.x + clip.width + duo.tsd.zoomFactor - 1) / duo.tsd.zoomFactor)); // omhoog afronden !!!

				final SortedMap<Integer,nBit> b =    duo.time2value.headMap(fromX);
				final SortedMap<Integer,nBit> m =    duo.time2value.subMap(fromX, toX);
				final Iterator<Integer>  i =         m.keySet().iterator();
				      int       state =     BEFORE;
				      int       currentMoment;
				      int       currentHeight;

				public int calculateHeight(nBit curValue)
				{
					// Divide all the information as equal as possible over the available height
					// This is useless if there are more than log(height) bits.
					// But it is fair and general
					try    {return offset-curValue.cast(curValue.bits + height.bits).MUL(height).DIV(curValue.allOnes()).toInt();}
					catch (Exception ex)    { throw new ExceptionHandler(ex); }
				}

				public boolean hasNext()
				{
					switch (state)
					{
						case BEFORE:
							state = IN;
					        if (fromX.intValue() < duo.tsd.listener.tt.getCurrentTime() && b.size() > 0) // Er zijn waarden beschikbaar voor de eerste uit m.
					        {
						        currentMoment =         fromX.intValue();
							    currentHeight =         calculateHeight(b.get(b.lastKey()));
						        return true;
					        }

						case IN:
							if (i.hasNext())
							{
								Integer key =       i.next();
								currentMoment =     key.intValue();
								currentHeight =     calculateHeight(m.get(key));
								return true;
							}
							state = AFTER;

						case AFTER:
							state = DONE;
							currentMoment = Math.min(toX.intValue(), duo.tsd.listener.tt.getCurrentTime()); // Of tot het einde van de tijd, of tot het einde van het venster
							return true;
					}
				    return false;
				}

				public int getNextHeight()
				{
					return currentHeight;
				}
				public int getNextMoment()
				{
					return currentMoment;
				}
			};

			if (!i.hasNext()) return;

			int curMoment = i.getNextMoment();
			int curHeight = i.getNextHeight();

			while (i.hasNext())
			{
				int prevMoment = curMoment;
				int prevHeight = curHeight;

				curMoment = i.getNextMoment();
				curHeight = i.getNextHeight();

				g.drawLine((int)(prevMoment * duo.tsd.zoomFactor),  prevHeight, (int)(curMoment * duo.tsd.zoomFactor), prevHeight);
				g.drawLine((int)(curMoment  * duo.tsd.zoomFactor),  prevHeight, (int)(curMoment * duo.tsd.zoomFactor), curHeight);
			}
		}
	    catch (Exception ex)
	    {
		    throw new ExceptionHandler(ex);
	    }
	}

//	public void setCurrentTime(int currentTime)
//	{
//		this.currentTime = currentTime;
//	}

//	public Dimension getMinimumSize()
//	{
//		return new Dimension((int)(duo.tsd.listener.tt.getCurrentTime() * duo.tsd.zoomFactor + MARGIN), duo.nl.getMinimumSize().height);
//	}
//
//	public Dimension getPreferredSize()
//	{
//		Dimension d = super.getPreferredSize();
//		d.width = Math.max((int)(duo.tsd.listener.tt.getCurrentTime() * duo.tsd.zoomFactor + MARGIN), d.width);
//		d.height = duo.nl.getMaximumSize().height;
//		return d;
//	}
//
//	public Dimension getMaximumSize()
//	{
//		Dimension d = super.getMaximumSize();
//		d.height = duo.nl.getMaximumSize().height;
//		return d;
//	}
}



public class TimeSequenceDiagram extends ExecuterDialog implements ImageSource, TimeTableListener
{
	private static final String ID = "TimeSequenceDiagram";


	final HashMap<ValuedObject, Duo>     pin2duo =               new HashMap<ValuedObject, Duo>();
	boolean     fullNames =             true;
	double      zoomFactor =            1.0;
	boolean     viewCycleStarts =       true;

	final SortedMap<Integer, Boolean> time2clock = new TreeMap<Integer, Boolean>();

    final BorderLayout borderLayout1 =        new BorderLayout();
    final JScrollPane jScrollPane1 =          new JScrollPane();
    Box         boxLeft;
    ClockPanel  pClock;

    final JPanel motherPanel = new JPanel();
    final BorderLayout borderLayout4 = new BorderLayout();
    final JMenuBar jMenuBar1 = new JMenuBar();
    final JMenu jMenu1 = new JMenu();
    final JMenu jMenu2 = new JMenu();

    final JCheckBoxMenuItem cbmenuItem_viewFullNames = new JCheckBoxMenuItem();
    final JCheckBoxMenuItem cbCycleStarts = new JCheckBoxMenuItem();

	public static final Image icon = IconManager.loadImage(TimeSequenceDiagram.class, "TSD.png");


	WAction aExport = new WAction(
		   "Export", //"Export Image",
		   true,
		   ExecuterIconManager.FILE_EXPORT_SMALL, ExecuterIconManager.FILE_EXPORT_LARGE,
		   "Export an image of the current time sequence diagram",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		   KeyEvent.VK_E) {
		public void actionPerformed(ActionEvent e) {
			File_ExportGraphics(e);
		}
	};


	public String getID() { return "TimeSequenceDiagram"; }


	public class ClockPanel extends JPanel
	{
		final Color clockRising =   Color.lightGray;
		final Color clockFalling =  Color.yellow;

		public ClockPanel()
		{
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			setOpaque(false);
			setEnabled(false);
		}


		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);

			if (viewCycleStarts)
			{
				Rectangle clip = g.getClipBounds();

				SortedMap<Integer,Boolean> m = time2clock.subMap(new Integer((int)(clip.x / zoomFactor)), new Integer((int)((clip.x + clip.width) / zoomFactor)));

				g.setColor(clockRising);

				for (Map.Entry<Integer,Boolean> e : m.entrySet()) {
					Integer time = e.getKey();
					Boolean clock = e.getValue();
		    		int timeX = (int)(time.intValue() * zoomFactor);
					if (clock.booleanValue()) g.drawLine(timeX, clip.y, timeX, clip.y + clip.height);
	    		}
		    }
		}
	}



	public static final String zoomPercentages [] = {"800%", "400%", "200%", "100%", "50%", "25%", "12.5%"};
//    JMenuItem mExportGraphics = new JMenuItem();
    JComboBox jComboBox1 = new JComboBox(zoomPercentages);




	private TimeSequenceDiagram(WorkSheet listener, boolean overloadDummy) {
		super(listener, "Time Sequence Diagram");

		jbInit();

		removeAllPins();
		time2clock.clear();

		// we want to make a single cycle 50 pixels
		// normally (at 100%) it is preferredCycleTime pixels
		// so we accomodate the zoom factor for that:
		jComboBox1.setSelectedItem(Double.toString(50 * 100.0 /
			listener.tt.getEffector().getComponent().getPreferredCycleLength()));
	}


	public TimeSequenceDiagram(WorkSheet listener, POD p) throws Exception {
		this(listener, true);
		setZoom(p.zoomFactor);
		setViewCycleStarts(p.viewCycleStarts);
		setViewFullNames(p.fullNames);

		if (p.probes != null) for (ValuedObjectPOD vop : p.probes) {
			try {
				addPin(vop.getValuedObject(listener.tt.getEffector()));
			}
			catch (ResolutionException ex) {
				System.err.println("Object not found: " + vop.instance + ":" + vop.name);
			}
			catch (InvalidHPLException ex) {
				System.err.println("Object not found: " + vop.instance + ":" + vop.name);
			}
		}
		// extract pins from POD

		refresh();
	}


	public TimeSequenceDiagram(WorkSheet listener) {
		this(listener, true);

		Instance instance = listener.tt.getEffector().getInstance();
		databinding.Component component = listener.tt.getEffector().getComponent();

		for (Pin p : component.getIo().getPins()) {
			this.addPin(new Instance_Pin(instance, p));
		}
		refresh();
	}


	public void addClock(int time, boolean isRising)
	{
		time2clock.put(new Integer(time), new Boolean(isRising));
	}

	public void setZoom(double zoomFactor)
	{
		if (this.zoomFactor == zoomFactor) return;
		this.zoomFactor = zoomFactor;
		jComboBox1.setSelectedItem(Double.toString(100*zoomFactor));
		refresh();
	}

	public boolean contains(ValuedObject ip)
	{
		return pin2duo.containsKey(ip);
	}


	public void addPin(ValuedObject ip)
	{
		if (!pin2duo.containsKey(ip))
		{
			Duo d = new Duo(ip, listener.tt, this, fullNames);
			d.td.setAlignmentX(LEFT_ALIGNMENT);
			pin2duo.put(ip,  d);
			boxLeft.add(d.nl);
			pClock.add(d.td);
		}
	}

	public void removePin(ValuedObject ip)
	{
		Duo d = pin2duo.remove(ip);
		if (d != null)
		{
			boxLeft.    remove(d.nl);
			pClock.remove(d.td);
		}
	}

	public void refresh()
	{
		int width = (int)(listener.tt.getCurrentTime() * zoomFactor + TimeDiagram.MARGIN);
		for (Duo d : pin2duo.values()) {
			Dimension dim = new Dimension(width, d.nl.getMaximumSize().height);
			d.td.setPreferredSize(dim);
			d.td.setMaximumSize(dim);
		}
		pClock.repaint();
		pClock.invalidate();
		jScrollPane1.revalidate();
		jScrollPane1.repaint();
	}

	public void getValues()
	{
		for (Duo d: pin2duo.values()) {
			d.getValue();
		}
	}

	public void removeAllPins()
	{
		for (Iterator<ValuedObject> i = pin2duo.keySet().iterator(); i.hasNext(); )
		{
			Duo d = pin2duo.get(i.next());
			i.remove();
			boxLeft.    remove(d.nl);
			pClock.remove(d.td);
		}
		refresh();
	}

	public void clearStoredSignals()
	{
		for (Iterator<ValuedObject> i = pin2duo.keySet().iterator(); i.hasNext(); )
		{
			pin2duo.get(i.next()).empty();
		}
		time2clock.clear();
		refresh();
	}



	public void representationChanged()
	{
	}

    private void jbInit()
    {
		this.setJMenuBar(jMenuBar1);
        boxLeft =   Box.createVerticalBox();
		pClock =    new ClockPanel();

		setTitle("Time Sequence Diagram");
        this.getContentPane().setLayout(borderLayout1);
        motherPanel.setBackground(Color.white);
        motherPanel.setLayout(borderLayout4);
        jMenu1.setText("File");
        jMenu2.setText("Settings");
        cbmenuItem_viewFullNames.setText("View Full Names");
        cbmenuItem_viewFullNames.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                setViewFullNames(cbmenuItem_viewFullNames.isSelected());
            }
        });

        cbCycleStarts.setText("View Cycle Starts");
        cbCycleStarts.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                setViewCycleStarts(cbCycleStarts.isSelected());
            }
        });
//        mExportGraphics.setMnemonic('E');
//        mExportGraphics.setText("Export Graphics...");
//        mExportGraphics.addActionListener(new java.awt.event.ActionListener()
//        {
//            public void actionPerformed(ActionEvent e)
//            {
//                File_ExportGraphics(e);
//            }
//        });
        jComboBox1.setEditable(true);
		jComboBox1.setPrototypeDisplayValue("1000.00%");
        jComboBox1.setMaximumSize(jComboBox1.getPreferredSize());
		jComboBox1.setSelectedIndex(3);
        this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);

        jScrollPane1.getViewport().add(motherPanel, null);
        motherPanel.add(boxLeft, BorderLayout.WEST);
        motherPanel.add(pClock, BorderLayout.CENTER);
        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);
		jMenu1.add(aExport);
//		WAction.deployMenu(jMenu1, aExport);
        jMenu2.add(cbmenuItem_viewFullNames);
        jMenu2.add(cbCycleStarts);

		jMenuBar1.add(Box.createHorizontalStrut(10));
		jMenuBar1.add(jComboBox1);

		((JTextField)(jComboBox1.getEditor().getEditorComponent())).setInputVerifier(new HappyPercentage());

		jComboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
				try {
				    double d = HappyPercentage.parsePercentage(jComboBox1.getSelectedItem().toString());
					setZoom(d);
				} catch (NumberFormatException ex) {}
            }
		});

		cbmenuItem_viewFullNames.setState(true);
		cbmenuItem_viewFullNames.doClick();
		cbCycleStarts.doClick();

		setIconImage(icon);
    }

	public Rectangle getBounds(Graphics2D g2) {
		return new Rectangle(motherPanel.getPreferredSize());
	}

 	public void drawImage(Graphics2D g2)
	{
		Rectangle bb = getBounds(g2);
		g2.setClip(bb);
		motherPanel.printAll(g2); // NOTE: print, not paint
	}



    void setViewFullNames(boolean b) {
		if (fullNames == b) return;
		fullNames = b;
		cbmenuItem_viewFullNames.setState(b);
		for (Duo d : pin2duo.values()) {
			d.nl.viewFullName(fullNames);
		}
		refresh();
    }

    void setViewCycleStarts(boolean b) {
		if (viewCycleStarts == b) return;
		viewCycleStarts = b;
		cbCycleStarts.setState(b);
		refresh();
    }

    void File_ExportGraphics(ActionEvent e)
    {
		GraphicsExporter.doExport(this, this);
    }


	public void cycleLengthChanged(TimeTable tt)    {    }
    public void timeTableCleared(TimeTable tt)    {
		this.clearStoredSignals();
		getValues();
    }

    public void clockFlank(TimeTable tt, boolean rising)    {
		this.addClock(tt.getCurrentTime(), rising);
    }

    public void eventSequenceCompleted(TimeTable tt) {
		refresh();
    }

    public void valuesChanged(TimeTable tt)    {
		getValues();
    }

	public void initialisationProgramChanged(TimeTable tt) {
	}


	public POD getPOD() {
		ArrayList<ValuedObjectPOD> probes = new ArrayList<ValuedObjectPOD>();
		for (java.awt.Component c : boxLeft.getComponents()) {
			ValuedObject vo = ((NameLabel)c).duo.ip;

			if (vo instanceof LocalWireInstance) {
				LocalWireInstance lwi = (LocalWireInstance)vo;
				probes.add(new Wire_POD(lwi.getParent().getFullName(), lwi.getLocalName()));

			} else if (vo instanceof Instance_Pin) {
				Instance_Pin ip = (Instance_Pin)vo;
				probes.add(new Pin_POD(ip.instance.getFullName(), ip.getLocalName()));

			} else throw new Error("Unexpected ValuedObject: " + vo);
		}
		return new POD(fullNames, zoomFactor, viewCycleStarts, probes);
	}


	public static abstract class ValuedObjectPOD {
		public abstract ValuedObject getValuedObject(Effector e) throws Exception;
		public String instance;
		public String name;

		public ValuedObjectPOD(){}
		public ValuedObjectPOD(String instance, String name) {
			this.instance = instance;
			this.name = name;
		}

	}

	public static class Pin_POD extends ValuedObjectPOD {
		public Pin_POD() {}
		public Pin_POD(String instance, String name) {
			super(instance, name);
		}

		public ValuedObject getValuedObject(Effector e) throws InvalidHPLException, ResolutionException {
			Instance_Component<?,?> ic = (Instance_Component<?,?>)e.getInstance().resolve(new HPL(instance));
			Pin pin = ic.getComponent().getIo().getByName(name);
			if (pin == null) throw new ResolutionException(ic, name);
			return new Instance_Pin(ic, pin);
		}
	}

	public static class Wire_POD extends ValuedObjectPOD {
		public Wire_POD() {}
		public Wire_POD(String instance, String name) {
			super(instance, name);
			this.name = name;
		}

		public ValuedObject getValuedObject(Effector e) throws Exception {
			Instance_Complex ic = (Instance_Complex)e.getInstance().resolve(new HPL(instance));
			Wire w = ic.getComponent().getWires().getByName(name);
			return new LocalWireInstance(w, ic.getWireInstance(name), ic);
		}
	}


	public static class POD implements KidPOD {
		public boolean     fullNames;
		public double      zoomFactor;
		public boolean     viewCycleStarts;

		public ArrayList<ValuedObjectPOD> probes = new ArrayList<ValuedObjectPOD>();

		public POD(){} // from XML default construction

		public POD(boolean fullNames, double zoomFactor, boolean viewCycleStarts, ArrayList<ValuedObjectPOD> probes) {
			this.fullNames = fullNames;
			this.zoomFactor = zoomFactor;
			this.viewCycleStarts = viewCycleStarts;
			this.probes = probes;
		}

		public String getID() { return ID; }

		public TimeSequenceDiagram createDialog(WorkSheet w) {
			throw new Error();
		}
	}


}


class HappyPercentage extends InputVerifier {

	static double parsePercentage(String text) throws NumberFormatException {
		text.trim();
		if (text.endsWith("%")) text = text.substring(0, text.length()-1);
		double d = Double.parseDouble(text)/100.0;
		if (d < 0) throw new NumberFormatException(d + " not a valid percentage");
		return d;
	}

	public boolean verify (JComponent input) {
		JTextField tf = (JTextField)input;
		try {
			parsePercentage(tf.getText());
			return true;
		}
		catch (NumberFormatException ex) { return false; }
	}
}
