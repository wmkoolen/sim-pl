/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.timetable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import util.ExceptionHandler;

import compiler.ComponentProgram;
import compiler.wisent.Instance_Simple;

import executer.Effector;
import executer.Instance;
import executer.Instance_Pin;
import executer.Instance_Wire;
import executer.NamespaceUpdate;
import executer.TimeTableListener;

/** The body of the simulator.
 * @author Wouter Koolen-Wijkstra
 * @version 1.0
 */

public class TimeTable
{
	private final PriorityQueue<ContentChangeEvent> pq;
	private int         currentTime =               0;
	private int         cycleLength =               10;
	private int         nextClockRisingTime =       0;
	private int         nextClockFallingTime =      -10;
	private boolean     nextClockIsRising =         true;
	private Map<ContentChangeEvent,NamespaceUpdate>     cce2namespaceUpdate =       new HashMap<ContentChangeEvent,NamespaceUpdate>();
	private final Effector    effector;
	private ComponentProgram initialiser = null;

	private AliasResolver aliasResolver = null;
	private HighlightManager highlightManager = null;


	/**
	 * Creates a new time table.
	 */

    public TimeTable(Effector effector)	throws Exception {
		this.pq = new PriorityQueue<ContentChangeEvent>(11, TimeOrdening.order);
		this.effector = effector;

		this.cycleLength = effector.getComponent().getPreferredCycleLength();
		this.setInitialisationProgram(null);
		this.init();
	}

	/**
	 * Copies a time table. This is important to read it, for through the nature of a priority queue, it can only be read by destroying it.
	 * @param T The time table to copy
	 */

	private TimeTable(TimeTable T)
	{
		this.pq = new PriorityQueue<ContentChangeEvent>(T.pq);
		this.currentTime =          T.currentTime;
	    this.cycleLength =          T.cycleLength;
	    this.nextClockRisingTime =  T.nextClockRisingTime;
	    this.nextClockFallingTime = T.nextClockFallingTime;
	    this.nextClockIsRising =    T.nextClockIsRising;
		this.effector =             T.effector;
		this.cce2namespaceUpdate =  new HashMap<ContentChangeEvent,NamespaceUpdate>(T.cce2namespaceUpdate);
	}

	/**
	 * Provides a (shallow) copy of this time table.
	 * @return a copy
	 */

	public TimeTable clone () {
		return new TimeTable(this);
	}


	public Effector getEffector() {
		return effector;
	}


	public void setInitialisationProgram(ComponentProgram initialiser) {
		this.initialiser = initialiser;
		fireInitialisationProgramChanged();
	}


	public ComponentProgram getInitialisationProgram() {
		return initialiser;
	}

	public  AliasResolver getAliasResolver() {
		return aliasResolver;
	}

	public HighlightManager getHighlightManager() {
		return highlightManager;
	}

	/**
	 * Erases the entire content of this time table.
	 */

	public void init() throws Exception
	{
		pq.clear();
		this.currentTime =          0;
	    this.nextClockRisingTime =  0;
	    this.nextClockFallingTime = -10;
	    this.nextClockIsRising =    true;
		cce2namespaceUpdate.clear();
		aliasResolver = null;
		highlightManager = null;

		effector.clear();
		if (initialiser != null) {
			initialiser.execute(this);
			aliasResolver = initialiser.getAliasResolver();
			highlightManager = initialiser.getHighlightManager();
		}
		effector.onInit(this);
		fireTimeTableCleared();
		fireEventSequenceCompleted();
	}

	protected void insert(NamespaceUpdate update)
	{
		cce2namespaceUpdate.put(update.cce, update);
		pq.add(update.cce);
	}

	public int getCurrentTime()
	{
		return currentTime;
	}

	public int getNextClockTime()
	{
		return nextClockIsRising ? nextClockRisingTime : nextClockFallingTime;
	}

	public boolean isNextClockRising()
	{
		return nextClockIsRising;
	}

	public int getCycleLength()
	{
		return cycleLength;
	}

	public void setCycleLength(int cycleLength)
	{
		this.cycleLength = cycleLength;
		this.fireCycleLengthChanged();
	}

	public NamespaceUpdate getNamespaceUpdate(ContentChangeEvent cce)
	{
		cce.time += currentTime;
		NamespaceUpdate update = cce2namespaceUpdate.get(cce);
		if (update == null)
		{
			update = new NamespaceUpdate(cce);
	    	insert(update);
		}
		return update;
	}

	public NamespaceUpdate getMin()
	{
		ContentChangeEvent cce =    pq.peek();
		assert cce != null;
		NamespaceUpdate n =         cce2namespaceUpdate.get(cce);
		assert n != null;
		return n;
	}

	public NamespaceUpdate deleteMin()
	{
		ContentChangeEvent cce =    pq.poll();
		assert cce != null;
		NamespaceUpdate n =         cce2namespaceUpdate.remove(cce);
		assert n != null;
		if (cce.time != currentTime) currentTime = cce.time;
		return n;
	}

	public int getNextEventTime()
	{
		int nextEvent;
		// There are always clock events
		if (nextClockIsRising)  nextEvent = nextClockRisingTime;
		else                    nextEvent = nextClockFallingTime;

		// There might be update events before the next clock event
		try { if (pq.size() > 0) nextEvent = Math.min(nextEvent, pq.peek().time); }
		catch (Exception ex) { throw new ExceptionHandler(ex); }

		return nextEvent;
	}

//	static final int MAX_STEPS_BEFORE_EXCEPTION = 500;
    private transient ArrayList<TimeTableListener> timeTableListeners = new ArrayList<TimeTableListener>();


	/**
	 *  Performs a single step of execution.
	 *
	 *  First advances the time to the next event. Then executes the next event.
	 *  There are three possible events.
	 *  <ul> <li> A rising clock flank </li>
	 *       <li> A falling clock flank</li>
	 *       <li> A data change is performed. This means that all data changes that are scheduled for the current time are executed in parallel. </li>
	 *  </ul>
	 *  Note that clock flanks do not change the values of pins (even for components with delay zero). They only schedule the change to happen. The actual change is executed as a data change type step.
	 * @throws Exception
	 */

	protected void executeStep() throws Exception
    {
		int nextEventTime =     this.getNextEventTime();
//		System.out.println("Current time updated from " + this.currentTime + " to: " + nextEventTime);
		this.currentTime =  nextEventTime;

		if (nextClockIsRising && nextEventTime == nextClockRisingTime)
		{
//			System.out.println("Clock rising event on time: " + nextEventTime);
			nextClockIsRising = false;
			nextClockFallingTime = nextClockRisingTime + (cycleLength+1)/2;
			nextClockRisingTime =  nextClockRisingTime + cycleLength;
			effector.onClockRising(this);

			this.fireClockFlank(true);
//			executer.tsd.addClock(this.currentTime, true);
			return;
		}

		if (!nextClockIsRising && nextEventTime == nextClockFallingTime)
		{
//			System.out.println("Clock falling event on time: " + nextEventTime);
			nextClockIsRising = true;
			effector.onClockFalling(this);

			this.fireClockFlank(false);
//			executer.tsd.addClock(this.currentTime, false);
			return;
		}
		// Als hier ooit wordt gekomen is er een echt event over

//		System.out.println("Update event on time: " + nextEventTime);

//		int nextTime = getFirstEventTime();
//
//		// Also process generated events
//		for (int steps = 0; size > 0 && getFirstEventTime() == nextTime; steps++)
//			if (steps == MAX_STEPS_BEFORE_EXCEPTION) throw new Exception("Maxumum number of steps (" + MAX_STEPS_BEFORE_EXCEPTION + ") reached. You possibly have constructed an infinite loop");
		LinkedList<NamespaceUpdate> happenNow = new LinkedList<NamespaceUpdate>();
		// Gegarandeerd minstens 1 event!
		while (pq.size() > 0 && getNextEventTime() == nextEventTime) {
			happenNow.addLast(this.deleteMin());
		}

		Set<Instance_Wire> wiresWhoseInputChanged = new HashSet<Instance_Wire>();
		Set<Instance> componentsWhoseInputChanged = new HashSet<Instance>();
		for (NamespaceUpdate n : happenNow) {
			n.execute(this, wiresWhoseInputChanged, componentsWhoseInputChanged);
		}

		for (Instance_Wire iw : wiresWhoseInputChanged)	{
			iw.recompute(componentsWhoseInputChanged);
		}

		for (Instance i : componentsWhoseInputChanged) {
			effector.recalculateForComponent(i, this);
		}
		fireValuesChanged();
//		executer.tsd.getValues();
    }

	public void step() throws Exception {
//		assert(executer.effector == effector);
		executeStep();
		fireEventSequenceCompleted();
	}

	public void cycle() throws Exception {
		doCycle();
		fireEventSequenceCompleted();
	}

	public void cycle(int cycles) throws Exception {
		for (int i = 0; i < cycles; i++) {
			doCycle();
		}
		fireEventSequenceCompleted();
	}

	public void doCycle() throws Exception
	{
		assert currentTime <= nextClockRisingTime;
//		assert(executer.effector == effector);
		// Als we aan het eind van een oude cycle zijn voeren we een nieuwe cycle uit.
		if (currentTime == nextClockRisingTime) executeStep();
//		System.out.println("Cycle called on time: " + this.currentTime + " running to time: " + nextClockTime);
		while (getNextEventTime() < nextClockRisingTime)
		{
			executeStep();
		}
		currentTime = nextClockRisingTime;
	}









    public synchronized void removeTimeTableListener(TimeTableListener l)    {
		assert timeTableListeners.contains(l);
		timeTableListeners.remove(l);
    }
    public synchronized void addTimeTableListener(TimeTableListener l)    {
		assert !timeTableListeners.contains(l);
		timeTableListeners.add(l);
    }

    protected void fireEventSequenceCompleted()
    {
		if (highlightManager != null) highlightManager.recomputeHighlights(this);

		for (TimeTableListener t : timeTableListeners) {
			t.eventSequenceCompleted(this);
		}
    }

    protected void fireValuesChanged()
    {
		for (TimeTableListener t : timeTableListeners) {
			t.valuesChanged(this);
		}
    }



	protected void fireInitialisationProgramChanged()
	{
		for (TimeTableListener t : timeTableListeners) {
			t.initialisationProgramChanged(this);
		}
	}



    protected void fireCycleLengthChanged()
    {
		for (TimeTableListener t : timeTableListeners) {
			t.cycleLengthChanged(this);
		}
    }

    protected void fireTimeTableCleared()
    {
		for (TimeTableListener t : timeTableListeners) {
			t.timeTableCleared(this);
        }
    }
    protected void fireClockFlank(boolean rising)
    {
		for (TimeTableListener t : timeTableListeners) {
			t.clockFlank(this, rising);
		}
    }

	public int size() {
		return pq.size();
	}

	public void changeInputTo(Instance_Pin ip, Instance_Simple r) throws Exception {

		Set<Instance_Wire> wiresWhoseInputChanged = new HashSet<Instance_Wire> ();
		Set<Instance> componentsWhoseInputChanged = new HashSet<Instance> ();

		ip.instance.getNamespace().get(ip.pin.getName()).setValue(r, wiresWhoseInputChanged, componentsWhoseInputChanged);

		for (Instance_Wire iw : wiresWhoseInputChanged) {
			iw.recompute(componentsWhoseInputChanged);
		}

		for (Instance i : componentsWhoseInputChanged) {
			effector.recalculateForComponent(i, this);
		}

		this.fireValuesChanged();
		this.fireEventSequenceCompleted();
	}

}
