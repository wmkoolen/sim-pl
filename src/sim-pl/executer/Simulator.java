/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.ParameterContext;
import nbit.NumberFormat;
import nbit.ZeroBitException;
import util.ExceptionHandler;
import util.GUIHelper;
import util.UnionFind;

import compiler.wisent.DataSource;
import compiler.wisent.DataSource_Direct;
import compiler.wisent.IdentifierAlreadyDefined;
import compiler.wisent.Instance_Array;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Namespace;
import compiler.wisent.Type_Array;
import compiler.wisent.Type_Simple;
import compiler.wisent.Type_Struct;
import compiler.wisent.UndefinedIdentifierException;

import databinding.CompilationMessageBuffer;
import databinding.Complex;
import databinding.Component;
import databinding.Event;
import databinding.HPL;
import databinding.NameDeclaringNode;
import databinding.Pin;
import databinding.ResolutionException;
import databinding.Simple;
import databinding.SubComponent;
import databinding.compiled.CompilationMessage;
import databinding.compiled.Compiled.CompiledComplex;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.compiled.Compiled.CompiledPin;
import databinding.compiled.Compiled.CompiledSimple;
import databinding.compiled.Compiled.CompiledStorage;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;
import databinding.render.ProbeRenderNode;
import databinding.wires.Edge;
import databinding.wires.MainPinStub;
import databinding.wires.Node;
import databinding.wires.Span;
import databinding.wires.Split;
import databinding.wires.SubPinNode;
import databinding.wires.Wire;
import executer.ComponentPanel.LocalWireInstance;
import executer.Simulator.Instance_Complex;
import executer.timetable.ContentChangeEvent;
import executer.timetable.TimeTable;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


/**
 * Externally unconnected inputs  need cells (free inputs)
 * Internally unconnected outputs need cells (output buffer)
 * Simple bidirectionals
 */

public class Simulator implements Effector {

	private final File componentFile;

	/** the root of the tree containing pin values & wires */
	private final Instance_Component<?,?> instance;

	/** compiled version of the component */
	private final CompiledComponent<?> cc;

//	/** mapping of gui elements to wires */
//	private final Map<Location<?>,Instance_Wire> gc2wire = new HashMap<Location<?>,Instance_Wire>();

	/** all wire instances */
	private final List<Instance_Wire> wires = new ArrayList<Instance_Wire>();

	/** all default probes */
	private final Map<Location<Pin>, Instance_Wire> pin2wire = new HashMap<Location<Pin>, Instance_Wire>();



	public static abstract class Instance_Component<S extends Component, T extends CompiledComponent<S>> implements Instance {
		Instance_Component<?,?> parent;
		T peer;
		SubComponent subpeer;
		Namespace<String, DataSource> namespace = new Namespace<String,DataSource>();
//		Map<Pin,VInstance> pin2instance = new HashMap<Pin,VInstance>();

		public Instance_Component(T peer, SubComponent subpeer, Instance_Component<?,?> parent) {
			this.peer = peer;
			this.subpeer = subpeer;
			this.parent = parent;
		}

		public Namespace<String, DataSource> getNamespace() {
			return namespace;
		}

		protected void collectPins(UnionFind<Location<?>> uf, Set<Location<Pin>> unconnected_inputs) {
			for (CompiledPin p : peer.pin2cpin.values()) {
				Location<Pin> lp = new Location<Pin>(this, p.peer);
				uf.add(lp);
				if (p.peer.isInput()) unconnected_inputs.add(lp);
			}
		}

		protected abstract void collectWiring(UnionFind<Location<?>> uf, Set<Location<Pin>> unconnected_inputs) throws Exception;
		protected abstract void createPinInstances(Map<Location<Pin>, Instance_Wire> pin2wire, Set<Location<Pin>> unconnected_inputs);

		public String toString() {
			return subpeer == null
				? peer.peer.toString()
				: parent.toString() + "." + subpeer.toString();
		}

		public String getHybridName(String postfix) {
			if (subpeer == null) {
				return postfix == null ? peer.peer.toString() : postfix;
			} else {
				return parent.getHybridName(subpeer.getName() + (postfix != null ? "."+postfix : ""));
			}
		}

		public String getFullName() {
			return subpeer == null
				? peer.peer.toString()
				: parent.getFullName() + "." + subpeer.toString();
		}

		public final S getComponent() { return peer.peer; }

		public void restart() throws Exception {
			for (DataSource d : namespace.getValues()) {
				d.setToAllOnes();
			}
		}

		public final Object resolve(HPL hpl) throws ResolutionException {
			if (!this.peer.peer.getName().equals(hpl.root)) throw new ResolutionException(null, hpl.root);
			return resolve(hpl, 0);
		}

		public Object resolve(HPL hpl, int index) throws ResolutionException {
			assert index >= 0 && index <= hpl.subs.size();
			assert index == 0 || subpeer.getName().equals(hpl.subs.get(index - 1));

			// we do not have subcomponents ( can be handled by overloading)
			if (index < hpl.subs.size())
				throw new ResolutionException(this, hpl.subs.get(index));

			if (hpl.part == null)
				return this;

			try {
				return namespace.get(hpl.part);
			} catch (UndefinedIdentifierException ex) {
				throw new ResolutionException(this, ex.identifier);
			}
		}

		protected void collectProbes(AffineTransform t, List<Probe> probes, Map<Location<Pin>,Instance_Wire> pin2wire) {
			for (CompiledPin p : peer.pin2cpin.values()) {
				Location<Pin> pl = new Location<Pin>(this, p.peer);
				Instance_Wire wi = pin2wire.get(pl);
				if (wi == null ||
				// unconnected complex input to wire
					p.peer.isInput() && wi.senders.contains(pl) ||
				// unconnected complex output of wire
 					p.peer.isOutput() && !wi.senders.contains(pl)) {
					probes.add(new PinProbe(t, p.peer, p.peer.getPos(), HAlignment.center, VAlignment.center, this));
				}
			}
		}
	}

	/** Simple component instance
	 *  (C), (U) for (un)connected
	 *  (I), (O), (B) for in/output or both
	 *
	 *       read value from         write value to
	 * IU    Cell (free input)       -
	 * IC    Wire                    -
	 * OU    Cell (simple output)    Cell (simple output)
	 * OC    Wire                    Cell (simple output)
	 *
	 * A note on bidirectional pins:
	 * bidirectionals get their value from the wire and write their value to a cell
	 */


	public static class Instance_Simple extends Instance_Component<Simple,CompiledSimple> {

		public Instance_Simple(CompiledSimple peer, SubComponent subpeer, Instance_Component<?,?> parent) throws Exception {
			super(peer, subpeer, parent);

//			for (CompiledPin p : peer.pin2cpin.values()) {
//				Type_Simple t = new Type_Simple(p.bits, p.peer.isSigned());
//				// TODO: DataSource_Direct is not the right kind here
//				namespace.set(p.peer.getName(), new DataSource_Direct(t.createInstance()));
//			}

			for (CompiledStorage s : peer.storage2cstorage.values()) {
				DataSource ds = new DataSource_Direct(new Instance_Array(new  Type_Array(s.width, new Type_Simple(s.bits, s.peer.isSigned()))));
				namespace.set(s.peer.getName(), ds);
			}
		}

		public Type_Struct getType() {
			return this.peer.type;
		}


		public void collectWiring(UnionFind<Location<?>> uf, Set<Location<Pin>> unconnected_inputs) {}

		public void createPinInstances(Map<Location<Pin>,
									   Instance_Wire > pin2wire,
									   Set<Location<Pin>> unconnected_inputs) {
			for (CompiledPin p : peer.pin2cpin.values()) {
				Location<Pin> lp = new Location<Pin> (this, p.peer);

				// outputs: need a buffer
				// inputs:  need a buffer if unconnected

				boolean isFreeInput = p.peer.isInput() && unconnected_inputs.contains(lp);
				boolean needBuffer = p.peer.isOutput() || isFreeInput;
				boolean isEditable = p.peer.isInput();
//				assert (pin2wire.get(lp) != null) || isFreeInput : "needbuffer: " + needBuffer + " v.s. pin2wire: " + pin2wire.get(lp);

				Instance_Wire wi = pin2wire.get(lp);
				Instance notifyOnChange = isFreeInput ? this  : null;

				try {
					DataSource ds = needBuffer
						? new FreePin(new compiler.wisent.Instance_Simple(new Type_Simple(p.bits, p.peer.isSigned())), wi, notifyOnChange, isEditable)
						: new WiredPin(wi, p.peer.isSigned());
					namespace.set(p.peer.getName(), ds);
				} catch (IdentifierAlreadyDefined ex) { throw new Error(ex); }
				  catch (ZeroBitException ex) { throw new Error(ex); }

				if (wi != null) {
					if (p.peer.isInput())  wi.receivers.add(lp);
					if (p.peer.isOutput()) wi.senders.add(lp);
				}
			}
		}

		public void restart() throws Exception {
			// clear all pin values
			super.restart();
		}

		public void initPhaseOne() throws Exception {
			// execute the INIT action without delay
			if (peer.wcdl[Event.EVENT_INIT] != null) {
				peer.wcdl[Event.EVENT_INIT].execute(namespace);
			}
		}

		public void initPhaseTwo(TimeTable tt) throws Exception {
			this.onEvent(Event.EVENT_INPUT_CHANGE, tt);
		}


		public void onClockRising(TimeTable tt) throws Exception {
			this.onEvent(Event.EVENT_CLOCK_RISING, tt);
		}

		public void onClockFalling(TimeTable tt) throws Exception {
			this.onEvent(Event.EVENT_CLOCK_FALLING, tt);
		}

		private static final boolean DEBUG = false;


		public void onEvent(int event, TimeTable tt) throws Exception {
			Simple component = peer.peer;

			if (DEBUG)
				System.out.println("On Event: " + Event.events[event] + " for " +
								   this +"/" + component);

			if (peer.wcdl[event] != null) {
				ContentChangeEvent cce = new ContentChangeEvent(this, component.getInternals().getDelay());

				NamespaceUpdate update = tt.getNamespaceUpdate(cce);

				if (DEBUG) System.out.println("Namespace before: " + namespace);
				// we want to keep (cache) current outputs
				namespace.open();

				for (Pin p : component.getIo().getPins()) {
					if (p.isOutput())
						namespace.set(p.getName(),
									  update.getDataSource(p.getName()));
				}

				// TODO: implement copy-on-write, this is very wasteful when changes in memories are sparse
				// EFFECTUATE CHANGES IN MEMORY IMMEDIATELY
//				for (CompiledStorage s : peer.storage2cstorage.values()) {
//					namespace.set(s.peer.getName(), update.getDataSource(s.peer.getName()));
//				}

				try {
					peer.wcdl[event].execute(namespace);
				} catch (Exception ex) {
//				ex.printStackTrace();
					throw new Exception("Execution error in " + getComponent().getName() + " on " + peer.wcdl[event], ex);
				}

				if (DEBUG) System.out.println("Namespace after: " + namespace);
				namespace.close();
			}
		}
	}


	/** Complex component instance
	 *  (C), (U) for (un)connected
	 *  (I), (O) for in/output
	 *
	 * IXY means input  with external X and internal Y
	 * OXY means output with internal X and external Y
	 *
	 *        value from
	 * IUU    Cell (free input)
	 * IUC    Cell (free input)
	 * ICU    Wire
	 * ICC    Wire
	 * OUU    LooseEnd
	 * OUC    LooseEnd
	 * OCU    Wire
	 * OCC    Wire
	 *
	 * Note here:
	 * for inputs,  functionality depends on EXTERNAL connectedness
	 * for outputs, functionality depends on INTERNAL connectedness
	 */

	public static class Instance_Complex extends Instance_Component<Complex,CompiledComplex> {
		public final Map<SubComponent, Instance_Component<?,?>> sub2i = new HashMap<SubComponent,Instance_Component<?,?>>();
		public final Map<Wire, Instance_Wire> w2wi = new HashMap<Wire,Instance_Wire>();

		public Instance_Complex(CompiledComplex peer, SubComponent subpeer, Instance_Component<?,?> parent) throws Exception {
			super(peer, subpeer, parent);

			for (Map.Entry<SubComponent, CompiledComponent<?>> e : peer.sub2csub.entrySet()) {
				SubComponent s = e.getKey();
				sub2i.put(s, Simulator.createInstance(this, s.getSubComponent(), s, e.getValue()));
			}


//			for (CompiledPin p : peer.pin2cpin.values()) {
//				Type_Simple t = new Type_Simple(p.bits, p.peer.isSigned());
//
//				// TODO: DataSource_Direct is not the right kind here
//				namespace.set(p.peer.getName(), new DataSource_Direct(t.createInstance()));
//			}
		}

		public Type_Struct getType() {
			throw new ExceptionHandler("Unimplemented method");
		}


		public Instance getSubComponentInstance(String name) throws Exception{
			return sub2i.get(lookup(peer.peer.getSubcomponents().getSubcomponents(), name));
		}

		public Instance_Wire getWireInstance(String name) throws Exception{
			return w2wi.get(peer.peer.getWires().getByName(name));
		}

		public void collectPins(UnionFind<Location<?>> uf, Set<Location<Pin>> unconnected_inputs) {
			super.collectPins(uf, unconnected_inputs);
			for (Instance_Component<?,?> ic : sub2i.values()) {
				ic.collectPins(uf, unconnected_inputs);
			}
		}

		private <T extends NameDeclaringNode> T lookup(Collection<T> es, String name) throws Exception {
			for (T e : es) {
				if (e.getName().equals(name)) return e;
			}
			throw new Exception(name + " not found");
		}

		private Location<Pin> getLocation(SubPinNode l) throws Exception {
			Instance_Component<? extends Component,? extends CompiledComponent<? extends Component>> c = this;
			Instance_Complex ic = (Instance_Complex)c;
			SubComponent sc = lookup((ic.peer.peer).getSubcomponents().getSubcomponents(), l.getSubRef());
			c = ic.sub2i.get(sc);
			assert c != null;
			return new Location<Pin>(c, lookup(c.peer.peer.getIo().getPins(), l.getStub().getPinRef()));
		}

		public void collectWiring(UnionFind<Location<?>> uf, Set<Location<Pin>> unconnected_inputs) throws Exception {
			// forward to children
			for (Instance_Component<?,?> ic : sub2i.values()) {
				ic.collectWiring(uf, unconnected_inputs);
			}

			for (Wire w : peer.peer.getWires().getChildren()) {
				Location<Wire> lw = new Location<Wire>(this, w);
				uf.add(lw);

				// first add all nodes to the union-find
				for (Node n : w.getNodes().getChildren()) {
					Location nl = new Location<Node> (this, n);
					uf.add(nl);
					uf.union(lw, nl); // union node with wire

					// union pin-nodes with their true pin node
					if (n instanceof SubPinNode) {
						SubPinNode pt = (SubPinNode)n;
						Location<Pin> pl = getLocation(pt);
						uf.union(nl, pl);
						// inputs are connected if there is an EXTERNAL wire leading to them
						if (pl.element.isInput() && pl.i != this)
							unconnected_inputs.remove(pl);
					} else if (n instanceof MainPinStub) {
						MainPinStub pt = (MainPinStub)n;
						Location<Pin> pl = new Location<Pin> (this, pt.getPin());
						uf.union(nl, pl);
						// inputs are connected if there is an EXTERNAL wire leading to them
						if (pl.element.isInput() && pl.i != this)
							unconnected_inputs.remove(pl);
					}
				}

				// then union all nodes with their edges
				for (Span s : w.getSpans().getChildren())
					for (Edge e : s.edges) {
						Location<?> el = new Location<Edge> (this, e);
						uf.add(el);
						uf.union(el, lw);

						uf.union(el, new Location<Node> (this, e.getNode1()));
						uf.union(el, new Location<Node> (this, e.getNode2()));
					}
			}
		}

		public void createPinInstances(Map<Location<Pin>, Instance_Wire> pin2wire, Set<Location<Pin>> unconnected_inputs) {
			// forward to children
			for (Instance_Component<?,?> ic : sub2i.values()) {
				ic.createPinInstances(pin2wire, unconnected_inputs);
			}


			// compute internally unconnected outputs
			Map<String, Pin> unconnected_outputs = new HashMap<String, Pin> ();
			for (CompiledPin p : peer.pin2cpin.values()) {
				if (p.peer.isOutput())
					unconnected_outputs.put(p.peer.getName(), p.peer);
			}
			for (Wire w : peer.peer.getWires().getChildren())
			for (Node n : w.getNodes().getChildren()) {
				if (n instanceof MainPinStub) {
					MainPinStub pn = (MainPinStub)n;
					//TODO: solve correctly
					unconnected_outputs.remove(pn.getPin().getName());
				}
			}

			for (CompiledPin p : peer.pin2cpin.values()) {
				Location<Pin> lp = new Location<Pin> (this, p.peer);

				// outputs: need a 'loose end' buffer if internally unconnected
				// inputs:  need a buffer if unconnected

				boolean isFreeInput = p.peer.isInput() && unconnected_inputs.contains(lp);
				boolean isFreeOutput = p.peer.isOutput() &&
					unconnected_outputs.containsKey(p.peer.getName());
				boolean needBuffer = isFreeOutput || isFreeInput;
				boolean isEditable = p.peer.isInput();
				assert (pin2wire.get(lp) != null) || needBuffer;

				Instance_Wire wi = pin2wire.get(lp);

				try {
					DataSource ds = needBuffer
						? new FreePin(new compiler.wisent.Instance_Simple(new Type_Simple(p.bits, p.peer.isSigned())), wi, null, isEditable)
						: new WiredPin(wi, p.peer.isSigned());
					namespace.set(p.peer.getName(), ds);
				} catch (IdentifierAlreadyDefined ex) {
					throw new Error(ex);
				} catch (ZeroBitException ex) {
					throw new Error(ex);
				}

				if (wi != null && (isFreeInput || isFreeOutput)) {
					wi.senders.add(lp);
				}

//				//TODO: properly resolve bit width
//				int bits = 10;
//
//				try {
//					VInstance v = needBuffer
//						? new VCBuffer(new nBit(bits))
//						: new VCWire(pin2wire.get(lp));
//					assert!pin2instance.containsKey(p);
//					pin2instance.put(p.peer, v);
//				} catch (ZeroBitException ex) {
//					throw new Error(ex);
//				}
			}
		}

		public void restart() throws Exception {
			super.restart();
			for (Instance_Component<?,?> ic : sub2i.values())
				ic.restart();
		}


		public void initPhaseOne() throws Exception
		{
			// forward to children
			for (Instance_Component<?,?> ic : sub2i.values())
				ic.initPhaseOne();
		}

		public void initPhaseTwo(TimeTable tt) throws Exception
		{
			// forward to children
			for (Instance_Component<?,?> ic : sub2i.values())
				ic.initPhaseTwo(tt);
		}


		public void onClockRising(TimeTable tt) throws Exception
		{
			for (Instance_Component<?,?> ic : sub2i.values())
				ic.onClockRising(tt);
		}

		public void onClockFalling(TimeTable tt) throws Exception
		{
			for (Instance_Component<?,?> ic : sub2i.values())
				ic.onClockFalling(tt);
		}

		// need to overload here, for we can handle subcomponents now
		public Object resolve(HPL hpl, int index) throws ResolutionException {
			// check wether we require subcomponents
			if (index == hpl.subs.size()) return super.resolve(hpl, index);
			SubComponent sc;
			try {
				sc = lookup(peer.peer.getSubcomponents().getSubcomponents(), hpl.subs.get(index));
			} catch (Exception ex) { throw new ResolutionException(this, hpl.subs.get(index)); }
			return this.sub2i.get(sc).resolve(hpl, index + 1);
		}




		public void collectProbes(AffineTransform t, List<Probe> probes, Map<Location<Pin>,Instance_Wire> pin2wire) {
			super.collectProbes(t, probes, pin2wire);

			// forward to subcomponents
			for (Instance_Component<?,?> ic : sub2i.values()) {
				AffineTransform tc = new AffineTransform(t);
				tc.concatenate(ic.subpeer.getTransform());
				ic.collectProbes(tc, probes, pin2wire);
			}
			for (Wire w : peer.peer.getWires().getChildren())
			for (Node n : w.getNodes().getChildren())
			for (databinding.wires.Probe p : n.getChildren()) {
				if (n instanceof Split) {
					Instance_Wire iw = w2wi.get(w);
					assert iw != null;
					probes.add(new WireProbe(w, iw, this, p.getTruePos(), p.getHalign(), p.getValign(), t));
				} else if (n instanceof MainPinStub) {
					probes.add(new PinProbe(t, ((MainPinStub)n).getPin(), p.getTruePos(), p.getHalign(), p.getValign(), this));
				} else if (n instanceof SubPinNode) {
					SubPinNode spn = (SubPinNode)n;
					Instance_Component<?,?> ic = sub2i.get(spn.getSubComponent());
					Pin pin = spn.getSubComponent().getSubComponent().getIo().getByName(spn.getPinRef());
					probes.add(new PinProbe(t, pin, p.getTruePos(), p.getHalign(), p.getValign(), ic));
				}
			}
		}
	}





	 static Instance_Component createInstance(Instance_Component<?,?> parent, Component c, SubComponent subpeer, CompiledComponent tt) throws Exception {
		assert c instanceof Simple || c instanceof Complex;
//
//		if (subpeer != null) {
//			// resolve all parameters in external scope
//			Map<String, Integer> p2v = new HashMap<String,Integer>();
//
//			for (ParameterVal pv : subpeer.getChildren()) {
//				p2v.put(pv.getName(), pv.getValue().getValue(pc));
//			}
//
//			pc.enterScope(subpeer);
//			for (Entry<String,Integer> e : p2v.entrySet()) {
//				pc.put(e.getKey(), e.getValue());
//			}
//			pc.exitScope(subpeer);
//		}
//
		return (c instanceof Simple)
			? new Instance_Simple((CompiledSimple)tt, subpeer, parent)
			: new Instance_Complex((CompiledComplex)tt, subpeer, parent);
	}

	public Simulator(Component c, File componentFile) throws Exception {
		this.componentFile = componentFile;

		ComponentNamespace cns = new ComponentNamespace(c);
		CompilationMessageBuffer cmb = new CompilationMessageBuffer();
		ParameterContext<String,Integer> pc = new ParameterContext<String,Integer>();

		cc = cns.compile(pc, Collections.<String>emptySet(), cmb);
		if (cmb.getErrorCount() > 0) {
			StringBuffer s = new StringBuffer();
			for (CompilationMessage m : cmb) {
				s.append(m + "\n");
			}
			throw new Exception("Component has errors:\n" + s);
		}
		instance = createInstance(null, c, null, cc);

		UnionFind<Location<?>> uf = new UnionFind<Location<?>>();
		Set<Location<Pin>> unconnected_inputs = new HashSet<Location<Pin>>();

		// collect all pins in the union-find, and all inputs in unconnected-inputs
		instance.collectPins(uf, unconnected_inputs);

		// then union all wires toghether
		instance.collectWiring(uf, unconnected_inputs);



		// iterate over connected components, to create Instance_Wire objects
		for (Collection<Location<?>> ls : uf.getEqvClasses()) {
			ArrayList<Location<Edge>> ledges = new ArrayList<Location<Edge>>();
			ArrayList<Location<Node>> lnodes = new ArrayList<Location<Node>>();
			ArrayList<Location<Pin>>  lpins  = new ArrayList<Location<Pin>>();
			ArrayList<Location<Wire>> lwires = new ArrayList<Location<Wire>>();

			for (Location<?> l : ls) {
				if (l.element instanceof Edge) ledges.add((Location<Edge>)l);
				else if (l.element instanceof Node) lnodes.add((Location<Node>)l);
				else if (l.element instanceof Pin)  lpins.add((Location<Pin>)l);
				else if (l.element instanceof Wire) lwires.add((Location<Wire>)l);
				else throw new Error("Unexpected type: " + l.element.getClass().getName());
			}

			// only create wire instances if there are edges involved
			assert ledges.isEmpty() == lnodes.isEmpty();
			assert ledges.isEmpty() == lwires.isEmpty();
			if (ledges.isEmpty()) continue;

			// check bit widths and type
			Integer bits = null;
			for (Location<Pin> l : lpins) {
				CompiledPin cp = ((Instance_Component<?,?>)l.i).peer.pin2cpin.get(l.element);
				if (bits == null) {
					bits = cp.bits;
				} else {
					if (bits != cp.bits) {
						throw new Exception("Bit width problem");
					}
				}
			}
			assert (bits != null);

			Instance_Wire wi = new Instance_Wire(bits);
			wires.add(wi);

			for (Location<Wire> l : lwires) {
				assert !((Instance_Complex)l.i).w2wi.containsKey(l.element);
				((Instance_Complex)l.i).w2wi.put(l.element, wi);
			}

			for (Location<Pin> l : lpins) {
				pin2wire.put(l, wi);
			}
		}

		instance.createPinInstances(pin2wire, unconnected_inputs);
	}


	// be a nice effector


	/** Returns a list of probes, that is positions with an associated value-source
	 * The executer displays these probes's values in yellow boxes.
	 * Probes can also be selected in the time-table dialog
	 *
	 * The idea is this: we make this function REALLY intelligent, such that there
	 * is no real need to explicitly place probes anymore.
	 *
	 * We want
	 * 1 single probe per wire, in the middle of the longest wire
	 * outputs receive probes if they are unconnected, or connected to a wire with multiple outputs
	 * inputs receive probes only if they are unconnected
	 */

	public void collectProbes(AffineTransform f, List<Probe> probes, Instance instance) {
		((Instance_Component<?,?>)instance).collectProbes(new AffineTransform(), probes, pin2wire);
	}


	public Component getComponent() {
		return instance.getComponent();
	}

	public Instance getInstance() {
		return instance;
	}

	public CompiledComponent<?> getCompiledComponent() {
		return cc;
	}

	public void clear() throws Exception {
		for (Instance_Wire wi : wires) {
			wi.restart();
		}
		instance.restart();
	}

	public void onInit(TimeTable tt) throws Exception {

		// execute the first init step, in which components recieve INIT events
		instance.initPhaseOne();

		// then recompute all wires
		Set<Instance> changed = new HashSet<Instance>();
		for (Instance_Wire wi : wires) {
			wi.recompute(changed);
		}

		// finally, send all components input-changed events
		instance.initPhaseTwo(tt);
	}

	public void onClockRising(TimeTable tt) throws Exception {
		instance.onClockRising(tt);
	}

	public void onClockFalling(TimeTable tt) throws Exception {
		instance.onClockFalling(tt);
	}

	public void onClose() throws Exception {
	}

	public void recalculateForComponent(Instance i, TimeTable tt) throws Exception {
		Instance_Simple instance = (Instance_Simple)i;
		instance.onEvent(Event.EVENT_INPUT_CHANGE, tt);
	}

	public File getComponentFile() {
		return componentFile;
	}
}






































class Location<T> {
	final Instance i;
	final T element;

	public Location(Instance i, T element) {
		assert i != null;
		assert element != null;
		this.i = i;
		this.element = element;
	}

	public int hashCode() {
		return i.hashCode() * 513 ^ element.hashCode();
	}

	public boolean equals(Object o) {
		Location l = (Location)o;
		assert (element == l.element) == (element.equals(l.element));
		return i == l.i && element == l.element;
	}

	public String toString() {
		return i.toString() + ":" + element.toString();
	}
}


abstract class SProbe implements Probe {
	final Point2D p;
	final HAlignment halign;
	final VAlignment valign;
	final AffineTransform f;

	public SProbe(Point2D p, HAlignment halign, VAlignment valign, AffineTransform f) {
		this.p = p;
		this.f = f;
		this.halign = halign;
		this.valign = valign;
	}

	public abstract Instance_Simple getValue();
	public abstract Color getColor();

	public void render(Graphics2D g, NumberFormat nf) {
		AffineTransform o = g.getTransform();
		g.transform(f);
		GUIHelper.drawEncadredString(g, p, halign, valign, getValue().toFormattedString(nf),
									 getColor(), Color.black, Color.black);
		g.setTransform(o);
	}


	public double getDist(Point2D pos, Graphics2D g, NumberFormat nf) {
		AffineTransform o = g.getTransform();
		g.transform(f);
		Rectangle2D r = GUIHelper.getEncadredStringBound(g, p, halign, valign, getValue().toFormattedString(nf));
		g.setTransform(o);

		return r.contains(pos)
			? 0
			: GUIHelper.minDist(r, pos);
	}

}


class WireProbe extends SProbe {
	final Instance_Wire wi;
	final Wire w;
	final Instance_Complex ic;

	public WireProbe(Wire w, Instance_Wire wi, Instance_Complex ic, Point2D p, HAlignment halign, VAlignment valign, AffineTransform f) {
		super(p, halign, valign, f);
		this.w = w;
		this.wi = wi;
		this.ic = ic;
	}

	public Instance_Simple getValue() { return wi.value; }
	public Color getColor() { return ProbeRenderNode.wireProbeColor; }

	public ValuedObject getUnderlyingNode() {
		return new LocalWireInstance(w, wi, ic);
	}

	//
//	public void render(Graphics2D g, NumberFormat nf) {
//		AffineTransform o = g.getTransform();
//		g.transform(f);
//		GUIHelper.drawEncadredString(g, p, wi.value.toFormattedString(nf),
//									 ProbeRenderNode.wireProbeColor, Color.black, Color.black);
//		g.setTransform(o);
//	}
}


class PinProbe extends SProbe {
	final Simulator.Instance_Component<?,?> ci;
	final Pin pin;

	PinProbe(AffineTransform f, Pin pin, Point2D pos, HAlignment halign, VAlignment valign, Simulator.Instance_Component<?,?> ci) {
		super(pos, halign, valign, f);
		this.ci = ci;
		this.pin = pin;
	}

	public Instance_Simple getValue() {
		try {
			return (Instance_Simple)ci.getNamespace().get(pin.getName()).getValue();
		} catch (UndefinedIdentifierException ex) { throw new Error(ex); }
	}

	public Color getColor() { return ProbeRenderNode.pinProbeColor; }

	public ValuedObject getUnderlyingNode() {
		return new Instance_Pin(ci, pin);
	}

	//	public void render(Graphics2D g, NumberFormat nf) {
//		try {
//			AffineTransform o = g.getTransform();
//			g.transform(f);
//			GUIHelper.drawEncadredString(g, pos,
//							   ci.getNamespace().get(p.getName()).getValue().
//							   toFormattedString(nf),
//							   ProbeRenderNode.pinProbeColor, Color.black, Color.black);
//			g.setTransform(o);
//		} catch (UndefinedIdentifierException ex) { throw new Error(ex); }
//	}
}




/*
public static interface VInstance {
	public nBit get();         // used in WSDL
	public void set(nBit v);   // used in WSDL

//		public nBit read();        // used by executer
//		public void write(nBit v); // used by executer
}



// bidirectional: read value from wire, write value to cell
public static class VCBidi implements VInstance {
	final nBit value;
	final Instance_Wire w;

	public VCBidi(nBit value, Instance_Wire w) {
		this.value = value;
		this.w = w;
	}

	public nBit get() { return w.value.get(); }
	public void set(nBit value) { this.value.ASSIGN(value); }
}

// read and write value from/to buffer
public static class VCBuffer implements VInstance {
	final nBit value;
	public VCBuffer(nBit value) {
		this.value = value;
	}
	public nBit get() { return value; }
	public void set(nBit value) { this.value.ASSIGN(value); }
}

// read value from wire, no writes
public static class VCWire implements VInstance {
	final Instance_Wire w;
	VCWire(Instance_Wire w) {
		this.w = w;
	}

	public nBit get() { return w.value.get(); }
	public void set(nBit value) { throw new Error("direct write to wire-through"); }
}
*/


class FreePin extends DataSource_Direct {
	final Instance_Wire wi; // wire to notify when we've changed
	final Instance ic;      // component to notify when we've changed
	final boolean editable;

	public FreePin(compiler.wisent.Instance i, Instance_Wire wi, final Instance ic, boolean editable) {
		super(i);
		this.wi = wi;
		this.ic = ic;
		this.editable = editable;
	}

	public boolean isValueEditable() {
		return editable;
	}

	public void setValue(compiler.wisent.Instance i, Set<Instance_Wire> wwic, Set<Instance> cwic) {
		super.setValue(i, wwic, cwic);
		if (wi != null) wwic.add(wi);
		if (ic != null) cwic.add(ic);
	}
}

class WiredPin implements DataSource {
	final Instance_Wire wi; // the wire that the data comes from
	final boolean signed;   // pins know wheter values should be interpreted signed
	final Type_Simple override;

	WiredPin(Instance_Wire wi, boolean signed) {
		this.wi = wi;
		this.signed = signed;
		try {
			override = (wi.value.type.signed == signed)
				? null
				: new Type_Simple(wi.value.type.bits, signed);
		} catch (ZeroBitException ex) { throw new Error(ex); }
	}

	public compiler.wisent.Instance getValue() {
		if (override == null) return wi.value;
		return new compiler.wisent.Instance_Simple(override, wi.value.value);
	}

	public void setValue(compiler.wisent.Instance i, Set<Instance_Wire> wwic, Set<Instance> cwic) {
		throw new Error("impossible");
	}

	public boolean isValueEditable() {
		return false;
	}

	public DataSource createDirectCopy() {
		throw new Error("impossible");
	}

	public void setToAllOnes() {
		// is called on reset(). Just ignore
	}
}



