/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import nbit.ZeroBitException;
import nbit.nBit;

import compiler.wisent.Type_Simple;
import compiler.wisent.UndefinedIdentifierException;

import databinding.Pin;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Instance_Wire {
	final List<Location<Pin>> receivers = new ArrayList<Location<Pin>>();
	final List<Location<Pin>> senders = new ArrayList<Location<Pin>>();

	public final compiler.wisent.Instance_Simple value;

	public Instance_Wire(int bits) throws ZeroBitException {
		value = new compiler.wisent.Instance_Simple(new Type_Simple(bits, false));
	}

	public String toString() {
		return "("+senders.toString() + " | " + receivers.toString() +")";
	}

	// does wired AND
	public void recompute(Set<Instance> changed) {
//		System.out.println("Old value: " + value);
		compiler.wisent.Instance_Simple v2 = (compiler.wisent.Instance_Simple) value.klone();
		v2.setToAllOnes();
//		System.out.println("All ones: " + v2);

//		System.out.println("senders: " + senders);
		for (Location<Pin> l : senders) {
			try {
				nBit v = ( (compiler.wisent.Instance_Simple)l.i.getNamespace().
						  get(l.element.getName()).getValue()).getValue();
//				System.out.println("sender: " + l + " has value " + v);
				v2.setValue(v2.getValue().AND_BIT(v));
			} catch (UndefinedIdentifierException ex) { throw new Error(ex); }
		}

//		System.out.println("our new value is : " + v2);

		if (value.changeInto(v2)) {
//			System.out.println("change! receivers are : " + receivers);
			for (Location<Pin> l : receivers) {
				changed.add(l.i);
			}
		}
	}

	public void restart() {
		value.setToAllOnes();
	}
}
