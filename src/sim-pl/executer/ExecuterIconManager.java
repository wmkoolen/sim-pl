/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import icons.IconManager;

import javax.swing.ImageIcon;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ExecuterIconManager extends IconManager {
	static final ImageIcon INIT_LARGE = load(ExecuterIconManager.class,"22x22_init.png");;
	static final ImageIcon INIT_SMALL = load(ExecuterIconManager.class,"16x16_init.png");;

	static final ImageIcon INIT_PROGRAM_LARGE = load(ExecuterIconManager.class,"22x22_init_program.png");;
	static final ImageIcon INIT_PROGRAM_SMALL = load(ExecuterIconManager.class,"16x16_init_program.png");;

	static final ImageIcon STEP_LARGE = load(ExecuterIconManager.class,"22x22_step.png");
	static final ImageIcon STEP_SMALL = load(ExecuterIconManager.class,"16x16_step.png");

	static final ImageIcon CYCLE_LARGE = load(ExecuterIconManager.class,"22x22_cycle.png");
	static final ImageIcon CYCLE_SMALL = load(ExecuterIconManager.class,"16x16_cycle.png");

	static final ImageIcon CYCLES_LARGE = load(ExecuterIconManager.class,"22x22_cycles.png");
	static final ImageIcon CYCLES_SMALL = load(ExecuterIconManager.class,"16x16_cycles.png");

	static final ImageIcon CYCLE_LENGTH_LARGE = load(ExecuterIconManager.class,"22x22_cycle_length.png");
	static final ImageIcon CYCLE_LENGTH_SMALL = load(ExecuterIconManager.class,"16x16_cycle_length.png");

	static final ImageIcon TRUTH_TABLE_SMALL = load(ExecuterIconManager.class,"16x16_truthtable.png");
	static final ImageIcon TRUTH_TABLE_LARGE = TRUTH_TABLE_SMALL;


}
