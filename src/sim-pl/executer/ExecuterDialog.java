/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JRootPane;

import util.ScreenShot;

import net.infonode.docking.View;

abstract public class ExecuterDialog extends JRootPane implements TimeTableListener
{

	// Methods to overload immediately!!!
	public void representationChanged() {}
	public void selectionChanged(Instance instance) {}

	public final WorkSheet listener;
	public final String menuCheckBoxName;

	public final View v;

	// For graphic designer, default constructor
	public ExecuterDialog()	{ this(null, null);}

    public ExecuterDialog(WorkSheet listener, String menuCheckBoxName)
    {
		v = new View(menuCheckBoxName, null, this);
		this.menuCheckBoxName = menuCheckBoxName;
		this.listener = listener;
		
		// set up print screen action
		ScreenShot.installHook(this);

//
//		this.addWindowListener(new WindowAdapter()
//		{
//		    public void windowActivated(WindowEvent e)      { cb.setState(isVisible()); }
//			public void windowDeactivated(WindowEvent e)    { cb.setState(isVisible()); }
//		});
    }


	public void setTitle(String title) {
		v.getViewProperties().setTitle(title);
	}


	public void setIconImage(Image icon) {
		v.getViewProperties().setIcon(new ImageIcon(icon));
	}



	public static interface KidPOD {
		public ExecuterDialog createDialog(WorkSheet w) throws Exception;
		public String getID();
	}

	public abstract KidPOD getPOD();

	/** Obtain a unique, pesistent dialog ID. This ID is stored in the layout string, and then used in the XML data to store the settings
	 * @return String
	 */
	public abstract String getID();
}
