/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.programeditor;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.undo.UndoManager;

import log.Log_Dialog;
import log.Logger;
import nbit.NumberFormat;
import nbit.nBit;
import util.CurrentDirectoryManager;
import util.ExceptionHandler;
import util.ExtensionAddingFilter;
import util.FileSlurper;
import util.MyFormatter;
import util.PseudoFileFilter;
import util.VersionManager;
import util.VerticallyNumberedComponent;
import util.WAction;
import antlr.RecognitionException;

import compiler.ComponentCompiler;
import compiler.ComponentProgram;
import compiler.sal.SAL;
import compiler.wasm.WASM;
import compiler.wisent.DataSource;

import databinding.Pin;
import executer.ExecuterDialog;
import executer.Instance;
import executer.Simulator;
import executer.TimeTableListener;
import executer.WorkSheet;
import executer.timetable.Highlight;
import executer.timetable.HighlightManager;
import executer.timetable.TimeTable;


/** The ProgramEditorDialog2 allows users to view and edit programs
 *
 * The ProgramEditorDialog2 has two distinct states:
 *
 * - The 'view' state. The PED2 is in this state when the user compiled the
 *   program, and has not edited it since. The PED2 shows highlights if
 *   available. Any edit causes a transition to the 'editing' state.
 *   The document consists of text runs, possibly with highlight information.
 *   Highlight information is a CHARACTER attribute.
 *
 * - The editing state. This state is used for editing. There are no highlights.
 *   all text is displayed using the default document style. The 'Compile'
 *   action causes a transition to the 'view' state.
 *
 * The normal 'style' is represented using paragraph attributes
 * The highlights are character attributes. (They can be wiped out really easily this way)
 *
 * There are certain attributes that are NOT taken over from paragraph styles
 * - bold/italic
 * - background
 * this of course sucks very much, but we'll leave it like that.
 * we only use background color for highlights anyway.
 */

public class ProgramEditorDialog2 extends ExecuterDialog implements TimeTableListener {
	File                    programFile =   null;
	boolean                 changed =       false;


	ImageIcon success = IconManager.load(ProgramEditorDialog2.class, "16x16_success.png");
	ImageIcon failure = IconManager.load(ProgramEditorDialog2.class, "16x16_failure.png");

	UndoManager undom = new UndoManager();

	public String getID() { return "ProgramEditor"; }

	WAction aNew = new WAction(
		"New",
		false,
		IconManager.FILE_NEW_SMALL,
		IconManager.FILE_NEW_LARGE,
		"Start with a blank file",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK),
		KeyEvent.VK_N
	) {
		 public void actionPerformed(ActionEvent e) {
			 File_New(e);
		 }
	 };


	WAction aOpen = new WAction(
		"Open",
		true,
		IconManager.FILE_OPEN_SMALL,
		IconManager.FILE_OPEN_LARGE,
		"Open file",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK),
		KeyEvent.VK_O
	 ) {
		  public void actionPerformed(ActionEvent e) {
			  File_Open(e);
		  }
	  };


	WAction aSave = new WAction(
		"Save",
		false,
		IconManager.FILE_SAVE_SMALL,
		IconManager.FILE_SAVE_LARGE,
		"Save file to disk",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		KeyEvent.VK_S
	 ) {
		  public void actionPerformed(ActionEvent e) {
			  File_Save(e);
		  }
	  };

	  WAction aSaveAs = new WAction(
		  "Save As",
		  true,
		  IconManager.FILE_SAVEAS_SMALL,
		  IconManager.FILE_SAVEAS_LARGE,
		  "Save file to disk under a new name",
		  null,
		  null, //KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		  KeyEvent.VK_V
		  ) {
		  public void actionPerformed(ActionEvent e) {
			  File_SaveAs(e);
		  }
	  };

	  WAction aGenerateTruthtableProgram = new WAction(
		  "Generate Truth Table",
		  true,
		  IconManager.EMPTY_SMALL,
		  IconManager.EMPTY_LARGE,
		  "Generate a program to compute the Truth Table",
		  null,
		  null, //KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		  KeyEvent.VK_G
		  ) {
		  public void actionPerformed(ActionEvent e) {
			  File_TruthtableProgram(e);
		  }
	  };

	  WAction aUndo = new WAction(
		  "Undo",
		  false,
		  IconManager.UNDO_SMALL, IconManager.UNDO_LARGE,
		  "Undo the most recent edit",
		  null,
		  KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK),
		  KeyEvent.VK_U) {
		  public void actionPerformed(ActionEvent e) {
			  undom.undo();
			  doc.setCharacterAttributes(0, doc.getLength(), sc.getEmptySet(), true);
			  updateUndoRedoState();
		  }
	  };

	WAction aPaste = new WAction(
		"Paste",
		false,
		IconManager.PASTE_SMALL, IconManager.PASTE_LARGE,
		"Paste clipboard text",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK),
		KeyEvent.VK_P) {
		public void actionPerformed(ActionEvent e) {
			taSource.paste();
		}
	};

	WAction aCopy = new WAction(
		"Copy",
		false,
		IconManager.COPY_SMALL, IconManager.COPY_LARGE,
		"Copy text to clipboard",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK),
		KeyEvent.VK_C) {
		public void actionPerformed(ActionEvent e) {
			taSource.copy();
		}
	};

	WAction aCut = new WAction(
		"Cut",
		false,
		IconManager.CUT_SMALL, IconManager.CUT_LARGE,
		"Cut text to clipboard",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK),
		KeyEvent.VK_T) {
		public void actionPerformed(ActionEvent e) {
			taSource.cut();
		}
	};



	  WAction aRedo = new WAction(
		  "Redo",
		  false,
		  IconManager.REDO_SMALL, IconManager.REDO_LARGE,
		  "Redo the most recent undo",
		  null,
		  KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK),
		  KeyEvent.VK_R) {
		  public void actionPerformed(ActionEvent e) {
			  undom.redo();
			  doc.setCharacterAttributes(0, doc.getLength(), sc.getEmptySet(), true);
			  updateUndoRedoState();
		  }
	  };


	WAction aFont = new WAction(
		"Font size",
		true,
//		IconManager.FONT_SMALL, IconManager.FONT_LARGE,
		IconManager.ABOUT_SMALL, IconManager.ABOUT_LARGE,
		"Change the font",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK),
		KeyEvent.VK_F) {
		public void actionPerformed(ActionEvent e) {
			String v =JOptionPane.showInputDialog(ProgramEditorDialog2.this, "New font size", Integer.toString(editorFont.getSize()));
			if (v != null) {
				try {
					int s = Integer.parseInt(v);
					changeFontTo(new Font(editorFont.getName(), editorFont.getStyle(), s), editorFontCol);
				} catch (NumberFormatException ex) {}
			}
		}
	};


	protected class MyUndoableEditListener implements UndoableEditListener
	{
		public void undoableEditHappened(UndoableEditEvent e) {
			if (e.getEdit().getPresentationName().equals("style change")) return;
			//Remember the edit and update the menus
			undom.addEdit(e.getEdit());
			updateUndoRedoState();
		}
	}


	  public void updateUndoRedoState() {
		  aUndo.setEnabled(undom.canUndo());
		  aRedo.setEnabled(undom.canRedo());
	  }


	  ComponentCompiler sal = new SAL();
	  final ComponentCompiler[] compilers = {
		  sal,
		  new WASM()
	  };

    BorderLayout borderLayout1 = new BorderLayout();
    JToolBar jToolBar1 = new JToolBar();
    JComboBox cbCompiler = new JComboBox();
    JButton bCompile = new JButton();
    JScrollPane jScrollPane1 = new JScrollPane();

	// related to text component
	Style def;

	StyleContext sc = new StyleContext();

	// listener for state transition events
	DefaultStyledDocument doc = new DefaultStyledDocument(sc) {
		public void insertUpdate(DefaultDocumentEvent e, AttributeSet as) {
			// discard all style info
			super.insertUpdate(e, null);
			notifyAnEdit(); // need to do this second, for when inserting into a highlight, the
			// new text is automatically highlighted.
			// So we need to disable the highlights last

//			SwingUtilities.invokeLater(new Runnable() {
//				public void run() {
//					if (doc.getLength() == 0 || Math.random() < 0.2) {
//						doc.setCharacterAttributes( 0, doc.getLength(), sc.getEmptySet(), true);
//					} else {
//						for (int i = 0; i < 20; i++) {
//							Color c = new Color( (float) Math.random(), (float) Math.random(), (float) Math.random());
//							doc.setCharacterAttributes( (int) (Math.random() * doc.getLength()), 1, sc.addAttribute(sc.getEmptySet(), StyleConstants.Background, c), true);
//						}
//					}
//				}
//			});
		}

		public void postRemoveUpdate(DefaultDocumentEvent e) {
			super.postRemoveUpdate(e);
			notifyAnEdit();
		}
	};

    JTextPane taSource = new JTextPane(doc);

    JMenuBar    jMenuBar1 =       new JMenuBar();
    JMenu       jMenu1 =          new JMenu();
	JMenu       jMenu2 =          new JMenu();
	JMenu       jMenu3 =          new JMenu();
//    JMenuItem   file_save =       new JMenuItem();
//    JMenuItem   file_saveas =     new JMenuItem();


    public ProgramEditorDialog2(WorkSheet exec)
    {
		super(exec, "Program Editor");
		jbInit();

//		this.taSource.setEnabled(true);
		this.taSource.setBackground(Color.white);
//		this.cbCompiler.setEnabled(true);
//		this.bCompile.setEnabled(true);
//		this.jMenu1.setEnabled(true);
//		this.jMenu2.setEnabled(true);
//		this.jMenu3.setEnabled(true);

//		WAction[] ws = new WAction[] {aNew, aOpen, aFont, aSave, aSaveAs, aGenerateTruthtableProgram};
//		for (int i = 0; i < ws.length; i++) {
//			ws[i].setEnabled(true);
//		}

		this.setChanged(false);

		if (log != null) log.clean();
		jButton1.setEnabled(false);
		jButton2.setEnabled(false);

		refreshTitle();
    }


	private void jbInit()
    {
		this.setTitle("Program Editor");

		bCompile.setText("Compile");
		bCompile.setToolTipText("Compile the source file, and associate the program with the 'Init' action");
        bCompile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				compile(((ComponentCompiler)cbCompiler.getSelectedItem()), taSource.getText());
            }
        });

		cbCompiler.setModel(new DefaultComboBoxModel(compilers));
		cbCompiler.setEditable(false);
		cbCompiler.setSelectedIndex(0);

		jMenu1.setMnemonic('F');
        jMenu1.setText("File");

		jMenu2.setMnemonic('E');
		jMenu2.setText("Edit");

		jMenu3.setText("Settings");


		jMenu1.add(aNew);
		jMenu1.add(aOpen);
		jMenu1.add(aGenerateTruthtableProgram);
		jMenu1.addSeparator();
		jMenu1.add(aSave);
		jMenu1.add(aSaveAs);

		jMenu2.add(aUndo);
		jMenu2.add(aRedo);
		jMenu2.addSeparator();
		jMenu2.add(aCut);
		jMenu2.add(aCopy);
		jMenu2.add(aPaste);


		jMenu3.add(aFont);


		setIconImage(IconManager.loadImage(ProgramEditorDialog2.class, "PE.png"));

//        file_save.setMnemonic('S');
//        file_save.setText("Save");
//		file_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
//        file_save.addActionListener(new java.awt.event.ActionListener()
//        {
//            public void actionPerformed(ActionEvent e)
//            {
//                File_Save(e);
//            }
//        });
//        file_saveas.setText("Save As...");
//        file_saveas.addActionListener(new java.awt.event.ActionListener()
//        {
//            public void actionPerformed(ActionEvent e)
//            {
//                File_SaveAs(e);
//            }
//        });

		taSource.setEditorKit(new NoWrapEditorKit());
		taSource.setDocument(doc);

		jButton1.setIcon(success);
		jButton1.setEnabled(false);
		jButton1.setToolTipText("View compilation log");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				assert (log != null);
				log.setVisible(true);
			}
		});
		jButton2.setEnabled(false);
		jButton2.setText("Unload");
		jButton2.setToolTipText("Remove the compiled program from the 'Init' action");
		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				Clear(actionEvent);
			}
		});

		jMenuBar1.add(jMenu1);
		jMenuBar1.add(jMenu2);
		jMenuBar1.add(jMenu3);

		jToolBar1.add(bCompile);
		jToolBar1.add(jButton1);
		jToolBar1.add(jButton2);
		jToolBar1.addSeparator();
		jToolBar1.add(cbCompiler);

		def = taSource.addStyle("SIM-PL default", taSource.getStyle(StyleContext.DEFAULT_STYLE));
		changeFontTo(new Font("Monospaced", Font.PLAIN, 12), Color.black);

//		taSource.setTabSize(4);
//		taSource.setLineWrap(false);

		vnc.setAlign(false);
//		final VerticallyNumberedComponent vnc = new VerticallyNumberedComponent(editorFont, false);
		vnc.setBorder(BorderFactory.createRaisedBevelBorder());
		jScrollPane1.getViewport().add(taSource, null);
		jScrollPane1.setRowHeaderView(vnc);

		taSource.addComponentListener(new ComponentListener() {
			public void componentHidden(ComponentEvent componentEvent) {}
			public void componentMoved(ComponentEvent componentEvent) {}

			public void componentShown(ComponentEvent componentEvent) {}
			public void componentResized(ComponentEvent e) {
				vnc.setPreferredSize(new Dimension(vnc.getWidth(), taSource.getHeight()));
				vnc.revalidate();
			}
		});

		this.getContentPane().setLayout(borderLayout1);
        this.getContentPane().add(jToolBar1,  BorderLayout.SOUTH);
		this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
		this.getContentPane().add(jMenuBar1, java.awt.BorderLayout.NORTH);
		this.setJMenuBar(jMenuBar1);

		doc.addDocumentListener(new DocumentListener()
		{
			public void changedUpdate(DocumentEvent e)  { /* only used for style changes */ }
			public void insertUpdate(DocumentEvent e)   { setChanged(true); }
			public void removeUpdate(DocumentEvent e)   { setChanged(true); }
		});

		doc.addUndoableEditListener(new MyUndoableEditListener());
		updateUndoRedoState();
    }

	final VerticallyNumberedComponent vnc = new VerticallyNumberedComponent();

	Font editorFont = new Font("Monospaced", Font.PLAIN, 18);
	Color editorFontCol = Color.black;

	private void changeFontTo(Font f, Color c) {
		this.editorFont = f;
		this.editorFontCol = c;

		StyleConstants.setFontFamily(def, editorFont.getFamily());
		StyleConstants.setFontSize(def, editorFont.getSize());
		StyleConstants.setItalic(def, editorFont.isItalic());
		StyleConstants.setBold(def, editorFont.isBold());
		StyleConstants.setForeground(def, editorFontCol);
		doc.setParagraphAttributes(0, doc.getLength()+1, def, true);
		vnc.setFont(editorFont);
//		taSource.setParagraphAttributes(def, true);
	}



	//	we alwyas have the invariant
	// init_version <= compile_version <= edit_version
	int init_version = -1;    // the source text version when the 'init' was issued
	int compile_version = -1; // the source text version when the 'compile' was issued
	int edit_version =  0;    // the source text current version


	/** Query current state
	 *
	 * @return boolean
	 */
	public boolean isDebugMode() {
		assert init_version <= compile_version;
		assert compile_version <= edit_version;

		return init_version == edit_version;
	}


	public void notifyAnEdit() {
		assert init_version <= compile_version;
		assert compile_version <= edit_version;

		if (isDebugMode()) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					// we can not do this in a notification callback
					eraseAllHighlights();
				}
			});
		}
		edit_version++;

		assert init_version <= compile_version;
		assert compile_version <= edit_version;
		assert !isDebugMode();
	}


	public void notifyACompilation() {
		assert init_version <= compile_version;
		assert compile_version <= edit_version;

		compile_version = edit_version;
	}

	public void notifyAnInit() {
		assert init_version <= compile_version;
		assert compile_version <= edit_version;

		init_version = compile_version;
	}


	public void eraseAllHighlights() {
		doc.setCharacterAttributes(0, doc.getLength(), sc.getEmptySet(), true);
		taSource.setCharacterAttributes(sc.getEmptySet(), true);
	}


	public void representationChanged()	{
	}

	public void setChanged(boolean changed)
	{
		if (changed != this.changed)
		{
			this.changed = changed;
			this.refreshTitle();
		}
	}

	public void refreshTitle()
	{
		StringBuffer s = new StringBuffer();
		s.append("Program Editor");

		s.append(": (");
		s.append(programFile == null ? "Untitled" : programFile);
		if (changed)                s.append("*");
		s.append(")");

		setTitle(s.toString());
	}


	Log_Dialog log = null;
	JButton jButton1 = new JButton();
	JButton jButton2 = new JButton();


	public void setCompilationResult(ComponentProgram cp) {
		jButton1.setEnabled(true);
		jButton1.setIcon( cp == null ? failure : success);
//		jButton2.setEnabled(cp != null);
		listener.tt.setInitialisationProgram(cp);
	}

	public void Clear(ActionEvent actionEvent) {
//		jButton2.setEnabled(false);
		listener.tt.setInitialisationProgram(null);
	}


	public void compile(ComponentCompiler cc, String source)
	{
		/* we need a default include directory when there is no file name.
		   there are a couple of options
		   - the component directory
		   - the current directory
		   - no directory

		   We believe that the first option is the most desirable (for we often
		   want to include instruction set definition files, which are by default
		   in the component directory.

		   The moment the file is saved for the first time, the behaviour might
		   thus change. We normally put files in the component directory, so this
		   is not a problem (yet).
		*/

	   File includesDir = programFile != null
		   ? programFile.getParentFile()
		   : ((Simulator)listener.tt.getEffector()).getComponentFile().getParentFile();

//		if (!SaveAs(programFile)) return; // we need the file name to resolve the #includes, so the program needs to be saved

		if (log == null) log = new Log_Dialog((JFrame)super.getTopLevelAncestor().getParent(), "Compile Log", null);
		else    		 log.clean();

		Logger logger = new Logger(log);

		try
		{
			ComponentProgram cp = cc.compile(source, includesDir, listener.tt.getEffector().getInstance(), logger);
//			cp.execute(listener.tt);

//			listener.ttd.timeTableChanged();
//			jButton1.setEnabled(true);
//			jButton1.setIcon(success);
//			jButton2.setEnabled(true);
			logger.addParagraph("Compilation Result: Succes!\nProgram will be loaded on next 'Init'.");
			setCompilationResult(cp);

			notifyACompilation(); // or should we do this in the listener... hell
//			listener.setProgram(cp);
		}
//		catch (CompilerException ex)
//		{
//			logger.addParagraph(ex.getMessage());
//			logger.addParagraph("Compilation Result: Failure");
//		}
//		catch (nbit.ParseException ex) {
//			logger.addParagraph(ex.getMessage());
//			logger.addParagraph("Compilation Result: Failure");
//		}
		catch (RecognitionException ex) {
			logger.addParagraph(ex.toString()); // only toString prints file:line information
			logger.addParagraph("Compilation Result: Failure");
			setCompilationResult(null);
			log.setVisible(true);
		}
		catch (Exception ex)
		{
			logger.addParagraph(ex.getMessage());
			logger.addParagraph("Compilation Result: Failure");
			setCompilationResult(null);
			log.setVisible(true);
		}
	}


	public boolean tryCloseWithUserCancel()
	{
		return doSaveFirst();
	}


	public boolean doSaveFirst()
	{
		if (changed)
		{
			switch (JOptionPane.showConfirmDialog(this, "Current program was modified. Save first?", "Save first", JOptionPane.YES_NO_CANCEL_OPTION)) {
				case JOptionPane.YES_OPTION:
					if (!SaveAs(programFile)) return false;
				break;

				case JOptionPane.NO_OPTION:
					// proceed erasing stuff
				break;

				case JOptionPane.CANCEL_OPTION:
				case JOptionPane.CLOSED_OPTION:
					// no, wait!
					return false;
			}
		}

		taSource.setText("");
		taSource.setParagraphAttributes(def, true);
		undom.discardAllEdits();
		setChanged(false);
		return true;
	}


//	JFileChooser fc;
//	JFileChooser getFC() {
//		if (fc == null) {
//			fc = new JFileChooser(new File("."));
//			for (int i = 0; i < compilers.length; i++) {
//				fc.addChoosableFileFilter(compilers[i].getFileFilter());
//			}
//		}
//		return fc;
//	}


	boolean SaveAs(File saveTo)
	{
		try
		{
		    if (saveTo == null)
		    {
				File suggestedFile = programFile;

				JFileChooser fc = new JFileChooser(new File("."));
				for (int i = 0; i < compilers.length; i++) {
					fc.addChoosableFileFilter(compilers[i].getFileFilter());
				}

				if (suggestedFile != null) fc.setSelectedFile(suggestedFile);
				fc.setFileFilter(((ComponentCompiler)cbCompiler.getSelectedItem()).getFileFilter());
				fc.setDialogTitle("Save Program");

				int result = CurrentDirectoryManager.wrapSave(fc, this);
				if (result != JFileChooser.APPROVE_OPTION) return false;

				saveTo = fc.getSelectedFile();
				FileFilter ff = fc.getFileFilter();
				if (ff != null && ff instanceof ExtensionAddingFilter) {
					saveTo = ( (ExtensionAddingFilter)ff).correctExtenstion(saveTo);
				}
//				saveTo = FileManager.askForFile(this, "Save Program",  suggestedFile, "Save", FileManager.filter_txt);
//				if (saveTo == null) return false;

			    if (saveTo.exists())
			    {
					switch (JOptionPane.showConfirmDialog(this, "File exists.\nOverwrite?", "Overwrite question",  JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
				    {
						case JOptionPane.YES_OPTION:    break;

						case JOptionPane.CLOSED_OPTION:
					    case JOptionPane.NO_OPTION:     return false;
					}
				}
			}

			BufferedWriter bw = new BufferedWriter(new java.io.FileWriter(saveTo));
			bw.write(taSource.getText());
			bw.close();

			programFile = saveTo;
			setChanged(false);
			refreshTitle();
			return true;
		}
	    catch (Exception ex)
	    {
		    ExceptionHandler.handleException(ex);
			return false;
	    }
	}

    void File_Save(ActionEvent e)
    {
		SaveAs(programFile);
    }

    void File_SaveAs(ActionEvent e)
    {
		SaveAs(null);
    }

	void File_New(ActionEvent e)
	{
		if (doSaveFirst())
		{
			assert changed == false;
			programFile = null;
			refreshTitle();
		}
	}



	JFileChooser fcopen; // lazy instantiate

	void File_Open(ActionEvent e) {
		if (doSaveFirst())
		{
			try
			{
				if (fcopen == null) {
					fcopen = new JFileChooser();
					PseudoFileFilter pf = new PseudoFileFilter("All Programs");
					fcopen.addChoosableFileFilter(pf);
					for (int i = 0; i < compilers.length; i++) {
						FileFilter ff = compilers[i].getFileFilter();
						fcopen.addChoosableFileFilter(ff);
						pf.addFileFilter(ff);
					}
					fcopen.setFileFilter(pf);
					fcopen.setDialogTitle("Open Program");
				}
//				JFileChooser fc = getFC();
//				if (suggestedFile != null) fc.setSelectedFile(suggestedFile);


				int result = CurrentDirectoryManager.wrapOpen(fcopen, this);
				if (result != JFileChooser.APPROVE_OPTION) return;
				programFile = fcopen.getSelectedFile();

//				programFile = FileManager.askForFile(this, "Open Program", null, "Open", FileManager.filter_txt);
//				if (programFile == null) return;

				taSource.setText(FileSlurper.slurp(programFile));
				taSource.setCaretPosition(0);

				// phew, that went fine. Discard all edits
				undom.discardAllEdits();

				// select the right compiler
//				FileFilter ff = fc.getFileFilter();
				for (int i = 0; i < compilers.length; i++) {
					if (compilers[i].getFileFilter().accept(programFile)) cbCompiler.setSelectedIndex(i);
				}
			}
			catch (Exception ex)
			{
				programFile = null;
				ExceptionHandler.handleException(ex);
			}
			setChanged(false);
			refreshTitle();
		}
	}


	void File_TruthtableProgram (ActionEvent e) {
		if (doSaveFirst())
		{
			assert changed == false;
			try
			{
				Vector<Instance> instances =  new Vector<Instance>();
				Vector<String> names =      new Vector<String>();

				int bits = ProgramEditorDialog2.addPins(listener.tt.getEffector().getInstance(), instances, names);
				if (bits == 0) throw new Exception("No free inputs found");

				if (bits < 6 || JOptionPane.showConfirmDialog(ProgramEditorDialog2.this, "The Truth Table will have 2^" + bits + " entries.\nThis is a lot.\nAre you sure?", "Possibly large", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					try
					{
						String intervalStr = (String)JOptionPane.showInputDialog(ProgramEditorDialog2.this, "Specify time interval", "Truth Table", JOptionPane.QUESTION_MESSAGE, null, null, Integer.toString(listener.tt.getCycleLength()));
						if (intervalStr == null) return;
						int interval;
						try { interval = Integer.parseInt(intervalStr);}
						catch (NumberFormatException ex) { throw new Exception("Invalid time interval: " + intervalStr); }


						// Calculate length of time part
						int time_max = (1 << bits) * interval;
						int timeb = nBit.bitsUsed(time_max);
						int timel = new nBit(timeb, time_max).toString(NumberFormat.DECIMAL, false, false).length();


						// Fix up input pin names
						String [] pinnames = new String[instances.size()];
						int pinnamel = 0;

						for (int i = 0; i < pinnames.length; i++)
						{
							pinnames[i] = instances.elementAt(i).getHybridName(names.elementAt(i));
							// escape if necessary
							if (!pinnames[i].matches("^\\p{Alpha}[\\p{Alnum}_]*$")) pinnames[i] = "\"" + pinnames[i] + "\"";
							pinnamel = Math.max(pinnamel, pinnames[i].length());
						}

						// Fill in initial values array
						compiler.wisent.Instance_Simple [] values = new compiler.wisent.Instance_Simple[instances.size()];
						for (int i = 0; i < instances.size(); i++)
						{
							values[i] = new compiler.wisent.Instance_Simple(((compiler.wisent.Instance_Simple)((instances.elementAt(i)).getNamespace().get(names.elementAt(i))).getValue()).type);
						}

						// Create the initial output string
						StringBuffer p = new StringBuffer();
						p.append("# Time table for " + listener.tt.getEffector().getInstance().getFullName() + "\n")
						 .append("# " + VersionManager.currentVersion + "\n")
						 .append("# " + new Date() + "\n\n");

						for (int entry = 0; entry < (1 << bits); entry ++)
						{
							p.append(MyFormatter.pad( new nBit(timeb, entry * interval).toString(nbit.NumberFormat.DECIMAL,false, false) + ":", timel + 3));

							for (int i = 0; i < pinnames.length; i++)
							{
								p.append(" ");
								p.append(MyFormatter.pad(pinnames[i] + " = ", pinnamel + 3));
								p.append(values[i].getValue().toString(nbit.NumberFormat.HEXADECIMAL, false, true) + ";");
							}

							// NOTE: first input contains the LSB
							// (in contrary to the truth table)
							boolean carry = true;
							for (int i = 0; carry && i < values.length; i++)
							{
								values[i] = new compiler.wisent.Instance_Simple(values[i].type, values[i].value.ADD(nBit.one));
								carry = !values[i].value.toBoolean();
							}

							p.append('\n');
						}

						taSource.setText(p.toString());
						undom.discardAllEdits();
					}
					catch (compiler.wisent.UndefinedIdentifierException ex) { throw new ExceptionHandler(ex); }

					cbCompiler.setSelectedItem(sal);
				}
			}
			catch (Exception ex)
			{
				ExceptionHandler.handleException(ex);
			}
		}

	}

	private static int addPins(executer.Instance instance, List<Instance> instances, List<String> names) {
		try {
			int bits = 0;

			for (Pin p : instance.getComponent().getIo().getPins()) {
//			for (Iterator i = instance.getComponent().getIo().pins[IO.INPUTS].iterator();
//				 i.hasNext(); ) {
//				Pin p = (Pin) i.next();
				if (!p.isInput()) continue;
				DataSource d = instance.getNamespace().get(p.getName());
				if (d.isValueEditable()) {
					instances.add(instance);
					names.add(p.getName());
					// TODO: fix this
					bits += p.getBits().getValue(null);
				}
			}
			return bits;
		} catch (compiler.wisent.UndefinedIdentifierException ex) {
			throw new ExceptionHandler(ex);
		}
	}



//	abstract class ProgramFactory extends JMenuItem implements ActionListener
//	{
//		public ProgramFactory(String name)
//		{
//			super(name);
//			addActionListener(this);
//		}
//	}


//	class NewProgramFactory extends ProgramFactory
//	{
//		public NewProgramFactory()
//		{
//			super("New");
//			setMnemonic('N');
//			setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
//		}
//
//		public void actionPerformed(ActionEvent e)
//		{
//			if (doSaveFirst())
//			{
//				programFile = null;
//	    		setChanged(false);
//			}
//		}
//	}

//	class LoadProgramFactory extends ProgramFactory
//	{
//		public LoadProgramFactory()
//		{
//			super("Open...");
//			setMnemonic('O');
//			setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
//		}
//
//		public void actionPerformed(ActionEvent e)
//		{
//			if (doSaveFirst())
//			{
//				try
//				{
//					programFile = FileManager.askForFile(this, "Open Program", null, "Open", FileManager.filter_txt);
//					if (programFile == null) return;
//
//					BufferedReader br = new BufferedReader(new FileReader(programFile));
//
//					String line = null;
//					while ((line =br.readLine()) != null)
//					{
//						taSource.append(line + "\n");
//					}
//
//					br.close();
//				}
//				catch (Exception ex)
//				{
//					programFile = null;
//					ExceptionHandler.handleException(ex);
//				}
//				setChanged(false);
//				refreshTitle();
//			}
//		}
//	}
//

//	abstract class TruthTableProgramFactory extends ProgramFactory
//	{
//		public TruthTableProgramFactory(String name)
//		{
//			super(name);
//		}
//
//		public void actionPerformed(ActionEvent e)
//		{
//			if (doSaveFirst())
//			{
//				try
//				{
//					Vector instances =  new Vector();
//					Vector names =      new Vector();
//
//					int bits = this.addPins(listener.tt.getEffector().getInstance(), instances, names);
//					if (bits == 0) throw new Exception("No free inputs found");
//
//					if (bits < 6 || JOptionPane.showConfirmDialog(This, "The truth table will have 2^" + bits + " entries.\nThis is a lot.\nAre you sure?", "Possibly large", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
//					{
//						try
//						{
//							String intervalStr = (String)JOptionPane.showInputDialog(This, "Specify time interval", "Truth Table", JOptionPane.QUESTION_MESSAGE, null, null, Integer.toString(listener.tt.getCycleLength()));
//							if (intervalStr == null) return;
//							int interval;
//							try { interval = Integer.parseInt(intervalStr);}
//							catch (NumberFormatException ex) { throw new Exception("Invalid time interval: " + intervalStr); }
//
//
//							// Calculate length of time part
//							int time_max = (1 << bits) * interval;
//							int timeb = nBit.bitsUsed(time_max);
//							int timel = new nBit(timeb, time_max).toString().length();
//
//
//						    // Fix up input pin names
//							String [] pinnames = new String[instances.size()];
//							int pinnamel = 0;
//
//							for (int i = 0; i < pinnames.length; i++)
//							{
//								pinnames[i] = ((Instance)instances.elementAt(i)).getHybridName((String)names.elementAt(i));
//								pinnamel = Math.max(pinnamel, pinnames[i].length());
//							}
//
//						    // Fill in initial values array
//							compiler.wisent.Instance_Simple [] values = new compiler.wisent.Instance_Simple[instances.size()];
//							for (int i = 0; i < instances.size(); i++)
//							{
//								values[i] = new compiler.wisent.Instance_Simple(((compiler.wisent.Instance_Simple)((DataSource)((component.Instance)instances.elementAt(i)).namespace.get(names.elementAt(i))).getValue()).type);
//							}
//
//							// Create the initial output string
//							StringBuffer p = new StringBuffer();
//							p.append("# Time table for " + listener.tt.getEffector().getInstance().getFullName() + "\n")
//							 .append("# " + VersionManager.currentVersion + "\n")
//							 .append("# " + new Date() + "\n\n");
//
//							for (int entry = 0; entry < (1 << bits); entry ++)
//							{
//								p.append(MyFormatter.pad( new nBit(timeb, entry * interval).toString(nbit.NumberFormat.HEXADECIMAL,false, true) + ":", timel + 3));
//
//								for (int i = 0; i < pinnames.length; i++)
//								{
//									p.append(" ");
//									p.append(MyFormatter.pad(pinnames[i] + " = ", pinnamel + 3));
//									p.append(values[i].getValue() + ";");
//								}
//
//								boolean carry = true;
//								for (int i = 0; carry && i < values.length; i++)
//								{
//									values[i] = new compiler.wisent.Instance_Simple(values[i].type, values[i].value.ADD(nBit.one));
//									carry = !values[i].value.toBoolean();
//								}
//
//								p.append('\n');
//							}
//
//							taSource.setText(p.toString());
//						}
//						catch (compiler.wisent.UndefinedIdentifierException ex) { throw new ExceptionHandler(ex); }
//
//						cbCompiler.setSelectedItem(SAL.compiler);
//					}
//				}
//				catch (Exception ex)
//				{
//					ExceptionHandler.handleException(ex);
//				}
//			}
//		}
//
//		abstract public int addPins(component.Instance instance, Vector instances, Vector names);
//	}


//
//	class MainInputsTruthTableProgramFactory extends TruthTableProgramFactory
//	{
//		public MainInputsTruthTableProgramFactory(String name)
//		{
//			super(name);
//		}
//
//		public int addPins(component.Instance instance, Vector instances, Vector names)
//		{
//			try
//			{
//				int bits = 0;
//
//				for (Iterator i = instance.getComponent().getIo().pins[IO.INPUTS].iterator(); i.hasNext(); )
//				{
//					Pin p =                   (Pin)i.next();
//					DataSource_Component d =    (DataSource_Component) instance.namespace.get(p.name);
//					if (d.isValueEditable())
//					{
//						instances.add(instance);
//						names.add(p.name);
//						bits += p.type.bits;
//					}
//				}
//				return bits;
//			}
//			catch (compiler.wisent.UndefinedIdentifierException ex) { throw new ExceptionHandler(ex); }
//		}
//	}
//
//
//	class AllInputsTruthTableProgramFactory extends TruthTableProgramFactory
//	{
//		public AllInputsTruthTableProgramFactory(String name)
//		{
//			super(name);
//		}
//
//		public int addPins(component.Instance instance, Vector instances, Vector names)
//		{
//			try
//			{
//				int bits = 0;
//
//				for (Iterator i = instance.getComponent().getIo().pins[IO.INPUTS].iterator(); i.hasNext(); )
//				{
//					Pin p =                   (Pin)i.next();
//					DataSource_Component d =    (DataSource_Component) instance.namespace.get(p.name);
//					if (d.isValueEditable())
//					{
//						instances.add(instance);
//						names.add(p.name);
//						bits += p.type.bits;
//					}
//				}
//
//				if (instance instanceof Instance_Complex)
//				{
//					Instance_Complex ic = (Instance_Complex)instance;
//
//					for (int i = 0; i < ic.subs.length; i++)
//					{
//						bits += addPins(ic.subs[i], instances, names);
//					}
//				}
//
//				return bits;
//			}
//			catch (compiler.wisent.UndefinedIdentifierException ex) { throw new ExceptionHandler(ex); }
//		}
//	}


    public void cycleLengthChanged(TimeTable tt)    {    }

    public void timeTableCleared(TimeTable tt)    {
		notifyAnInit();
	}



    public void eventSequenceCompleted(TimeTable tt)    {
		if (!isDebugMode()) return;

		// remove all old highlights
		eraseAllHighlights();

		// and apply new ones


		HighlightManager hm = tt.getHighlightManager();
		if (hm == null) return;
		if (hm.getHighlights() == null) return;

		// scroll the first highlight to be visible
		boolean first = true;

		for (Highlight h : hm.getHighlights()) {
			if (h.ltr == null) continue;
			// must be in this file
			if (h.ltr.file != null) continue;

			// must match to a paragraph
			Element par = doc.getDefaultRootElement().getElement(h.ltr.b_line-1); // lines one based
			if (par == null) continue;

			AttributeSet high = sc.addAttribute(sc.getEmptySet(), StyleConstants.Background, h.color);

//			String stylestr = "highlight:" + h.color.toString();
//			Style high = doc.getStyle(stylestr);
//			if (high == null) {
//				high = doc.addStyle(stylestr, def);
//				StyleConstants.setBackground(high, h.color);
//			}

			int start = par.getStartOffset()+h.ltr.b_col-1; // columns are one based
			int end;
			if (h.ltr.e_line == -1) {
				end = par.getEndOffset();
			} else {
				Element par_e = doc.getDefaultRootElement().getElement(h.ltr.e_line-1); // lines one based
				if (par_e == null) end = par.getEndOffset();
				else 			   end = par_e.getStartOffset() + h.ltr.e_col-1; // columns one based
			}

			// set the style
			doc.setCharacterAttributes(start, end-start, high, true);
			if (first) {
				taSource.setCaretPosition(start);
				first = false;
			}
		}

	}
    public void clockFlank(TimeTable param1, boolean param2)    {    }
    public void valuesChanged(TimeTable e)    {    }

	public void initialisationProgramChanged(TimeTable tt) {
		jButton2.setEnabled(tt.getInitialisationProgram() != null);
	}


	public KidPOD getPOD() {
		throw new Error("Not Implemented");
	}

}
