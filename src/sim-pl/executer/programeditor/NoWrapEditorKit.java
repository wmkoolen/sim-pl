/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer.programeditor;


/* adapted from http://forum.java.sun.com/thread.jspa?forumID=57&threadID=626224
*/

import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.Element;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import util.ExceptionHandler;

public class NoWrapEditorKit extends StyledEditorKit
{
	public ViewFactory getViewFactory()	{
			return new StyledViewFactory();
	}

	static class StyledViewFactory implements ViewFactory {
		public View create(Element elem) {
			String kind = elem.getName();

			if (kind != null) {
				if (kind.equals(AbstractDocument.ContentElementName)) {
					return new LabelView(elem);
				}
				else if (kind.equals(AbstractDocument.ParagraphElementName)) {
					return new ParagraphView(elem);
				}
				else if (kind.equals(AbstractDocument.SectionElementName)) {
					return new NoWrapBoxView(elem, View.Y_AXIS);
				}
				else if (kind.equals(StyleConstants.ComponentElementName)) {
					throw new ExceptionHandler("Somebody fed us an Component?");
				}
				else if (kind.equals(StyleConstants.IconElementName)) {
					throw new ExceptionHandler("Somebody fed us an Icon?");
				}
			}

			return new LabelView(elem);
		}
	}

	static class NoWrapBoxView extends BoxView {
		public NoWrapBoxView(Element elem, int axis) {
			super(elem, axis);
		}

		public void layout(int width, int height) {
			super.layout(32768, height);
		}

		public float getMinimumSpan(int axis) {
			return super.getPreferredSpan(axis);
		}
	}

//	static class MyLabelView extends LabelView
//	{
//		public MyLabelView(Element elem) {
//			super(elem);
//		}
//
//		public float getPreferredSpan(int axis) {
//			if (axis == View.X_AXIS)
//			{
//				TabExpander ex = getTabExpander();
//
//				if (ex == null)
//				{
//					//paragraph implements TabExpander
//					ex =(TabExpander)this.getParent().getParent();
//					getTabbedSpan(0, ex);
//				}
//			}
//
//			return super.getPreferredSpan(axis);
//		}
//	}
}
