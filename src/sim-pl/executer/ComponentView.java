/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import icons.IconManager;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import util.WAction;
import util.graphics.GraphicsExporter;
import about.AboutWindow;
import executer.timetable.TimeTable;

public class ComponentView extends ExecuterDialog {
	public final ComponentPanel cp;

	private static final String ID = "ComponentView";

	final JCheckBoxMenuItem menuItem_ViewValues = new JCheckBoxMenuItem();
	final JCheckBoxMenuItem menuitem_viewLabels = new JCheckBoxMenuItem();
	final JCheckBoxMenuItem menuItem_DrawPins = new JCheckBoxMenuItem();

	final JMenuBar MainMenuBar = new JMenuBar();
	final JMenu menuView = new JMenu();
	final JMenu menuSettings = new JMenu();
	final JMenu menuFile = new JMenu();

	// scrollpane needs componentPanel defined
	public final JScrollPane jScrollPane1;


	final WAction aZoomIn = new WAction(
		"Zoom in",
		IconManager.ZOOM_IN_SMALL,
		IconManager.ZOOM_IN_LARGE,
		"Zoom in",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_I) {
		public void actionPerformed(ActionEvent e) {
			cp.zoom(+1);
		}
	};

	final WAction aZoomOut = new WAction(
		"Zoom out",
		IconManager.ZOOM_OUT_SMALL,
		IconManager.ZOOM_OUT_LARGE,
		"Zoom out",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_MINUS,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_O) {
		public void actionPerformed(ActionEvent e) {
			cp.zoom(-1);
		}
	};

	final WAction aZoom100 = new WAction(
		"Zoom 100%",
		IconManager.ZOOM_100_SMALL,
		IconManager.ZOOM_100_LARGE,
		"Zoom 100%",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_0,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_1) {
		public void actionPerformed(ActionEvent e) {
			cp.setZoom(1);
		}
	};


	final WAction aExport = new WAction(
		   "Export", //"Export Image",
		   true,
		   ExecuterIconManager.FILE_EXPORT_SMALL, ExecuterIconManager.FILE_EXPORT_LARGE,
		   "Export an image of the current file",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		   KeyEvent.VK_E) {
		public void actionPerformed(ActionEvent e) {
			File_ExportImage(e);
		}
	};


	public ComponentView(WorkSheet worksheet, Instance i) {
		super(worksheet, "Component View");
		cp = new ComponentPanel(worksheet, i);
		jScrollPane1 = new JScrollPane(cp);
		this.setContentPane(jScrollPane1);

		setTitle(i.getComponent().getName());
		super.setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));

		menuView.setMnemonic('V');
		menuView.setText("View");

		menuView.add(aZoomIn);
		menuView.add(aZoom100);
		menuView.add(aZoomOut);

		menuItem_ViewValues.setText("View Values");
		menuItem_ViewValues.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setViewValues(menuItem_ViewValues.getState());
			}
		});
		menuitem_viewLabels.setText("View Pin Labels");
		menuitem_viewLabels.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setViewLabels(menuitem_viewLabels.getState());
			}
		});

		menuItem_DrawPins.setText("Show Free Pins");
		menuItem_DrawPins.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDrawPins(menuItem_DrawPins.getState());
			}
		});

		menuSettings.add(menuItem_ViewValues);
		menuSettings.add(menuitem_viewLabels);
		menuSettings.add(menuItem_DrawPins);


		menuItem_ViewValues.doClick(0);
		menuitem_viewLabels.doClick(0);
		menuItem_DrawPins.doClick(0);

		menuSettings.setMnemonic('e');
		menuSettings.setText("Settings");

		menuFile.setMnemonic('F');
		menuFile.setText("File");

		menuFile.add(aExport);

		MainMenuBar.add(menuFile);
		MainMenuBar.add(menuView);
		MainMenuBar.add(menuSettings);

		setJMenuBar(MainMenuBar);
	}

	public ComponentView(WorkSheet workSheet, POD p) {
		this(workSheet, workSheet.tt.getEffector().getInstance());
		cp.setZoom(p.zoom);
		setDrawPins(p.drawPins);
		setViewLabels(p.viewLabels);
		setViewValues(p.viewValues);
	}

	public String getID() { return ID; }


	void setViewValues(boolean b) {
		if (cp.viewValues == b) return;
		cp.viewValues = b;
		menuItem_ViewValues.setState(b);
		cp.repaint();
	}

	void setViewLabels(boolean b) {
		if (cp.viewLabels == b) return;
		cp.setViewLabels(b);
		menuitem_viewLabels.setState(b);
	}

	void setDrawPins(boolean b)
	{
		if (cp.drawPins == b) return;
		cp.drawPins = b;
		this.menuItem_DrawPins.setState(b);
		cp.repaint();
	}

	void File_ExportImage(ActionEvent e)
	{
		GraphicsExporter.doExport(this, cp.getIS(menuitem_viewLabels.isSelected(), menuItem_ViewValues.isSelected()));
	}

	public void cycleLengthChanged(TimeTable tt) { cp.cycleLengthChanged(tt);}
	public void timeTableCleared(TimeTable tt) {cp.timeTableCleared(tt);}
	public void eventSequenceCompleted(TimeTable tt) {cp.eventSequenceCompleted(tt);}
	public void clockFlank(TimeTable tt, boolean rising) {cp.clockFlank(tt, rising);}
	public void valuesChanged(TimeTable tt) {cp.valuesChanged(tt);}
	public void initialisationProgramChanged(TimeTable tt) {cp.initialisationProgramChanged(tt);}


	public void representationChanged() { cp.repaint();}
	public void selectionChanged(Instance instance) { }


	public POD getPOD() {
		return new POD(cp.getZoom(),
					   menuItem_DrawPins.isSelected(),
					   menuitem_viewLabels.isSelected(),
					   menuItem_ViewValues.isSelected());
	}


	public static class POD implements KidPOD {
		public double zoom;
		public boolean drawPins;
		public boolean viewLabels;
		public boolean viewValues;

		public POD(){} // from XML default construction

		public POD(double zoom, boolean drawPins, boolean viewLabels, boolean viewValues) {
			this.zoom = zoom;
			this.drawPins = drawPins;
			this.viewLabels = viewLabels;
			this.viewValues = viewValues;
		}

		public String getID() { return ID; }

		public ComponentView createDialog(WorkSheet w) {
			throw new Error();
		}
	}


}
