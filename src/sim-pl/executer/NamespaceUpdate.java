/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package executer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import nbit.NumberFormat;
import util.ExceptionHandler;

import compiler.wisent.DataSource;
import compiler.wisent.DataSource_Direct;
import compiler.wisent.Namespace;
import compiler.wisent.UndefinedIdentifierException;

import executer.timetable.ContentChangeEvent;
import executer.timetable.TimeTable;

public class NamespaceUpdate
{
	public  final   ContentChangeEvent  cce;
	private final   HashMap<String,DataSource> identifier2datasource = new HashMap<String,DataSource>();

	public NamespaceUpdate(ContentChangeEvent cce) {
		this.cce = cce;
    }

	public String getInstanceString()
	{
		return cce.instance.getHybridName(null);
	}


	public String getUpdateString(NumberFormat representation)
	{
		StringBuffer s = new StringBuffer();

		for (Iterator i = identifier2datasource.keySet().iterator(); i.hasNext(); )
		{
			Object id = i.next();
			s.append(id);
			s.append("=");
			s.append(identifier2datasource.get(id).getValue().toFormattedString(representation));
			if (i.hasNext()) s.append(", ");
		}
		return s.toString();
	}

	public DataSource getDataSource(String identifier) throws UndefinedIdentifierException
	{
		DataSource d = identifier2datasource.get(identifier);
		if (d == null)
		{
			d = cce.instance.getNamespace().get(identifier).createDirectCopy();
	    	identifier2datasource.put(identifier, d);
		}
		return d;
	}

	public void setDataSource(String identifier, compiler.wisent.Instance i) throws UndefinedIdentifierException
	{
		cce.instance.getNamespace().get(identifier); // Make sure namespace contains a mapping for the identifier
		identifier2datasource.put(identifier, new DataSource_Direct(i));
	}


//	public void writeTo(NamespaceUpdate to)
//	{
//		for (Entry<String,DataSource> e : identifier2datasource.entrySet()) {
//			to.identifier2datasource.put(e.getKey(), e.getValue());
//		}
//	}

	public void execute(TimeTable tt, Set<Instance_Wire> wwic, Set<Instance> cwic)
	{
		try
		{
		    Namespace<String, DataSource> oldNamespace = cce.instance.getNamespace();

			for (Entry<String,DataSource> e : identifier2datasource.entrySet()) {
				DataSource oldD = oldNamespace.get(e.getKey());
				DataSource newD = e.getValue();

				oldD.setValue(newD.getValue(), wwic, cwic);
		    }
		}
		catch (UndefinedIdentifierException ex) {throw new ExceptionHandler(ex); };
	}
/*
	public void toXML(XMLWriter xml)
	{
		xml.openTag("NAMESPACEUPDATE");
		xml.addAttribute("TIME",        cce.time);
		xml.addAttribute("INSTANCE",    cce.instance.getFullName());
		xml.endTag();

		for (Iterator i = this.identifier2datasource.keySet().iterator(); i.hasNext(); )
		{
		    Object  id =    i.next();
			Object  value = identifier2datasource.get(id);

			xml.openTag("MAPPING");
			xml.addAttribute("IDENTIFIER", id.toString());
			xml.addAttribute("VALUE", value.toString());
			xml.closeTag();
		}
		xml.closeTag("NAMESPACEUPDATE");
	}

	public static void parse(XMLReader xml, TimeTable tt, executer.Instance instance) throws Exception
	{
		int     time =          Converter.toInteger(xml.atts, "TIME", null);
		String instanceStr =    xml.atts.getNamedItem("INSTANCE").getNodeValue();
		String updateStr =      xml.getText();

		executer.Instance i = instance;

		while (instanceStr != null)
		{
			int index = instanceStr.indexOf('.');
			String firstPart;

			if (index != -1)
			{
				firstPart =     instanceStr.substring(0, index-1);
				instanceStr =   instanceStr.substring(index);
			}
			else
			{
				firstPart =     instanceStr;
				instanceStr =   null;
			}


			if (i.container == null)
			{
				if (firstPart.equals(i.component.getType()))
				{
					// i blijft i;
				}
				else throw new Exception("Component not found: " + firstPart + "\nThis program is probably not created for this component");
			}
			else
			{
				if (i instanceof Simulator.Instance_Complex)
				{
					Simulator.Instance_Complex ic = (Simulator.Instance_Complex) i;
					i = ic.getSubComponentInstance(firstPart);
				}
				throw new Exception("Component not found: " + i.getFullName() + "." + firstPart + "\nThis program is probably not created for this component");
			}
		}

		ContentChangeEvent cce = new ContentChangeEvent(i, time);

		NamespaceUpdate update = tt.getNamespaceUpdate(cce);

		while (xml.childrenLeft())
		{
			String childName = xml.getCurrentChildName();
			if (childName.equals("MAPPING"))
			{
				XMLReader child = xml.nextChild();
				String id =     child.atts.getNamedItem("IDENTIFIER").getNodeValue();
				String value =  child.atts.getNamedItem("VALUE").getNodeValue();

				DataSource d = update.getDataSource(id);
				compiler.wisent.Instance_Simple is = new compiler.wisent.Instance_Simple(((compiler.wisent.Instance_Simple)d.getValue()).type, nbit.nBit.parse_nBit(value));
				d.setValue(is, null);
			}
			else throw new Exception("Expected: MAPPING, found: " + childName);
		}

	}
*/
	public String toString()
	{
		return cce + ": " + identifier2datasource;
	}
}
