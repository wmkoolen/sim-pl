/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import databinding.myStroke;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class myStrokeRenderer implements TableCellRenderer {

	StrokeDisplay sd = new StrokeDisplay();

	public static class StrokeDisplay extends JComponent {
		BasicStroke stroke;

		public StrokeDisplay() {
			setOpaque(true);
		}

		public void setStroke(BasicStroke stroke) {
			this.stroke = stroke;
			setPreferredSize(new Dimension(100, 20+(int)stroke.getLineWidth()));
			repaint();
		}

		public void paintComponent(Graphics legacyg) {
			if (isOpaque()) {
				Rectangle r = legacyg.getClipBounds();
				legacyg.setColor(getBackground());
				legacyg.fillRect(r.x, r.y, r.width, r.height);
			}

			if (stroke != null) {
				Graphics2D g = (Graphics2D)legacyg.create();
				// het getNormalizingTransform dilemma (zie executer.ComponentPanel)
				AffineTransform normTrans = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();
				g.transform(normTrans);

				Point2D p1 = new Point2D.Double(0.10 * getWidth(), 0.5 * getHeight());
				Point2D p2 = new Point2D.Double(0.90 * getWidth(), 0.5 * getHeight());
				try {
					normTrans.inverseTransform(p1, p1);
					normTrans.inverseTransform(p2, p2);
				} catch (Exception ex) {
					throw new Error(ex);
				}

				g.setStroke(stroke);
				g.setColor(Color.black);
				g.draw(new Line2D.Double(p1, p2));
			}
		}
	}


	public static Class getForClass() { return myStroke.class; }


	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		sd.setStroke(value == null ? null : (BasicStroke)((myStroke)value).getStroke());
		sd.setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
		sd.setForeground(isSelected ? table.getSelectionForeground() : table.getForeground());
		return sd;
	}
}
