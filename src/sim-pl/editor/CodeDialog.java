/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import log.Log_Dialog;
import log.Logger;

import compiler.LocatedException;
import compiler.TokenList;
import compiler.wisent.Statement;
import compiler.wisent.Type_Struct;
import compiler.wisent.Wisent;


public class CodeDialog extends JDialog
{
	Type_Struct         type;

	JTextArea           ta =                new JTextArea(25, 80);
	JScrollPane         scroll = new        JScrollPane();
//	CodeBlock           returnCodeBlock;
	String              returnCodeBlock;
	JPanel              jPanel1 = new JPanel();
	JButton bOK  =          new JButton("OK");
	JButton bParseLogged =  new JButton("Parse Logged");
	JButton bCancel  =      new JButton("Cancel");
    JPanel jPanel2 =        new JPanel();
    BorderLayout borderLayout1 = new BorderLayout();
    JPanel jPanel3 =            new JPanel();
    JLabel lcursorPosition =            new JLabel();
    BorderLayout borderLayout2 = new BorderLayout();
    JToolBar jToolBar1 = new JToolBar();

	public CodeDialog()
	{
		super((Frame)null, "New Code", true);

		jbInit();
		pack();
	}

	AbstractAction aCancel  = new AbstractAction("Cancel") {
		public void actionPerformed(ActionEvent e) {
			setVisible(false);
		}
	};



	public void jbInit()
	{
		util.DialogHelper.performOnEscapeKey(this, aCancel);

		this.getContentPane().setLayout(new BorderLayout());
		jPanel2.setLayout(borderLayout1);
        jPanel3.setLayout(borderLayout2);

		ta.addCaretListener(new CaretListener()
		{
			public void caretUpdate(CaretEvent e)
			{
				int line = 1;
				int col = 0;
				String  text =  ta.getText();
				int     pos =   e.getDot();
				for (int i = 0; i < pos; i++)
				{
					if (text.charAt(i) == '\n') {line++; col = 0;}
					else                        col++;
				}
				lcursorPosition.setText("line " + line + ", col " + col + " ");
			}
		});

        lcursorPosition.setText("CursorPosition");
        jToolBar1.setFloatable(false);
        this.getContentPane().add(jPanel2,  BorderLayout.CENTER);
		jPanel2.add(scroll,  BorderLayout.CENTER);

		scroll.getViewport().add(ta);

        jPanel1.add(jToolBar1, null);
        jToolBar1.add(bOK, null);
        jToolBar1.add(bParseLogged, null);
        jToolBar1.add(bCancel, null);

        jPanel2.add(jPanel3,  BorderLayout.SOUTH);
        jPanel3.add(lcursorPosition,  BorderLayout.EAST);
        jPanel3.add(jPanel1, BorderLayout.CENTER);
		ta.setTabSize(4);
		ta.setFont(new Font("Monospaced", Font.PLAIN, 12).deriveFont(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform()));
//		ta.setFont(new java.awt.Font("Monospaced", 0, 12));

		getRootPane().setDefaultButton(bOK);

		this.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				aCancel.actionPerformed(null);
				super.windowClosing(e);
			}
		});



		bOK.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
//				try
//				{
//					Wisent.compiler.parse(type, ta.getText());
					returnCodeBlock = ta.getText();
//					returnCodeBlock = new CodeBlock(parent, ta.getText());
					setVisible(false);
//				}
//				catch (compiler.LocatedException ex)
//				{
//					ExceptionHandler.handleException(ex);
//					ta.setCaretPosition(ex.pl.index);
//					ta.requestFocus();
//				}
//				catch (Exception ex)
//				{
//					ExceptionHandler.handleException(ex);
//				}
			}
		});

//		final CodeDialog This = this;

		bParseLogged.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Log_Dialog  log =       new Log_Dialog(CodeDialog.this, "Parse Log", null);
				Logger      logger =    new Logger(log);

				try
				{
					TokenList tl;
					Statement s;

					try
					{
					    logger.addDoing("Tokenizing");
					    tl = Wisent.compiler.tokenize(ta.getText());
					    logger.addResult("OK");
					}
					catch (LocatedException ex)
					{
						logger.addResult("FAILED");
						logger.addLine(ex.getMessage());
						throw ex;
					}

					logger.addLine("-------- Tokenlist dump -------- ");
    				logger.addLine(tl.toString());


					try
					{
					    logger.addDoing("Parsing");
						s = Wisent.compiler.parse(type, tl);
						logger.addResult("OK");
					}
					catch (LocatedException ex)
					{
						logger.addResult("FAILED");
						logger.addLine(ex.getMessage());
						throw ex;
					}

					logger.addLine("-------- Parse dump -------- ");
	   				logger.addLine(s.toString());

					logger.addLine("Parse completed successfully");
				}
				catch (LocatedException ex)
				{
					logger.addLine("Parse failed");
					// gracefully handle errors at EOF
					ta.setCaretPosition( ex.pl == null
										 ? ta.getText().length()
										 : ex.pl.index);
					ta.requestFocus();
				}
				finally
				{
					log.setVisible(true);
				}
			}
		});


		bCancel.setAction(aCancel);
	}

	public String edit(String text, Type_Struct type)
	{
//		try
//		{
//			parent = old.parent;
			this.type = type; //CodeBlock.getType(old.getComponent());

		    returnCodeBlock = null;

		    ta.setText(text);
		    setLocationRelativeTo(this.getOwner());

		    super.setVisible(true);
		    return returnCodeBlock;
//		}
//		catch (Exception ex)
//		{
//			ExceptionHandler.handleException(ex);
//			return null;
//		}

	}

}
