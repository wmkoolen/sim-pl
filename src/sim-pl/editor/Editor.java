/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.BeanInfo;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.undo.UndoManager;

import namespace.ComponentTable;
import namespace.ComponentTable.ComponentFileChangeListener;
import namespace.ComponentTable.ComponentFileChangedEvent;
import util.CurrentDirectoryManager;
import util.FileManager;
import util.ScreenShot;
import util.Terminator;
import util.WAction;
import util.graphics.GraphicsExporter;
import util.graphics.ImageOutputter;
import about.AboutWindow;
import about.LogoPanel;
import databinding.BasicEditorNode;
import databinding.ColorConverter;
import databinding.Complex;
import databinding.Component;
import databinding.Flip;
import databinding.Input;
import databinding.InternalNode;
import databinding.Internals;
import databinding.Output;
import databinding.ParameterDecl;
import databinding.Simple;
import databinding.SolidFill;
import databinding.Storage;
import databinding.SubComponent;
import databinding.myStroke;
import databinding.forms.Ellipse;
import databinding.forms.HAlignment;
import databinding.forms.Line;
import databinding.forms.Polygon;
import databinding.forms.Text;
import databinding.forms.VAlignment;
import databinding.wires.Probe;
import databinding.wires.Wires;
import editor.EditorDocumentContext.EditorEvent;
import editor.EditorDocumentContext.EditorListener;
import editor.FTEditor.FontText;
import editor.tool.ActionDialog;
import editor.tool.CurrentProperties;
import editor.tool.EllipseTool;
import editor.tool.LineTool;
import editor.tool.ParameterDialog;
import editor.tool.PinTool;
import editor.tool.PolygonTool;
import editor.tool.ProbeTool;
import editor.tool.RectangleTool;
import editor.tool.Select;
import editor.tool.StorageDialog;
import editor.tool.SubComponentTool;
import editor.tool.TextTool;
import editor.tool.Tool;
import editor.tool.WireTool;
import executer.MMA_Application;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Editor extends MMA_Application {
	public final static String baseTitle = "SIM-PL Editor";

	public String getProgramName() { return "Editor"; }

	final WAction aNew = new WAction(
		"New ...",
		EditorIconManager.FILE_NEW_SMALL,
		EditorIconManager.FILE_NEW_LARGE,
		"Create a new, empty component",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK),
		KeyEvent.VK_N) {
		public void actionPerformed(ActionEvent e) {
			NewComponentDialog ncd = new NewComponentDialog(Editor.this);
			if (ncd.ask()) {
				try {
					Component c = ncd.isSimple()
						? new Simple(ncd.getNewName())
						: new Complex(ncd.getNewName());
					addDC(c, null);
					assert c.isValidRecursively();
				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(Editor.this, "Error", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	};


	final WAction aOpen = new WAction(
		"Open ...",
		EditorIconManager.FILE_OPEN_SMALL,
		EditorIconManager.FILE_OPEN_LARGE,
		"Edit an existing component",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK),
		KeyEvent.VK_O) {
		public void actionPerformed(ActionEvent e) {
			File_Open();
		}
	};

	final WAction aSave = new WAction(
		"Save",
		EditorIconManager.FILE_SAVE_SMALL,
		EditorIconManager.FILE_SAVE_LARGE,
		"Save the current component",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		KeyEvent.VK_S) {
		public void actionPerformed(ActionEvent e) {
			File_Save();
		}
	};

	final WAction aSaveAs = new WAction(
		"Save as ...",
		EditorIconManager.FILE_SAVEAS_SMALL,
		EditorIconManager.FILE_SAVEAS_LARGE,
		"Save the current component under a new name",
		null,
		null,
		KeyEvent.VK_V) {
		public void actionPerformed(ActionEvent e) {
			File suggestedFile = currentDC.currentFile;

			FileManager fm = FileManager.getFileManager();
			File saveTo;
			do {
				saveTo = fm.askForFile(Editor.this, "Save Component File As", suggestedFile, "Save", FileManager.filter_sim_pl, FileManager.filter_xml);
				if (saveTo == null) return;
			} while (saveTo.exists() && JOptionPane.showConfirmDialog(Editor.this,
					"File exists.\nOverwrite?", "Overwrite question",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) !=
					JOptionPane.YES_OPTION);
			try
			{
				saveTo = saveTo.getCanonicalFile();
				// we need TWO copies here
				// one copy becomes the cached reference version of the file
				// the second copy becomes the edit instance
				Component copy = copy(ComponentTable.write(saveTo, copy(currentDC.edc.node)));
				addDC(copy, saveTo);
			} catch (Exception ex)  {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(Editor.this, ex.getMessage(), "Error saving to " + saveTo, JOptionPane.ERROR_MESSAGE);
			}
		}
	};



	Component copy(Component c) {
		assert c != null;
		try {
			return ComponentTable.expensiveCopy(c);
		} catch (Exception ex) {
			throw new Error(ex);
		}
	}


	final WAction aExport = new WAction(
		   "Export", //"Export Image",
		   true,
		   IconManager.FILE_EXPORT_SMALL,
		   IconManager.FILE_EXPORT_LARGE,
		   "Export an image of the current file",
		   null,
		   null, //KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK),
		   KeyEvent.VK_E) {
		public void actionPerformed(ActionEvent e) {
			GraphicsExporter.doExport(Editor.this, new ImageOutputter(currentDC.edc.getRoot(), currentDC.edc.getCompiledComponent(), true, true));
		}
	};


	final WAction aClose = new WAction(
		"Close",
		EditorIconManager.FILE_CLOSE_SMALL,
		EditorIconManager.FILE_CLOSE_LARGE,
		"Close the current component",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK),
		KeyEvent.VK_C) {
		public void actionPerformed(ActionEvent e) {
			File_Close();
		}
	};

	final WAction aExit = new WAction(
		"Exit",
		EditorIconManager.EXIT_SMALL, EditorIconManager.EXIT_LARGE,
		"Closes all components and exits",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK),
		KeyEvent.VK_X) {
		public void actionPerformed(ActionEvent e) {
			File_Exit();
		}
	};



	final WAction aUndo = new WAction(
		"Undo",
		IconManager.UNDO_SMALL, IconManager.UNDO_LARGE,
		"Undo the last edit",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK),
		KeyEvent.VK_U) {
		public void actionPerformed(ActionEvent e) {
			currentDC.edc.undo();
			currentDC.cp.doToolReset();
		}
	};

	final WAction aRedo = new WAction(
		"Redo",
		IconManager.REDO_SMALL, IconManager.REDO_LARGE,
		"Redo the last undone edit",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_Z,
							   InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK),
		KeyEvent.VK_R) {
		public void actionPerformed(ActionEvent e) {
			currentDC.edc.redo();
			currentDC.cp.doToolReset();
		}
	};


	final WAction aZoomIn = new WAction(
		"Zoom in",
		EditorIconManager.ZOOM_IN_SMALL,
		EditorIconManager.ZOOM_IN_LARGE,
		"Zoom in",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_I) {
		public void actionPerformed(ActionEvent e) {
			currentDC.cp.zoom(+1);
		}
	};

	final WAction aZoomOut = new WAction(
		"Zoom out",
		EditorIconManager.ZOOM_OUT_SMALL,
		EditorIconManager.ZOOM_OUT_LARGE,
		"Zoom out",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_MINUS,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_O) {
		public void actionPerformed(ActionEvent e) {
			currentDC.cp.zoom(-1);
		}
	};

	final WAction aZoom100 = new WAction(
		"Zoom 100%",
		EditorIconManager.ZOOM_100_SMALL,
		EditorIconManager.ZOOM_100_LARGE,
		"Zoom 100%",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_0,
							   InputEvent.CTRL_MASK),
		KeyEvent.VK_1) {
		public void actionPerformed(ActionEvent e) {
			currentDC.cp.setZoom(1);
		}
	};


	final WAction aDelete = new WAction(
		"Delete",
		EditorIconManager.DELETE_SMALL,
		EditorIconManager.DELETE_LARGE,
		"Delete the current selection",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0),
		KeyEvent.VK_D) {

		public void actionPerformed(ActionEvent e) {
			currentDC.edc.deleteSelection();
			currentDC.cp.doToolReset();
		}
	};



	final WAction aGrid5 = new WAction(
		"Fine grid",
		EditorIconManager.GRID_FINE_SMALL,
		EditorIconManager.GRID_FINE_LARGE,
		"Grid 5",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK),
		KeyEvent.VK_F) {
		public void actionPerformed(ActionEvent e) {
			currentDC.cp.setGridSize(5);
		}
	};

	final WAction aGrid10 = new WAction(
		"Course grid",
		EditorIconManager.GRID_COURSE_SMALL,
		EditorIconManager.GRID_COURSE_LARGE,
		"Grid 10",
		null,
		KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK),
		KeyEvent.VK_C) {
		public void actionPerformed(ActionEvent e) {
			currentDC.cp.setGridSize(10);
		}
	};



	public Editor() throws Exception {
		jbInit();
	}

	public static void main(final String [] args) throws Exception {
	        Thread.setDefaultUncaughtExceptionHandler(new Terminator());
		final Editor ed = new Editor();

		ed.pack(); // need a realised frame for successful maximisation

		ed.setSize(1000,800);
		ed.setLocationRelativeTo(ed.getOwner());

		// nice if maximisation can be done
		if (ed.getToolkit().isFrameStateSupported(JFrame.MAXIMIZED_BOTH)) {
			ed.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
		ed.setVisible(true);


		if (args.length > 0) {
			File[] fs = new File[args.length];
			for (int i = 0; i < args.length; i++) {
				fs[i] = new File(args[i]);
			}

			ed.loadFiles(fs);
		}
	}


	StatusBar statusBar = new StatusBar();

	JPanel fillTree = new JPanel();
	JPanel fillPropTable = new JPanel();
	JPanel fillMessageTable = new JPanel();

	CodeEditor ce = new CodeEditor();

	LogoPanel fillcp = new LogoPanel();


	public static class StatusBar extends JPanel  {
		JLabel text1 = new JLabel();
		JLabel text2 = new JLabel();

		public StatusBar() {
			text1.setBorder(BorderFactory.createLoweredBevelBorder());
			text2.setBorder(BorderFactory.createLoweredBevelBorder());
			setLayout(new BorderLayout());
			add(text1, BorderLayout.EAST);
			add(text2, BorderLayout.CENTER);
			new Timer().schedule(new TimerTask() {
				public void run() {
					Runtime r = Runtime.getRuntime();
					final int M = 1024*1024;

					text1.setText(//r.availableProcessors() + " P, "
								+ r.totalMemory()/M + " MB total, "
								+ r.freeMemory()/M + " MB free, " +
								+ r.maxMemory()/M + " MB max");
				}
			}, 0, 1000);

			text1.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					Runtime.getRuntime().gc();
				}
			});
		}


		public void setText(String text) {
			text2.setText(text);
		}
	}




	Color disabledColor = Color.lightGray;


	class ToolActionListener implements ActionListener {
		Tool t;
		public ToolActionListener(Tool t) {
			this.t = t;
		}

		public void actionPerformed(ActionEvent e) {
			setTool(t);
		}
	}


	private void jbInit() throws Exception {

		setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));

		// set up print screen action
		ScreenShot.installHook(getRootPane());


		for (JPanel p : new JPanel [] {fillTree, fillPropTable, fillMessageTable, fillcp} ) {
			p.setBackground(disabledColor);
			p.setEnabled(false);
		}

		// we use window listener to exit
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener( new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				File_Exit();
			}
		});


		setTitle(baseTitle);
		jSplitPane1.setResizeWeight(0.20);
		jSplitPane1.setOneTouchExpandable(true);
		jSplitEditMessage.setOneTouchExpandable(true);
		jSplitTreeProp.setOneTouchExpandable(true);
		this.setJMenuBar(jMenuBar1);
		this.getContentPane().setLayout(borderLayout1);


		JMenu jMenu1 = new JMenu();
		jMenu1.setText("File");
		  jMenu1.add(aNew);
		  jMenu1.add(aOpen);
		  jMenu1.add(aSave);
		  jMenu1.add(aSaveAs);
		  jMenu1.add(aExport);
		  jMenu1.add(aClose);
		  jMenu1.addSeparator();
		  jMenu1.add(aExit);
		jMenuBar1.add(jMenu1);

		JMenu jMenu2 = new JMenu();
		jMenu2.setText("Edit");
		  jMenu2.add(aUndo);
		  jMenu2.add(aRedo);
		  jMenu2.addSeparator();
		  jMenu2.add(aDelete);
		jMenuBar1.add(jMenu2);

		JMenu jMenu4 = new JMenu();
		jMenu4.setText("Settings");
		  jMenu4.add(aGrid10);
		  jMenu4.add(aGrid5);
		jMenuBar1.add(jMenu4);

		JMenu jMenu3 = new JMenu();
		jMenu3.setText("View");
		  jMenu3.add(aZoomIn);
		  jMenu3.add(aZoom100);
		  jMenu3.add(aZoomOut);
		jMenuBar1.add(jMenu3);

		JMenu jMenu5 = new JMenu("Help");
		   jMenu5.add(aAbout);
		   jMenu5.add(aWebPage);
		jMenuBar1.add(jMenu5);


		jSplitTreeProp.setOrientation(JSplitPane.VERTICAL_SPLIT);
		jSplitTreeProp.setResizeWeight(0.5);
		jSplitEditMessage.setOrientation(JSplitPane.VERTICAL_SPLIT);
		jSplitEditMessage.setResizeWeight(0.8);

		jSplitTreeProp.setTopComponent(fillTree);
		jSplitTreeProp.setBottomComponent(fillPropTable);
		jSplitEditMessage.setBottomComponent(fillMessageTable);
		jSplitEditMessage.setTopComponent(fillcp);

		final int iconType = BeanInfo.ICON_COLOR_32x32;
		jButton1.setIcon(IconManager.chargeIcon(Polygon.class, iconType));
		jButton2.setIcon(IconManager.chargeIcon(Output.class, iconType));
		jButton4.setIcon(IconManager.chargeIcon(Input.class, iconType));
		jButton5.setIcon(IconManager.chargeIcon(Ellipse.class, iconType));
		jButton7.setIcon(IconManager.chargeIcon(Storage.class, iconType));
		jButton8.setIcon(IconManager.chargeIcon(databinding.Action.class, iconType));
		jButton9.setIcon(IconManager.chargeIcon(Text.class, iconType));
		jButton10.setIcon(IconManager.chargeIcon(SubComponent.class, iconType));
		jButton11.setIcon(IconManager.chargeIcon(databinding.forms.RoundRect.class, iconType));
		jButton12.setIcon(IconManager.chargeIcon(Line.class, iconType));
		jButton14.setIcon(IconManager.chargeIcon(ParameterDecl.class, iconType));
		jButtonExtra.setIcon(IconManager.chargeIcon(Wires.class, iconType));
		jButtonProbe.setIcon(IconManager.chargeIcon(Probe.class, iconType));
		jbSelector.setIcon(EditorIconManager.SELECT_LARGE);


		jButton1.setToolTipText("Draw a polygon");
		jButton2.setToolTipText("Add an output");
		jButton4.setToolTipText("Add an input");
		jButton5.setToolTipText("Draw an ellipse");
		jButton7.setToolTipText("Add a memory");
		jButton8.setToolTipText("Program an event");
		jButton9.setToolTipText("Draw text");
		jButton10.setToolTipText("Add a subcomponent");
		jButton11.setToolTipText("Draw a rectangle");
		jButton12.setToolTipText("Draw a line");
		jButton14.setToolTipText("Add a parameter");
		jButtonExtra.setToolTipText("Add wires");
		jButtonProbe.setToolTipText("Add probes");
		jbSelector.setToolTipText("Selector");



		jbSelector.addActionListener(new ToolActionListener(new Select()));
		jButton11.addActionListener(new ToolActionListener(new RectangleTool()));
		jButton5.addActionListener(new ToolActionListener(new EllipseTool()));
		jButton1.addActionListener(new ToolActionListener(new PolygonTool()));
		jButton12.addActionListener(new ToolActionListener(new LineTool()));
		jButton9.addActionListener(new ToolActionListener(new TextTool()));
		jButton4.addActionListener(new ToolActionListener(new PinTool(true)));
		jButton2.addActionListener(new ToolActionListener(new PinTool(false)));
		jButton10.addActionListener(new ToolActionListener(new SubComponentTool()));


		jButtonExtra.addActionListener(new ToolActionListener(new WireTool()));
		jButtonProbe.addActionListener(new ToolActionListener(new ProbeTool()));

		jButton14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ParameterDialog pd = new ParameterDialog(Editor.this);
				if (pd.ask()) {
					currentDC.edc.addNodeUndoable(currentDC.edc.getRoot().getParameters(), new ParameterDecl(pd.getNewName(), pd.getDefaultValue()), true);
				}
			}
		});

	    jButton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CurrentProperties cp = CurrentProperties.getProps();
				StorageDialog pd = new StorageDialog(Editor.this);
				pd.setBits(cp.getBits());
				pd.setSigned(cp.isSigned());
				pd.setArraySize(cp.getSize());
				if (pd.ask()) {
					cp.setBits(pd.getBits());
					cp.setSigned(pd.isSigned());
					cp.setSize(pd.getArraySize());

					currentDC.edc.addNodeUndoable(((Simple)currentDC.edc.getRoot()).getMemory(), new Storage(pd.getNewName(), pd.getBits(), pd.getArraySize(), pd.isSigned()), true);
				}
			}
		});

		jButton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Internals intnls = ((Simple)currentDC.edc.getRoot()).getInternals();

				try {
					ActionDialog pd = new ActionDialog(Editor.this);
					if (pd.ask(intnls)) {
						currentDC.edc.addNodeUndoable(intnls,
							new databinding.Action(pd.getEvent(), pd.getCode()), true);
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(Editor.this, ex.getMessage(), "Error adding events", JOptionPane.ERROR_MESSAGE);
				}
			}
		});


		final AbstractButton [] buttons = {
			jbSelector,
			jButton11,
			jButton5,
			jButton1,
			jButton9,
			jButton12,
			jButton14,
			jButton4,
//			jButton2, // not enough F-keys (we can create outputs via inputs)
			jButton7,
			jButton8,
			jButton10,
			jButtonExtra,
		    jButtonProbe};
		JComponent pane = (JComponent)getContentPane();
		InputMap imap = pane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		ActionMap amap = pane.getActionMap();

		for (int i = 0; i < buttons.length; i++) {
			String toolStr = "Tool #"+i;
			imap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F1+i, 0), toolStr);
			final AbstractButton b = buttons[i];
			amap.put(toolStr, new AbstractAction() {
				public void actionPerformed(ActionEvent e) {
					b.doClick(0);
				}
			});
		}



		bg.add(jButton1);
		bg.add(jButton2);
		bg.add(jButton4);
		bg.add(jButton5);
		bg.add(jButton9);
		bg.add(jButton10);
		bg.add(jButton11);
		bg.add(jButton12);
		bg.add(jbSelector);
		bg.add(jButtonExtra);
		bg.add(jButtonProbe);
		jSplitPane1.setLeftComponent(jSplitTreeProp);
		jSplitPane1.setRightComponent(jSplitEditMessage);



		this.getContentPane().add(jToolBar1, java.awt.BorderLayout.NORTH);
		this.getContentPane().add(statusBar, java.awt.BorderLayout.SOUTH);
		// TODO: implement useful hints
//		statusBar.setText("Hints here");

		jToolBar1.add(aNew);
		jToolBar1.add(aOpen);
		jToolBar1.add(aSave);
		jToolBar1.addSeparator();
		jToolBar1.add(jbSelector);
		jToolBar1.addSeparator();
		jToolBar1.add(jButton11);
		jToolBar1.add(jButton5);
		jToolBar1.add(jButton1);
		jToolBar1.add(jButton9);
		jToolBar1.add(jButton12);
		jToolBar1.addSeparator();
		jToolBar1.add(jButton14);
		jToolBar1.addSeparator();
		jToolBar1.add(jButton4);
		jToolBar1.add(jButton2);
		jToolBar1.addSeparator();
		jToolBar1.add(jButton7);
		jToolBar1.add(jButton8);
		jToolBar1.addSeparator();
		jToolBar1.add(jButton10);
		jToolBar1.add(jButtonExtra);
		jToolBar1.add(jButtonProbe);

		this.getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

		jTabbedPane1.addChangeListener(tcl);

		// fake initial tool selection (do this before next line disables jButton13!)
		jbSelector.doClick(0);

		// fake initial tab event to set buttons enabledness etc.
		onTabsChanged();
	}

	ChangeListener tcl = new ChangeListener() {
		public void stateChanged(ChangeEvent e) {
			onTabsChanged();
		}
	};



	Tool   currentTool = null;
	DCView currentDC = null;

	private void setTool(Tool t) {
		if (currentDC != null) {
			assert currentDC.cp.getTool() == currentTool;
			currentDC.cp.setTool(t);
		}
		this.currentTool = t;
	}

	public void refreshTitle() {
		if (currentDC == null) {
			setTitle(baseTitle);
		} else {
			setTitle(baseTitle + " - "
					 + (currentDC.currentFile == null ? "Untitled" : currentDC.currentFile.getAbsolutePath())
					 + (currentDC.edc.hasChanged() ? "*" : ""));
		}
	}

	private void onTabsChanged() {
		assert tabsSorted();

		// remove listener from previous DC
		if (currentDC != null) {
			currentDC.edc.getEDM().removeListener(undoStateHandler);
			assert currentDC.cp.getTool() == currentTool;
			currentDC.cp.setTool(null);
		}

		currentDC = (DCView)jTabbedPane1.getSelectedComponent();

		refreshTitle();
		if (currentDC == null) { // no open tabs
			ce.setDocumentContext(null);
//			setTitle(baseTitle);
			replaceSplit(jSplitTreeProp, fillTree, fillPropTable);
			replaceSplit(jSplitEditMessage, fillcp, fillMessageTable);
			updateUndoState(null);
		} else {
			ce.setDocumentContext(currentDC.edc);
//			setTitle(baseTitle + " - " + ComponentTable.reverseLookup(currentDC.edc.node));
			replaceSplit(jSplitTreeProp, currentDC.jScrollTree,
						 currentDC.jScrollProperty);
			replaceSplit(jSplitEditMessage, jTabbedPane1,
						 currentDC.jScrollMessage);
			currentDC.edc.getEDM().addListener(undoStateHandler);
			updateUndoState(currentDC.edc.getUndoManager());

			// check if tool still OK
			if (!currentTool.canHandle(currentDC.edc.getRoot())) {
				currentTool = null;
				jbSelector.doClick(); // sets tool on currentDC
			} else {
				currentDC.cp.setTool(currentTool);
			}
		}

		for (WAction a : new WAction [] {aClose, aExport, aSave, aSaveAs, aDelete, aZoomIn, aZoom100, aZoomOut, aGrid10, aGrid5}) {
			a.setEnabled(currentDC != null);
		}
		jButton1.setEnabled(currentDC != null);
		jButton2.setEnabled(currentDC != null);
		jButton4.setEnabled(currentDC != null);
		jButton5.setEnabled(currentDC != null);
		jButton7.setEnabled(currentDC != null && currentDC.edc.node instanceof Simple);
		jButton8.setEnabled(currentDC != null && currentDC.edc.node instanceof Simple);
		jButton9.setEnabled(currentDC != null);
		jButton10.setEnabled(currentDC != null && currentDC.edc.node instanceof Complex);
		jButtonExtra.setEnabled(currentDC != null && currentDC.edc.node instanceof Complex);
		jButtonProbe.setEnabled(currentDC != null && currentDC.edc.node instanceof Complex);
		jButton11.setEnabled(currentDC != null);
		jButton12.setEnabled(currentDC != null);
		jbSelector.setEnabled(currentDC != null);
		jButton14.setEnabled(currentDC != null);
	}

	JSplitPane jSplitPane1 = new JSplitPane();
	JSplitPane jSplitTreeProp = new JSplitPane();
	JSplitPane jSplitEditMessage = new JSplitPane();

	JTabbedPane jTabbedPane1 = new JTabbedPane();
	JMenuBar jMenuBar1 = new JMenuBar();


	JFileChooser fc;
	BorderLayout borderLayout1 = new BorderLayout();
	JToolBar jToolBar1 = new JToolBar() {
		public JButton add(javax.swing.Action a) {
			JButton b = super.add(a);
			if (a instanceof WAction) {
				WAction wa = (WAction)a;
				b.setIcon((Icon)wa.getValue(WAction.LARGE_ICON));
			}
			return b;
		}
	};

	JToggleButton jButton1 = new JToggleButton();
	JToggleButton jButton2 = new JToggleButton();
	JToggleButton jButton4 = new JToggleButton();
	JToggleButton jButton5 = new JToggleButton();
	JButton jButton7 = new JButton();
	JButton jButton8 = new JButton();
	JToggleButton jButton9 = new JToggleButton();
	JToggleButton jButtonExtra = new JToggleButton();
	JToggleButton jButtonProbe = new JToggleButton();
	JToggleButton jButton10 = new JToggleButton();
	JToggleButton jButton11 = new JToggleButton();
	JToggleButton jButton12 = new JToggleButton();
	JToggleButton jbSelector = new JToggleButton();
	JButton jButton14 = new JButton();
	ButtonGroup bg = new ButtonGroup();


	class FileOpenThread extends Thread {
		final File [] fs;

		public FileOpenThread(Collection<File> fs) {
			this.fs = fs.toArray(new File[fs.size()]); // make a copy, we work asynchronously
		}


		public void run() {
			// TODO: present a global error summary at the end
			// instead of a per-error popup
			ProgressMonitor pm = new ProgressMonitor(Editor.this, "Loading " + (fs.length > 1 ? "files" : "file"),  "", 0, fs.length);
			for (int i = 0; i < fs.length && !pm.isCanceled(); i++) {
				pm.setProgress(i);
				pm.setNote(fs[i].getName());
				try {
					// do the hard work in this thread
					// ComponentTable is thread-safe
					final File      rootF = fs[i];
					final Component rootC = copy(ComponentTable.load(rootF));


					// leave not-so-hard but race-prone work to AWT event thread
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							addDC(rootC, rootF);
						}
					});
				} catch (Exception ex) {
					// JOptionPane nicely blocks current thread
					JOptionPane.showMessageDialog(Editor.this, fs[i].getName() + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					ex.printStackTrace();
				}
			}
			pm.setProgress(fs.length); // this closes the progress meter
		}

	}

	public void File_Open() {
		// we go beyond FileManager by allowing multiple files to be opened
		if (fc == null) {
			fc = new JFileChooser(System.getProperty("user.dir"));
			fc.addChoosableFileFilter(FileManager.filter_anycmp);
			fc.addChoosableFileFilter(FileManager.filter_sim_pl);
			fc.addChoosableFileFilter(FileManager.filter_xml);
			fc.setFileFilter(FileManager.filter_anycmp);
			fc.setMultiSelectionEnabled(true); // crucial
		}

		switch (CurrentDirectoryManager.wrapOpen(fc, this)) {
			case JFileChooser.APPROVE_OPTION:
				// I checked that fc.getSelectedFiles() returns a COPY
				loadFiles(fc.getSelectedFiles());
				break;
		}
	}

	protected void loadFiles(File ... fs) {
		ArrayList<File> newFiles = new ArrayList<File>();
		ArrayList<DCView> oldViews = new ArrayList<DCView>();
		for (File ncf : fs) {
			File f = getCanonicalFile(ncf);
			DCView dcv = getViewFor(f);
			if (dcv == null) newFiles.add(f);
			else             oldViews.add(dcv);
		}

		if (newFiles.isEmpty()) {
			if (!oldViews.isEmpty()) selectDC(oldViews.get(0));
		} else {
			new FileOpenThread(newFiles).start();
		}
	}

	/** Save the current component in a file
	 * @param f File null to save to old file, new name to save to new file
	 */
	public void File_Save() {
		Component cur = currentDC.edc.node;
		File f = currentDC.currentFile;

		if (f == null) {
			 f = new File(cur.getName());
			 FileManager fm = FileManager.getFileManager();
			 do {
				 f = fm.askForFile(Editor.this, "Save Component File As",  f, "Save", FileManager.filter_sim_pl, FileManager.filter_xml);
				 if (f == null) return;
			 } while  (f.exists() && JOptionPane.showConfirmDialog(Editor.this, "File exists.\nOverwrite?",
												   "Overwrite question",
												   JOptionPane.YES_NO_OPTION,
												   JOptionPane.QUESTION_MESSAGE) !=
					 JOptionPane.YES_OPTION);
		}

		try {
			// save a COPY, we want the cached version to be different from the
			// current, because we are editing the current version.
			File cf = f.getCanonicalFile();
			ComponentTable.write(f, copy(cur));
			currentDC.setCurrentFile(cf);
			currentDC.edc.mark();
			assert !currentDC.edc.hasChanged();
			refreshTitle();
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Error saving " + f, JOptionPane.ERROR_MESSAGE);
		}
	}




	// return false if user cancels
	public boolean File_Close() {
		assert jTabbedPane1.getComponentCount() > 0;
		DCView dcv = (DCView)jTabbedPane1.getSelectedComponent();

		if (!dcv.edc.hasChanged()) {
			removeDC(dcv);
			return true;
		} else {
			String msg = dcv.currentFile != null
				? "File " + dcv.currentFile.getName() + " changed, save first?"
				: "Component was modified, save first?";

			switch (JOptionPane.showConfirmDialog(this,
												  msg,
												  "Close modified component",
												  JOptionPane.
												  YES_NO_CANCEL_OPTION,
												  JOptionPane.QUESTION_MESSAGE)) {
				case JOptionPane.YES_OPTION:
					File_Save();
					removeDC(dcv);
					return true;

				case JOptionPane.NO_OPTION:
					removeDC(dcv);
					return true;

				case JOptionPane.CLOSED_OPTION:
				case JOptionPane.CANCEL_OPTION:
					return false;
				default:
					throw new Error("Invalid option");
			}
		}
	}

	public void File_Exit() {
		while (jTabbedPane1.getTabCount() > 0) {
			if (!File_Close()) return; // user cancels
		}
//		for (java.awt.Component c: jTabbedPane1.getComponents()) {
//			DCView dcv = (DCView)c;
//			removeDC(dcv);
//			// TODO: ask on changes, abort when 'cancel' selected
//		}
		assert jTabbedPane1.getTabCount() == 0;
		dispose();
		System.exit(0);
	}


//	private void loadFile(File f) throws Exception {
//		Component root = ComponentTable.load(f);
//		addComponent(root);
//	}

	static File getCanonicalFile(File f) {
		try { return f.getCanonicalFile(); }
		catch (Exception ex) { throw new Error(ex); }
	}

	private DCView getViewFor(File f) {
		assert f.equals(getCanonicalFile(f)) : f + " vs " + getCanonicalFile(f);
		for (DCView v : views) {
			if (f.equals(v.currentFile)) return v;
		}
		return null;
	}
	private DCView getViewFor(Component c) {
		for (DCView v : views) {
			if (v.edc.getRoot() == c)  return v;
		}
		return null;
	}



	public void open(SubComponent sc) {
		// it is possible that the user double-clicks a non-resolved component.
		// in that case, we try to re-load it from disk

		if (!sc.isResolved()) {
			try {
				ComponentTable.load(sc.getData());
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage(), "Load error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		assert sc.isResolved();
		// TODO: get canonical file earlier (like in SubComponent setData)
		DCView view = getViewFor(getCanonicalFile(sc.getData()));
		if (view == null) {
			loadFiles(sc.getData());
		} else {
			selectDC(view);
		}
	}




	static class StringConstructorEditor extends DefaultCellEditor {
		Constructor cons;
		Method valueOf;
		Object value;

		public StringConstructorEditor(Class c) {
			super(new JTextField());
			try {
				cons = c.getConstructor(String.class);
			} catch (NoSuchMethodException ex) {
				try {
					valueOf = c.getMethod("valueOf", String.class);
					assert (valueOf.getModifiers() & Modifier.STATIC) == Modifier.STATIC;
				} catch (NoSuchMethodException nex) {
					throw new Error(nex);
				}
			}
		}

		public boolean stopCellEditing() {
		 String s = (String)super.getCellEditorValue();
		 // Here we are dealing with the case where a user
		 // has deleted the string value in a cell, possibly
		 // after a failed validation. Return null, so that
		 // they have the option to replace the value with
		 // null or use escape to restore the original.
		 // For Strings, return "" for backward compatibility.
//		 if ("".equals(s)) {
//			 if (cons.getDeclaringClass() == String.class) {
//				 value = s;
//			 }
//			 super.stopCellEditing();
//		 }

		 try {
			 value =  (cons != null)
				 ? cons.newInstance(s)
				 : valueOf.invoke(null, s);
		 }
		 catch (Exception e) {
			 ((JComponent)getComponent()).setBorder(new LineBorder(Color.red));
			 return false;
		 }
		 return super.stopCellEditing();
	 }

	 public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		 this.value = null;
			 ((JComponent)getComponent()).setBorder(new LineBorder(Color.black));
//		 try {
//			 PropertyTableModel ptm = (PropertyTableModel)table.getModel();
//			 Class type = table.getColumnClass(column);
		 // Since our obligation is to produce a value which is
		 // assignable for the required type it is OK to use the
		 // String constructor for columns which are declared
		 // to contain Objects. A String is an Object.
//		 if (type == Object.class) {
//			 type = String.class;
//		 }
//		 constructor = type.getConstructor(argTypes);
//		 }
//		 catch (Exception e) {
//		 return null;
//		 }
		 return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	 }

	 public Object getCellEditorValue() {
		 return value;
	 }
    }



	// we want to be able to strictly sort DCView objects
	static int IDs = 0;
	public class DCView extends JScrollPane implements ComponentFileChangeListener, Comparable<DCView> {
		// a unique, sortable identifier
		final int id = IDs++;
		// cache the component name, for sorting
		String currentName;

		public int compareTo(DCView v) {
			int i = currentName.compareTo(v.currentName);
			if (i == 0) i = id - v.id;
			return i;
		}


		final EditorDocumentContext edc;
		final ComponentPanel cp;

		final JScrollPane jScrollProperty = new JScrollPane();
		final JScrollPane jScrollTree = new JScrollPane();
		final JScrollPane jScrollMessage = new JScrollPane();

		final JTable jTableMessage;
		final JTable jTableProperty;
		final JTree jTree;

		private File currentFile; // can change, and can be null

		public void setCurrentFile(File f) {
			this.currentFile = f == null
				? null
				: getCanonicalFile(f);
		}


		public String toString() {
			return "DCVIEW " + edc.getRoot().getName();
		}

		public DCView(final EditorDocumentContext edc, File currentFile) {
			this.edc = edc;
			setCurrentFile(currentFile);
			this.cp = new ComponentPanel(edc, Editor.this);
			this.currentName = edc.getRoot().getName();
			setViewportView(cp);

			setWheelScrollingEnabled(true);

			this.jTableMessage = new JTable(edc.getErrorTableModel());
			this.jTableProperty = new JTable(edc.getPropertyTableModel()) {

				public Class toNonPrimitive(Class c) {
					if (!c.isPrimitive()) return c;
					if (c == Integer.TYPE) return Integer.class;
					if (c == Float.TYPE) return Float.class;
					if (c == Double.TYPE) return Double.class;
					if (c == Boolean.TYPE) return Boolean.class;
					throw new Error("Nonconvertible primitive type: " + c);
				}


				public TableCellRenderer getCellRenderer(int row, int col) {
					if (col == 0) return super.getCellRenderer(row, col);
					BeanInfo bi = edc.getPropertyTableModel().bi;
					PropertyDescriptor pd = bi.getPropertyDescriptors()[row];
//					System.out.println("Request cell renderer for " + pd.getPropertyType());
					TableCellRenderer tcr = getDefaultRenderer(toNonPrimitive(pd.getPropertyType()));
					assert tcr != null : "No TableCellRenderer for type " + pd.getPropertyType();
					return tcr;
				}


				private Class lookingForEditorClass;
				public TableCellEditor getDefaultEditor(Class c) {
					if (c == Object.class || c == Number.class) return new StringConstructorEditor(lookingForEditorClass);
					else return super.getDefaultEditor(c);
				}

				public TableCellEditor getCellEditor(int row, int col) {
					assert col == 1;
					BeanInfo bi = edc.getPropertyTableModel().bi;
					PropertyDescriptor pd = bi.getPropertyDescriptors()[row];
					lookingForEditorClass = toNonPrimitive(pd.getPropertyType());
//					System.out.println("Request cell editor for " + lookingForEditorClass);
					return getDefaultEditor(lookingForEditorClass);
//
//					TableCellEditor tce = getDefaultEditor(normalised);
//					System.out.println("Normalised: " + normalised + "  editor: " + tce);
//					assert tce != null;
//					return tce;
				}
//				public TableCellRenderer getCellRenderer(int row, int col) {
//					if (col == 0) return super.getCellRenderer(row, col);
//
//					Object v = getValueAt(row, col);
//					BeanInfo bi = ((PropertyTable)getModel()).bi;
//					final PropertyEditor pe = bi.getPropertyDescriptors()[row].createPropertyEditor(v);
//					final PropertyEditor pe2 = pe != null ? pe :
//						PropertyEditorManager.findEditor(v.getClass());
//					assert pe2 != null : "No property editor for " + v.getClass();
//					pe2.setValue(v);
//					return new TableCellRenderer() {
//						public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//							if (pe.supportsCustomEditor()) {
//								return pe.getCustomEditor();
//							}
//							if (pe.getTags() != null) {
//								return new JComboBox(pe.getTags());
//							}
//							return new JLabel("OOps");
//						}
//					};
//				}

				public void tableChanged(TableModelEvent tableModelEvent) {
					super.tableChanged(tableModelEvent);
//
//					System.out.println(
//						tableModelEvent.getType() + " " +
//						tableModelEvent.getColumn() + " " +
//						tableModelEvent.getFirstRow() + " " +
//						tableModelEvent.getLastRow() + " "
//						);
//
					if (tableModelEvent.getType() == TableModelEvent.UPDATE ||
						tableModelEvent.getType() == TableModelEvent.INSERT) {
						if (tableModelEvent.getColumn() == TableModelEvent.ALL_COLUMNS ||
							tableModelEvent.getColumn() == 1) {
							if (tableModelEvent.getFirstRow() != TableModelEvent.HEADER_ROW) {
								// last row (exclusive)
								int last = Math.min(getRowCount()-1, tableModelEvent.getLastRow())+1;
//								System.out.println("update " + tableModelEvent.getFirstRow() + " to " + last);
								assert last >= tableModelEvent.getFirstRow();
								for (int row = tableModelEvent.getFirstRow(); row < last; row++) {
									TableCellRenderer tcr = getCellRenderer(row, 1);
									java.awt.Component cmp = prepareRenderer(tcr, row, 1);
									setRowHeight(row, Math.max(getRowHeight(), cmp.getPreferredSize().height));
								}
							}
						}
					}
				}
			};
			jTableProperty.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
			jTableProperty.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);


			jTree = new JTree(edc.getTreeModel());


			jTableMessage.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
			jTableMessage.getColumnModel().setColumnSelectionAllowed(false);
			jTableMessage.doLayout();


			jScrollTree.getViewport().setView(jTree);
			jScrollProperty.getViewport().setView(jTableProperty);
			jScrollMessage.getViewport().setView(jTableMessage);


			jTableProperty.setDefaultRenderer(ColorRenderer.getForClass(), new ColorRenderer());
			jTableProperty.setDefaultRenderer(myStrokeRenderer.getForClass(), new myStrokeRenderer());
			jTableProperty.setDefaultRenderer(SolidFillRenderer.getForClass(), new SolidFillRenderer());
			jTableProperty.setDefaultRenderer(FontRenderer.getForClass(), new FontRenderer());
			jTableProperty.setDefaultRenderer(databinding.Action.CodeBlock.class, new ButtonlikeRenderer("Edit Code"));
			jTableProperty.setDefaultRenderer(FontText.class, new FTRenderer());

			jTableProperty.setDefaultEditor(HAlignment.class, new DefaultCellEditor(new JComboBox(HAlignment.alignments)));
			jTableProperty.setDefaultEditor(VAlignment.class, new DefaultCellEditor(new JComboBox(VAlignment.alignments)));
			jTableProperty.setDefaultEditor(Flip.class, new DefaultCellEditor(new JComboBox(Flip.flips)));
			jTableProperty.setDefaultEditor(Color.class, new ColorEditor());
			jTableProperty.setDefaultEditor(SolidFill.class, new FillEditor());
			jTableProperty.setDefaultEditor(databinding.Action.CodeBlock.class, ce);
			jTableProperty.setDefaultEditor(myStroke.class, new StrokeEditor());
			jTableProperty.setDefaultEditor(FontText.class, new FTEditor());

			edc.getEDM().addListener(new EditorListener() {

				public void editOccured(EditorEvent e) {
					refreshTitle();	// place a * after file name if necessary

					// make sure tabs are still sorted alphabetically
					if (!edc.getRoot().getName().equals(currentName)) {
						reshuffleTab(DCView.this, edc.getRoot().getName());
					}
				}
			});

			edc.getEDM_Compile().addListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					// called after recompile
					edc.getErrorTableModel().fireTableDataChanged();
					notifyTree(edc.getRoot());
					edc.getRenderRoot().deepInvalidate();
				}
				private void notifyTree(BasicEditorNode n) {
					edc.getTreeModel().nodeChanged(edc.getTreeNode(n));
					if (n instanceof InternalNode) {
						for (BasicEditorNode c : ( (InternalNode<?, ?>)n).getChildren()) {
							notifyTree(c);
						}
					}
				}
			});



			// SELECTION LISTENERS

			final ListSelectionListener lsl = new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					assert e.getSource() == jTableMessage.getSelectionModel();

					ListSelectionModel mts = jTableMessage.getSelectionModel();

					// TODO : use central selection and then
					// use e.getFirstIndex, e.getLastIndex
					ArrayList<BasicEditorNode> newselection = new ArrayList<BasicEditorNode>();
					for (int row = 0; row < jTableMessage.getRowCount(); row++) {
						if (mts.isSelectedIndex(row)) {
							BasicEditorNode node = (BasicEditorNode)jTableMessage.getValueAt(row, 1);
							newselection.add(node);
						}
					}
					edc.getSelection().set(newselection, edc.getErrorTableModel());
				}
			};

			final TreeSelectionListener tsl = new TreeSelectionListener() {
				public void valueChanged(TreeSelectionEvent e) {
					if (jTree.getSelectionPaths() == null) {
						edc.getSelection().clear(edc.getTreeModel());
					} else {
						ArrayList<BasicEditorNode> newsel = new ArrayList<BasicEditorNode> ();
						for (TreePath tp : jTree.getSelectionPaths()) {
							TreeNodeAdapter tn = (TreeNodeAdapter)tp.getLastPathComponent();
							BasicEditorNode sel = tn.getUserObject();
							newsel.add(sel);
						}
						edc.getSelection().set(newsel, edc.getTreeModel());
					}
				}
			};

			edc.getSelection().getEventDispatcher().addListener(new SelectionEventListener() {
				public void selectionChanged(SelectionChangedEvent e) {
					jTree.getSelectionModel().removeTreeSelectionListener(tsl);
					jTableMessage.getSelectionModel().removeListSelectionListener(lsl);

					assert e.cause == edc.getTreeModel() ||
						e.cause == edc.getErrorTableModel() ||
						e.cause == cp ||
						e.cause == edc;

					// now we can update without incurring new events
					if (e.cause != edc.getErrorTableModel()) {
						jTableMessage.clearSelection();
					}
					if (e.cause != edc.getTreeModel()) {
						TreeSelectionModel ts = jTree.getSelectionModel();
						ts.clearSelection();
						for (BasicEditorNode node : edc.getSelection().getSelectedNodes()) {
							TreeNodeAdapter tna = edc.getTreeNode(node);
							assert tna != null:"messages source not found for " + node;
							ts.addSelectionPath(tna.getPath());
						}
						jTree.scrollPathToVisible(ts.getLeadSelectionPath());
					}

					// cancel all editing currently going on
					TableCellEditor tce = jTableProperty.getCellEditor();
					if (tce != null) {
						if (!tce.stopCellEditing()) tce.cancelCellEditing();
					}

					if (edc.getSelection().getSelectedNodes().size() == 1) {
						BasicEditorNode sel = edc.getSelection().getSelectedNodes().iterator().next();
						edc.getPropertyTableModel().setSelected(sel);
					} else {
						// TODO: compute intersection of properties?
						edc.getPropertyTableModel().setSelected(null);
					}

					// and restore event listeners

					jTableMessage.getSelectionModel().addListSelectionListener(lsl);
					jTree.getSelectionModel().addTreeSelectionListener(tsl);
				}
			});




//			cp.getEventDispatcher().addListener(new NodeSelectionListener() {
//				public void nodeSelected(NodeSelectedEvent e) {
//					TreeNodeAdapter tna =  dc.n2t.getS(e.node);
//					assert tna != null : "messages source not found for " + e.node;
//
//					TreePath np = tna.getPath();
//					TreeSelectionModel ts = jTree.getSelectionModel();
//					ts.setSelectionPath(np);
//					jTree.scrollPathToVisible(np);
//				}
//			});

			jTree.getSelectionModel().addTreeSelectionListener(tsl);
			jTableMessage.getSelectionModel().addListSelectionListener(lsl);

			jTree.setCellRenderer(new DefaultTreeCellRenderer() {
				public java.awt.Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
					java.awt.Component tcr = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
					assert tcr == this;
						BasicEditorNode val = ( (TreeNodeAdapter)value).getUserObject();
						ImageIcon ic = IconManager.chargeIcon(val.getClass(), BeanInfo.ICON_COLOR_16x16);
						if (ic != null) setIcon(ic);

						if (edc.getCMB().hasErrors(val)) {
							super.setBackground(ColorConverter.withTransparency(Color.red, 0x80));
							super.setOpaque(true);
						} else if (edc.getCMB().hasWarnings(val)) {
							super.setBackground(ColorConverter.withTransparency(Color.orange, 0x80));
							super.setOpaque(true);
						} else {
							super.setOpaque(false);
						}

					return this;
				}
			});

		}

		public void fileChanged(ComponentFileChangedEvent cfce) {
			Component c = edc.getRoot();
			if (c instanceof Complex) {
				( (Complex)c).rewire();
				this.edc.getRenderRoot().deepInvalidate();
				this.cp.repaint();
				edc.recompile();
			}
		}
	}


	/** Uses a list, sorted by component name */
	ArrayList<DCView> views = new ArrayList<DCView>();


//	private final Comparator<DCView> dcvc = new Comparator<DCView>() {
//		public int compare(DCView dcv1, DCView dcv2) {
//			return dcv1.edc.node.getName().compareTo(
//				   dcv2.edc.node.getName());
//		}
//	};

	private boolean tabsSorted() {
		if (jTabbedPane1.getTabCount() == 0) return true;
		DCView prev = (DCView)jTabbedPane1.getComponentAt(0);
		assert views.get(0) == prev;
		for (int i = 1; i < jTabbedPane1.getTabCount(); i++) {
			DCView cur = (DCView)jTabbedPane1.getComponentAt(i);
			assert prev.compareTo(cur) < 0 : prev + " erroneously sits before " + cur;
			assert views.get(i) == cur;
			prev = cur;
		}
		return true;
	}

	private void addDC(Component root, File f) {
		assert tabsSorted();
		assert getViewFor(root) == null;
		EditorDocumentContext edc = new EditorDocumentContext(root, true);
		DCView dcv = new DCView(edc, f);
		int insertIndex = Collections.binarySearch(views, dcv);
		assert insertIndex < 0 : dcv + " already present";
		insertIndex = -insertIndex-1;
		views.add(insertIndex, dcv);
		ComponentTable.getEDM().addListener(dcv);

		ImageIcon ic = IconManager.chargeIcon(root.getClass(), BeanInfo.ICON_COLOR_16x16);

//		// retireve tabs in order
//		DCView [] dcvs = new DCView[jTabbedPane1.getTabCount()];
//		for (int j = 0; j < dcvs.length; j++) dcvs[j] = (DCView)jTabbedPane1.getComponentAt(j);
//
//		int pos = Arrays.binarySearch(dcvs, dcv, dcvc);
//
		assert dcv.cp.getTool() == null;

//		// this inserts identical keys before all copies
//		if (pos < 0) pos = -pos-1;
		// insert tab in sorted position
		jTabbedPane1.insertTab(root.getName(), ic, dcv, null, insertIndex);
		assert tabsSorted();

		selectDC(dcv);
	}

	private void removeDC(DCView dcv) {
		assert tabsSorted();
		int presentIndex = Collections.binarySearch(views, dcv);
		assert presentIndex >= 0 : dcv + " not present";
		views.remove(presentIndex);
		ComponentTable.getEDM().removeListener(dcv);
		assert jTabbedPane1.getComponentAt(presentIndex) == dcv;

		// jTabbedPane1 FIRST changes selection, THEN removes the tab. We can not handle that
		// we also need to work around bug #6368047, where the selection notification is only
		// sent when the LAST tab is removed. We want this to always happen.
		jTabbedPane1.removeChangeListener(tcl);
		jTabbedPane1.removeTabAt(presentIndex);
		jTabbedPane1.addChangeListener(tcl);
		assert tabsSorted();
		onTabsChanged();
	}


	private void reshuffleTab(DCView dcv, String newName) {
		assert tabsSorted();

		// temporarily disable events
		jTabbedPane1.removeChangeListener(tcl);

		// kick tab out
		int presentIndex = Collections.binarySearch(views, dcv);
		assert presentIndex >= 0 : dcv + " not present";
		views.remove(presentIndex);
		assert jTabbedPane1.getComponentAt(presentIndex) == dcv;
		Icon icon = jTabbedPane1.getIconAt(presentIndex);
		jTabbedPane1.removeTabAt(presentIndex);

		// update name, hence change position in the alphabetical order
		dcv.currentName = newName;

		// put tab back in at right position
		int insertIndex = Collections.binarySearch(views, dcv);
		assert insertIndex < 0 : dcv + " already present";
		insertIndex = -insertIndex-1;
		views.add(insertIndex, dcv);
		jTabbedPane1.insertTab(dcv.getName(), icon, dcv, null, insertIndex);

		assert tabsSorted();

		jTabbedPane1.setSelectedIndex(insertIndex);

		// re-enable events
		jTabbedPane1.addChangeListener(tcl);
	}



	/**  replace split components without affecting divider position
	 * @param split JSplitPane
	 * @param top JComponent null to leave it as it is
	 * @param bot JComponent null to leave it as it is
	 */
	private static void replaceSplit(JSplitPane split, JComponent top, JComponent bot) {
			int dl = split.getDividerLocation();
			if (top != null && split.getTopComponent() != top)
				split.setTopComponent(top);
			if (bot != null && split.getBottomComponent() != bot)
				split.setBottomComponent(bot);
			split.setDividerLocation(dl);
	}


	public void updateUndoState(UndoManager um) {
		aUndo.setEnabled(um != null && um.canUndo());
		aRedo.setEnabled(um != null && um.canRedo());

	}

	EditorListener undoStateHandler = new EditorListener() {
		public void editOccured(EditorEvent e) {
			DCView dcv = (DCView)jTabbedPane1.getSelectedComponent();
			jTabbedPane1.setTitleAt(jTabbedPane1.getSelectedIndex(), dcv.edc.node.getName());
			assert dcv.edc == e.edc;
			assert dcv != null;
			updateUndoState(dcv.edc.getUndoManager());
		}
	};

	private void selectDC(DCView dcv) {
		jTabbedPane1.setSelectedComponent(dcv);
	}

}
