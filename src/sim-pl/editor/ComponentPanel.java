/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Scrollable;

import util.GUIHelper;
import databinding.BasicEditorNode;
import databinding.IMoveable;
import editor.EditorDocumentContext.EditorEvent;
import editor.EditorDocumentContext.EditorListener;
import editor.GlobalSelection.SelectionCause;
import editor.tool.Tool;
import editor.tool.Tool.MoveAction;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

/*
 * My favorite graphics packages Inkscape enlarges the area when the users moves a 
 * graphical object out of the current bounds, and never contracts it later. 
 * We adopt that same system. 
 */


public class ComponentPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, SelectionEventListener, SelectionCause, EditorListener, Scrollable {


	private static final int MARGIN = 20;
	private final boolean antialias = true;

	private final EditorDocumentContext edc;
	private final Editor editor;
	private double zoom = 1;
	private int gridSize = 10;

	// work area in component coordinates. Can only ever grow.
	private Rectangle2D bound = new Rectangle2D.Double(); 
	
	private AffineTransform t;
	private Tool tool = null;


	public static class CPPoint {
		public final Point2D real;
		public final Point2D grid;
		public CPPoint(Point2D real, double gridSize) {
			this.real = real;
			this.grid =  gridSize == 0
				? real
				: new Point2D.Double(Math.round(real.getX() / gridSize)*gridSize,
									 Math.round(real.getY() / gridSize)*gridSize);
		}
	}



	public ComponentPanel(final EditorDocumentContext edc, final Editor editor) {
		this.edc = edc;
		this.editor = editor;
		this.setFocusable(true);
		this.setBackground(Color.white);
		this.setToolTipText("Component panel"); // turn on tooltips
		this.addMouseWheelListener(this);
		edc.getSelection().getEventDispatcher().addListener(this);
		edc.getEDM().addListener(this);

		String abort = "abort";
		this.getInputMap(WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), abort);
		this.getActionMap().put(abort, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				tool.abort();
			}
		});

		// use 'w,a,s,d' as arrows keys for single grid-step movement
		final int [][] dir = {{1,0}, {-1,0}, {0,1}, {0,-1}};
		final int [] keys = {KeyEvent.VK_D, KeyEvent.VK_A, KeyEvent.VK_S, KeyEvent.VK_W};
		for (int i = 0; i < dir.length; i++) {
			final int [] diri = dir[i];
			this.getInputMap(WHEN_FOCUSED).put(KeyStroke.getKeyStroke(keys[i], 0), diri);
			this.getActionMap().put(diri, new AbstractAction() {
				public void actionPerformed(ActionEvent e) {
					Set<BasicEditorNode> selection = edc.getSelection().getSelectedNodes();
					Set<IMoveable> movers = new HashSet<IMoveable>();
					
					for (BasicEditorNode<BasicEditorNode> n : selection) {
						n.collectInvolvedInMove(selection, movers);
					}
					
					edc.doEdit(new MoveAction(gridSize*diri[0], gridSize*diri[1],
											  movers,
											  edc));
					edc.getRenderRoot().deepInvalidate();
				}
			});
		}
	}

	public double getZoom() { return zoom; }

	public void setZoom(double zoom) {
		this.zoom = zoom;
		invalidateTransform();
		edc.getRenderRoot().deepInvalidate();
		repaint();
	}
	
	public void zoom(double clicks) {
		setZoom(getZoom() * Math.pow(1.25, clicks));
	}
	

	public void invalidateTransform() {
		t = null;
	}


	public AffineTransform getComponent2Pixels(Graphics2D g) {
		if (t == null) t = computeCorrectingTransform(g);
		return t;
	}



	// the result transforms component coordinates to pixels
	// sets preferred size of this component while at it.
	// updates bound (which may change (due to text) when Graphics2D changes)
	public AffineTransform computeCorrectingTransform(Graphics2D g) {
		Graphics2D g2 = (Graphics2D)this.getGraphics().create();
		// het getNormalizingTransform dillemma (zie executer.ComponentPanel)
		AffineTransform a = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();
		a.scale(zoom,zoom);

		g2.transform(a);
		// recompute bounds (which depend on Graphics2D, which just changed)
		Rectangle2D curbound = edc.getRenderRoot().getDeepBound(g2);
		if (curbound != null) Rectangle2D.union(bound, curbound, bound);
		
		Rectangle2D margin_bound = GUIHelper.grow(MARGIN, bound, null);
		
		a.translate(-margin_bound.getX(), -margin_bound.getY());

		Rectangle pixelbound = a.createTransformedShape(margin_bound).getBounds();
		setPreferredSize(pixelbound.getSize());
		revalidate();
		return a;
	}



	public Tool getTool() { return tool; }

	public void setTool(Tool newtool) {
		assert (tool != null) ==  Arrays.asList(getMouseListeners()).contains(this);
		assert (tool != null) ==  Arrays.asList(getMouseMotionListeners()).contains(this);

//		System.out.println("Replaced " + tool + " with " + newtool);
		if (tool != null) {
			tool.disconnect();
			if (newtool == null) {
				this.removeMouseListener(this);
				this.removeMouseMotionListener(this);
			}
		}

		if (newtool != null) {
			newtool.connect(edc, this, editor);

			if (tool == null) {
				this.addMouseListener(this);
				this.addMouseMotionListener(this);
			}
		}

		tool = newtool;

		assert (tool != null) == Arrays.asList(getMouseListeners()).contains(this);
		assert (tool != null) == Arrays.asList(getMouseMotionListeners()).contains(this);

		this.repaint();
	}
	
	
	public void doToolReset() {
		tool.forget();
		// TODO: repaint smart
		repaint();
	}
	
	
	
	public void repaintInComponentSpace(Rectangle2D bound) {
		assert bound != null : "repaint with null bound";
		Rectangle pix_bd = GUIHelper.transform(getComponentGraphics().getTransform(), bound).getBounds();
		// hack to correct for anti-aliasing
		// it seems difficult to find out how far AA extends beyond the shape boundary
		if (antialias) pix_bd.grow(1, 1);
		repaint(pix_bd);
	}
	



	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		assert tool != null;
		Graphics2D g2 = (Graphics2D)g;
		AffineTransform oldt = g2.getTransform();

		g2.transform(getComponent2Pixels(g2));
		Rectangle2D curbound = edc.getRenderRoot().getDeepBound(g2);
		if (curbound != null && !this.bound.contains(curbound)) {
			// TODO: use notifications of bounds changes instead of this hack
			// This is difficult, because bounds depend on the Graphics2D.
			System.out.println("Expanding work area");
			Rectangle2D.union(bound, curbound, bound); // bound can only grow
			invalidateTransform();
			repaint();
			return;
		}

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antialias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);

		renderGrid(g2);

		tool.render(g2);

		g2.setTransform(oldt);
	}


	private void renderGrid(Graphics2D g) {
		Rectangle clip = g.getClipBounds();
		
		if (false) {
			// visually debug redraws
			Color randc = new Color((int)(Math.random()*(1<<23)));
			g.setColor(randc);
			g.fill(clip);
		}
		
		
		// turn into (x1,y1)-(x2,y2) form
		clip.width  +=  clip.x;
		clip.height +=  clip.y;

		g.setColor(Color.gray);
		Rectangle2D.Double stamp = new Rectangle2D.Double(0,0,1,1);
		// Note: we are in component coordinates, no need to fuss with bounds and margins
		for (stamp.x = clip.x -(clip.x) % gridSize - 0.5; stamp.x < clip.width;  stamp.x += gridSize)	{
			for (stamp.y = clip.y - (clip.y) % gridSize- 0.5; stamp.y < clip.height; stamp.y += gridSize) {
				g.fill(stamp);
			}
		}
	}


	private CPPoint getComponentPoint(Point2D p) {
		Graphics2D g = (Graphics2D)getGraphics();
		try {
			return new CPPoint(getComponent2Pixels(g).inverseTransform(p, null), gridSize);

		} catch (NoninvertibleTransformException ex) { throw new Error(ex); }
	}


	public String getToolTipText(MouseEvent e) {
		CPPoint cp = getComponentPoint(e.getPoint());
		return tool == null
			? null
			: tool.getToolTipText(cp.real, e);
	}

	public Graphics2D getComponentGraphics() {
		Graphics2D g2 = (Graphics2D)getGraphics();
		g2.transform(getComponent2Pixels(g2));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antialias  ?  RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
		return g2;
	}

	public void mouseMoved(MouseEvent e) {
		tool.mouseMoved(getComponentPoint(e.getPoint()),e);
	}

	public void mouseClicked(MouseEvent e) {
		tool.mouseClicked(getComponentPoint(e.getPoint()),e);
	}


	public void mousePressed(MouseEvent e) {
		this.requestFocusInWindow();
		tool.mousePressed(getComponentPoint(e.getPoint()), e);
	}

	public void mouseDragged(MouseEvent e) {
		tool.mouseDragged(getComponentPoint(e.getPoint()), e);
	}

	public void mouseReleased(MouseEvent e) {
		tool.mouseReleased(getComponentPoint(e.getPoint()), e);
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

	public void selectionChanged(SelectionChangedEvent e) {
		tool.notifyStructureChange();
		repaint(); // TODO: sure but a bit heavy
	}

	public void editOccured(EditorEvent e) {
		tool.notifyStructureChange();
		repaint(); // TODO: sure but a bit heavy
	}

	public double getGridSize() {
		return gridSize;
	}

	public void setGridSize(int i) {
		assert i >= 1;
		this.gridSize = i;
		repaint();
	}

	
	
	
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			// zoom
			zoom(-e.getWheelRotation());
		} else {
			getParent().dispatchEvent(e);
		}
	}
	
	
	
	/* ================== scrollable stuff ==================  */
	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 20;
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 100;
	}

	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}
	public boolean getScrollableTracksViewportHeight() { 
		return getPreferredSize().height < getParent().getHeight();
	}
}
