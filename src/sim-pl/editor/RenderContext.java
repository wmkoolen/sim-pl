/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.List;

import databinding.BasicEditorNode;
import databinding.Component;
import databinding.InternalNode;
import databinding.SubTreeListener;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */



/** A DocumentContext contains all information needed to use a component as a subcomponent in another complex component.
 * This includes
 *  - render information
 *  - compilation information
 */


public class RenderContext implements SubTreeListener {

	/** Associate each editor node with a render adapter */
	private final Pairing<BasicEditorNode, RenderAdapter> n2r = new Pairing<BasicEditorNode, RenderAdapter>();

	/** The root node of the component */
	protected final Component node;

	/** The render root */
	protected final RenderAdapter rroot;

	public final boolean renderLabels;
	public final boolean renderProbeStubs;


	public RenderContext(Component node, boolean renderLabels, boolean renderProbeStubs) {
		this(node, true, renderLabels, renderProbeStubs);
	}

	protected RenderContext(Component node, boolean buildtree, boolean renderLabels, boolean renderProbeStubs) {
		this.node = node;
		this.renderLabels = renderLabels;
		this.renderProbeStubs = renderProbeStubs;
		rroot = new RenderAdapter(this, node);
		n2r.put(node, rroot);

		if (buildtree) {
			subTreeAttached(null, node, 0);
		}
	}

	public final Component getRoot() {
		return node;
	}


	public final RenderAdapter getRenderRoot() {
		return rroot;
	}

	public final RenderAdapter getRenderNode(BasicEditorNode n) {
		return n2r.getS(n);
	}








/****************** SUBTREE LISTENER ************************/

	protected void nodeAttached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		RenderAdapter tna = new RenderAdapter(this, child);
		n2r.put(child, tna);
	}

	public final void subTreeAttached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
//		System.out.println("Processing attachment of child : " + child + " at index " + index);

		assert (parent == null) == n2r.containsS(child);

		if (parent != null) {
			nodeAttached(parent, child, index);
		}

		if (child instanceof InternalNode) {
			// make sure we hear info about future new subkids
			InternalNode<?,?> nl = (InternalNode<?,?>)child;
			nl.addSubtreeListener(this);

			// notify ourself about current subkids
			List<? extends BasicEditorNode> l = nl.getChildren();
			for (int i = 0; i < l.size(); ++i) {
				BasicEditorNode<?> cn = l.get(i);
				assert cn != null:"Null kid in " + child.getClass();
				subTreeAttached(nl, cn, i);
			}
		}
	}

	protected void nodeDetached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		n2r.removeF(child);
	}

	public final void subTreeDetached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		// notify tree model
//		System.out.println("Processing detachment of child : " + child + " at index " + index);

		if (child instanceof InternalNode) {
			InternalNode<?, ?> nl = (InternalNode<?, ?>)child;

			// notify ourself about all removed subkids
			List<? extends BasicEditorNode> l = nl.getChildren();
			// process the list in reverse (this is more efficient, and also
			// works when the nodes are not in fact really removed)
			for (int i = l.size()-1; i >= 0; --i) {
				BasicEditorNode<?> cn = l.get(i);
				assert cn != null:"Null kid in " + child.getClass();
				subTreeDetached(nl, cn, i);
			}

			// disconnect listener
			nl.removeSubtreeListener(this);
		}

		nodeDetached(parent, child, index);

	}
}
