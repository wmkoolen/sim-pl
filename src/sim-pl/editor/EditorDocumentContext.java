/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import compiler.wisent.Type_Struct;

import databinding.BasicEditorNode;
import databinding.Component;
import databinding.InternalNode;
import databinding.InternalNode.Mutator;
import databinding.InternalNode.MutatorList;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.compiled.Compiled.CompiledSimple;
import editor.GlobalSelection.SelectionCause;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

/** The EditorDocumentContext adds the information to a DocumentContext that is required to edit the component in the SIM-PL Editor.
 * This includes
 *  - tree model
 *  - error table model
 *  - component selection
 *  - undo/redo information
 */


 /* we acknowledge the following actions
 - addition of a new node (context sensitive?)
    * tool-dependent mouse operation (drag, click)
	* button click in tool menu
 - removal of selected node
    * delete key
 - moving selected nodes around
    * mouse
	* arrow keys
 - changing a node's within-parent position
    * mouse in tree
	* pgup-pgdown keys
 - set property by textual editing
 - change property by moving control points around
 - rename a node (need to inform all referencing nodes)

the following operations use the global clipboard
 - cut selected components
   (locally equivalent to a delete)
 - copy components to clipboard
   (locally equivalent to doing nothing)
 - paste clipboard component (at current mouse position), and select them
   (locally equivalent to several well-placed additions)

these actions can be decomposed in the following simpler operations,
which need to be done, undone and redone atomically of course
  - add node X to parent Y
  - remove node X
  - change node property X from Y into Z
  - clipboard set
  - clipboard copy
 */


public class EditorDocumentContext extends DocumentContext implements SelectionCause {

	/** Associate each editor node with a tree node */
	private final Pairing<BasicEditorNode, TreeNodeAdapter> n2t = new Pairing<BasicEditorNode, TreeNodeAdapter>();


	private Set<BasicEditorNode> nodes = new HashSet<BasicEditorNode>();

	static class ComponentTreeModel extends DefaultTreeModel implements SelectionCause {
		public ComponentTreeModel(TreeNode root) {
			super(root);
		}
	}
	/** The tree model */
	private final ComponentTreeModel dtm;

	/** The tree root */
	private final TreeNodeAdapter troot;


	/** The error table model */
	private final ErrorTableModel etm;

	/** Property table model */
	private final PropertyTableModel ptm = new PropertyTableModel(this);


	/** The selection (set of nodes) */
	private final GlobalSelection selection = new GlobalSelection();


	/** mark the state after saving to file*/
	public UndoableEdit markedState  = null;


	private CompiledComponent<?> cc;

	public CompiledComponent<?> getCompiledComponent() {
		assert cc != null;
		return cc;
	}

	/** The type structure, stored here for simple component code editing */
	Type_Struct type = new Type_Struct();

	public void mark() { this.markedState = uem.getState();  }
	public boolean hasChanged() {
		return markedState != uem.getState();
	}


	protected CompiledComponent<?> recompile() {
		cc = super.recompile();
		this.type = (cc instanceof CompiledSimple)
			? ((CompiledSimple)cc).type
			: null;
//		System.out.println("New cc: " + cc);
		return cc;
	}


	static class MyUndoManager extends UndoManager {
		public UndoableEdit getState() {
			return super.editToBeUndone();
		}

		public UndoableAction undoLoud() {
			UndoableAction e = (UndoableAction)super.editToBeUndone();
			super.undo();
			return e;
		}

		public UndoableAction redoLoud() {
			UndoableAction e = (UndoableAction)super.editToBeRedone();
			super.redo();
			return e;
		}
	}

	private final MyUndoManager uem = new MyUndoManager();



	static public class EditorEvent {
		static enum Kind {FIRSTTIME, UNDO, REDO};

		final EditorDocumentContext edc;
		final UndoableAction edit;
		final Kind kind;

		public EditorEvent(EditorDocumentContext edc, UndoableAction edit, Kind kind) {
			this.edc = edc;
			this.edit = edit;
			this.kind = kind;
		}
	}

	static public interface EditorListener extends EventListener {
		public void editOccured(EditorEvent e);
	}


	/** edm events happen when edits, undos and redos occur */
	private EventDispatchManager<EditorEvent, EditorListener> edm = new EventDispatchManager<EditorEvent,EditorListener>() {
		protected void dispatchEvent(EditorListener listener, EditorEvent event) {
			listener.editOccured(event);
		}
	};

	EventDispatcher<EditorListener> getEDM() { return edm; }

/*
	/** edm2 events happen when tree structure changes  * /
	private EventDispatchManager<ChangeEvent, ChangeListener> edm2 = new EventDispatchManager<ChangeEvent,ChangeListener>() {
		protected void dispatchEvent(ChangeListener listener, ChangeEvent event) {
			listener.stateChanged(event);
		}
	};

	EventDispatcher<ChangeListener> getEDM2() { return edm2; }
*/



	public void nodeAttached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		super.nodeAttached(parent, child, index);
		assert !nodes.contains(child);
		nodes.add(child);
		TreeNodeAdapter tna = new TreeNodeAdapter(child, n2t);
		n2t.put(child, tna);
		// notify tree model
		dtm.nodesWereInserted(n2t.getS(parent), new int [] {index});
	}

	public void nodeDetached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		assert nodes.contains(child);
		nodes.remove(child);
		// notify tree model
		TreeNodeAdapter tna = n2t.getS(child);
		dtm.nodesWereRemoved(n2t.getS(parent), new int[] {index}, new Object [] {tna});
		n2t.remove(child, tna);
		super.nodeDetached(parent, child, index);
	}


	public EditorDocumentContext(Component c, boolean renderLabels) {
		super(c, false, renderLabels, true); // we build the tree ourselves


		// take special care w.r.t. the root node
		troot = new TreeNodeAdapter(node, n2t);
		n2t.put(c, troot);

		dtm = new ComponentTreeModel(troot);
		etm  = new ErrorTableModel(getCMB());

		getEDM().addListener(new EditorListener() {
			public void editOccured(EditorEvent e) {
				if (e.edit.requiresRecompile()) {
					recompile();
				}

				//TODO: possibly overkill here
				ptm.parsimoniousRefresh();
			}
		});

		// build the tree-adapter tree
		subTreeAttached(null, c, 0);
		recompile();
	}


	public TreeNodeAdapter getTreeNode(BasicEditorNode n) {
		return n2t.getS(n);
	}

	public ComponentTreeModel getTreeModel() { return dtm; }
	public ErrorTableModel getErrorTableModel() { return etm; }
	public PropertyTableModel getPropertyTableModel() { return ptm; }
	public GlobalSelection getSelection()  { return selection; }


	public static interface UndoableAction extends UndoableEdit {
		public void perform();
		public boolean requiresRecompile();
	}

	public static abstract class UndoableEditAction extends AbstractUndoableEdit implements UndoableAction {}

	UndoManager getUndoManager() { return uem; }

	public void undo() {
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();
		UndoableAction edit = uem.undoLoud();
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();

		edm.fireEvent(new EditorEvent(this, edit, EditorEvent.Kind.UNDO));
	}

	public void redo() {
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();
		UndoableAction edit = uem.redoLoud();
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();

		edm.fireEvent(new EditorEvent(this, edit, EditorEvent.Kind.REDO));
	}


	public void doEdit(UndoableAction e) {
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();
		e.perform();
		assert getRoot().getParent() == null;
		assert getRoot().isValidRecursively();
		uem.addEdit(e);
		edm.fireEvent(new EditorEvent(this, e, EditorEvent.Kind.FIRSTTIME));
	}


//	public <T extends BasicEditorNode<NodeList<?,T>>> void addNodeUndoable(NodeList<?, T> parent, T child) {
	public void addNodeUndoable(final InternalNode parent, final BasicEditorNode child, final boolean triggersRecompile) {

		doEdit(new UndoableEditAction() {
			Mutator mut;

			public boolean requiresRecompile() { return triggersRecompile; }

			public void perform() {
				assert mut == null;
				mut = parent.add(child);
//				TreeNodeAdapter tna = new TreeNodeAdapter( child, n2t);
//				RenderAdapter ra = new RenderAdapter(child, n2r);
//				child.setParent(parent);
//				parent.getChildren().add(child);
//				dtm.nodesWereInserted(n2t.getS(parent), new int [] {parent.getChildren().size()-1});
				selection.set(child, EditorDocumentContext.this);
//				edm2.fireEvent(new ChangeEvent(this));
			}

			public void undo() {
				super.undo();
				mut.undo();
//				TreeNodeAdapter tna = n2t.getS(child); assert tna != null;
//				RenderAdapter ra = n2r.getS(child); assert ra != null;

//				assert parent.getChildren().contains(child);
//				assert parent.getChildren().indexOf(child) == parent.getChildren().size()-1;

//				// TODO: handle removal of subtrees correctly
//				parent.getChildren().remove(child);
//				child.setParent(null);

//				n2t.remove(child, tna);
//				n2r.remove(child, ra);
//				dtm.nodesWereRemoved(n2t.getS(parent),
//									 new int [] {parent.getChildren().size()},
//									 new Object [] {tna});
				selection.remove(child, EditorDocumentContext.this);
//				edm2.fireEvent(new ChangeEvent(this));
			}

			public void redo()  {
				super.redo();
				mut.redo();
				selection.set(child, EditorDocumentContext.this);
			}
		});

	}




	private void addRecursive(BasicEditorNode<?> n, Set<BasicEditorNode> ns) {
		if (ns.contains(n)) return; // subtree already present

		if (n.canBeDetached()) {
			ns.add(n);
		}

		if (n instanceof InternalNode) {
			for (BasicEditorNode<?> c : ((InternalNode<?,?>)n).getChildren()) {
				addRecursive(c, ns);
			}
		}
	}

	public void deleteSelection() {
		if (selection.isEmpty()) return; // do not waste an undo slot

//		final ArrayList<BasicEditorNode> parents = new ArrayList<BasicEditorNode>();
//		final Map<BasicEditorNode,Integer> node2index = new HashMap<BasicEditorNode,Integer>();


		// compute the transitive tree-reduction of the selection
		final Set<BasicEditorNode> rec_sel = new HashSet<BasicEditorNode>();
		for (BasicEditorNode n : selection.getSelectedNodes()) {
			addRecursive(n, rec_sel);
		}

//		System.out.println("Delete tree: " + rec_sel);

		if (rec_sel.isEmpty()) return; // do not waste an undo slot

		 doEdit(new UndoableEditAction() {
			 MutatorList muts = new MutatorList();

			 // TODO: find out whether we really need to recompile
			 // for example during the addRecursive part of the deletion
			 public boolean requiresRecompile() { return true; /* safe bet */ }


			public void perform() {
				for (BasicEditorNode<?> n : rec_sel) {
					if (nodes.contains(n)) {
//						System.out.println("Delete " + n.getClass().getName() + " " + n);
						muts.add( ( (InternalNode<?, BasicEditorNode<?>>)n.getParent()).remove(n) );
					}
				}
			}


			public void undo() throws CannotUndoException {
				super.undo();
				muts.undo();
				getSelection().set(rec_sel, EditorDocumentContext.this);
			}


			public void redo() throws CannotRedoException {
				super.redo();
				muts.redo();
			}

		});
	}
}
