/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

import util.GUIHelper;
import databinding.BasicEditorNode;
import editor.ComponentPanel;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class RectangularShapeTool<T extends RectangularShape> extends Tool {

	public abstract T getShape();
	public abstract BasicEditorNode createNode(T shape);

	protected State getDefaultState() { return new RectDefaultState(); }

	class RectDefaultState extends DefaultState {
		public String getToolTipText() { return null; }
		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new LockRect(p));
		}
	}

	class LockRect extends LockState {
		final ComponentPanel.CPPoint dragOrigin;

		public LockRect(ComponentPanel.CPPoint dragOrigin) {
			this.dragOrigin = dragOrigin;
		}

		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new MoveRect(dragOrigin, dragOrigin), p, e, true);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			// clicking nothing (without dragging) empties selection
			edc.getSelection().clear(cp);
			transition(defaultState);
		}
	}

	class MoveRect extends MoveState {
		
		Rectangle2D bound_buf = new Rectangle2D.Double();
		
		ComponentPanel.CPPoint dragTarget;
		boolean centered = false;
		boolean snapped = false;
		T r = getShape(); // draw buffer

		public MoveRect(ComponentPanel.CPPoint dragOrigin, ComponentPanel.CPPoint dragTarget) {
			super(dragOrigin);
			this.dragTarget = dragTarget;
			r.setFrameFromDiagonal(dragOrigin.grid, dragTarget.grid);
		}

		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			centered = e.isShiftDown();
			snapped =  e.isControlDown();
			dragTarget = p;
			
			bound_buf.setRect(r.getBounds2D());
			
			if (centered) {
				r.setFrameFromCenter(dragOrigin.grid, dragTarget.grid);
			} else {
				r.setFrameFromDiagonal(dragOrigin.grid, dragTarget.grid);
			}
			
			bound_buf.add(r.getBounds2D());
			
			CurrentProperties cps = CurrentProperties.getProps();
			GUIHelper.grow(cps.getStroke().getLineWidth()/2, bound_buf, bound_buf);

			repaintInComponentSpace(bound_buf);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
//			CurrentProperties ps = CurrentProperties.getProps();
//			edc.addNodeUndoable(((Component)edc.getRoot()).getForms(), new Rectangle(ps.getStroke(), ps.getFill(), r.getBounds()));
			edc.addNodeUndoable(edc.getRoot().getForms(), createNode(r), false);
			transition(defaultState, p, e);
		}

		public void render(Graphics2D g) {
			super.render(g);

			CurrentProperties cps = CurrentProperties.getProps();

			if (cps.getFill() != null) {
				g.setColor(cps.getFill().getColor());
				g.fill(r);
			}
			if (cps.getStroke() != null) {
				g.setColor(cps.getColor());
				g.setStroke(cps.getStroke().getStroke());
				g.draw(r);
			}
		}
	}
}
