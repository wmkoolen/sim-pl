/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.IntResolver;
import databinding.Parameters;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ParameterDialog extends NewElementDialog implements ChangeListener {
	public ParameterDialog(Frame owner) {
		super(owner, "New Parameter");
		jbInit();
	}

	/** null is used to indicate parse failure */
	String name;

	/** null is used to indicate parse failure */
	IntResolver value;

	public String getNewName() {
//		assert returnValue;
//		assert name != null;
		return name;
	}

	public IntResolver getDefaultValue() {
//		assert returnValue;
//		assert value != null;
		return value;
	}

	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return name != null;
		}

		protected void revalidate() {
			name = jTextField1.getText().length() > 0 ? jTextField1.getText() : null;
		}
	};

	ValidityManager rdl2 = new ValidityManager() {
		public boolean isValid() {
			return value != null;
		}

		protected void revalidate() {
			try {
				value = IntResolver.valueOf(jTextField2.getText());
			} catch (Exception ex) {
				value = null;
			}
		}
	};

	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid() && rdl2.isValid());
	}


	private void jbInit()  {
		jPanel2.setLayout(gridBagLayout1);
		setIcon(IconManager.chargeIcon(Parameters.class, BeanInfo.ICON_COLOR_32x32));


		jTextField1.setColumns(20);


		jLabel1.setLabelFor(jTextField2);
		jLabel1.setText("Default value: ");
		jLabel2.setLabelFor(jTextField1);
		jLabel2.setText("Name: ");
		this.getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);
		jPanel2.add(jLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.EAST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jLabel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.EAST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jTextField1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jTextField2, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));

		wire(jTextField1, rdl1, this);
		wire(jTextField2, rdl2, this);
	}


	JPanel jPanel2 = new JPanel();
	JTextField jTextField1 = new JTextField();
	JTextField jTextField2 = new JTextField();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();

}
