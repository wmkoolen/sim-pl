/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import util.DialogHelper;
import editor.EventDispatchManager;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class NewElementDialog extends JDialog {

	public NewElementDialog(Frame owner, String title) {
		super(owner, title, true);
//		assert isModal();
		jbInit();
	}

	/** whether user clicked 'ok' or 'cancel' */
	protected boolean returnValue;

	public void setIcon(Icon i) {
		jLabel4.setIcon(i);
	}

	public Icon getIcon() {
		return jLabel4.getIcon();
	}

	public void setOKenabled(boolean b) {
		jButton1.setEnabled(b);
	}

	// make these methods final on purpose
	public final String getName() {return super.getName(); }
	public final void setName(String name) {super.setName(name); }


	private void jbInit()  {
		DialogHelper.performOnEscapeKey(this, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); // escape == setVisible(false)
			}
		});
		setDefaultCloseOperation(HIDE_ON_CLOSE); // close == setVisible(false)

		this.getContentPane().setLayout(borderLayout1);
//		jLabel4.setIcon(new ImageIcon(new java.net.URL("file:////home/wouter/Werk/SIM-PL/SIM-PL/v2/build/generated/sim-pl/databinding/Parameters_large.png")));
//		this.setTitle("New parameter");
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);
		jButton2.setText("Cancel");
		jPanel1.add(jButton1);
		jPanel1.add(jButton2);
		this.getContentPane().add(jLabel4, java.awt.BorderLayout.WEST);

		jButton1.setText("OK");

		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				returnValue = true;
				setVisible(false);
			}
		});

		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				returnValue = false;
				setVisible(false);
			}
		});

		((JPanel)getContentPane()).setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		jLabel4.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		this.getRootPane().setDefaultButton(jButton1);
	}

	public boolean ask() {
		returnValue = false;
		pack();
		setLocationRelativeTo(getOwner());
		setVisible(true);
		dispose();
		return returnValue;
	}

	private BorderLayout borderLayout1 = new BorderLayout();
	private JPanel jPanel1 = new JPanel();
	private JButton jButton1 = new JButton();
	private JButton jButton2 = new JButton();
	private JLabel jLabel4 = new JLabel();













	public abstract static class ValidityManager extends EventDispatchManager<ChangeEvent, ChangeListener> implements DocumentListener {
		private boolean state;
		abstract public boolean isValid();
		abstract protected void revalidate();

		protected void dispatchEvent(ChangeListener l, ChangeEvent e) {
			l.stateChanged(e);
		}

		public void insertUpdate(DocumentEvent e)  { stateChanged(); }
		public void removeUpdate(DocumentEvent e)  { stateChanged(); }
		public void changedUpdate(DocumentEvent e) { stateChanged(); }

		public void init() {
			state = isValid();
			fireEvent(null);
		}

		private void stateChanged() {
			revalidate();
			Boolean v = isValid();
			if (v != state) {
				state = v;
				fireEvent(null);
			}
		}
	}

	static class ValidityInputVerifier extends InputVerifier {
		final ValidityManager vm;
		ValidityInputVerifier(ValidityManager vm) {
			this.vm = vm;
		}

		public boolean verify(JComponent input) {
			return vm.isValid();
		}
	}


	static class ItemColorizer implements ChangeListener {
		JTextComponent t;
		ValidityManager v;
		Color good;
		Color bad;

		public ItemColorizer( JTextComponent t, ValidityManager v) {
			this(t,v, Color.black, Color.red);
		}

		public ItemColorizer( JTextComponent t, ValidityManager v, Color good, Color bad) {
			this.t = t;
			this.v = v;
			this.good = good;
			this.bad = bad;
		}

		public void stateChanged(ChangeEvent e) {
			t.setForeground(v.isValid() ? good : bad);
		}
	}


	public static void wire(JTextComponent jTextField1, ValidityManager rdl1, ChangeListener cl) {
		jTextField1.getDocument().addDocumentListener(rdl1);
		jTextField1.setInputVerifier(new ValidityInputVerifier(rdl1));
		rdl1.addListener(cl);
		rdl1.addListener(new ItemColorizer(jTextField1, rdl1));
		rdl1.init();
	}

}
