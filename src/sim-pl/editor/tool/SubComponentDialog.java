/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.Flip;
import databinding.SubComponents;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SubComponentDialog extends NewElementDialog implements ChangeListener {

	Set<String> forbidden;

	public SubComponentDialog(Frame owner, Set<String> forbidden) {
		super(owner, "New SubComponent");
		assert forbidden != null;
		this.forbidden = forbidden;
		jbInit();
	}


	// name
	String name;
	public String getNewName() { assert name != null; return name; }

	public void setNewName(String name) {
		assert name != null;
		jTextField1.setText(name);
	}

	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return name != null;
		}

		protected void revalidate() {
			name = jTextField1.getText();
			if (name.length() == 0 || forbidden.contains(name)) name = null;
		}
	};

	// zoom
	Double zoom;
	public double getZoom() { return zoom; }
	public void setZoom(double d) {
		jTextField3.setText(Double.toString(d));
	}

	ValidityManager rdl3 = new ValidityManager() {
		public boolean isValid() {
			return zoom != null;
		}

		protected void revalidate() {
			try {
				zoom = Double.valueOf(jTextField3.getText());
				if (zoom <= 0) zoom = null;
			} catch (NumberFormatException ex) {
				zoom = null;
			}
		}
	};


	// collapsed state
	public boolean isCollapsed() { return jCheckBox1.isSelected(); }
	public void setCollapsed(boolean collapsed) { jCheckBox1.setSelected(collapsed); }


	// flip state
	public Flip getFlip() {
		if (jRadioButton4.isSelected()) return Flip.none;
		if (jRadioButton3.isSelected()) return Flip.horizontal;
		if (jRadioButton2.isSelected()) return Flip.vertical;
		if (jRadioButton1.isSelected()) return Flip.diagonal;
		throw new Error("no option selected");
	}

	public void setFlip(Flip f) {
		jRadioButton4.setSelected(f.equals(Flip.none));
		jRadioButton3.setSelected(f.equals(Flip.horizontal));
		jRadioButton2.setSelected(f.equals(Flip.vertical));
		jRadioButton1.setSelected(f.equals(Flip.diagonal));
	}




	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid() && rdl3.isValid());
	}



	private void jbInit()  {
		setIcon(IconManager.chargeIcon(SubComponents.class, BeanInfo.ICON_COLOR_32x32));

		jPanel1.setLayout(gridBagLayout1);

		jLabel1.setLabelFor(jTextField1);
		jLabel3.setLabelFor(jCheckBox1);
		jLabel4.setLabelFor(jTextField3);
		jLabel5.setLabelFor(jPanel2);

		jLabel1.setText("Name: ");
		jLabel3.setText("Collapse: ");
		jLabel4.setText("Zoom: ");
		jLabel5.setText("Flip: ");

		jTextField1.setColumns(20);
		jTextField3.setColumns(20);

		jRadioButton1.setText("Diag");
		jRadioButton2.setText("Ver");
		jRadioButton3.setText("Hor");
		jRadioButton4.setText("None");
		jRadioButton4.setSelected(true);
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);




		jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField1, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jTextField2, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
//			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
//			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jButton1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
//			, GridBagConstraints.EAST, GridBagConstraints.NONE,
//			new Insets(0, 0, 0, 0), 0, 0));
	    jPanel1.add(jCheckBox1, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel4, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField3, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jPanel2, new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
//            , GridBagConstraints.WEST, GridBagConstraints.NONE,
//			new Insets(0, 0, 0, 0), 0, 0));

		jPanel2.add(jRadioButton4);
		jPanel2.add(jRadioButton3);
		jPanel2.add(jRadioButton2);
		jPanel2.add(jRadioButton1);


		buttonGroup1.add(jRadioButton1);
		buttonGroup1.add(jRadioButton2);
		buttonGroup1.add(jRadioButton3);
		buttonGroup1.add(jRadioButton4);


		wire(jTextField1, rdl1, this);
		wire(jTextField3, rdl3, this);
	}

	JPanel jPanel1 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JTextField jTextField1 = new JTextField();
	JLabel jLabel3 = new JLabel();
	JCheckBox jCheckBox1 = new JCheckBox();
	JLabel jLabel4 = new JLabel();
	JLabel jLabel5 = new JLabel();
	JTextField jTextField3 = new JTextField();
	JPanel jPanel2 = new JPanel();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JRadioButton jRadioButton1 = new JRadioButton();
	JRadioButton jRadioButton2 = new JRadioButton();
	JRadioButton jRadioButton3 = new JRadioButton();
	JRadioButton jRadioButton4 = new JRadioButton();

}
