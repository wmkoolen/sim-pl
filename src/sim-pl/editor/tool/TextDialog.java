/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.beans.BeanInfo;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import databinding.forms.Text;
import editor.FTEditor.FontText;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class TextDialog extends NewElementDialog implements ChangeListener {
	public TextDialog(String title, Frame owner) {
		super(owner, title);
		jbInit();
	}

	public void setFont(Font f) {
		throw new Error("setFont deprecated, use setMyFont");
	}

	Font f;
	private Font getMyFont() {
		assert rdl1.isValid() && rdl2.isValid();
		return f;
	}

	private void setMyFont(Font f) {
		this.f = f;
		jComboBox1.setSelectedItem(f.getFamily());
		jTextArea1.setFont(f);
		jCheckBox1.setSelected(f.isBold());
		jCheckBox2.setSelected(f.isItalic());
		jTextField1.setText(Float.toString(f.getSize2D()));
	}

	String text;
	private String getText() {
		assert rdl1.isValid() && rdl2.isValid();
		return text;
	}

	private void setText(String string) {
		jTextArea1.setText(string);
	}


	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return text != null;
		}

		protected void revalidate() {
			text = jTextArea1.getText();
			if (text.length() == 0) text = null;
		}
	};


	Float size;
	ValidityManager rdl2 = new ValidityManager() {
		public boolean isValid() {
			return size != null;
		}

		protected void revalidate() {
			try {
				size = Float.valueOf(jTextField1.getText());
				if (size <= 0) size = null;
			} catch (Exception ex) {
				size = null;
			}
		}
	};


	public void addNotify() {
		super.addNotify();
//		t = ((Graphics2D)getGraphics()).getDeviceConfiguration().getNormalizingTransform();
		recomputeFont();
	}

	public void recomputeFont() {
		if (rdl2.isValid()) {
			f = new Font( (String)jComboBox1.getSelectedItem(),
						 (jCheckBox1.isSelected() ? Font.BOLD : Font.PLAIN) |
						 (jCheckBox2.isSelected() ? Font.ITALIC : Font.PLAIN),
						 1).deriveFont(size);
//	        System.out.println("New font: " + f);
			AffineTransform t = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();
			jTextArea1.setFont(f.deriveFont(t));
		}
	}

	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid() && rdl2.isValid());
		recomputeFont();
	}







	private void jbInit()  {
		setIcon(IconManager.chargeIcon(Text.class, BeanInfo.ICON_COLOR_32x32));

		jTextArea1.setBorder(BorderFactory.createLoweredBevelBorder());
		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jPanel1.setLayout(borderLayout1);
		jLabel4.setLabelFor(jCheckBox1);
		jLabel4.setText("Bold: ");
		jLabel5.setLabelFor(jCheckBox2);
		jLabel5.setText("Italic: ");
		jLabel1.setLabelFor(jComboBox1);
		jLabel2.setLabelFor(jTextField1);
		jCheckBox2.setHorizontalAlignment(SwingConstants.CENTER);
		jCheckBox1.setHorizontalAlignment(SwingConstants.CENTER);
		jScrollPane1.getViewport().add(jTextArea1);
		jPanel2.setLayout(gridBagLayout1);
		jLabel1.setText("Font: ");
		jLabel2.setText("Size: ");
		jLabel3.setText("jLabel3");
		jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);
		jPanel1.add(jPanel2, java.awt.BorderLayout.NORTH);
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		jPanel2.add(jLabel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jLabel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jTextField1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jLabel4, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jLabel5, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jComboBox1, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jCheckBox1, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jCheckBox2, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));

		jComboBox1.setSelectedIndex(0);

		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recomputeFont();
			}

		};
		jComboBox1.addActionListener(al);
		jCheckBox1.addActionListener(al);
		jCheckBox2.addActionListener(al);

		jTextField1.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) { recomputeFont(); }
			public void removeUpdate(DocumentEvent e) { recomputeFont(); }
			public void changedUpdate(DocumentEvent e) { recomputeFont(); }
		});

		wire(jTextArea1,  rdl1, this);
		wire(jTextField1, rdl2, this);
		setMyFont(new Font("Serif", Font.PLAIN, 18));
//		recomputeFont();
	}

	public void setVisible(boolean b) {
		if (b) jTextArea1.requestFocus();
		super.setVisible(b);
	}



	JScrollPane jScrollPane1 = new JScrollPane();
	JTextArea jTextArea1 = new JTextArea();
	JPanel jPanel1 = new JPanel();
	JPanel jPanel2 = new JPanel();
	BorderLayout borderLayout1 = new BorderLayout();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
//	JComboBox jComboBox1 = new JComboBox(GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames());
	JComboBox jComboBox1 = new JComboBox(new String [] {"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput"});
	JTextField jTextField1 = new JTextField();
	JLabel jLabel3 = new JLabel();
	JLabel jLabel4 = new JLabel();
	JLabel jLabel5 = new JLabel();
	JCheckBox jCheckBox1 = new JCheckBox();
	JCheckBox jCheckBox2 = new JCheckBox();


	public void setFT(FontText fontText) {
		this.setMyFont(fontText.f);
		this.setText(fontText.text);
	}

	public FontText getFT() {
		assert rdl1.isValid() && rdl2.isValid();
		return new FontText(getMyFont(), getText());
	}

	public FontText edit(FontText currentFT) {
		setFT(currentFT);
		return ask()
			? getFT()
			: null;
	}
}
