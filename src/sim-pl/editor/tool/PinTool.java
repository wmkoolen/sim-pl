/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;

import util.GUIHelper;
import util.GUIHelper.Alignment;
import databinding.Input;
import databinding.Output;
import databinding.Pin;
import databinding.render.PinLabelRenderNode;
import databinding.render.PinRenderNode;
import editor.ComponentPanel;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PinTool extends Tool {
	boolean input;

	public PinTool(boolean input) {
		this.input = input;
	}

	protected State getDefaultState() {
		return new InputDefaultState();
	}

	class InputDefaultState extends CursorState {
		final Point2D.Double pinPos   = new Point2D.Double(); // absolute position of the pin
		final Point2D.Double labelPos = new Point2D.Double(); // absolute position of the label
		boolean pressed = false;
		boolean dragging = false;

		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

	    public void forget() {
			pressed = false;
			dragging = false;
		}

		// no hovering
		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			assert !pressed && !dragging;
			doRepaint();
			pinPos.setLocation(p.grid);
			doRepaint();
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			assert !pressed && !dragging;
			pressed = true;
			pinPos.setLocation(p.grid);
			labelPos.setLocation(p.grid);
			labelPos.x += input ? -10 : 10; // used when not dragging
			doRepaint();
		}


		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			assert pressed;
			dragging = true;
			doRepaint();
			labelPos.setLocation(p.grid);
			doRepaint();
		}

	    public Alignment getAlign() { return GUIHelper.getAlign(pinPos, labelPos); }


		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			assert pressed;
			doRepaint(); // call repaint with pressed=true
			if (dragging) labelPos.setLocation(p.grid); // only necessary due to precision issues
			CurrentProperties cp = CurrentProperties.getProps();

			Set<String> forbidden = new HashSet<String>();
			edc.getRoot().getIo().collectForbidden(forbidden);

			PinDialog pd = new PinDialog((Frame)PinTool.this.cp.getTopLevelAncestor(), forbidden);
			pd.setLocation(e.getPoint());
			pd.setBits(cp.getBits());
			pd.setInput(input);
			pd.setSigned(cp.isSigned());
			if (pd.ask()) {
				cp.setBits(pd.getBits());
				cp.setSigned(pd.isSigned());

				// make label position relative
				Point2D labelRelPos = new Point2D.Double(labelPos.x - pinPos.x, labelPos.y - pinPos.y);

				Pin pin = pd.isInput()
					? new Input(pd.getNewName(), pd.getBits(), pd.isSigned(), getAlign(), pinPos, labelRelPos)
					: new Output(pd.getNewName(), pd.getBits(),pd.isSigned(), getAlign(), pinPos, labelRelPos);


				edc.addNodeUndoable(edc.getRoot().getIo(), pin, true);
			}

			doRepaint(); // repaint before pressed, so we erase label
			
			pressed  = false;
			dragging = false;
			
			transition(defaultState, p, e);
		}

		Ellipse2D.Double         e     = new Ellipse2D.Double();
		PinLabelRenderNode.Cache cache = new PinLabelRenderNode.Cache();

		public void render(Graphics2D g) {
			super.render(g);
			Color c = PinRenderNode.getColor(input, !input);
			PinRenderNode.render(g, pinPos, c, e);
			if (pressed) {
				cache.update(g, g.getFont(), labelPos, "label", getAlign());
				cache.render(g, c);
			}
		}

		public void doRepaint() {
			Rectangle2D.Double r = new Rectangle2D.Double();
			GUIHelper.grow(1, PinRenderNode.getBound(pinPos), r);
			repaintInComponentSpace(r);
			if (pressed) {
				Graphics2D g = cp.getComponentGraphics();
				cache.update(g, g.getFont(), labelPos, "label", getAlign());
				repaintInComponentSpace(cache.getBound());
			}
		}

		public void onEntry() {
			super.onEntry();
			doRepaint();
		}
		public void onExit() {
			if (cp.isDisplayable()) doRepaint();
			super.onExit();
		}

		public void abort() {
			doRepaint();
			if (pressed) transition(defaultState); // need to catch release
			super.abort();
		}
	}
}
