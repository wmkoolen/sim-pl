/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

import namespace.ComponentTable;
import util.FileManager;
import util.GUIHelper;
import databinding.Complex;
import databinding.SubComponent;
import databinding.render.RegularControlPoint;
import editor.ComponentPanel;
import editor.RenderContext;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SubComponentTool extends Tool {

	public SubComponentTool() {
	}

	public boolean canHandle(databinding.Component component) { return component instanceof Complex; }

	protected State getDefaultState() {
		return new SubComponentDefaultState();
	}

	class SubComponentDefaultState extends DefaultState {
		public String getToolTipText() { return null; }
		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

//		// no hovering
//		public void move(Point2D p, MouseEvent e) {}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new SubComponentLockedState(p.grid));
		}
	}

	class SubComponentLockedState extends LockState {
		final Point2D pos = new Point2D.Double();
		final Rectangle2D bound_buf = new Rectangle2D.Double();
		
		public SubComponentLockedState(Point2D p) {
			pos.setLocation(p);
		}

		public String getToolTipText() { return pos.toString(); }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}
		
		public void onEntry() {
			doRepaint();
			super.onEntry();
		}
		
		public void onExit() {
			doRepaint();
			super.onExit();
		}	
		
		public void doRepaint() {
			bound_buf.setFrameFromDiagonal(pos, pos);
			GUIHelper.grow(Math.sqrt(2)*3+1, bound_buf, bound_buf);
			repaintInComponentSpace(bound_buf);			
		}
		
		
		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			doRepaint();
			pos.setLocation(p.grid);
			doRepaint();
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			try {
				assert pos.equals(p.grid);
				CurrentProperties cps = CurrentProperties.getProps();
				final Frame fparent = (Frame)SubComponentTool.this.cp.
					getTopLevelAncestor();
				Complex mainc = (Complex)edc.getRoot();

				Set<String> forbidden = new HashSet<String> ();
				mainc.getSubcomponents().collectForbidden(forbidden);

				FileManager fm = FileManager.getFileManager();
				File subcf = fm.askForFile(fparent, "Open SubComponent",
										   cps.getSubComponentFile(), "Open",
										   FileManager.filter_anycmp, FileManager.filter_sim_pl, FileManager.filter_xml);
				if (subcf == null) return;
				if (!subcf.isAbsolute()) subcf = subcf.getAbsoluteFile();

				assert subcf.isAbsolute();

				databinding.Component subc;
				try {
					subc = ComponentTable.load(subcf);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,
												  "File could not be opened\n" +
												  ex.getMessage(),
												  "Loading failed",
												  JOptionPane.ERROR_MESSAGE);
					return;
				}
				assert subc != null;
				cps.setSubComponentFile(subcf); // file loading worked, cash in

				// now ask user for properties
				SubComponentDialog pd = new SubComponentDialog(fparent, forbidden);
				pd.setNewName(mainc.getSubcomponents().createNewName(subc.getName()));
				pd.setLocation(e.getPoint());
				pd.setFlip(cps.getFlip());
				pd.setCollapsed(cps.isCollapsed());
				pd.setZoom(cps.getZoom());
				if (!pd.ask()) return;

				cps.setFlip(pd.getFlip()); // component properties worked, cash in
				cps.setCollapsed(pd.isCollapsed());
				cps.setZoom(pd.getZoom());

				RenderContext rc = new RenderContext(subc, false, false);
				Rectangle2D bound = rc.getRenderRoot().getDeepBound(cp.getComponentGraphics());

				double cogx = bound.getX() + bound.getWidth() / 2;
				double cogy = bound.getY() + bound.getHeight() / 2;

				// move bound by gridSize to make center of gravity as close to click
				// point as possible
				// TODO: use grid affinity of subcomponent here instead
				Point2D newp = new Point2D.Double(
								p.grid.getX() -
								Math.round(cogx / cp.getGridSize()) *
								cp.getGridSize(),
								p.grid.getY() -
								Math.round(cogy / cp.getGridSize()) *
								cp.getGridSize());

				//String data = RelativeDirectoryResolver.getRelativePath(ComponentTable.reverseLookup(edc.getRoot()).getParentFile(), pd.getData().getAbsoluteFile());
				SubComponent sc = new SubComponent(pd.getNewName(), subcf, newp, pd.getZoom(), pd.getFlip());
				edc.addNodeUndoable(mainc.getSubcomponents(), sc, true);
			} finally {
				transition(defaultState, p, e);
			}
		}

		public void render(Graphics2D g) {
			super.render(g);

			AffineTransform t = g.getTransform();
			g.translate(pos.getX(), pos.getY());
			g.rotate(Math.PI/4);
			g.setColor(RegularControlPoint.hoverColor);
			g.fillRect(-3,-3,6,6);
			g.setColor(Color.black);
			g.drawRect(-3,-3,6,6);
			g.setTransform(t);
		}
	}
}
