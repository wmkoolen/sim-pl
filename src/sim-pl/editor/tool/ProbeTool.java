/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.ListIterator;

import util.GUIHelper;
import util.GUIHelper.Alignment;
import databinding.Complex;
import databinding.InternalNode.MutatorList;
import databinding.render.ProbeRenderNode;
import databinding.render.RegularControlPoint;
import databinding.wires.Edge;
import databinding.wires.Node;
import databinding.wires.Probe;
import databinding.wires.Span;
import databinding.wires.Wires;
import editor.ComponentPanel;
import editor.ComponentPanel.CPPoint;
import editor.Editor;
import editor.EditorDocumentContext;
import editor.EditorDocumentContext.UndoableEditAction;
import editor.RenderAdapter.NodeDist;
import editor.tool.WireTool.EdgeStub;
import editor.tool.WireTool.NodeStub;
import editor.tool.WireTool.PotentialNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ProbeTool extends Tool {
	public ProbeTool() {
	}

	public boolean canHandle(databinding.Component component) { return component instanceof Complex; }

	protected State getDefaultState() {
		return new ProbeDefaultState();
	}

	public void forget() {
		pins.clear();
		gatherPins();
		super.forget();
	}

	public void connect(EditorDocumentContext edc, ComponentPanel cp, Editor editor) {
		super.connect(edc, cp, editor);
		assert pins.isEmpty();
		gatherPins();
	}

	public void disconnect() {
		super.disconnect();
		pins.clear();
	}

	public void render2(Graphics2D g)  {
		// render component and selection regularly
		super.render2(g);

		// then render pin markers on top
		Ellipse2D e = new Ellipse2D.Double();
		// render in reverse order, so the first pins show up on top
		// this is nice, for then you can select what you see
		for (ListIterator<PotentialNode> it = pins.listIterator(pins.size()); it.hasPrevious(); ) {
			PotentialNode n = it.previous();
			RegularControlPoint.draw(g, n.getPos(), e, n.getColor());
		}
	}



	private  void gatherPins() {
		Wires w = ((Complex)edc.getRoot()).getWires();
		w.addStubs(pins, false);
	}

	ArrayList<PotentialNode> pins = new ArrayList<PotentialNode>();


	public PotentialNode getCableEnd(ComponentPanel.CPPoint p) {
		for (PotentialNode n : pins) {
			if (n.getPos().distance(p.real) <= MAX_SELECT_DIST) {
				return n;
			}
		}

		NodeDist nd = edc.getRenderNode(((Complex)edc.getRoot()).getWires()).getNode(p.real, cp.getComponentGraphics(), flns);
		if (nd.d <= MAX_SELECT_DIST) {
			if (nd.node instanceof Node) {
				return new NodeStub((Node)nd.node);
			}
			if (nd.node instanceof Span) {
				Span s = (Span)nd.node;
				Edge e = s.getClosestEdge(p.real);
				return new EdgeStub(e, p.grid);
			}
		}
		return null;
	}



	class ProbeDefaultState extends DefaultState {

		public String getToolTipText() { return null; }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			PotentialNode ce = getCableEnd(p);
			if (ce != null) transition(new ProbeNodeHoverState(ce), p, e, false);
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(defaultState);
		}
	}

	class ProbeNodeHoverState extends CursorState {
		PotentialNode ps;

		public ProbeNodeHoverState(PotentialNode ps) {
			this.ps = ps;
			this.curPos.setLocation(ps.getPos());
		}

		public String getToolTipText() { return ps.getDescription(); }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			doRepaint();
			if (!ps.equals(getCableEnd(p))) {
			// we left the cable thingy
				transition(defaultState, p, e);
			} else {
				curPos.setLocation(p.grid);
				doRepaint();
			}
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
		}

		Point2D.Double curPos = new Point2D.Double();

		public void drag(CPPoint p, MouseEvent e) {
			doRepaint();
			curPos.setLocation(p.grid);
			doRepaint();
		}

		public void forget() {
			transition(defaultState);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			doRepaint();
			final EditorDocumentContext edc = ProbeTool.this.edc;
			final MutatorList muts = new MutatorList();
			Point2D probeRelPos = new Point2D.Double(curPos.x - ps.getPos().getX(), curPos.y - ps.getPos().getY());
			final Probe probe = new Probe(probeRelPos, getAlign());
			edc.doEdit(new UndoableEditAction() {

				public void perform() {
					Wires cg = ((Complex)edc.getRoot()).getWires();
					Node n = ps.realise(cg, muts);
					muts.add(n.add(probe));
					edc.getSelection().set(probe, edc);
				}

				public void undo() {
					super.undo();
					muts.undo();
					edc.getSelection().clear(edc);
				}

				public void redo() {
					super.redo();
					muts.redo();
					edc.getSelection().set(probe, edc);
				}

				public boolean requiresRecompile() {
					return true;
				}
			});

			transition(defaultState, p, e);
		}

		final Rectangle2D.Double r = new Rectangle2D.Double();
		final Line2D.Double      l = new Line2D.Double();

		private void doRepaint() {
			ProbeRenderNode.getBound(curPos, ps.getPos(), r);
			repaintInComponentSpace(r);
		}

		public void onEntry() {
			super.onEntry();
			doRepaint();
		}

		public void onExit() {
			if (cp.isDisplayable()) doRepaint();
			super.onExit();
		}

		public Alignment getAlign() { return GUIHelper.getAlign(ps.getPos(), curPos); }

		public void render(Graphics2D g) {
			super.render(g);
			ProbeRenderNode.render(g, curPos, getAlign(), ps.getPos(), !ps.isForNode(), l, r);
		}
	}
}
