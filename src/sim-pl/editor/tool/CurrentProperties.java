/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

import util.GUIHelper;
import databinding.Flip;
import databinding.IntResolver;
import databinding.SolidFill;
import databinding.StrokedOutline;
import databinding.myStroke;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class CurrentProperties {
	myStroke stroke = new myStroke(GUIHelper.unitStroke);
	SolidFill fill = null; //new SolidFill(Color.green);
	Font font = new Font("Serif", Font.PLAIN, 14);
	IntResolver bits = IntResolver.valueOf(1);
	IntResolver size = IntResolver.valueOf(1);
	HAlignment halign = HAlignment.center;
	VAlignment valign = VAlignment.center;
	Flip flip = Flip.none;
	boolean signed;
	boolean collapsed;
	Color color = Color.black;
	double zoom = 1;
	File subComponentFile;

	public myStroke getStroke() { return stroke; }
	public void setStroke(myStroke stroke) { this.stroke = stroke; }

	public SolidFill getFill() { return fill; }

	public Font getFont() { return font; }
	public void setFont(Font font) { assert font != null; this.font = font; }

	public HAlignment getHalign() { return halign; }
	public VAlignment getValign() { return valign; }

	public IntResolver getBits() { return bits; }
	public void setBits(IntResolver bits) { this.bits = bits; }

	public IntResolver getSize() { return size; }
	public void setSize(IntResolver size) { this.size = size; }


	public boolean isSigned() { return signed; }
	public void setSigned(boolean s) { signed = s; }

	public Color getColor() { return color; }
	public void setColor(Color color) { assert color != null; this.color = color; }

	public Flip getFlip() { return flip; }
	public void setFlip(Flip  flip) { this.flip = flip; }

	public double getZoom() { return zoom; }
	public void setZoom(double zoom) { this.zoom = zoom; }

	public boolean isCollapsed() { return collapsed; }
	public void setCollapsed(boolean collapsed) { this.collapsed = collapsed; }

	public File getSubComponentFile() { return subComponentFile; }
	public void setSubComponentFile(File subComponentFile) { this.subComponentFile= subComponentFile; }


	/** signleton design pattern **/
	private CurrentProperties() {
	}

	private static CurrentProperties p;

	public static CurrentProperties getProps() {
		if (p == null) p = new CurrentProperties();
		return p;
	}

	public StrokedOutline getOutline() {
		return new StrokedOutline(getColor(), getStroke());
	}


}
