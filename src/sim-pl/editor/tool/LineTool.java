/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.BasicStroke;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.forms.Line;
import editor.ComponentPanel;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class LineTool extends Tool {
	protected State getDefaultState() {
		return new LineDefaultState();
	}

	class LineDefaultState extends DefaultState {
		public String getToolTipText() { return null; }
		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new LineLockState(p));
		}
	}

	class LineLockState extends LockState {
		final ComponentPanel.CPPoint origin;

		public LineLockState(ComponentPanel.CPPoint origin) {
			this.origin = origin;
		}

		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new LineState(origin.grid, origin.grid), p, e, true);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			// clicking nothing (without dragging) empties selection
			edc.getSelection().clear(cp);
			transition(defaultState, p, e);
		}

//		public void render(Graphics2D g) {
//			super.render(g);
//			CurrentProperties cp = CurrentProperties.getProps();
//			g.setStroke(cp.getStroke().getStroke());
//			g.setColor(cp.getStroke().getColor());
//			g.draw(l);
//		}
	}

	class LineState extends CursorState {
		Line2D l = new Line2D.Double();
		Rectangle2D bound_buf = new Rectangle2D.Double();
		
		public LineState(Point2D from, Point2D to) {
			l.setLine(from,to);
		}


		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) { throw new Error("Impossible"); }

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {throw new Error("Impossible"); }

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			bound_buf.setFrameFromDiagonal(l.getP1(), l.getP2());
			l.setLine(l.getP1(), p.grid);
			bound_buf.add(p.grid);
			
			CurrentProperties cps = CurrentProperties.getProps();
			double width = cps.getStroke().getLineWidth()/2;
			if (cps.getStroke().getEndCap() == BasicStroke.CAP_SQUARE)
				width *= Math.sqrt(2);
			GUIHelper.grow(width, bound_buf, bound_buf);
			
			repaintInComponentSpace(bound_buf);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			CurrentProperties cp = CurrentProperties.getProps();
			Line line = new Line(cp.getStroke(), cp.getColor(), l.getP1(), l.getP2());
			edc.addNodeUndoable(edc.getRoot().getForms(), line, false);
			transition(defaultState, p, e);
		}

		public void forget() {
			transition(defaultState);
		}

		public void render(Graphics2D g) {
			super.render(g);
			CurrentProperties cp = CurrentProperties.getProps();
			g.setStroke(cp.getStroke().getStroke());
			g.setColor(cp.getColor());
			g.draw(l);
		}
	}

}
