/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.IntResolver;
import databinding.Memory;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class StorageDialog extends NewElementDialog implements ChangeListener{
	public StorageDialog(Frame owner) {
		super(owner, "New Storage");
		jbInit();
	}




	String name;
	public String getNewName() {
		return name;
	}

	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return name != null;
		}

		protected void revalidate() {
			name = jTextField1.getText().length() > 0 ? jTextField1.getText() : null;
		}
	};



	IntResolver bits;
	public IntResolver getBits() { return bits; }
	public void setBits(IntResolver bits) { jTextField2.setText(bits.toString()); assert this.bits.equals(bits);}
	ValidityManager rdl2 = new ValidityManager() {
		public boolean isValid() {
			return bits != null;
		}

		protected void revalidate() {
			try {
				bits = IntResolver.valueOf(jTextField2.getText());
			} catch (Exception ex) {
				bits = null;
			}
		}
	};

	IntResolver size;
	public IntResolver getArraySize() { return size; }
	public void setArraySize(IntResolver size) { jTextField3.setText(size.toString()); assert this.size.equals(size);}

	ValidityManager rdl3 = new ValidityManager() {
		public boolean isValid() {
			return size != null;
		}

		protected void revalidate() {
			try {
				size = IntResolver.valueOf(jTextField3.getText());
			} catch (Exception ex) {
				size = null;
			}
		}
	};



	public boolean isSigned() {
		return jCheckBox1.isSelected();
	}

	public void setSigned(boolean b) {
		jCheckBox1.setSelected(b);
	}



	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid() && rdl2.isValid() && rdl3.isValid());
	}


	private void jbInit()  {
		setIcon(IconManager.chargeIcon(Memory.class, BeanInfo.ICON_COLOR_32x32));


		jLabel1.setLabelFor(jTextField1);
		jLabel2.setLabelFor(jTextField2);
		jLabel3.setLabelFor(jTextField3);
		jLabel4.setLabelFor(jCheckBox1);


		jTextField2.setColumns(20);
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

		jLabel1.setText("Name: ");
		jLabel2.setText("Bits: ");
		jLabel3.setText("Size: ");
		jLabel4.setText("Signed: ");
		jPanel1.setLayout(gridBagLayout1);

		jCheckBox1.setHorizontalAlignment(SwingConstants.CENTER);
		jPanel1.add(jLabel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField2, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField3, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jCheckBox1, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));


		wire(jTextField1, rdl1, this);
		wire(jTextField2, rdl2, this);
		wire(jTextField3, rdl3, this);
	}

	JPanel jPanel1 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	JTextField jTextField1 = new JTextField();
	JTextField jTextField2 = new JTextField();
	JLabel jLabel3 = new JLabel();
	JTextField jTextField3 = new JTextField();
	JCheckBox jCheckBox1 = new JCheckBox();
	JLabel jLabel4 = new JLabel();


}
