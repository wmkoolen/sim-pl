/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;

import util.GUIHelper;
import databinding.BasicEditorNode;
import editor.ComponentPanel;
import editor.RenderAdapter;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Select extends Tool {
	private static final Color selectFillColor =   new Color(0, 0, 0x70, 0x10);
	private static final Color selectLineColor =   new Color(0,0, 0x70, 0xFF);
	private static final BasicStroke selectLineStroke = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);

	public Select() {
	}

	protected State getDefaultState() {
		return new SelectDefaultState();
	}

	class SelectDefaultState extends DefaultState {

		public String getToolTipText() { return null; }

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			if (!e.isShiftDown()) {
				edc.getSelection().clear(cp);
			}
			transition(new LockRubberBand(p));
		}

	}
	class LockRubberBand extends LockState {
		ComponentPanel.CPPoint origin;
		public LockRubberBand(ComponentPanel.CPPoint origin) {
			this.origin = origin;
		}

		public String getToolTipText() { return null; }

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new MoveRubberBand(origin, origin), p, e, true);
		}


		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			RenderAdapter.NodeDist<BasicEditorNode<?>> nd = edc.getRenderRoot().getNode(p.real, cp.getComponentGraphics(), flns);
			if (nd.d <= MAX_SELECT_DIST) {
				if (e.isShiftDown()) {
					edc.getSelection().toggle(nd.node, cp);
				} else {
					edc.getSelection().set(nd.node, cp);
				}
			} else {
				if (e.isShiftDown()) {
				} else {
					edc.getSelection().clear(cp);
				}
			}
			transition(defaultState,p,e);
		}
	}

	class MoveRubberBand extends MoveState {
		ComponentPanel.CPPoint dragTarget;
		Rectangle2D r = new Rectangle2D.Double(); // draw buffer
		Rectangle2D bound_buf = new Rectangle2D.Double(); // bound buffer

		public MoveRubberBand(ComponentPanel.CPPoint dragOrigin, ComponentPanel.CPPoint dragTarget) {
			super(dragOrigin);
			this.dragTarget = dragTarget;
			r.setFrameFromDiagonal(dragOrigin.real, dragTarget.real);
		}

		public String getToolTipText() { return null; }
		
		private void registerRedraw() {
			bound_buf.setFrameFromDiagonal(dragOrigin.real, dragTarget.real);
			GUIHelper.grow(selectLineStroke.getLineWidth()/2, bound_buf, bound_buf);
			repaintInComponentSpace(bound_buf);
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			registerRedraw();

			// we manage the selection rectangle in component coordinates
			dragTarget = p;
			r.setFrameFromDiagonal(dragOrigin.real, dragTarget.real);
			
			registerRedraw();		
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			registerRedraw();
			
			Set<BasicEditorNode> selection = new HashSet<BasicEditorNode> ();
			edc.getRenderRoot().collectEnclosedNodes(cp.getComponentGraphics(),
									  r, selection);

			if (e.isShiftDown()) edc.getSelection().add(selection, cp);
			else                 edc.getSelection().set(selection, cp);
			transition(defaultState, p,e);
		}

//		private Rectangle2D getSelectionRect() {
//			assert dragOrigin != null && dragTarget != null;
//			double x1 = Math.min(dragOrigin.getX(), dragTarget.getX());
//			double y1 = Math.min(dragOrigin.getY(), dragTarget.getY());
//			double x2 = Math.max(dragOrigin.getX(), dragTarget.getX());
//			double y2 = Math.max(dragOrigin.getY(), dragTarget.getY());
//			return new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);
//		}


		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);}

		public void forget() {
			transition(defaultState);
		}

		// draw rubber band
		public void render(Graphics2D g) {
			super.render(g);

			g.setColor(selectFillColor);
			g.fill(r);

			g.setStroke(selectLineStroke);
			g.setColor(selectLineColor);
			g.draw(r);
		}
	}



}
