/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JOptionPane;

import util.GUIHelper;
import databinding.Complex;
import databinding.InternalNode.Mutator;
import databinding.InternalNode.MutatorList;
import databinding.Pin;
import databinding.SubComponent;
import databinding.SubPinStub;
import databinding.render.PinRenderNode;
import databinding.render.RegularControlPoint;
import databinding.wires.Edge;
import databinding.wires.MainPinStub;
import databinding.wires.Node;
import databinding.wires.Probe;
import databinding.wires.Span;
import databinding.wires.Split;
import databinding.wires.SubPinNode;
import databinding.wires.Wires;
import editor.ComponentPanel;
import editor.Editor;
import editor.EditorDocumentContext;
import editor.EditorDocumentContext.UndoableEditAction;
import editor.RenderAdapter.NodeDist;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class WireTool extends Tool {
	public WireTool() {
	}

	public boolean canHandle(databinding.Component component) { return component instanceof Complex; }

	protected State getDefaultState() {
		return new CableDefaultState();
	}

	public static interface PotentialNode {
		public Point2D getPos();
		public Node realise(Wires cg, MutatorList muts);
		public String getDescription();
		public Color getColor();

		public BasicStroke getWireStroke();

		public Color getWireColor();

		public boolean isForNode();
		
		public boolean equals(PotentialNode o);
	}

	public void forget() {
		pins.clear();
		gatherPins();
		super.forget();
	}


	private static Color getHoverColor(Color c) { return c.brighter(); }
	private static Color getSelectedColor(Color c) { return c.darker(); }


	public static class NodeStub implements PotentialNode {
		final Node node;

		public Point2D getPos() { return node.getPos(); }

		public NodeStub(Node node) {
			this.node = node;
			assert
				node instanceof MainPinStub ||
				node instanceof SubPinNode ||
				node instanceof Split;
		}

		public Node realise(Wires cg, MutatorList muts) {
			return node;
		}

		public String getDescription() { return getNodeDescription(node); }

		public Color getColor() {
			if (node instanceof MainPinStub) {
				return PinRenderNode.getColor(((MainPinStub)node).getPin());
			}
			if (node instanceof SubPinNode) {
				SubPinStub sps = ((SubPinNode)node).getStub();
				return sps.isResolved()
					? PinRenderNode.getColor(sps.getPin())
					: Color.red;
			}
			if (node instanceof Split) {
				return ((Split)node).getParent().getParent().getColor();
			}
			throw new Error("Impossible");
		}

		public BasicStroke getWireStroke() {
			return node.getParent().getParent().getStroke().getStroke();
		}

		public Color getWireColor() {
			return node.getParent().getParent().getColor();
		}

		public boolean isForNode() {
			return !(node instanceof Split);
		}

		public boolean equals(PotentialNode o) {
			return (o instanceof NodeStub) && node == ((NodeStub)o).node;
		}
	}

	public static class StubStub implements PotentialNode {
		SubComponent s;
		Pin p;

		public Point2D getPos() {
			return s.getTransform().transform(p.getPos(), null);
		}

		public StubStub(SubComponent s, Pin p) {
			this.s = s;
			this.p = p;
		}

		public Node realise(Wires cg, MutatorList muts) {
			SubPinStub sps = new SubPinStub(p);
			SubPinNode spn = new SubPinNode(sps);
			sps.setNode(spn);

			muts.add(s.getSubPinStubs().add(sps));

			return spn;
		}

		public String getDescription() { return s.getName() + ":" + p.getName(); }

		public Color getColor() {
			return PinRenderNode.getColor(p);
		}

		public BasicStroke getWireStroke() {
			CurrentProperties cp = CurrentProperties.getProps();
			return cp.getStroke().getStroke();
		}

		public Color getWireColor() {
			CurrentProperties cp = CurrentProperties.getProps();
			return cp.getColor();
		}

		public boolean isForNode() {
			return true;
		}
		
		public boolean equals(PotentialNode o) {
			return (o instanceof StubStub) && 
					s == ((StubStub)o).s &&
					p == ((StubStub)o).p;
		}
	}


	public static class StubMainStub implements PotentialNode {
		Pin p;

		public Point2D getPos() {
			return p.getPos();
		}

		public StubMainStub(Pin p) {
			assert p.getNode() == null;
			this.p = p;
		}

		public Node realise(Wires cg, MutatorList muts) {
			final MainPinStub mps = new MainPinStub(p);

			muts.add(new Mutator() {
				protected void pre()      { assert p.getNode() == null;}
				protected void commit()   { p.setNode(mps);  }
				protected void post()     { assert p.getNode() == mps; }
				protected void rollback() { p.setNode(null); }
			}.perform());
			return mps;
		}

		public String getDescription() { return p.getName(); }

		public Color getColor() {
			return PinRenderNode.getColor(p);
		}

		public BasicStroke getWireStroke() {
			CurrentProperties cp = CurrentProperties.getProps();
			return cp.getStroke().getStroke();
		}

		public Color getWireColor() {
			CurrentProperties cp = CurrentProperties.getProps();
			return cp.getColor();
		}

		public boolean isForNode() {
			return true;
		}
		
		public boolean equals(PotentialNode o) {
			return (o instanceof StubMainStub) && 
					p == ((StubMainStub)o).p;
		}
	}



	static class EdgeStub implements PotentialNode {
		Edge edge;
		// the split point
		Point2D p = new Point2D.Double();

		public Point2D getPos() { return p; }


		public EdgeStub(Edge edge, Point2D p) {
			this.edge = edge;
			this.p.setLocation(p);
		}

		public Node realise(Wires cg, MutatorList muts) {
//			CurrentProperties cps = CurrentProperties.getProps();
			Split s = cg.splitEdge(edge, p, muts);
			return s;
		}

		public String getDescription() { return "Split edge here"; }

		public Color getColor() {
			return edge.getSpan().getParent().getParent().getColor();
		}

		public BasicStroke getWireStroke() {
			return edge.getSpan().getParent().getParent().getStroke().getStroke();
		}

		public Color getWireColor() {
			return edge.getSpan().getParent().getParent().getColor();
		}

		public boolean isForNode() {
			return false;
		}
		
		public boolean equals(PotentialNode o) {
			return (o instanceof EdgeStub) && 
					edge == ((EdgeStub)o).edge &&
					p.equals(((EdgeStub)o).p);					
		}
	}


/*
	class SubPinStub implements CableEnd {
		SubComponent s;
		Pin pin;
		Point2D p;

		public Point2D getPoint() { return p; }

		public SubPinStub(SubComponent s, Pin pin) {
			this.p = p;
			this.pin = pin;
		}

		public Node getNode(Wires cg, List<Mutator> muts) {
			databinding.wires.PinStub ps = cg.getPinStub(pinref);
			if (ps != null) return ps;
			throw new Error("Implement");
//			return cg.addWire(pinref, muts);
		}
	}

	class MainPinStub implements CableEnd {
		String pinref;
		Point2D p;
		Pin pin;

		public Point2D getPoint() { return p; }

		public MainPinStub(String pinref, Point2D p, Pin pin) {
			this.p = p;
			this.pinref = pinref;
			this.pin = pin;
		}

		public Node getNode(Wires cg, List<Mutator> muts) {
			databinding.wires.PinStub ps = cg.getPinStub(pinref);
			if (ps != null) return ps;
			throw new Error("Implement");
//			return cg.addWire(pinref, muts);
		}
	}
	*/

	private  void gatherPins() {
		Wires w = ((Complex)edc.getRoot()).getWires();
		w.addStubs(pins, true);
	}

	ArrayList<PotentialNode> pins = new ArrayList<PotentialNode>();


	public void connect(EditorDocumentContext edc, ComponentPanel cp, Editor editor) {
		super.connect(edc, cp, editor);
		assert pins.isEmpty();
		gatherPins();
	}

	public void disconnect() {
		super.disconnect();
		pins.clear();
	}

	public void render2(Graphics2D g)  {
		// render component and selection regularly
		super.render2(g);

		// then render pin markers on top
		Ellipse2D e = new Ellipse2D.Double();
		// render in reverse order, so the first pins show up on top
		// this is nice, for then you can select what you see
		for (ListIterator<PotentialNode> it = pins.listIterator(pins.size()); it.hasPrevious(); ) {
			PotentialNode n = it.previous();
			RegularControlPoint.draw(g, n.getPos(), e, n.getColor());
		}
	}




	public PotentialNode getCableEnd(ComponentPanel.CPPoint p) {
		for (PotentialNode n : pins) {
			if (n.getPos().distance(p.real) <= MAX_SELECT_DIST) {
				return n;
			}
		}

		NodeDist nd = edc.getRenderNode(((Complex)edc.getRoot()).getWires()).getNode(p.real, cp.getComponentGraphics(), flns);
		if (nd.d <= MAX_SELECT_DIST) {
			if (nd.node instanceof Node) {
				return new NodeStub((Node)nd.node);
			}
			if (nd.node instanceof Probe) {
				return new NodeStub(((Probe)nd.node).getParent());
			}
			if (nd.node instanceof Span) {
				Span s = (Span)nd.node;
				Edge e = s.getClosestEdge(p.real);
				return new EdgeStub(e, p.grid);
			}

		}
		return null;
	}



	class CableDefaultState extends DefaultState {

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			PotentialNode ce = getCableEnd(p);
			if (ce != null) {
				transition(new CableEndHoverState(ce));
				return;
			}
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
		}

		public String getToolTipText() { return null; }
	}

	class CableEndHoverState extends HoverState {
		PotentialNode ps;

		public CableEndHoverState(PotentialNode ps) {
			this.ps = ps;
		}

		public String getToolTipText() { return ps.getDescription(); }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			if (!ps.equals(getCableEnd(p))) {
			// check if we leave the cable thingy
				transition(defaultState, p, e);
			}
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			ArrayList<Point2D> points = new ArrayList<Point2D>();
//			points.add(ps.getPoint());
			transition(new AfterInitialPressState(ps, points));
		}

		public void onEntry() {
			super.onEntry();
			repaintInComponentSpace(RegularControlPoint.getBound(ps.getPos(), null));
		}

		public void onExit() {
			if (cp.isDisplayable()) repaintInComponentSpace(RegularControlPoint.getBound(ps.getPos(), null));
			super.onExit();
		}

		public void render(Graphics2D g) {
			super.render(g);
			RegularControlPoint.draw(g, ps.getPos(), new Ellipse2D.Double(), getHoverColor(ps.getColor()));
		}

	}

//	class CableHoverState extends HoverState {
//		BasicEditorNode cablePart;
//
//		CableHoverState(BasicEditorNode cablePart) {
//			this.cablePart = cablePart;
//		}
//
//		public String getToolTipText() { return getNodeDescription(cablePart); }
//
//		public Cursor getCursor() {
//			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
//		}
//
//		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
//			transition(defaultState, p, e);
//		}
//
//		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
//			// TODO: fix this
//			transition(abortedState);
//		}
//	}


	abstract class CableWorkingState extends CursorState {
		final PotentialNode ce;
		final ArrayList<Point2D> points;

		// buffers
		final Line2D l = new Line2D.Double();
		final Ellipse2D e = new Ellipse2D.Double();

		CableWorkingState(PotentialNode ce, ArrayList<Point2D> points) {
			assert ce != null;
			this.ce = ce;
			this.points = points;
		}

		public void render(Graphics2D g) { throw new Error("Superseded method called"); };
		
		private Rectangle2D.Double getLastSegmentBound(Graphics2D g, Point2D marked) {
			Rectangle2D.Double r = new Rectangle2D.Double();
			Point2D penultimate = points.isEmpty() 
					? ce.getPos() 
					: points.get(points.size()-1);
			
			r.setFrameFromDiagonal(penultimate, marked);
			GUIHelper.grow(ce.getWireStroke().getLineWidth()/2, r, r);
			
			Rectangle2D.union(r, RegularControlPoint.getBound(penultimate, null), r);
			Rectangle2D.union(r, RegularControlPoint.getBound(marked, null), r);
			
			return r;
		}

		public void render(Graphics2D g, Point2D marked, Color c) {
			super.render(g);

			g.setStroke(ce.getWireStroke());
			g.setColor(ce.getWireColor());


			if (points.isEmpty()) {
				l.setLine(ce.getPos(), marked);
				g.draw(l);
			} else {
				l.setLine(ce.getPos(), points.get(0));
				g.draw(l);

				for (int i = 1; i < points.size(); i++) {
					l.setLine(points.get(i - 1), points.get(i));
					g.draw(l);
				}
				l.setLine(points.get(points.size() - 1), marked);
				g.draw(l);

				for (Point2D pt : points) {
					RegularControlPoint.draw(g, pt, e, RegularControlPoint.defaultColor);
				}

			}
			RegularControlPoint.draw(g, ce.getPos(), e, ce.getColor());
			RegularControlPoint.draw(g, marked, e, c);
		}
	}


	class AfterInitialPressState extends CableWorkingState {
		public AfterInitialPressState(PotentialNode ce, ArrayList<Point2D> points) {
			super(ce, points);
		}

		public String getToolTipText() { return null; }

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("impossible");
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("impossible");
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			// nothing happens, we clicked a fixed point
			// just wait for the release.
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new CableReleasedState(ce, points, p.grid), p, e, false);
		}

		public void forget() {
			transition(defaultState);
		}

		public void render(Graphics2D g) {
			super.render(g, ce.getPos(), getSelectedColor(ce.getColor()));
		}

	}


	class CableReleasedState extends CableWorkingState {
		Point2D mouse = new Point2D.Double();
		Rectangle2D r1 = new Rectangle2D.Double();
		Rectangle2D r2 = new Rectangle2D.Double();
		PotentialNode hover = null;


		public CableReleasedState(PotentialNode ce, ArrayList<Point2D> points, Point2D mouse) {
			super(ce, points);
			this.mouse.setLocation(mouse);
		}

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public String getToolTipText() { return null; }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			Point2D prev = points.isEmpty() ? ce.getPos() : points.get(points.size()-1);
			Point2D cur  = hover != null ? hover.getPos() : mouse;
			r1.setFrameFromDiagonal(prev, cur);
			mouse.setLocation(p.grid);
			hover = getCableEnd(p);
			r1.add(hover != null ? hover.getPos() : mouse);
			repaintInComponentSpace(GUIHelper.grow(RegularControlPoint.radius+RegularControlPoint.strokeWidth/2, r1, r1));
		}


		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			if (hover == null || ce.equals(hover)) {
				transition(new CablePressedState(ce, points, mouse), p, e, true);
			} else {
				try {
					edc.doEdit(new UndoableEditAction() {
						EditorDocumentContext edc = WireTool.this.edc;
						MutatorList muts = new MutatorList();
						Span s;
						public void perform() {
							Wires cg = ((Complex)edc.getRoot()).getWires();
							Node n1, n2;
							if (ce instanceof EdgeStub && hover instanceof EdgeStub && ((EdgeStub)ce).edge == ((EdgeStub)hover).edge) {
								EdgeStub es1 = (EdgeStub)ce;
								EdgeStub es2 = (EdgeStub)hover;
								if (es1.p.distance(es1.edge.getNode1().getPos()) >
									es2.p.distance(es1.edge.getNode1().getPos())) {
										EdgeStub t = es1; es1 = es2; es2 = t;
								}
								Span x = es1.edge.getSpan();
								int i = x.edges.indexOf(es1.edge);
								n1 = cg.splitEdge(x.edges.get(i),   es1.p, muts);
								n2 = cg.splitEdge(x.edges.get(i+1), es2.p, muts);
							} else {
								n1 = ce.realise(cg, muts);
								n2 = hover.realise(cg, muts);
							}
							s = cg.connect(n1, n2, points, muts);
							edc.getSelection().set(s, edc);
						}

						public void undo() {
							super.undo();
							muts.undo();
							edc.getSelection().clear(edc);
						}

						public void redo() {
							super.redo();
							muts.redo();
							edc.getSelection().set(s, edc);
						}

						public boolean requiresRecompile() {
							return true;
						}
					});
					pins.clear(); gatherPins();
					transition(defaultState, p, e, true); // wait for button release
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(cp, ex.getMessage(), "Cable Error", JOptionPane.ERROR_MESSAGE);
					ex.printStackTrace();
					// JOptionPane catches button release
					transition(defaultState, p, e);
				}
				cp.repaint();
			}
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("Impossible");
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("Impossible");
		}

		public void forget() {
			transition(defaultState);
		}

		public void render(Graphics2D g) {
			if (hover == null) {
				super.render(g, mouse, RegularControlPoint.defaultColor);
			} else {
				super.render(g, hover.getPos(), getHoverColor(hover.getColor()));
			}
		}
	}

	class CablePressedState extends CableWorkingState {
		Point2D pt = new Point2D.Double();

		public CablePressedState(PotentialNode ce, ArrayList<Point2D> points, Point2D pt) {
			super(ce, points);
			this.pt.setLocation(pt);
		}
		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
		}
		public String getToolTipText() { return null; }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("impossible");
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("impossible");
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			Graphics2D cg = cp.getComponentGraphics();
			repaintInComponentSpace(super.getLastSegmentBound(cg, pt));
			
			pt.setLocation(p.grid);
			
			repaintInComponentSpace(super.getLastSegmentBound(cg, pt));
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			points.add(pt);
			transition(new CableReleasedState(ce, points, p.grid), p, e, false);
		}

		public void forget() {
			transition(defaultState);
		}
		public void render(Graphics2D g) {
			super.render(g, pt, RegularControlPoint.selectedColor);
		}
	}

}
