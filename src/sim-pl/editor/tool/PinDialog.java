/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;
import java.util.Set;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.IO;
import databinding.Input;
import databinding.IntResolver;
import databinding.Output;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PinDialog extends NewElementDialog implements ChangeListener {

	Set<String> forbidden;

	public PinDialog(Frame owner, Set<String> forbidden) {
		super(owner, "New Pin");
		assert forbidden != null;
		this.forbidden = forbidden;
		jbInit();
	}

	// name
	String name;
	public String getNewName() { return name; }
//	public void setNewName(String s) { assert s != null; jTextField1.setText(s); }

	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return name != null;
		}

		protected void revalidate() {
			name = jTextField1.getText();
			if (name.length() == 0 || forbidden.contains(name)) name = null;
		}
	};


	// bits
	IntResolver bits;
	public IntResolver getBits() { return bits; }
	public void setBits(IntResolver b) { jTextField2.setText(b.toString()); }

	ValidityManager rdl2 = new ValidityManager() {
		public boolean isValid() {
			return bits != null;
		}

		protected void revalidate() {
			try {
				bits = IntResolver.valueOf(jTextField2.getText());
			} catch (Exception ex) {
				bits = null;
			}
		}
	};


	// signedness
	public boolean isSigned() { return jCheckBox1.isSelected(); }
	public void setSigned(boolean b) { jCheckBox1.setSelected(b); }

	// input/output selection
	public boolean isInput() { return jRadioButton3.isSelected(); }

	public void setInput(boolean b) {
		jRadioButton3.setSelected(b);
		jRadioButton4.setSelected(!b);
	}


	// color
//	public Color getColor() { return jPanel2.getBackground(); }
//	public void setColor(Color c) { jPanel2.setBackground(c); }





	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid() && rdl2.isValid());
	}



	private void jbInit() {
		jLabel1.setLabelFor(jTextField1);
		jLabel2.setLabelFor(jTextField2);
		jLabel3.setLabelFor(jCheckBox1);
//		jLabel4.setLabelFor(jPanel2);
//		jPanel2.setBackground(Color.green);
		jCheckBox1.setHorizontalAlignment(SwingConstants.CENTER);
//		jRadioButton3.setText("Input");
//		jRadioButton4.setText("Output");
		jLabel5.setLabelFor(jPanel3);
		jLabel5.setText("Type: ");
		setIcon(IconManager.chargeIcon(IO.class, BeanInfo.ICON_COLOR_32x32));
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		jLabel2.setText("Bits: ");
		jTextField1.setColumns(20);
		jLabel3.setText("Signed: ");
//		jLabel4.setText("Color: ");
		jLabel1.setText("Name: ");
		jPanel1.setLayout(gridBagLayout1);

		jPanel3.add(jRadioButton3);
		JLabel l3 = new JLabel("Input", IconManager.chargeIcon(Input.class, BeanInfo.ICON_COLOR_32x32), JLabel.RIGHT);
		l3.setLabelFor(jRadioButton3);
		jPanel3.add(l3);

		jPanel3.add(Box.createHorizontalStrut(20));

		jPanel3.add(jRadioButton4);
		JLabel l4 = new JLabel("Output", IconManager.chargeIcon(Output.class, BeanInfo.ICON_COLOR_32x32), JLabel.RIGHT);
		l4.setLabelFor(jRadioButton4);
		jPanel3.add(l4);

		buttonGroup1.add(jRadioButton3);
		buttonGroup1.add(jRadioButton4);
		jRadioButton3.setSelected(true); // enable one radio button

		jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField2, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jCheckBox1, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jLabel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
//			, GridBagConstraints.WEST, GridBagConstraints.NONE,
//			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jPanel2, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
//			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
//			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jPanel3, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));


		wire(jTextField1, rdl1, this);
		wire(jTextField2, rdl2, this);
	}

	JPanel jPanel1 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	JTextField jTextField1 = new JTextField();
	JTextField jTextField2 = new JTextField();
	JLabel jLabel3 = new JLabel();
	JCheckBox jCheckBox1 = new JCheckBox();
//	JLabel jLabel4 = new JLabel();
//	JPanel jPanel2 = new JPanel();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JPanel jPanel3 = new JPanel();
	JRadioButton jRadioButton3 = new JRadioButton();
	JRadioButton jRadioButton4 = new JRadioButton();
	JLabel jLabel5 = new JLabel();

}
