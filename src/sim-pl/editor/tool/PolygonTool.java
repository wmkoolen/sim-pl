/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import util.GUIHelper;
import databinding.forms.PPoint;
import databinding.forms.Polygon;
import databinding.render.RegularControlPoint;
import editor.ComponentPanel;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PolygonTool extends Tool {

	protected State getDefaultState() {
		return new PolygonDefaultState();
	}

	class PolygonDefaultState extends DefaultState {
		public String getToolTipText() { return null; }
		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			ArrayList<Point2D> ps = new ArrayList<Point2D>();
			ps.add(p.grid);
			edc.getSelection().clear(cp);
			cp.repaint(); // TODO: be smart
			transition(new SelectingPointsPressed(ps));
		}
	}

	abstract class SelectingState extends CursorState {
		ArrayList<Point2D> ps = new ArrayList<Point2D>();
		boolean closeMark = false;
		public SelectingState(ArrayList<Point2D> ps) {
			this.ps = ps;
		}

		public Cursor getCursor() {
			return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
		}

		public void checkMark(Point2D p) {
			closeMark = ps.size() >= 3 && ps.get(0).distance(p) <= MAX_SELECT_DIST;
		}
		public void render(Graphics2D g) {
			super.render(g);

			Ellipse2D e = new Ellipse2D.Double(0,0,5,5);
			Line2D l = new Line2D.Double();
			CurrentProperties cp = CurrentProperties.getProps();

			if (cp.getStroke() != null) {
				g.setStroke(cp.getStroke().getStroke());
				g.setColor(cp.getColor());

				for (int i = 1; i < ps.size(); i++) {
					l.setLine(ps.get(i - 1), ps.get(i));
					g.draw(l);
				}
			}
			for (Point2D p : ps) {
				RegularControlPoint.draw(g, p, e, RegularControlPoint.defaultColor);
			}
			if (closeMark) {
				RegularControlPoint.draw(g, ps.get(0), e, RegularControlPoint.selectedColor);
			}
		}


	}

	class SelectingPointsReleased extends SelectingState {
		Point2D mouse = new Point2D.Double();

		// buffers
		Rectangle2D.Double r1 = new Rectangle2D.Double();

		public SelectingPointsReleased(Point2D mouse, ArrayList<Point2D> ps) {
			super(ps);
			this.mouse.setLocation(mouse);
		}

		public String getToolTipText() { return null; }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			r1.setFrameFromDiagonal(ps.get(ps.size()-1), closeMark ? ps.get(0) : mouse);
			mouse.setLocation(p.grid);
			checkMark(p.real);
			r1.add(closeMark ? ps.get(0) : mouse);
			GUIHelper.grow(RegularControlPoint.radius + RegularControlPoint.strokeWidth, r1, r1);
			repaintInComponentSpace(r1);
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			ps.add(p.grid);
			transition(new SelectingPointsPressed(ps),p,e,true);
		}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) { throw new Error("Impossible"); }
		public void release(ComponentPanel.CPPoint p, MouseEvent e) { throw new Error("Impossible"); }

		public void render(Graphics2D g) {
			if (closeMark) {
				ps.add(ps.get(0)); // redraw first point too
				super.render(g);
				ps.remove(ps.size()-1);
			} else {
				ps.add(mouse);
				super.render(g);
				ps.remove(ps.size()-1);
			}
		}

		public void forget() {
			transition(defaultState);
		}
	}

	class SelectingPointsPressed extends SelectingState {

		public SelectingPointsPressed(ArrayList<Point2D> ps) {
			super(ps);
		}

		public String getToolTipText() { return null; }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {throw new Error("Impossible");}
		public void press(ComponentPanel.CPPoint p, MouseEvent e) {throw new Error("Impossible");}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			ps.get(ps.size()-1).setLocation(p.grid);
			checkMark(p.real);
			cp.repaint(); // TODO: smarter repaint
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			if (closeMark) {
				// by closing the polygon, we duplicated the first point
				ps.remove(ps.size()-1);

				CurrentProperties cp = CurrentProperties.getProps();
				Polygon pnode = new Polygon(cp.getOutline(), cp.getFill());
				for (Point2D pt : ps) {
					PPoint pp = new PPoint(pt);
					pnode.getChildren().add(pp);
					pp.setParent(pnode);
				}
				edc.addNodeUndoable(edc.getRoot().getForms(), pnode, false);
				transition(defaultState, p, e);
			} else {
				transition(new SelectingPointsReleased(p.grid, ps));
			}
			cp.repaint();
		}

		public void forget() {
			transition(defaultState);
		}

	}
}
