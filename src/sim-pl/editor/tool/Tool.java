/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.IMoveable;
import databinding.InternalNode.Mutator;
import databinding.SubComponent;
import databinding.render.ControlPoint;
import databinding.render.ControlPointContainer;
import databinding.render.RenderNode;
import databinding.wires.Span;
import editor.ComponentPanel;
import editor.Editor;
import editor.EditorDocumentContext;
import editor.EditorDocumentContext.UndoableEditAction;
import editor.RenderAdapter;
import editor.RenderAdapter.NodeRepresentantMapping;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class Tool  {
	
	private static final boolean DEBUG = false;

	private static final Color selectionColor =    new Color(0x70,0x70, 0x70, 0xFF);
	private static final Stroke selectionStroke =  new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0, new float [] {3,3}, 0);


	/** Auxiliary function for repainting easily */
	protected void repaintInComponentSpace(Rectangle2D bound) {
		cp.repaintInComponentSpace(bound);
	}

	public boolean canHandle(databinding.Component component) { return true; }

	/** called when the user presses escape **/

	public void abort() {
		s.abort();
		// TODO: smarter repaint here
		cp.repaint();
	}

	/** called when we need to forget hover related state.  **/

	public void forget() {
		s.forget();
		// TODO: smarter repaint here
		cp.repaint();
	}



	ArrayList<ControlPoint> cps = new ArrayList<ControlPoint>();



	/** ask us to render the components */
	public void render(Graphics2D g) {
		s.render(g);
	}

	public void render2(Graphics2D g2) {
		edc.getRenderRoot().render(g2, edc.getCompiledComponent());

		if (!edc.getSelection().getSelectedNodes().isEmpty()) {
			g2.setColor(selectionColor);
			g2.setStroke(selectionStroke);
			for (BasicEditorNode n : edc.getSelection().getSelectedNodes()) {
				edc.getRenderNode(n).renderSelectionMark(g2);
			}
		}

		// draw control points
		for (ControlPoint cp : cps) {
			cp.render(g2);
		}
	}

	EditorDocumentContext edc;
	ComponentPanel cp;
	Editor editor;

	public void disconnect() {
		if (DEBUG) System.out.println("Disabling tool " + getClass().getName() + ", leaving state " + s.getClass().getName());
		s.forget();
		s.onExit();
		s = null;
		edc = null;
		editor = null;
		cp = null;
	}

	public void connect(EditorDocumentContext edc, ComponentPanel cp, Editor e) {
		assert s == null;
		assert this.edc == null;
		assert this.cp == null;

		this.edc = edc;
		this.cp = cp;
		this.editor = e;
		s = defaultState;
		if (DEBUG) System.out.println("Enabled tool " + getClass().getName() + ", entering state " + s.getClass().getName());
		refreshControlPoints();
		s.onEntry();
	}


	protected static final int MAX_SELECT_DIST = 5;



	public ControlPoint getHoverControlPoint(Point2D p) {
		for (ControlPoint cp : cps) {
			if (cp.distance(p) <= MAX_SELECT_DIST) {
				return cp;
			}
		}
		return null;
	}

	public void mouseMoved(ComponentPanel.CPPoint p, MouseEvent e) {
		s.move(p,e);
	}




	public void mouseClicked(ComponentPanel.CPPoint p, MouseEvent e) {

		// we handle all click-release information in mouseReleased
		// so we do not need this function
		return;
	}


	public void mousePressed(ComponentPanel.CPPoint p, MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			s.press(p, e);
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			s.pressRight(p,e);
		}
	}


	public void mouseDragged(ComponentPanel.CPPoint p, MouseEvent e) {
		if ( (e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) == InputEvent.BUTTON1_DOWN_MASK) {
			s.drag(p, e);
		}
	}


	public static class MoveAction extends UndoableEditAction {
		EditorDocumentContext edc;
		int dx = 0;
		int dy = 0;
		Set<IMoveable> movers = new HashSet<IMoveable>();

		public MoveAction(int dx, int dy, Set<IMoveable> movers, EditorDocumentContext edc) {
			this.edc = edc;
			this.movers = movers;
			updateTo(dx, dy);
		}

		public boolean requiresRecompile() { return false; }
		

		public void updateTo(int dx2, int dy2) {
			for (IMoveable n : movers) {
				n.move(dx2-dx, dy2-dy);
			}
			dx = dx2;
			dy = dy2;
			
			edc.getPropertyTableModel().parsimoniousRefresh();
		}

		public void perform() {
			// do nothing, we're already performed during the drag
		}

		public void redo() {
			super.redo();
			for (IMoveable n : movers) {
				n.move(dx, dy);
			}
			edc.getRenderRoot().deepInvalidate(); // TODO: smarter
		}

		public void undo() {
			super.undo();
			for (IMoveable n : movers) {
				n.move(-dx, -dy);
			}
			edc.getRenderRoot().deepInvalidate(); // TODO: smarter
		}
	};

	// TODO: unify moving selection and control points

	class CP_MoveAction extends UndoableEditAction {
		EditorDocumentContext edc;

		Point2D initial = new Point2D.Double();
		int dx;
		int dy;
		ControlPoint cp;

		public CP_MoveAction(int dx, int dy, ControlPoint cp, EditorDocumentContext edc) {
			this.edc = edc;
			this.dx = dx;
			this.dy = dy;
			initial.setLocation(cp.getPoint());
			this.cp = cp;
			repaintInComponentSpace(cp.getBound());
		}

		public boolean requiresRecompile() { return false; }

		public void updateTo(int dx2, int dy2) {
			repaintInComponentSpace(cp.getBound());
			
			dx = dx2;
			dy = dy2;
			cp.setPoint(new Point2D.Double(initial.getX() + dx, initial.getY() + dy));
			
			repaintInComponentSpace(cp.getBound());
			edc.getPropertyTableModel().parsimoniousRefresh();
		}

		public void perform() {
			// do nothing, we're already performed during the drag
		}

		public void redo() {
			super.redo();
			cp.setPoint(new Point2D.Double(initial.getX() + dx, initial.getY() + dy));
		}

		public void undo() {
			super.undo();
			cp.setPoint(initial);
		}
	};

	public void mouseReleased(ComponentPanel.CPPoint p, MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			s.release(p,e);
		}
	}


	private void refreshControlPoints() {
		cps.clear();
		if (edc.getSelection().getSelectedNodes().size() == 1) {
			BasicEditorNode sel = edc.getSelection().getSelectedNodes().iterator().next(); 
			edc.getRenderNode(sel).collectControlPoints(cps);
		}
	}

	public void notifyStructureChange() {
		refreshControlPoints();
	}

	static class FirstLevelNodeSelector implements NodeRepresentantMapping<BasicEditorNode<?>> {
		public DeeperLevelNodeSelector getSubComponentMapping(SubComponent s) {
			return new DeeperLevelNodeSelector(s);
		}

		public BasicEditorNode<?> getRepresentant(BasicEditorNode<?> node) {
			return node;
		}
	}

	static final FirstLevelNodeSelector flns = new FirstLevelNodeSelector();

	static class DeeperLevelNodeSelector implements NodeRepresentantMapping<BasicEditorNode<?>> {
		final SubComponent s;
		DeeperLevelNodeSelector(SubComponent s) {
			this.s = s;
		}
		public DeeperLevelNodeSelector getSubComponentMapping(SubComponent s) {
			return this;
		}

		public SubComponent getRepresentant(BasicEditorNode<?> node) {
			return s;
		}
	}



	public String getToolTipText(Point2D p, MouseEvent e) {
		return this.s.getToolTipText();
	}

	protected abstract State getDefaultState();

	protected final State defaultState = getDefaultState();
	private State s = null;

	void transition(State s) {
		if (DEBUG) System.out.println("Transition from " + this.s.getClass().getName() + " to " + s.getClass().getName());
		if (this.s == s) return;
		this.s.onExit();
		this.s = s;
		this.s.onEntry();
	}

	void transition(State s, ComponentPanel.CPPoint p, MouseEvent e, boolean drag) {
		transition(s);
		if (drag) s.drag(p,e);
		else      s.move(p,e);
	}

	void transition(State s, ComponentPanel.CPPoint p, MouseEvent e) {
		transition(s,p,e,false);
	}

	static interface State {
		public void move(ComponentPanel.CPPoint p,       MouseEvent e);
		public void press(ComponentPanel.CPPoint p,      MouseEvent e);
		public void pressRight(ComponentPanel.CPPoint p, MouseEvent e);
		public void drag(ComponentPanel.CPPoint p,       MouseEvent e);
		public void release(ComponentPanel.CPPoint p,    MouseEvent e);
		public void abort();
		public void forget();

		public String getToolTipText();

		public void render(Graphics2D g);
		public void onEntry();
		public void onExit();
	}


	public static String getNodeDescription(BasicEditorNode node) {
		if (node instanceof Span) return ((Span)node).getParent().getParent().toString();
		return node.toString();
	}


	abstract class CursorState implements State {

		public final void move(Point2D p,       MouseEvent e) {}
		public final void press(Point2D p,      MouseEvent e) {}
		public final void pressRight(Point2D p, MouseEvent e) {}
		public final void drag(Point2D p,       MouseEvent e) {}
		public final void release(Point2D p,    MouseEvent e) {}

		public abstract Cursor getCursor();

		public void onEntry() {
			cp.setCursor(getCursor());
		}

		public void onExit() {
			assert cp.getCursor().equals(getCursor()) : "Cursor problem for " + this.getClass().getName();
		}

		public void render(Graphics2D g) {
			Tool.this.render2(g);
		}

		public void abort() {
			forget(); // this is the minimum that we need to do on escape
		}

		public void pressRight(ComponentPanel.CPPoint p, MouseEvent e) {}
	}

	
	abstract class DefaultState extends CursorState {
		public Cursor getCursor() { return Cursor.getDefaultCursor(); }


		public void hoverNode(BasicEditorNode node) {
			transition(new HoverNode(node));
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			// option one: hover a control point
			ControlPoint hcp = getHoverControlPoint(p.real);
			if (hcp != null) {
				transition(new HoverControlPoint(hcp));
				return;
			}
			// option two: select an object
			RenderAdapter.NodeDist<BasicEditorNode<?>> nd = edc.getRenderRoot().getNode(p.real, cp.getComponentGraphics(), flns);
			if (nd.d <= MAX_SELECT_DIST) {
				hoverNode(nd.node);
				return;
			}
		}


		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			System.out.println("Spurious drag ignored in state " + s + "of tool " + this);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			System.out.println("Spurious release ignored in state " + s + "of tool " + this);
		}

		public void abort() {
			edc.getSelection().clear(cp);
		}

		public void forget() { // remain here
		}

		public void pressRight(final ComponentPanel.CPPoint p, MouseEvent e) {
			if (edc.getSelection().getSelectedNodes().size() == 1) {
				final BasicEditorNode node = edc.getSelection().getSelectedNodes().iterator().next();
				final RenderNode rn = edc.getRenderNode(node).getRoot();
				if (rn != null && rn instanceof ControlPointContainer) {
					final ControlPointContainer cpc = (ControlPointContainer)rn;
					edc.doEdit(new ControlPointAction() {
						Mutator mut;
						EditorDocumentContext edc = Tool.this.edc;

						public void perform() {
							assert mut == null;
							mut = cpc.addControlPoint(p.grid);
							rn.invalidate();
							refreshControlPoints();
							cp.repaint();
						}

						public void undo() {
							super.undo();
							mut.undo();
							rn.invalidate();
							if (edc.getSelection().isSigleton(node)) {
								refreshControlPoints();
								cp.repaint();
							}
						}

						public void redo() {
							super.redo();
							mut.redo();
							rn.invalidate();
							if (edc.getSelection().isSigleton(node)) {
								refreshControlPoints();
								cp.repaint();
							}
						}
					});
				}
			}
		}
	}
	static abstract class ControlPointAction extends UndoableEditAction {
		public boolean requiresRecompile() { return false; }
	}



	
	abstract class HoverState extends CursorState {
		public final void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public final void release(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public void abort() {
			edc.getSelection().clear(cp);
			super.abort();
		}

		public void forget() {
			transition(defaultState);
		}

	}

	class HoverControlPoint extends HoverState {
		ControlPoint lockedcp;

		public HoverControlPoint(ControlPoint lockedcp) {
			this.lockedcp = lockedcp;
		}

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {
			if (lockedcp.distance(p.real) > MAX_SELECT_DIST) {
				transition(defaultState, p, e);
			}
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			// gracefully deal with presses that moved too much
			if (lockedcp.distance(p.real) <= MAX_SELECT_DIST)
				transition(new LockControlPoint(lockedcp));
			else
				transition(defaultState);
		}

		public void pressRight(final ComponentPanel.CPPoint p, MouseEvent e) {
			assert edc.getSelection().getSelectedNodes().size() == 1;
			final BasicEditorNode node = edc.getSelection().getSelectedNodes().iterator().next();
			RenderNode rn = edc.getRenderNode(node).getRoot();
			if (rn != null && rn instanceof ControlPointContainer) {
				transition(defaultState); // transition here, onExit uses 'lockedcp'

				final ControlPointContainer cpc = (ControlPointContainer)rn;
//				final Point2D pc = new Point2D.Double(p.getX(), p.getY());
				edc.doEdit(new ControlPointAction() {
					Mutator mut;
					EditorDocumentContext edc = Tool.this.edc;

					public void perform() {
						assert mut == null;
						assert edc.getSelection().getSelectedNodes().contains(node);
						mut = cpc.removeControlPoint(lockedcp);
						refreshControlPoints();
						cp.repaint();
					}

					public void undo() {
						super.undo();
						mut.undo();
						if (edc.getSelection().isSigleton(node)) {
							refreshControlPoints();
							cp.repaint();
						}
					}

					public void redo() {
						super.redo();
						mut.redo();
						if (edc.getSelection().isSigleton(node)) {
							refreshControlPoints();
							cp.repaint();
						}
					}
				});
				transition(defaultState,p,e); // forward move after delete
			}
		}


		public void render(Graphics2D g) {
			super.render(g);
			lockedcp.renderHover(g);
		}

		public Cursor getCursor() { return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR); }

		public void onEntry() {
			super.onEntry();
			repaintInComponentSpace(lockedcp.getBound());
		}

		public void onExit() {
			if (cp.isDisplayable()) repaintInComponentSpace(lockedcp.getBound());
			super.onExit();
		}

		public String getToolTipText() { return lockedcp.getDescription();}
	}


	class HoverNode extends HoverState {
		BasicEditorNode node;
		public HoverNode(BasicEditorNode node) {
			this.node = node;
		}

		public String getToolTipText() { return getNodeDescription(node); }

		public void move(ComponentPanel.CPPoint p, MouseEvent e) {

			// control points take precedence over nodes
			ControlPoint hdc = getHoverControlPoint(p.real);
			if (hdc != null) {
				transition(new HoverControlPoint(hdc));
				return;
			}

			//we can also hover onto another, less deep node
			RenderAdapter.NodeDist nd = edc.getRenderRoot().getNode(p.real, cp.getComponentGraphics(), flns);
			if (nd.d <= MAX_SELECT_DIST && node == nd.node) {
				// no noteworthy change, stay where we are
			} else {
				// we left the node, or moved onto another less deep one
				// defaultState knows best what to do in this case
				transition(defaultState, p, e);
			}
		}

		public void press(ComponentPanel.CPPoint p, MouseEvent e) {
			if (e.getClickCount() == 2 && node instanceof SubComponent) {
				SubComponent sc = (SubComponent)node;
				transition(defaultState);
				editor.open(sc);
			} else {
				// this may be the initial part of a drag, in which we move the whole selection
				transition(new LockNodes(node, p));
			}
		}

		public void pressRight(ComponentPanel.CPPoint p, MouseEvent e) {
			// forward to default state
			transition(defaultState);
			defaultState.pressRight(p, e);
			transition(defaultState,p,e);
		}

		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);}

	}

	abstract class LockState extends CursorState {
		public final void move(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public final void press(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public void forget() {
			transition(defaultState);
		}
	}

	class LockControlPoint extends LockState {
		ControlPoint lockedcp;
		public LockControlPoint(ControlPoint lockedcp) {
			this.lockedcp = lockedcp;
		}

		public String getToolTipText() { return lockedcp.getDescription();}

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(new MoveControlPoint(p, lockedcp), p, e, true);
		}

		public void render(Graphics2D g) {
			super.render(g);
			lockedcp.renderSelected(g);
		}

		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			transition(defaultState);
		}

		public void onEntry() {
			super.onEntry();
			repaintInComponentSpace(lockedcp.getBound());
		}

		public void onExit() {
			if (cp.isDisplayable()) repaintInComponentSpace(lockedcp.getBound());
			super.onExit();
		}

	}


	class LockNodes extends LockState {
		final BasicEditorNode node;
		final ComponentPanel.CPPoint clicked_point;
		public LockNodes(BasicEditorNode node, ComponentPanel.CPPoint clicked_point) {
			this.node = node;
			this.clicked_point = clicked_point;
		}

		public String getToolTipText() { return getNodeDescription(node); }

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			if (!edc.getSelection().isSelected(node)) {
				if (e.isShiftDown()) {
					edc.getSelection().toggle(node, cp);
				} else {
					edc.getSelection().set(node, cp);
				}
			} else {
				if (e.isShiftDown()) {
					edc.getSelection().toggle(node, cp);
				} else {
					// Interestingly, we do nothing here
				}
			}

			transition(new MoveNodes(clicked_point, edc.getSelection().getSelectedNodes()), p, e, true);
		}

		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			if (e.isShiftDown()) {
				edc.getSelection().toggle(node, cp);
			} else {
				edc.getSelection().set(node, cp);
			}

			transition(defaultState, p, e);
		}
	}



	abstract class MoveState extends CursorState {
		final ComponentPanel.CPPoint dragOrigin;
		public MoveState(ComponentPanel.CPPoint dragOrigin) {
			this.dragOrigin = dragOrigin;
		}

		public final void move(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public final void press(ComponentPanel.CPPoint p, MouseEvent e) {
			throw new Error("not available");
		}

		public void forget() {
			transition(defaultState);
		}
	}


	class MoveNodes extends MoveState {
		Set<BasicEditorNode> selection;
		Set<BasicEditorNode> affected = new HashSet<BasicEditorNode>();
		MoveAction m;		
		
		public MoveNodes(ComponentPanel.CPPoint dragOrigin, Set<BasicEditorNode> nodes) {
			super(dragOrigin);
			selection =  new HashSet<BasicEditorNode>(nodes);
			HashSet<IMoveable> movers = new HashSet<IMoveable>();
			for (BasicEditorNode n : selection) {
				n.collectInvolvedInMove(selection, movers);
			}
			
			for (IMoveable m : movers) m.collectAffected(affected);
			
			m  = new MoveAction(0, 0, movers, edc);
		}

		public String getToolTipText() { return "Move selection";}

		public void forget() {
			m.undo();
			super.forget();
		}
		
		private void invalidate() {
			for (BasicEditorNode n : affected) { 
				RenderAdapter r = edc.getRenderNode(n);
				r.deepInvalidate();				
			}
		}
		
		
		private void scheduleRepaint(Graphics2D cg) {
			for (BasicEditorNode n : selection) {
				RenderAdapter r = edc.getRenderNode(n); 
				repaintInComponentSpace(r.getSelectionMarkBound(cg));
			}
			for (BasicEditorNode n : affected) { 
				RenderAdapter r = edc.getRenderNode(n);
				repaintInComponentSpace(r.getDeepBound(cg));
			}
			
			for (ControlPoint cp : cps) 
				repaintInComponentSpace(cp.getBound());		
		}


		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			
			Graphics2D cg = cp.getComponentGraphics();
			cg.setStroke(selectionStroke); // used by selection bound
			
			// register an update within the old bound
			scheduleRepaint(cg);
			
			int totaldx = (int)(p.grid.getX() - dragOrigin.grid.getX());
			int totaldy = (int)(p.grid.getY() - dragOrigin.grid.getY());			

			m.updateTo(totaldx, totaldy);
			
			invalidate(); // cached render state is now out of date

			// register an update within the new bound
			scheduleRepaint(cg);
		}

		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			edc.doEdit(m);
			transition(defaultState, p, e);
		}


		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);}
	}

	class MoveControlPoint extends MoveState {
		CP_MoveAction m;
		
		public MoveControlPoint(ComponentPanel.CPPoint dragOrigin, ControlPoint lockedcp) {
			super(dragOrigin);
			m  = new CP_MoveAction(0,0, lockedcp, edc);
		}

		public String getToolTipText() { return "Move control point"; }


		public void forget() {
			m.undo();
			super.forget();
		}
		
		private void invalidate() {
			BasicEditorNode n = m.cp.getAffected(); 
			RenderAdapter r = edc.getRenderNode(n);
			r.deepInvalidate();				
		}
		
		private void scheduleRepaint(Graphics2D cg) {
			BasicEditorNode n = m.cp.getAffected();
			RenderAdapter r = edc.getRenderNode(n);
			repaintInComponentSpace(r.getDeepBound(cg));
			
			for (ControlPoint cp : cps) 
				repaintInComponentSpace(cp.getBound());		
		}
		

		public void drag(ComponentPanel.CPPoint p, MouseEvent e) {
			Graphics2D cg = cp.getComponentGraphics();
			scheduleRepaint(cg);
			
			int totaldx = (int)(p.grid.getX() - dragOrigin.grid.getX());
			int totaldy = (int)(p.grid.getY() - dragOrigin.grid.getY());

			m.updateTo(totaldx, totaldy);
			
			invalidate(); // cached render state is out of date
			scheduleRepaint(cg);
		}

		
		public void release(ComponentPanel.CPPoint p, MouseEvent e) {
			Graphics2D cg = cp.getComponentGraphics();
			scheduleRepaint(cg);
			
			edc.doEdit(m);
			transition(defaultState, p, e);
		}

		public void render(Graphics2D g) {
			super.render(g);
			m.cp.renderSelected(g);
		}

		public Cursor getCursor()  {return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);}

		public void onEntry() {
			super.onEntry();
			repaintInComponentSpace(m.cp.getBound());

		}

		public void onExit() {
			if (cp.isDisplayable()) repaintInComponentSpace(m.cp.getBound());
			super.onExit();
		}

	}

}
