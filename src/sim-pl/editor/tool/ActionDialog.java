/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor.tool;

import icons.IconManager;

import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.Action;
import databinding.Event;
import databinding.Internals;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ActionDialog extends NewElementDialog implements ChangeListener {
	public ActionDialog(Frame owner) {
		super(owner, "New Action");
		jbInit();
	}


	public Event getEvent() {
		return (Event)JComboBox1.getSelectedItem();
	}


	String text;
	public String getCode() { return text; }

	ValidityManager rdl2 = new ValidityManager() {
		public boolean isValid() {
			return text != null;
		}

		protected void revalidate() {
			text = jTextField2.getText();
			if (text.length() == 0) text = null;
		}
	};

	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl2.isValid());
	}


	private void jbInit()  {
		jPanel2.setLayout(gridBagLayout1);
		setIcon(IconManager.chargeIcon(Internals.class, BeanInfo.ICON_COLOR_32x32));

		JComboBox1.setSelectedIndex(0);

		jTextField2.setColumns(60);
		jTextField2.setRows(20);
		jTextField2.setFont(new Font("Monospaced", Font.PLAIN, 12).deriveFont(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform()));

		jLabel1.setLabelFor(jTextField2);
		jLabel1.setText("Code: ");
		jLabel2.setLabelFor(JComboBox1);
		jLabel2.setText("Event: ");
		this.getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);
		jPanel2.add(jLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jLabel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(JComboBox1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(new JScrollPane(jTextField2), new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		wire(jTextField2, rdl2, this);
	}

	public void setVisible(boolean b) {
		if (b) jTextField2.requestFocus();
		super.setVisible(b);
	}

	public boolean ask() {
		throw new Error("Call ask(Internals) please");
	}

	public boolean ask(Internals intnls) throws Exception {
		JComboBox1.removeAllItems();
		for (Event e: Event.events) {
			JComboBox1.addItem(e);
		}
		for (Action a : intnls.getChildren()) {
			JComboBox1.removeItem(a.getEvent());
		}

		if (JComboBox1.getItemCount() == 0) throw new Exception("All available events already used");
		JComboBox1.setSelectedIndex(JComboBox1.getItemCount()-1); // INPUT_CHANGE is default
		return super.ask();
	}


	JComboBox JComboBox1 = new JComboBox(Event.events);
	JPanel jPanel2 = new JPanel();
	JTextArea jTextField2 = new JTextArea();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();

}
