/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.Collections;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import namespace.ComponentNamespace;
import namespace.ParameterContext;
import databinding.BasicEditorNode;
import databinding.CompilationMessageBuffer;
import databinding.Component;
import databinding.InternalNode;
import databinding.compiled.Compiled.CompiledComponent;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */



/** A DocumentContext contains all information needed to use a component as a subcomponent in another complex component.
 * This includes
 *  - render information
 *  - compilation information
 */


public class DocumentContext extends RenderContext {

	/** The namespace for the root node (the Component) */
	private final ComponentNamespace cns;

//	/** the root of the compiled version of 'node' */
//	private final component.Component cnode;

	/** The result of the last compilation */
	private final CompilationMessageBuffer cmb = new CompilationMessageBuffer();

//	/** The component type-struct (currently only used for simple components) */
//	private final Type_Struct type = new Type_Struct();


	public DocumentContext(Component node, boolean renderLabels, boolean renderProbeStubs) {
		this(node, true, renderLabels, renderProbeStubs);
	}

	protected DocumentContext(Component node, boolean buildtree, boolean renderLabels, boolean renderProbeStubs) {
		super(node, false, renderLabels, renderProbeStubs);
		this.cns = new ComponentNamespace(node);

		// USER should call recompile after construction (subclasses can override compile)

		if (buildtree) {
			recompile();
			subTreeAttached(null, node, 0);
		}

/*
		Set<String> s = new HashSet<String>();
//		System.out.println("Status report about " + node);
		EditorNode.deepCollectUsedParameters(node, s);
//		System.out.println("Used parameters: " + s);
		s.clear();
		EditorNode.collectDefines(node, s);
//		System.out.println("Defines: " + s);
	   */
	}

	public ComponentNamespace getComponentNamespace() {
		return cns;
	}


	public CompilationMessageBuffer getCMB() {
		return cmb;
	}


	/** edm_compile events happen when recompilations occur */
	private EventDispatchManager<ChangeEvent, ChangeListener> edm_compile = new EventDispatchManager<ChangeEvent,ChangeListener>() {
		protected void dispatchEvent(ChangeListener listener, ChangeEvent event) {
			listener.stateChanged(event);
		}
	};

	EventDispatcher<ChangeListener> getEDM_Compile() { return edm_compile; }


	/** Should only be called by ComponentTable or by the containing complex owner */
	protected CompiledComponent<?> recompile() {
		cmb.clear();

		ParameterContext<String, Integer> pc = new ParameterContext<String,Integer>();
		CompiledComponent tt = cns.compile(pc, Collections.<String>emptySet(), cmb);

		edm_compile.fireEvent(new ChangeEvent(this));
		return tt;
	}









/****************** SUBTREE LISTENER ************************/

	protected void nodeAttached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		super.nodeAttached(parent, child, index);
		// TODO: something with compilation?
	}

	protected void nodeDetached(InternalNode<?,?> parent, BasicEditorNode<?> child, int index) {
		super.nodeDetached(parent, child, index);
		// TODO: something with compilation?
	}


}
