/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import editor.tool.NewElementDialog;
import editor.tool.NewElementDialog.ValidityManager;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class StrokeDialog extends JPanel implements ChangeListener{
	public StrokeDialog() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	StrokeDisplay sd = new StrokeDisplay();


	public static class StrokeDisplay extends JComponent {
		GeneralPath path = new GeneralPath();

		public StrokeDisplay() {
			path.moveTo(6,1);
			path.lineTo(10,10);
			path.lineTo(1,10);
			path.lineTo(6,6);
			setPreferredSize(new Dimension(100,100));
		}

		BasicStroke stroke;

		public void setStroke(BasicStroke stroke) {
			this.stroke = stroke;
			repaint();
		}

		public void paintComponent(Graphics g) {
			if (getWidth() == 0 || getHeight() == 0) return;

			Graphics2D g2 = (Graphics2D)g;

			g2.setStroke(stroke);
			g2.setColor(Color.black);
			double aspect = Math.min(getWidth()/12.0, getHeight()/12.0);

			g2.scale(aspect, aspect );
			g2.draw(path);
		}
	}


	static class Pattern {
		public final float [] pat;

		public Pattern(float ... pat) {
			this.pat = pat;
		}
	}


	Pattern [] patterns = {
		new Pattern(null), // null has a special meaning for lists, we use this instead
		new Pattern(1,1),
		new Pattern(1,2),
		new Pattern(2,2),
		new Pattern(3,3),
		new Pattern(4,4),
		new Pattern(5,5),
	};

	float width = 1;

	ValidityManager rdl1 = new ValidityManager() {
		boolean isvalid = true;
		public boolean isValid() {
			return isvalid;
		}

		protected void revalidate() {
			try {
				float tryWidth = Float.valueOf(jTextField1.getText());
				if (tryWidth < 0) throw new Exception("Line width negative: " + tryWidth);
				width = tryWidth;
				isvalid = true;
			} catch (Exception ex) {
				isvalid = false;
			} finally {
				stateChanged(null);
			}

		}
	};




	int cap = BasicStroke.CAP_ROUND;
	int join = BasicStroke.JOIN_ROUND;

	float miterlimit = 1;
	ValidityManager rdl2 = new ValidityManager() {
		boolean isvalid = true;
		public boolean isValid() {
			return isvalid;
		}

		protected void revalidate() {
			try {
				float tryMiterLimit = Float.valueOf(jTextField2.getText());
				if (tryMiterLimit < 1) throw new Exception("Invalid miter limit: " + tryMiterLimit);
				miterlimit = tryMiterLimit;
				isvalid = true;
			} catch (Exception ex) {
				isvalid = false;
			} finally {
				stateChanged(null);
			}
		}
	};


	class StrokeRenderer implements ListCellRenderer {
		myStrokeRenderer.StrokeDisplay sd = new myStrokeRenderer.StrokeDisplay();

		public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
				Pattern val = (Pattern)value;
			sd.setStroke(new BasicStroke(width, cap, join, miterlimit, val.pat, 0));
			sd.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
			sd.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
			return sd;
		}
	}

	public BasicStroke getStroke() {
		assert jComboBox1.getSelectedIndex() != -1;
		return new BasicStroke(width, cap, join, miterlimit, patterns[jComboBox1.getSelectedIndex()].pat, 0);
	}

	private void jbInit() throws Exception {
		this.setLayout(gridBagLayout1);
		jLabel1.setText("Cap: ");
		jLabel2.setText("Join: ");
		jLabel3.setText("Width: ");
		jLabel4.setText("Pattern: ");
		jLabel5.setText("Miter limit: ");
		jLabel6.setText("Example: ");
		jTextField1.setText("1");
		jTextField2.setText("1");
		jRadioButton1.setText("Round");
		jRadioButton2.setText("Square");
		jRadioButton3.setText("Butt");
		jRadioButton4.setText("Round");
		jRadioButton5.setText("Miter");
		jRadioButton6.setText("Beveled");
		this.add(jPanel1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
												 , GridBagConstraints.CENTER,
												 GridBagConstraints.HORIZONTAL,
												 new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jRadioButton1);
		jPanel1.add(jRadioButton2);
		jPanel1.add(jRadioButton3);
		this.add(jPanel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
												 , GridBagConstraints.CENTER,
												 GridBagConstraints.HORIZONTAL,
												 new Insets(0, 0, 0, 0), 0, 0));
		jPanel2.add(jRadioButton4);
		jPanel2.add(jRadioButton5);
		jPanel2.add(jRadioButton6);
		this.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
												 , GridBagConstraints.WEST,
												 GridBagConstraints.NONE,
												 new Insets(0, 0, 0, 0), 0, 0));
		this.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
												 , GridBagConstraints.WEST,
												 GridBagConstraints.NONE,
												 new Insets(0, 0, 0, 0), 0, 0));
		this.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
												 , GridBagConstraints.WEST,
												 GridBagConstraints.NONE,
												 new Insets(0, 0, 0, 0), 0, 0));
		this.add(jLabel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
												 , GridBagConstraints.WEST,
												 GridBagConstraints.NONE,
												 new Insets(0, 0, 0, 0), 0, 0));
		this.add(jComboBox1, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
		buttonGroup1.add(jRadioButton1);
		buttonGroup1.add(jRadioButton2);
		buttonGroup1.add(jRadioButton3);
		buttonGroup2.add(jRadioButton4);
		buttonGroup2.add(jRadioButton5);
		buttonGroup2.add(jRadioButton6);

		this.add(jTextField1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));

		this.add(jLabel5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
													 , GridBagConstraints.EAST,
													 GridBagConstraints.NONE,
													 new Insets(0, 0, 0, 0), 0, 0));

		this.add(jTextField2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
													 , GridBagConstraints.CENTER,
													 GridBagConstraints.HORIZONTAL,
													 new Insets(0, 0, 0, 0), 0, 0));
		this.add(jLabel6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		this.add(sd, new GridBagConstraints(1, 5, 1, 1, 1.0, 1.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));


		jComboBox1.setRenderer(new StrokeRenderer());
		jComboBox1.setSelectedIndex(0);
		assert jComboBox1.getSelectedIndex() == 0;


		ActionListener al = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == jRadioButton1) cap = BasicStroke.CAP_ROUND;
					else if (e.getSource() == jRadioButton2) cap = BasicStroke.CAP_SQUARE;
					else if (e.getSource() == jRadioButton3) cap = BasicStroke.CAP_BUTT;

					else if (e.getSource() == jRadioButton4) {
						join = BasicStroke.JOIN_ROUND;
						jTextField2.setEnabled(false);
					}
					else if (e.getSource() == jRadioButton5) {
						join = BasicStroke.JOIN_MITER;
						jTextField2.setEnabled(true);
					}
					else if (e.getSource() == jRadioButton6) {
						join = BasicStroke.JOIN_BEVEL;
						jTextField2.setEnabled(false);
					}
					stateChanged(new ChangeEvent(this));
				}
		};

		jRadioButton1.addActionListener(al);
		jRadioButton2.addActionListener(al);
		jRadioButton3.addActionListener(al);
		jRadioButton4.addActionListener(al);
		jRadioButton5.addActionListener(al);
		jRadioButton6.addActionListener(al);
		jComboBox1.addActionListener(al);

		jRadioButton1.doClick();
		jRadioButton4.doClick();

		NewElementDialog.wire(jTextField1, rdl1, this);
		NewElementDialog.wire(jTextField2, rdl2, this);
	}

//	public void setCap(int cap)  {
//		this.cap = cap;
//		this.stateChanged(null);
//	}
//
//	public void setJoin(int join)  {
//		this.join = join;
//		this.stateChanged(null);
//	}


	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	JLabel jLabel3 = new JLabel();
	JLabel jLabel4 = new JLabel();
	JLabel jLabel5 = new JLabel();
	JLabel jLabel6 = new JLabel();
	JPanel jPanel1 = new JPanel();
	JPanel jPanel2 = new JPanel();

	JTextField jTextField1 = new JTextField();
	JTextField jTextField2 = new JTextField();
	JComboBox jComboBox1 = new JComboBox(patterns);
	JRadioButton jRadioButton1 = new JRadioButton();
	JRadioButton jRadioButton2 = new JRadioButton();
	JRadioButton jRadioButton3 = new JRadioButton();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JRadioButton jRadioButton4 = new JRadioButton();
	JRadioButton jRadioButton5 = new JRadioButton();
	JRadioButton jRadioButton6 = new JRadioButton();
	ButtonGroup buttonGroup2 = new ButtonGroup();


	public void stateChanged(ChangeEvent e) {
		sd.setStroke(getStroke());
		jComboBox1.revalidate();
		jComboBox1.repaint();
		w.fireEvent(new ChangeEvent(this));
	}

	private Widget w = new Widget();

	public StateChangeDialog.ControllerWidget getWidget() {
		return w;
	}

	public void setStroke(BasicStroke stroke) {
		jTextField1.setText(Float.toString(stroke.getLineWidth()));
		jTextField2.setText(Float.toString(stroke.getMiterLimit()));

		switch (stroke.getEndCap()) {
			case BasicStroke.CAP_ROUND:   jRadioButton1.doClick(); break;
			case BasicStroke.CAP_SQUARE:  jRadioButton2.doClick(); break;
			case BasicStroke.CAP_BUTT:    jRadioButton3.doClick(); break;
			default: throw new Error("Unknown end cap: " + stroke.getEndCap());

		}
		switch (stroke.getLineJoin()) {
			case BasicStroke.JOIN_ROUND:  jRadioButton4.doClick(); break;
			case BasicStroke.JOIN_MITER:  jRadioButton5.doClick(); break;
			case BasicStroke.JOIN_BEVEL:  jRadioButton6.doClick(); break;
			default: throw new Error("Unknown line join: " + stroke.getLineJoin());
		}

		select(stroke.getDashArray());
	}

	private void select(float [] dash) {
		for (int i = 0; i < jComboBox1.getItemCount(); ++i) {
			if (Arrays.equals(dash, ((Pattern)jComboBox1.getItemAt(i)).pat)) {
				jComboBox1.setSelectedItem(i);
				return;
			}
		}
		Pattern p = new Pattern(dash);
		jComboBox1.addItem(p);
		jComboBox1.setSelectedItem(p);
	}

	private class Widget extends EventDispatchManager<ChangeEvent, ChangeListener> implements StateChangeDialog.ControllerWidget {
		protected void dispatchEvent(ChangeListener listener, ChangeEvent event) {
			listener.stateChanged(event);
		}

		public boolean isValid() {
			return rdl1.isValid() && rdl2.isValid();
		}

		public void addChangeListener(ChangeListener l) {
			addListener(l);
		}

		public void removeChangeListener(ChangeListener l) {
			removeListener(l);
		}

		public JComponent getWidget() {
			return StrokeDialog.this;
		}
	}

}
