/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Pairing <S,T> {
	private Map<S,T> s2t = new HashMap<S,T>();
	private Map<T,S> t2s = new HashMap<T,S>();

	public T getUncheckedS(S s) { return s2t.get(s); }
	public S getUncheckedT(T t) { return t2s.get(t); }

	public T getS(S s) { assert s2t.containsKey(s) : "no association for " + s; return s2t.get(s); }
	public S getF(T t) { assert t2s.containsKey(t) : "no association for " + t; return t2s.get(t); }

	public boolean containsS(S s) { return s2t.containsKey(s); }
	public boolean containsT(T t) { return t2s.containsKey(t); }

	public void put(S s, T t) {
		assert !s2t.containsKey(s);
		assert !t2s.containsKey(t);
		s2t.put(s,t);
		t2s.put(t,s);
		assert getS(s) == t;
		assert getF(t) == s;
	}

	public void remove(S s, T t) {
		assert getS(s) == t;
		assert getF(t) == s;
		t2s.remove(t);
		s2t.remove(s);
		assert !s2t.containsKey(s);
		assert !t2s.containsKey(t);
	}

	public void removeF(S s) { remove(s, getS(s)); }
	public void removeS(T t) { remove(getF(t), t); }

	public void clear() {
		s2t.clear();
		t2s.clear();
	}

	public boolean isPair(S s, T t) {
		return  s.equals(t2s.get(t)) && t.equals(s2t.get(s));
	}
}
