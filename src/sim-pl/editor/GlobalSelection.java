/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import databinding.BasicEditorNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class GlobalSelection {

	// just a marker interface
	static interface SelectionCause {}


	Set<BasicEditorNode> nodes = new HashSet<BasicEditorNode>();

	private final EventDispatchManager<SelectionChangedEvent, SelectionEventListener> edm = new EventDispatchManager<SelectionChangedEvent,SelectionEventListener>() {
		protected void dispatchEvent(SelectionEventListener listener, SelectionChangedEvent event) {
			listener.selectionChanged(event);
		}
	};

	public EventDispatcher<SelectionEventListener> getEventDispatcher() { return edm; }


	public void set(BasicEditorNode node, SelectionCause cause) {
		set(Collections.singleton(node), cause);
	}

	public void set(Collection<BasicEditorNode> newnodes, SelectionCause cause) {
		if (!nodes.equals(newnodes)) {
			nodes.clear();
			nodes.addAll(newnodes);
			edm.fireEvent(new SelectionChangedEvent(this, cause));
		}
	}

	public void toggle(BasicEditorNode node, SelectionCause cause) {
		if (nodes.contains(node)) remove(node, cause);
		else                      add(node, cause);
	}



	public void add(BasicEditorNode node, SelectionCause cause) {
		if (nodes.add(node)) edm.fireEvent(new SelectionChangedEvent(this, cause));
	}

	public void add(Collection<BasicEditorNode> newnodes, SelectionCause cause) {
		if (nodes.addAll(newnodes)) edm.fireEvent(new SelectionChangedEvent(this, cause));
	}


	public void remove(BasicEditorNode node, SelectionCause cause) {
		if (nodes.remove(node)) edm.fireEvent(new SelectionChangedEvent(this, cause));
	}

	public void remove(Collection<BasicEditorNode> newnodes, SelectionCause cause) {
		if (nodes.removeAll(newnodes)) edm.fireEvent(new SelectionChangedEvent(this, cause));
	}


	public void clear(SelectionCause cause) {
		if (!nodes.isEmpty()) {
			nodes.clear();
			edm.fireEvent(new SelectionChangedEvent(this, cause));
		}
	}

	public boolean isSelected(BasicEditorNode node) {
		return nodes.contains(node);
	}

	public Set<BasicEditorNode> getSelectedNodes() {
		return Collections.unmodifiableSet(nodes);
	}

	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	public boolean isSigleton(BasicEditorNode node) {
		return nodes.size() == 1 && nodes.iterator().next() == node;
	}

}
