/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import javax.swing.table.AbstractTableModel;

import databinding.CompilationMessageBuffer;
import databinding.compiled.CompilationMessage;
import editor.GlobalSelection.SelectionCause;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ErrorTableModel extends AbstractTableModel implements SelectionCause {
	CompilationMessageBuffer cmb;

	public ErrorTableModel(CompilationMessageBuffer cmb) {
		this.cmb = cmb;
	}

	private static final String [] cn = {"Type", "Node","Message"};

	public String getColumnName(int i) {
		return cn[i];
	}

	/**
	 * Returns the number of columns in the model.
	 *
	 * @return the number of columns in the model
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public int getColumnCount() {
		return cn.length;
	}

	public int getRowCount() {
		return cmb.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		CompilationMessage cm = cmb.get(rowIndex);
		switch (columnIndex) {
			case 0: return cm.error ? "Error" : "Warning";
			case 1: return cm.genBy;
			case 2: return cm.message;
			default: throw new Error("Invalid case");
		}
	}
}
