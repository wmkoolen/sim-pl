/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import databinding.SolidFill;



/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SolidFillRenderer extends JComponent implements TableCellRenderer {
	public SolidFillRenderer() {
		setOpaque(true);
	}

	SolidFill fill;

	public static Class getForClass() { return SolidFill.class; }

	public static final int block = 4;

	public static void blockPattern(Graphics g, Color bc) {
		g.setColor(bc);
		Rectangle r = g.getClipBounds();
		r.width += r.x;
		r.height += r.y;

		for (int x = block*(r.x/block); x < r.width; x += 2*block) {
			for (int y = block*(r.y/block); y < r.height; y += block) {
				g.fillRect(x+y%(2*block), y, block, block);
			}
		}
	}

	public void paintComponent(Graphics legacyg) {
		Graphics2D g = (Graphics2D)legacyg.create();
		// het getNormalizingTransform dilemma (zie executer.ComponentPanel)		
		AffineTransform nt = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();;
		
		g.transform(nt);

		// draw black blocked pattern
		if (fill == null || fill.getColor().getAlpha() != 255) {
			blockPattern(g, Color.black);
		}
		if (fill != null) {
			Rectangle r = g.getClipBounds();
			g.setColor(fill.getColor());
			g.fillRect(r.x, r.y, r.width, r.height);
		}
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		fill = (SolidFill)value;
		setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
		setForeground(isSelected ? table.getSelectionForeground() : table.getForeground());
		return this;
	}
}
