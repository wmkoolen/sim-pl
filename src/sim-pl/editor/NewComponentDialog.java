/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import icons.IconManager;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.BeanInfo;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import databinding.Complex;
import databinding.Simple;
import editor.tool.NewElementDialog;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class NewComponentDialog extends NewElementDialog implements ChangeListener {
	public NewComponentDialog(Frame owner) {
		super(owner, "New Component");
		jbInit();
	}


	String name;
	public String getNewName() {
		return name;
	}

	ValidityManager rdl1 = new ValidityManager() {
		public boolean isValid() {
			return name != null;
		}

		protected void revalidate() {
			name = jTextField1.getText().length() > 0 ? jTextField1.getText() : null;
		}
	};



//	File file;
//	public File getFile() {
//		return file;
//	}

//	public void setFile(File f) {
//		jTextField2.setText(f.getAbsolutePath());
//	}

//	ValidityManager rdl2 = new ValidityManager() {
//		public boolean isValid() {
//			return file != null;
//		}
//
//		protected void revalidate() {
//			file = new File(jTextField2.getText());
//			if (file.exists())  file = null;
//		}
//	};

	public boolean isSimple() {
		return jRadioButton1.isSelected();
	}



	public void stateChanged(ChangeEvent e) {
		setOKenabled(rdl1.isValid());
	}



	private void jbInit()  {
		setIcon(IconManager.chargeIcon(Simple.class, BeanInfo.ICON_COLOR_32x32));

		jPanel1.setLayout(gridBagLayout1);
		jLabel1.setText("Type: ");
		jLabel1.setLabelFor(jPanel2);
		jLabel4.setIcon(IconManager.chargeIcon(Simple.class,  BeanInfo.ICON_COLOR_32x32));
		jLabel5.setIcon(IconManager.chargeIcon(Complex.class, BeanInfo.ICON_COLOR_32x32));
		jRadioButton2.setSelected(true);
//		jLabel2.setText("File: ");
//		jLabel2.setLabelFor(jTextField2);
		jLabel3.setLabelFor(jTextField1);
//		jButton1.setText("...");
		jLabel3.setText("Name: ");
//		jTextField2.setColumns(20);
		jLabel4.setLabelFor(jRadioButton1);
		jLabel4.setText("Simple");
		jLabel5.setLabelFor(jRadioButton2);
		jLabel5.setText("Complex");
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		jPanel2.add(jRadioButton1);
		jPanel2.add(jLabel4);
		jPanel2.add(Box.createHorizontalStrut(20));
		jPanel2.add(jRadioButton2);
		jPanel2.add(jLabel5);
		buttonGroup1.add(jRadioButton1);
		buttonGroup1.add(jRadioButton2);
		jRadioButton2.setSelected(true);
		jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));

//		jPanel1.add(jButton1, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
//			, GridBagConstraints.EAST, GridBagConstraints.NONE,
//			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jPanel2, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jLabel2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
//			, GridBagConstraints.WEST, GridBagConstraints.NONE,
//			new Insets(0, 0, 0, 0), 0, 0));
//		jPanel1.add(jTextField2, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
//			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
//			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField1, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0), 0, 0));


//		jButton1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				FileManager fm = FileManager.getFileManager();
//				File cf = fm.askForFile(NewComponentDialog.this, "New Component",  getFile(), "Open", fm.filter_xml);
//				if (cf == null) return;
//					setFile(cf);
//			}
//		});

		wire(jTextField1, rdl1, this);
//		wire(jTextField2, rdl2, this);
	}

	JPanel jPanel1 = new JPanel();
	JLabel jLabel1 = new JLabel();
	JPanel jPanel2 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JRadioButton jRadioButton1 = new JRadioButton();
	JRadioButton jRadioButton2 = new JRadioButton();
//	JLabel jLabel2 = new JLabel();
	JTextField jTextField1 = new JTextField();
//	JButton jButton1 = new JButton();
	JLabel jLabel3 = new JLabel();
//	JTextField jTextField2 = new JTextField();
	JLabel jLabel4 = new JLabel();
	JLabel jLabel5 = new JLabel();

	public void setVisible(boolean b) {
		if (b) jTextField1.requestFocus();
		super.setVisible(b);
	}

}
