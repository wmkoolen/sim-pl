/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import editor.EditorDocumentContext.UndoableEditAction;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PropertyTableModel extends AbstractTableModel {
	EditorDocumentContext edc;
	Object cur;
	BeanInfo bi;

	public void setSelected(Object cur) {
		this.cur = cur;
		if (cur != null) {
			try {
				bi = Introspector.getBeanInfo(cur.getClass());
			} catch (IntrospectionException ex) {
				ex.printStackTrace();
				this.cur = null;
			}
		}
		this.fireTableDataChanged();
	}


	public void parsimoniousRefresh() {
		super.fireTableChanged(new TableModelEvent(this, 0, getRowCount()-1, 1));
	}


	public PropertyTableModel(EditorDocumentContext edc) {
		this.edc = edc;
	}

	private static final String [] cn = {"Property", "Value"};

	public String getColumnName(int i) {
		return cn[i];
	}

	/**
	 * Returns the number of columns in the model.
	 *
	 * @return the number of columns in the model
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public int getColumnCount() {
		return cn.length;
	}

	/**
	 * Returns the number of rows in the model.
	 *
	 * @return the number of rows in the model
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public int getRowCount() {
		if (cur == null) return 0;
		return bi.getPropertyDescriptors().length;
	}

	/**
	 * Returns the value for the cell at <code>columnIndex</code> and
	 * <code>rowIndex</code>.
	 *
	 * @param rowIndex the row whose value is to be queried
	 * @param columnIndex the column whose value is to be queried
	 * @return the value Object at the specified cell
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		PropertyDescriptor pd = bi.getPropertyDescriptors()[rowIndex];
		if (columnIndex == 0) return pd.getName();
		try {
			return pd.getReadMethod().invoke(cur);
		} catch (Exception ex) {
			throw new Error(ex);
		}
	}

	public boolean isCellEditable(int row, int col) {
		return col == 1;
	}



	public void setValueAt(final Object value, int row, int col) {
		assert col == 1;

		// make sure something changed, and handle nulls gracefully
		Object old = getValueAt(row, col);
		if (old == null
			? value == null
			: old.equals(value)) return;

//		System.out.println("setValue(" + value + ")  " +value.getClass());

		final PropertyDescriptor pd = bi.getPropertyDescriptors()[row];



		edc.doEdit(new UndoableEditAction() {
			final Object cur = PropertyTableModel.this.cur;
			Object oldValue;
			public void perform() {
				try {
					oldValue = pd.getReadMethod().invoke(cur);
//					System.out.println(pd.getWriteMethod());
					pd.getWriteMethod().invoke(cur, value);
				} catch (Exception ex) { throw new Error(ex); }
			}
			public void redo() {
				super.redo();
				try {
					pd.getWriteMethod().invoke(cur, value);
				} catch (Exception ex) {
					throw new Error(ex);
				}
			}
			public void undo() {
				super.undo();
				try {
					pd.getWriteMethod().invoke(cur, oldValue);
				} catch (Exception ex) { throw new Error(ex); }
			}

			public boolean requiresRecompile() {
				return true; // TODO: too safe
			}
		});
	}
}
