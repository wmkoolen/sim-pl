/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import util.DialogHelper;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class StateChangeDialog extends JDialog implements ChangeListener, ActionListener{

	StateChangeDialog.ControllerWidget peer;
	JPanel jToolBar1 = new JPanel();
	JButton jButton1 = new JButton();
	JButton jButton2 = new JButton();

	public static interface ControllerWidget {
		public boolean isValid();
		public void addChangeListener(ChangeListener l);
		public void removeChangeListener(ChangeListener l);
		public JComponent getWidget();
	}

	public StateChangeDialog(Frame owner, String title) {
		super(owner,title, true);
		jbInit();
	}

	public void stateChanged(ChangeEvent e) {
		jButton1.setEnabled(peer.isValid());
	}

	public boolean go(StateChangeDialog.ControllerWidget peer) {
		assert this.peer == null;

		success = false;
		this.peer = peer;
		peer.addChangeListener(this);
		getContentPane().add(peer.getWidget(), BorderLayout.CENTER);
		jButton1.setEnabled(peer.isValid());
		pack();
		setLocationRelativeTo(getOwner());
		setVisible(true);
		getContentPane().remove(peer.getWidget());
		peer.removeChangeListener(this);
		this.peer = null;
		dispose();
		return success;
	}

	private void jbInit()  {
		DialogHelper.performOnEscapeKey(this, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});

		setDefaultCloseOperation(HIDE_ON_CLOSE); // == setVisible(false)

		jButton1.setText("OK");
		jToolBar1.add(jButton1);
		jToolBar1.add(jButton2);
		jButton1.addActionListener(this);
		jButton2.addActionListener(this);
		this.getContentPane().add(jToolBar1, java.awt.BorderLayout.SOUTH);
		jButton2.setText("Cancel");
		getRootPane().setDefaultButton(jButton1);
	}


	boolean success;

	public void actionPerformed(ActionEvent e) {
		success = e.getSource() == jButton1;
		setVisible(false);
	}
}
