/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.geom.AffineTransform;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import editor.FTEditor.FontText;



public class FTRenderer extends JLabel implements TableCellRenderer
{
	public static Class getForClass()	{	return FontText.class; 	}

//	public FTRenderer()	{
//		setOpaque(true); //MUST do this for background to show up.
//	}

	private AffineTransform getNT() {
		// het getNormalizingTransform dilemma (zie executer.ComponentPanel)		
		AffineTransform nt = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getNormalizingTransform();
		assert nt.getTranslateX() == 0 && nt.getTranslateY() == 0 && nt.getShearX() == 0 && nt.getShearY() == 0;
		return nt;
	}


	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		FontText  ft = (FontText)value;
		setFont(ft.f.deriveFont(getNT()));
		setText(ft.text);
		this.setBackground( isSelected
							? table.getSelectionBackground()
							: table.getBackground());
		this.setForeground( isSelected
							? table.getSelectionForeground()
							: table.getForeground());
		return this;
	}

//	public void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		Rectangle r = g.getClipBounds();
//		g.setColor(getBackground());
//		g.fillRect(r.x,r.y, r.width, r.height);
//	}
}
