/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import databinding.BasicEditorNode;
import databinding.InternalNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class TreeNodeAdapter implements TreeNode {
	BasicEditorNode<? extends BasicEditorNode> node;
	Pairing<BasicEditorNode, TreeNodeAdapter> n2t;

	/** Creates a new treenode representative */

	public TreeNodeAdapter(BasicEditorNode<? extends BasicEditorNode> node, Pairing<BasicEditorNode, TreeNodeAdapter> n2t) {
		assert node != null;
		assert !n2t.containsS(node);
		this.node = node;
		this.n2t = n2t;
	}

	BasicEditorNode<? extends BasicEditorNode> getUserObject() {
		return node;
	}

	public String toString() {
		return node.toString();
	}

	public TreeNode getChildAt(int childIndex) {
		BasicEditorNode c = ((InternalNode<?,?>)node).getChildren().get(childIndex);
		return n2t.getS(c);
	}

	public int getChildCount() {
		return ((InternalNode<?,?>)node).getChildren().size();
	}

	public TreeNodeAdapter getParent() {
		return node.getParent() == null
			? null
			: n2t.getS(node.getParent());
	}

	public int getIndex(TreeNode child) {
		return ((InternalNode<?,?>)node).getChildren().indexOf(n2t.getF((TreeNodeAdapter)child));
	}

	public boolean getAllowsChildren() {
		return node instanceof InternalNode;
	}

	public boolean isLeaf() {
		return !(node instanceof InternalNode) || ((InternalNode<?,?>)node).getChildren().isEmpty();
	}

	public Enumeration children() {
		final List<? extends BasicEditorNode> l = ((InternalNode<?,?>)node).getChildren();

//		if (l == null) return new Enumeration() {
//				public boolean hasMoreElements() { return false; }
//				public Object nextElement() { throw new Error("Dead End"); }
//			};
//
		final Iterator<? extends BasicEditorNode> it = l.iterator();

		return new Enumeration() {
			public boolean hasMoreElements() { return it.hasNext(); }
			public Object nextElement() { return n2t.getS(it.next()); }
		};
	}

	public TreePath getPath() {
		if (getParent() == null) return new TreePath(this);
		return getParent().getPath().pathByAddingChild(this);

	}
}
