/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.IMoveable;
import databinding.InternalNode;
import databinding.PinLabel;
import databinding.SubComponent;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.render.ControlPoint;
import databinding.render.RenderNode;
import databinding.render.SubComponentRenderNode;
import databinding.wires.Probe;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


/* RenderAdapter
 * 
 */

public class RenderAdapter {
	private final BasicEditorNode<? extends BasicEditorNode> node;
	private final RenderNode<? extends BasicEditorNode> r;

	private final RenderContext rc;

	public RenderAdapter(RenderContext rc, BasicEditorNode<? extends BasicEditorNode> node) {
		this.rc = rc;
		this.node = node;
//		this.n2r = n2r;
		String cname = RenderNode.class.getPackage().getName() + "." + node.getClass().getSimpleName() + RenderNode.class.getSimpleName();
//		this.r = ((node.getClass() == PinLabel.class || node instanceof Pin) && !rc.renderLabels)
		this.r = (node.getClass() == PinLabel.class && !rc.renderLabels ||
			      node.getClass() == Probe.class  && !rc.renderProbeStubs)
			? null
			: createRenderNode(cname);
	}

	private RenderNode<? extends BasicEditorNode> createRenderNode(String cname) {
		try {
			Class rnc = Thread.currentThread().getContextClassLoader().loadClass(cname);

			assert RenderNode.class.isAssignableFrom(rnc);
			return (RenderNode<? extends BasicEditorNode<?>>)rnc.getConstructor(node.getClass()).newInstance(node);
		} catch (ClassNotFoundException ex) {
			return null;
		} catch (Exception ex)  {
			throw new Error(ex);
		}
	}

	public RenderNode getRoot() {
		return this.r;
	}

	private List<? extends BasicEditorNode<?>> getChildrenInRenderOrder() {
		if (r != null) return r.getChildrenInRenderOrder();
		if (node instanceof InternalNode) return ((InternalNode<?,?>)node).getChildren();
		return null;
	}


	private static class RectangleResult {
		Rectangle2D r = null;

		public void union(Rectangle2D s) {
			if (s == null) return;
			if (r == null) r = new Rectangle2D.Double(s.getX(), s.getY(), s.getWidth(), s.getHeight());
			else Rectangle2D.union(r, s, r);
		}
	}


	private void collectDeepBound(Graphics2D g, RectangleResult rr) {
		if (r != null) {
			rr.union(r.getBound(g));
		}

		List<? extends BasicEditorNode<?>> l = getChildrenInRenderOrder();
		if (l == null) return;

		for (BasicEditorNode n : l) {
			rc.getRenderNode(n).collectDeepBound(g,rr);
		}
	}

	public Rectangle2D getDeepBound(Graphics2D g) {
		RectangleResult rr = new RectangleResult();
		collectDeepBound(g, rr);
		return rr.r;
	}


	final boolean REDRECTS = false;
	final boolean DIST_PROFILE = false;


	public void render(Graphics2D g, CompiledComponent<?> cc) {

		Rectangle2D b = r == null ? null : r.getBound(g);
		Rectangle2D b2 = getDeepBound(g);

		if (REDRECTS) {
			if (b != null) {
				g.setColor(new Color(0.5f, 0, 0, 0.2f));
				g.draw(b);
			}
		}

		if (DIST_PROFILE) {
			if (b != null && r != null && !(r instanceof SubComponentRenderNode)) {
				g.setColor(Color.red);
				Rectangle br = b.getBounds();
				br.width  += br.x;
				br.height += br.y;
				Point p = new Point();
				NodeDist<BasicEditorNode<?>> nd = new NodeDist<BasicEditorNode<?>>();
				IdentityNodeRepresentantMapping irm = new IdentityNodeRepresentantMapping();

				for (p.x = br.x; p.x <= br.width; p.x+=3) {
					for (p.y = br.y; p.y <= br.height; p.y+=3) {
						if (r.getDistance(p, g) < 5) g.drawRect(p.x, p.y, 1, 1);
					}
				}
			}
		}


//		Disabled this warning, which is annoying on an empty component.	
//		if (r != null && b2 == null && !(node instanceof Split || node instanceof SubPinStub)) System.err.println("Strange, no rect for " + node.getClass());

		if (r != null && b2 != null) {
			assert !b2.isEmpty() : "empty bound for " + node.getClass();
			Rectangle clip = g.getClipBounds();
			// NOTE: clip can be null when rendering an exported image
			if (clip == null || b2.intersects(clip)) r.render(g, cc);
		}

		List<? extends BasicEditorNode<?>> l = getChildrenInRenderOrder();
		if (l == null) return;
		for (BasicEditorNode n : l) {
			rc.getRenderNode(n).render(g, cc);
		}
	}


	public void renderSelectionMark(Graphics2D g) {
		if (r != null) r.renderSelectionMark(g);
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		return r == null ? null : r.getSelectionMarkBound(g);
	}
	


	public void collectControlPoints(Collection<ControlPoint> cps) {
		if (r != null) r.collectControlPoints(cps);
	}


	public static class NodeDist <T> {
		public T node = null;
		public double d = Double.POSITIVE_INFINITY;

		public String toString() { return node + ": " + d; }
	}

	public <T> NodeDist<T> getNode(Point2D cp, Graphics2D g, NodeRepresentantMapping<T> n2m) {
		NodeDist<T> nd = new NodeDist<T>();
		getNode(cp, nd, g, n2m);
		return nd;
	}

	public static interface NodeRepresentantMapping <T> {
		public NodeRepresentantMapping<T> getSubComponentMapping(SubComponent s);
		public abstract T getRepresentant(BasicEditorNode<?> node);
	}

	public static class IdentityNodeRepresentantMapping implements NodeRepresentantMapping<BasicEditorNode<?>> {
		public IdentityNodeRepresentantMapping getSubComponentMapping(SubComponent s) {
			return this;
		}
		public BasicEditorNode<?> getRepresentant(BasicEditorNode<?> node) {
			return node;
		}
	}



	/** Retrieve the node with shortest distance. Among equidistant nodes, we
	 * prefer the frontmost one. (That is what the user sees when she clicks.)
	 * @param p Point2D        the cursor
	 * @param nd NodeDist      result object
	 * @param g Graphics2D     graphics (yes, we need this for text)
	 * @param treshold double
	 * @param n2m NodeRepresentantMapping
	 */

	public <X> void getNode(Point2D p, NodeDist<X> nd, Graphics2D g, NodeRepresentantMapping<X> n2m) {
		if (r != null) {
			r.getNode(p, g, nd, n2m);
		}

		List<? extends BasicEditorNode> l = getChildrenInRenderOrder();
		if (l == null || l.isEmpty()) return;

		// traverse objects from BACK to FRONT (in render order)
		for (BasicEditorNode n : l) {
			rc.getRenderNode(n).getNode(p, nd, g, n2m);
		}
	}

	/** Finds all enclosed nodes by their bounding rectangle
	 * @param g Graphics2D
	 * @param rect Rectangle2D
	 * @param result Set
	 */
	public void collectEnclosedNodes(Graphics2D g, Rectangle2D rect, Set<BasicEditorNode> result) {
		Rectangle2D deepBound = getDeepBound(g);
		if (deepBound == null) return;
		if (!deepBound.intersects(rect)) return;

		if (r != null) {
			Rectangle2D b = r.getBound(g);
			if (b != null && rect.contains(b)) result.add(node);
		}

		List<? extends BasicEditorNode> l = getChildrenInRenderOrder();
		if (l == null || l.isEmpty()) return;
		for (BasicEditorNode n : l) {
			rc.getRenderNode(n).collectEnclosedNodes(g, rect, result);
		}
	}

	public void move(int dx, int dy) {
		if (node instanceof IMoveable)  ((IMoveable)node).move(dx, dy);
		if (r != null) r.invalidate();
	}

	public void deepInvalidate() {
		if (r != null) r.invalidate();
		List<? extends BasicEditorNode<?>> l = getChildrenInRenderOrder();
		if (l == null) return;

		for (BasicEditorNode n : l) {
			rc.getRenderNode(n).deepInvalidate();
		}
	}
}
