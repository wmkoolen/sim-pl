/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package log;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;


public class Log_Dialog extends JDialog implements Log
{
    BorderLayout    borderLayout1 =     new BorderLayout();
	JScrollPane     jScrollPane1 =      new JScrollPane();
    JTextArea       jTextArea1 =        new JTextArea();
    JToolBar jToolBar1 = new JToolBar();
    JButton bClose = new JButton();
    JPanel jPanel1 = new JPanel();

    public Log_Dialog(String title)
    {
		this((Frame)null, title, null);
    }

    public Log_Dialog(Frame owner, String title, Rectangle bounds)
    {
		super(owner, title, true);
        jbInit();
		if (bounds != null)     setBounds(bounds);
		else                    { setSize(niceSize); setLocationRelativeTo(getOwner());}
//		if (owner == null)  this.setVisible(true);
    }

    public Log_Dialog(Dialog owner, String title, Rectangle bounds)
    {
		super(owner, title, true);
        jbInit();
		if (bounds != null)     setBounds(bounds);
		else                    { setSize(niceSize); setLocationRelativeTo(getOwner());}
//		if (owner == null) this.setVisible(true);
    }


    private void jbInit()
    {
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.getContentPane().setLayout(borderLayout1);
        jTextArea1.setEditable(false);
        jTextArea1.setColumns(80);
        jTextArea1.setTabSize(4);
		jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 12));
        jTextArea1.setAutoscrolls(true);
        bClose.setText("Close");

		ActionListener closer = new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Close(e);
			}
		};


        bClose.addActionListener(closer);
        jToolBar1.setFloatable(false);
        this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
        this.getContentPane().add(jPanel1, BorderLayout.SOUTH);
        jPanel1.add(jToolBar1, null);
        jToolBar1.add(bClose, null);
        jScrollPane1.getViewport().add(jTextArea1, null);
		getRootPane().setDefaultButton(bClose);

		util.DialogHelper.performOnEscapeKey(this, new AbstractAction() {
			public void actionPerformed(ActionEvent arg0) {
				Close(arg0);
			}
		});
    }



	public void append(String text)
	{
		jTextArea1.append(text);
		jTextArea1.setCaretPosition(jTextArea1.getText().length());
	}

    void Close(ActionEvent e)
    {
		this.setVisible(false);
    }

	public void clean()
	{
		jTextArea1.setText("");
		jTextArea1.setCaretPosition(0);
	}
}
