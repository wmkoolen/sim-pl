/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package log;

import util.MyFormatter;

public class Logger
{
	Log log;
	boolean wantResult = false;

    public Logger(Log log)
    {
		this.log = log;
    }

	public void addParagraph(String text)
	{
		assert !wantResult;
		log.append("\n" + text + "\n");
	}

	public void addLine(String text)
	{
		assert !wantResult;
		log.append(text + "\n");
	}

	public void addDoing(String text)
	{
		assert !wantResult;
		log.append(MyFormatter.pad(text, 60)); // no newline, followed by addResult!
		wantResult = true;
	}

	public void addResult(String text)
	{
		assert wantResult;
		log.append(" " + text + "\n");      // provides the newline for addDoing
		wantResult = false;
	}

	public void addSuccess()
	{
		addResult("OK");
	}

	public Exception addFailure(Exception ex)
	{
		addResult("Failed");
		addLine(ex.getMessage());
		return ex;
	}

	public Error addFailure(Error ex)
	{
		addResult("Failed");
		addLine(ex.getMessage());
		return ex;
	}

}
