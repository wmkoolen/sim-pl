/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package log;

import java.awt.BorderLayout;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class Log_Frame extends JFrame implements Log
{
    BorderLayout borderLayout1 = new BorderLayout();
    public JScrollPane jScrollPane1 = new JScrollPane();
    JTextArea jTextArea1 = new JTextArea();

    public Log_Frame(String title)
    {
		this(title, null);
    }

    public Log_Frame(String title, Rectangle bounds)
    {
		super(title);
        jbInit();
		if (bounds != null)     setBounds(bounds);
		else                    setSize(niceSize);
		this.setVisible(true);
    }

    private void jbInit()
    {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(borderLayout1);
        jTextArea1.setEditable(false);
        jTextArea1.setColumns(80);
        jTextArea1.setTabSize(4);
		jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 12));
        jTextArea1.setAutoscrolls(true);
        this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
        jScrollPane1.getViewport().add(jTextArea1, null);
    }

	public void append(String text)
	{
		jTextArea1.append(text);
		jTextArea1.setCaretPosition(jTextArea1.getText().length());
	}


}