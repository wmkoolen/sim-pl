/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package hardware;

import icons.IconManager;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import util.ExecutableFilter;
import util.ExtensionAddingFilter;
import util.FileManager;
import util.Terminator;
import about.AboutWindow;

public class HardwareBooter extends JFrame {
	public HardwareBooter() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
	        Thread.setDefaultUncaughtExceptionHandler(new Terminator());
		final HardwareBooter isfgui = new HardwareBooter();
		isfgui.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		if (args.length >= 1) isfgui.tComponent.setText(args[0]);
		if (args.length >= 2) isfgui.tDriver.   setText(args[1]);
		if (args.length >= 3) isfgui.tPeriod.   setText(args[2]);
		if (args.length >= 4) isfgui.tProgram.  setText(args[3]);

		util.DialogHelper.performOnEscapeKey(isfgui, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				isfgui.dispose();
			}
		});

		isfgui.pack();
		isfgui.setLocationRelativeTo(isfgui.getOwner());
		isfgui.setVisible(true);
	}

	private void jbInit() throws Exception {
		setTitle("Hardware");
		setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));
		jPanel1.setBorder(titledBorder1);
		titledBorder1.setTitle("Settings");
		titledBorder2.setTitle("Log");
		tComponent.setColumns(80);
		lComponent.setText("Component");
		tDriver.setColumns(80);
		lDriver.setText("Hardware Driver");
		tPeriod.setColumns(80);
		bComponent.setText("...");
		bComponent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openWASM(e);
			}
		});
		bDriver.setText("...");
		bDriver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openComponent(e);
			}
		});
		bGo.setText("Go!");
		bGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				go(e);
			}
		});
		jPanel1.setLayout(gridBagLayout1);
		lProgram.setText("Program");
		bProgram.setText("...");
		bProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openProgram(e);
			}
		});
		this.getContentPane().add(jPanel3, java.awt.BorderLayout.SOUTH);
		jPanel3.add(bGo);

		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		jPanel1.add(lComponent, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(lDriver, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(lPeriod, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(tComponent, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(tDriver, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(tPeriod, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(bComponent, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(lProgram, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(tProgram, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(bDriver, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(bProgram, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		lPeriod.setText("period (ms)");
		getRootPane().setDefaultButton(bGo);

		DocumentListener docl = new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {onChange();}
			public void removeUpdate(DocumentEvent e) {onChange();}
			public void changedUpdate(DocumentEvent e) {onChange();}
		};

		this.tComponent.getDocument().addDocumentListener(docl);
		this.tDriver.getDocument().addDocumentListener(docl);
		this.tPeriod.getDocument().addDocumentListener(docl);

		onChange();
	}

	private void onChange() {
		final File comp = new File(tComponent.getText());
		final File driver = new File(tDriver.getText());
		final File wasm = tProgram.getText().isEmpty() ? null : new File(tProgram.getText());
		try {
			long period = Long.parseLong(tPeriod.getText());
			this.bGo.setEnabled(comp.exists() && driver.exists() && (wasm==null || wasm.exists()));
		} catch (NumberFormatException ex) {
			this.bGo.setEnabled(false);
		}
	}

	JPanel jPanel1 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel lComponent = new JLabel();
	JTextField tComponent = new JTextField();
	JLabel lDriver = new JLabel();
	JTextField tDriver = new JTextField();
	JLabel lPeriod = new JLabel();
	JTextField tPeriod = new JTextField();
	JButton bComponent = new JButton();
	JButton bDriver = new JButton();

	TitledBorder titledBorder1 = new TitledBorder("");
	TitledBorder titledBorder2 = new TitledBorder("");
	JPanel jPanel3 = new JPanel();
	JButton bGo = new JButton();

	ExecutableFilter ff_driver = new ExecutableFilter();
	ExtensionAddingFilter ff_wasm =   new ExtensionAddingFilter("WASM program", ".wasm");

	JLabel lProgram = new JLabel();
	JTextField tProgram = new JTextField();
	JButton bProgram = new JButton();

	public void openWASM(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Open Component", null,
			"Open", FileManager.filter_anycmp, FileManager.filter_sim_pl,
			FileManager.filter_xml);

		if (f != null) {
			tComponent.setText(f.getAbsolutePath());
		}
	}

	public void openComponent(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Open Driver", null, "Open", ff_driver);
		if (f != null) {
			tDriver.setText(f.getAbsolutePath());
		}
	}

	public void openProgram(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Open Program", null, "Open", ff_wasm);
		if (f != null) {
			tProgram.setText(f.getAbsolutePath());
		}
	}


	public void go(ActionEvent e) {
		try {
			this.dispose();
			Hardware.main(new File(tComponent.getText()),
						  new String[]{tDriver.getText()},
						  Long.parseLong(tPeriod.getText()),
						  tProgram.getText().isEmpty() ? null : new File(tProgram.getText()));
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}


}
