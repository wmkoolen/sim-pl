/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package hardware;

import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import log.Logger;
import log.StreamLog;
import namespace.ComponentTable;
import nbit.NumberFormat;
import nbit.nBit;
import util.FileSlurper;
import util.Terminator;
import util.WAction;
import about.AboutWindow;
import about.LogoPanel;

import compiler.ComponentCompiler;
import compiler.ComponentProgram;
import compiler.wasm.WASM;
import compiler.wisent.DataSource;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Namespace;
import compiler.wisent.Type_Simple;

import databinding.Component;
import databinding.Pin;
import executer.ExecuterIconManager;
import executer.Instance_Pin;
import executer.MMA_Application;
import executer.Simulator;
import executer.timetable.TimeTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p> </p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 1.0
 */
public class Hardware extends MMA_Application {

	private static final boolean DEBUG = false;

	final LogoPanel       logoPanel = new LogoPanel();

	POD pod;


	final WAction aExit = new WAction(
		   "Exit",
		   ExecuterIconManager.EXIT_SMALL, ExecuterIconManager.EXIT_LARGE,
		   "Quit the application",
		   null,
		   KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK),
		   KeyEvent.VK_X) {

		public void actionPerformed(ActionEvent e) {
			File_Exit();
		}
	};



	BorderLayout borderLayout1 = new BorderLayout();
	JMenuBar jMenuBar1 = new JMenuBar();
	JToolBar jToolBar1 = new JToolBar();
	JMenu mFile = new JMenu();
	JMenu mHelp = new JMenu();
	public class POD {
		final Component c;
		final TimeTable tt;
		final BufferedReader p_is;
		final BufferedWriter p_os;
		final Process pIO;
		final ComponentPanel cp;

	    NumberFormat getNumberFormat() {return NumberFormat.HEXADECIMAL;}

	    POD(File f, File program, String [] args) throws Exception {
			c = ComponentTable.load(f);
			tt = new TimeTable(new Simulator(c, f));

			if (program != null) {
				ComponentCompiler cc = new WASM();
				String source = FileSlurper.slurp(program);
				Logger logger = new Logger(new StreamLog(System.out));
				ComponentProgram cp = cc.compile(source, program.getParentFile(), tt.getEffector().getInstance(), logger);
				tt.setInitialisationProgram(cp);
			}


			pIO = Runtime.getRuntime().exec(args);
			p_is = new BufferedReader(new  InputStreamReader(pIO.getInputStream()));
			p_os = new BufferedWriter(new OutputStreamWriter(pIO.getOutputStream()));

			cp = new ComponentPanel(this, tt.getEffector().getInstance());
			cp.setZoom(1);

			tt.addTimeTableListener(cp);
		}



		public void tick() throws Exception {
			ArrayList<Pin> inputs = new ArrayList<Pin>();
			// first read all outputs, write them to process
			Namespace<String,DataSource> ns = tt.getEffector().getInstance().getNamespace();
			String output = "";
			boolean first = true;
			for (Pin p : c.getIo().getPins()) {
				if (p.isInput()) {
					inputs.add(p);
					continue;
				}
				DataSource ds = ns.get(p.getName());
				if (first) first = false; else output = output + " ";
				output = output + ds.getValue().toFormattedString(NumberFormat.DECIMAL);
			}
			if (DEBUG) System.out.println("write: " + output);
			p_os.write(output + "\n");
			p_os.flush();

			// then read inputs from process, put them on pins
			String s = p_is.readLine();
			if (DEBUG) System.out.println("got: " + s);
			//System.out.println("read: " + s);
			assert s != null : "received EOF from driver"; // EOF
			String [] values = s.split("\\s+");
			assert values.length == inputs.size() : "received \"" + s + "\" but expected " + inputs.size() + " values";

			for (int i = 0; i < values.length; ++i) {
				Pin p = inputs.get(i);
				Instance_Pin ip = new Instance_Pin(tt.getEffector().getInstance(), p);
				Type_Simple ts = (Type_Simple)ns.get(p.getName()).getValue().getType();
				nBit val = NumberFormat.DECIMAL.parsePrefixfree(values[i], ts.bits);

				Instance_Simple ival = new Instance_Simple(ts, val);
				tt.changeInputTo(ip,ival);
			}
			tt.cycle();
		}

		public void close() throws Exception {
			p_is.close();
			p_os.close();
			pIO.waitFor();
		}
	}

	public Hardware() {
		jbInit();
	}

	private void jbInit() {
		this.getContentPane().setLayout(borderLayout1);
		mFile.setMnemonic('F');
		mFile.setText("File");
		mHelp.setMnemonic('H');
		mHelp.setText("Help");
		this.getContentPane().add(jToolBar1, java.awt.BorderLayout.NORTH);
		this.getContentPane().add(logoPanel, java.awt.BorderLayout.CENTER);
		this.getContentPane().add(new editor.Editor.StatusBar(), java.awt.BorderLayout.SOUTH);
		jMenuBar1.add(mFile);
		jMenuBar1.add(mHelp);
		mFile.add(aExit);
		mHelp.add(aAbout);
		mHelp.add(aWebPage);

		this.addWindowListener(new java.awt.event.WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				File_Exit();
			}
		});

		setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));

		this.setJMenuBar(jMenuBar1);
	}

	public void File_Open(File f, File program, String [] args) {
		assert pod == null;
		assert getContentPane().isAncestorOf(logoPanel);

		getContentPane().remove(logoPanel);

		try {
			this.pod = new POD(f, program, args);
			this.getContentPane().add(pod.cp, BorderLayout.CENTER);
			pod.cp.getPreferredSize();
			pod.tt.init();
		} catch (Exception ex) {
			// TODO: do something useful when file load fails.
			throw new Error(ex);
		}
	}



	private final void File_Exit() {
		if (tryCloseWithUserCancel()) System.exit(0);
	}



	public boolean tryCloseWithUserCancel() {
		return true;
		// TODO: ask user for saving if necessary
	}


	public static void main(String [] args) throws Exception {
		if (args.length < 3 || args.length > 4) throw new Error("usage: <Component> <Controller> <Period> [Program]");
		File f = new File(args[0]);
		String [] cmdline = new String []{ args[1] };
		long period = Long.parseLong(args[2]);
		File program = args.length == 3 ? null : new File(args[3]);

		main(f, cmdline, period, program);
	}

	public static void main(File f, String [] cmdline, long period, File program) throws Exception {
	        Thread.setDefaultUncaughtExceptionHandler(new Terminator());
		final Hardware e = new Hardware();
		e.pack(); // need a realised frame for successful maximisation

		e.setSize(1000,800);
		e.setLocationRelativeTo(e.getOwner());

		// nice if maximisation can be done
		if (e.getToolkit().isFrameStateSupported(JFrame.MAXIMIZED_BOTH)) {
			e.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}

		e.setVisible(true);

		e.File_Open(f, program, cmdline);


	    Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				try {
					e.pod.tick();
//					System.out.println(e.pod.tt.getCurrentTime());
				} catch (Exception ex) {
					throw new Error(ex);
				}
			}
		}, 0, period);

	}



	/**
	 * getProgramName
	 *
	 * @return String
	 * @todo Implement this executer.MMA_Application method
	 */
	public String getProgramName() { return "Hardware"; }


}
