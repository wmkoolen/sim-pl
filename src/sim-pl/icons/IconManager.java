/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package icons;

import java.awt.Image;
import java.awt.Toolkit;
import java.beans.Introspector;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IconManager {

	public static ImageIcon load(Class c, String file) {
		URL url = c.getResource(file);
		if (url == null) throw new Error("Could not resolve icon " + file + " relative to " + c);
		return new ImageIcon(url);
	}

	public static Image loadImage(Class c, String file) {
		URL url = c.getResource(file);
		if (url == null) throw new Error("Could not resolve image " + file + " relative to " + c);
		Image i = Toolkit.getDefaultToolkit().createImage(url);
		if (i==null) throw new Error("Failed to load image from " + url);
		return i;
	}

    // BeanInfo lookup + Icon query
	public static ImageIcon chargeIcon(Class c, int kind) {
		try {
			java.beans.BeanInfo bi = Introspector.getBeanInfo(c);
			Image i = bi.getIcon(kind);

			// work around java bug 7057507
			if (i==null) {
				System.err.println("Got hit by java bug 7057507. Working around it for class " + c.getName());
				Introspector.flushFromCaches(c);
				return chargeIcon(c, kind);
			}
		    return new ImageIcon(i);
		} catch (Exception ex) {
		    throw new Error(ex);
		}
	}



	public static final ImageIcon EMPTY_SMALL = load(IconManager.class, "16x16_empty.png");
	public static final ImageIcon EMPTY_LARGE = load(IconManager.class, "22x22_empty.png");


	public static final ImageIcon FILE_NEW_SMALL = load(IconManager.class, "16x16_filenew.png");
	public static final ImageIcon FILE_NEW_LARGE = load(IconManager.class, "22x22_filenew.png");

	public static final ImageIcon FILE_OPEN_SMALL = load(IconManager.class, "16x16_fileopen.png");
	public static final ImageIcon FILE_OPEN_LARGE = load(IconManager.class, "22x22_fileopen.png");

	public static final ImageIcon FILE_SAVE_SMALL = load(IconManager.class, "16x16_filesave.png");
	public static final ImageIcon FILE_SAVE_LARGE = load(IconManager.class, "22x22_filesave.png");

	public static final ImageIcon FILE_SAVEAS_SMALL = load(IconManager.class, "16x16_filesaveas.png");
	public static final ImageIcon FILE_SAVEAS_LARGE = load(IconManager.class, "22x22_filesaveas.png");

	public static final ImageIcon FILE_EXPORT_SMALL = load(IconManager.class, "16x16_fileexport.png");
	public static final ImageIcon FILE_EXPORT_LARGE = load(IconManager.class, "22x22_fileexport.png");

	public static final ImageIcon FILE_CLOSE_SMALL = load(IconManager.class, "16x16_fileclose.png");
	public static final ImageIcon FILE_CLOSE_LARGE = load(IconManager.class, "22x22_fileclose.png");


	public static final ImageIcon CUT_SMALL = load(IconManager.class, "16x16_cut.png");
	public static final ImageIcon CUT_LARGE = load(IconManager.class, "22x22_cut.png");
	public static final ImageIcon COPY_SMALL = load(IconManager.class, "16x16_copy.png");
	public static final ImageIcon COPY_LARGE = load(IconManager.class, "22x22_copy.png");
	public static final ImageIcon PASTE_SMALL = load(IconManager.class, "16x16_paste.png");
	public static final ImageIcon PASTE_LARGE = load(IconManager.class, "22x22_paste.png");
        public static final ImageIcon DELETE_SMALL = load(IconManager.class, "16x16_delete.png");
        public static final ImageIcon DELETE_LARGE = load(IconManager.class, "22x22_delete.png");



	public static final ImageIcon UNDO_SMALL = load(IconManager.class, "16x16_undo.png");
	public static final ImageIcon UNDO_LARGE = load(IconManager.class, "22x22_undo.png");

	public static final ImageIcon REDO_SMALL = load(IconManager.class, "16x16_redo.png");
	public static final ImageIcon REDO_LARGE = load(IconManager.class, "22x22_redo.png");


	public static final ImageIcon EXIT_SMALL = load(IconManager.class, "16x16_exit.png");
	public static final ImageIcon EXIT_LARGE = load(IconManager.class, "22x22_exit.png");

	public static final ImageIcon ABOUT_SMALL = load(IconManager.class, "16x16_about.png");
	public static final ImageIcon ABOUT_LARGE = load(IconManager.class, "22x22_about.png");

public static final ImageIcon ZOOM_IN_SMALL = load(IconManager.class, "16x16_zoom_in.png");
public static final ImageIcon ZOOM_IN_LARGE = load(IconManager.class, "22x22_zoom_in.png");

public static final ImageIcon ZOOM_OUT_SMALL = load(IconManager.class, "16x16_zoom_out.png");
public static final ImageIcon ZOOM_OUT_LARGE = load(IconManager.class, "22x22_zoom_out.png");

public static final ImageIcon ZOOM_100_SMALL = load(IconManager.class, "16x16_zoom_100.png");
public static final ImageIcon ZOOM_100_LARGE = load(IconManager.class, "22x22_zoom_100.png");
}
