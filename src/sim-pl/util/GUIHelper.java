/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import databinding.forms.HAlignment;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class GUIHelper {
	public static final BasicStroke unitStroke = new BasicStroke(1);

	public static Rectangle2D transform(AffineTransform f, Rectangle2D r) {
		// only works with axix-parallel transformations
		assert f.getShearX() == 0 && f.getShearY() == 0;

		Point2D p1 = new Point2D.Double(r.getX(), r.getY());
		Point2D p2 = new Point2D.Double(r.getX() + r.getWidth(), r.getY() + r.getHeight());
		f.transform(p1, p1);
		f.transform(p2, p2);
		return getRect(p1, p2);
	}

	public static Rectangle2D getRect(Point2D p1, Point2D p2) {
		return new Rectangle2D.Double(
			Math.min(p1.getX(), p2.getX()), Math.min(p1.getY(), p2.getY()),
			Math.abs(p1.getX() - p2.getX()), Math.abs(p1.getY() - p2.getY())
			);

	}

	public static Rectangle2D grow(double delta, Rectangle2D src, Rectangle2D dest) {
		if (dest == null) dest = new Rectangle2D.Double();
		dest.setFrame(src.getX()-delta, src.getY()-delta, src.getWidth() + 2*delta, src.getHeight() + 2*delta);
		return dest;
	}



	public static Rectangle2D getEncadredStringBound(Graphics2D g, Point2D ref, HAlignment halign, VAlignment valign, String s) {
		Rectangle2D   bound = g.getFontMetrics().getStringBounds(s, g);

		double px = ref.getX();
		double py = ref.getY();

		if (halign == HAlignment.center) px -= bound.getWidth()/2;
		if (halign == HAlignment.right)  px -= bound.getWidth();

		if (valign == VAlignment.center) py += g.getFontMetrics().getAscent()/2; // best way to center vertically
		if (valign == VAlignment.top)    py += bound.getHeight();

		// disregards stroke width of enclosing rectangle
		return new Rectangle2D.Double(
				  px + bound.getX() - 2,
				  py + bound.getY() - 1 ,
				  bound.getWidth() + 4,
				  bound.getHeight() + 1);
	}


	public static  void drawEncadredString(Graphics2D g, Point2D ref, HAlignment halign, VAlignment valign, String s, Color background, Color cadre, Color text) {
		Rectangle2D   bound = g.getFontMetrics().getStringBounds(s, g);

		double px = ref.getX();
		double py = ref.getY();

		if (halign == HAlignment.center) px -= bound.getWidth()/2;
		if (halign == HAlignment.right)  px -= bound.getWidth();

		if (valign == VAlignment.center) py += g.getFontMetrics().getAscent()/2; // best way to center vertically
		if (valign == VAlignment.top)    py += bound.getHeight();

		Rectangle2D rbuf = new Rectangle2D.Double(
				  px + bound.getX() - 2,
				  py + bound.getY() - 1,
				  bound.getWidth() + 4,
				  bound.getHeight()+1);
		g.setColor(background);
		g.fill(rbuf);
		g.setColor(cadre);
		g.draw(rbuf);
		g.setColor(text);
		g.drawString(s, (float)px, (float)py);
	}

	public static Rectangle2D translate(Rectangle2D src, Point2D p, Rectangle2D dest) {
		return translate(src, p.getX(), p.getY(), dest);
	}
	public static Rectangle2D translate(Rectangle2D src, double dx, double dy, Rectangle2D dest) {
		if (dest == null) dest = new Rectangle2D.Double();
		dest.setFrame(src.getX() + dx, src.getY() + dy,
					  src.getWidth(), src.getHeight());
		return dest;
	}


	/** Returns a FontRenderContext with zero translation
	 * @param g Graphics2D
	 * @return FontRenderContext
	 */
	public static FontRenderContext getPureFRC(Graphics2D g) {
		AffineTransform ot = g.getTransform();
		// suppress translation
		g.setTransform(new AffineTransform(ot.getScaleX(), ot.getShearX(),
										   ot.getShearY(), ot.getScaleY(), 0, 0));
		FontRenderContext frc = g.getFontRenderContext();
		g.setTransform(ot);
		return frc;
	}

	public static final void translate(Point2D.Double pos, int dx, int dy) {
		pos.x += dx;
		pos.y += dy;
	}



	public static class Alignment {
		public final HAlignment halign;
		public final VAlignment valign;

		public Alignment(HAlignment halign, VAlignment valign) {
			this.halign = halign;
			this.valign = valign;
		}

		public boolean equals(Object o) {
			Alignment a = (Alignment)o;
			return
				halign == a.halign &&
				valign == a.valign;
		}
	}


	public static Alignment getAlign(Point2D origin, Point2D sattelite) {
		// manhattan radius
		double dx = sattelite.getX() - origin.getX();
		double dy = sattelite.getY() - origin.getY();
		double r = Math.max(Math.abs(dx), Math.abs(dy));

		// we use strict inequality, this works nicely when r=0
		return new Alignment(
				    (dx >  r/3) ? HAlignment.left
				  : (dx < -r/3) ? HAlignment.right
				  : HAlignment.center,
				    (dy >  r/3) ? VAlignment.top
				  : (dy < -r/3) ? VAlignment.bottom
				  : VAlignment.center);
	}


	public static double minDist(Rectangle2D r, Point2D p) {
		return minDist(r.getX(), r.getY(), r.getWidth(), r.getHeight(), p.getX(), p.getY());
	}

	public static double minDist(double x, double y, double width, double height, double px, double py) {

		if (px < x) {
			if (py < y)        return Point2D.distance(px, py, x,y);
			if (py > y+height) return Point2D.distance(px, py, x, y+height);
		}
		if (px > x+width) {
			if (py < y)        return Point2D.distance(px, py, x+width,y);
			if (py > y+height) return Point2D.distance(px, py, x+width, y+height);
		}
		assert
			(x <= px && px <= x+width) ||
			(y <= py && py <= y+height);

		if (x <= px && px <= x+width) {
			if (py < y) return y-py;
			if (py > y+height) return py-y-height;
		}

		assert (y <= py && py <= y+height);
		if (px < x) return x-px;
		if (px > x+width) return px-x-width;


		assert (x <= px && px <= x+width) && (y <= py && py <= y+height);
		return Math.min(Math.min(Math.abs(x-px), Math.abs(x + width - px)),
						Math.min(Math.abs(y-py), Math.abs(y + height - py)));
	}

}
