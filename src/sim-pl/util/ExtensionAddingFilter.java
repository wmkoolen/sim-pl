/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */



// FileFilter is a class, we need to extend it (can't make an interface here)
public class ExtensionAddingFilter extends FileFilter implements java.io.FileFilter
{
	public final String description;
	public final String extension;

	public ExtensionAddingFilter(String name, String extension) {
		assert extension.startsWith(".");

		this.description = name;
		this.extension = extension;
	}

	public String getDescription() {
		return extension + " - " + description;
	}

	public boolean accept(File f) {
		return
			f.isDirectory() ||
			f.getName().toLowerCase().endsWith(extension.toLowerCase());
	}

	/** Utility function to correct extensions
	 *
	 * @param f File
	 * @param extension String
	 * @return File
	 */
	public static File correctExctension(File f, String extension) {
		assert extension.startsWith(".");

		if (f.toString().endsWith(extension)) return f;

		String s = f.getPath();
		// correct extension casing
		if (s.toLowerCase().endsWith(extension.toLowerCase())) {
			s = s.substring(0, s.length()-extension.length());
		}
		s += extension;
		return new File(s);
	}

	public File correctExtenstion (File f) {
		return correctExctension(f, extension);
	}

}
