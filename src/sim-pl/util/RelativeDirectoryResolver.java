/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * this class provides functions used to generate a relative path
 * from two absolute paths
 *
 * Loosely based on the following DevX file path tip:
  * @author David M. Howard
 * Found on http://www.devx.com/free/tips/tipview.asp?content_id=2466
 * But unfortunately no longer available
 */

public class RelativeDirectoryResolver
{
	static class RootedNameList {
		File root;
		List<String> l = new ArrayList<String>();

		private RootedNameList(File root) {
			this.root = root;
		}

		// expects an absolute canonical file
		static private RootedNameList _get(File f) {
			File parent = f.getParentFile();
			if (parent == null) return new RootedNameList(f);
			RootedNameList rnl = _get(parent);
			assert f.getName() != null && ! "".equals(f.getName());
			rnl.l.add(f.getName());
			return rnl;
		}

		static public RootedNameList get(File f) throws IOException {
			return _get(f.getAbsoluteFile().getCanonicalFile());
		}
	}


	/**
	 * get relative path of File 'f' with respect to 'home' directory
	 * example : home = /a/b/c
	 *           f    = /a/d/e/x.txt
	 *           s = getRelativePath(home,f) = ../../d/e/x.txt
	 * @param baseDirectory base path, should be a directory, not a file, or it doesn't make sense
	 * @param f file to generate path for
	 * @return path from home to f as a string
	 */
	public static String getRelativePath(File baseDirectory, File faux)
	{
		assert baseDirectory.exists();

		RootedNameList homelist, filelist;
		File f;
		try {
			f = faux.getAbsoluteFile().getCanonicalFile();
			homelist = RootedNameList.get(baseDirectory);
			filelist = RootedNameList.get(f);
		} catch (IOException ex) {
			throw new ExceptionHandler(ex);
		}

	    if (!homelist.root.equals(filelist.root)) return f.getPath();

		String s = "";
		int i,j;
		// skip common prefix
		for (i=0,j=0; i < homelist.l.size() && j < filelist.l.size() &&
			 homelist.l.get(i).equals(filelist.l.get(j)); i++,j++);

		// for each remaining level in the home path, add a ..
		for(;i<homelist.l.size();i++) {
			s += ".." + File.separator;
		}

		// for each level in the file path, add the path
		for (; j < filelist.l.size()-1; j++) {
			s += filelist.l.get(j) + File.separator;
		}

		// file name
		s += filelist.l.get(j);

		// check result
		try {
			if (!new File(baseDirectory,s).getAbsoluteFile().getCanonicalFile().equals(f))
				throw new ExceptionHandler(
					"Relative directory resolution problem; files should be equal:\n" +
					s + "\nvs\n" + f);
		} catch (IOException ex) { throw new ExceptionHandler(ex); }
		return s;
	}

}
