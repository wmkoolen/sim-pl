/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.truthtable;

import java.util.ArrayList;

import javax.swing.ProgressMonitor;
import javax.swing.table.AbstractTableModel;

import nbit.nBit;

import compiler.wisent.DataSource;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Type_Simple;

import databinding.Component;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.compiled.Compiled.CompiledPin;
import executer.NamespaceUpdate;
import executer.Simulator;
import executer.timetable.ContentChangeEvent;
import executer.timetable.TimeTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TruthTable extends AbstractTableModel {

	private Component c;
	private CompiledPin [] inputs;
	private CompiledPin [] outputs;
	private nBit [][] ival, oval;

	public TruthTable(CompiledComponent<?> cc) throws Exception {
		this (cc, cc.peer.getPreferredCycleLength());
	}

	public TruthTable(CompiledComponent<?> cc, int cycleTime) throws Exception {
		this.c = cc.peer;
		{
			ArrayList<CompiledPin> inputs = new ArrayList<CompiledPin> ();
			ArrayList<CompiledPin> outputs = new ArrayList<CompiledPin> ();

			for (CompiledPin p : cc.pin2cpin.values()) {
				if (p.peer.isInput()) inputs.add(p);
				if (p.peer.isOutput()) outputs.add(p);
			}
			this.inputs =  inputs. toArray(new CompiledPin[inputs. size()]);
			this.outputs = outputs.toArray(new CompiledPin[outputs.size()]);
		}
//		inputs = new IOPin_Simulated_Input[c.getIo().getPins()[IO.INPUTS].size()];
//		c.getIo().getPins()[IO.INPUTS].toArray(inputs);
//
//		outputs = new IOPin_Simulated_Output[c.getIo().getPins()[IO.OUTPUTS].size()];
//		c.getIo().getPins()[IO.OUTPUTS].toArray(outputs);

		Simulator s = new Simulator(c, null);
		TimeTable tt = new TimeTable(s);
		tt.setCycleLength(cycleTime);

		int input_bits = 0;
		DataSource [] ids = new DataSource[inputs.length];

		for (int i = 0; i < inputs.length; i++) {
			input_bits += inputs[i].bits;
			ids[i] = s.getInstance().getNamespace().get(inputs[i].peer.getName());
		}

		int output_bits = 0;

		for (int i = 0; i < outputs.length; i++) {
			output_bits += outputs[i].bits;
		}

		if (input_bits > 16) throw new Exception("Too many input bits (" + input_bits + "), the maximum is 16");
		if (output_bits == 0) throw new Exception("No outputs to measure");

		int lines = 1<<input_bits;
		ival = new nBit[lines][inputs.length];
		oval = new nBit[lines][outputs.length];

		ProgressMonitor pm = new ProgressMonitor(null, "Generating Truth Table", "computing", 0, oval.length);

		for (int line = 0; line < lines; line++) {
			// set the input bits
			int bits_seen = 0;
			ContentChangeEvent cce = new ContentChangeEvent(s.getInstance(), 0);
			NamespaceUpdate nsu = tt.getNamespaceUpdate(cce);

			// put LSB on the RIGHT-most input (Ben's binary code convention)
			for (int i = inputs.length-1; i >=0; i--) {
				nBit value = new nBit(inputs[i].bits, (line >>> bits_seen));
				nsu.setDataSource(inputs[i].peer.getName(), new Instance_Simple(new Type_Simple(inputs[i].bits, inputs[i].peer.isSigned()), value));
				ival[line][i] = value;
				bits_seen += inputs[i].bits;
			}

			// run number of cycles
			tt.cycle();

			// read the output bits
			for (int i = 0; i < outputs.length; i++) {
				nBit value = ((Instance_Simple)(s.getInstance().getNamespace().get(outputs[i].peer.getName())).getValue()).getValue();
				// we need a clone, for the values are used efficiently
				oval[line][i] = new nBit(value);
			}

			if (pm.isCanceled()) throw new Exception("user cancelled Truth Table generation");
			pm.setProgress(line);
		}
		pm.close();
	}

	/**
	 * Returns the most specific superclass for all the cell values in the
	 * column.
	 *
	 * @param columnIndex the index of the column
	 * @return the common ancestor class of the object values in the model.
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public Class getColumnClass(int columnIndex) {
		return TTPin.class;
	}

	/**
	 * Returns the number of columns in the model.
	 *
	 * @return the number of columns in the model
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public int getColumnCount() {
		return inputs.length + outputs.length;
	}

	/**
	 * Returns the name of the column at <code>columnIndex</code>.
	 *
	 * @param columnIndex the index of the column
	 * @return the name of the column
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public String getColumnName(int columnIndex) {
		if (columnIndex < inputs.length) return inputs[columnIndex].peer.getName();
		return outputs[columnIndex-inputs.length].peer.getName();
	}

	/**
	 * Returns the number of rows in the model.
	 *
	 * @return the number of rows in the model
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public int getRowCount() {
		return oval.length;
	}

	/**
	 * Returns the value for the cell at <code>columnIndex</code> and
	 * <code>rowIndex</code>.
	 *
	 * @param rowIndex the row whose value is to be queried
	 * @param columnIndex the column whose value is to be queried
	 * @return the value Object at the specified cell
	 * @todo Implement this javax.swing.table.TableModel method
	 */
	public TTPin getValueAt(int rowIndex, int columnIndex) {
		return columnIndex < inputs.length
			? new util.truthtable.TTPin(ival[rowIndex][columnIndex], true)
			: new util.truthtable.TTPin(oval[rowIndex][columnIndex-inputs.length], false);
	}



//	public TableModel getInputTableModel() {
//		return new AbstractTableModel() {
//			public int getColumnCount()                { return inputs.length; }
//			public int getRowCount()                   { return ival.length; }
//			public Class getColumnClass(int i)         { return TTPin.class; }
//			public String getColumnName(int i)         { return inputs[i].peer.getName(); }
//			public Object getValueAt(int row, int col) { return ival[row][col]; }
//
//		};
//	}
//
//	public TableModel getOutputTableModel() {
//		return new AbstractTableModel() {
//			public int getColumnCount()                { return outputs.length; }
//			public int getRowCount()                   { return oval.length; }
//			public Class getColumnClass(int i)         { return TTPin.class; }
//			public String getColumnName(int i)         { return outputs[i].peer.getName(); }
//			public Object getValueAt(int row, int col) { return oval[row][col]; }
//		};
//	}
}
