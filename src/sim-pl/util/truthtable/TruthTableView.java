/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.truthtable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import namespace.ComponentNamespace;
import namespace.ComponentTable;
import namespace.ParameterContext;
import util.FileManager;
import databinding.CompilationMessageBuffer;
import databinding.Component;
import databinding.compiled.Compiled.CompiledComponent;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TruthTableView extends JFrame {
	TruthTable tm;

	// very pastelized yellow and green for inputs and outputs
	public static final Color
		INPUT_BACKGROUND = new Color(250, 250, 175), // pastel yellow
	    OUTPUT_BACKGROUND = new Color(200, 230, 200); // pastel green

	// jbuilder happy constructor
	public TruthTableView() throws Exception {
//		this.tm = new TruthTable(ComponentTable.load(new File("Components/Experiments_Simulated/ALU Simple.xml")));
		jbInit();
	}

	public TruthTableView(CompiledComponent<?> cc) throws Exception {
		this(new TruthTable(cc));
	}

	public TruthTableView(TruthTable tm) {
		this.tm = tm;
		jbInit();
	}

	JTable jt = new JTable();
	JScrollPane sp = new JScrollPane();


	private void jbInit() {
		this.setTitle("Truth Table View");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jt.setModel(tm);

		jt.setDefaultRenderer(TTPin.class, new PinRenderer(INPUT_BACKGROUND, OUTPUT_BACKGROUND));
		sp.getViewport().add(jt);
		getContentPane().add(sp, BorderLayout.CENTER);

// does not work sadly
//		DialogHelper.performOnEscapeKey(this, new AbstractAction() {
//			public void actionPerformed(ActionEvent e) {
//				switch (getDefaultCloseOperation()) {
//					case JFrame.DO_NOTHING_ON_CLOSE:
//						break;
//					case JFrame.HIDE_ON_CLOSE:
//						setVisible(false);
//						break;
//					case JFrame.DISPOSE_ON_CLOSE:
//						dispose();
//						break;
//					case JFrame.EXIT_ON_CLOSE:
//						System.exit(0);
//						break;
//				}
//			}
//		});
	}



	public static void main(String [] args) throws Exception {
		File componentFile;
		if (args.length == 1) {
			componentFile = new File(args[0]);
		} else {
			FileManager fm = FileManager.getFileManager();
			componentFile = fm.askForFile(null, "Open Component", null, "Truth Table", FileManager.filter_anycmp, FileManager.filter_sim_pl, FileManager.filter_xml);
			if (componentFile == null) return;
		}

		Component c = ComponentTable.load(componentFile);
		ComponentNamespace cns = new ComponentNamespace(c);
		ParameterContext<String,Integer> pc = new ParameterContext<String,Integer>();
		CompiledComponent<?> cc = cns.compile(pc, Collections.<String>emptySet(), new CompilationMessageBuffer());

		JFrame f = new TruthTableView(cc);

		f.pack();
		f.setLocationRelativeTo(f.getOwner());
		f.setVisible(true);
	}

}


class PinRenderer extends JLabel implements TableCellRenderer {
	Color input,output;

	public PinRenderer(Color input, Color output) {
		this.input = input;
		this.output = output;
		setHorizontalAlignment(JLabel.CENTER);
	}

	public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		TTPin p = (TTPin)value;
		setText(p.value.toString());
		setOpaque(isSelected || (row&1)==0);
		setBackground(isSelected ? table.getSelectionBackground() : p.isInput ? input : output);
		return this;
	}

}
