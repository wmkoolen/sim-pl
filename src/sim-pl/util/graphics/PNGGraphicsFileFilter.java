/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import util.ExceptionHandler;
import util.ImageSource;



public class PNGGraphicsFileFilter extends GraphicsFileFilter {

	public PNGGraphicsFileFilter() {
	    super("Portable Network Grahpic", ".png");
	}

	boolean transparency = true;
	int colorType = GraphicsExporter.TYPE_COLOR;


	public class SettingsPanel extends JPanel implements ActionListener {
		public SettingsPanel() { jbInit(); }

		private void jbInit() {
			titledBorder1.setTitle("Image Type");
			jRadioButton1.setText("Black and White");
			jRadioButton2.setText("Greyscale");
			jRadioButton3.setSelected(true);
			jRadioButton3.setText("Color");
			jPanel2.setLayout(gridLayout1);
			gridLayout1.setColumns(1);
			gridLayout1.setRows(4);
			jPanel2.setBorder(titledBorder1);
			jCheckBox1.setSelected(true);
			jCheckBox1.setText("Transparency");
			jPanel2.add(jRadioButton1);
			jPanel2.add(jRadioButton2);
			jPanel2.add(jRadioButton3);
			jPanel2.add(jCheckBox1);

			this.add(jPanel2);
			buttonGroup1.add(jRadioButton1);
			buttonGroup1.add(jRadioButton2);
			buttonGroup1.add(jRadioButton3);

			jRadioButton1.addActionListener(this);
			jRadioButton2.addActionListener(this);
			jRadioButton3.addActionListener(this);
			jCheckBox1.addActionListener(this);
			jCheckBox1.setSelected(true);
		}

		ButtonGroup buttonGroup1 = new ButtonGroup();
		TitledBorder titledBorder1 = new TitledBorder("");
		JPanel jPanel2 = new JPanel();
		JRadioButton jRadioButton1 = new JRadioButton();
		JRadioButton jRadioButton2 = new JRadioButton();
		JRadioButton jRadioButton3 = new JRadioButton();
		GridLayout gridLayout1 = new GridLayout();
		JCheckBox jCheckBox1 = new JCheckBox();

		public void actionPerformed(ActionEvent e) {
			if      (e.getSource() == jRadioButton1) colorType = GraphicsExporter.TYPE_BLACKnWHITE;
			else if (e.getSource() == jRadioButton2) colorType = GraphicsExporter.TYPE_GRAYSCALE;
			else if (e.getSource() == jRadioButton3) colorType = GraphicsExporter.TYPE_COLOR;
			else if (e.getSource() == jCheckBox1)    transparency = jCheckBox1.isSelected();
		}
	}


	public JPanel getSettingsPanel() { return new SettingsPanel(); }


	public void save(OutputStream os, ImageSource source) throws IOException {

		//use dummy image to compute bounds
		BufferedImage im = new BufferedImage(1,1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = im.createGraphics();
		java.awt.Rectangle bb = source.getBounds(g2);

		int type = -1;

		switch (colorType) {
			case GraphicsExporter.TYPE_BLACKnWHITE :    type = BufferedImage.TYPE_BYTE_BINARY; break;
			case GraphicsExporter.TYPE_GRAYSCALE :      type = BufferedImage.TYPE_BYTE_GRAY; break;
			case GraphicsExporter.TYPE_COLOR :          type = transparency ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB; break;
			default: throw new ExceptionHandler("Invalid case");
		}

		// then create real image
		im = new BufferedImage(bb.width, bb.height, type);
		g2 = im.createGraphics();
		assert source.getBounds(g2).equals(bb);

		g2.translate(-bb.x, -bb.y);

		if (!transparency) {
			// explicitly render background
			g2.setColor(Color.white);
			g2.fill(bb);
		}

		// draw the image
		source.drawImage(g2);

		// and write to file
		ImageIO.write(im, "PNG", os);
	}
}
