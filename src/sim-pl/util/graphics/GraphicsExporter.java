/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import util.CurrentDirectoryManager;
import util.ExceptionHandler;
import util.ImageSource;

public class GraphicsExporter extends JFileChooser implements PropertyChangeListener
{
	// singleton design pattern
	static private   GraphicsExporter   exporter =          null;
	static final int                    TYPE_COLOR =        0;
	static final int                    TYPE_GRAYSCALE =    1;
	static final int                    TYPE_BLACKnWHITE =  2;


	private GraphicsExporter() {
		jbInit();
	}

	public static GraphicsFileFilter [] getFormats() {
		return new GraphicsFileFilter [] {
			new JPEGGraphicsFileFilter(),
			new PNGGraphicsFileFilter(),
			new SVGGraphicsFileFilter(),
			new PDFGraphicsFileFilter(),
			new EPSGraphicsFileFilter(), // last one is preferred
		};
	}


    private void jbInit()
    {
		this.setDialogTitle("Export Image");
		setCurrentDirectory(new File("."));

		setFileSelectionMode(JFileChooser.FILES_ONLY);

		setAcceptAllFileFilterUsed(false);

		this.addPropertyChangeListener(this);


		GraphicsFileFilter [] gffs = getFormats();
		for (int i = 0; i < gffs.length; i++ ) {
			addChoosableFileFilter(gffs[i]);
		}

		setMultiSelectionEnabled(false);

		setDialogTitle("Export Image to File");
		setApproveButtonText("Export"); // automatically sets Dialog type to Custom
		setApproveButtonMnemonic('E');
		setApproveButtonToolTipText("Save to file");
	}


	public static void doExport(Component parent, ImageSource source) {
		if (exporter == null) exporter = new GraphicsExporter();
		exporter.export(parent, source);
	}

    private void export(Component parent, ImageSource source)
    {
		int result = CurrentDirectoryManager.wrap(this,parent, null);

		if (result == JFileChooser.APPROVE_OPTION) {

			GraphicsFileFilter gff = (GraphicsFileFilter)getFileFilter();
			File f = gff.correctExtenstion(getSelectedFile());
			try {

				if (f.exists()) {
					if (JOptionPane.showConfirmDialog(parent, "Overwrite the following file?\n"+ f.getName(), "File already exists",  JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
						!= JOptionPane.YES_OPTION) return;
				}
				OutputStream os = new FileOutputStream(f);
				try {
					gff.save(os, source /*, getColorType()*/);
				} finally {
					os.close();
				}
			} catch (IOException ex) {
				ExceptionHandler.handleException(ex);
			}

		}
    }

	public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
		if (propertyChangeEvent.getPropertyName().equals(JFileChooser.FILE_FILTER_CHANGED_PROPERTY)) {
			JComponent accessory = ( (GraphicsFileFilter) getFileFilter()).getSettingsPanel();
			setAccessory(accessory);
			revalidate();
		}
	}
}
