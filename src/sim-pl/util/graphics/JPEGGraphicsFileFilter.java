/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.swing.JPanel;

import util.ExceptionHandler;
import util.ImageSource;


public class JPEGGraphicsFileFilter extends GraphicsFileFilter {

	public JPEGGraphicsFileFilter() {
	    super("Joint Photographic Expert Group", ".jpeg");
	}

	int colorType = GraphicsExporter.TYPE_COLOR;
	float compression = 1.0f;
	public JPanel getSettingsPanel() { return null; }


	public void save(OutputStream os, ImageSource source) throws IOException {

		//use dymmy image to compute bounds
		BufferedImage im = new BufferedImage(1,1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = (Graphics2D)im.getGraphics();
		java.awt.Rectangle bb = source.getBounds(g2);

		int type = -1;

		switch (colorType) {
			case GraphicsExporter.TYPE_BLACKnWHITE :    type = BufferedImage.TYPE_BYTE_BINARY; break;
			case GraphicsExporter.TYPE_GRAYSCALE :      type = BufferedImage.TYPE_BYTE_GRAY; break;
			case GraphicsExporter.TYPE_COLOR :          type = BufferedImage.TYPE_INT_RGB; break;
			default: throw new ExceptionHandler("Invalid case");
		}

		// then create real image
		im = new BufferedImage(bb.width, bb.height, type);
		g2 = (Graphics2D)im.getGraphics();

		g2.translate(-bb.x, -bb.y);

		// explicitly render background
		g2.setColor(Color.white);
		g2.fill(bb);

		// draw the image
		source.drawImage(g2);

		// and write to file
		ImageWriter iw = ImageIO.getImageWritersByFormatName("jpeg").next();
		iw.setOutput(ImageIO.createImageOutputStream(os));
		// disable image compression

        ImageWriteParam iwparam = iw.getDefaultWriteParam();
        iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(compression);

        // Write the image
        iw.write(null, new IIOImage(im, null, null), iwparam);

		os.flush();
		iw.dispose();
	}
}
