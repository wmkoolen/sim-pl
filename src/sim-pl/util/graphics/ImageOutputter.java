/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import log.Logger;
import log.StreamLog;
import namespace.ComponentNamespace;
import namespace.ComponentTable;
import namespace.ParameterContext;
import nbit.Hexadecimal;
import nbit.NumberFormat;
import util.GUIHelper;
import util.ImageSource;

import compiler.ComponentProgram;
import compiler.wasm.WASM;

import databinding.CompilationMessageBuffer;
import databinding.Component;
import databinding.compiled.Compiled.CompiledComponent;
import editor.RenderContext;
import executer.Probe;
import executer.Simulator;
import executer.timetable.TimeTable;

/** Exports (editor) images to file
 */

public class ImageOutputter implements ImageSource {
	

	// mask small errors in bounding rectangle
	private static final int MARGIN = 5;

	final RenderContext rc;
	final CompiledComponent<?> cc;

	public ImageOutputter(Component c, CompiledComponent<?> cc, boolean renderLabels, boolean renderProbeStubs) {
		this.rc = new RenderContext(c, renderLabels, renderProbeStubs); // render probestubs too (we're editor)
		this.cc = cc;
	}

	public java.awt.Rectangle getBounds(Graphics2D g) {
		Rectangle2D bound = rc.getRenderRoot().getDeepBound(g);
		GUIHelper.grow(MARGIN, bound, bound);
		return bound.getBounds();
	}

	public void drawImage(Graphics2D g) {
		rc.getRenderRoot().render(g, cc);
	}

	public static GraphicsFileFilter lookup(String format) throws Exception {
		String extension = "."+format.toUpperCase();
		GraphicsFileFilter [] gffs = GraphicsExporter.getFormats();
		for (int i = 0; i < gffs.length; i++) {
			if (gffs[i].extension.toUpperCase().equals(extension)) {
				return gffs[i];
			}
		}
		throw new Exception("Unknown graphics file format: " + format);
	}


    static class InvalidUsageException extends Exception {}



	public static void bail_with_usage() throws InvalidUsageException {
	    throw new InvalidUsageException();
	}


	public static void main(String[] args) throws Exception {
	    try {
		go(args);
	    } catch (InvalidUsageException ex) {
		System.err.println("Usage: [options] <input component>\n" +
				"where [options] is a sequence of\n" +
				" -labels <bool>          : draw labels (default true)\n" +
				" -probes <bool>          : draw probes (default false)\n" +
				" -format <format>        : select output format\n" +
				" -output <file>          : set output file\n" +
				" -program <file>         : load program file\n" +
				" -cycles <int>           : execute number of cycles\n" +
				" -numberformat <format>  : set number format\n" +
				" -zoom <double>          : set zoom\n" +
				" -antialiasing <bool>    : set antialias (default false)\n" +
				" -help                   : print this\n");
		System.exit(1);
	    }
	}


	public static void go(String[] args) throws Exception {
		boolean labels = true;
		boolean probes = false;
		File out = null;
		File componentFile = null;
		GraphicsFileFilter gff = lookup("PDF");
		
		File    program = null;
		Integer cycles =  null;
		NumberFormat nf = Hexadecimal.HEXADECIMAL;
		
		Double zoom = null;
		Boolean antialias = null;
		
		
		for (int i = 0; i < args.length; ++i) {
			if (args[i].equals("-help")) {
				bail_with_usage();
			} else if (args[i].equals("-labels")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				labels = Boolean.parseBoolean(args[i]);
			} else if (args[i].equals("-probes")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				probes = Boolean.parseBoolean(args[i]);
			} else if (args[i].equals("-output")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				out = new File(args[i]);
			} else if (args[i].equals("-format")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				gff = lookup(args[i]);
			} else if (args[i].equals("-program")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				program = new File(args[i]);
			} else if (args[i].equals("-cycles")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				cycles = Integer.parseInt(args[i]);
			} else if (args[i].equals("-numberformat")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				nf = NumberFormat.parse(args[i]);
			} else if (args[i].equals("-zoom")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				zoom = Double.parseDouble(args[i]);
			} else if (args[i].equals("-antialiasing")) {
				if (i+1==args.length) bail_with_usage();
				i++;
				antialias = Boolean.parseBoolean(args[i]);
			} else {
				componentFile = new File(args[i]);
			}
		}
		
		if (out == null || componentFile == null || gff == null) bail_with_usage();
		
		Component c = ComponentTable.load(componentFile);
		
		
		ImageSource io;
		
		if (program == null && cycles == null) {
			// no need to start the simulator
			
			ComponentNamespace cns = new ComponentNamespace(c);
			ParameterContext<String,Integer> pc = new ParameterContext<String,Integer>();
			CompilationMessageBuffer cmb = new CompilationMessageBuffer();
			CompiledComponent<?> cc = cns.compile(pc, Collections.<String>emptySet(), cmb);
			
			io = new ImageOutputter(c, cc, labels, probes);
			
		} else {
			// start simulator
			Simulator s = new Simulator(c, componentFile);
			TimeTable tt = new TimeTable(s);
			
			if (program != null) {
				WASM wASM = new WASM();
				ComponentProgram p = wASM.compile(program, s.getInstance(), new Logger(new StreamLog(System.out)));
				tt.setInitialisationProgram(p);
			}
			tt.init();
			
			
			final List<Probe> probeList = new ArrayList<Probe>();
			s.collectProbes(new AffineTransform(), probeList, s.getInstance());
			
			final NumberFormat fnf = nf;
			
			if (cycles != null)  tt.cycle(cycles);
			io = new ImageOutputter(c, s.getCompiledComponent(), labels, probes) {
				public void drawImage(Graphics2D g) {
					super.drawImage(g);
					g.setStroke(GUIHelper.unitStroke);
					for (Probe p : probeList) {
						p.render(g, fnf);
					}
				}				
			};
		}
		
		if (zoom != null) io = new ZoomWrapper(io, AffineTransform.getScaleInstance(zoom, zoom));
		if (antialias != null) io = new AntiAliasWrapper(io, antialias);

		FileOutputStream os = new FileOutputStream(out);
		try {
			gff.save(os, io);
		} finally {
			os.close();
		}
	}
}
