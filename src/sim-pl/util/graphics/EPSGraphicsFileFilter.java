/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import org.sourceforge.jlibeps.epsgraphics.EpsGraphics2D;

import util.ExceptionHandler;
import util.ImageSource;


class EPSGraphicsFileFilter extends GraphicsFileFilter {
	public EPSGraphicsFileFilter() {
		super("Encapsulated PostScript", ".eps");
	}

	int colorType = GraphicsExporter.TYPE_COLOR;

	class SettingsPanel extends JPanel implements ActionListener {
		public SettingsPanel() { jbInit(); }

		private  void jbInit() {
			titledBorder1.setTitle("Image Type");
			jRadioButton1.setText("Black and White");
			jRadioButton2.setText("Greyscale");
			jRadioButton3.setSelected(true);
			jRadioButton3.setText("Color");
			jPanel2.setLayout(gridLayout1);
			gridLayout1.setColumns(1);
			gridLayout1.setRows(3);
			jPanel2.setBorder(titledBorder1);
			jPanel2.add(jRadioButton1);
			jPanel2.add(jRadioButton2);
			jPanel2.add(jRadioButton3);

			this.add(jPanel2);
			buttonGroup1.add(jRadioButton1);
			buttonGroup1.add(jRadioButton2);
			buttonGroup1.add(jRadioButton3);

			jRadioButton1.addActionListener(this);
			jRadioButton2.addActionListener(this);
			jRadioButton3.addActionListener(this);
		}

		ButtonGroup buttonGroup1 = new ButtonGroup();
		TitledBorder titledBorder1 = new TitledBorder("");
		JPanel jPanel2 = new JPanel();
		JRadioButton jRadioButton1 = new JRadioButton();
		JRadioButton jRadioButton2 = new JRadioButton();
		JRadioButton jRadioButton3 = new JRadioButton();
		GridLayout gridLayout1 = new GridLayout();

		public void actionPerformed(ActionEvent e) {
			if      (e.getSource() == jRadioButton1) colorType = GraphicsExporter.TYPE_BLACKnWHITE;
			else if (e.getSource() == jRadioButton2) colorType = GraphicsExporter.TYPE_GRAYSCALE;
			else if (e.getSource() == jRadioButton3) colorType = GraphicsExporter.TYPE_COLOR;
		}
	}

	public JPanel getSettingsPanel() { return new SettingsPanel(); }

	public void save(OutputStream s, ImageSource source) throws IOException {

		//use dymmy image to compute bounds
		EpsGraphics2D g2 = new EpsGraphics2D();
		Rectangle bb = source.getBounds(g2);

		// then create real image
		g2 = new EpsGraphics2D("SIM-PL excerpt", s, bb.x, bb.y, bb.x+bb.width, bb.y+bb.height);

		switch (colorType) {
			case GraphicsExporter.TYPE_BLACKnWHITE :    g2.setColorDepth(EpsGraphics2D.BLACK_AND_WHITE); break;
			case GraphicsExporter.TYPE_GRAYSCALE :      g2.setColorDepth(EpsGraphics2D.GRAYSCALE); break;
			case GraphicsExporter.TYPE_COLOR :          g2.setColorDepth(EpsGraphics2D.RGB); break;
			default: throw new ExceptionHandler("Invalid case");
		}

		// draw the image
		source.drawImage(g2);

		// and write to file
		g2.close();
	}
}


