/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JPanel;

import util.ImageSource;
import util.VersionManager;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;




public class PDFGraphicsFileFilter extends GraphicsFileFilter {
	public PDFGraphicsFileFilter() {
		super("Portable Document Format", ".pdf");
	}

	public JPanel getSettingsPanel() { return null; }

	public void save(OutputStream s, ImageSource source) throws IOException {

		try {

			Rectangle bb;
			// first create a dummy document to get the bounds right
			{
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				com.lowagie.text.Document doc = new com.lowagie.text.Document();
				PdfWriter w = PdfWriter.getInstance(doc, bos);
				doc.open();
				PdfContentByte cb = w.getDirectContent();

				Graphics2D dummyg2 = cb.createGraphics(0, 0);

				bb = source.getBounds(dummyg2);

				bos.close();
				dummyg2.dispose();
			}

			com.lowagie.text.Document document = new com.lowagie.text.Document(new com.lowagie.text.Rectangle(
				bb.width, bb.height));
			document.addCreator(VersionManager.currentVersion);
			document.addCreationDate();
			PdfWriter writer = PdfWriter.getInstance(document, s);

			document.open();
			PdfContentByte cb = writer.getDirectContent();
			PdfTemplate tp = cb.createTemplate(bb.width, bb.height);
			Graphics2D g2 = tp.createGraphics(bb.width, bb.height,
											  new DefaultFontMapper());
			g2.translate( -bb.x, -bb.y);


			// draw the image
			source.drawImage(g2);

			// and write to file
			g2.dispose();
			cb.addTemplate(tp, 0, 0);
			document.close();
		}
		catch (DocumentException ex) { throw new IOException(ex.toString()); }
	}
}


