package util.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import util.ImageSource;

public class AntiAliasWrapper implements ImageSource {
	
	final ImageSource wrapped;
	final boolean antialias;
	
	public AntiAliasWrapper(ImageSource wrapped, boolean antialias) {
		this.wrapped = wrapped;
		this.antialias = antialias;
	}

	public Rectangle getBounds(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antialias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
		return wrapped.getBounds(g);
	}

	public void drawImage(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antialias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
		wrapped.drawImage(g);
	}
}
