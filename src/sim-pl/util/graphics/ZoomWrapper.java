package util.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import util.GUIHelper;
import util.ImageSource;

public class ZoomWrapper implements ImageSource {
	
	final ImageSource wrapped;
	final AffineTransform t;
	
	public ZoomWrapper(ImageSource wrapped, AffineTransform t) {
		this.wrapped = wrapped;
		this.t = t;
	}

	public Rectangle getBounds(Graphics2D g) {
		AffineTransform old = g.getTransform();
		g.transform(t);   		// scaling 'g' does not scale the returned bounds (but provides hints to the text rendering system)
		Rectangle r = wrapped.getBounds(g);
		g.setTransform(old);
		return GUIHelper.transform(t, r).getBounds(); 
	}

	public void drawImage(Graphics2D g) {
		AffineTransform old = g.getTransform();
		g.transform(t);
		wrapped.drawImage(g);
		g.setTransform(old);
	}
}