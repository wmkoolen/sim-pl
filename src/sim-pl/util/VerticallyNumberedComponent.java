/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.JComponent;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class VerticallyNumberedComponent extends JComponent {
	Font brotherFont = getFont();
	boolean alignLeft = true;

	public VerticallyNumberedComponent() {}

//	public VerticallyNumberedComponent(Font brotherFont, boolean alignLeft) {
//		setFont(brotherFont);
//		setAlign(alignLeft);
//	}

	public void setFont(Font brotherFont) {
		this.brotherFont = brotherFont;
		super.setFont(brotherFont);
		this.revalidate();
	}

	public void setAlign(boolean alignLeft) {
		this.alignLeft = alignLeft;
		this.revalidate();
	}

	public Dimension getPreferredSize() {
		Dimension d = super.getPreferredSize();
		Insets i = getInsets();
		return new Dimension(getGraphics().getFontMetrics(getFont()).stringWidth("999")+ i.left + i.right + 4, d.height);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle r = g.getClipBounds();
		FontMetrics fm = g.getFontMetrics(brotherFont);
		int h = fm.getHeight();

		int max = (r.y + r.height+h)/h; // round down plus one, so one too high
		Insets is = getInsets();
		int right = getWidth()-2-is.right;
		int left = is.left+2;

		g.setColor(getForeground());
		for (int i = r.y/h; i < max; i++) {
			String val = Integer.toString(i+1);
			int p = alignLeft ? left : right-fm.stringWidth(val);
			g.drawString(val, p, is.top+i*h+fm.getAscent());
		}
	}
}
