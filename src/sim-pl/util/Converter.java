/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Color;


public class Converter
{
	public static char [] Hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	private static int toNum(char hex) throws Exception
	{
		if (hex >= '0' && hex <= '9') return hex-'0';
		if (hex >= 'a' && hex <= 'f') return hex-'a' + 10;
		if (hex >= 'A' && hex <= 'F') return hex-'A' + 10;
		throw new Exception("Invalid hexadecimal digit: " + hex);
	}

	public static String toString(Color c)
	{
		StringBuffer s = new StringBuffer();
		s.append('#');
		s.append(Hex[c.getRed()   >> 4]);
		s.append(Hex[c.getRed()   & 15]);
		s.append(Hex[c.getGreen() >> 4]);
		s.append(Hex[c.getGreen() & 15]);
		s.append(Hex[c.getBlue()  >> 4]);
		s.append(Hex[c.getBlue()  & 15]);
		if (c.getAlpha() != 255) {
			s.append(Hex[c.getAlpha()  >> 4]);
			s.append(Hex[c.getAlpha()  & 15]);
		}

		return s.toString();
	}
//
//	public static String toString(boolean b)
//	{
//		return b ? "true" : "false";
//	}
//
//
	public static Color toColor(String s) throws Exception {
		if (s.charAt(0) != '#' || (s.length() != 7 && s.length() != 9))
			throw new Exception("Invalid color format");

	    int r = toNum(s.charAt(1)) << 4 | toNum(s.charAt(2));
		int g = toNum(s.charAt(3)) << 4 | toNum(s.charAt(4));
		int b = toNum(s.charAt(5)) << 4 | toNum(s.charAt(6));
		int a = s.length() == 9 ? toNum(s.charAt(7)) << 4 | toNum(s.charAt(8)) : 255;

		return new Color(r, g, b, a);
	}
//
//	public static Color toColor(NamedNodeMap atts, String attributeName, Color defaultColor) throws Exception
//	{
//		Node x = atts.getNamedItem(attributeName);
//		if (x != null) {
//			return toColor(x.getNodeValue());
//		}
//		else
//		{
//			if (defaultColor == null) throw new Exception("Attribute " + attributeName + " not found");
//			return defaultColor;
//		}
//	}
//
//	public static boolean toBoolean(NamedNodeMap atts, String attributeName, boolean defaultBoolean) throws Exception
//	{
//		Node x = atts.getNamedItem(attributeName);
//		if (x != null)		{
//			return toBoolean(x.getNodeValue(), defaultBoolean);
//		} else {
//			return defaultBoolean;
//		}
//	}
//
//	public static boolean toBoolean(String s, boolean defaultBoolean) throws Exception
//	{
//		if (s != null) return toBoolean(s);
//		else           return defaultBoolean;
//	}
//
//	public static boolean toBoolean(String s) throws Exception
//	{
//		if (s.equals("true")) return true;
//		if (s.equals("false")) return false;
//		else throw new Exception("Invalid boolean value: " + s);
//	}
//
//
//
//	public static int toInteger(NamedNodeMap atts, String attributeName, Integer defaultInteger) throws Exception
//	{
//		Node x = atts.getNamedItem(attributeName);
//		if (x != null)  return Integer.parseInt(x.getNodeValue().trim());
//		if (defaultInteger == null) throw new Exception("Attribute " + attributeName + " not found");
//		return defaultInteger.intValue();
//	}
//
//	public static int toInteger(String s) throws Exception {
//		return Integer.parseInt(s.trim());
//	}
//
//
//	public static double toDouble(NamedNodeMap atts, String attributeName, double defaultDouble) throws Exception
//	{
//		Node x = atts.getNamedItem(attributeName);
//		if (x != null)
//		{
//			return Double.parseDouble(x.getNodeValue());
//		}
//		else
//		{
//			return defaultDouble;
//		}
//	}
//
//
//	public static Point toPoint(NamedNodeMap atts, String attributeName_X, String attributeName_Y, Point defaultPoint) throws Exception
//	{
//		Node x = atts.getNamedItem(attributeName_X);
//		Node y = atts.getNamedItem(attributeName_Y);
//
//		if (x == null && y == null && defaultPoint != null) return defaultPoint;
//
//		     if (x == null) throw new Exception(attributeName_X + " expected");
//		else if (y == null) throw new Exception(attributeName_Y + " expected");
//		else return new Point(Integer.parseInt(x.getNodeValue().trim()), Integer.parseInt(y.getNodeValue().trim()));
//	}
//
//	public static HAlignment toHAlignment(NamedNodeMap atts, String attributeName, HAlignment defaultHAlignment) throws Exception
//	{
//		Node h_alignment = atts.getNamedItem(attributeName);
//
//		if (h_alignment == null)    return defaultHAlignment;
//		else                        return HAlignment.parseHAlignment(h_alignment.getNodeValue());
//	}
//
//
//	public static VAlignment toVAlignment(NamedNodeMap atts, String attributeName, VAlignment defaultVAlignment) throws Exception
//	{
//		Node v_alignment = atts.getNamedItem(attributeName);
//
//		if (v_alignment == null)    return defaultVAlignment;
//		else                        return VAlignment.parseVAlignment(v_alignment.getNodeValue());
//	}
//
//
//	public static NumberFormat toNumberFormat(NamedNodeMap atts, String attributeName, NumberFormat defaultNumberFormat) throws Exception
//	{
//		Node element = atts.getNamedItem(attributeName);
//
//		if (element == null) return defaultNumberFormat;
//		else                 return NumberFormat.parse(element.getNodeValue());
//	}

}
