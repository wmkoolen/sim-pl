/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class MyFormatter
{
	public static String pad(Object o, int length)
	{
		StringBuffer sb = new StringBuffer();
		String s = o.toString();

		if (s.length() > length)
		{
			sb.append(s.substring(0, length-3));
			sb.append("...");
		}
		else
		{
			sb.append(s);
	    	for (int i = s.length(); i < length; i++) sb.append(' ');
		}

		return sb.toString();
	}

	public static String unformat(Object o)
	{
		StringBuffer sb = new StringBuffer();
		String s = o.toString();
		int length = s.length();

		for (int i = 0; i < length; i++)
		{
			char c = s.charAt(i);
			     if (c == '\t') sb.append("\\t");
			else if (c == '\n') sb.append("\\n");
			else if (c == '\r') sb.append("\\r");
		    else if (c < 32)  sb.append("(#" + (int)c + ")");
			else if (c > 127) sb.append("(#" + (int)c + ")");
			else sb.append(c);
		}

		return sb.toString();
	}

	public static String join(String glue, Collection c) {
		StringBuffer b = new StringBuffer();
		for (Iterator it = c.iterator(); it.hasNext(); ) {
			b.append(it.next());
			if (it.hasNext()) b.append(glue);
		}
		return b.toString();
	}

	public static String join(String mapsTo, String glue, Map m) {
		StringBuffer b = new StringBuffer();
		for (Iterator it = m.keySet().iterator(); it.hasNext(); ) {
			Object k = it.next();
			Object v = m.get(k);
			b.append(k);
			b.append(mapsTo);
			b.append(v);
			if (it.hasNext()) b.append(glue);
		}
		return b.toString();
	}


}
