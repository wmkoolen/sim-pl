/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class UnionFind <T> {
	Map<T, T> n2rep = new HashMap<T,T>();

	public boolean contains(T n) {
		return n2rep.containsKey(n);
	}

	public void add(T n) {
		assert !n2rep.containsKey(n) : n + " already added";
		n2rep.put(n,n);
	}

	public void union(T ... ns) {
		if (ns.length == 0) return;

		assert n2rep.containsKey(ns[0]) : ns[0] + " not yet added";
		T r = find(ns[0]);

		for (int i = 1; i < ns.length; ++i) {
			assert n2rep.containsKey(ns[i]) : ns[i] + " not yet added";
			T r2 = find(ns[i]);
			n2rep.put(r2, r); // also fine when r2 == r
		}
	}


//	public void union(T n1, T n2) {
//		assert n2rep.containsKey(n1) : n1 + " not yet added";
//		assert n2rep.containsKey(n2) : n2 + " not yet added";
//
//		T r1 = find(n1);
//		T r2 = find(n2);
//
//		assert n2rep.get(r1) == r1;
//		assert n2rep.get(r2) == r2;
//
//		n2rep.put(r1, r2); // also fine when r1 == r2
//	}

	public T find(T n) {
		assert n2rep.containsKey(n) : n + " not yet added";

		T r = n2rep.get(n);
		if (r == n) return r;
		r = find(r);
		n2rep.put(n,r);
		assert n2rep.get(r) == r;
		return r;
	}

	public Collection<? extends Collection<T>> getEqvClasses() {
		Map<T,ArrayList<T>> cls = new HashMap<T, ArrayList<T>>();

		for (Entry<T,T> e : n2rep.entrySet()) {
			T r = find(e.getValue());
			ArrayList<T> eqvcls = cls.get(r);
			if (eqvcls == null) cls.put(r, eqvcls = new ArrayList<T>());
			eqvcls.add(e.getKey());
		}
		return cls.values();
	}
}
