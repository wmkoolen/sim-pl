/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

abstract public class WAction extends AbstractAction {
	public static final String LARGE_ICON = AbstractAction.class.getName() + "_LARGE_ICON";

	public WAction(String name, Icon smallIcon, Icon largeIcon, String shortDescription, String longDescription, KeyStroke acceleratorKey, int mnemonicKey) {
		super(name, smallIcon);
		assert(getValue(SMALL_ICON) == smallIcon);
		putValue(LARGE_ICON, largeIcon);
		putValue(CustomizableToolbarButton.TOOLBAR_ICON, largeIcon);
		putValue(SHORT_DESCRIPTION, shortDescription);
		putValue(LONG_DESCRIPTION, longDescription);
		putValue(ACCELERATOR_KEY, acceleratorKey);
		putValue(MNEMONIC_KEY, new Integer(mnemonicKey));
	}

	public WAction(String name, boolean ellipsis, Icon smallIcon, Icon largeIcon, String shortDescription, String longDescription, KeyStroke acceleratorKey, int mnemonicKey) {
		this(ellipsis ? name + " ..." : name, smallIcon, largeIcon, shortDescription, longDescription, acceleratorKey, mnemonicKey);
		if (ellipsis) putValue(CustomizableToolbarButton.TOOLBAR_TEXT, name);
	}
}
