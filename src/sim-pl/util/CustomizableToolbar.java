/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

public class CustomizableToolbar extends JToolBar
{

    JPopupMenu jPopupMenu1 = new JPopupMenu("Labels");
    JCheckBoxMenuItem cbmiBelow = new JCheckBoxMenuItem();
    JCheckBoxMenuItem cbmiLeft = new JCheckBoxMenuItem();
    JCheckBoxMenuItem cbmiRight = new JCheckBoxMenuItem();
    JCheckBoxMenuItem cbmiIconsOnly = new JCheckBoxMenuItem();
    JCheckBoxMenuItem cbmiLabelsOnly = new JCheckBoxMenuItem();
	ButtonGroup buttonGroup1 = new ButtonGroup();


    public CustomizableToolbar()
    {
		jbInit();
    }

	void jbInit() {
        cbmiBelow.setText("Labels below icons");
        cbmiLeft.setText("Labels left of icons");
        cbmiRight.setText("Labels right of icons");
        cbmiIconsOnly.setText("Icons only");
        cbmiLabelsOnly.setText("Labels only");

        jPopupMenu1.add(cbmiLabelsOnly);
        jPopupMenu1.add(cbmiIconsOnly);
        jPopupMenu1.add(cbmiBelow);
        jPopupMenu1.add(cbmiLeft);
        jPopupMenu1.add(cbmiRight);

		buttonGroup1.add(cbmiBelow);
		buttonGroup1.add(cbmiLeft);
		buttonGroup1.add(cbmiRight);
		buttonGroup1.add(cbmiIconsOnly);
		buttonGroup1.add(cbmiLabelsOnly);

		ActionListener a = new ActionListener() {
			public void actionPerformed(ActionEvent e)    {
				correctToolbarLook();
			}
		};

		setRollover(true);

		cbmiBelow.addActionListener(a);
		cbmiLeft.addActionListener(a);
		cbmiRight.addActionListener(a);
		cbmiIconsOnly.addActionListener(a);
		cbmiLabelsOnly.addActionListener(a);

        addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e) {
				checkToolbarPopup(e);

            }
		    public void mouseReleased(MouseEvent e) {
				checkToolbarPopup(e);
            }
        });

		cbmiIconsOnly.setSelected(true);
	}

	public JButton add(Action a) {
		throw new Error("Deprectated method called");
	}

	public CustomizableToolbarButton add(WAction a) {
		CustomizableToolbarButton ctb = new CustomizableToolbarButton(a);
		add(ctb);
		return ctb;
	}

	public Component add(Component c) {
		correctComponentLook(c, getDesiredHorizontal(), getDesiredVertical());
		return super.add(c);
	}

	public void setAction(CustomizableToolbarButton ctb, Action a) {
		ctb.setAction(a);
		correctComponentLook(ctb, getDesiredHorizontal(), getDesiredVertical());
	}

	protected void correctComponentLook(Component c, int horizontal, int vertical) {
		// work around separators (and other silly stuff)
		if (c instanceof CustomizableToolbarButton) {
			CustomizableToolbarButton ctb = (CustomizableToolbarButton) c;
			ctb.setShowIcon(!cbmiLabelsOnly.isSelected());
			ctb.setShowText(!cbmiIconsOnly.isSelected());
			ctb.setVerticalTextPosition(vertical);
			ctb.setHorizontalTextPosition(horizontal);
		}
	}

	protected int getDesiredHorizontal() {
		return
					cbmiLeft.isSelected() ?    SwingConstants.LEADING  :
					cbmiRight.isSelected() ?   SwingConstants.TRAILING :
					                           SwingConstants.CENTER;
	}

	protected int getDesiredVertical() {
		return cbmiBelow.isSelected() ? SwingConstants.BOTTOM : SwingConstants.CENTER;
	}

    public void correctToolbarLook()
    {
		int horizontal = getDesiredHorizontal();
		int vertical =   getDesiredVertical();

		Component [] cs = getComponents();
		for (int i = 0; i < cs.length; i++) {
			correctComponentLook(cs[i], horizontal, vertical);
		}

		revalidate();
    }

    void checkToolbarPopup(MouseEvent e)
    {
		if (e.isPopupTrigger()) {
			jPopupMenu1.show(e.getComponent(), e.getX(), e.getY());
		}
    }

}
