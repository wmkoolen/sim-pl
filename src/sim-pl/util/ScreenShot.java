/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ScreenShot {

	private ScreenShot() {}

	static abstract class ComponentExporter extends AbstractAction implements ImageSource {
		Component w;

		protected void go(Component c) {
			System.out.println("Printing for " + c + "\nin" + this.getClass().getName());
			w = c;
			util.graphics.GraphicsExporter.doExport(w, this);
		}

		public Rectangle getBounds(Graphics2D g) {
			// w.getBounds() is incorrect, it is in screen coordinates
			return new Rectangle(w.getSize());
		}

		public void drawImage(Graphics2D g) {
			w.printAll(g);
		}
	}


	static class WholeScreen extends ComponentExporter {
		/** The current action prints the whole window, including title bar and border */
		public void actionPerformed(ActionEvent e) {
			go(SwingUtilities.getWindowAncestor((Component)e.getSource()));
		}
	}

	static WholeScreen ws;
	static final String wsName = "printWholeScreen";


	public static void installHook(JRootPane p) {
		if (ws == null) ws = new WholeScreen();

		KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_MASK);
		p.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(ks, wsName);
		p.getActionMap().put(wsName, ws);
	}
}
