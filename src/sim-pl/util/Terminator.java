/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Terminator implements Thread.UncaughtExceptionHandler {

    public void uncaughtException(Thread t, Throwable ex) {
	ex.printStackTrace();
	StringWriter sw = new StringWriter();
	ex.printStackTrace(new PrintWriter(sw));

	String s = 
	    "SIM-PL encountered an internal error.\n" +
	    "This means that it is unsafe to continue.\n" +
	    "Any unsaved information will be lost.\n" +
	    "We are very sorry for the inconvenience.\n" +
	    "\n" +
	    "Please let us know about this problem by sending an email to " + VersionManager.currentEmail + "\n" +
	    "Describe (in a few lines) what you were doing when this problem occurred, and include the information below.\n" +
	    "\n\n" +
	    "Version: " + VersionManager.currentVersion + "\n" +
	    sw;

	JTextArea jta = new JTextArea(s, 25, 80);
	JOptionPane.showMessageDialog(null, new JScrollPane(jta), "Error", JOptionPane.ERROR_MESSAGE);
	System.exit(-1);
    }
}
