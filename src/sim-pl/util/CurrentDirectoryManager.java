/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CurrentDirectoryManager {
	private static File currentDirectory  = new File(System.getProperty("user.dir"));

	public static File getCurrentDirectory() {
		return currentDirectory;
	}

	public static void setCurrentDirectory(File cd) {
		currentDirectory = cd;
	}

	public static int wrap(JFileChooser jfc, Component c, String approveButtonText) {
		jfc.setCurrentDirectory(getCurrentDirectory());
		int result = jfc.showDialog(c, approveButtonText);
		setCurrentDirectory(jfc.getCurrentDirectory());
		return result;
	}

	public static int wrapOpen(JFileChooser jfc, Component c) {
		jfc.setCurrentDirectory(getCurrentDirectory());
		int result = jfc.showOpenDialog(c);
		setCurrentDirectory(jfc.getCurrentDirectory());
		return result;
	}

	public static int wrapSave(JFileChooser jfc, Component c) {
		jfc.setCurrentDirectory(getCurrentDirectory());
		int result = jfc.showSaveDialog(c);
		setCurrentDirectory(jfc.getCurrentDirectory());
		return result;
	}


}
