/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class CustomizableToolbarButton extends JButton
{
	public static final String TOOLBAR_ICON = CustomizableToolbar.class.toString() + " Icon";
	public static final String TOOLBAR_TEXT = CustomizableToolbar.class.toString() + " Text";

	public CustomizableToolbarButton(Action a) {
		setAction(a);
	}

	String text = null;
	Icon icon = null;

	public void setText(String text) {
		super.setText(text);
		this.text = text;
	}
	public void setIcon(Icon icon) {
		super.setIcon(icon);
		this.icon = icon;
	}

	public void setShowText(boolean b) {
		super.setText(b ? text : null);
	}

	public void setShowIcon(boolean b) {
		super.setIcon(b ? icon : null);
	}

	public void setAction(Action a) {
		super.setAction(a);
		ImageIcon i = (ImageIcon)a.getValue(TOOLBAR_ICON);
		if (i != null) setIcon(i);
		String t = (String)a.getValue(TOOLBAR_TEXT);
		if (t != null) setText(t);
	}
}
