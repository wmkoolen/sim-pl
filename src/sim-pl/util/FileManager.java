/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

//import java.io.*;
//import java.awt.*;
//import javax.swing.*;

public class FileManager
{
	private static FileManager singleton;
	private FileManager() {}

	public static FileManager getFileManager() {
		if (singleton == null) singleton = new FileManager();
		return singleton;
	}

	private JFileChooser    chooser =        new JFileChooser(new File(System.getProperty("user.dir")));

	public static final ExtensionAddingFilter filter_jpeg =   new ExtensionAddingFilter("Joint Pictures Expert Group", ".jpeg");
	public static final ExtensionAddingFilter filter_xml =    new ExtensionAddingFilter("eXtensible Markup Language", ".xml");
	public static final ExtensionAddingFilter filter_sim_pl = new ExtensionAddingFilter("SIM-PL Component", ".sim-pl");
	public static final ExtensionAddingFilter filter_worksheet = new ExtensionAddingFilter("SIM-PL Worksheet", ".sim-pl-ws");
	public static final ExtensionAddingFilter filter_txt =    new ExtensionAddingFilter("Text file", ".txt");
	public static final PseudoFileFilter      filter_anycmp = new PseudoFileFilter("All component files", filter_sim_pl, filter_xml);

	public void setCurrentDirectory(File directory)
	{
		chooser.setCurrentDirectory(directory);
	}

	public File askForFile(Component parent, String Title, File currentSelection, String approveButtonText, FileFilter ... filters)
	{
		assert filters.length > 0;
		chooser.setDialogTitle(Title);
		chooser.cancelSelection();
		chooser.setSelectedFile(currentSelection);
		chooser.resetChoosableFileFilters();
		for (FileFilter filter : filters) {
			chooser.addChoosableFileFilter(filter);
		}
		chooser.setFileFilter(filters[0]);
		if (CurrentDirectoryManager.wrap(chooser, parent, approveButtonText) == JFileChooser.APPROVE_OPTION )
		{
			File f = chooser.getSelectedFile();
			FileFilter filter = chooser.getFileFilter();
			if (filter instanceof ExtensionAddingFilter) f = ((ExtensionAddingFilter)filter).correctExtenstion(f);
			return  f;
		}
		else return null;
	}


	public static File getFile(File parent, String path) {
		File f = new File(path);
		if (f.isAbsolute() || parent == null) return f;
		return new File(parent, path);
	}
}
