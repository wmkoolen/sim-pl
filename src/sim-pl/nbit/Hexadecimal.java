/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

public class Hexadecimal extends NumberFormat
{
	private static final String prefix = "0x";

    public Hexadecimal()                {}
	public boolean equals(Object o)     { return o instanceof Hexadecimal;}
	public String toString()            { return "Hexadecimal"; }

	public String unPrefix(String prefixedNumberStr)
	{
		if (prefixedNumberStr.startsWith(prefix))   return prefixedNumberStr.substring(prefix.length());
		else                                        return null;
	}

	public nBit parsePrefixfree(String prefixfreeNumberStr, Integer bits) throws ParseException
	{
		return parse_Hex(prefixfreeNumberStr, bits);
	}

	public boolean isSignedPrefixfree(String prefixfreeNumberStr)
	{
		return false;
	}

	public NumberFormat tryParse(String numberFormat)
	{
		if (numberFormat.equals("Hexadecimal") ||
		    numberFormat.equals("Hex") ||
			numberFormat.equals("HEX") ||
			numberFormat.equals("H")) return this;
		else return null;
	}

	protected nBit parse_Hex(String s, Integer bits) throws ParseException
	{
		try
		{
		    nBit v = new nBit(bits != null ? bits : 4 * s.length());

		    for (int i = 0; i < s.length(); i++)
		    {
			    int value;
			    char c = s.charAt(s.length()-1-i);

		        switch(c)
			    {
				    case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
				    value = c - '0'; break;

				    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
				    value = c - 'a' + 10; break;

				    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
				    value = c - 'A' + 10; break;

	    		    default:	throw new ParseException(this, c, s);
			    }

			    //Elements[i / 8] = (Elements[i / 8] & (~(0xF << ((i * 4 ) % 32)))) | value << ((i * 4 ) % 32);
			    v.elements[ i >>> 3] &= ~(0xF << ((i << 2 ) & 31));
			    v.elements[ i >>> 3] |= value << ((i << 2 ) & 31);
				int valueBits = Integer.numberOfTrailingZeros(Integer.highestOneBit(value))+1;
				if (value != 0 && 4*i+valueBits > v.bits) throw new ParseException(s, v.bits);
		    }
		    v.Truncate();
		    return v;
	    }
		catch (ZeroBitException ex)
		{
			throw new ParseException(this, s);
		}
	}

	public String toFormattedString(nBit nB, boolean signed, boolean prefixed)
	{
		StringBuffer s = new StringBuffer();
		if (prefixed) s.append(prefix);

	    for(int i = (nB.bits+3)/4-1; i >= 0; i--)
		{
			s.append(DIGITS[((nB.elements[i >>> 3] >>> ((i&7)<<2)) & 15)]);
		}

		return s.toString();
	}
}
