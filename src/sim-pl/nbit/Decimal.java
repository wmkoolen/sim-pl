/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

import util.ExceptionHandler;

public class Decimal extends NumberFormat
{
	private static final String prefix = "0d";


    public Decimal()                    {}
	public boolean equals(Object o)     { return o instanceof Decimal;}
	public String toString()            { return "Decimal"; }

	public String unPrefix(String prefixedNumberStr)
	{
		if (prefixedNumberStr.startsWith(prefix))   return prefixedNumberStr.substring(prefix.length());
		else                                        return null;
	}

	public nBit parsePrefixfree(String prefixfreeNumberStr, Integer bits) throws ParseException
	{
		if (prefixfreeNumberStr.startsWith("-"))    return parse_Dec(prefixfreeNumberStr.substring(1),bits).NEG();
		else                                        return parse_Dec(prefixfreeNumberStr,bits);
	}

	public boolean isSignedPrefixfree(String prefixfreeNumberStr)
	{
		return prefixfreeNumberStr.startsWith("-");
	}


	public NumberFormat tryParse(String numberFormat)
	{
		if (numberFormat.equals("Decimal") ||
		    numberFormat.equals("Dec") ||
			numberFormat.equals("DEC") ||
			numberFormat.equals("D")) return this;
		else return null;
	}


	protected nBit parse_Dec(String s, Integer bits) throws ParseException
	{
		try
		{
			long val = Long.parseLong(s);
			int bits_used = val == 0 ? 0 : Long.numberOfTrailingZeros(Long.highestOneBit(val))+1;
		    nBit v = new nBit(bits != null ? bits : Math.max(32,bits_used), val);
			if (bits_used > v.bits) throw new ParseException(s, v.bits);

//	        v.elements[0] = 0;
//
//	        for (int i = 0; i < s.length(); i++)
//	        {
//		        char c = s.charAt(i);
//		        if (c < '0' || c > '9') throw new ParseException(this, c);
//		        v.elements[0] *= 10;
//		        v.elements[0] += c - '0';
//	        }
//
		    return v;
	    }
		catch (ZeroBitException ex)
		{
			throw new ParseException(this, s);
		}
	}

	public String toFormattedString(nBit nB, boolean signed, boolean prefixed)
	{
		StringBuffer s = new StringBuffer();
		if (prefixed) s.append(prefix);

		nBit toPrint = nB;
		if (signed && nB.getBit(nB.bits-1)) // check highest bit
		{
			toPrint = toPrint.NEG();
			s.append("-");
		}

		if (toPrint.toBoolean())    printDec(toPrint, s);       // Er is minstens 1 bit 1, dus getal > 0
		else                        s.append("0");		        // getal = 0. PrintDec zal in dat geval geen 0 toevoegen

		return s.toString();
	}

	void printDec(nBit toPrint, StringBuffer s)
	{
		try
		{
	        if (toPrint.toBoolean()) // cheap != 0 test
		    {
				nBit [] DIV_REM = toPrint.Division(nBit.ten);
			    printDec(DIV_REM[0], s);
	            s.append(DIGITS[DIV_REM[1].elements[0]]);
		    }
		}
		catch (nbit.DivisionByZeroException ex) // We delen niet door 0, dus dit gebeurt nooit!
		{
			throw new ExceptionHandler(ex);
		}
	}


}
