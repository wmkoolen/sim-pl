/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

public class Binary extends NumberFormat
{
	private static final String prefix = "0b";

    public Binary()                     {}
	public boolean equals(Object o)     { return o instanceof Binary;}
	public String toString()            { return "Binary"; }


	public String unPrefix(String prefixedNumberStr)
	{
		if (prefixedNumberStr.startsWith(prefix))   return prefixedNumberStr.substring(prefix.length());
		else                                        return null;
	}

	public  nBit parsePrefixfree(String prefixfreeNumberStr, Integer bits) throws ParseException
	{
		try
		{
		    nBit v = new nBit(bits != null ? bits : prefixfreeNumberStr.length());

			for (int i = 0; i < prefixfreeNumberStr.length(); i++)
	    	{
		        int value;
		        char c = prefixfreeNumberStr.charAt(prefixfreeNumberStr.length()-1-i);

			    switch(c)
			    {
				    case '0': case '1':		value = c - '0'; break;
				    default:				throw new ParseException(this, c, prefixfreeNumberStr);
			    }

			    //Elements[i / 32] = (Elements[i / 32] & (~(0xF << (i % 32)))) | value << (i % 32);
			    v.elements[i >>> 5] &= ~(  1 << (i & 31));
			    v.elements[i >>> 5] |= value << (i & 31);

				if (value != 0 && i > v.bits) throw new ParseException(prefixfreeNumberStr, v.bits);
		    }

		    v.Truncate();
		    return v;
		}
		catch (ZeroBitException ex)
		{
			throw new ParseException(this, prefixfreeNumberStr);
		}
	}

	public boolean isSignedPrefixfree(String prefixfreeNumberStr)
	{
		return false;
	}

	public NumberFormat tryParse(String numberFormat)
	{
		if (numberFormat.equals("Binary") ||
		    numberFormat.equals("Bin") ||
			numberFormat.equals("BIN") ||
			numberFormat.equals("B")) return this;
		else return null;
	}


	public String toFormattedString(nBit nB, boolean signed, boolean prefixed)
	{
		StringBuffer s = new StringBuffer();
		if (prefixed) s.append(prefix);

	    for(int i = nB.bits-1; i >= 0; i--)
		{
		    s.append(DIGITS[((nB.elements[i >>> 5] >>> (i&31)) & 1)]);
		}

		return s.toString();
	}

}
