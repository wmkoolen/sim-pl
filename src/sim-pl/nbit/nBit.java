/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

import util.ExceptionHandler;

import compiler.wisent.Type_Simple;


public class nBit implements Comparable<nBit>
{
//	public static final NumberFormat PREFIXFREE =    NumberFormat.BINARY;
	public static final NumberFormat PREFIXFREE =    NumberFormat.HEXADECIMAL;

	protected   final int []	    elements;
	public      final int			bits;

	public nBit(int bits) throws ZeroBitException
	{
	    if (bits <= 0) throw new ZeroBitException(bits);
		this.bits = bits;
	    elements = new int[(bits+31)/32];
	    for (int i = 0; i < elements.length; i++) elements[i] = 0;
	}

	public nBit(Type_Simple type)
	{
		try
		{
		    if (type.bits <= 0) throw new ZeroBitException(type.bits);
		    this.bits = type.bits;
	        elements = new int[(bits+31)/32];
	        for (int i = 0; i < elements.length; i++) elements[i] = 0;
		}
		catch (ZeroBitException ex)
		{
			throw new ExceptionHandler(ex);
		}
	}

	public nBit(int bits, boolean number) throws ZeroBitException
	{
		this(bits, number ? 1 : 0);
	}

	public nBit(int bits, int number) throws ZeroBitException
	{
	    if (bits <= 0) throw new ZeroBitException(bits);
		this.bits = bits;
	    elements = new int[(bits+31)/32];
	    elements[0] = number;

	    for (int i = 1; i < elements.length; i++) {
		    elements[i] = 0;
	    }

	    Truncate();
	}

	public nBit(int bits, long number) throws ZeroBitException
	{
		if (bits <= 0) throw new ZeroBitException(bits);
		this.bits = bits;
		elements = new int[(bits+31)/32];
		elements[0] = (int) number;
		if (elements.length > 1) elements[1] = (int)(number >>> 32);

		for (int i = 2; i < elements.length; i++) {
			elements[i] = 0;
		}

		Truncate();
	}



	public nBit(nBit nB)
	{
	    bits	  = nB.bits;
	    elements  = new int[nB.elements.length];
	    for (int i = 0; i < elements.length; i++)
	    {
		    elements[i] = nB.elements[i];
	    }
	}



	public static int bitsUsed(int value)
	{
		for (int i = 31; i >= 0; i--)
		{
			if ( ((value >>> i) & 1) != 0) return i+1;
		}
		return 1; // ( we currently need to use at least one bit)
	}


	public static nBit parse_nBit(String s) throws ParseException
	{
		return NumberFormat.parse_nBit(s, PREFIXFREE);
	}

	// This procedure only checks POSSIBLE representations. It does not perform
	// further validity checks

	public static NumberFormat getRepresentation(String s)
	{
		return NumberFormat.getRepresentation(s, PREFIXFREE);
	}

	// This procedure only checks POSSIBLE signedness. It does not perform
	// further validity checks

	public static boolean isSigned(String s)
	{
		return NumberFormat.isSigned(s, PREFIXFREE);
	}

// Print functions     -------------------------------------------------------------------

	public String toString()
	{
		return toString(PREFIXFREE, false);
	}

	public String toString(NumberFormat format, boolean signed)
	{
		return format.toFormattedString(this, signed, !format.equals(PREFIXFREE));
	}

	public String toString(NumberFormat format, boolean signed, boolean prefixed)
	{
		return format.toFormattedString(this, signed, prefixed);
	}




	public int getBits()
	{
	    return bits;
	}

	public nBit createResult()
	{
		try
		{
			return new nBit(bits);
		}
		catch (ZeroBitException ex)
		{
			throw new ExceptionHandler(ex);
		}
	}

	public nBit createResult(boolean answer)
	{
		try
		{
			return new nBit(bits, answer);
		}
		catch (ZeroBitException ex)
		{
			throw new ExceptionHandler(ex);
		}
	}





// Manipulation operators -------------------------------------------------------------------

	private static final int max = 0xFFFFFFFF;

	protected void Truncate()
	{
		int rest = (((elements.length)*32)-bits);
		assert rest < 32;
	    elements[elements.length-1] &= ((max << rest) >>> rest);
	}


	public nBit cast(int newBits) throws ZeroBitException
	{
		if (newBits == bits) return this;

	    nBit R = new nBit(newBits);

	    if (newBits > bits)
	    {
		    for (int i = 0; i < elements.length; i++) R.elements[i] = elements[i];
	    }
	    else
	    {
		    for (int i = 0; i < R.elements.length; i++) R.elements[i] = elements[i];
			R.Truncate();
	    }
		return R;
	}


	public nBit cast(Type_Simple type)
	{
		try
		{
			return this.cast(type.bits);
		}
		catch (ZeroBitException ex) // can not happen!!
		{
			throw new ExceptionHandler(ex);
		}
	}






// Bitwise operators -------------------------------------------------------------------


	public nBit NOT_BIT()
	{
	    nBit R  = createResult();

	    for (int i = 0; i < elements.length; i++)
	    {
		    R.elements[i] = ~elements[i];
	    }

	    R.Truncate();

	    return R;
	}



	public nBit AND_BIT (nBit nB)
	{
        nBit R = createResult();

        for (int i = 0; i < elements.length; i++)
        {
	        R.elements[i] = elements[i] & ((i < nB.elements.length) ? nB.elements[i] : 0);
        }

        // R.Truncate(); & cannot introduce extra 1's
        return R;
	}


	public nBit OR_BIT (nBit  nB)
	{
	    nBit R = createResult();

	    for (int i = 0; i < elements.length; i++)
	    {
		    R.elements[i] = elements[i] | ((i < nB.elements.length) ? nB.elements[i] : 0);
	    }

	    R.Truncate();

	    return R;
	}


	public nBit XOR_BIT (nBit  nB)
	{
	    nBit R = createResult();

	    for (int i = 0; i < elements.length; i++)
	    {
		    R.elements[i] = elements[i] ^ ((i < nB.elements.length) ? nB.elements[i] : 0);
	    }

	    R.Truncate();

	    return R;
	}


// Shift operators -------------------------------------------------------------------


	public nBit SHIFT_LEFT (nBit  nB)
	{
		// If nB is too big then result is zero.
	    for (int i = 1; i < nB.elements.length; i++) {
			if (nB.elements[i] > 0) return createResult();
		}
		// else, shift by an int number of bits
		return SHIFT_LEFT(nB.elements[0]);
	}

	public nBit SHIFT_LEFT (int bits)
	{
		if (bits == 0) return this; // fast-out, we're immutable

		nBit R = createResult();

		int sE = bits >>> 5;      //bits / 32;
		int sB = bits & 31;       //bits % 32;

		if (sB == 0) {
			// this needs to be a special case, for shifts in java are modulo num_bits (32 for int)
			for (int i = sE; i < elements.length; i++) {
				R.elements[i] = elements[i-sE];
			}

		} else {
			int i = sE;
			if (i < elements.length) {
				R.elements[i] = (elements[i-sE]   <<   sB);
			}
			for (i++; i < elements.length; i++) {
				R.elements[i] = (elements[i-sE]   <<   sB)
					          | (elements[i-sE-1] >>> (32 - sB));
			}

		}

		R.Truncate();

		//if (DEBUG) System.out.println(this+" << "+bits+" = "+R+" (sE: "+sE+" sB: "+sB+")");

		return R;
	}


	public nBit SHIFT_RIGHT (nBit nB)
	{
		// If nB is too big then result is zero.
	    for (int i = 1; i < nB.elements.length; i++) {
			if (nB.elements[i] > 0) return createResult();
		}
		// else, shift by an int number of bits
		return SHIFT_RIGHT(nB.elements[0]);
	}

	public nBit SHIFT_RIGHT (int bits)
	{
		if (bits == 0) return this; // fast-out, we're immutable

		nBit R = createResult();

		int sE = bits >>> 5;      //bit / 32;
		int sB = bits & 31;       //bit % 32;


		if (sB == 0) {
			// this needs to be a special case, for shifts in java are modulo num_bits (32 for int)
			for (int i = 0; i+sE < elements.length; i++) {
				R.elements[i] =  (elements[i+sE]   >>>     sB);
			}

		} else {
//			for (int i = 0; i < elements.length; i++) {
//				R.elements[i] = ((i+sE   < elements.length) ? (elements[i+sE]   >>>     sB ) : 0) |
//					            ((i+sE+1 < elements.length) ? (elements[i+sE+1] <<  (32-sB)) : 0);
//			}

			int i = 0;
			for (; i+sE+1 < elements.length; i++) {
				R.elements[i] = (elements[i+sE]   >>>     sB)
				              | (elements[i+sE+1] <<  (32-sB));
			}

			if (i+sE  < elements.length) {
				R.elements[i] = (elements[i+sE]   >>>     sB);
			}
		}

		R.Truncate();

		//if (DEBUG) System.out.println(this+" >> "+bits+" = "+R+" (sE: "+sE+" sB: "+sB+")");

		return R;
	}



// Conversion operators -------------------------------------------------------------------


	public boolean toBoolean()
	{
	    for (int i = 0; i < elements.length; i++)
	    {
		    if (elements[i] != 0) return true;
	    }

	    return false;
	}


	public int toInt() throws IntConversionException
	{
	    for (int i = 1; i < elements.length; i++)
	    {
		    if (elements[i] != 0 ) throw new IntConversionException(this);
	    }
	    return elements[0];
	}




// Logical operators -------------------------------------------------------------------


	public nBit NOT_BIN ()
	{
		return createResult(!toBoolean());
	}


	public nBit AND_BIN (nBit  nB)
	{
	    return createResult(toBoolean() && nB.toBoolean());
	}


	public nBit OR_BIN (nBit  nB)
	{
	    return createResult(toBoolean() || nB.toBoolean());
	}


// Comparison operators -------------------------------------------------------------------

	public int compareTo(nBit nB)
	{
	    int i;

	    if (elements.length > nB.elements.length)
	    {
		    for (i = elements.length-1; i >= nB.elements.length; i--)
		    {
			    if (elements[i] != 0) return 1;
		    }
	    }
	    else
	    {
		    for (i = nB.elements.length-1; i >= elements.length; i--)
		    {
			    if (nB.elements[i] != 0) return -1;
		    }
	    }

	    for (; i >= 0; i--)
	    {
			// Get rid of signed shit (first compare high 31 bits)
			int hb = (elements[i] >>> 1) - (nB.elements[i] >>> 1);
			if (hb != 0) return hb;

			// Second compare lowest 1 bit
			int lb = (elements[i] & 0x1) - (nB.elements[i] & 0x1);
			if (lb != 0) return lb;
	    }
		return 0;
	}

	public boolean equals(Object o) { return equals((nBit)o); }
	public boolean equals(nBit   nB)
	{
		return compareTo(nB) == 0;
	}


	public nBit EQUALS (nBit nB)        { return this.createResult(compareTo(nB) == 0); }
	public nBit NEQUALS (nBit  nB)      { return createResult(compareTo(nB) != 0);      }
	public nBit SMALLER (nBit  nB)      { return createResult(compareTo(nB) < 0);       }
	public nBit SMALLER_EQUAL (nBit  nB){ return createResult(compareTo(nB) <= 0);      }
	public nBit LARGER ( nBit  nB)      { return createResult(compareTo(nB) > 0);       }
	public nBit LARGER_EQUAL (nBit  nB) { return createResult(compareTo(nB) >= 0);      }



// Arithmetic operators -------------------------------------------------------------------

	public nBit ADD (nBit  nB)
	{
	    nBit R = createResult();
		int carry = 0;
		int i;

	    if (elements.length > nB.elements.length)
	    {
		    for (i = 0; i < nB.elements.length; i++)
		    {
			    long sum = (elements[i] & 0xffffffffL) + (nB.elements[i] & 0xffffffffL) + carry;
			    R.elements[i] = (int)sum;
			    carry = (int)(sum >> 32);
		    }

			for (; i < elements.length; i++)
	    	{
		    	R.elements[i] = elements[i] + carry;
			    carry = (R.elements[i] != elements[i] && R.elements[i] == 0) ? 1 : 0;
		    }
		}
	    else
	    {
		    for (i = 0; i < elements.length; i++)
		    {
			    long sum = (elements[i] & 0xffffffffL) + (nB.elements[i] & 0xffffffffL) + carry;
			    R.elements[i] = (int)sum;
			    carry = (int)(sum >> 32);
		    }
	    }

	    R.Truncate();
	    return R;
	}


	public static final nBit one = nBitConstant(1,1);
	public static final nBit ten = nBitConstant(4,10);


	public static nBit allOnes(int bits) throws ZeroBitException
	{
	    return new nBit(bits).NOT_BIT();
	}

	public nBit allOnes()
	{
		try
		{
	        return allOnes(bits);
		}
		catch (Exception ex)
		{
			throw new ExceptionHandler(ex);
		}
	}

	public boolean getBit(int bit)
	{
		int w = bit >>> 5;      //bit / 32;
		int o = bit & 31;       //bit % 32;

		return (elements[w] & (1 << o)) != 0;
	}


	public void setBit(int bit, boolean value)
	{
		int w = bit >>> 5;      //bit / 32;
		int o = bit & 31;       //bit % 32;

		if (value)  elements[w] |=  (1 << o);
		else        elements[w] &= ~(1 << o);
	}


	private static nBit nBitConstant(int bits, int value)
	{
		try
		{
			return new nBit(bits,value);
		}
		catch (Exception ex)
		{
			throw new ExceptionHandler(ex);
		}

	}


	public nBit NEG()
	{
	    nBit R = ((NOT_BIT())).ADD(one);
	    if (DEBUG) System.out.println("neg " + this + " = " + R);
	    return R;
	}


	public nBit SUB (nBit  nB)
	{
		try
		{
	        return ADD(nB.cast(bits).NEG());
		}
		catch (ZeroBitException ex)
		{
			throw new ExceptionHandler(ex);
		}
	}


	public nBit MUL (nBit  nB)
	{
	    return Multiplication(nB);
	}


	public nBit DIV (nBit  nB) throws DivisionByZeroException
	{
	    return Division(nB)[0];
	}


	public nBit REM (nBit  nB) throws DivisionByZeroException
	{
		return Division(nB)[1];
	}


	/* DIT MOET NOG VERBETERD WORDEN !!!! */
	public nBit REMS(nBit  nB) throws DivisionByZeroException
	{
		return REM(nB);
	}


	public nBit MULS(nBit nB)
	{
	    boolean neg1 = ((elements[ (   bits-1) / 32]) >>> ((   bits-1) % 32)) != 0;
	    boolean neg2 = ((elements[ (nB.bits-1) / 32]) >>> ((nB.bits-1) % 32)) != 0;

	    nBit Arg1 = new nBit(neg1 ? this.NEG() : this);
	    nBit Arg2 = new nBit(neg2 ? nB.NEG()   : nB   );

		nBit R = Arg1.Multiplication(Arg2);

	    if (neg1 ^ neg2) R = R.NEG();

	    return R;
	}


	public nBit DIVS(nBit nB) throws DivisionByZeroException
	{
	    boolean neg1 = ((elements[ (   bits-1) / 32]) >>> ((   bits-1) % 32)) != 0;
	    boolean neg2 = ((elements[ (nB.bits-1) / 32]) >>> ((nB.bits-1) % 32)) != 0;

	    nBit Arg1 = new nBit(neg1 ? this.NEG() : this);
	    nBit Arg2 = new nBit(neg2 ? nB.NEG()   : nB   );

	    nBit R = Arg1.Division(Arg2)[0];

	    if (neg1 ^ neg2) R = R.NEG();

	    return R;

	}



// Assignment operators -------------------------------------------------------------------


	public nBit ASSIGN (nBit  nB)
	{
		int i;

	    if (elements.length > nB.elements.length)
	    {
		    for (i = 0; i < nB.elements.length; i++)	elements[i] = nB.elements[i];
		    for (; i < elements.length; i++)			elements[i] = 0;
	    }
	    else
	    {
		    for (i = 0; i < elements.length; i++)		elements[i] = nB.elements[i];
		    Truncate();
	    }
		return this;
	}


	public nBit PREINCREMENT()
	{
	    elements[0]++;
	    for (int i = 1; (i < elements.length) && (elements[i-1] == 0); i++)
		    elements[i]++;

	    Truncate();
		return this;
	}


	public nBit PREDECREMENT ()
	{
	    elements[0]--;
	    for (int i = 1; (i < elements.length) && (elements[i-1] == max); i++)
		    elements[i]--;

	    Truncate();
		return this;
	}


	public nBit POSTINCREMENT ()
	{
	    nBit R = new nBit(this);
		PREINCREMENT();
	    return R;
	}

	public nBit POSTDECREMENT ()
	{
	    nBit R = new nBit(this);
		PREDECREMENT();
		return R;
	}





	public nBit Multiplication(nBit nB)
	{
	    nBit R = createResult();
	    nBit nL = this;
	    nBit nR = nB;

	    for (int i = 0; i < bits; i++)
	    {
		    if ((nR.elements[0] & 1) != 0)
		    {
			    R = R.ADD(nL);
		    }
		    nR = nR.SHIFT_RIGHT(one);
		    nL = nL.SHIFT_LEFT(one);
	    }
	    return R;
	}


	private static final boolean DEBUG = false;
	
	public nBit [] Division(nBit nB) throws DivisionByZeroException
	{
		try
		{
		    if (!nB.toBoolean()) throw new DivisionByZeroException();

			if (DEBUG) System.out.println("\ndividing " + this + " by " + nB);

	        nBit P = this.cast(2 * bits + 1);
	        nBit D = nB.cast(2 * bits + 1).SHIFT_LEFT(bits);
	        nBit quot = new nBit(bits);
	        
	        for (int i = bits - 1; i >= 0; i--) {
	            nBit t = P.SHIFT_LEFT(1).SUB(D);
	            if (DEBUG) System.out.println("P " + P + " D " + D + " t " + t + " " + t.getBit(2 * bits));
	            if (!t.getBit(2 * bits)) {
	                P = t;
	                quot.setBit(i, true);
	            }
	            else {
	                P = P.SHIFT_LEFT(1);
	            }
	        }
	        
	        P = P.SHIFT_RIGHT(bits).cast(bits);
	        
	        if (DEBUG) System.out.println("quot= " + quot + " rem= " + P);

	        return new nBit[] { quot, P };
	    }
		catch (ZeroBitException ex)
	    {
		    throw new ExceptionHandler(ex);
	    }
	}
}
