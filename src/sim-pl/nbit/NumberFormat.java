/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

public abstract class NumberFormat
{
	public      static final NumberFormat   BINARY =        new Binary();
	public      static final NumberFormat   DECIMAL =       new Decimal();
	public      static final NumberFormat   HEXADECIMAL =   new Hexadecimal();
	protected   static final char []        DIGITS =        {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	protected static final NumberFormat [] formats = {BINARY, DECIMAL, HEXADECIMAL};

    NumberFormat() {}

	public abstract String          unPrefix            (String prefixedNumberStr);
	public abstract boolean         isSignedPrefixfree  (String prefixfreeNumberStr);
	public abstract nBit            parsePrefixfree     (String prefixfreeNumberStr, Integer bits) throws ParseException;
	public abstract String          toFormattedString   (nBit nB, boolean signed, boolean prefixed);
	public abstract NumberFormat    tryParse            (String numberFormat);



	/** Tries to parse an nBit number.
	 * First tries all known prefixed formats. If none of them work, then the
	 * preferred unprefixed format is used.
	 *
	 * @param s String
	 * @param prefixfree NumberFormat
	 * @return nBit
	 * @throws ParseException
	 */
	public static nBit parse_nBit(String s, NumberFormat prefixfree) throws ParseException {
		return parse_nBit(s, prefixfree, null);
	}

	public static nBit parse_nBit(String s, NumberFormat prefixfree, Integer bits) throws ParseException
	{
		for (int i = 0; i < formats.length; i++)
		{
		    String result = formats[i].unPrefix(s);
		    if (result != null) return formats[i].parsePrefixfree(result, bits);
		}

		return prefixfree.parsePrefixfree(s, bits);
	}


	public static NumberFormat valueOf(String s) {
		try {
			return parse(s);
		} catch (Exception ex) {
			return null;
		}
	}


	public static NumberFormat getRepresentation(String s, NumberFormat prefixfree)
	{
		for (int i = 0; i < formats.length; i++)
		{
			if (formats[i].unPrefix(s) != null) return formats[i];
		}

		return prefixfree;
	}

	public static boolean isSigned(String s, NumberFormat prefixfree)
	{
		for (int i = 0; i < formats.length; i++)
		{
		    String result = formats[i].unPrefix(s);
		    if (result != null) return formats[i].isSignedPrefixfree(result);
		}

		return prefixfree.isSignedPrefixfree(s);
	}


	public static NumberFormat parse(String numberFormat) throws Exception
	{
		for (int i = 0; i < formats.length; i++)
		{
			NumberFormat result = formats[i].tryParse(numberFormat);
			if (result != null) return result;
		}
		throw new Exception("Wrong number format: " + numberFormat);
	}

	public static NumberFormat [] getAvailableFormats() { return formats; }
}
