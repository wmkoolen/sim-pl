/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.util.HashMap;

public class SymbolTable<V>  {
	final HashMap<String, V> n2v = new HashMap<String, V>();

    public void put(String name, V value) throws SymbolRedefinedException {
		if (n2v.containsKey(name)) throw new SymbolRedefinedException(name);
		n2v.put(name, value);
    }

	public V get(String name) {
		return n2v.get(name);
	}

    public V getExists(String name) throws UndefinedSymbolException {
		V r = n2v.get(name);
		if (r==null) throw new UndefinedSymbolException(name, null);
		return r;
    }
}




class SymbolRedefinedException extends WASMException {
    public SymbolRedefinedException(String symbol) {
		super("Symbol redefined: " + symbol);
    }
}

class UndefinedSymbolException extends WASMException {
    public UndefinedSymbolException(String symbol, Context context) {
		this("symbol", symbol, context);
    }

    public UndefinedSymbolException(String symbolKind, String symbol, Context context) {
		super("Undefined " + symbolKind + ": " + symbol +
			  (context != null ? " in " + context : ""));
    }
}



