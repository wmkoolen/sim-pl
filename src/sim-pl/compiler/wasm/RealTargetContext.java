/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.io.File;
import java.util.ArrayList;

import nbit.nBit;

import compiler.ComponentCompiler;
import compiler.wisent.DataSource_Direct;
import compiler.wisent.Instance_Array;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Type_Array;
import compiler.wisent.Type_Simple;

import databinding.HPL;
import executer.Instance;
import executer.Simulator;
import executer.timetable.LocatedTextRun;


class RealTargetContext extends Context{
//	Vector  content = new Vector();

	int offset = 0; // position of the next item (during compilation)

	Type_Array     type;
	Type_Simple    elemType;
	Instance_Array content;
	boolean []     initialized;
	LocatedTextRun [] ltr;
	ArrayList<String> [] names;        // names of addresses (variables, loop labels)
	Simulator.Instance_Simple i;
	HPL targetMemory;

//	static final String TARGET_COMPONENT = "targetComponent";
	static final String TARGET_MEMORY    = "targetMemory";

	public RealTargetContext(String name) {
		super(name);
	}

	public void execute() throws Exception {
		i.getNamespace().reset(targetMemory.part, new DataSource_Direct(content.klone()));
	}

	public void resolveComponent(Instance ifor) throws WASMException {
//		Object oTargetComponent = resolveExists(TARGET_COMPONENT, SettingResolver.R);
//		if (!(oTargetComponent instanceof String)) throw new SemanticException("Setting " + TARGET_COMPONENT + " needs to be a string");
//		String targetComponent = (String) oTargetComponent;

		String oTargetMemory = resolveExists(TARGET_MEMORY, SettingResolver.R);
//		if (! (oTargetMemory instanceof String)) throw new WASMException("Setting " + TARGET_MEMORY + " needs to be a string");


		try {
//			// TODO check for simple instances here
//			Instance_Complex icomp = (Instance_Complex)ifor;
//
//			Instance itarget = icomp.getSubComponentInstance(targetComponent);
			targetMemory = new HPL(oTargetMemory);
			if (targetMemory.part == null) throw new Exception("Invalid memory location string. No memory part in " + targetMemory);

			i = (Simulator.Instance_Simple) ifor.resolve(targetMemory.stripPart());

			Object x = i.getNamespace().get(targetMemory.part);

			if (!(x instanceof DataSource_Direct)) throw new Exception(TARGET_MEMORY + " " +targetMemory + " needs to be a memory");
			DataSource_Direct ds = (DataSource_Direct)x;

			type = (Type_Array)ds.getValue().getType();
			elemType = (Type_Simple)type.type;
			content = (Instance_Array)type.createInstance();
			content.setToAllOnes();
			initialized = new boolean[content.size()];
			ltr = new LocatedTextRun[content.size()];
			names = new ArrayList [content.size()];

		}	catch (Exception ex) { throw new WASMException(ex); }
	}

	public void setValue(int p, nBit value, WAST ast) throws WASMException {
		assert p >= 0 : "negative offset " + p;
		if (p >= content.size()) throw new WASMException("Offset " + p + " out of range, memory size is " + content.size());

		if (initialized[p]) throw new OffsetDoubleInitializedException(p);
		if (value.bits != getBits()) new WASMException(
			  ast.getLocation() + "\t" +
			  "Trying to put a " + value.bits + " bit value into " + elemType.bits + " bit memory " + targetMemory);
		Instance_Simple is = (Instance_Simple)content.getElement(p);
		is.setValue(value);
		initialized[p] = true;

//
//	public void setLocation(int p, WAST ast) throws WASMException {

		if (ltr == null) ltr = new LocatedTextRun [content.size()];

		WAST cs = (WAST)ast.getFirstChild();
		while (cs.getNextSibling() != null) cs = (WAST)cs.getNextSibling();

		File sourceFile = ast.getFilename().equals(ComponentCompiler.INPUT_TEXT_NAME)
			? null
			: new File(ast.getFilename());


		ltr[p] = new LocatedTextRun(
			sourceFile,
			ast.getLine(),
			ast.getCol(),
			cs.getLine(),
			cs.getCol() + cs.getLength()
			);
	}


	public void setName(int p, String name) {
		ArrayList<String> nl = names[p];
		if (nl == null) {
			names[p] = nl = new ArrayList<String>();
		}
		nl.add(name);
	}

	public int getBits() {
		return elemType.bits;
	}
}


class OffsetDoubleInitializedException extends WASMException {
	public OffsetDoubleInitializedException(int offset) {
		super("Offset doubly initialized: " + offset);
	}

}
