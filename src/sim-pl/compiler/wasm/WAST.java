/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class WAST extends CommonAST {
	int line;
	int col;
	int length;
	public String filename;

	public WAST() {
		super();
	}

	public WAST(Token tok) {
		super(tok);
	}

	public int getLine()          { return line; }
	public int getCol()           { return col; }
	public int getLength()        { return length; }
	public String getFilename()   { return filename; }

	public void initialize(AST t) {
		super.initialize(t);
//		System.out.println("initialize(AST " + t + ")");
		WAST w = (WAST)t;
		copyLocation(w);
//		System.out.println("Result is " + this);
	}

	public void copyLocation(WAST w) {
//		System.out.println("copyLocation " + w + ")");
		line = w.line;
		col  = w.col;
		length = w.length;
		filename = w.filename;
	}

	public void initialize(int i, String s) {
		super.initialize(i,s);
//		System.out.println("initialize(int " + i + ", String " + s + ")");
//		System.out.println("Result is " + this);
	}

	public void initialize(Token t) {
		super.initialize(t);
//		System.out.println("initialize(Token " + t +")");
		line = t.getLine();
		col  = t.getColumn();
		length = t.getText().length();
		filename = t.getFilename();
//		System.out.println("Result is " + this);
	}


	public String getLocation() {
		return filename + " " + line + ":" + col;
	}



//	public String toString() {
//		return super.toString() + " " + getLocation();
//	}
}
