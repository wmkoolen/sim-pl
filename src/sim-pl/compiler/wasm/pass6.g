/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
    import compiler.wasm.Context.ConstantResolver;
    import compiler.wasm.Context.SettingResolver;
    import compiler.wasm.Context.AutocastResolver;
}

/* Pass 6
  * resolves (and removes) all
    - instructions

*/


class Pass6 extends PassBase;
{
    SymbolTable id2segment;

    public Pass6(SymbolTable id2segment) {
        this();
        this.id2segment = id2segment;
        this.setASTNodeClass(WAST.class.getName());
    }

    String passInfo = "Instructions";
}

program 
    :   (   processor 
        |   memory 
        |   code 
        |   data
        )* 
    ;


processor {
    String name;
    Processor p;
}
    :   #(".processor" 
            name=t:id 
            { p = (Processor)id2segment.getExists(name); }
            (deepcopy)*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

memory {
    String name;
    Memory m;
}
    :   #(".memory" 
            name=t:id 
            { m = (Memory)id2segment.getExists(name); }
            (deepcopy)*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

code {
    String name;
    AutocastResolver acr;
    Code c;
}
    :   #(".code" 
            name=t:id 
            {   c = (Code)id2segment.getExists(name); 
                c.offset = 0;
                acr = new AutocastResolver(c);
            }
            (   instruction[c, acr]!
            |   variabledecl[c]!
            |   offset_spec[c]!
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

data {
    String name;
    Data d;
}
    :   #(".data" 
            name=t:id 
            { d = (Data)id2segment.getExists(name); }
            (deepcopy)*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


instruction! [ Code c, AutocastResolver acr ] {
    String name;
    ValueType<nBit,String> vt;
    InstructionDefinition inde;

    ArrayList<ValueType<nBit,String>> args = new ArrayList<ValueType<nBit,String>>();

}
    :   #(INSTRUCTION 
            name=id    { inde = c.resolveExists(name, InstructionDefinitionResolver.R); }
            (   vt=valuetype[c] { args.add(vt);} )*
        )
        {   
            nBit [] results = inde.assemble(acr, c.elemType, c.offset, args);
            for (nBit result : results) {
                c.setValue(c.offset, result, ##_in);
                c.offset++;
            }
        }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }

valuetype [Context c] returns [ValueType<nBit,String> vt] {
    String type;
}
    :   vt=valuetype_atom[c] 
        (   SLASH type=id { vt = new ValueType<nBit, String>(vt.value, type); }
        )?
    ;


valuetype_atom [Context c] returns [ValueType<nBit,String> vt] {
    String v, type;
    nBit n;
}
    :   v=id                    { vt = c.resolveExists(v, ConstantResolver.R); }
    |   n=nbit                  { vt = new ValueType<nBit, String>(n, c.resolveExists(SettingResolver.NUMBER_TYPE, SettingResolver.R)); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }




offset_spec [ RealTargetContext rtc] {
    nBit n;
    String c;
}
    :   #(t:OFFSET_SPEC 
            (   n=nbit 
            |   c=id   {  n = rtc.resolveExists(c, ConstantResolver.R).value; }
            )
            {  rtc.offset = n.toInt(); }
        )    
    ;
    exception 
    catch [IntConversionException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }
    catch [UndefinedSymbolException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }



variabledecl [ RealTargetContext d] 
    :   #(WORD (deepcopy)*)
            // automatically advance to next position
        { d.offset++; }
    ;


