/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import log.Log;
import log.Logger;
import log.StreamLog;
import namespace.ComponentTable;

import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.AttributesImpl;

import util.FileSlurper;
import util.MyFormatter;
import databinding.Component;
import executer.Instance;
import executer.Simulator;


// with some help from
// http://www.javazoom.com/services/newsletter/xmlgeneration.html

public class InstructionSetFormatter {


	public static String [] style = {
//		"h1, h2, h3              { text-align: center; }" ,
		"div.segment             { text-align:center; font-size: x-large; font-weight: bold; }",
		"div.format              { text-align:center; font-size: large; font-weight: bold; padding-top: 1em;}",
		"table.instructionset    { overflow:visible; margin:auto; padding-bottom: 1em; }" ,
		"span.type               { font-style: italic; }" ,
		"span.mnemonic           { padding-right: 1em;}" ,
		"tr.fieldnames th        { border : 1px solid black; }" ,
		"tr.bitoffsets td.left   { border-left : 1px dotted gray; text-align: left;}" ,
		"tr.bitoffsets td.right  { border-right: 1px dotted gray; text-align: right}" ,
		"tr.bitoffsets td        { font-size: smaller; }" ,
		"tr.instructionfields td { border : 1px dotted gray; text-align: center; }" ,
		"tr.instructionfields th { text-align: left; font-weight: normal; padding-right: 2em; }" ,
	};

	public static String getStyleContent() {
		return MyFormatter.join("\n", Arrays.asList(style));
	}




	public static void main(String [] args) throws Exception {
		assert args.length == 2 || args.length == 3;

		File sourceFile = new File(args[0]);
		File componentFile = new File(args[1]);
		File outFile = (args.length == 3) ? new File(args[2]) : null;

		Log log = new StreamLog();
		Logger logger = new Logger(log);

		main(sourceFile, componentFile, outFile, logger);
	}



	public static void main(File sourceFile, File componentFile, File outFile, Logger logger) throws Exception {
		StreamResult streamResult = outFile != null
			? new StreamResult(new FileOutputStream(outFile))
			: new StreamResult(System.out);
		try {

			logger.addLine("Instruction Set Formatter");

			Instance  i;
			String source;
			try {
				logger.addDoing("Loading component file");
				Component c = ComponentTable.load(componentFile);
				logger.addSuccess();

				logger.addDoing("Starting simulator");
				Simulator s = new Simulator(c, componentFile);
				i = s.getInstance();
				logger.addSuccess();

				logger.addDoing("Loading WASM program");
				source = FileSlurper.slurp(sourceFile);
				logger.addSuccess();
			}
			catch (Exception ex) {
				throw logger.addFailure(ex);
			}

			WASM wasm = new WASM();
			SymbolTable<Context> id2segment = wasm.compilePreprocess(source, sourceFile.getParentFile(), i, logger, sourceFile.getName());
			toXML(i, id2segment, streamResult, logger);


			logger.addParagraph("OK");
		} finally {
			if (streamResult.getOutputStream() != null)
				streamResult.getOutputStream().close();
		}
	}

	public static void toXML(Instance i, SymbolTable<Context> id2segment, StreamResult streamResult, Logger log) throws Exception  {
		// PrintWriter from a Servlet
//		PrintWriter out = response.getWriter();

		SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
		// SAX2.0 ContentHandler.
		TransformerHandler hd = tf.newTransformerHandler();
		Transformer serializer = hd.getTransformer();
		serializer.setOutputProperty(OutputKeys.METHOD, "xml");
		serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
		serializer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd");
		serializer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"-//W3C//DTD XHTML 1.0 Strict//EN");


		serializer.setOutputProperty(OutputKeys.INDENT,"yes");
		hd.setResult(streamResult);

		hd.startDocument();
		AttributesImpl atts = new AttributesImpl();

		atts.addAttribute("","","xmlns", "CDATA", "http://www.w3.org/1999/xhtml");
		atts.addAttribute("","","xml:lang", "CDATA", "en");
		atts.addAttribute("","","lang", "CDATA", "en");

		hd.startElement("","","html",atts);

		atts.clear();
		hd.startElement("","","head",atts);
		hd.startElement("","","title",atts);
		elementText(hd, "Instruction Format of " + i.getFullName());
		hd.endElement("","","title");

		atts.addAttribute("","","type", "CDATA", "text/css");
		hd.startElement("","","style",atts);
		elementText(hd, getStyleContent());
		hd.endElement("","","style");

		hd.endElement("","","head");

		atts.clear();
		hd.startElement("", "", "body", atts);

		atts.clear();
		hd.startElement("", "", "h1", atts);
		elementText(hd, "Instruction Format of " + i.getFullName());
		hd.endElement("", "", "h1");

		toXMLBody(hd, i, id2segment, log);

		hd.endElement("", "", "body");
		hd.endElement("", "", "html");
		hd.endDocument();
	}

	public static void toXMLBody(ContentHandler hd, Instance i, SymbolTable<Context> id2segment, Logger log) throws Exception {
		AttributesImpl atts = new AttributesImpl();

		for (Context c : id2segment.n2v.values()) {
			if (c instanceof Processor) {
				toXMLProcessor(hd, atts, (Processor) c, log);
			}
		}
	}


	public static void toXMLProcessor(ContentHandler hd, AttributesImpl atts, Processor p, Logger log) throws Exception {

		// our first index is trough formats. We make a mapping of formats to lists of idefs

		Map<InstructionFormat, Set<InstructionDefinition>> f2idefs = new TreeMap<InstructionFormat, Set<InstructionDefinition>>(new Comparator<InstructionFormat>() {
			public int compare(InstructionFormat i1, InstructionFormat i2) {
				// short names always first
				if (i1.shortName != null) {
					return (i2.shortName != null)
						? i1.shortName.compareTo(i2.shortName)
						: -1;
				} else {
					return (i2.shortName != null)
						? 1
						: i1.getName().compareTo(i2.getName());
				}
			}
		});

		for (InstructionDefinition idef : p.instructiondefinitions.n2v.values()) {
			Set<InstructionDefinition> s = f2idefs.get(idef.format);
			if (s == null) {
				s = new TreeSet<InstructionDefinition>(new Comparator<InstructionDefinition>() {
					public int compare(InstructionDefinition i1, InstructionDefinition i2) {
						// short names always first
						if (i1.shortName != null) {
							return (i2.shortName != null)
								? i1.shortName.compareTo(i2.shortName)
								: -1;
						} else {
							return (i2.shortName != null)
								? 1
								: i1.getName().compareTo(i2.getName());
						}
					}
				});
				f2idefs.put(idef.format, s);
			}
			s.add(idef);
		}


	  atts.clear();
	  atts.addAttribute("","","class","CDATA", "segment");
	  hd.startElement("","","div",atts);
	  elementText(hd, "Processor: " + p.name);
	  hd.endElement("","","div");

	  for (InstructionFormat format : f2idefs.keySet()) {
		  { // title
			  atts.clear();
			  atts.addAttribute("","","class","CDATA", "format");
			  hd.startElement("", "", "div", atts);

			  atts.clear();
			  atts.addAttribute("", "", "class", "DATA", "mnemonic");
			  hd.startElement("", "", "span", atts);
			  elementText(hd, format.getName());
			  hd.endElement("", "", "span");

			  boolean first = true;
			  for (WideTypedField wtf : format.args) {
				  if (first) {
					  elementText(hd, " ");
					  first = false;
				  } else {
					  elementText(hd, ", ");
				  }

				  atts.clear();
				  atts.addAttribute("","","class", "DATA", "type");
				  hd.startElement("","","span", atts);
				  elementText(hd, wtf.type);
				  hd.endElement("", "", "span");
				  elementText(hd, " " + wtf.name);
			  }
			  hd.endElement("", "", "div");
		  }


		  {// body
		      atts.clear();
			  atts.addAttribute("","","class", "CDATA", "instructionset");
			  hd.startElement("", "", "table", atts);

			  int opcode_width = 0;
			  { // field names line
				  atts.clear();
				  atts.addAttribute("","","class", "CDATA", "fieldnames");
			      hd.startElement("", "", "tr", atts);

				  // skip first column
				  atts.clear();
				  hd.startElement("", "", "td", atts);
				  hd.endElement("", "", "td");

				  for (WordPartition wp : format.part.words)
				  for (WideField wf : wp.fields) {
					  opcode_width += wf.width.toInt();
					  atts.clear();
					  atts.addAttribute("","","style", "CDATA", "width: " + (wf.width.toInt()*1.5) + "em;");
					  atts.addAttribute("","","colspan", "CDATA", "2");
					  hd.startElement("", "", "th", atts);
					  elementText(hd, wf.name);
					  { // field width
						  atts.clear();
						  hd.startElement("", "", "sub", atts);
						  elementText(hd, Integer.toString(wf.width.toInt()));
						  hd.endElement("", "", "sub");
					  }
					  hd.endElement("", "", "th");
				  }


				  hd.endElement("", "", "tr");
			  }

			  { // bit offsets line
				  atts.clear();
				  atts.addAttribute("","","class", "CDATA", "bitoffsets");
				  hd.startElement("", "", "tr", atts);

				  // skip first column
				  atts.clear();
				  hd.startElement("", "", "th", atts);
				  hd.endElement("", "", "th");

				  for (WordPartition wp : format.part.words)
				  for (WideField wf : wp.fields) {
					  atts.clear();
					  atts.addAttribute("","","class", "CDATA", "left");
					  hd.startElement("", "", "td", atts);
					  elementText(hd, Integer.toString(opcode_width-1));
					  hd.endElement("", "", "td");
					  opcode_width -= wf.width.toInt();

					  atts.clear();
					  atts.addAttribute("","","class", "CDATA", "right");
					  hd.startElement("", "", "td", atts);
					  elementText(hd, Integer.toString(opcode_width));
					  hd.endElement("", "", "td");
				  }

				  hd.endElement("", "", "tr");
			  }
			  assert opcode_width == 0;

			  // content for instructions

			  Set<InstructionDefinition> instructions = f2idefs.get(format);

			  for (InstructionDefinition id : instructions) {

				  atts.clear();
				  atts.addAttribute("","","class", "CDATA", "instructionfields");
				  hd.startElement("", "", "tr", atts);

				  { // instruction line
					  atts.clear();
					  hd.startElement("", "", "th", atts);
					  elementText(hd, id.getName());
					  hd.endElement("", "", "th");

					  // pre-process the WSDL

					  Map f2code = new HashMap();
					  if (id.format.part.wsdl != null) preprocessWSDL(f2code, id.format.part.wsdl, log);
					  if (id.format     .wsdl != null) preprocessWSDL(f2code, id.format     .wsdl, log);
					  if (id            .wsdl != null) preprocessWSDL(f2code, id            .wsdl, log);


					  atts.addAttribute("","","colspan", "CDATA", "2");
					  for (WordPartition wp : format.part.words)
					  for (WideField wf : wp.fields) {
						  hd.startElement("", "", "td", atts);
						  String val = (String)f2code.get(wf.name);
						  if (val == null) log.addLine("Warning: uninitialised field " + wf.name + " for instruction " + id.getName());
						  elementText(hd, val == null ? "?" : val);
						  hd.endElement("", "", "td");
					  }
				  }
				  hd.endElement("", "", "tr");
			  }


			  hd.endElement("","","table");
		  }
	  }
	}

	public static void preprocessWSDL(Map f2code, String wsdl, Logger log) {
		String[] fields = wsdl.trim().split("\\s*;\\s*");

		for (int i = 0; i < fields.length; i++) {
			String[] ps = fields[i].split("\\s*=\\s*");
			assert ps.length == 2;
			if (f2code.containsKey(ps[0])) log.addLine("Warning: redefinition of field " + ps[0]);
			f2code.put(ps[0], ps[1]);
		}
	}


	// auxilary method
	public static void elementText(ContentHandler hd, String data) throws Exception {
		 char [] cs = data.toCharArray();
		 hd.characters(cs, 0, cs.length);
 }


}

