/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
}


class PassBase extends TreeParser;
options {
    defaultErrorHandler = false;
    importVocab=P;
    buildAST = true;
    ASTLabelType = WAST;
}


// deepcopy!
//     : t:. { ## = #t_in; }
//     ;

deepcopy 
    : #(. (deepcopy)*)
    ;


atom returns [String o = null]
    :   (   o=id
        |   o=string_literal
//        |   o=nbit
        )
    ;

id returns [String s = null]
    : t:ID { s = t.getText(); }
    ;


string_literal returns [String s = null]
    : t:STRING_LITERAL { s = t.getText(); }
    ;

nbit returns [nBit nB = null]
    :   n:NBIT
        // interpret unprefixed numbers as decimal
        { nB = NumberFormat.parse_nBit(n.getText(), NumberFormat.DECIMAL); }
    ;
    exception catch [nbit.ParseException ex] { throw new SemanticException(ex.toString(), #n.getFilename(), #n.getLine(), #n.getCol()); }

nbitw [Integer bits] returns [nBit nB = null]
    :   n:NBIT
        // interpret unprefixed numbers as decimal
        { nB = NumberFormat.parse_nBit(n.getText(), NumberFormat.DECIMAL, bits); }
    ;
    exception catch [nbit.ParseException ex] { throw new SemanticException(ex.toString(), #n.getFilename(), #n.getLine(), #n.getCol()); }









/////////////////////////////////////////////////////////////////////////////////////


opcodepartition_declaration! [Processor p] 
    :   #(TOP_OPCODEPARTITION
            ( opcodepartition_association[p] )*
        )
    ;


opcodepartition_association! [Processor p] {
    String name;
    OpcodePartition part;
}
    :   #(EQUALS 
            name=t:id 
            part=opcodepartition
            { part.shortName = name; }
        )
        { p.opcodepartitions.put(name,part); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }



opcodepartition! returns [ OpcodePartition part ] {
    String wsdl = null;
    List<WordPartition> wps = new ArrayList<WordPartition>();
    WordPartition wp;
}
    :   #(OPEN_SQUARE
            (wp=wordpartition {wps.add(wp);} )* 
            (   PIPE
                wsdl=string_literal
            )?
        )
        { part = new OpcodePartition(wps, wsdl); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }


wordpartition! returns [ WordPartition part ] {
    WideField wf;
    List<WideField> fields = new ArrayList<WideField>();
}
    :   #(WORDPARTITION
            (wf=widefield { fields.add(wf); } )*
        )
        { part = new WordPartition(fields); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }



widefield!  returns [ WideField w] {
    String name;
    nBit width;
}
    :   #(COLON 
            name=id 
            width=nbit
        )
        { w = new WideField(name, width); }
    ;




opcodepartition_val [ Processor p ] returns [OpcodePartition part] {
    String name;
}
    :   #(OPCODEPARTITION_REF name=id) 
        { part = p.resolveExists(name, OpcodePartitionResolver.R); }
    |   part=opcodepartition
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }




/////////////////////////////////////////////////////////////////////////////////////




instructionformat_declaration! [Processor p] 
    :   #(TOK_INSTRUCTIONFORMAT
            (   instructionformat_association[p] )*
        )
    ;


instructionformat_association! [Processor p] {
    String name;
    InstructionFormat format;
}
    :   #(EQUALS
            name=t:id!
            format=instructionformat[p]!
            { format.shortName = name; }
        )
        {   p.instructionformats.put(name, format); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


instructionformat! [Processor p] returns [InstructionFormat format] {
    OpcodePartition part;
    WideTypedField wtf;
    String wsdl = null;
    List<WideTypedField> args = new ArrayList<WideTypedField>();
}
    :   #(OPEN_SQUARE
            part = opcodepartition_val[p]
            PIPE
            (   wtf=widetypedfield
                { args.add(wtf); }
            )*
            (   PIPE
                wsdl=string_literal
            )?
        )
        { format = new InstructionFormat(part, args, wsdl); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }



widetypedfield!  returns [ WideTypedField wtf ] {
    String name;
    nBit width;
    String type;
}
    : #(COLON 
            name=id
            width=nbit 
            type=id
        )
        { wtf = new WideTypedField(name, width, type); }
    ;


instructionformat_val [ Processor p ] returns [InstructionFormat format] {
    String name;
}
    :   #(INSTRUCTIONFORMAT_REF name=t:id) 
        {   format = p.resolveExists(name, InstructionFormatResolver.R); }
        exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }
            
    |   format=instructionformat[p]
    ;


//////////////////////////////////////////////////////////////




instructiondefinition_declaration! [Processor p]
    :   #(TOK_IDEF 
            ( instructiondefinition_association[p] )*
        )
    ;

instructiondefinition_association! [Processor p]  {
    String name;
    InstructionDefinition idef;
}
    :   #(EQUALS
            name=t:id
            idef=instructiondefinition[p]
            { idef.shortName = name; }
        )
        {   p.instructiondefinitions.put(name, idef); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }
            


instructiondefinition! [Processor p] returns [InstructionDefinition idef] {
    InstructionFormat format;
    String wsdl = null;
}
    :   #(OPEN_SQUARE
            format=t:instructionformat_val[p]
            (   PIPE
                wsdl=string_literal
            )?
        )
        { idef = new InstructionDefinition(format, wsdl); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


