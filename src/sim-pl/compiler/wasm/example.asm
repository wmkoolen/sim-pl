##########################################################################
#  SIM-PL 2                                                              #
#  Digital Component Discrete Event Simulator                            #
#  Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                        #
#                                                                        #
#                                                                        #
#  This file is part of SIM-PL.                                          #
#                                                                        #
#                                                                        #
#  SIM-PL is free software; you can redistribute it and/or modify        #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation; either version 2 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  SIM-PL is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#                                                                        #
#  You should have received a copy of the GNU General Public License     #
#  along with SIM-PL; if not, write to the Free Software                 #
#  Foundation, Inc., 51 Franklin St, Fifth Floor,                        #
#  Boston, MA  02110-1301  USA                                           #
#                                                                        #
#                                                                        #
#  Contact:                                                              #
#  Wouter Koolen-Wijkstra                                                #
#  wmkoolen@gmail.com                                               #
#                                                                        #
##########################################################################


#@include "exampleInc1.asm"
	
	
# this part is for the include file

.memory DataMem
.setting {
	targetMemory = "Mips Processor.Data Memory:Memory"
	identifierType=address
}

.memory Registers
.setting {
	 targetMemory   ="Mips Processor.Registers:Registers"
	 identifierType=register
}
.constant { 
	 $0 = 0x00/register	
	 $1=  0x01/register
	 $2=  0x02/register
	 $3=  0x03/register
	 $4=  0x04/register
	 $5=  0x05/register
	 $6=  0x06/register
	 $7=  0x07/register
	 $8=  0x08/register
	 $9=  0x09/register
	 $10= 0x0A/register
	 $11= 0x0B/register
	 $12= 0x0C/register
	 $13= 0x0D/register
	 $14= 0x0E/register
	 $15= 0x0F/register
}


	
.processor MIPS : Registers
.setting {
	"targetMemory"    =  "Mips Processor.Instruction Memory:Memory"
}	


.opcodepartition BOGUS = [OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, RD: 0d4, UNUSED:0d12]
	
.opcodepartition {
	ARITH  = [OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, RD: 0d4, UNUSED:0d12]
	ARITHI = [OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, IMM:0d16]
	LSB =    [OP:0d2, AUX:0d3,                  ALU:0d3, RS:0d4, RT:0d4, OFFS:0d16]
}

.instructionformat   BOGUS = BOGUS []
.instructionformat   BOGUS2 = [OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, RD: 0d4, UNUSED:0d12] []
.instructionformat   {
	BOGUS3 = [OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, RD: 0d4, UNUSED:0d12] []
}

.instructionformat   {
	ZERO =      ARITH  []	
	ARITH0 =    ARITH  [rd:0d4/register]
	ARITH1 =    ARITH  [rd:0d4/register, rt:04/register]
	ARITH2 =    ARITH  [rd:0d4/register, rs:0d4/register, rt:04/register]
	ARITHI1 =   ARITHI [rt:0d4/register, imm:0d16/number]
	ARITHI2 =   ARITHI [rt:0d4/register, rs:0d4/register, imm:0d16/immediate]
	
	LOADSTORE = LSB [rt:0d4/register, offs:0d16/address, rs:0d4/register]
	BRANCH2  =  LSB [rt:0d4/register, rs:0d4/register, offs:0d16/label]
	BRANCH1  =  LSB [rt:0d4/register, offs:0d16/register]
	BRANCH0  =  LSB [offs:0d16/label]
}



.instructiondefinition { 
  HALT = ZERO      "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x7; RS=0;  RT=0;  RD=0;  UNUSED=0xFFF;"
  NOP  = ZERO      "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x7; RS=0;  RT=0;  RD=0;  UNUSED=0;"
  MAX  = ARITH0    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x7; RS=0;  RT=0;  RD=rd; UNUSED=0;"
  NOT  = ARITH1    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x0; RS=0;  RT=rt; RD=rd; UNUSED=0;"
  TOO  = ARITH1    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x1; RS=0;  RT=rt; RD=rd; UNUSED=0;"
  SUB  = ARITH2    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x2; RS=rs; RT=rt; RD=rd; UNUSED=0;"
  ADD  = ARITH2    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x3; RS=rs; RT=rt; RD=rd; UNUSED=0;"
  XOR  = ARITH2    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x4; RS=rs; RT=rt; RD=rd; UNUSED=0;"
  OR   = ARITH2    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x5; RS=rs; RT=rt; RD=rd; UNUSED=0;"
  AND  = ARITH2    "OP=0b01; REGDEST=0b1; ALUSRCB=0b00; ALU=0x6; RS=rs; RT=rt; RD=rd; UNUSED=0;"
	
  NOTI = ARITHI1   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x0; RS=0;  RT=rt; IMM=imm;"
  TOOI = ARITHI1   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x1; RS=0;  RT=rt; IMM=imm;"
  LI   = ARITHI1   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x1; RS=0;  RT=rt; IMM=imm;"
  SUBI = ARITHI2   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x2; RS=rs; RT=rt; IMM=imm;"
  ADDI = ARITHI2   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x3; RS=rs; RT=rt; IMM=imm;"
  XORI = ARITHI2   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x4; RS=rs; RT=rt; IMM=imm;"
  ORI  = ARITHI2   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x5; RS=rs; RT=rt; IMM=imm;"
  ANDI = ARITHI2   "OP=0b01; REGDEST=0b0; ALUSRCB=0b10; ALU=0x6; RS=rs; RT=rt; IMM=imm;"


# Alas, the compiler does not handle absolute adresses
# We implement the unconditional jump as a conditional one
# with the condition that register zero equals itself.
# #.instructionformat   BRANCH0  OP:0d2, OFFS:0d30 | offs:0d30
# #J              BRANCH0  "OP=0b11; OFFS=offs;"

  J    = BRANCH0   "OP=0b10; AUX=0b110; ALU=0x2; RS=0;  RT=0;  OFFS=offs-self;"
  BZ   = BRANCH1   "OP=0b10; AUX=0b110; ALU=0x1; RS=0;  RT=rt; OFFS=offs-self;"
  BEQ  = BRANCH2   "OP=0b10; AUX=0b110; ALU=0x2; RS=rs; RT=rt; OFFS=offs-self;"
  LW   = LOADSTORE "OP=0b00; AUX=0b000; ALU=0x3; RS=rs; RT=rt; OFFS=offs;"
  SW   = LOADSTORE "OP=0b00; AUX=0b001; ALU=0x3; RS=rs; RT=rt; OFFS=offs;"
}

	

	
# this part actually appears in the user's file	
	
	

.data Flmem :	DataMem # for the Main memory
0x0:	WORD blarf 0x10
	WORD florf
	WORD florf2
0x5:	WORD N


.data Fliirf:	Registers # for the register file
0x0:	WORD  beginnig, srart, first 0
	WORD  1
	WORD  1
	WORD  1


.code MainCode :	MIPS, Flmem
start:	ADD $1, $2, $1
	HALT
	AND $1, $2, $3 
	MAX $1
	LW  $1, blarf, $2
	J    start
end:	BEQ $0, $1, end
	#fluflo
	