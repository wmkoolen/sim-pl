/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nbit.nBit;

public class Context {

	abstract static class Resolver<T> {
		abstract T resolve(String id, Context c);

		abstract String looksFor();

		T getDefault(String id) {
			return null;
		}
	}

	static class ConstantResolver extends Resolver<ValueType<nBit, String>> {
		static final ConstantResolver R = new ConstantResolver();

		ValueType<nBit, String> resolve(String id, Context c) {
			return c.constants.get(id);
		}

		String looksFor() {
			return "constant";
		}
	}

	static class AutocastResolver {
		final Context c;
		public AutocastResolver(Context c) {
			this.c = c;
		}

		public boolean canAutocast(Object from, String into) {
			assert !from.equals(into) : "should already be tested for externally";
			return canRecAutocast(c, from, into);
		}

		// depth first search for a match
		private boolean canRecAutocast(Context c, Object from, String into) {
			Collection<String> s = c.autocast.get(into);
			if (s != null && s.contains(from)) return true;
			for (Context x : c.parents) {
				if (canRecAutocast(x, from, into)) return true;
			}
			return false;

		}
	}


	static class SettingResolver extends Resolver<String> {
		static final SettingResolver R = new SettingResolver();

		String resolve(String id, Context c) {
			return c.settings.get(id);
		}

		public static final String ADDRESS_TYPE = "addressType";
		public static final String LABEL_TYPE = "labelType";
		public static final String NUMBER_TYPE = "numberType";

		String getDefault(String name) {
			if (name.equals(ADDRESS_TYPE))
				return "address";
			if (name.equals(LABEL_TYPE))
				return "label";
			if (name.equals(NUMBER_TYPE))
				return "number";
			return null;
		}

		String looksFor() {
			return "setting";
		}

	}

    String name;
    List<Context> parents = new ArrayList<Context>();

    SymbolTable<String>       settings   = new SymbolTable<String>();
    SymbolTable<ValueType<nBit, String>>    constants  = new SymbolTable<ValueType<nBit, String>>();
	Collection<HighlightDef>  highlights = new ArrayList<HighlightDef>();

	// key: target type,  value: list of source types
	Map<String, Collection<String>> autocast = new HashMap<String,Collection<String>>();

    public Context(String name) {
		this.name = name;
    }

    public void addParent(Context c) {
		parents.add(c);
    }

	public void addAutocast(String from, String into) {
		Collection<String> s = autocast.get(into);
		if (s == null) {
			s = new ArrayList<String>();
			autocast.put(into, s);
		}
		s.add(from);
	}




	public void collectHighlights(Collection<HighlightDef> c) {
		c.addAll(highlights);
		for (Iterator<Context> it = parents.iterator(); it.hasNext(); ) {
			it.next().collectHighlights(c);
		}
	}


    public <T> T resolveExists(String name, Resolver<T> r) throws UndefinedSymbolException {
		T o = resolve(name, r);
		if (o == null) throw new UndefinedSymbolException(r.looksFor(), name, this);
		return o;
    }


    public <T> T resolve(String name, Resolver<T> r) {
		T o = resolveRec(name, r);
		if (o != null) return o;
		return r.getDefault(name);
    }

    public <T> T resolveRec(String name, Resolver<T> r) {
		T o = r.resolve(name, this);
		if (o != null) return o;
		for (Context c : parents) {
			o = c.resolveRec(name, r);
			if (o != null) return o;
		}
		return null;
    }

}
