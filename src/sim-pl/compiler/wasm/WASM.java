/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.awt.Color;
import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import log.Logger;
import nbit.nBit;
import util.ExceptionHandler;
import util.ExtensionAddingFilter;
import util.FileSlurper;
import antlr.CommonAST;
import antlr.TokenStreamSelector;

import compiler.ComponentCompiler;
import compiler.ComponentProgram;
import compiler.wisent.Instance_Simple;

import executer.Instance;
import executer.Simulator;
import executer.timetable.AliasResolver;
import executer.timetable.Highlight;
import executer.timetable.HighlightManager;
import executer.timetable.InstanceAliasResolver;
import executer.timetable.LocatedTextRun;
import executer.timetable.TimeTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class WASM implements ComponentCompiler {

//	public static final WASM compiler = new WASM();

	public String toString() {
		return "WASM";
	}

	ExtensionAddingFilter filter; // lazy instantiate

	public ExtensionAddingFilter getFileFilter() {
		if (filter == null) filter = new ExtensionAddingFilter("WASM program", ".wasm");
		return filter;
	}

	public static final boolean DEBUG = false;

	public SymbolTable<Context> compilePreprocess(String source, File includesDir, Instance ifor, Logger log, String sourceFileName)  throws Exception {

		Reader dis = new StringReader(source);

		TokenStreamSelector tss = new TokenStreamSelector();

		//-------------------------------------
		L lexer = new L(dis, tss, false, includesDir);
		lexer.setFilename(sourceFileName);

		tss.addInputStream(lexer, "main");
		tss.select(lexer);

		P parser = new P(tss, true);

		try {
			log.addDoing("Parsing");
			parser.program();
			log.addSuccess();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw log.addFailure(ex);
		}

		CommonAST ast = (CommonAST) parser.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (1):");
			System.out.println(ast.toStringList());
		}
		final SymbolTable<Context> id2segment = new SymbolTable<Context>();

		//-------------------------------------
		Pass1 pass1 = new Pass1(id2segment);
		try {
			log.addDoing(pass1.passInfo);
			pass1.program(ast);
			log.addSuccess();
		} catch (Exception ex) {
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast2 = (CommonAST) pass1.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (2):");
			System.out.println(ast2.toStringList());
		}

		//-------------------------------------
		Pass2 pass2 = new Pass2(id2segment);
		try {
			log.addDoing(pass2.passInfo);
			pass2.program(ast2);
			log.addSuccess();
		} catch (Exception ex) {
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast3 = (CommonAST) pass2.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (3):");
			System.out.println(ast3.toStringList());
		}
		//-------------------------------------
		Pass3 pass3 = new Pass3(id2segment, ifor);
		try {
			log.addDoing(pass3.passInfo);
			pass3.program(ast3);
			log.addSuccess();
		} catch (Exception ex) {
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast4 = (CommonAST) pass3.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (4):");
			System.out.println(ast4.toStringList());
		}
		//-------------------------------------
		Pass4 pass4 = new Pass4(id2segment);
		try {
			log.addDoing(pass4.passInfo);
			pass4.program(ast4);
			log.addSuccess();
		} catch (Exception ex) {
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast5 = (CommonAST) pass4.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (5):");
			System.out.println(ast5.toStringList());
		}

		//-------------------------------------
		Pass5 pass5 = new Pass5(id2segment);
		try {
			log.addDoing(pass5.passInfo);
			pass5.program(ast5);
			log.addSuccess();
		} catch (Exception ex) {
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast6 = (CommonAST)pass5.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (6):");
			System.out.println(ast6.toStringList());
		}


		//-------------------------------------
		Pass6 pass6 = new Pass6(id2segment);
		try {
			log.addDoing(pass6.passInfo);
			pass6.program(ast6);
			log.addSuccess();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw log.addFailure(ex);
		}

	//		id2segment.dump();

		CommonAST ast7 = (CommonAST) pass6.getAST();
		if (DEBUG) {
			System.out.println("AST STRINGLIST (7):");
			System.out.println(ast7.toStringList());
		}
		return id2segment;
	}


	public ComponentProgram compile(String source, File includesDir, Instance ifor, Logger log)  throws Exception {
		final SymbolTable<Context> id2segment = compilePreprocess(source, includesDir, ifor, log, INPUT_TEXT_NAME);
		return new WASMProgram(id2segment);
	}

	public ComponentProgram compile(File sourceFile, Instance ifor, Logger log)  throws Exception {
		final String source = FileSlurper.slurp(sourceFile);
		final SymbolTable<Context> id2segment = compilePreprocess(source, sourceFile.getParentFile(), ifor, log, sourceFile.getAbsolutePath());
		return new WASMProgram(id2segment);
	}



	class WASMProgram implements ComponentProgram, AliasResolver, HighlightManager {
		SymbolTable<Context> id2segment;
		Map<Simulator.Instance_Simple, InstanceAliasResolver> instance2addr2name = new HashMap<Simulator.Instance_Simple,InstanceAliasResolver>();
		List<Highlight> highlights;

		public WASMProgram(SymbolTable<Context> id2segment) {
			this.id2segment = id2segment;

			for (Context c : id2segment.n2v.values()) {
				if (c instanceof RealTargetContext) {
					final RealTargetContext rtc = (RealTargetContext)c;
					instance2addr2name.put(rtc.i, new InstanceAliasResolver() {
						public List<String> resolveAliases(int address) {
							return rtc.names[address];
						}
					});
				}
//				c.collectHighlights(highlightdefs);
			}
		}

		public void execute(TimeTable tt) throws Exception {
			for (Object c : id2segment.n2v.values()) {
				if (c instanceof RealTargetContext) {
					RealTargetContext rtc = (RealTargetContext)c;
					rtc.execute();
				}
			}
		}

		public AliasResolver getAliasResolver() {
			return this;
		}

		public HighlightManager getHighlightManager() {
			return this;
		}

		public InstanceAliasResolver getAliases(Instance component) {
			return instance2addr2name.get(component);
		}

		public void recomputeHighlights(TimeTable tt) {
			highlights = new ArrayList<Highlight>();

			for (Context c : id2segment.n2v.values()) {
				if (c instanceof Code) {
					Code rtc = (Code) c;
					List<HighlightDef> highlightdefs = new ArrayList<HighlightDef>();
					rtc.collectHighlights(highlightdefs);

					highlights = new ArrayList<Highlight>();
					for (HighlightDef hd : highlightdefs) {
						try {
							nBit address = ( (Instance_Simple) hd.e.evaluate(hd.i.getNamespace())).getValue();
							int addr = address.toInt();
//							System.out.println("Request to highlight " + address);
							if (rtc.ltr == null || addr < 0 || addr >= rtc.ltr.length)  continue;
							LocatedTextRun ltr = rtc.ltr[addr];
							if (ltr == null) continue; // past the end
//							if (ltr.file.equals(sourceFile)) ltr.file = sourceFile;
							highlights.add(new Highlight(hd.c != null ? hd.c : Color.green, ltr));
						} catch (Exception ex) {
							throw new ExceptionHandler(ex);
						}
					}
				}
			}
//			System.out.println("New highlights: " + highlights);
		}

		public List<Highlight> getHighlights() {
			return highlights;
		}
	}
}
