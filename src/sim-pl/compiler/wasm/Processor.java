/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.util.List;

import nbit.IntConversionException;
import nbit.ZeroBitException;
import nbit.nBit;
import util.ExceptionHandler;

import compiler.LocatedException;
import compiler.wasm.Context.Resolver;
import compiler.wisent.DataSource;
import compiler.wisent.DataSource_Direct;
import compiler.wisent.IdentifierAlreadyDefined;
import compiler.wisent.Instance_Simple;
import compiler.wisent.Namespace;
import compiler.wisent.Statement;
import compiler.wisent.Type;
import compiler.wisent.Type_Simple;
import compiler.wisent.Type_Struct;
import compiler.wisent.Wisent;


class WideField {
    String name;
    nBit width;
    public WideField(String name, nBit width) {
		this.name = name;
		this.width = width;
    }

    public String toString() {
		return name + ":" + width;
    }
}

class WideTypedField  {
    String name;
    nBit width;
    String type;

    public WideTypedField(String name, nBit width, String type) {
		this.name = name;
		this.width = width;
		this.type = type;
    }

    public String toString() {
		return name + ":" + width+ "/" + type;
    }
}

class WordPartition {
	List<WideField> fields;
	int bits = 0;

	public WordPartition(List<WideField> fields) throws WASMException {
		this.fields = fields;

		for (WideField f : fields) {
			try {
				int width = f.width.toInt();
				bits += width;
			} catch (IntConversionException ex) {
				throw new WASMException("Field " + f + " is too wide");
			}
		}

	}
}

class OpcodePartition {
	String shortName;
	List<WordPartition> words;
	String wsdl;
	Type_Struct t;

	Statement s = null;


	public OpcodePartition(List<WordPartition> words, String wsdl) throws WASMException {
		this.words = words;
		this.wsdl = wsdl;

		t = new Type_Struct();

		for (WordPartition w : words) {
			for (WideField f : w.fields) {
				try {
					int width = f.width.toInt();
					t.set(f.name, new Type.Access(true, false, new Type_Simple(width, false)));
				} catch (IntConversionException ex) {
					throw new WASMException("Field " + f + " is too wide");
				} catch (ZeroBitException ex) {
					throw new WASMException("Invalid field width: " + f);
				} catch (IdentifierAlreadyDefined ex) {
					throw new WASMException("Redefined field: " + f);
				}
			}
		}

		if (wsdl != null) {
			try {
				s = Wisent.compiler.parse(t, "{" + wsdl + "}");
			} catch (LocatedException ex) {
				throw new WASMException("Error in code: " + wsdl + "\n"+ex.getMessage(), ex);
			}
		}
	}

	public String toString() {
		return words.toString();
	}

	public String getName() {
		return shortName == null
				? words.toString()
				: shortName;
	}
}

class InstructionFormat {
    String shortName;
	OpcodePartition part;
    List<WideTypedField> args;
	String wsdl;

	Type_Struct t;
	Statement   s = null;

	Namespace<String,DataSource>  n;

	public static final String SELF_STRING = "self";


    public InstructionFormat(OpcodePartition part, List<WideTypedField> args, String wsdl) throws WASMException {
//		this.name = name;
		this.part = part;
		this.args = args;
		this.wsdl = wsdl;

		t = new Type_Struct(part.t);

		for (WideTypedField f : args) {
			try {
				int width = f.width.toInt();
				t.set(f.name, new Type.Access(false, true, new Type_Simple(width, false)));
			} catch (IntConversionException ex) {
				throw new WASMException("Field " + f + " is too wide");
			} catch (ZeroBitException ex) {
				throw new WASMException("Invalid field width: " + f);
			} catch (IdentifierAlreadyDefined ex) {
				throw new WASMException("Redefined field: " + f);
			}
		}

		try {
			t.set(SELF_STRING, new Type.Access(false, true, new Type_Simple(32, false)));
		} catch (IdentifierAlreadyDefined ex)	{
			throw new WASMException("The field " + SELF_STRING + " is reserved for internal use" + ex.identifier);
		} catch (ZeroBitException ex) {
			throw new ExceptionHandler("That's impossible");
		}

		if (wsdl != null) {
			try {
				s = Wisent.compiler.parse(t, "{" + wsdl + "}");
			} catch (LocatedException ex) {
				throw new WASMException("Error in code: " + wsdl + "\n"+ex.getMessage(), ex);
			}
		}

		n = t.createNamespace();
    }

    public String toString() {
		return (part.shortName == null ? part.toString() : part.shortName) + " " + args;
    }

	public String getName() {
		return shortName == null
				? part.getName() + " " + args.toString()
				: shortName;
	}

}

class InstructionDefinition {
	String shortName;
    String wsdl;
    InstructionFormat format;
	Statement s;

	public InstructionDefinition(InstructionFormat format, String wsdl) throws WASMException {
//		this.name = name;
		this.format = format;
		this.wsdl = wsdl;

		if (wsdl != null) {
			try {
				this.s = Wisent.compiler.parse(format.t, "{" + wsdl + "}");
			} catch (LocatedException ex) {
				throw new WASMException("Error in code: " + wsdl + "\n"+ex.getMessage(), ex);
			}
		}
    }

    public String toString() {
		return (format.shortName == null ? format.toString() : format.shortName)  + " \"" + wsdl + "\"";
    }



    public nBit [] assemble(Context.AutocastResolver acr, Type_Simple type, int cur, List<ValueType<nBit,String>> args) throws WASMException {

		if (args.size() != format.args.size()) throw new WASMException("Illegal number of arguments. " + args.size() + " given, while " + format.args.size() + " required");
		for (WordPartition w : format.part.words) {
			if (w.bits != type.bits) throw new WASMException("Can not assemble " + w.bits + " bit instruction word into " + type.bits + " bit memory");
		}

		// type checking of arguments
	    for (int i = 0; i < args.size(); i++) {
			ValueType<nBit,String> arg = args.get(i);
			WideTypedField wtf = format.args.get(i);

			if (!arg.type.equals(wtf.type) && !acr.canAutocast(arg.type, wtf.type)) {
				throw new WASMException("Type error. Expected type is " + wtf.type + " but given type is " + arg.type);
			}
		}


		try {
			for (int i = 0; i < format.args.size(); i++) {
				DataSource_Direct ds = (DataSource_Direct) format.n.get( format.args.get(i).name);
				ds.getValue().changeInto(new Instance_Simple( (Type_Simple) ds.
					getValue().getType(), args.get(i).value));
			}

			DataSource_Direct ds = (DataSource_Direct)format.n.get(InstructionFormat.SELF_STRING);
			ds.getValue().changeInto(new Instance_Simple((Type_Simple)ds.getValue().getType(), new nBit(32, cur)));

//			System.out.println("Pre execution: " + format.n);
			if (format.part.s != null) format.part.s.execute(format.n);
			if (format     .s != null) format.     s.execute(format.n);
			if (            s != null)             s.execute(format.n);
//			System.out.println("Post execution: " + format.n);

			//	TODO change bit width from 32 to "tagetWordSize"

			nBit [] results = new nBit[format.part.words.size()];

			for (int wi = 0; wi < format.part.words.size(); wi++) {
				WordPartition w = format.part.words.get(wi);
				nBit result = new nBit(type);

				for (WideField f : w.fields) {
					result = result.SHIFT_LEFT(f.width);
					nBit v = ( (Instance_Simple) ( format.n.get(f.name)).
							  getValue()).getValue();
					result = result.OR_BIT(v);
				}
				results[wi] = result;
			}
			return results;
		} catch (ZeroBitException ex) {
			throw new WASMException(ex.getMessage());
		} catch (UndefinedSymbolException ex ) {
			throw new WASMException(ex.getMessage());
		} catch (compiler.wisent.UndefinedIdentifierException ex) {
			throw new WASMException(ex.getMessage());
		} catch (Exception ex) {
			ExceptionHandler.handleException(ex);
			throw new WASMException(ex.getMessage());
		}
    }

	public String getName() {
		return shortName == null
				? format.getName() + " " + wsdl
				: shortName;
	}
}


class Processor extends Context {
	SymbolTable<OpcodePartition>       opcodepartitions =       new SymbolTable<OpcodePartition>();
	SymbolTable<InstructionFormat>     instructionformats  =    new SymbolTable<InstructionFormat>();
    SymbolTable<InstructionDefinition> instructiondefinitions = new SymbolTable<InstructionDefinition>();

    public Processor(String name) {
		super(name);
    }

    public String toString() {
		return "Processor " + name;
    }
}



class OpcodePartitionResolver extends Resolver<OpcodePartition> {
	static final OpcodePartitionResolver R = new OpcodePartitionResolver();

	OpcodePartition resolve(String id, Context c) {
	if (c instanceof Processor) {
		Processor p = (Processor)c;
		return p.opcodepartitions.get(id);
	}
	return null;
	}

	String looksFor() { return "opcode partition"; }
}


class InstructionFormatResolver extends Resolver<InstructionFormat> {
    static final InstructionFormatResolver R = new InstructionFormatResolver();

    InstructionFormat resolve(String id, Context c) {
		if (c instanceof Processor) {
			Processor p = (Processor)c;
			return p.instructionformats.get(id);
		}
		return null;
    }

    String looksFor() { return "instruction format"; }
}

class InstructionDefinitionResolver extends Resolver<InstructionDefinition> {
    static final InstructionDefinitionResolver R = new InstructionDefinitionResolver();

    InstructionDefinition resolve(String id, Context c) {
		if (c instanceof Processor) {
			Processor p = (Processor)c;
			return p.instructiondefinitions.get(id);
		}
		return null;
    }

    String looksFor() { return "instruction"; }
}


