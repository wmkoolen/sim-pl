/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
}


/* First compiler pass
   - stores segment names into symbol table (segment_declaration)

   * declares (and removes) all
     - settings
     - autocast rules
     - opcodepartitions
*/

class Pass1 extends PassBase;
{
    SymbolTable<Context> id2segment;
    public Pass1(SymbolTable<Context> id2segment) {
        this();
        this.id2segment = id2segment;
        this.setASTNodeClass(WAST.class.getName());
    }

    String passInfo = "Segment declaration, settings, autocasts & opcode partitions";
}

program  {
    Context c;
}
    :   ( segment ) *
    ;


segment {
    Context c;
}
    :   (   c=processor 
        |   c=memory 
        |   c=code 
        |   c=data
        )
        {   id2segment.put(c.name, c); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }



processor returns [ Processor p = null ] {
    String name;
}
    :   #(".processor" 
            name=id 
            {   p = new Processor(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   settings[p]!
            |   autocasts[p]!
            |   opcodepartition_declaration[p]!
            |   deepcopy
            )*
        )
    ;

memory returns [ Memory m = null ] {
    String name;
}
    :   #(".memory" 
            name=id 
            {   m = new Memory(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   settings[m]!
            |   autocasts[m]!
            |   deepcopy
            )*
        )
    ;

code returns [ Code c = null ] {
    String name;
}
    :   #(".code" 
            name=id 
            {    c = new Code(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   settings[c]!
            |   autocasts[c]!
            |   deepcopy
            )*
        )
    ;

data returns [Data d = null] {
    String name;
}
    :   #(".data" 
            name=id 
            {   d = new Data(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   settings[d]!
            |   autocasts[d]!
            |   deepcopy
            )*
        )
    ;



settings!  [Context c] 
    :   #(TOK_SETTING (setting[c])*)
    ;

setting! [Context c] {
    String name;
    String value;
}
    :   #(t:EQUALS 
            (name=id | name=string_literal) 
            value=atom)
        {   c.settings.put(name, value); } 
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }




autocasts!  [Context c] 
    :   #(TOK_AUTOCAST (autocast[c])*)
    ;

autocast! [Context c] {
    String from, into;
}
    :   #(t:INTO
            (from=id | from=string_literal) 
            (into=id | into=string_literal))
        {   c.addAutocast(from, into); }         
    ;
//    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }





// instructionformat!  [ Processor p] {
//     List fields = new ArrayList();
//     List args = new ArrayList();
//     String name;
// }
//     :   #(TOK_INSTRUCTIONFORMAT
//             name=id
//             ( opcodefield[fields] )* // list of fields
//             PIPE
//             ( inst_arg[args] )* // list of args
//         )
//         { p.instructionformats.put(name, new InstructionFormat(name, fields, args)); }
//     ;




//instructiondefinition
//    :   #(TOK_IDEF (deepcopy)*)
//    ;


