/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
    import databinding.Component;
    import compiler.wasm.Context.SettingResolver; 
    import compiler.wasm.Context.ConstantResolver; 
}

/* Pass 5
  * declares (and removes) all
    - labels
    - variables
*/


class Pass5 extends PassBase;
{
    SymbolTable id2segment;

    public Pass5(SymbolTable id2segment) {
        this();
        this.id2segment = id2segment;
        this.setASTNodeClass(WAST.class.getName());
    }

    String passInfo = "Labels & variables";
}

program 
    :   (   processor 
        |   memory 
        |   code 
        |   data
        )* 
    ;


processor {
    String name;
    Processor p;
}
    :   #(".processor" 
            name=t:id 
            {   p = (Processor)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


memory {
    String name;
    Memory m;
}
    :   #(".memory" 
            name=t:id 
            { m = (Memory)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

code {
    String name;
    Code c;
}
    :   #(".code" 
            name=t:id 
            {   c = (Code)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   label[c]! 
            |   instruction[c]  // for counting label addresses
            |   variabledecl[c] // leave in for counting
            |   offset_spec[c]  // leave in for counting
            |   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


data {
    String name;
    Data d;
}
    :   #(".data" 
            name=t:id 
            {   d = (Data)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   variabledecl[d]!
            |   offset_spec[d]!
            |   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }







label! [ Code c] {
    String l;
}

    :   #(t:LABEL l=id)
        { 
            try {
                // the following 32 is not silly
                c.constants.put(l, new ValueType<nBit, String>(new nBit(32, c.offset), c.resolveExists(SettingResolver.LABEL_TYPE, SettingResolver.R))); 
                c.setName(c.offset, l);
            } catch (ZeroBitException ex) {
                throw new Error(ex); 
            }
        }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }




offset_spec [ RealTargetContext rtc] {
    nBit n;
    String c;
}
    :   #(t:OFFSET_SPEC 
            (   n=nbit 
            |   c=id   {  n = rtc.resolveExists(c, ConstantResolver.R).value; }
            )
            {  rtc.offset = n.toInt(); }
        )    
    ;
    exception 
    catch [IntConversionException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }
    catch [UndefinedSymbolException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }





instruction [ Code c ]  {
    String name;
    InstructionDefinition inde;
}
    :   #(t:INSTRUCTION 
            name=id { inde = c.resolveExists(name, InstructionDefinitionResolver.R); }
            (deepcopy)*)
        { c.offset += inde.format.part.words.size(); } // advance # of words
    ;
    exception 
    catch [UndefinedSymbolException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


variabledecl [ RealTargetContext d] {
    String  name =      null;
    nBit    v =         null;
}
    
    :   #(WORD 
            // variable declarations
            (   name=id 
                {   d.constants.put(name, new ValueType<nBit, String>(new nBit(32, d.offset), d.resolveExists(SettingResolver.ADDRESS_TYPE, SettingResolver.R))); 
                    d.setName(d.offset, name);
                } 
            )*  

            // optional initialiser
            (   v=nbitw[d.getBits()]
                { d.setValue(d.offset, v, ##_in); }
            )?  

            // automatically advance to next position
            { d.offset++; }
        )
    ;
    exception
    catch [ZeroBitException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }
    catch [WASMException ex] { throw new SemanticException(ex.toString(), ##_in.getFilename(), ##_in.getLine(), ##_in.getCol()); }

