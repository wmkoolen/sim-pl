/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.awt.Color;
import java.util.Map;

import util.Converter;

import compiler.wisent.Expression;
import compiler.wisent.IdentifierValueMapping;
import compiler.wisent.Type;
import compiler.wisent.Type.Access;
import compiler.wisent.Type_Struct;
import compiler.wisent.Wisent;

import databinding.HPL;
import executer.Instance;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class HighlightDef {
	Instance i;
	Expression e;
	Color c;

	public HighlightDef(Instance ifor, String hplstr, String wsdl, String color) throws WASMException {
	    try {
		HPL hpl = new HPL(hplstr);
		Object o = ifor.resolve(hpl);
		if (!(o instanceof Instance)) throw new WASMException(hplstr + " does not resolve to a component");
		this.i = (Instance)o;
		Type_Struct t = i.getType();
		
		// Highlights can read outputs (which are normally write-only).
		// but they can not write at all
		Type_Struct read_all = new Type_Struct();
		for (String id : t.getIdentifiers()) {
			read_all.set(id, new Access(false, true, t.get(id).type));
		}
		
		this.e = Wisent.compiler.parseExpression(read_all, wsdl);

		if (color != null) c = Converter.toColor(color);

	    } catch (WASMException ex) { throw ex; }
	      catch (Exception ex) { throw new WASMException(ex); }
	}
}
