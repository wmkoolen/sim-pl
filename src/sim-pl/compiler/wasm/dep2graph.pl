#!/usr/bin/perl -w

##########################################################################
#  SIM-PL 2                                                              #
#  Digital Component Discrete Event Simulator                            #
#  Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                        #
#                                                                        #
#                                                                        #
#  This file is part of SIM-PL.                                          #
#                                                                        #
#                                                                        #
#  SIM-PL is free software; you can redistribute it and/or modify        #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation; either version 2 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  SIM-PL is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#                                                                        #
#  You should have received a copy of the GNU General Public License     #
#  along with SIM-PL; if not, write to the Free Software                 #
#  Foundation, Inc., 51 Franklin St, Fifth Floor,                        #
#  Boston, MA  02110-1301  USA                                           #
#                                                                        #
#                                                                        #
#  Contact:                                                              #
#  Wouter Koolen-Wijkstra                                                #
#  wmkoolen@gmail.com                                               #
#                                                                        #
##########################################################################


use strict;

my %n2deps;

while (<>) {
    chomp;
    s/#.*//;
    $_ or next;

    my ($ids, $deps) = split /\s*:\s*/;

    my @ids  = split /\s*,\s*/, $ids;
    my @deps = split /\s*,\s*/, $deps;

    for (@ids) {
	push @{$n2deps{$_}}, @deps;
    }
#    print "id: $id, $deps: $deps\n"; 
}


print "digraph G {\n";

while (my ($id,$deps) = each %n2deps) {
    dummy($id) and next;

    print "$id;\n"; # only useful for tasks without dependencies

    for (map {resolve($_)} (@$deps)) {
	print "$id -> $_;\n";
    }
}
print "}\n";


sub resolve {
    my $id = shift;
    if (dummy($id)) {
	return map { resolve($_) } (@{$n2deps{$id}});
    }
    return $id;
}

sub dummy {
    substr($_[0], 0, 1) eq ".";
}
