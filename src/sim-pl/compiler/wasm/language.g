/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
    import antlr.*;
    import java.io.*;
    import util.FileManager;
}



class P extends Parser;
options {
    k = 2; /* distinguish labels from instructions */
    defaultErrorHandler = false;
    buildAST = true;
    ASTLabelType = WAST;
}
tokens {
    LABEL;
    OFFSET_SPEC;
    INSTRUCTION;
    OPCODEPARTITION_REF;
    INSTRUCTIONFORMAT_REF;
    WORDPARTITION;
}

{
    TokenStreamSelector tss;

    public P(TokenStreamSelector tss, boolean unique) {
        this(tss);
        this.tss = tss;
        this.setASTNodeClass(WAST.class.getName());
    }

    public String getFilename() {
        return ((L)tss.getCurrentStream()).getFilename();
    }
}


program 
    :   (NL!)* 
        ( processor | memory | code | data )*
        EOF!
    ;


inheritance
    :   (COLON^ id (COMMA! id)*)?
    ;

processor
    :  ".processor"^  id inheritance sep
//        settings
//        constants
        (
            (   setting_definition
            |   autocast_definition
            |   constant_definition
            |   highlight_definition
            |   opcodepartition_definition
            |   instructiondefinition_definition
            |   instructionformat_definition
            ) 
            sep
        )*
    ;

memory
    :  ".memory"^  id inheritance sep
        (
            (   setting_definition 
            |   autocast_definition
            |   constant_definition
            )
            sep
        )*
    ;


code 
    :   ".code"^  id inheritance sep
        ( 
            (   setting_definition
            |   autocast_definition
            |   constant_definition
            |   command
            )
            sep 
        )*
    ;

data
    :   ".data"^  id inheritance sep
        (   
            (   setting_definition
            |   autocast_definition
            |   constant_definition
            |   variabledecl
            ) 
            sep 
        )*
    ;

sep! 
    :   (EOF! | (NL!)+)
    ;


setting_definition
    :   TOK_SETTING^
        (   OPEN_BRACE! (sep)?
            (setting_association (sep)?)*
            CLOSE_BRACE!
        |   setting_association
        )
    ;

setting_association
    :   (string_literal|id) 
        EQUALS^
        atom
    ;





autocast_definition
    :   TOK_AUTOCAST^
        (   OPEN_BRACE! (sep)?
            (autocast_association (sep)?)*
            CLOSE_BRACE!
        |   autocast_association
        )
    ;


autocast_association
    :   (string_literal|id) 
        INTO^ 
        (string_literal|id)
    ;





constant_definition
    :   TOK_CONSTANT^
        (   OPEN_BRACE! (sep)?
            ( constant_association (sep)? ) *
            CLOSE_BRACE!
        |   constant_association
        )
    ;

// constants
//     :   (TOK_CONSTANTS) => constants2 sep
//     | ! {## = #[TOK_CONSTANTS, ".constants"]; }
//     ;

// constants2
//     :   TOK_CONSTANTS^ (sep)? OPEN_BRACE! (sep)? (constant (sep)?)* CLOSE_BRACE! 
//     ;

constant_association
    :   id EQUALS^ nbit (SLASH! id)?
    ;

atom
    :   string_literal
    |   id
//    |   nbit
    ;


opcodepartition_definition
    :   TOP_OPCODEPARTITION^
        (   OPEN_BRACE! (sep)?
            ( opcodepartition_association (sep)? ) *
            CLOSE_BRACE!
        |   opcodepartition_association
        )
    ;

opcodepartition_association
    :   id EQUALS^ opcodepartition
    ;

opcodepartition
    :   OPEN_SQUARE^
        (wordpartition (SEMICOL! wordpartition)*)? 
        (   PIPE
            string_literal // program code for unused fields
        )?
        CLOSE_SQUARE!
    ;

wordpartition
    :   wp:wordpartition_workhorse
        {   ##=#([WORDPARTITION, "WORDPARTITION"], wp);
            ##.copyLocation(#wp); 
        }
    ;

wordpartition_workhorse
    :   opcodefield (COMMA! opcodefield)*
    ;

opcodefield
    :   id COLON^ nbit
    ;
    

instructionformat_definition
    :   TOK_INSTRUCTIONFORMAT^ 
        (   OPEN_BRACE! (sep)?
            (instructionformat_association (sep)?)*
            CLOSE_BRACE!
        |   instructionformat_association
        )
    ;

instructionformat_association
    :   id 
        EQUALS^ 
        instructionformat 
    ;

instructionformat
    :   OPEN_SQUARE^ 
        (opcodepartition | opcodepartition_ref)
        PIPE
        (inst_arg (COMMA! inst_arg)*)? 
        (   PIPE
            string_literal // program code for common fields
        )?
        CLOSE_SQUARE! 
    ;

opcodepartition_ref
    :   i:id 
        {   ## = #([OPCODEPARTITION_REF, "OPCODEPARTITION_REF"], i); 
            ##.copyLocation(#i); }
    ;

inst_arg
    : id COLON^ nbit SLASH! id
    ;


instructiondefinition_definition
    :   TOK_IDEF^
        (   OPEN_BRACE! (sep)?
            (instructiondefinition_association (sep)?)*
            CLOSE_BRACE!
        |   instructiondefinition_association
        )
    ;


instructiondefinition_association
    :   id  // instruction name
        EQUALS^
        instructiondefinition
    ;
instructiondefinition
    :   OPEN_SQUARE^
        (instructionformat | instructionformat_ref)  // instruction format specifier
        (   PIPE
            string_literal
        )?
        CLOSE_SQUARE!
    ;




highlight_definition
    :   TOK_HIGHLIGHT^
        (   OPEN_BRACE! (sep)?
            (highlight (sep)?)*
            CLOSE_BRACE!
        |   highlight
        )
    ;

highlight
    :   OPEN_SQUARE^
        string_literal // component context
        PIPE 
        string_literal // wsdl
        (   PIPE
            string_literal // color literal
        )?
        CLOSE_SQUARE!
    ;








instructionformat_ref!
    :   i:id 
        {   ## = #([INSTRUCTIONFORMAT_REF, "INSTRUCTIONFORMAT_REF"], i); 
            ##.copyLocation(#i);
        }
    ;



// make sure multiple labels can be given consecutively
command
    :   command_spec
    |   prefix_spec (prefix_spec)* (command_spec)?
    ;

prefix_spec
    :   label
    |   offset_spec
    ;

command_spec
    :   instruction
    |   variabledecl2
    ;
        


instruction!
    : mnem:id arg:arglist 
        {   ## = #([INSTRUCTION, "INSTRUCTION"],mnem,arg); 
            ##.copyLocation(#mnem);    
        }
    ;

arglist 
    :   (arg (COMMA! arg)*)?
    ;


/* we do not 'treeify' the SLASH, in order to have simple 
   instruction highlighting in the program editor
*/
arg 
    :   (   id
        |   nbit)
        (SLASH id)?
    ;
        

label!
    :   i:id COLON 
        {   ##=#([LABEL, "LABEL"], i); 
            ##.copyLocation(#i); 
        }
    ;

offset_spec!
    :   n:nbit COLON 
        {   ##=#([OFFSET_SPEC, "OFFSET_SPEC"], n); 
            ##.copyLocation(#n); 
        }
    ;

variabledecl
    :   variabledecl2
    |   offset_spec (offset_spec)* (variabledecl2)?
    ;



variabledecl2 
    :   WORD^
        (   (id (COMMA! id)*)? (nbit)?  // memory-cell declarations, with optional names and initialisation 
        )
    ;


string_literal
    :   STRING_LITERAL
    ;

id
    : ID
    ;


nbit
    : NBIT
    ;



class L extends Lexer;
options {
    charVocabulary = '\u0000'..'\u00ff'; // we're working 8 bit ascii
}

tokens {
TOK_SETTING = ".setting";
TOK_AUTOCAST = ".autocast";
TOK_CONSTANT = ".constant";
TOK_INSTRUCTIONFORMAT = ".instructionformat";
TOK_IDEF = ".instructiondefinition";
TOK_HIGHLIGHT = ".highlight";
TOP_OPCODEPARTITION=".opcodepartition";
WORD = "WORD";
}

{
    TokenStreamSelector selector;
    boolean isSubLexer;
    File parent; // directory to base include file names upon

    private void init(TokenStreamSelector tss, boolean isSubLexer, File parent) {
        this.selector = tss;
        this.isSubLexer = isSubLexer;
        this.parent = parent;
        setTokenObjectClass(WToken.class.getName());

        // we need correct tab size for text highlighting, where we
        // use the convention that every symbol (including tab) 
        // has unit length
        setTabSize(1);
    }

    public L(InputStream is, TokenStreamSelector tss, boolean isSubLexer, File parent) {
        this(is);
        init(tss, isSubLexer, parent);
    }

    public L(Reader is, TokenStreamSelector tss, boolean isSubLexer, File parent) {
        this(is);
        init(tss, isSubLexer, parent);
    }

    public Token makeToken(int _ttype) {
        Token t = super.makeToken(_ttype);
        t.setFilename(getFilename());
        return t;
    }


/**********  first half of the include file mechanism  *************/

	public void uponEOF() throws TokenStreamException, CharStreamException {
		if ( isSubLexer ) {
			// don't allow EOF until main lexer.  Force the
			// selector to retry for another token.
			selector.pop(); // return to old lexer/stream
			selector.retry();
		}
	}

}


protected
DEC_DIGIT
options { paraphrase = "a decimal digit"; }
    : ('0'..'9')
    ;

protected
HEX_DIGIT
options { paraphrase = "a hexadecimal digit"; }
    : ('0'..'9' | 'a'..'f' | 'A'..'F')
    ;

protected
BIN_DIGIT
options { paraphrase = "a binary digit"; }
    : ('0'..'1')
    ;


protected
LETTER          
    :       ('a'..'z' | 'A'..'Z' | '$' | '_')
    ;

NBIT
options { paraphrase = "a number constant"; }
    :   ( '0'
            (       'x' (HEX_DIGIT)+
            |       'b' (BIN_DIGIT)+
            |       'd' (DEC_DIGIT)+
            |       (DEC_DIGIT)*
            )
        | '1'..'9' (DEC_DIGIT)*
        )
    ;

STRING_LITERAL
options { paraphrase = "a string constant"; }
    :   '"'! 
        (   
            ('\\' ('\r'|'\n')) => LINE_CONTINUATION!
        |    ~('"')
        )* 
        '"'!
    ;


ID
options { paraphrase = "an identifier"; }
    :       LETTER (LETTER | DEC_DIGIT)*
    ;


OPEN_PAREN    
    :       '('
    ;

CLOSE_PAREN
    :       ')'
    ;

OPEN_BRACE
    :   '{'
    ;

CLOSE_BRACE
    :   '}'
    ;

OPEN_SQUARE
    :   '['
    ;

CLOSE_SQUARE
    :   ']'
    ;

//OP              :       '-' | '+' | '*' | '&' | EQUALS | '^' | '%' | '!' | '~' | '[' | ']' | '{' | '}' | '<' | '>' | '?'
//                ;
EQUALS  
    :
        '='
    ;

INTO
    :
        "->"
    ;


COLON   
    :   ':'
    ;

SEMICOL 
options { paraphrase = "a semicolon"; }
    :   ';'
    ;

SLASH
options { paraphrase = "a slash"; }
    :   '/'
    ;

COMMA   
    :   ','
    ;

PRAGMA  
    :   '.' (LETTER)+
    ;

PIPE    
    :       '|'
    ;

//DIRSEP  
//    :       '/' | '\\'
//    ;



/* NOTE: the split in three cases is not really 
necessary when this is a token by iteself, but it is 
when this rule is used as a nested sublexer rule */

NL
options{ paraphrase = "new line"; }            
    :   (   options { generateAmbigWarnings = false; }
        :   '\n'            // unix
        |   '\r' '\n'       // DOS/Windows
        |   '\r'            // Macintosh
        )   { newline(); }
    ;

// whitespace
WS           
    :   ( ' ' | '\t' )
        {$setType(Token.SKIP);} //ignore this token
    ;

COMMENT
options{paraphrase = "single-line comment";}
    :   '#' (~('\n'|'\r'))*
        {$setType(Token.SKIP);} //ignore this token
    ;

/* Note: the SKIP token type only works when this is
   a proper token. When it is used in a subrule, you
   need to add ! to make it disappear from the tree */

LINE_CONTINUATION
options { paraphrase = "line continuation construction"; }
    :   '\\' NL
        {$setType(Token.SKIP);} //ignore this token
    ;



/**********  second half of the include file mechanism  *************/



INCLUDE
	:	"@include" (WS)+  fn:STRING_LITERAL
		{
		// create lexer to handle include
            String name = fn.getText();
            DataInputStream input=null;
            File included = null;
            try {
                included = FileManager.getFile(parent, name);
                FileInputStream fi = new FileInputStream(included);
                input = new DataInputStream(fi);
            }
            catch (FileNotFoundException ex) {
//                System.err.println("cannot find file "+name);
                throw new SemanticException(ex.getMessage(), getFilename(), getLine(), getColumn());
            }

            L sublexer = new L(input, selector, true, included.getParentFile());
            // make sure errors are reported in right file
            sublexer.setFilename(included.getPath());

            // you can't just call nextToken of sublexer
            // because you need a stream of tokens to
            // head to the parser.  The only way is
            // to blast out of this lexer and reenter
            // the nextToken of the sublexer instance
            // of this class.
            
            selector.push(sublexer);
            // ignore this as whitespace; ask selector to try
            // to get another token.  It will call nextToken()
            // of the new instance of this lexer.
            selector.retry(); // throws TokenStreamRetryException
		}
	;

