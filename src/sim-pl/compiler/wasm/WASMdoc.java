/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import icons.IconManager;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import log.Log_Frame;
import log.Logger;
import util.ExtensionAddingFilter;
import util.FileManager;
import about.AboutWindow;

import com.centerkey.utils.BareBonesBrowserLaunch;

public class WASMdoc extends JFrame {
	public WASMdoc() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		final WASMdoc isfgui = new WASMdoc();
		isfgui.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		util.DialogHelper.performOnEscapeKey(isfgui, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				isfgui.dispose();
			}
		});

		isfgui.pack();
		isfgui.setLocationRelativeTo(isfgui.getOwner());
		isfgui.setVisible(true);
	}

	private void jbInit() throws Exception {
		setTitle("Instruction Set Formatter");
		setIconImage(IconManager.loadImage(AboutWindow.class, "SIM-PL_mini.png"));
		jPanel1.setBorder(titledBorder1);
		titledBorder1.setTitle("Settings");
		titledBorder2.setTitle("Log");
		jTextField1.setColumns(80);
		jLabel2.setText("Component");
		jTextField2.setColumns(80);
		jLabel3.setText("XHTML");
		jTextField3.setColumns(80);
		jButton1.setText("...");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openWASM(e);
			}
		});
		jButton2.setText("...");
		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openComponent(e);
			}
		});
		jButton3.setText("...");
		jButton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openXHTML(e);
			}
		});
		jButton4.setText("Go!");
		jButton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				go(e);
			}
		});
		jPanel1.setLayout(gridBagLayout1);
		jButton5.setText("Browse XHTML");
		jButton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBrowse(e);
			}
		});
		jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));

		jPanel1.add(jTextField1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField2, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jTextField3, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		jPanel1.add(jButton1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jButton2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(jButton3, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(jPanel3, java.awt.BorderLayout.SOUTH);
		jPanel3.add(jButton4);
		jPanel3.add(jButton5);
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
		jLabel1.setText("WASM");
		getRootPane().setDefaultButton(jButton4);

		DocumentListener docl = new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {onChange();}
			public void removeUpdate(DocumentEvent e) {onChange();}
			public void changedUpdate(DocumentEvent e) {onChange();}
		};

		this.jTextField1.getDocument().addDocumentListener(docl);
		this.jTextField2.getDocument().addDocumentListener(docl);
		this.jTextField3.getDocument().addDocumentListener(docl);

		onChange();
	}

	private void onChange() {
		final File wasm = new File(jTextField1.getText());
		final File comp = new File(jTextField2.getText());
		final File xhtml = new File(jTextField3.getText());

		this.jButton4.setEnabled(wasm.exists() && comp.exists());
		this.jButton5.setEnabled(xhtml.exists());
	}

	JPanel jPanel1 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JLabel jLabel1 = new JLabel();
	JTextField jTextField1 = new JTextField();
	JLabel jLabel2 = new JLabel();
	JTextField jTextField2 = new JTextField();
	JLabel jLabel3 = new JLabel();
	JTextField jTextField3 = new JTextField();
	JButton jButton1 = new JButton();
	JButton jButton2 = new JButton();
	JButton jButton3 = new JButton();
	TitledBorder titledBorder1 = new TitledBorder("");
	TitledBorder titledBorder2 = new TitledBorder("");
	JPanel jPanel3 = new JPanel();
	JButton jButton4 = new JButton();

	ExtensionAddingFilter ff_wasm = new ExtensionAddingFilter("WASM program", ".wasm");
	ExtensionAddingFilter ff_xhtml = new ExtensionAddingFilter("XHTML file", ".xhtml");
	JButton jButton5 = new JButton();

	public void openWASM(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Open WASM", null, "Open", ff_wasm);
		if (f != null) {
			String WASM = f.getAbsolutePath();
			jTextField1.setText(WASM);

			if (f.exists() && WASM.matches(".*\\.wasm$")) {
				String stripped = WASM.replaceAll("\\.wasm$","");
				// guess component filename
				if (jTextField2.getText().length() == 0) {
					for (String ext : new String[] {".sim-pl", ".xml"}) {
						File comp = new File(stripped + ext);
						if (comp.exists()) {
							jTextField2.setText(comp.getAbsolutePath());
							break;
						}
					}
				}
				// guess output filename
				if (jTextField3.getText().length()==0) {
					jTextField3.setText(new File(stripped + ".xhtml").getAbsolutePath());
				}
			}
		}
	}

	public void openComponent(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Open Component", null, "Open", FileManager.filter_anycmp, FileManager.filter_sim_pl, FileManager.filter_xml);

		if (f != null) {
			jTextField2.setText(f.getAbsolutePath());
		}
	}

	public void openXHTML(ActionEvent e) {
		File f = FileManager.getFileManager().askForFile(this, "Save XHTML", null, "Save", ff_xhtml);

		if (f != null) {
			jTextField3.setText(f.getAbsolutePath());
		}
	}

	public void go(ActionEvent e) {
		final File wasm = new File(jTextField1.getText());
		final File comp = new File(jTextField2.getText());
		final File xhtml = new File(jTextField3.getText());

		// bail early
		if (xhtml.exists() && JOptionPane.showConfirmDialog(this,
					"File exists: " + xhtml.getPath() +".\nOverwrite?", "Overwrite question",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) !=
					JOptionPane.YES_OPTION) return;

		// good to go
		final Log_Frame   log =       new Log_Frame("Process log", null);
		final Logger      logger =    new Logger(log);
		log.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		util.DialogHelper.performOnEscapeKey(log, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				log.dispose();
			}
		});


		log.setVisible(true);

		new Thread() {
			public void run() {
				try {
					InstructionSetFormatter.main(wasm, comp, xhtml, logger);
				} catch (Exception ex) {
					logger.addParagraph(ex.toString());
				}
				onChange();
			}
		}.start();
	}

	public void goBrowse(ActionEvent e) {
		final File xhtml = new File(jTextField3.getText());
		BareBonesBrowserLaunch.openURL(xhtml.toURI().toString());
	}
}
