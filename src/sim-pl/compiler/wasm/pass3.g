/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
    import databinding.Component;
    import compiler.wasm.Context.SettingResolver; 
}

/* Pass 3
  * declares (and removes) all
    - instruction formats
    - constants
    - highlight definition
  * performs component resolution

*/


class Pass3 extends PassBase;
{
    SymbolTable id2segment;
    executer.Instance   ifor;

    public Pass3(SymbolTable id2segment, executer.Instance ifor) {
        this();
        this.id2segment = id2segment;
        this.ifor = ifor;
        this.setASTNodeClass(WAST.class.getName());
    }
    String passInfo = "Constants & instruction formats";
}

program 
    :   (   processor 
        |   memory 
        |   code 
        |   data
        )* 
    ;


processor {
    String name;
    Processor p;
}
    :   #(t:".processor" 
            name=id 
            {   p = (Processor)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   constant_declaration[p]!
            |   instructionformat_declaration[p]!
            |   highlight_declaration[p]!
            |   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

memory {
    String name;
    Memory m;
}
    :   #(t:".memory" 
            name=id 
            { m = (Memory)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   constant_declaration[m]!
            |   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

code {
    String name;
    Code c;
}
    :   #(t:".code" 
            name=id 
            {   c = (Code)id2segment.getExists(name); 
                c.resolveComponent(ifor);
            }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   constant_declaration[c]!
            |   deepcopy 
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

data {
    String name;
    Data d;
}
    :   #(t:".data" 
            name=id 
            {   d = (Data)id2segment.getExists(name); 
                d.resolveComponent(ifor);
            }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   constant_declaration[d]!
            |   deepcopy 
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }






constant_declaration!  [Context c]  {
    String name;
}
    :   #(TOK_CONSTANT 
            ( constant_association[c] )*
        )
    ;


constant_association!  [Context c]  {
    String name;
    ValueType<nBit, String> val;
}
    :   #(EQUALS
            name=t:id
            val= valuetype[c]
        )
        { c.constants.put(name, val); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


valuetype! [Context c] returns [ValueType<nBit,String> vt] {
    nBit value;
    String type = null;
}
    :   value=t:nbit
        (type=id)?
        {   if (type == null) type = c.resolveExists(SettingResolver.NUMBER_TYPE, SettingResolver.R);
            vt = new ValueType<nBit,String>(value, type);
        }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }




highlight_declaration[Context c] {
}
    :   #(TOK_HIGHLIGHT 
            (highlight[c]!)*
        )
    ;

highlight [Context c] {
    String hpl;
    String wsdl;
    String color = null;
}
    :   #(t:OPEN_SQUARE
            hpl=string_literal // component context
            PIPE
            wsdl=string_literal // wsdl expression
            (   PIPE
                color=string_literal
            )?
        )
        { c.highlights.add(new HighlightDef(ifor, hpl, wsdl,color)); }
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

