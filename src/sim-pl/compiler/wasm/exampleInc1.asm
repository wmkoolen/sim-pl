##########################################################################
#  SIM-PL 2                                                              #
#  Digital Component Discrete Event Simulator                            #
#  Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                        #
#                                                                        #
#                                                                        #
#  This file is part of SIM-PL.                                          #
#                                                                        #
#                                                                        #
#  SIM-PL is free software; you can redistribute it and/or modify        #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation; either version 2 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  SIM-PL is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#                                                                        #
#  You should have received a copy of the GNU General Public License     #
#  along with SIM-PL; if not, write to the Free Software                 #
#  Foundation, Inc., 51 Franklin St, Fifth Floor,                        #
#  Boston, MA  02110-1301  USA                                           #
#                                                                        #
#                                                                        #
#  Contact:                                                              #
#  Wouter Koolen-Wijkstra                                                #
#  wmkoolen@gmail.com                                               #
#                                                                        #
##########################################################################


@include "exampleInc2.asm"	

# toy example

.code MainCode2 :	 MIPS
.settings {
labelType = "winkywonky"
}
	
Label:	ADD $1, $2, $3
	HALT
Other:	AND $1, $2, $3 ;  MAX $1
	#fluflo
	

	.processor MIPS_try
#	.type register
	.constants {
	 $0 = 0x00/register
	}
	.instructionformat   ARITH2     OP:0d2, REGDEST:0d1, ALUSRCB:0d2, ALU:0d3, RS:0d4, RT:0d4, RD: 0d4, UNUSED:0d12 | rd:0d4/register, rs:0d4/register, rt:04/register
	.instructiondefinition SUB              ARITH2   " OP = 0b01; REGDEST = 0b1; ALUSRCB=0b00; ALU = 0x2; RS = rs; RT = rt; RD = rd; UNUSED = 0;"
	
	
	

