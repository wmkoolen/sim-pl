/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.wasm;
    import java.util.*;
    import nbit.*;
    import databinding.Component;
    import compiler.wasm.Context.SettingResolver; 
    import compiler.wasm.Context.ConstantResolver; 
}

/* Pass 4
  * declares (and removes) all
    - instruction definitions
*/


class Pass4 extends PassBase;
{
    SymbolTable id2segment;

    public Pass4(SymbolTable id2segment) {
        this();
        this.id2segment = id2segment;
        this.setASTNodeClass(WAST.class.getName());
    }

    String passInfo = "Instruction definitions";
}

program 
    :   (   processor 
        |   memory 
        |   code 
        |   data
        )* 
    ;


processor {
    String name;
    Processor p;
}
    :   #(".processor" 
            name=t:id 
            {   p = (Processor)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   instructiondefinition_declaration[p]!
            |   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


memory {
    String name;
    Memory m;
}
    :   #(".memory" 
            name=t:id 
            { m = (Memory)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

code {
    String name;
    Code c;
}
    :   #(".code" 
            name=t:id 
            {   c = (Code)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }


data {
    String name;
    Data d;
}
    :   #(".data" 
            name=t:id 
            {   d = (Data)id2segment.getExists(name); }
            (   options { generateAmbigWarnings=false; }  // always want first listed if possible
            :   deepcopy
            )*
        )
    ;
    exception catch [WASMException ex] { throw new SemanticException(ex.toString(), #t.getFilename(), #t.getLine(), #t.getCol()); }

