/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import nbit.nBit;

public class OperatorSymbol_Index extends OperatorSymbol implements OperatorSymbol_Infix
{
	private  final Operator_Infix infix = new Operator_Infix(10, Operator_Infix.YFX, true)
	{
		public String toString() { return "[]";}

		public Type.Access getType(Expression e1, Expression e2, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
		{
			Type.Access t1 = e1.getType(namespace);
			Type.Access t2 = e2.getType(namespace);

			if (!t1.isRValue) throw new Exception(e1 + " is not an RValue");
			if (!t2.isRValue) throw new Exception(e2 + " is not an RValue");

			if (!(t1.type instanceof Type_Array))    throw new Exception("Can only apply " + this.toString() + " to arrays");
			if (!(t2.type instanceof Type_Simple))   throw new Exception("Can only index arrays with noncomposite types");

			return new Type.Access(true, true, ((Type_Array)t1.type).type);
		}


		public Instance operate(Instance i1, Instance i2) throws Exception
		{
			if (!(i1 instanceof Instance_Array)) throw new Exception("Can only apply [] to arrays");
			if (!(i2 instanceof Instance_Simple)) throw new Exception("Can only index arrays with simple values");

			Instance_Array  ia1 = (Instance_Array) i1;
			Instance_Simple is2 = (Instance_Simple)i2;

			if (is2.getValue().compareTo(new nBit(32, ia1.type.size)) >= 0)
			{
				throw new Exception("Index " + is2.getValue() + " out of range 0 to " + ia1.type.size);
			}

			return ia1.getElement(is2.getValue().toInt());
		}

	};
	public Operator_Infix getInfixOperator() {	return infix;	}
	
	public static final OperatorSymbol_Index index = new OperatorSymbol_Index();


	private OperatorSymbol_Index()    {}

	public String toString()
	{
		return "[]";
	}
}
