/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.LinkedList;

import compiler.wisent.Expression.PrecedentedExpression;

abstract public class Operator_Prefix extends Operator
{
	static final int FX = 0;
	static final int FY = 1;

	final int asso;

    public Operator_Prefix(int precedence, int asso, boolean returnsLValue)
    {
		super(precedence, returnsLValue);
		this.asso = asso;
    }

	public Type.Access getType(Expression e, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
	{
		Type.Access t = e.getType(namespace);
		if (t.type instanceof Type_Simple) return t;
		else throw new Exception("Cannot only apply " + toString() + " to noncomposite types");
	}

	public abstract Instance operate(Instance   e) throws Exception;

	public PrecedentedExpression resolve(LinkedList<PrecedentedExpression> expressionStack) throws Exception
	{
		if (expressionStack.size() < 1) throw new Exception("Not ennough parameters specified for operator " + this);

		PrecedentedExpression e = expressionStack.removeFirst();
	    if ((asso == FX && e.precedence >= precedence) ||
		    (asso == FY && e.precedence >  precedence))  throw new Exception("Operator conflict");

	    return new PrecedentedExpression(new Expression_Operator_Prefix(this, e.e), precedence);
	}

	public void account(LinkedList<Operator> operatorStack, LinkedList<PrecedentedExpression> expressionStack)
	{
		//	while (!S_op.isEmpty() && S_op.top()->getPrecedence() < Ops[nOperator].Precedence) S_ex.push(S_op.toppop()->Resolve(S_ex));
	    //	if (Ops[nOperator].Asso == FX)
	    //	{
	    //		while (!S_op.isEmpty() && S_op.top()->getPrecedence() == Ops[nOperator].Precedence) S_ex.push(S_op.toppop()->Resolve(S_ex));
	    //	}
	    operatorStack.addFirst(this);
	}
}
