/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class Statement_If  extends Statement
{
	private final Expression e;
	private final Statement s;

    public Statement_If(Expression e, Statement s)
    {
    	assert(e != null);
    	assert(s != null);
		this.e = e;
		this.s = s;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("if (");
		s.append(e);
		s.append(") ");
		if (this.s instanceof Statement_Block) s.append('\n');
		s.append(this.s.toString());
		return s.toString();
	}

	public int execute(Scope<String,DataSource> scope) throws Exception
	{
		int returnCause = NORMAL;
		scope.open();
		Instance i = e.evaluate(scope);

		if (i instanceof Instance_Simple)
		{
			if (((Instance_Simple)i).getValue().toBoolean())     returnCause = s.execute(scope);
		}
		else
		{
			throw new Exception(e + " does not resolve to a nBit number");
		}
		scope.close();
		return returnCause;
	}


}
