/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class Namespace<iT,vT> extends Scope<iT, vT>
{
    public Namespace()
    {
    }

//	public String toFormattedString(NumberFormat representation)
//	{
//		StringBuffer s = new StringBuffer();
//
//		for (Iterator i = identifier2value.keySet().iterator(); i.hasNext(); )
//		{
//			Object identifier = i.next();
//			s.append(identifier);
//			s.append(" = ");
//			s.append(((DataSource)identifier2value.get(identifier)).getValue().toFormattedString(representation));
//			if (i.hasNext()) s.append(", ");
//		}
//
//		return s.toString();
//	}

	public void     reset(iT identifier, vT value) throws UndefinedIdentifierException
	{
		if (!identifier2value.containsKey(identifier)) throw new UndefinedIdentifierException(identifier);
		identifier2value.put(identifier, value);
	}


}
