/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.HashMap;
import java.util.Map;

import compiler.LocatedException;
import compiler.TokenList;

abstract public class Keyword extends LanguageElement
{
	public static final Keyword _if =       new Keyword_If();
	public static final Keyword _else =     new Keyword_Else();
	public static final Keyword _for =      new Keyword_For();
	public static final Keyword _while =    new Keyword_While();
	public static final Keyword _do =       new Keyword_Do();
	public static final Keyword _switch =   new Keyword_Switch();
	public static final Keyword _case =     new Keyword_Case();
	public static final Keyword _default =  new Keyword_Default();
	public static final Keyword _break =    new Keyword_Break();
	public static final Keyword _bit =      new Keyword_Bit();
	public static final Keyword _signed =   new Keyword_Signed();
	public static final Keyword _unsigned = new Keyword_Unsigned();
	public static final Keyword _struct =   new Keyword_Struct();

	public static final Map<String,Keyword>     keywords =  keywordMap();


	public String toString()
	{
		return this.toString();
	}

	public Statement tryStatement(Scope<String, Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		return null;
	}

	private static Map<String,Keyword> keywordMap()
	{
	    Map <String,Keyword> keywords = new HashMap<String,Keyword>();
		keywords.put(_if.       toString(),     _if);
		keywords.put(_else.     toString(),     _else);
		keywords.put(_for.      toString(),     _for);
		keywords.put(_while.    toString(),     _while);
		keywords.put(_do.       toString(),     _do);
		keywords.put(_switch.   toString(),     _switch);
		keywords.put(_case.     toString(),     _case);
		keywords.put(_default.  toString(),     _default);
		keywords.put(_break.    toString(),     _break);
		keywords.put(_bit.      toString(),     _bit);
		keywords.put(_signed.   toString(),     _signed);
		keywords.put(_unsigned. toString(),     _unsigned);
		keywords.put(_struct.   toString(),     _struct);

		return keywords;
	}
}
