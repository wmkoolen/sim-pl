/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;
import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;

public class Keyword_If extends Keyword
{
	public Statement tryStatement(Scope<String, Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		tokens.removeFirst(); // remove this keyword

		globalTypeSpace.open();
		if (tokens.size() == 0) throw new ParseException("'('");


		Token o = tokens.removeFirst();
		if (o.token != Interpunction.braceOpen) throw new ParseException("'('", o);

		Expression e = Expression.parse(globalTypeSpace, tokens);

		if (tokens.size() == 0) throw new ParseException("')'");
		o = tokens.removeFirst();
		if (o.token != Interpunction.braceClose) throw new ParseException("')'", o);

		Statement s1 = Statement.parse(globalTypeSpace, tokens);

		if (tokens.size() != 0 && tokens.getFirst().token == Keyword._else)
		{
			tokens.removeFirst();
			Statement s2 = Statement.parse(globalTypeSpace, tokens);
			return new Statement_If_Else(e, s1, s2);
		}

		globalTypeSpace.close();
		return new Statement_If(e, s1);
	}


	public String toString()
	{
		return "if";
	}

}
