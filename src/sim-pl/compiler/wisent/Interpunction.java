/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.HashMap;
import java.util.Map;


public class Interpunction extends LanguageElement
{
	public static final Interpunction   semicolon =       new Interpunction_Semicolon();
	public static final Interpunction   colon =           new Interpunction_Colon();
	public static final Interpunction   braceOpen =       new Interpunction_BraceOpen();
	public static final Interpunction   braceClose =      new Interpunction_BraceClose();
	public static final Interpunction   curlyBraceOpen =  new Interpunction_CurlyBraceOpen();
	public static final Interpunction   curlyBraceClose = new Interpunction_CurlyBraceClose();
	public static final Interpunction   squareBraceOpen = new Interpunction_SquareBraceOpen();
	public static final Interpunction   squareBraceClose =new Interpunction_SquareBraceClose();
	public static final Interpunction   period =          new Interpunction_Period();


	public static final Map<String,Interpunction> interpunctions =  interpunctionMap();

    public Interpunction()
    {
    }

	public String toString()
	{
		return this.toString();
	}

	private static Map<String,Interpunction> interpunctionMap()
	{
	    Map<String,Interpunction> interpunctions = new HashMap<String,Interpunction>();

		interpunctions.put(semicolon.       toString(), semicolon);
		interpunctions.put(colon.           toString(), colon);
		interpunctions.put(braceOpen.       toString(), braceOpen);
		interpunctions.put(braceClose.      toString(), braceClose);
		interpunctions.put(curlyBraceOpen.  toString(), curlyBraceOpen);
		interpunctions.put(curlyBraceClose. toString(), curlyBraceClose);
		interpunctions.put(squareBraceOpen. toString(), squareBraceOpen);
		interpunctions.put(squareBraceClose.toString(), squareBraceClose);
		interpunctions.put(period.          toString(), period);

		return interpunctions;
	}

}
