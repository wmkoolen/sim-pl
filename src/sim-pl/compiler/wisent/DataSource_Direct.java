/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Set;

import nbit.NumberFormat;
import nbit.nBit;

public class DataSource_Direct implements DataSource
{
	Instance instance;

	public DataSource_Direct(Instance instance)
    {
		this.instance = instance;
    }

	public String toString()
	{
		return toFormattedString(nBit.PREFIXFREE);
	}

	public String toFormattedString(NumberFormat representation)
	{
		return instance.toFormattedString(representation);
	}

    public Instance getValue()
    {
		return instance;
    }

	public void setValue(Instance value, Set<executer.Instance_Wire> wwic, Set<executer.Instance> cwic)
	{
		instance.changeInto(value);
	}

	public DataSource createDirectCopy()
    {
		return new DataSource_Direct(instance.klone());
    }

	public void setToAllOnes()
	{
		instance.setToAllOnes();
	}

	public boolean isValueEditable()
	{
		return true;
	}
}
