/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import nbit.NumberFormat;
import nbit.nBit;

public class Instance_Simple extends Instance
{
	public Type_Simple type;
	public nBit        value;

	public Instance_Simple(Type_Simple type)
	{
		this.type = type;
		this.value = new nBit(type);
	}

    public Instance_Simple(Type_Simple type, nBit value)
    {
		this.type = type;
		this.value = value.cast(type);
    }
    
    public void overwriteWith(nBit value)
    {
    	this.value.ASSIGN(value);
    }

	public Type getType() { return type; }

	public nBit getValue()
	{
		return value;
	}

	public void setValue(nBit value)
	{
		this.value = value.cast(type);
	}


	public String toFormattedString(NumberFormat representation)
	{
		return value.toString(representation, type.signed, false);
	}

	public Instance klone()
	{
		return new Instance_Simple(type, new nBit(value));
	}

	public boolean changeInto(Instance newI)
	{
		Instance_Simple newIs = (Instance_Simple)newI;
		if (value.compareTo(newIs.value) == 0) return false;
		value.ASSIGN(newIs.value);
		return true;
	}

	public void setToAllOnes()
	{
		value.ASSIGN(value.allOnes());
	}
}
