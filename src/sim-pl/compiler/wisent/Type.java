/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;


abstract public class Type extends LanguageElement
{
	public static class Access {
		public final boolean isLValue;
		public final boolean isRValue;
		public final Type type;

		public Access(boolean isLValue, boolean isRValue, Type type) {
			this.isLValue = isLValue;
			this.isRValue = isRValue;
			this.type = type;
		}

		public boolean equals(Object o) {
			Access a = (Access)o;
			return isLValue == a.isLValue &&
				   isRValue == a.isRValue &&
				   type.equals(a.type);
		}

//		public Type getType() {
//			return type;
//		}
	}

	abstract public int getSize();

	abstract public Instance    createInstance();

    public Type() {
    }

	public static Type tryType(TokenList tokens) throws LocatedException
	{

		// Test simple type with signedness specification (recognized by 'signed' or 'unsigned' keyword)
		if (tokens.size() > 0 && (tokens.getFirst().token == Keyword._signed || tokens.getFirst().token == Keyword._unsigned))
		{
			Token signedness = tokens.removeFirst();

			if (tokens.size() == 0) throw new ParseException("Number");
			Token number =     tokens.removeFirst();
			if (!(number.token instanceof Number)) throw new ParseException("Number", number);

			if (tokens.size() == 0) throw new ParseException("'bit'");
			Token bit =     tokens.removeFirst();
			if (bit.token != Keyword._bit) throw new ParseException("'bit'", number);

			try
			{
			    return new Type_Simple(((Number)number.token).value.getValue().toInt(), signedness.token == Keyword._signed);
			}
			catch (Exception ex)
			{
				throw new LocatedException(signedness.location, ex.getMessage());
			}
		}

		// Test simple type without signedness specification (recognized by 'bit' keyword)
		if (tokens.size() > 1 && tokens.getSecond().token == Keyword._bit)
		{
			Token number =     tokens.removeFirst();
			if (!(number.token instanceof Number)) throw new ParseException("Number", number);

			// We are ceratain the next token is 'bit'
			tokens.removeFirst();

			try
			{
			    return new Type_Simple(((Number)number.token).value.getValue().toInt(), false);
			}
			catch (Exception ex)
			{
				throw new LocatedException(number.location, ex.getMessage());
			}
		}

		// Test array type (recognized by opening square brace)

		if (tokens.size() > 0 && tokens.getFirst().token == Interpunction.squareBraceOpen)
		{
			Token sqbo = tokens.removeFirst(); // remove '['

			if (tokens.size() == 0) throw new ParseException("Number");
			Token size =     tokens.removeFirst();
			if (!(size.token instanceof Number)) throw new ParseException("Number", size);

			if (tokens.size() == 0) throw new ParseException(Interpunction.squareBraceClose);
			Token sqbc =     tokens.removeFirst();
			if (sqbc.token != Interpunction.squareBraceClose) throw new ParseException(Interpunction.squareBraceClose, sqbc);

			Type t = Type.parse(tokens);
			try
			{
			    return new Type_Array( ((Number)size.token).value.getValue().toInt(), t);
			}
			catch (Exception ex)
			{
				throw new LocatedException(sqbo.location, ex.getMessage());
			}
		}


		// Test struct type (recognized by 'struct' keyword)
		{
			if (tokens.size() > 0 && tokens.getFirst().token == Keyword._struct)
			{
				Token struct = tokens.removeFirst(); // remove 'struct'

				if (tokens.size() == 0) throw new ParseException(Interpunction.curlyBraceOpen);
	    		Token cbo =     tokens.removeFirst();
		    	if (cbo.token != Interpunction.curlyBraceOpen) throw new ParseException(Interpunction.curlyBraceOpen, cbo);

				try
				{
					Type_Struct ts = new Type_Struct();

	    			while (tokens.size() > 0 && tokens.getFirst().token != Interpunction.curlyBraceClose)
		    		{
			    		Type        t = Type.parse(tokens);

						if (tokens.size() == 0) throw new ParseException("Identifier");
	            		Token id =     tokens.removeFirst();
		            	if (!(id.token instanceof Identifier)) throw new ParseException("Identifier", id);

						if (tokens.size() == 0) throw new ParseException(Interpunction.semicolon);
	            		Token sc =     tokens.removeFirst();
		            	if (sc.token != Interpunction.semicolon) throw new ParseException(Interpunction.semicolon, sc);

						ts.set(((Identifier)id.token).name, new Type.Access(true, true, t));
	    			}

		    		if (tokens.size() == 0) throw new ParseException(Interpunction.curlyBraceClose);
	    	    	Token cbc =     tokens.removeFirst();
		    	    if (cbc.token != Interpunction.curlyBraceClose) throw new ParseException(Interpunction.curlyBraceClose, cbc);

					return ts;
			    }
				catch (Exception ex)
				{
					throw new LocatedException(struct.location, ex.getMessage());
				}
			}
		}


		return null;
	}


	public static Type parse(TokenList tokens) throws compiler.LocatedException
	{
		if (tokens.size() == 0) throw new ParseException("type");

		Type t = tryType(tokens);

		if (t == null) throw new ParseException("type", tokens.getFirst());

		return t;
	}


}
