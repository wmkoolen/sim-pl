/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.LinkedList;

import compiler.wisent.Expression.PrecedentedExpression;

abstract public class Operator_Infix extends Operator
{
	static final int XFX = 0;
	static final int XFY = 1;
	static final int YFX = 2;

	final int asso;

    public Operator_Infix(int precedence, int asso, boolean returnsLValue)
    {
		super(precedence, returnsLValue);
		this.asso = asso;
    }


	/** default access type: take simple rvalues into rvalue of first type */
	public Type.Access getType(Expression e1, Expression e2, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
	{
		Type.Access t1 = e1.getType(namespace);
		Type.Access t2 = e2.getType(namespace);
		if (!t1.isRValue) throw new Exception(e1 + " is not an R-value");
		if (!t2.isRValue) throw new Exception(e2 + " is not an R-value");
		if (t1.type instanceof Type_Simple && t2.type instanceof Type_Simple) return new Type.Access(false, true, t1.type);
		else throw new Exception("Cannot only apply " + toString() + " to noncomposite types");
	}

	public abstract Instance operate(Instance e1,   Instance e2) throws Exception;

	public PrecedentedExpression resolve(LinkedList<PrecedentedExpression> expressionStack) throws Exception
	{
		if (expressionStack.size() < 2) throw new Exception("Not ennough parameters specified for operator " + this);

		PrecedentedExpression e2 = expressionStack.removeFirst();
		PrecedentedExpression e1 = expressionStack.removeFirst();

	    if ((asso == XFX && e1.precedence >= precedence) ||
		    (asso == YFX && e1.precedence >  precedence))  throw new Exception("Operator conflict");

	    if ((asso == XFX && e2.precedence >= precedence) ||
		    (asso == XFY && e2.precedence >  precedence))  throw new Exception("Operator conflict");

	    return new PrecedentedExpression(new Expression_Operator_Infix(e1.e, this, e2.e), precedence);
	}


	public void account(LinkedList<Operator> operatorStack, LinkedList<PrecedentedExpression> expressionStack) throws Exception
	{
	    while (operatorStack.size() > 0 && operatorStack.getFirst().precedence < precedence)
		{
			expressionStack.addFirst(operatorStack.removeFirst().resolve(expressionStack));
		}

	    if (asso == YFX)
	    {
		    while (operatorStack.size() > 0 && operatorStack.getFirst().precedence == precedence)
			{
				expressionStack.addFirst(operatorStack.removeFirst().resolve(expressionStack));
			}
	    }

		operatorStack.addFirst(this);
	}
}
