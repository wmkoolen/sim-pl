/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.HashMap;
import java.util.Map;

import compiler.LocatedException;
import compiler.ParseLocation;

public class OperatorSymbol
{
	public static final OperatorSymbol plus =             new OperatorSymbol_Plus();
	public static final OperatorSymbol minus =            new OperatorSymbol_Minus();
	public static final OperatorSymbol multiply =         new OperatorSymbol_Multiply();
	public static final OperatorSymbol multiplyS =        new OperatorSymbol_Multiply_Signed();
	public static final OperatorSymbol divide =           new OperatorSymbol_Divide();
	public static final OperatorSymbol divideS =          new OperatorSymbol_Divide_Signed();
	public static final OperatorSymbol remainder =        new OperatorSymbol_Remainder();
	public static final OperatorSymbol remainderS =       new OperatorSymbol_Remainder_Signed();

	public static final OperatorSymbol plusplus =         new OperatorSymbol_PlusPlus();
	public static final OperatorSymbol minmin =           new OperatorSymbol_MinMin();

	public static final OperatorSymbol and_bin =          new OperatorSymbol_And_Bin();
	public static final OperatorSymbol or_bin =           new OperatorSymbol_Or_Bin();

	public static final OperatorSymbol and_bit =          new OperatorSymbol_And_Bit();
	public static final OperatorSymbol or_bit =           new OperatorSymbol_Or_Bit();
	public static final OperatorSymbol xor_bit =          new OperatorSymbol_Xor_Bit();

	public static final OperatorSymbol nequals =          new OperatorSymbol_Nequals();
	public static final OperatorSymbol equals =           new OperatorSymbol_Equals();
	public static final OperatorSymbol smallerequal =     new OperatorSymbol_SmallerEqual();
	public static final OperatorSymbol smaller =          new OperatorSymbol_Smaller();
	public static final OperatorSymbol largerequal =      new OperatorSymbol_LargerEqual();
	public static final OperatorSymbol larger =           new OperatorSymbol_Larger();

	public static final OperatorSymbol shiftright =       new OperatorSymbol_ShiftRight();
	public static final OperatorSymbol shiftleft =        new OperatorSymbol_ShiftLeft();

	public static final OperatorSymbol not_bin =          new OperatorSymbol_Not_Bin();
	public static final OperatorSymbol not_bit =          new OperatorSymbol_Not_Bit();

	public static final OperatorSymbol assignment =       new OperatorSymbol_Assignment();


	public static final Map<String, OperatorSymbol> operators = operatorMap();


	public static OperatorSymbol get(String operatorStr, ParseLocation loc) throws LocatedException
	{
		OperatorSymbol  operatorSym = operators.get(operatorStr);
		if (operatorSym == null) throw new LocatedException(loc, "Operator expected");
		return operatorSym;
	}

	private static Map<String, OperatorSymbol> operatorMap()
	{
		Map<String, OperatorSymbol> operators = new HashMap<String,OperatorSymbol>();
		operators.put("+",              plus);
		operators.put("-",              minus);
		operators.put("*",              multiply);
		operators.put("*S",             multiplyS);
		operators.put("/",              divide);
		operators.put("/S",             divideS);
		operators.put("%",              remainder);
		operators.put("%S",             remainderS);

		operators.put("++",             plusplus);
		operators.put("--",             minmin);

		operators.put("&&",             and_bin);
		operators.put("||",             or_bin);

		operators.put("&",              and_bit);
		operators.put("|",              or_bit);
		operators.put("^",              xor_bit);

		operators.put("!=",             nequals);
		operators.put("==",             equals);
		operators.put("<=",             smallerequal);
		operators.put("<",              smaller);
		operators.put(">=",             largerequal);
		operators.put(">",              larger);

		operators.put(">>",             shiftright);
		operators.put("<<",             shiftleft);

		operators.put("!",              not_bin);
		operators.put("~",              not_bit);

		operators.put("=",              assignment);

		return operators;
	}

}
