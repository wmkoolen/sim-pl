/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;
import nbit.NumberFormat;
import nbit.ZeroBitException;
import nbit.nBit;
import util.ExceptionHandler;

public class Instance_Array extends Instance
{
	Type_Array      type;
	Instance []     elements;

    public Instance_Array(Type_Array type)
    {
		this.type = type;

		elements = new Instance[type.size];
		for (int i = 0; i < elements.length; i++)
		{
			elements[i] = type.type.createInstance();
		}
    }

	private Instance_Array(Instance_Array a)
	{
		this.type = a.type;

		elements = new Instance[a.elements.length];
		for (int i = 0; i < elements.length; i++)
		{
			elements[i] = a.elements[i].klone();
		}
	}

	public void overwriteWith(nBit value)
	{
		int bitsPerElem = type.type.getSize();
		
		try
		{
			for (int i = 0; i < elements.length; i++)
			{
				nBit slice = value.cast(bitsPerElem);
				elements[i].overwriteWith(slice);
				value = value.SHIFT_RIGHT(bitsPerElem);
			}
		}
		catch (ZeroBitException e)
		{
			// impossible
			assert false;
		}
	}

	public int size() { return elements.length; }

	public Type getType() { return type; }

	public Instance getElement(int index)
	{
		return elements[index];
	}

	public void setElement(int index, Instance i) throws Exception
	{
		if (!(i.getType().equals(type.type))) throw new Exception("Invalid Type: received " + i.getType() + ", expected " + type.type);
		elements[index] = i;
	}

	public String toFormattedString(NumberFormat representation)
	{
		StringBuffer b = new StringBuffer();
		b.append("{");

		for (int i= 0; i < elements.length; i++)
		{
			if (i != 0) b.append(", ");
			b.append(elements[i].toFormattedString(representation));
		}

		b.append("}");

		return b.toString();
	}

	public Instance klone()
	{
		return new Instance_Array(this);
	}

	public boolean changeInto(Instance newI)
	{
		if (!(newI instanceof Instance_Array)) throw new ExceptionHandler("You cannot assign a non-array value to an array");
		Instance_Array ia = (Instance_Array)newI;
		if (!ia.type.equals(this.type)) throw new ExceptionHandler("Arrays are not of the same type");
		if (ia.elements.length != this.elements.length) throw new ExceptionHandler("Arrays do not have the same size");

		boolean changed = false;

		for (int i = 0; i < elements.length; i++)
		{
			changed |= elements[i].changeInto(ia.elements[i]);
		}

		return changed;
	}

	public void setToAllOnes()
	{
		for (int i = 0; i < elements.length; i++)
		{
			elements[i].setToAllOnes();
		}
	}

	public nBit getValue()
	{
		try
		{
			nBit res = new nBit(type.getSize());
			int elemsize = type.type.getSize();
			for (int i = elements.length - 1; i >= 0; i--)
			{
				res = res.SHIFT_LEFT(elemsize);
				res = res.OR_BIT(elements[i].getValue());
			}
			return res;
		} 
		catch (ZeroBitException e)
		{
			// impossible
			assert false;
			return null;
		}
	}
}