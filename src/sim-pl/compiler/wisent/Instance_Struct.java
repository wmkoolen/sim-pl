/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import nbit.NumberFormat;
import nbit.ZeroBitException;
import nbit.nBit;
import util.ExceptionHandler;

public class Instance_Struct extends Instance
{
	Type_Struct     type;
	Instance []     elements;

	public Instance_Struct(Type_Struct type)
    {
		this.type = type;
		elements = new Instance[type.types.size()];

		for (int i = 0; i < elements.length; i++)
		{
			Type t =        type.types.get(i).type;
			elements[i] =   t.createInstance();
		}
    }

	private Instance_Struct(Instance_Struct s)
	{
		this.type = s.type;
		elements = new Instance[s.elements.length];
		for (int i = 0; i < elements.length; i++)
		{
			elements[i] = s.elements[i].klone();
		}
	}
	
	public void overwriteWith(nBit value)
	{
		try
		{
			for (int i = 0; i < elements.length; i++)
			{
				Type elemtype = type.types.get(i).type;
				int elemBits = elemtype.getSize();
				nBit slice = value.cast(elemBits);
				elements[i].overwriteWith(slice);
				value = value.SHIFT_RIGHT(elemBits);
			}
		} 
		catch (ZeroBitException e)
		{
			// impossible
			assert false;
		}
	}

	public Type getType() { return type; }

	public Instance getMember(Identifier identifier)
	{
		return elements[type.identifier2index.get(identifier.name).intValue()];
	}

	public String toFormattedString(NumberFormat representation)
	{
		StringBuffer s = new StringBuffer();
		s.append("{");
		for (int i = 0; i < this.elements.length; i++)
		{
			if (i != 0) s.append(", ");
			s.append(elements[i].toFormattedString(representation));
		}
		s.append("}");
		return s.toString();
	}

	public Instance klone()
	{
		return new Instance_Struct(this);
	}

	public boolean changeInto(Instance newI)
	{
		throw new ExceptionHandler("Method not implemented yet: Instance_Struct:changeInto");
	}

	public void setToAllOnes()
	{
		for (int i = 0; i < elements.length; i++)
		{
			elements[i].setToAllOnes();
		}
	}
	
	public nBit getValue()
	{
		try
		{
			nBit res = new nBit(type.getSize());
			for (int i = elements.length - 1; i >= 0; i--)
			{
				res = res.SHIFT_LEFT(type.types.get(i).type.getSize());
				res = res.OR_BIT(elements[i].getValue());
			}
			return res;
		}
		catch (ZeroBitException e)
		{
			// impossible
			assert false;
			return null;
		}
	}
}
