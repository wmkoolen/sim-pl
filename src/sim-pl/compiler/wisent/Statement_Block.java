/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.LinkedList;
import java.util.List;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.TokenList;

public class Statement_Block extends Statement
{
	private final Statement [] statements;

    public Statement_Block(Statement [] statements)
    {
		this.statements = statements;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("{\n");
		for (Statement st : statements)
		{
			s.append(st.toString());
			s.append("\n");
		}
		s.append("}");
		return s.toString();
	}


	public static Statement parse(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		globalTypeSpace.open();
		LinkedList<Statement> statements = new LinkedList<Statement>();

		while(true)
		{
			if (tokens.size() == 0) throw new ParseException("Statement");
			if (tokens.getFirst().token == Interpunction.curlyBraceClose)
			{
				tokens.removeFirst();
				break;
			}
			statements.addLast(Statement.parse(globalTypeSpace, tokens));
		}

		globalTypeSpace.close();
		return new Statement_Block(statements.toArray(new Statement[statements.size()]));
	}

	public int execute(Scope<String,DataSource> scope) throws Exception
	{
		int returnCause = NORMAL;

		scope.open();
		for (Statement stm : statements)
		{
			if ( stm.execute(scope) == BREAK) { returnCause = BREAK; break; }
		}
		scope.close();
		return returnCause;
	}
}
