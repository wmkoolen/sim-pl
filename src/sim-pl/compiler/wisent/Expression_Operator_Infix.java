/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class Expression_Operator_Infix extends Expression
{
	Expression e1;
	Operator_Infix o;
	Expression e2;


    public Expression_Operator_Infix(Expression e1, Operator_Infix o, Expression e2)
    {
		this.e1 =   e1;
		this.o =    o;
		this.e2 =   e2;
    }

	public String toString()
	{
		return "(" + e1.toString() + " " + o.toString() + " " + e2.toString() + ")";
	}


	public Type.Access getType(IdentifierValueMapping<String,Type.Access> namespace) throws Exception
	{
		return o.getType(e1, e2, namespace);
	}



	public Instance evaluate(IdentifierValueMapping<String, DataSource> namespace) throws Exception
	{
		Instance i1 = e1.evaluate(namespace);
		Instance i2 = e2.evaluate(namespace);

	    return o.operate(i1, i2);
	}
}
