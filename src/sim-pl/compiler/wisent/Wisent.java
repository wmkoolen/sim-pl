/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import util.MyFormatter;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.ParseLocation;
import compiler.TokenList;


public class Wisent implements compiler.Compiler
{
	public static final Wisent compiler = new Wisent();

	Wisent() {}

	final boolean DEBUG = false;

 	Statement parse(Scope<String,Type.Access> globalTypespace, TokenList tokens) throws LocatedException
	{
		Statement s = Statement.parse(globalTypespace, tokens);
		if (tokens.size() != 0) throw new ParseException(null, tokens.getFirst());
		return s;
	}

	Expression parseExpression(Scope<String, Type.Access> globalTypespace, TokenList tokens) throws LocatedException
	{
		Expression e = Expression.parse(globalTypespace, tokens);
		if (tokens.size() != 0) throw new ParseException(null, tokens.getFirst());
		return e;
	}

//	Expression parseExpression(String code) throws LocatedException {
//		return parseExpression(new Scope<String,Type.Access>(), tokenize(code));
//	}

// 	Statement parse(String code) throws LocatedException {
//		return parse(new Scope<String,Type.Access>(), tokenize(code));
//	}

 	public Statement parse(Type_Struct globalTypespaceStruct, String code) throws LocatedException
	{
		return parse(globalTypespaceStruct, tokenize(code));
	}

	public Expression parseExpression(Type_Struct globalTypespaceStruct, String code) throws LocatedException
	{
		return parseExpression(globalTypespaceStruct, tokenize(code));
	}


 	public Statement parse(Type_Struct globalTypespaceStruct, TokenList tokens) throws LocatedException
	{
		Scope<String,Type.Access> globalTypespace = globalTypespaceStruct.createTypeScope();
		return parse(globalTypespace, tokens);
	}

	public Expression parseExpression(Type_Struct globalTypespaceStruct, TokenList tokens) throws LocatedException
	{
		Scope<String,Type.Access> globalTypespace = globalTypespaceStruct.createTypeScope();
		return parseExpression(globalTypespace, tokens);
	}



	private static       int num = 0;
	private static final int SPACE =            1 << (num++);
	private static final int IDENTIFIERFIRST =  1 << (num++);
	private static final int IDENTIFIERLAST =   1 << (num++);
	private static final int NUMBERFIRST =      1 << (num++);
	private static final int NUMBERLAST =       1 << (num++);
	private static final int OPERATORFIRST =    1 << (num++);
	private static final int OPERATORLAST =     1 << (num++);
	private static final int INTERPUNCTION =    1 << (num++);
	private static final int COMMENTSECOND =    1 << (num++);

	private static final int charIs[] =
{
/* 0 */  0,
/* 1 */  0,
/* 2 */  0,
/* 3 */  0,
/* 4 */  0,
/* 5 */  0,
/* 6 */  0,
/* 7 */  0,
/* 8 */  0,
/* 9  '\t'*/  SPACE,
/* 10 '\n'*/  SPACE,
/* 11 */  0,
/* 12 */  0,
/* 13 '\r'*/  SPACE,
/* 14 */  0,
/* 15 */  0,
/* 16 */  0,
/* 17 */  0,
/* 18 */  0,
/* 19 */  0,
/* 20 */  0,
/* 21 */  0,
/* 22 */  0,
/* 23 */  0,
/* 24 */  0,
/* 25 */  0,
/* 26 */  0,
/* 27 */  0,
/* 28 */  0,
/* 29 */  0,
/* 30 */  0,
/* 31 */  0,
/* 32 = ' ' */  SPACE,
/* 33 = '!' */  OPERATORFIRST,
/* 34 = '"' */  0,
/* 35 = '#' */  0,
/* 36 = '$' */  0,
/* 37 = '%' */  OPERATORFIRST,
/* 38 = '&' */  OPERATORFIRST | OPERATORLAST,
/* 39 = ''' */  0,
/* 40 = '(' */  INTERPUNCTION,
/* 41 = ')' */  INTERPUNCTION,
/* 42 = '*' */  OPERATORFIRST | COMMENTSECOND,
/* 43 = '+' */  OPERATORFIRST | OPERATORLAST,
/* 44 = ',' */  0,
/* 45 = '-' */  OPERATORFIRST | OPERATORLAST,
/* 46 = '.' */  INTERPUNCTION,
/* 47 = '/' */  OPERATORFIRST | COMMENTSECOND,
/* 48 = '0' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 49 = '1' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 50 = '2' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 51 = '3' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 52 = '4' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 53 = '5' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 54 = '6' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 55 = '7' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 56 = '8' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 57 = '9' */  NUMBERFIRST | NUMBERLAST | IDENTIFIERLAST,
/* 58 = ':' */  INTERPUNCTION,
/* 59 = ';' */  INTERPUNCTION,
/* 60 = '<' */  OPERATORFIRST | OPERATORLAST,
/* 61 = '=' */  OPERATORFIRST | OPERATORLAST,
/* 62 = '>' */  OPERATORFIRST | OPERATORLAST,
/* 63 = '?' */  0,
/* 64 = '@' */  0,
/* 65 = 'A' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 66 = 'B' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 67 = 'C' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 68 = 'D' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 69 = 'E' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 70 = 'F' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 71 = 'G' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 72 = 'H' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 73 = 'I' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 74 = 'J' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 75 = 'K' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 76 = 'L' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 77 = 'M' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 78 = 'N' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 79 = 'O' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 80 = 'P' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 81 = 'Q' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 82 = 'R' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 83 = 'S' */  IDENTIFIERFIRST | IDENTIFIERLAST | OPERATORLAST,
/* 84 = 'T' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 85 = 'U' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 86 = 'V' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 87 = 'W' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 88 = 'X' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 89 = 'Y' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 90 = 'Z' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 91 = '[' */  INTERPUNCTION,
/* 92 = '\' */  0,
/* 93 = ']' */  INTERPUNCTION,
/* 94 = '^' */  OPERATORFIRST,
/* 95 = '_' */  IDENTIFIERLAST,
/* 96 = '`' */  0,
/* 97 = 'a' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 98 = 'b' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 99 = 'c' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 100 = 'd' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 101 = 'e' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 102 = 'f' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 103 = 'g' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 104 = 'h' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 105 = 'i' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 106 = 'j' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 107 = 'k' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 108 = 'l' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 109 = 'm' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 110 = 'n' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 111 = 'o' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 112 = 'p' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 113 = 'q' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 114 = 'r' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 115 = 's' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 116 = 't' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 117 = 'u' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 118 = 'v' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 119 = 'w' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 120 = 'x' */  IDENTIFIERFIRST | IDENTIFIERLAST | NUMBERLAST,
/* 121 = 'y' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 122 = 'z' */  IDENTIFIERFIRST | IDENTIFIERLAST,
/* 123 = '{' */  INTERPUNCTION,
/* 124 = '|' */  OPERATORFIRST | OPERATORLAST,
/* 125 = '}' */  INTERPUNCTION,
/* 126 = '~' */  OPERATORFIRST,
/* 127 */  0
};

	private static final int S_FREE =               0;
	private static final int S_IDENTIFIER =         1;
	private static final int S_OPERATOR =           2;
	private static final int S_NUMBER =             3;
	private static final int S_AFTER =              4;
	private static final int S_COMMENT_ONELINE =    5;
	private static final int S_COMMENT_MULTILINE =  6;




	public TokenList tokenize(String s) throws LocatedException
	{
		TokenList l = new TokenList();

		s = s + '\0';

		ParseLocation start =   new ParseLocation(1, 0, 0, s);  // Lines always count one-based, while columns count zero based
		ParseLocation current = start.klone();

		int state =     0;
		char c;
		int length = s.length();

		while (start.index != length)
		{
			c = current.get();
			//System.out.println("state: " + state + " start: " + start + " current: " + current + " s: " + s.substring(start, current) + " c: " + c + "(" + (int)c + ")");

			switch (state)
			{
				case S_FREE:
					if (c == '\0')
					{
						current.advance();
						start = current.klone();
						continue;
					}
				    else if ((charIs[c] & SPACE) != 0)
					{
						current.advance();
						start = current.klone();
						continue;
					}
					else if ((charIs[c] & IDENTIFIERFIRST) != 0)
					{
						state = S_IDENTIFIER;
					}
					else if ((charIs[c] & NUMBERFIRST) != 0)
					{
						 state = S_NUMBER;
					}
					else if (c == '/' && current.index < length && (charIs[current.getNext()] & COMMENTSECOND) != 0)
					{
						current.advance();
						c = current.get();
						if (c == '*')       state = S_COMMENT_MULTILINE;
						else if (c == '/')  state = S_COMMENT_ONELINE;
						else throw new Error("Invalid comment symbol: " + MyFormatter.unformat(Character.toString(c)));
					}
					else if ((charIs[c] & OPERATORFIRST) != 0)
					{
						 state = S_OPERATOR;
					}
					else if ((charIs[c] & INTERPUNCTION) != 0)
					{
	 					 String ip = s.substring(start.index, current.index+1);
						 l.addLast(Interpunction.interpunctions.get(ip), start);
						 if (DEBUG) System.out.println("Interpunction: \"" + ip + "\"");

						 current.advance();
						 start = current.klone();
						 continue;
					}
					else
					{
						throw new LocatedException(start, "Expected: Valid token start, found \'" + c + "\'");
					}
				break;

				case S_AFTER:
					if (c == '\0')
					{
						current.advance();
						start = current.klone();
						continue;
					}
				    else if ((charIs[c] & SPACE) != 0)
					{
						state = S_FREE;

						current.advance();
						start = current.klone();
						continue;
					}
					else if ((charIs[c] & OPERATORFIRST) != 0)
					{
						 state = S_OPERATOR;
					}
					else if ((charIs[c] & INTERPUNCTION) != 0)
					{
	 					String ip = s.substring(start.index, current.index+1);
						l.addLast(Interpunction.interpunctions.get(ip), start);
						if (DEBUG) System.out.println("Interpunction: \"" + s.substring(start.index, current.index+1) + "\"");
						state = S_FREE;

						current.advance();
						start = current.klone();
						continue;
					}
					else
					{
						throw new LocatedException(start, "Expected: Valid token continuation or new token, found \'" + c + "\'");
					}
				break;

				case S_IDENTIFIER:
					if ((charIs[c] & IDENTIFIERLAST) == 0)
					{
						String id = s.substring(start.index, current.index);
						Keyword kwd;
						if ((kwd = Keyword.keywords.get(id)) != null)
						{
							l.addLast(kwd, start);
							if (DEBUG) System.out.println("Keyword: \"" + id + "\"");
						}
						else
						{
							l.addLast(new Identifier(id), start);
						    if (DEBUG) System.out.println("Identifier: \"" + id + "\"");
						}
						state = S_AFTER;
						start = current.klone();
						continue;
					}
				break;

				case S_OPERATOR:
					if ((charIs[c] & OPERATORLAST) == 0 || OperatorSymbol.operators.get(s.substring(start.index, current.index+1)) == null)
					{
						String          operatorStr = s.substring(start.index, current.index);
						l.addLast(OperatorSymbol.get(operatorStr, start), start);
						if (DEBUG) System.out.println("Operator: \"" + operatorStr + "\"");
						state = S_FREE;
						start = current.klone();
						continue;
					}
				break;


				case S_NUMBER:
				    if ((charIs[c] & NUMBERLAST) == 0)
					{
						try {
						    l.addLast(new Number(s.substring(start.index, current.index)), start);
						} catch (Exception ex) {
							throw new LocatedException(start, ex.getMessage());
						}
						if (DEBUG) System.out.println("Number: \"" + s.substring(start.index, current.index) + "\"");
						state = S_AFTER;
						start = current.klone();
						continue;
					}
				break;


				case S_COMMENT_MULTILINE:
					if (c == '\0')
					{
						throw new LocatedException(start, "Multi line comment not closed");
					}
					else if (c == '*' && current.index < length && s.charAt(current.index+1) == '/')
					{
						if (DEBUG) System.out.println("Comment: \"" + s.substring(start.index+2, current.index) + "\"");

						state = S_FREE;
						current.advance();
						current.advance();
						start = current.klone();
						continue;
					}
				break;

				case S_COMMENT_ONELINE:
					if (c == '\n' || c == '\0')
					{
						if (DEBUG) System.out.println("Comment: \"" + s.substring(start.index+2, current.index) + "\"");
						state = S_FREE;
						current.advance();
						start = current.klone();
						continue;
					}
				break;
			}

			current.advance();
		}
		return l;
	}
}
