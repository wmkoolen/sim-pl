/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import util.ExceptionHandler;
import util.MyFormatter;


// Een scope mapt identifiers naar instanties van variabelen.

// scopeStack is een Stack, geimplementeerd als LinkedList met TOP als EERSTE element
// scopeStack bevat HashMaps van alle elementen die op een bepaalde scopediepte zijn ingevoegd
// gemapt naar hun oude waarde
// Een element mag niet meerdere malen op dezelfde stackdiepte worden ingevoegd!!!!


public class Scope <iT, vT> implements IdentifierValueMapping<iT, vT>
{
	final HashMap<iT, vT>            identifier2value =    new HashMap<iT, vT>();
	final LinkedList<HashMap<iT, vT>> scopeStack =         new LinkedList<HashMap<iT, vT>>();

	static final boolean DEBUG = false;

    public Scope() {
		scopeStack.addFirst(new HashMap<iT, vT>()); // Make room for the permanently contained items
    }


	public String toString()
	{
		StringBuffer s = new StringBuffer();

		boolean first = true;
		for(Map.Entry<iT, vT> entry : identifier2value.entrySet()) {
		    if (first) first = false; else s.append(", ");
		    iT identifier = entry.getKey();
		    vT value = entry.getValue();
		    s.append(identifier);
		    s.append(" = ");
		    s.append(value);
		}

		return s.toString();
	}


	public void open()
	{
		if (DEBUG) System.out.println("Scope opened");
		scopeStack.addFirst(new HashMap<iT, vT>());
	}

	public Collection<vT> getValues() {
		return identifier2value.values();
	}


	public void set(iT identifier, vT value) throws IdentifierAlreadyDefined
	{
		HashMap<iT, vT>  scopeTop =    scopeStack.getFirst();
		if (scopeTop.containsKey(identifier)) throw new IdentifierAlreadyDefined(identifier);
		vT      oldValue =      identifier2value.put(identifier, value);
		scopeTop.put(identifier, oldValue);
		if (DEBUG && scopeStack.size() != 1) System.out.println("New identifier inserted into scope: " + identifier + " changes from " + oldValue + " into " + MyFormatter.unformat(value));
	}

	public vT get(iT identifier) throws UndefinedIdentifierException
	{
		vT value = identifier2value.get(identifier);
		if (value == null) throw new UndefinedIdentifierException(identifier);
		return value;
	}

	public void close()
	{
		if (scopeStack.size() == 1) throw new ExceptionHandler("You are trying to removing the global namespace from scope. Strange action ?!?!");
		HashMap<iT, vT> lastScope = scopeStack.removeFirst();


		for(Map.Entry<iT,vT> entry : lastScope.entrySet()) {
		    iT identifier = entry.getKey();
		    vT oldValue =   entry.getValue();
		    if (DEBUG) System.out.println("Identifier fell out of local scope: " + identifier + " changes from " + MyFormatter.unformat(identifier2value.get(identifier)) + " back into " + MyFormatter.unformat(oldValue));
		    if (oldValue == null)       identifier2value.remove(identifier);
		    else                        identifier2value.put   (identifier, oldValue);
		}

		if (DEBUG) System.out.println("Scope closed");
	}
}
