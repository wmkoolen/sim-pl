/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.LinkedList;

import util.MyFormatter;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;

abstract public class Expression extends LanguageElement
{

	public abstract Instance    evaluate(IdentifierValueMapping<String, DataSource> namespace) throws Exception;
	public abstract Type.Access getType(IdentifierValueMapping<String, Type.Access> namespace) throws Exception;



	private static final int            LEFT =     0;
	private static final int            RIGHT =    1;
	private static final int            DONE =     2;
	private static final String []      STATESTR = {"LEFT", "RIGHT", "DONE"};
	private static final boolean        DEBUG = false;
	
	static class PrecedentedExpression {
		final Expression e;
		final int precedence;
		
		PrecedentedExpression(Expression e, int precedence) {
			this.e = e;
			this.precedence = precedence;
		}
	}


	public static Expression tryExpression(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		LinkedList<Operator>              operatorStack =   new LinkedList<Operator>();
		LinkedList<PrecedentedExpression> expressionStack = new LinkedList<PrecedentedExpression>();

		Token first = tokens.getFirst();

		int state = LEFT;

		if (DEBUG) System.out.println("\nTrying to parsing an Expression: " + tokens);

		while (state != DONE && tokens.size() > 0)
		{
		    if (DEBUG) System.out.println(  "Current state:    " + STATESTR[state] +    "\n" +
											"Operator stack:   " + operatorStack +      "\n" +
											"Expression stack: " + expressionStack +    "\n" +
											"Input:            " + tokens +             "\n");

			if (state == LEFT)
			{
				Token o = tokens.getFirst();
				Type t;

				if ( (t = Type.tryType(tokens)) != null)
				{
					if (tokens.size() == 0) throw new ParseException("Identifier");
					if ((o = tokens.removeFirst()).token instanceof Identifier)
					{
						try
						{
						    globalTypeSpace.set(((Identifier)o.token).name, new Type.Access(true,true,t)); // Register the identifier!
						}
						catch (IdentifierAlreadyDefined ex)
						{
							throw new LocatedException(o.location, ex.getMessage());
						}
						Expression e = new Expression_VarDec(t, (Identifier)o.token);
	    				expressionStack.addFirst(new PrecedentedExpression(e,0));
					}
					else throw new ParseException("Identifier", o);
				    state = RIGHT;
				}
				else if (o.token instanceof OperatorSymbol_Prefix)
				{
					((OperatorSymbol_Prefix)tokens.removeFirst().token).getPrefixOperator().account(operatorStack, expressionStack);
				}
				else if (o.token instanceof Expression)
				{
					assert o.token instanceof Number ||
						   o.token instanceof Identifier;
					try
					{
					    if (o.token instanceof Identifier) globalTypeSpace.get(((Identifier)o.token).name); // Check if the identifier exists!
					}
					catch (UndefinedIdentifierException ex)
					{
						throw new LocatedException(o.location, ex.getMessage());
					}
					expressionStack.addFirst(new PrecedentedExpression((Expression)tokens.removeFirst().token, 0));
					state = RIGHT;
				}
				else if (o.token  == Interpunction.braceOpen)
				{
					tokens.removeFirst();

					// Kijk of het een cast is
					t = Type.tryType(tokens);
					if (t != null)
					{
	    				if (tokens.size() == 0) throw new ParseException("')'");
		    			o = tokens.removeFirst();
			    		if (o.token != Interpunction.braceClose) throw new ParseException("')'", o);
						try
						{
						    new OperatorSymbol_Cast(t).getPrefixOperator().account(operatorStack, expressionStack);
						}
						catch ( Exception ex)
						{
							throw new LocatedException(o.location, ex.getMessage());
						}
					}
					else
					{
						Expression e = Expression.parse(globalTypeSpace, tokens);
	    				if (tokens.size() == 0) throw new ParseException("')'");
		    			o = tokens.removeFirst();
			    		if (o.token != Interpunction.braceClose) throw new ParseException("')'", o);

				    	expressionStack.addFirst(new PrecedentedExpression(e, 0));
						state = RIGHT;
					}
				}
				else state = DONE;
			}
			else // state = RIGHT
			{
				Token o = tokens.getFirst();

				if (o.token instanceof OperatorSymbol_Postfix)
				{
					try
					{
					    ((OperatorSymbol_Postfix)tokens.removeFirst().token).getPostfixOperator().account(operatorStack, expressionStack);
					}
					catch ( Exception ex)
					{
						throw new LocatedException(o.location, ex.getMessage());
					}

				}
				else if (o.token instanceof OperatorSymbol_Infix)
				{
					try
					{
					    ((OperatorSymbol_Infix)tokens.removeFirst().token).getInfixOperator().account(operatorStack, expressionStack);
					}
					catch ( Exception ex)
					{
						throw new LocatedException(o.location, ex.getMessage());
					}

					state = LEFT;
				}
				else if (o.token == Interpunction.squareBraceOpen)
				{
					tokens.removeFirst();
					Expression e = Expression.parse(globalTypeSpace, tokens);
					if (tokens.size() == 0)                 throw new ParseException("']'");
					o = tokens.removeFirst();
					if (o.token != Interpunction.squareBraceClose) throw new ParseException("']'", o);

					try
					{
					    OperatorSymbol_Index.index.getInfixOperator().account(operatorStack, expressionStack);
						expressionStack.addFirst(new PrecedentedExpression(e, 0));
	    				expressionStack.addFirst(operatorStack.removeFirst().resolve(expressionStack));
					}
					catch ( Exception ex)
					{
						throw new LocatedException(o.location, ex.getMessage());
					}
				}
				else if (o.token == Interpunction.period)
				{
					tokens.removeFirst();

    				if (tokens.size() == 0) throw new ParseException("Identifier");
	    			Token id = tokens.removeFirst();
		    		if (!(id.token instanceof Identifier)) throw new ParseException("Identifier", o);

					try
					{
					    new OperatorSymbol_Member((Identifier)id.token).getPostfixOperator().account(operatorStack, expressionStack);
					}
					catch (Exception ex)
					{
						throw new LocatedException(first.location, ex.getMessage());
					}
				}

				else state = DONE;
			}
		}

		if (expressionStack.size() == 0) return null;

		while (operatorStack.size() > 0)
		{
			if (DEBUG) System.out.println("Resolving " + operatorStack.getFirst());
			try
			{
			    expressionStack.addFirst(operatorStack.removeFirst().resolve(expressionStack));
			}
			catch (Exception ex)
			{
				throw new LocatedException(first.location, ex.getMessage());
			}
			if (DEBUG) System.out.println(  "Operator stack:   " + operatorStack + "\n" +
											"Expression stack: " + expressionStack);
		}

		if (expressionStack.size() != 1) throw new ParseException("Single expression", first);

		Expression result = expressionStack.getFirst().e;

		try
		{
			Type.Access t = result.getType(globalTypeSpace);
			if (DEBUG) System.out.println("Expression: " + MyFormatter.unformat(result) + "\n" +
										  "Type: " +       MyFormatter.unformat(t));
		}
		catch (Exception ex)
		{
			throw new LocatedException(first.location, ex.getMessage());
		}
		return result;
	}


	public static Expression parse(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		if (tokens.size() == 0) throw new ParseException("expression");

		Expression e = tryExpression(globalTypeSpace, tokens);
		if (e == null) throw new ParseException("expression", tokens.getFirst());
		return e;
	}
}
