/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class OperatorSymbol_Cast extends OperatorSymbol implements OperatorSymbol_Prefix
{
	private final Operator_Prefix prefix = new Operator_Prefix(50, Operator_Prefix.FY, false)
	{
		public String toString() { return "(" + type.toString() + ")";}

		public Type.Access getType(Expression e, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
		{
			Type.Access t = e.getType(namespace);
			if (t.type instanceof Type_Simple)
			{
				return new Type.Access(false, true, type);
			}
			else throw new Exception("Can only apply cast to noncomposite types");
		}

		public Instance operate(Instance i) throws Exception
		{
			if (i instanceof Instance_Simple)
			{
 				Instance_Simple is = (Instance_Simple)i;
				return new Instance_Simple(type, is.getValue().cast(type));
			}
			else                                throw new Exception("Can only apply cast to noncomposite types");
		}
	};

	public Operator_Prefix getPrefixOperator()	{		return prefix;	}

	final Type_Simple type;


	public OperatorSymbol_Cast(Type type) throws Exception
    {
		if (!(type instanceof Type_Simple)) throw new Exception("Cannot cast to composite types");
	    this.type = (Type_Simple)type;
    }


//	public OperatorSymbol_Cast(Type_Simple type) {
//		this.type = type;
//    }

	public String toString()
	{
		return "(" + type + ")";
	}
}
