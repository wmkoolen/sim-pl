/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;
import nbit.ZeroBitException;
import util.ExceptionHandler;


public class Type_Simple extends Type
{
	public final int         bits;
	public final boolean     signed;

	public static final String [] sgn = {"unsigned", "signed"};

    public Type_Simple(int bits, boolean signed) throws ZeroBitException
    {
		if (bits <= 0) throw new ZeroBitException(bits);
		this.bits =     bits;
		this.signed =   signed;
    }

	public boolean equals(Object o)
	{
		if (o instanceof Type_Simple)
		{
			Type_Simple ts = (Type_Simple) o;
			return ts.bits == bits && ts.signed == signed;
		}
		return false;
	}


	public String toString()
	{
		return sgn[signed ? 1 : 0] + " " + bits + " bit"; 
	}

	public Instance createInstance()
	{
		try
		{
		    return new Instance_Simple(this);
		}
		catch (Exception ex)
		{
			throw new ExceptionHandler(ex);
		}
	}
	
	public int getSize()
	{
		return bits;
	}
}
