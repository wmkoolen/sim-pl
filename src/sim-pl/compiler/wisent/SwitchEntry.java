/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Arrays;
import java.util.Iterator;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;

abstract public class SwitchEntry
{
	public abstract boolean matches(Instance i, IdentifierValueMapping<String, DataSource> namespace) throws Exception;


	private final Statement [] statements;

    public SwitchEntry(Statement [] statements)
    {
		this.statements = statements;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();

		for (Iterator i = Arrays.asList(statements).iterator(); i.hasNext(); )
		{
			s.append(((Statement)i.next()).toString());
			if (i.hasNext()) s.append('\n');
		}

		return s.toString();
	}


	public int execute(Scope<String,DataSource> scope) throws Exception
	{
		for (Statement st : statements)
		{
			if(st.execute(scope) == Statement.BREAK) return Statement.BREAK;
		}

		return Statement.NORMAL;
	}

	public static SwitchEntry parse(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		if (tokens.size() == 0) throw new ParseException("'case' or 'default'");
		Token o = tokens.removeFirst();
		     if (o.token == Keyword._case)        return SwitchEntry_Case.parse(globalTypeSpace, tokens);
		else if (o.token == Keyword._default)     return SwitchEntry_Default.parse(globalTypeSpace, tokens);
		else throw new ParseException("'case' or 'default'", o);
	}
}
