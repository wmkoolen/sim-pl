/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Iterator;
import java.util.LinkedList;


public class Statement_Switch  extends Statement
{

	private final Expression e;
	private final LinkedList<SwitchEntry> cases;

    public Statement_Switch(Expression e, LinkedList<SwitchEntry> cases)
    {
		this.e = e;
		this.cases = cases;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("switch (");
		s.append(e);
		s.append(")\n{\n");

		for(SwitchEntry se : cases)
		{
			s.append(se.toString());
			s.append('\n');
		}
		s.append("}");
		return s.toString();
	}

	public int execute(Scope<String, DataSource> scope) throws Exception
	{
		scope.open();
		Instance i = e.evaluate(scope);

		Iterator        it = cases.iterator();
		SwitchEntry     c = null;

		while ( it.hasNext() && !(c = ((SwitchEntry)it.next())).matches(i, scope));

		while (c != null)
		{
			if (c.execute(scope) == BREAK) break;
			c = it.hasNext() ?  (SwitchEntry)it.next() : null;
		}
		scope.close();
		return NORMAL;
	}



}
