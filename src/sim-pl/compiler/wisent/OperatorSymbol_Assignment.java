/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class OperatorSymbol_Assignment extends OperatorSymbol implements OperatorSymbol_Infix
{
	public static final Operator_Infix infix = new Operator_Infix(390, Operator_Infix.XFY, false)
	{
		public String toString() { return "=";}

		public Type.Access getType(Expression e1, Expression e2, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
		{
			Type.Access t1 = e1.getType(namespace);
			Type.Access t2 = e2.getType(namespace);
			if (!t1.isLValue) throw new Exception(e1 + " is not an L-value");
			if (!t2.isRValue) throw new Exception(e2 + " is not an R-value");
			if (t1.type instanceof Type_Simple && t2.type instanceof Type_Simple) return t2;
			if ((t1.type instanceof Type_Simple ||
				 t1.type instanceof Type_Struct ||
				 t1.type instanceof Type_Array) &&
				(t2.type instanceof Type_Simple ||
				 t2.type instanceof Type_Struct ||
				 t2.type instanceof Type_Array))
				return t1;
			throw new Exception("Cannot apply " + toString() + " to these types");
		}


		public Instance operate(Instance i1, Instance i2) throws Exception
		{
			// important: return value is actually ignored
			// do assignment into existing instance!
			
			i1.overwriteWith(i2.getValue());
			return i1;
		}


	};

	public Operator_Infix getInfixOperator() { return infix; }


    public OperatorSymbol_Assignment()
    {
    }

	public String toString()
	{
		return "=";
	}


}
