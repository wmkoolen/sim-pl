/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import nbit.NumberFormat;
import nbit.nBit;

public class Number extends Expression
{
	/* we store the representation the number was input in, so that we can print it in recognisable form */
	final NumberFormat      representation;
	final Type_Simple       type;
	final Instance_Simple   value;

    public Number(String number) throws Exception
    {
		nBit val =          NumberFormat.parse_nBit(number, NumberFormat.DECIMAL);
		representation =    NumberFormat.getRepresentation(number, NumberFormat.DECIMAL);
		type =              new Type_Simple(val.bits, NumberFormat.isSigned(number, NumberFormat.DECIMAL));
		value =             new Instance_Simple(type, val);
    }

	public Type.Access getType(IdentifierValueMapping namespace)
	{
		return new Type.Access(false, true, type);
	}


	public Instance evaluate(IdentifierValueMapping namespace)
	{
		return value;
	}

	public String toString()
	{
		return value.toFormattedString(this.representation);
	}
}
