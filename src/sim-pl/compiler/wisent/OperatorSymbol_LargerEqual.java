/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class OperatorSymbol_LargerEqual extends OperatorSymbol implements OperatorSymbol_Infix
{
	public static final Operator_Infix infix = new Operator_Infix(280, Operator_Infix.YFX, false)
	{
		public String toString() { return ">=";}

		public Instance operate(Instance i1, Instance i2) throws Exception
		{
			if (i1 instanceof Instance_Simple && i2 instanceof Instance_Simple)
			{
				Instance_Simple is1 = (Instance_Simple)i1;
				Instance_Simple is2 = (Instance_Simple)i2;
				return new Instance_Simple(is1.type, is1.getValue().LARGER_EQUAL(is2.getValue()));
			}
			else                                                                    throw new Exception("Cannot only apply " + this.toString() + " to noncomposite types");
		}

	};
	public Operator_Infix getInfixOperator() { return infix; }



    public OperatorSymbol_LargerEqual()
    {
    }

	public String toString()
	{
		return ">=";
	}


}