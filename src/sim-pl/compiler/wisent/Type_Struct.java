/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import util.ExceptionHandler;


public class Type_Struct extends Type implements IdentifierValueMapping<String, Type.Access>
{
	final Map<String, Integer> identifier2index =  new HashMap<String,Integer>();
	final Vector<String>       identifiers =       new Vector<String>();
	final Vector<Access>       types =             new Vector<Access>();
	
	int size = -1;

    public Type_Struct()
    {    }

	/** Create a shallow copy
	 *
	 * @param t Type_Struct input to copy
	 */
	public Type_Struct(Type_Struct t)	{
		identifier2index.putAll(t.identifier2index);
		identifiers.addAll(t.identifiers);
		types.addAll(t.types);
	}

	public boolean equals(Object o)
	{
		if (o instanceof Type_Struct)
		{
			Type_Struct ts = (Type_Struct) o;
			return ts.identifiers.equals(identifiers) && ts.types.equals(types);
		}
		return false;
	}

	public Collection<Access> getValues()
	{
		return types;
	}
	
	// receiver should not change things
	public Vector<String> getIdentifiers() {
		return identifiers;
	}


	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("struct\n{\n");
		for (int i = 0; i < identifiers.size(); i++)
		{
			s.append((types.elementAt(i).type).toString());
			s.append(" ");
			s.append(identifiers.elementAt(i));
			s.append(";\n");
		}
		s.append("}\n");

		return s.toString();
	}

	public Scope<String,Type.Access> createTypeScope()
	{
		Namespace<String,Type.Access> n = new Namespace<String,Type.Access>();
		Iterator<String> i = identifiers.iterator();
		Iterator<Type.Access> j = types.iterator();

		try
		{
		    while (i.hasNext()) n.set(i.next(), j.next());
		}
		catch (IdentifierAlreadyDefined ex) { throw new ExceptionHandler(ex); }

		return n;
	}


	public Namespace<String,DataSource> createNamespace()
	{
		Namespace<String,DataSource> n = new Namespace<String,DataSource>();
		Iterator<String> i = identifiers.iterator();
		Iterator<Type.Access> j = types.iterator();

		try
		{
		    while (i.hasNext()) n.set( i.next(), new DataSource_Direct(j.next().type.createInstance()));
		}
		catch (IdentifierAlreadyDefined ex) { throw new ExceptionHandler(ex); }

		return n;
	}

	public void set(String identifier, Access type) throws IdentifierAlreadyDefined
	{
		if (identifier2index.containsKey(identifier)) throw new IdentifierAlreadyDefined(identifier);
		int index = identifiers.size();
		identifiers.add(identifier);
		types.      add(type);
		identifier2index.put(identifier, index);
	}

	public Access get(String identifier) throws UndefinedIdentifierException
	{
		if (!identifier2index.containsKey(identifier)) throw new UndefinedIdentifierException(identifier);
		return types.elementAt(identifier2index.get(identifier));
	}

	public Instance createInstance()
	{
		return new Instance_Struct(this);
	}
	
	public int getSize()
	{
		if (size < 0)
		{
			int pos = 0;
			for (Access a : types)
				pos += a.type.getSize();
			size = pos;
		}
		return size;
	}
}
