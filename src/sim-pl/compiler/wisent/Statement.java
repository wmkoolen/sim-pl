/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;

public abstract class Statement extends LanguageElement
{
	public static final int NORMAL =    0;
	public static final int BREAK =     1;


	public abstract int execute(Scope<String, DataSource> scope) throws Exception;



	public static Statement tryStatement(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		if (tokens.size() == 0) return null;

		Token o = tokens.getFirst();

		if (o.token == Interpunction.semicolon)
		{
			tokens.removeFirst();
			return Statement_Void.sv;
		}

		if (o.token instanceof Keyword)
		{
			Statement s = ((Keyword)o.token).tryStatement(globalTypeSpace, tokens);
			if (s != null) return s;
		}

		if (o.token == Interpunction.curlyBraceOpen)
		{
			tokens.removeFirst();
			return Statement_Block.parse(globalTypeSpace, tokens);
		}

		// Test for expression
		Expression e = Expression.tryExpression(globalTypeSpace, tokens);
		if (e != null)
		{
		    if (tokens.size() == 0) throw new ParseException("';'");
		    o = tokens.removeFirst();
		    if (o.token != Interpunction.semicolon) throw new ParseException("';'", o);
			return new Statement_Exp(e);
		}

		return null;
	}

	public static Statement parse(Scope<String,Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		if (tokens.size() == 0) throw new ParseException("Statement");

		Statement s = tryStatement(globalTypeSpace, tokens);
		if (s == null) throw new ParseException("Statement", tokens.getFirst());
		return s;
	}
}
