/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.LinkedList;


import compiler.LocatedException;
import compiler.ParseException;
import compiler.Token;
import compiler.TokenList;

public class SwitchEntry_Case extends SwitchEntry
{
	private final Expression e;

    public SwitchEntry_Case(Expression e, Statement [] statements)
    {
		super(statements);
		this.e = e;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("case ");
		s.append(e.toString());
		s.append(":\n");
		s.append(super.toString());
		return s.toString();
	}

	public boolean matches(Instance i, IdentifierValueMapping<String, DataSource> namespace) throws Exception
	{
		return ((Instance_Simple)e.evaluate(namespace)).getValue().equals(((Instance_Simple)i).getValue());
	}


	public static SwitchEntry parse(Scope<String, Type.Access> globalTypeSpace, TokenList tokens) throws LocatedException
	{
		Expression e = Expression.parse(globalTypeSpace, tokens);

		if (tokens.size() == 0) throw new ParseException("':'");
		Token o = tokens.removeFirst();
		if (o.token != Interpunction.colon) throw new ParseException("':'", o);


		LinkedList<Statement> statements = new LinkedList<Statement>();
		while (true)
		{
			if (tokens.size() == 0) throw new ParseException("'}' or 'case' or 'default'");
			o = tokens.getFirst();
			if (o.token == Interpunction.curlyBraceClose || o.token == Keyword._case || o.token == Keyword._default) break;
			statements.add(Statement.parse(globalTypeSpace, tokens));
		}

		return new SwitchEntry_Case(e, statements.toArray(new Statement [statements.size()]));
    }

}
