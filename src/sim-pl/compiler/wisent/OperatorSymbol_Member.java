/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class OperatorSymbol_Member extends OperatorSymbol implements OperatorSymbol_Postfix
{
	private final Operator_Postfix postfix = new Operator_Postfix(10, Operator_Postfix.YF, true)
	{
		public String toString() { return "." + member;}

		public Type.Access getType(Expression e, IdentifierValueMapping<String,Type.Access> namespace) throws Exception
		{
			Type.Access t = e.getType(namespace);

			if (!(t.type instanceof Type_Struct)) throw new Exception("Can only apply " + this.toString() + " to structures");

			Type_Struct ts = (Type_Struct)t.type;

			return ts.get(member.name);
		}

		public Instance operate(Instance i) throws Exception
		{
			if (!(i instanceof Instance_Struct)) throw new Exception("Cannot only apply " + this.toString() + " to structures");

			Instance_Struct is = (Instance_Struct)i;

			return is.getMember(member);
		}

	};
	public Operator_Postfix getPostfixOperator() {	return postfix;	}

	Identifier member;

	public OperatorSymbol_Member(Identifier member)
    {
		this.member = member;
    }

	public String toString()
	{
		return "." + member;
	}


}
