/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class Statement_Do  extends Statement
{
	private final Statement   s;
	private final Expression  e;

	public Statement_Do(Statement s, Expression e)
    {
		this.s = s;
		this.e = e;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("do ");
		if (this.s instanceof Statement_Block) s.append('\n');
		s.append(this.s.toString());
		s.append(" while (");
		s.append(e);
		s.append(")");
		return s.toString();
	}

	public int execute(Scope<String,DataSource> scope) throws Exception
	{
		scope.open();
		do
		{
			if (s.execute(scope) == BREAK) break;
		}
		while ( ((Instance_Simple)e.evaluate(scope)).getValue().toBoolean());
		scope.close();
		return NORMAL;
	}
}
