/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;


public class Statement_For  extends Statement
{
	private final Expression  init;
	private final Expression  cond;
	private final Expression  post;
	private final Statement   s;

    public Statement_For(Expression init, Expression cond, Expression post, Statement s)
    {
		this.init =     init;
		this.cond =     cond;
		this.post =     post;
		this.s =        s;
    }

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		s.append("for (");
		s.append(init.toString());
		s.append("; ");
		s.append(cond.toString());
		s.append("; ");
		s.append(post.toString());
		s.append(") ");
		if (this.s instanceof Statement_Block) s.append('\n');
		s.append(this.s.toString());
		return s.toString();
	}

	public int execute(Scope<String, DataSource> scope) throws Exception
	{
		scope.open();
		for (init.evaluate(scope); ((Instance_Simple)cond.evaluate(scope)).getValue().toBoolean(); post.evaluate(scope))
		{
			if (s.execute(scope) == BREAK) break;
		}
		scope.close();
		return NORMAL;
	}

}
