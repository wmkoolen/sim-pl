/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


header {
    package compiler.sal;
    import nbit.*;
    import compiler.sal.*;
    import java.util.*;
    import java.io.*;
	import util.MyFormatter;
	import util.ExceptionHandler;
	import executer.Instance;
	import databinding.*;
	import compiler.wisent.*;
	import executer.*;
	import executer.timetable.*;
}


class P extends Parser;
options { defaultErrorHandler=false; }
{
	Instance ifor;
	TimedFutureActions tfas = new TimedFutureActions();

	public P(Instance ifor, compiler.sal.L l) {
		this(l);
		this.ifor = ifor;
	}

	String getFileName() {
		// TODO: Fix this!!!!

		return "Unknown file";
	}


	FutureAction resolve(ArrayList<String> ids, nBit number) throws SemanticException, TokenStreamException
	{
		Instance i = ifor;

		// The first identifiers are all subcomponent selection operations
		for (int j = 0; j < ids.size() -1; j++)
		{
			String id = ids.get(j);
			if (!(i instanceof Simulator.Instance_Complex)) throw new SemanticException(i.getFullName() + " has no subcomponent", getFilename(), LT(1).getLine(), LT(1).getColumn());
			Simulator.Instance_Complex ic = (Simulator.Instance_Complex) i;
			try                     {   i = ic.getSubComponentInstance(id);        }
			catch (Exception ex)    {   throw new SemanticException(ex.getMessage(), getFilename(), LT(1).getLine(), LT(1).getColumn()); }
		}

		// The last element is the pin selection operation
		{
			String id = ids.get(ids.size()-1);

			// Check that this pin really exists and is usable
			DataSource d;
			try                     {   d = i.getNamespace().get(id);     }
			catch (Exception ex)    {   throw new SemanticException(ex.getMessage(), getFilename(), LT(1).getLine(), LT(1).getColumn()); }

			if (!d.isValueEditable()) throw new SemanticException(i.getFullName() + "." + id + " is not writable", getFilename(), LT(1).getLine(), LT(1).getColumn());

			compiler.wisent.Instance is = d.getValue();
			Type t = is.getType();
			if (!(t instanceof compiler.wisent.Type_Simple)) throw new SemanticException(i.getFullName() + "." + id + " is of a composite type", getFilename(), LT(1).getLine(), LT(1).getColumn());
			Type_Simple ts = (Type_Simple)t;

			// cast the assigned number to the type of the pin
		    compiler.wisent.Instance_Simple iss = new compiler.wisent.Instance_Simple(ts, number.cast(ts));
			if (!iss.getValue().equals(number)) throw new SemanticException(number + " too wide for type " + ts, getFilename(), LT(1).getLine(), LT(1).getColumn());

			return new FutureAction(iss, i, id);
		}
	}


	public void finish(TimeTable tt) {
		tfas.finish(tt);
	}

	class FutureAction {
		compiler.wisent.Instance_Simple iss;
		Instance i;
		String ds;

		public FutureAction(compiler.wisent.Instance_Simple iss, Instance i, String ds) {
			this.iss = iss;
			this. i = i;
			this.ds = ds;
		}


		public void finish(TimeTable tt, int time) {
			try {
			    // and make a content change event out of it
				ContentChangeEvent  cce = new ContentChangeEvent(i, time);
				NamespaceUpdate     nup = tt.getNamespaceUpdate(cce);
				nup.setDataSource(ds, iss);
			} catch (UndefinedIdentifierException ex) { throw new util.ExceptionHandler(ex); }
		}

		public String toString() {
			return i.getHybridName(ds) + " = " + iss + ";";
		}
	}

	class TimedFutureAction extends ArrayList<FutureAction> {
		int time;
		nBit time_nbit;

		public TimedFutureAction(int time) {
			this.time = time;
		}

		public void finish(TimeTable tt) {
			for (FutureAction fa : this) fa.finish(tt,time);
		}

		public String toString(int timew, int cellw, int time_bits) {
			StringBuffer s = new StringBuffer();
			try {
			    s.append(MyFormatter.pad(new nBit(time_bits, time).toString(NumberFormat.HEXADECIMAL,false,true),timew)+":");
			} catch (ZeroBitException ex) { throw new ExceptionHandler(ex); }

			for (Iterator it = iterator(); it.hasNext(); ) {
				s.append(' ');
				s.append(MyFormatter.pad(it.next(), cellw));
			}
			return s.toString();
		}
	}

	class TimedFutureActions extends ArrayList<TimedFutureAction> {
		int cellw = 0;
		int time_bits = 0;

		public boolean add(TimedFutureAction tfa) {
			super.add(tfa);
			time_bits = Math.max(time_bits, nBit.bitsUsed(tfa.time));
			for (Iterator it = tfa.iterator(); it.hasNext(); ) {
				cellw = Math.max(cellw, ((FutureAction)it.next()).toString().length());
			}
            return true;
		}

		public void finish(TimeTable tt) {
			for (TimedFutureAction tfa : this) tfa.finish(tt);
		}


		public String toString() {

			int timew = 0;
			try {
			    for (TimedFutureAction tfa : this) {
			        timew = Math.max(timew, new nBit(time_bits, tfa.time).toString(NumberFormat.HEXADECIMAL,false,true).length());
			    }
			} catch (ZeroBitException ex) { throw new ExceptionHandler(ex); }

			StringBuffer s = new StringBuffer();
			for (TimedFutureAction tfa : this) {
				s.append(tfa.toString(timew, cellw, time_bits));
				s.append('\n');
			}
			return s.toString();
		}
	}


    public TreeMap<Integer,Collection<LocatedTextRun>> t2hls = new TreeMap<Integer,Collection<LocatedTextRun>>();

    public void addHighlight(int time, int b_line, int b_col, int e_line, int e_col) {
        Integer itime = new Integer(time);
        Collection<LocatedTextRun> hls = t2hls.get(itime);
        if (hls == null) {
            hls = new ArrayList<LocatedTextRun>();
            t2hls.put(itime, hls);
        }
		// SAL highlights are always in the current file, which is represented 
		// by 'null' (says the contract of LTR)
        hls.add(new LocatedTextRun(null, b_line, b_col, e_line, e_col));
    }

}



lines returns [String s]
{

    TimedFutureAction tfa;
}
    :
        (tfa=line { tfas.add(tfa); } )* EOF
		{ s = tfas.toString(); }
 	;

line returns [TimedFutureAction tfa]
{
    FutureAction a;
	nBit t;
    int time;
    int b_line, b_col;
}
    :
        { b_line = LT(1).getLine(); b_col= LT(1).getColumn();}
        t=nbit { try { time = t.toInt();  }
		         catch (IntConversionException ex) { throw new SemanticException("Time: " + t + " too large.", getFilename(), LT(1).getLine(), LT(1).getColumn());	
            }
            tfa = new TimedFutureAction(time);
        }
		COLON
		(a=assignment { tfa.add(a); } )*
        // TODO: the null here is VERY silly
        { addHighlight(time, b_line, b_col, LT(1).getLine(), LT(1).getColumn()); }
    ;

assignment returns [FutureAction fa]
{
    ArrayList<String> ids = new ArrayList<String>();
    String head, tail;    
    nBit val;
}
    :
        head=id { ids.add(head);} (PERIOD tail=id { ids.add(tail);} )*
        EQUALS
        val=nbit
        SEMICOL
        { fa = resolve(ids,val); }
    ;

id returns [String s]
{
	s = null;
}
	:   ( i:NORMAL_ID  { s = i.getText(); }
        | j:ESCAPED_ID { s = j.getText(); }
        )
	;


nbit returns [nBit nB]
{
 	nB = null;
}
 	: n:NBIT
 	  {
 	  	try {
            // try to interpret unprefixed strings as decimal numbers
			nB = NumberFormat.parse_nBit(n.getText(), NumberFormat.DECIMAL);
		} catch (nbit.ParseException ex) {
			//pi.logger.addParagraph(ex.getMessage());
			throw new SemanticException("Error occurred", getFilename(), LT(1).getLine(), LT(1).getColumn());
		}
 	  }
 	;





class L extends Lexer;
options {
    charVocabulary = '\u0000'..'\u00ff'; // we're working 8 bit ascii
}

protected
DEC_DIGIT
options { paraphrase = "a decimal digit"; }
			: ('0'..'9')
			;

protected
HEX_DIGIT
options { paraphrase = "a hexadecimal digit"; }
			: ('0'..'9' | 'a'..'f' | 'A'..'F')
			;

protected
BIN_DIGIT
options { paraphrase = "a binary digit"; }
			: ('0'..'1')
			;

protected
LETTER		:	('a'..'z' | 'A'..'Z' | '$' | '_')
			;

NBIT
options { paraphrase = "a number constant"; }
		: 	( '0'
				(	'x' (HEX_DIGIT)+
				| 	'b' (BIN_DIGIT)+
				|	'd' (DEC_DIGIT)+
				| 	(DEC_DIGIT)*
				)
			| '1'..'9' (DEC_DIGIT)*
			)
		;


NORMAL_ID
options{paraphrase = "an identifier";}
    :	LETTER (LETTER | DEC_DIGIT)*
    ;

ESCAPED_ID
options { paraphrase = "an escaped identifier"; }
    :   '"'!
        ( ~('"') )*
        '"'!
    ;



COLON	:	':'
		;

SEMICOL	:	';'
		;

EQUALS  :   '='
		;

PERIOD 	:	'.'
 		;


 // whitespace
 WS		
    :	(	' '
        |	'\t'
        |   (   '\n'           // unix way
            |   '\r' ('\n')?   // Macintosh & DOS/Windows way
            )   { newline(); }
        )
        {$setType(Token.SKIP);}	//ignore this token
 		;


COMMENT
options{paraphrase = "single-line comment";}
	:	'#' (~('\n' | '\r'))*
        {$setType(Token.SKIP);}	//ignore this token
	;

