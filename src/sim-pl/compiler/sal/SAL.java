/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.sal;

import java.awt.Color;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.SortedMap;

import log.Logger;
import util.ExtensionAddingFilter;

import compiler.ComponentCompiler;
import compiler.ComponentProgram;

import executer.Instance;
import executer.timetable.AliasResolver;
import executer.timetable.Highlight;
import executer.timetable.HighlightManager;
import executer.timetable.LocatedTextRun;
import executer.timetable.TimeTable;


public class SAL implements ComponentCompiler
{
//	public static final SAL compiler = new SAL();

	// pastel orange for highlight background
	public static final Color HIGHLIGHT_BACKGROUND = new Color(255, 235, 195);
	public static final Color HIGHLIGHT_FIRST      = new Color(0xc8, 0xc8, 0xff);


	public ComponentProgram compile(String source, File includesDir, Instance ifor, Logger logger)  throws Exception {
		StringReader sr = new StringReader(source);
		L l = new L(sr);
		l.setFilename(INPUT_TEXT_NAME);
		final P p = new P(ifor, l);
		p.setFilename(INPUT_TEXT_NAME);
		String r = p.lines();
//		logger.addParagraph(r);

		return new ComponentProgram() {
			public void execute(TimeTable tt) {
				p.finish(tt);
			}
			public AliasResolver getAliasResolver() { return null; }

			public HighlightManager getHighlightManager() { return new HighlightManager() {
					List<Highlight> curh = null;

		/** Highlights behave as follows:
		 * - after pressing "CYCLE", the instruction just executed is highlighted.
		 * - at the beginning of the program, the first instruction must be hightlighted in
		 *   a different colour
		 * - if "CYCLE" is pressed too many times, then no instruction is highlighted.
		 */
					public void recomputeHighlights(TimeTable tt) {
						curh = new ArrayList<Highlight>();

						SortedMap<Integer, Collection<LocatedTextRun>> hm = p.t2hls.headMap(new Integer(tt.getCurrentTime()));
						if (hm.isEmpty()) {
							if (p.t2hls.isEmpty()) return; // no program at all

							// highlight first instruction in special colour
							for (LocatedTextRun ltr : p.t2hls.firstEntry().getValue()) {
								assert ltr.file == null :"SAL can not handle includes";
								curh.add(new Highlight(HIGHLIGHT_FIRST, ltr));
							}
							return;
						}

						Integer lastKey = hm.lastKey();

						// check wether we're past the end of the program
						if (lastKey + tt.getCycleLength() < tt.getCurrentTime())  return;

						for (LocatedTextRun ltr : hm.get(lastKey)) {
							assert ltr.file == null :"SAL can not handle includes";
							curh.add(new Highlight(HIGHLIGHT_BACKGROUND, ltr));
						}
					}

					public List<Highlight> getHighlights() {
						return curh;
					}
				};
			}
		};
    }

	public String toString()
	{
		return "SAL Compiler";
	}

	ExtensionAddingFilter filter; // lazy instantiate

	public ExtensionAddingFilter getFileFilter() {
		if (filter == null) filter = new ExtensionAddingFilter("Simple Assignment Language Program", ".sal");
		return filter;
	}
}
