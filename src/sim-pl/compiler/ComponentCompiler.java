/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler;

import java.io.File;

import log.Logger;
import util.ExtensionAddingFilter;
import executer.Instance;


public interface ComponentCompiler extends Compiler
{
	String INPUT_TEXT_NAME = "Input text";

	/** The contract for compile is as follows:
	 *  The compiler may output informational and warning messages.
	 *  If the compiler bails with an exception, then the exception message
	 *  should NOT be logged, this is the task of the caller.
	 *
	 * @param source String         Source text
	 * @param includesDir File      Where to resolve includes
	 * @param component Instance    Instance to run program on
	 * @param logger Logger         Logger (informational/warning messages only)
	 * @return ComponentProgram     A program that can be run on the instance
	 * @throws Exception
	 */

	public abstract ComponentProgram compile(String source, File includesDir, Instance component, Logger logger) throws Exception;


	public abstract ExtensionAddingFilter getFileFilter();
}
