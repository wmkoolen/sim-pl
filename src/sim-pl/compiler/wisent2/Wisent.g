/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


/** 
Adapted by Wouter Koolen-Wijkstra from the C grammar
http://www.antlr.org/grammar/1153358328744/C.g by Terence Parr.

Operator precedence reference:
http://en.cppreference.com/w/cpp/language/operator_precedence


TODO:
This file is the home of the future Wisent grammar, now redone with ANTLRv3.

*/

grammar Wisent;

scope Symbols {
	Map<String, Type>        types; // track types
	Map<String, Type.Access> vars;  // track variables
}


@header {
package compiler.wisent2;

import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import compiler.wisent.*;
import compiler.wisent.Number;
import nbit.*;
}

@lexer::header {
package compiler.wisent2;
}


@members {
	Type lookupType(String name) {
        System.out.println("Looking up type " + name);
		for (int i = Symbols_stack.size()-1; i>=0; i--) {
			Symbols_scope scope = (Symbols_scope)Symbols_stack.get(i);
            Type t = scope.types.get(name);
            if (t != null) return t;
		}
        System.out.println("Looking up type " + name + " failed");
		return null;
	}


    public static void main(String[] args) throws Exception {
        WisentLexer lex = new WisentLexer(new ANTLRFileStream(args[0]));
       	CommonTokenStream tokens = new CommonTokenStream(lex);

        WisentParser parser = new WisentParser(tokens);

        try {
            parser.translation_unit();
        } catch (RecognitionException e)  {
            e.printStackTrace();
        }        
    }
}

translation_unit returns [Statement [\] statements]
scope Symbols; // entire file is a scope
@init {
  $Symbols::types = new HashMap<String, Type>();
  $Symbols::vars  = new HashMap<String, Type.Access>();
}
	: sl=statement_list EOF
        { statements = $sl.ss; }
	;



type_specifier returns [Type t]
	: tsi=type_simple      { $t = $tsi.t; }
	| tst=struct_specifier { $t = $tst.t; }
    | tar=array_specifier  { $t = $tar.t; }
	| tid=type_id          { $t = $tid.t; }
	;

type_simple returns [Type_Simple t]
@init {
    boolean signed=false;
}
    : ('signed' { signed=true; } | 'unsigned')? 
        v=constant 
        'bit' 
        { 
            try {
                $t = new Type_Simple($v.n.toInt(), signed);
            } catch (Exception e) {
                throw new RecognitionException(); // TODO: provide informative error message
            }
        }
    ;


array_specifier returns [Type_Array t]
    : '[' v=constant ']' ts=type_specifier
        {
            try {
                $t = new Type_Array($v.n.toInt(), $ts.t);
            } catch (Exception e) {
                throw new RecognitionException(); // TODO: provide informative error message
            }
        }
    ;

type_id returns [Type t]
    :   IDENTIFIER
        { $t = lookupType($IDENTIFIER.text);
            if ($t == null) throw new RecognitionException(); // TODO: provide informative error message
        }
    ;

struct_specifier returns [Type_Struct t]
/*
options {
k=3;
}
*/
scope Symbols; // structs are scopes
@init {
  $Symbols::types = new HashMap<String, Type>();
  $Symbols::vars  = new HashMap<String, Type.Access>();
  $t = new Type_Struct();
}
	: 'struct' '{' 
        (
            sd=struct_declaration
            { 
                try {
                    $t.set($sd.id, new Type.Access(true,  true,  $sd.t)); 
                } catch (IdentifierAlreadyDefined ex) {
                    throw new Error(ex); // TODO: give sensible error message
                }
            }
        )+ 
        '}'
	;

struct_declaration returns [Type t, String id]
	: tp=type_specifier IDENTIFIER ';'
        { $t = $tp.t; $id = $IDENTIFIER.text; }
	;




// E x p r e s s i o n s

additive_expression returns [Expression e]
	: a=multiplicative_expression { $e=$a.e;}
        (  op=add_op 
            b=multiplicative_expression  
            {$e = new Expression_Operator_Infix($e, $op.op, $b.e); }
        )*
	;

add_op returns [Operator_Infix op]
    : '+' {$op = OperatorSymbol_Plus.infix; }
    | '-' {$op = OperatorSymbol_Minus.infix; }
    ;


multiplicative_expression returns [Expression e]
	: a=cast_expression { $e=$a.e;}
        ( op=mult_op b=cast_expression 
            {$e = new Expression_Operator_Infix($e, $op.op, $b.e); }
        )*
	;

mult_op returns [Operator_Infix op]
	: '*'   {$op = OperatorSymbol_Multiply.infix; }
    | '*S'  {$op = OperatorSymbol_Multiply_Signed.infix; }
    | '/'   {$op = OperatorSymbol_Divide.infix; }
    | '/S'  {$op = OperatorSymbol_Divide_Signed.infix; }
    | '%'   {$op = OperatorSymbol_Remainder.infix; }
    | '%S'  {$op = OperatorSymbol_Remainder_Signed.infix; }
	;

cast_expression returns [Expression e]
options {
    backtrack=true;
}
	: '(' t=type_specifier ')' c=cast_expression 
        {
            try {
                $e = new Expression_Operator_Prefix(new OperatorSymbol_Cast($t.t).getPrefixOperator(), $c.e);
            } catch (Exception ex) { throw new Error(ex); }
        }
	| a=unary_expression { $e=$a.e;}
	;

unary_expression returns [Expression e]
	: a=postfix_expression { $e=$a.e;}
	| op=prefix_operator b=unary_expression { $e = new Expression_Operator_Prefix($op.op,$b.e); }
	| op=unary_operator  b=cast_expression  { $e = new Expression_Operator_Prefix($op.op,$b.e); }
	;

prefix_operator returns [Operator_Prefix op]
	: '++' {$op = OperatorSymbol_PlusPlus.prefix; }
	| '--' {$op = OperatorSymbol_MinMin.prefix; }
	;

unary_operator returns [Operator_Prefix op]
	: '+' {$op = OperatorSymbol_Plus.prefix; }
	| '-' {$op = OperatorSymbol_Minus.prefix; }
	| '~' {$op = OperatorSymbol_Not_Bit.prefix; }
	| '!' {$op = OperatorSymbol_Not_Bin.prefix; }
	;

postfix_expression returns [Expression e]
	:   a=primary_expression {$e=$a.e;}
        (   '[' in=expression ']' {$e = new Expression_Operator_Infix($e, OperatorSymbol_Index.index.getInfixOperator(), $in.e); }
        |   '.' id=identifier {$e = new Expression_Operator_Postfix($e, new OperatorSymbol_Member($id.id).getPostfixOperator()); }
        |   op=postfix_operator { $e = new Expression_Operator_Postfix($e, $op.op); }
        )*
	;

postfix_operator returns [Operator_Postfix op]
	: '++' {$op = OperatorSymbol_PlusPlus.postfix; }
	| '--' {$op = OperatorSymbol_MinMin.postfix; }
	;

identifier returns [Identifier id]
    : IDENTIFIER {$id = new Identifier($IDENTIFIER.text);}
    ;

primary_expression returns [Expression e]
	: id=identifier {$e = $id.id;}
    | t=type_specifier id=identifier {$e = new Expression_VarDec($t.t, $id.id); }
	| b=constant { try { $e = new Number($b.text); } catch (Exception ex) {throw new Error(ex); } }
	| '(' c=expression ')' {$e = $c.e;}
	;

constant returns [nBit n]
    :   l=( HEX_LITERAL 
          | BINARY_LITERAL  
          | DECIMAL_LITERAL
          )
        { $n = NumberFormat.parse_nBit($l.text, NumberFormat.DECIMAL); }
    ;

/////

expression returns [Expression e]
	: a=assignment_expression {$e=$a.e;} 
        (',' a=assignment_expression {throw new Error("comma operator not supported yet");} )*
	;

constant_expression returns [Expression e]
	: c=conditional_expression {$e=$c.e;}
	;

assignment_expression returns [Expression e]
options {
    backtrack=true;
}
	: left=lvalue op=assignment_operator right=assignment_expression
        { $e = new Expression_Operator_Infix($left.e, $op.op, $right.e); }
	| a=conditional_expression {$e=$a.e;}
	;
	
lvalue returns [Expression e]
	:	a=unary_expression {$e=$a.e;}
	;

assignment_operator returns [Operator_Infix op]
	: '='  {$op = OperatorSymbol_Assignment.infix; }
	| '*='
	| '*S='
	| '/='
	| '/S='
	| '%='
	| '%S='
	| '+='
	| '-='
	| '<<='
	| '>>='
	| '&='
	| '^='
	| '|='
	;

/* Note: C specifies
     conditional_expression 
   as the right argument, whereas C++ dictates
     assignment_expression
   We go with C++ since it is more permissive.
*/
conditional_expression returns [Expression e]
	: a=logical_or_expression {$e=$a.e;} 
        ('?' expression ':' assignment_expression { throw new Error("?: operator not yet supported");})?
	;

logical_or_expression returns [Expression e]
	: a=logical_and_expression {$e=$a.e;}
        ('||' b=logical_and_expression
            {$e = new Expression_Operator_Infix($e,OperatorSymbol_Or_Bin.infix,$b.e);}
        )*
	;

logical_and_expression returns [Expression e]
	: a=inclusive_or_expression {$e=$a.e;} 
        ('&&' b=inclusive_or_expression
            {$e = new Expression_Operator_Infix($e,OperatorSymbol_And_Bin.infix,$b.e);}
        )*
	;

inclusive_or_expression returns [Expression e]
	: a=exclusive_or_expression {$e=$a.e;}
        ('|' b=exclusive_or_expression
            {$e = new Expression_Operator_Infix($e,OperatorSymbol_Or_Bit.infix,$b.e);}
        )*
	;

exclusive_or_expression returns [Expression e]
	: a=and_expression {$e=$a.e;}
        ('^' b=and_expression
            {$e = new Expression_Operator_Infix($e,OperatorSymbol_Xor_Bit.infix,$b.e);}
        )*
	;

and_expression returns [Expression e]
	: a=equality_expression {$e=$a.e;}
        ('&' b=equality_expression
            {$e = new Expression_Operator_Infix($e,OperatorSymbol_And_Bit.infix,$b.e);}
        )*
	;
equality_expression returns [Expression e]
	: a=relational_expression {$e=$a.e;}
        (op=eq_op b=relational_expression
            {$e = new Expression_Operator_Infix($e,$op.op,$b.e);}
        )*
	;

eq_op returns [Operator_Infix op]
    : '==' {$op = OperatorSymbol_Equals.infix; }
    | '!=' {$op = OperatorSymbol_Nequals.infix; }
    ;

relational_expression returns [Expression e]
	: a=shift_expression {$e=$a.e;}
        (op=rel_op b=shift_expression
            {$e = new Expression_Operator_Infix($e,$op.op,$b.e);}
        )*
	;

rel_op returns [Operator_Infix op]
    : '<'  {$op = OperatorSymbol_Smaller.infix; }
    | '>'  {$op = OperatorSymbol_Larger.infix; }
    | '<=' {$op = OperatorSymbol_SmallerEqual.infix; }
    | '>=' {$op = OperatorSymbol_LargerEqual.infix; }
    ;


shift_expression returns [Expression e]
	: a=additive_expression {$e=$a.e;}
        (op=shift_op b=additive_expression
            {$e = new Expression_Operator_Infix($e,$op.op,$b.e);}
        )*
	;

shift_op returns [Operator_Infix op]
    : '<<'  {$op = OperatorSymbol_ShiftLeft.infix; }
    | '>>'  {$op = OperatorSymbol_ShiftRight.infix; }
    ;


// S t a t e m e n t s

statement returns [Statement s]
	: ts=typedef_statement    {$s = Statement_Void.sv; }
    | ls=labeled_statement    {$s = $ls.s; }
	| cs=compound_statement   {$s = $cs.s; }
	| es=expression_statement {$s = $es.s; }
	| ss=selection_statement  {$s = $ss.s; }
	| is=iteration_statement  {$s = $is.s; }
	| js=jump_statement       {$s = $js.s; }
	;


typedef_statement
    : 'typedef' t=type_specifier IDENTIFIER ';'
        {   assert $t.t != null; 
            $Symbols::types.put($IDENTIFIER.text, $t.t); 
            System.out.println("Registered " + $IDENTIFIER.text+ " as a type");
        }
    ;

labeled_statement returns [Statement s]
	: IDENTIFIER ':' st=statement {$s = $st.s;}
	;

compound_statement returns [Statement_Block s]
scope Symbols; // blocks have a scope of symbols
@init {
  $Symbols::types = new HashMap<String, Type>();
  $Symbols::vars  = new HashMap<String, Type.Access>();
}
	: '{' sl=statement_list '}'
        { $s = new Statement_Block($sl.ss); }
	;

statement_list  returns [Statement [\] ss]
@init {
  LinkedList<Statement> statements = new LinkedList<Statement>();
}
	:   (st=statement {statements.add($st.s);} )*
        { $ss = statements.toArray(new Statement[statements.size()]); }
	;


expression_statement returns [Statement s]
	: ';'              {$s = Statement_Void.sv;}
	| e=expression ';' {$s = new Statement_Exp($e.e); }
	;

selection_statement returns [Statement s]
	: st=if_statement     {$s = $st.s; }
	| st=switch_statement {$s = $st.s; }
	;

if_statement returns [Statement s]
    : 'if' '(' e=expression ')' s1=statement ( ('else') => 'else' s2=statement)?
        { return $s2.s == null ? new Statement_If($e.e, $s1.s)
                               : new Statement_If_Else($e.e, $s1.s, $s2.s); }
    ;

switch_statement returns [Statement s]
@init{
    LinkedList<SwitchEntry> cases = new LinkedList<SwitchEntry>();
}
    : 'switch' '(' e=expression ')' 
        '{' 
        ( se=switch_entry {cases.add($se.se);} )*
        '}'
        { $s = new Statement_Switch($e.e, cases); }
    ;

switch_entry returns [SwitchEntry se]
    : 'case' e=constant_expression ':' sl=statement_list
        {$se = new SwitchEntry_Case($e.e, $sl.ss);}
    | 'default' ':' sl=statement_list
        {$se = new SwitchEntry_Default($sl.ss);}
    ;



iteration_statement returns [Statement s]
	: ws=while_statement {$s = $ws.s;}
	| ds=do_statement    {$s = $ds.s;}
	| fs=for_statement   {$s = $fs.s;}
	;

while_statement returns [Statement_While s]
    : 'while' '(' e=expression ')' st=statement
        {$s = new Statement_While($e.e, $st.s); }
    ;

do_statement returns [Statement_Do s]
    : 'do' st=statement 'while' '(' e=expression ')' ';'
        {$s = new Statement_Do($st.s, $e.e); }
    ;

for_statement returns [Statement_For s]
    : 'for' '(' init=expression ';'
                cond=expression ';'
                post=expression
        ')' st=statement
        { $s = new Statement_For($init.e, $cond.e, $post.e, $st.s); }
    ;

jump_statement returns [Statement s]
	: 'continue' ';' { throw new Error("continue support not yet implemented"); }
	| 'break' ';'    {$s = Statement_Break.sb;}
	;

IDENTIFIER
	:	LETTER (LETTER|'0'..'9')*
	;
	
fragment
LETTER
	:	'$'
	|	'A'..'Z'
	|	'a'..'z'
	|	'_'
	;


HEX_LITERAL : '0' ('x'|'X') HexDigit+;

DECIMAL_LITERAL : ('0' ('d'|'D'))? ('0'..'9')+;

BINARY_LITERAL : '0' ('b'|'B') ('0'..'1')+;

fragment
HexDigit : ('0'..'9'|'a'..'f'|'A'..'F') ;


WS  :  (' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;}
    ;

COMMENT
    :   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

LINE_COMMENT
    : '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;

// ignore #line info for now
LINE_COMMAND 
    : '#' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;
