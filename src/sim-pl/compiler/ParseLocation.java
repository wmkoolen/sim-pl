/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler;

import util.MyFormatter;

public class ParseLocation
{
	public int     line;
	public int     col;
	public int     index;
	public String  forString;

	public ParseLocation(int line, int col, int index, String forString)
	{
		this.line =         line;
		this.col =          col;
		this.index =        index;
		this.forString =    forString;
	}

	public ParseLocation klone()
	{
		return new ParseLocation(line, col, index, forString);
	}

	public String toString()
	{
		int contextStart =  index;
		int contextEnd =    index;
		while (contextStart > 0                && forString.charAt(contextStart-1) != '\n')                                                 contextStart--;
		while (contextEnd < forString.length() && forString.charAt(contextEnd)     != '\n' && forString.charAt(contextEnd)     != '\0')     contextEnd  ++;

		return ("line " + line + ", col " + col + " context: " + MyFormatter.unformat(forString.substring(contextStart, contextEnd)));
	}

	public void advance()
	{
		if (forString.charAt(index++) == '\n')
		{
			col = 0;
			line++;
		}
		else
		{
			col ++;
		}
	}

	public char get()
	{
		return forString.charAt(index);
	}

	public char getNext()
	{
		return index < forString.length()-1 ? forString.charAt(index + 1) : '\0';
	}
}
