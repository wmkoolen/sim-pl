/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler;

import java.util.Iterator;
import java.util.LinkedList;

import util.MyFormatter;

public class TokenList
{
	LinkedList<Token> l = new LinkedList<Token>();

	public void addLast(Object token, ParseLocation location)
	{
		l.addLast(new Token(token, location));
	}

	public Token getFirst()
	{
		return l.getFirst();
	}

	public Token getSecond()
	{
		return l.get(1);
	}

	public Token removeFirst()
	{
		return l.removeFirst();
	}

	public int size()
	{
		return l.size();
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();
		for (Token t : l)
		{
			s.append(MyFormatter.pad(t.token.getClass().getName(), 60));
			s.append(MyFormatter.unformat(t.token));
			s.append('\n');
		}

		return s.toString();
	}
}
