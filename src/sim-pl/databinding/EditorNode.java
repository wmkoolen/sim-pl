/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.Collection;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.Identifier;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public  class EditorNode<T extends BasicEditorNode> implements BasicEditorNode<T> {
	protected T parent;

	public boolean canBeDetached() { return true; }
	
	public void collectInvolvedInMove(Set<BasicEditorNode> selection, Set<IMoveable> movers) {
		// default for "normal" nodes
		if (this instanceof IMoveable) {
			movers.add((IMoveable)this);
		}
	}

	public final T getParent() {
//		assert Component.class.isInstance(this) == (parent == null) : "null parent in " + getClass().getName();
		return parent;
	}

	public final void setParent(T parent) {
		assert (this.parent == null) || (parent == null) : "double parent in " + this.getClass();
		this.parent = parent;
	}

	public void wire(T parent) {
		if (this.parent != parent) setParent(parent);
	}

	public boolean isWellFormed() { return true; }
	public boolean isValid()      { return isWellFormed(); }

	// bottom of recursion; forward to non-recursive method
	public boolean isWellFormedRecursively() { return isWellFormed(); }
	public boolean isValidRecursively()      { return isValid(); }


	public void collectUsedParameters(Set<String> params) {}


	public static void deepCollectUsedParameters(BasicEditorNode<?> node, Set<String> s) {
		node.collectUsedParameters(s);
		if (node instanceof InternalNode) {
			for (BasicEditorNode n : ( (InternalNode<?,?>)node).getChildren()) {
				deepCollectUsedParameters(n, s);
			}
		}
	}


	public static void collectDefines(BasicEditorNode<?> node, Set<String> defs) {
		if (node instanceof NameDeclaringNode) {
			defs.add(((NameDeclaringNode)node).getName());
		}
		if (node instanceof InternalNode) {
			for (BasicEditorNode n : ( (InternalNode<?,?>)node).getChildren()) {
				collectDefines(n, defs);
			}
		}
	}

	public void collectDefs(String cfnContext, Collection<Identifier> ids) {
		if (this instanceof NameDeclaringNode) {
			NameDeclaringNode ndn = (NameDeclaringNode)this;
			ids.add(new Identifier(cfnContext, ndn.getType(), ndn.getName()));
		}
	}

	public void collectRefs(ComponentNamespace ns, Collection<Identifier> ids) {}


}
