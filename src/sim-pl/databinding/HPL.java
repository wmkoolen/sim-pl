/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.ArrayList;
import java.util.List;

import util.MyFormatter;

/** Hierarchical Part Locator.
 *
 * A HPL serves to uniquely identify the path from
 * the root (which is ususally a component or instance) to a part of the tree.
 *
 * A HPL is of the following form:
 * <code> root(.sub)*(:part)? </code>
 *
 *
 */


public class HPL {

	/** The component root of the HPL.
	 */

	public final String root;
	/**
	 * A list of hierarchical subcomponents.
	 * Note that the first item is always a type name, not an instance name,
	 * as there is only one outermost instance.
	 */
	public final List<String> subs;

	/** An optional part within the final leaf of the subs list
	 */

	public final String part;


	/** Parses a HPL string.
	 *
	 * Note: this procedure does not check wether the HPL actually refers to
	 * something.
	 *
	 * @param hpl String A HPL string
	 * @throws InvalidHPLException when the string is not syntactically well-formed
	 *
	 */

	public HPL(String hpl) throws InvalidHPLException {
		String [] hp = hpl.split(":");
		assert hp.length > 0;

		if (hp.length > 2) throw new InvalidHPLException(hpl, "too many colons");
		String [] h = hp[0].split("\\."); // regex for '.'
		assert h.length > 0;

		root = h[0];
		subs = new ArrayList<String>();
		for (int i = 1; i < h.length; i++) subs.add(h[i]);
		part = (hp.length == 2 ? hp[1] : null);
	}

	/** Direct constructor.
	 * @param root String The root component type
	 * @param subs List   A list of subcomponent names
	 * @param part String The name of a part (optional)
	 */

	public HPL(String root, List<String> subs, String part) {
		this.root = root;
		this.subs = subs;
		this.part = part;
	}

	/** Append-part constructor.
	 * @param hpl HPL The root component
	 * @param part String The name of a part
	 */

	public HPL(HPL hpl, String part) {
		this.root = hpl.root;
		this.subs = hpl.subs;
		assert(hpl.part == null);
		this.part = part;
	}



	/** Strips the part, to always make the HPL resolve to a component
	 * @return HPL  stripped of the part
	 */

	public HPL stripPart() {
		return new HPL(root, subs, null);
	}

	/** Retransforms the HPL into String format.
	 * @return String
	 */
	public String toString() {
		return root + "." + MyFormatter.join(".", subs) + (part == null ? "" : ":"+part);
	}
}


