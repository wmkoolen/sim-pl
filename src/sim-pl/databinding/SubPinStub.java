/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.geom.Point2D;

import databinding.wires.SubPinNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


public class SubPinStub extends EditorNode<SubPinStubs> implements IPositioned {

	// the position of the pin (or the dangle) in SUBCOMPONENT coordinate system
	Point2D.Double pos = new Point2D.Double();

	// the name of the pin we reference
	String pinref;

	// the followint is defined iff we can resolve the pin reference
	Pin p;

	// used if we have a subpin node attached
	SubPinNode spn;

	public SubPinStub() { }

	public SubPinStub(Pin p) {
		this.p = p;
		this.pinref = p.getName();
		pos.setLocation(p.getPos());
	}

	public SubPinStub(String pinref, Point2D p) {
		this.pinref = pinref;
		pos.setLocation(p);
	}


	public boolean isResolved() {
		return  p != null;
	}

	public boolean isSpanEnd() { return true; }

	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert pinref != null;

		assert isResolved() ==
			(getSubComponent().isResolved() &&
			 getSubComponent().getSubComponent().getIo().getByName(pinref) != null);

	    assert p == null || p == getSubComponent().getSubComponent().getIo().getByName(pinref);
		assert p == null || pinref.equals(p.getName());
		assert p == null || pos.equals(p.getPos()) : "pin moved";
		return true;
	}

	public boolean isValid() {
		assert super.isValid();
		assert spn != null;
		assert spn.getStub() == this;
		assert spn.getParent() != null;
		assert spn.getParent().getChildren().contains(spn);
		return true;
	}


	// to XML method
	public String getPinRef() { return pinref; }

	// from XML method
	public void setPinRef(String pinref) {
		assert p == null; // only used during XML construction
		this.pinref = pinref;
	}

	public String toString() { return pinref; }

	public Point2D getInternalPos() { return pos; }
	public Point2D getExternalPos() {
		return getSubComponent().getTransform().transform(pos, null);
	}

	public double getX() { return pos.x; }    // XML serialisation
	public void setX(double x) { pos.x = x; } // XML construction
	public double getY() { return pos.y; }    // XML serialisation
	public void setY(double y) { pos.y = y; } // XML construction



	public SubComponent getSubComponent() {
		return super.getParent().getParent();
	}

	public SubPinNode getNode() {
		return spn;
	}
	public void setNode(SubPinNode spn) {
		this.spn = spn;
	}

	public void rewire() {
		if (getSubComponent().isResolved()) {
			p = getSubComponent().getSubComponent().getIo().getByName(pinref);
			if (p != null) {
				// immediately copy correct pin position
				pos.setLocation(p.getPos());
			}
		} else {
			p = null;
			// leave location where it is
		}
	}

	public void wire(SubPinStubs parent) {
		super.wire(parent);
		assert p == null;
		rewire();
	}

	public Pin getPin() {
		assert p != null;
		return p;
	}

}

