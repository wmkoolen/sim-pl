/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import namespace.ComponentTable;
import databinding.wires.Edge;
import databinding.wires.SubPinNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

// TODO: use parametervals



public class SubComponent extends FixedList<SubComponents, BasicEditorNode<?>> implements NameDeclaringNode, IMoveable, IPositioned {
	String name;
	File   data; // an absolute path string (conversion to/from relative paths is done on (un)marshalling)
	Point  pos = new Point();
	double zoom;
	Flip   flip;

	/** This reference is defined iff the component resolves properly */
	private Component c = null;

	ParameterVals parameterVals = new ParameterVals();
	public ParameterVals getParameterVals() { return parameterVals;}

	SubPinStubs   subPinStubs   = new SubPinStubs();
	public SubPinStubs getSubPinStubs() { return subPinStubs; }

	private List<BasicEditorNode<?>> children = Arrays.<BasicEditorNode<?>>asList(parameterVals, subPinStubs);
	public  List<BasicEditorNode<?>> getChildren() { return children; }

	public SubComponent() {
		flip = Flip.none;
	}

	public SubComponent(String name, File data, Point2D pos, double zoom, Flip flip) {
		parameterVals.setParent(this);
		subPinStubs  .setParent(this);

		this.name = name;
		this.data = data;
		assert data.isAbsolute();
		this.pos.setLocation(pos);
		this.zoom = zoom;
		this.flip = flip;

		try {
			c = ComponentTable.load(data);
		} catch (Exception ex) {
			c = null;
		}
	}

	public String toString() {
		return name;
	}


	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	public Class getType() { return SubComponent.class; }

	public File getData() { return data; }
	public void setData(File data) { this.data = data; 	assert data.isAbsolute(); 	}

	public double getX() { return pos.getX(); }
	public void setX(double x) { pos.setLocation(x, pos.getY()); }

	public double getY() { return pos.getY(); }
	public void setY(double y) { pos.setLocation(pos.getX(), y); }

	public double getZoom() { return zoom; }
	public void setZoom(double zoom) { this.zoom = zoom; }

	public Flip getFlip() { return flip; }
	public void setFlip(Flip flip) { this.flip = flip; }

	public void move(int dx, int dy) { pos.translate(dx, dy); }

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}

	public AffineTransform getTransform() {
		AffineTransform t = AffineTransform.getTranslateInstance(pos.x, pos.y);
		t.scale(zoom,zoom);
		t.concatenate(flip.getTransform());
		return t;
	}

	public boolean isResolved() {
		return c != null;
	}

	public Component getSubComponent() {
		assert isResolved();
		return c;
	}

	public void collectUsedParameters(Set<String> params) {
		try {
			deepCollectUsedParameters(getSubComponent(), params);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void localwire() {
		try {
			c = ComponentTable.load(data);
			// could also happen that c = null here, if file can not be parsed
		} catch (Exception ex) {
			c = null;
		}
	}

	public void rewire() {
		assert isValidRecursively();

		localwire();

		for (SubPinStub sps : subPinStubs.getChildren()) {
			sps.rewire();
		}
		assert isValidRecursively();
	}


	public void wire(SubComponents parent) {
		assert c == null;
		// resolve component before wiring stubs
		localwire();
		super.wire(parent);
	}

	public boolean isWellFormed() {
		assert super.isWellFormed();

		// assert one stub per name
		Set<String> stubnames = new HashSet<String>();
		for (SubPinStub sps : subPinStubs.getChildren()) {
			assert sps.getSubComponent() == this;
			assert stubnames.add(sps.getPinRef()) : sps.getPinRef() + " has two stubs";
		}

		// TODO: verify resolvedness state

		if (isResolved()) {
			// make sure that resolvable pins agree
			for (SubPinStub sps : subPinStubs.getChildren()) {
				assert
					sps.isResolved() ==
					(c.getIo().getByName(sps.getPinRef()) != null);
			}
		} else {
			// ensure that all stubs are unresolved
			for (SubPinStub sps : subPinStubs.getChildren()) {
				assert !sps.isResolved();
			}
		}

		// assure all stubs are necessary
		for (SubPinStub sps : subPinStubs.getChildren()) {
			assert sps.spn != null : sps.getPinRef() + " unnecessary";
		}

		return true;
	}

	public boolean hasStub(SubPinStub sps) {
		return subPinStubs.getChildren().contains(sps);
	}


	public void collectAffected(Set<BasicEditorNode> nodes) {
		SubPinStubs spss = getSubPinStubs();
		for (SubPinStub sps : spss.getChildren()) {
			SubPinNode spn = sps.getNode();
			for (Edge e: spn.getEdges()) {
				nodes.add(e.getSpan());
			}
		}
	}
}
