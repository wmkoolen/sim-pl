/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.Set;

import namespace.ParameterContext;
import nbit.NumberFormat;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class IntResolver {

	protected IntResolver() {}

//	public abstract Integer getValue(CompiledNode context);
	public abstract Integer getValue(ParameterContext<String, Integer> context);

	public abstract void collectUsedParameters(Set<String> params);

	static class Int extends IntResolver {
		int i;
		public Int(int i) {
			this.i = i;
		}

//		public Integer getValue(CompiledNode context) { return i; }
		public Integer getValue(ParameterContext<String, Integer> context) { return i; }
		public void collectUsedParameters(Set<String> params) {}
		public String toString() { return Integer.toString(i); }

		public boolean equals(Object o) {
			return (o instanceof Int) && ((Int)o).i == this.i;
		}
	}


	static class Param extends IntResolver {
		String p;

		public Param(String p) {
			this.p = p;
		}

//		public Integer getValue(CompiledNode context) {
//			if (resolver == null) resolver = new Resolver();
//
//			return context.handle(resolver, null);
//		}

		public Integer getValue(ParameterContext<String, Integer> context) {
			return context.get(p);
		}

		public boolean equals(Object o) {
			return (o instanceof Param) && ((Param)o).p.equals(this.p);
		}

		public void collectUsedParameters(Set<String> params) {
			params.add(p);
		}

		public String toString() { return "${"+p+"}"; }

//		Resolver resolver;
//
//		class Resolver implements CTWalkerR<Integer,Integer> {
//			public Integer handle(Action a, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(ParameterVal p, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//			public Integer handle(ParameterDecl p, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Parameters p, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Cable c, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Cables c, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(CableTarget_Pin c, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(CableTarget_Split c, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(CableSource_Pin c, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Complex c, Integer bestDefault) {
//				return handleComponent(c, bestDefault);
//			}
//
//			public Integer handle(Internals i, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(IO io, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Pin_Simulated_Input i, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Pin_Simulated_Output o, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Memory m, Integer bestDefault) {
//				return m.parent.handle(this, bestDefault);
//			}
//
//			public Integer handleComponent(Component c, Integer bestDefault) {
//				// see if our parent knows better
//				if(bestDefault == null) {
//					ParameterDecl par = c.parameters.getParameter(p);
//					if (par != null) {
//						if (par.type.equals("integer")) bestDefault = new Integer(par.defaultv);
//						else throw new ExceptionHandler("Expected integer type, found " + p);
//					}
//				}
//
//				if (c.getParent() == null) return bestDefault;
//
//				return c.getParent().handle(this,bestDefault);
//			}
//
//			public Integer handle(Simple s, Integer bestDefault) {
//				return handleComponent(s, bestDefault);
//			}
//
//			public Integer handle(Storage s, Integer bestDefault) {
//				return s.parent.handle(this, bestDefault);
//			}
//
//			public Integer handle(SubComponent s, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(SubComponents s, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Forms f, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Line f, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Polygon p, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Rectangle r, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Text t, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Ellipse e, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//
//			public Integer handle(Label l, Integer bestDefault) {
//				throw new Error("Not Implemented");
//			}
//		}
	}

	public static IntResolver valueOf(int i)  {
		return new Int(i);
	}

	public static IntResolver valueOf(String v) throws Exception {
		return parseInt(v);
	}

	public static IntResolver parseInt(String v) throws Exception {
		if (v.startsWith("${") && v.endsWith("}")) {
			if (v.length() <= 3) throw new Exception("empty parameter name");
			return new Param(v.substring(2,v.length()-1));
		} else {
			return new Int(NumberFormat.parse_nBit(v, NumberFormat.DECIMAL).toInt());
		}
	}
}
