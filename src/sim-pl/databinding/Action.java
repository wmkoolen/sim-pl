/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;



/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Action extends EditorNode<Internals> {
	Event event;
	String code;

	public Action() {
	}

	public Action(Event event, String code) {
		this.event = event;
		this.code = code;
	}

	public String toString() {
		return event.toString();
	}

	public Event getEvent() { return event; }
	public void setEvent(Event event) { this.event =  event; }

	public String getCode() { return code; }
	public void setCode(String code) { this.code = code; }



	public static class CodeBlock {
		public String code;
		public Simple component;
//		public Type_Struct type;
		public CodeBlock(String code, Simple component /*, Type_Struct type */) {
			this.code = code;
			this.component = component;
//			this.type = type;
		}

//		public Type_Struct getType() {
//			Type_Struct type = new Type_Struct();
//			ParameterContext<String, Integer> pc = new ParameterContext<String, Integer>();
//			ComponentNamespace cns = new ComponentNamespace(component);
//			CompilationMessageBuffer cmb = new CompilationMessageBuffer();
//			cns.compile(pc, cmb, type);
//
//			return type;
//		}
	}

	public CodeBlock getCodeBlock() {
		return new CodeBlock(getCode(), parent.parent);
	}

	public void setCodeBlock(CodeBlock w) {
		setCode(w.code);
	}


//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		try {
//			return new component.Action(event, code);
//		} catch (Exception ex) {
//			cmb.add(new CompilationMessage(this, ex.getMessage(), true));
//			return null;
//		}
//	}
}
