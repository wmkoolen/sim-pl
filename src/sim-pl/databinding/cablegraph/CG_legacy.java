/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.cablegraph;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import util.GUIHelper;
import databinding.Complex;
import databinding.Pin;
import databinding.SubComponent;
import databinding.SubPinStub;
import databinding.myStroke;
import databinding.forms.HAlignment;
import databinding.forms.PPoint;
import databinding.forms.VAlignment;
import databinding.wires.MainPinStub;
import databinding.wires.Node;
import databinding.wires.Split;
import databinding.wires.SubPinNode;
import databinding.wires.Wire;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class CG_legacy {

	public static abstract class CGNode {
		public String ref;

		abstract public Node realise(Complex x) throws Exception;
	}

	public static class CGSplit extends CGNode {
		public Color color;
		public PPoint pPos;

		public Node realise(Complex x) {
			Split s = new Split(new Point2D.Double(pPos.getX(), pPos.getY()));
			Wire w = new Wire(x.getWires().createNewName("Wire"), color, new myStroke(GUIHelper.unitStroke));
			w.getNodes().add(s);
			x.getWires().add(w);

			return s;
		}
	}

	public static class CGPinNode extends CGNode {
		public String pinRef;

		public Node realise(Complex x) throws Exception {

			int d = pinRef.indexOf(":");
			if (d == -1) {
// main node
				Pin p = x.getIo().getByName(pinRef);
				assert p != null : pinRef + " not found in " + x;
				MainPinStub s = p.getNode();
				if (s == null) {
					s = new MainPinStub(p);
					p.setNode(s);
					Wire w = new Wire(x.getWires().createNewName("Wire"),
									  Color.black,
									  new myStroke(GUIHelper.unitStroke));
					w.getNodes().add(s);
					x.getWires().add(w);
				}
				return s;
			} else {
				String subref = pinRef.substring(0, d);
				String pinref = pinRef.substring(d + 1);
				// sub node
				SubComponent sc = x.getSubcomponents().getByName(subref);
				Pin p = sc.getSubComponent().getIo().getByName(pinref);
				SubPinStub sps = p == null
					? new SubPinStub(pinref, new Point2D.Double(sc.getX(), sc.getY()))
					: new SubPinStub(p);
				SubPinNode s = new SubPinNode(sps);
				sps.setNode(s);
				Wire w = new Wire(x.getWires().createNewName("Wire"),
								  Color.black,
								  new myStroke(GUIHelper.unitStroke));
				w.getNodes().add(s);
				sc.getSubPinStubs().add(sps);
				x.getWires().add(w);
				return s;
			}
		}
	}


	public static class CGEdge {
		public CGNode node1, node2;
		public Color color;
		public myStroke stroke;
		public ArrayList<PPoint> pPoints;
	}

	public static class CGProbe {
		public Point2D.Double pos = new Point2D.Double();
		public HAlignment halign;
		public VAlignment valign;
		public CGNode node;

		public double getX() { return pos.getX();}
		public void setX(double x) { pos.setLocation(x, pos.getY()); }
		public double getY() { return pos.getY(); }
		public void setY(double y) { pos.setLocation(pos.getX(), y); }

	}


	public ArrayList<CGNode> nodes   = new ArrayList<CGNode>();
	public ArrayList<CGEdge> edges   = new ArrayList<CGEdge>();
	public ArrayList<CGProbe> probes = new ArrayList<CGProbe>();

}
