/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.geom.Point2D;
import java.util.Set;

import util.GUIHelper.Alignment;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PinLabel extends EditorNode<Pin> implements IAligned, IPositioned, IMoveable {
	HAlignment halign = HAlignment.center;
	VAlignment valign = VAlignment.center;
	Point2D.Double pos = new Point2D.Double();

	public PinLabel() {}

	public PinLabel(Alignment a, Point2D pos) {
		this(a.halign, a.valign, pos);
	}

	public PinLabel(HAlignment hAlignment, VAlignment vAlignment, Point2D pos) {
		this.halign = hAlignment;
		this.valign = vAlignment;
		this.pos.setLocation(pos);
	}

	public String toString() { return "Label"; }

	public boolean canBeDetached() { return false; }
	
	public void collectInvolvedInMove(Set<BasicEditorNode> selection, Set<IMoveable> movers) {
		if (!selection.contains(parent)) movers.add(this);
	}

//	public Color getColor() { return color; }
//	public void setColor(Color c) { this.color = c; }

	public HAlignment getHalign() { return halign; }
	public void setHalign(HAlignment h_align) { this.halign = h_align; }

	public VAlignment getValign() { return valign; }
	public void setValign(VAlignment v_align) { this.valign = v_align; }

	public double getX() { return pos.x; }
	public void setX(double x) { pos.x = x; }

	public double getY() { return pos.y; }
	public void setY(double y) { pos.y = y;	}

	public Point2D.Double getPos() { return pos; }
	public Point2D.Double getExternalPos() {
		return new Point2D.Double(pos.x + parent.getPos().x,
								  pos.y + parent.getPos().y); }

	public void move(int dx, int dy) {
		pos.x += dx;
		pos.y += dy;
	}
	
	public void collectAffected(Set<BasicEditorNode> nodes) {
		nodes.add(this); // no other nodes depend on our position
	}	
}
