/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.BasicStroke;

import util.GUIHelper;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class myStroke {
	private BasicStroke s;

	public myStroke() {
		this.s = GUIHelper.unitStroke;
	}

	public myStroke(BasicStroke s) {
		this.s = s;
	}

	public float getLineWidth() { return s.getLineWidth(); }
	public void setLineWidth (float lineWidth) {
		s = new BasicStroke(lineWidth, s.getEndCap(), s.getLineJoin(), s.getMiterLimit(), s.getDashArray(), s.getDashPhase());
	}

	public float [] getDashing() {
		return s.getDashArray();
	}

	public void setDashing(float [] ds) {
		s = new BasicStroke(s.getLineWidth(), s.getEndCap(), s.getLineJoin(), s.getMiterLimit(), ds, s.getDashPhase());
	}


	public int getEndCap() { return s.getEndCap(); }
	public void setEndCap(int ec) {
		s = new BasicStroke(s.getLineWidth(), ec, s.getLineJoin(), s.getMiterLimit(), s.getDashArray(), s.getDashPhase());
	}


	public int getLineJoin() { return s.getLineJoin(); }
	public void setLineJoin(int lj) {
		s = new BasicStroke(s.getLineWidth(), s.getEndCap(), lj, s.getMiterLimit(), s.getDashArray(), s.getDashPhase());
	}

	public float getMiterLimit() {  return s.getMiterLimit(); }

	public void setMiterLimit(float ml) {
		s = new BasicStroke(s.getLineWidth(), s.getEndCap(), s.getLineJoin(), ml, s.getDashArray(), s.getDashPhase());
	}

	public BasicStroke getStroke() { assert s != null; return s; };


	public Float getXMLLineWidth() {
		return s.getLineWidth() == GUIHelper.unitStroke.getLineWidth()
			? null
			: s.getLineWidth();
	}
	public Integer getXMLEndCap() {
		return s.getEndCap() == GUIHelper.unitStroke.getEndCap()
			? null
			: s.getEndCap();
	}
	public Integer getXMLLineJoin() {
		return s.getLineJoin() == GUIHelper.unitStroke.getLineJoin()
			? null
			: s.getLineJoin();
	}

	public Float getXMLMiterLimit() {
		return
			s.getLineJoin() != BasicStroke.JOIN_MITER ||
			s.getMiterLimit() == GUIHelper.unitStroke.getMiterLimit()
			? null
			: s.getMiterLimit();
	}
}
