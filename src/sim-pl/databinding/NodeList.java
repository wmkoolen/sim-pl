/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.ArrayList;
import java.util.Collection;

import namespace.Namespace;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class NodeList<P extends BasicEditorNode, T extends BasicEditorNode<? extends BasicEditorNode>> extends AbstractNodeList<P,T>  {

	private ArrayList<SubTreeListener> ls = new ArrayList<SubTreeListener>();
	public final void addSubtreeListener(SubTreeListener l) { assert !ls.contains(l); ls.add(l); }
	public final void removeSubtreeListener(SubTreeListener l) { assert ls.contains(l); ls.remove(l); }


	private final void fireSubtreeAttachedEvent(NodeList parent, BasicEditorNode child, int index) {
		for (SubTreeListener l : new ArrayList<SubTreeListener>(ls)) { // work on a copy to prevent concurrent modificaiton exceptions
			l.subTreeAttached(parent, child, index);
		}
	}

	private final void fireSubtreeDetachedEvent(NodeList parent, BasicEditorNode child, int index) {
		for (SubTreeListener l : new ArrayList<SubTreeListener>(ls)) { // work on a copy to prevent concurrent modificaiton exceptions
			l.subTreeDetached(parent, child, index);
		}
	}




	protected ArrayList<T> children = new ArrayList<T>() {
		public boolean add(T node) {
			assert parent == null || node.getParent() == NodeList.this : node + " is already child of " + node.getParent();
			boolean b = super.add(node);
			fireSubtreeAttachedEvent(NodeList.this, node, size()-1);
			return b;
		}

		public void add(int index, T node) {
			assert parent == null || node.getParent() == NodeList.this;
			super.add(index, node);
			fireSubtreeAttachedEvent(NodeList.this, node, index);
		}

		public boolean addAll(Collection<? extends T> nodes) {
			boolean change = false;
			for (T n : nodes) { change |= add(n); }
			return change;
		}

		public boolean addAll(int index, Collection<? extends T> nodes) {
			throw new Error("Unexpected method call");
//			return super.addAll(index, nodes);
		}

		public boolean remove(Object node) {
			int i = indexOf(node);
			assert i != -1;
			T child = get(i);
			assert (child == node);
			assert child.getParent() == NodeList.this;
			boolean b = super.remove(child);
			assert b;
			fireSubtreeDetachedEvent(NodeList.this, child, i);
			return true;
		}

		public T remove(int index) {
			T node = super.remove(index);
			assert node.getParent() == NodeList.this;
			fireSubtreeDetachedEvent(NodeList.this, node, index);
			return node;
		}

		public void removeRange(int from, int to) {
			throw new Error("Unexpected method call");
		}

		public void clear() {
			super.clear();
		}

		public T set(int index, T node ) {
			throw new Error("Unexpected method call");
			//return super.set(index, node);
		}
	};

	public ArrayList<T> getChildren() { return children; }

	public void collectDefs(Namespace<String, T, BasicEditorNode> ns) {
		for (T c: getChildren()) {
			if (c instanceof NameDeclaringNode) {
				ns.addDefiner(((NameDeclaringNode)c).getName(), c);
			}
		}
	}

	public Mutator add(final T child) {
		assert child != null;
		return new Mutator() {

			public void pre() {
				assert child.getParent() == null;
				assert !getChildren().contains(child);
			}

			public void commit() {
				((BasicEditorNode)child).setParent(NodeList.this);
				getChildren().add(child);
			}

			public void post() {
				assert getChildren().get(getChildren().size()-1) == child;
			}

			public void rollback() {
				getChildren().remove(getChildren().size()-1);
				child.setParent(null);
			}
		}.perform();
	}

	public Mutator remove(T child) {
		return removePure(child);
	}

	public final Mutator removePure(final T child) {
		assert child != null;
		final int index = getChildren().indexOf(child);

		return new Mutator() {
			public void pre() {
				assert children.get(index) == child;
				assert child.getParent() == NodeList.this;
			}

			public void commit() {
				children.remove(index);
				child.setParent(null);
			}
			public void post() {
				assert !children.contains(child);
				assert child.getParent() == null;
			}

			public void rollback() {
				((BasicEditorNode)child).setParent(NodeList.this);
				children.add(index, child);
			}
		}.perform();
	}
}
