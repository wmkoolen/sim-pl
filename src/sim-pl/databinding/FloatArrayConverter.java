/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import org.exolab.castor.mapping.GeneralizedFieldHandler;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */



/**
 * The FieldHandler for the float [] class
 *
 */
public class FloatArrayConverter
	extends GeneralizedFieldHandler
{
	/**
	 * Creates a new MyDateHandler instance
	 */
	public FloatArrayConverter() {
		super();
		setCollectionIteration(false);
	}

	/**
	 * This method is used to convert the value when the
	 * getValue method is called. The getValue method will
	 * obtain the actual field value from given 'parent' object.
	 * This convert method is then invoked with the field's
	 * value. The value returned from this method will be
	 * the actual value returned by getValue method.
	 *
	 * @param value the object value to convert after
	 *  performing a get operation
	 * @return the converted value.
	 */
	public Object convertUponGet(Object value) {
		if (value == null) return null;

		float [] fs = (float [])value;
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < fs.length; i++) {
			if (i > 0) b.append(", ");
			b.append(Float.toString(fs[i]));
		}
		return b.toString();
	}


	/**
	 * This method is used to convert the value when the
	 * setValue method is called. The setValue method will
	 * call this method to obtain the converted value.
	 * The converted value will then be used as the value to
	 * set for the field.
	 *
	 * @param value the object value to convert before
	 *  performing a set operation
	 * @return the converted value.
	 */
	public Object convertUponSet(Object value)  {
		try {
			String [] pts = ((String)value).split("\\s*,\\s*");
			float [] fs = new float[pts.length];
			for (int i = 0; i < fs.length; i++) {
				fs[i] = Float.parseFloat(pts[i]);
			}
			return fs;
		} catch (NumberFormatException ex) {
			return null;
		}
	}

	/**
	 * Returns the class type for the field that this
	 * GeneralizedFieldHandler converts to and from. This
	 * should be the type that is used in the
	 * object model.
	 *
	 * @return the class type of of the field
	 */
	public Class getFieldType() {
		return float[].class;
	}



	/**
	 * Creates a new instance of the object described by
	 * this field.
	 *
	 * @param parent The object for which the field is created
	 * @return A new instance of the field's value
	 * @throws IllegalStateException This field is a simple
	 *  type and cannot be instantiated
	 */
	public Object newInstance( Object parent )
		throws IllegalStateException
	{
		//-- Since it's marked as a string...just return null,
		//-- it's not needed.
		return null;
	}
}

