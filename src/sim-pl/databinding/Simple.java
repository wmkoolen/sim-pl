/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.Namespace;
import namespace.ParameterContext;
import util.Tuple;

import compiler.LocatedException;
import compiler.wisent.Type;
import compiler.wisent.Type_Array;
import compiler.wisent.Type_Simple;
import compiler.wisent.Type_Struct;
import compiler.wisent.Wisent;

import databinding.compiled.Compiled;
import databinding.compiled.Compiled.CompiledSimple;


/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Simple extends Component {

	Memory memory = new Memory();
	Internals internals = new Internals();

	 {
		children.add(memory);
		children.add(internals);
	}

	public Simple() {} // XML

	public Simple(String name) {
		super(name);
		memory.setParent(this);
		internals.setParent(this);
	}

//	public void dispatch(ComponentCallback callback) { callback.callback(this); }


	public Memory getMemory() { return memory; }
	public Internals getInternals() { return internals; }


	public void collectDefs(ComponentNamespace cns) {
		super.collectDefs(cns);
		memory.collectDefs(cns.mem);
	}




	public void populateType(CompiledSimple tt, ParameterContext<String, Integer> pc, ComponentNamespace cns, CompilationMessageBuffer cmb) {
		Type_Struct type = tt.type;

		Map<Storage, Tuple<Integer>> storage2wb = new IdentityHashMap<Storage,Tuple<Integer>>();
		// compile memories (resolving parameters along the way)
		for (Map.Entry<String, Namespace.RefDef<Storage, BasicEditorNode>> pe : cns.mem.entrySet()) {
			ArrayList<Storage> pdef = pe.getValue().def;
			assert!pdef.isEmpty();
			// only import successfully compiled pins
			if (pdef.size() == 1) {
				Storage p = pdef.get(0);
				Integer width = p.bits.getValue(pc);
				Integer size = p.size.getValue(pc);

				if (width != null && size != null) {
					storage2wb.put(p, new Tuple<Integer>(size, width));
				}

				if (size == null) {
					cmb.add(p, "unable to determine storage size from " + p.size);
				}
				if (width == null) {
					cmb.add(p, "unable to determine bit width from " + p.bits);
				}
			} else {
				for (Storage p : pdef) {
					cmb.add(p, pe.getKey() + " multiply defined", true);
				}
			}
		}


		// populate with pins
		for (Map.Entry<String, Namespace.RefDef<Pin, BasicEditorNode>> pe : cns.pin.entrySet()) {
			if (pe.getValue().def.size() == 1) {
				try {
					Pin p = pe.getValue().def.get(0);
					Integer bits = cns.pin2bits.get(p);
					if (bits == null) throw new Exception("Could not retrieve bit width of pin " + p);
					Type_Simple ts = new Type_Simple(bits, p.isSigned());
					type.set(pe.getKey(), new Type.Access(p.isOutput(), p.isInput(), ts));
				} catch (Exception ex) {
					ex.printStackTrace();
					cmb.add(this, ex);
				}
			}
		}
		// populate with storages
		for (Map.Entry<String, Namespace.RefDef<Storage, BasicEditorNode>> pe : cns.mem.entrySet()) {
			if (pe.getValue().def.size() == 1) {
				try {
					Storage p = pe.getValue().def.get(0);
					Tuple<Integer> wb = storage2wb.get(p);
					// wb = (size, width)
					if (wb.elems[0] == null) throw new Exception("Could not retrieve size of storage " + p);
					if (wb.elems[1] == null) throw new Exception("Could not retrieve bit width of storage " + p);
					Type_Simple ts = new Type_Simple(wb.elems[1], p.isSigned());
					type.set(pe.getKey(), new Type.Access(false, true, new Type_Array(wb.elems[0], ts)));
					tt.setBitsNWidth(p, wb.elems[1], wb.elems[0]);
				} catch (Exception ex) {
					cmb.add(this, ex);
				}
			}
		}
	}


	public CompiledSimple compile(ParameterContext<String, Integer> pc, Set<String> wiredPins, ComponentNamespace cns, CompilationMessageBuffer cmb) {
		Compiled.CompiledSimple tt = new Compiled.CompiledSimple(this);
		super.compile(tt, pc, wiredPins, cns, cmb);

		populateType(tt, pc, cns, cmb);

		if (internals.delay <= 0) {
			cmb.add(internals, "Impossible delay: " + internals.delay, true);
		}

		for (Action a : internals.children) {
			try {
				tt.wcdl[a.event.index] = Wisent.compiler.parse(tt.type, a.code);
			} catch (LocatedException ex) {
				cmb.add(a, ex);
			}
		}
		return tt;
	}





//	public RenderNode createRenderNode() {
//		final RenderNode rforms = forms.createRenderNode();
//		final RenderNode rio = io.createRenderNode();
//
//		return new RenderNode() {
//			public void render(Graphics2D g) {
//				rforms.render(g);
//				rio.render(g);
//			}
//		};
//	}
}
