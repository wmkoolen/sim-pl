/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import util.GUIHelper;
import util.PathIteratorFactory;
import databinding.InternalNode.Mutator;
import databinding.InternalNode.MutatorList;
import databinding.wires.Edge;
import databinding.wires.Span;
import databinding.wires.Split;
import databinding.wires.Wire;


/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SpanRenderNode extends RenderNode<Span> implements ControlPointContainer {
	public SpanRenderNode(Span peer) {
		super(peer);
	}

	public Mutator addControlPoint(Point2D p) {
		Edge e = peer.getClosestEdge(p);
		// create local copy of point, it might be moved
		Point2D pc = new Point2D.Double(p.getX(), p.getY());
		MutatorList muts  = new MutatorList();
		peer.getParent().getParent().getParent().splitEdge(e, pc, muts);
		return muts;
	}

	public Mutator removeControlPoint(ControlPoint cp) {
		SpanControlPoint ecp = (SpanControlPoint)cp;
		MutatorList muts  = new MutatorList();
		peer.getParent().getParent().getParent().dropNode(ecp.n, muts);
		return muts;
	}

	//  point unwrapper convenience method
	static final double ptSegDist(Point2D l1, Point2D l2, Point2D p) {
		return Line2D.ptSegDist(l1.getX(), l1.getY(), l2.getX(), l2.getY(), p.getX(), p.getY());
	}

	private class SpanControlPoint extends RegularControlPoint {
		public Span getAffected() { return peer;}
		public final Split n;
		public SpanControlPoint(Split n) {
			this.n = n;
		}

		public String getDescription() { return null;}

		public Point2D getPoint() {
			return n.getPos();
		}

		public void setPoint(Point2D pt) {
			n.setPos(pt);
			invalidate();
		}

	}


	public void collectControlPoints(Collection<ControlPoint> cps) {
		// only collect intermediate nodes
		for (int i = 1; i < peer.edges.size(); ++i) {
			Split n = (Split)peer.edges.get(i).getNode1();
			cps.add(new SpanControlPoint(n));
		}
	}

	private Shape shape = new EdgeShape();

	public void render(Graphics2D g) {
		Wire w = peer.getParent().getParent();
		g.setColor(w.getColor());
		g.setStroke(w.getStroke().getStroke());
		g.draw(shape);
	}

	// we cache selection shape for faster rendering
	private Shape selectionShape;

	public void invalidate() {
		super.invalidate();
		selectionShape = null;
	}

	public void renderSelectionMark(Graphics2D g) {
		if (selectionShape == null)
		{
			// grow the line by some small number of points
			BasicStroke s = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			selectionShape = s.createStrokedShape(shape);
		}
		// selection stroke is already set on g
		g.draw(selectionShape);
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		return GUIHelper.grow((5 + ((BasicStroke)g.getStroke()).getLineWidth())/2, shape.getBounds2D(), null);
	}
	
	


	public Rectangle2D getBound(Graphics2D g) {
		Rectangle2D r = new Rectangle2D.Double();
		Edge first = peer.edges.get(0);
		r.setFrameFromDiagonal(first.getNode1().getPos(), first.getNode2().getPos());
		for (Edge e : peer.edges) {
			r.add(e.getNode2().getPos());
		}
		GUIHelper.grow(1, r, r);
		return r;
	}


	public double getDistance(Point2D p, Graphics2D g) {
		double d = Double.POSITIVE_INFINITY;
		for (Edge e : peer.edges) {
			d = Math.min(d, e.dist(p));
		}
		return  d;
	}





	public class EdgeShape extends PathIteratorFactory {
		public PathIterator getPathIterator(AffineTransform at) {
			return new EdgePathIterator(at);
		}

		public Rectangle2D getBounds2D() {
			Edge first = peer.edges.get(0);
			Rectangle2D bound = new Rectangle2D.Double();
			bound.setFrameFromDiagonal(first.getNode1().getPos(), first.getNode2().getPos());

			for (Edge e : peer.edges) {
				bound.add(e.getNode2().getPos());
			}
			GUIHelper.grow(peer.getParent().getParent().getStroke().getLineWidth(), bound, bound);
			return bound;
		}


		// notes: the first point is asked before any call to 'next'
		//        isDone is called AFTER next, so there always is a failing 'next' call
		class EdgePathIterator implements PathIterator {
			int state = -1; // we start moving to source
			float[] coords = new float[2];
			int move = -1;
			AffineTransform at;

			public EdgePathIterator(AffineTransform at) {
				this.at = at != null ? at : new AffineTransform();

				Point2D p = peer.edges.get(0).getNode1().getPos();
				coords[0] = (float)p.getX();
				coords[1] = (float)p.getY();
				move = SEG_MOVETO;
			}

			public int getWindingRule() {
				return PathIterator.WIND_NON_ZERO;
			}

			public boolean isDone() {
				return state == peer.edges.size();
			}

			public void next() {
				state++;
				if (state == peer.edges.size()) return;
				Point2D p = peer.edges.get(state).getNode2().getPos();
				coords[0] = (float)p.getX();
				coords[1] = (float)p.getY();
				move = SEG_LINETO;
			}

			public int currentSegment(float[] coords) {
				at.transform(this.coords, 0, coords, 0, 1);
				return move;
			}

			public int currentSegment(double[] coords) {
				at.transform(this.coords, 0, coords, 0, 1);
				return move;
			}
		}

		public PathIterator getPathIterator(AffineTransform at,
											double flatness) {
			// just ignores the flatness parameter (we're generating straight line segments exclusively)
			return getPathIterator(at);
		}
	}

}
