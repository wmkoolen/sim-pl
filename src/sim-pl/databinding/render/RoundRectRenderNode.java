/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.util.Collection;

import databinding.forms.RoundRect;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class RoundRectRenderNode extends ShapeRenderNode<RoundRect> {
	public RoundRectRenderNode(RoundRect peer) {
		super(peer);
	}

	public Shape getShape() {
		// work around a java render bug: roundrects with both arcs set to zero
		// are rendered with a weird top-left corner.
		RoundRectangle2D r = peer.getShape();
		return (r.getArcHeight() == 0 || r.getArcWidth() == 0) ? r.getBounds2D() : r;
	}

	public double getDistance(Point2D p, Graphics2D g) {
		RoundRectangle2D r = peer.getShape();
		if (peer.getFill() != null && r.contains(p)) return 0;
		return minDist(r, p);
	}

	public static double minDist(RoundRectangle2D r, Point2D p) {
		return minDist(p.getX(), p.getY(), r.getX(), r.getY(), r.getWidth(), r.getHeight(), r.getArcWidth()/2, r.getArcHeight()/2);
	}

	public static double minDist(double px, double py, double x, double y, double width, double height, double arcwidth, double archeight) {
		if (px < x+arcwidth) {
			if (py < y+archeight)        return EllipseRenderNode.dist(px, py, x+arcwidth,       y+archeight,        arcwidth, archeight);
			if (py > y+height-archeight) return EllipseRenderNode.dist(px, py, x+arcwidth,       y+height-archeight, arcwidth, archeight);
		}
		if (px > x+width-arcwidth) {
			if (py < y+archeight)        return EllipseRenderNode.dist(px, py, x+width-arcwidth, y+archeight,        arcwidth, archeight);
			if (py > y+height-archeight) return EllipseRenderNode.dist(px, py, x+width-arcwidth, y+height-archeight, arcwidth, archeight);
		}
		assert (x+arcwidth  <= px && px <= x+width -arcwidth) ||
			   (y+archeight <= py && py <= y+height-archeight);

		if (x+arcwidth <= px && px <= x+width-arcwidth) {
			if (py < y+archeight)        return Math.abs(y-py);
			if (py > y+height-archeight) return Math.abs(py-y-height);
		}

		assert (y+archeight <= py && py <= y+height-archeight);
		if (px < x+arcwidth)           return Math.abs(x-px);
		if (px > x+width-arcwidth)     return Math.abs(px-x-width);


		assert (x+arcwidth  <= px && px <= x+width -arcwidth) &&
			   (y+archeight <= py && py <= y+height-archeight);
		return Math.min(Math.min(Math.abs(x-px), Math.abs(x + width - px)),
						Math.min(Math.abs(y-py), Math.abs(y + height - py)));
	}



	public void collectControlPoints(Collection<ControlPoint> cps) {
		// top left
		cps.add(new RegularControlPoint() {
			public RoundRect getAffected() { return peer;}
			public Point2D getPoint() {
				return new Point2D.Double(peer.getX(),
										  peer.getY());
			}

			public String getDescription() { return "top left"; }
			public void setPoint(Point2D t) {
				double x2 = peer.getX() + peer.getWidth();
				double y2 = peer.getY() + peer.getHeight();
				double x1 = Math.min((int)t.getX(), x2);
				double y1 = Math.min((int)t.getY(), y2);
				assert x1 <= x2 && y1 <= y2;


				peer.setX(x1);
				peer.setY(y1);
				peer.setWidth (x2 - x1);
				peer.setHeight(y2 - y1);
				invalidate();
			}
		});

		//bottom right
		cps.add(new RegularControlPoint() {
			public RoundRect getAffected() { return peer;}
			public Point2D getPoint() {
				return new Point2D.Double(peer.getX() + peer.getWidth(),
										  peer.getY() + peer.getHeight());
			}
			public String getDescription() { return "bottom right"; }
			public void setPoint(Point2D t) {
				peer.setWidth (Math.max(0, t.getX() - peer.getX()));
				peer.setHeight(Math.max(0, t.getY() - peer.getY()));

				invalidate();
			}
		});

		// horizontal rounding radius
		cps.add(new RegularControlPoint() {
			public RoundRect getAffected() { return peer;}
			public Point2D getPoint() {
				return new Point2D.Double(peer.getX() + peer.getWidth() - peer.getRadiusX(),
										  peer.getY());
			}
			public String getDescription() { return "horizontal rounding"; }

			public void setPoint(Point2D t) {

				// solve t.x = peer.getX() + peer.getWidth() - rx
				//       rx  = peer.getX() + peer.getWidth() - tx

				double rx = clip(peer.getWidth()+peer.getX()-t.getX(), 0, peer.getWidth()/2);
				peer.setRadiusX(rx);
				invalidate();
			}
		});

		// vertical rounding radius
		cps.add(new RegularControlPoint() {
			public RoundRect getAffected() { return peer;}
			public Point2D getPoint() {
				return new Point2D.Double(peer.getX()+peer.getWidth(),
										  peer.getY()+peer.getRadiusY());
			}

			public String getDescription() { return "vertical rounding"; }

			public void setPoint(Point2D t) {

				// solve t.y = peer.getY() + ry


				double ry = clip(t.getY()-peer.getY(), 0, peer.getHeight()/2);
				peer.setRadiusY(ry);
				invalidate();
			}
		});

	}

	static double clip(double v, double min, double max) {
		return Math.max(min, Math.min(max, v));
	}

}
