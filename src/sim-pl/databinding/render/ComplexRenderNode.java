/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import databinding.BasicEditorNode;
import databinding.Complex;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ComplexRenderNode extends RenderNode<Complex> {

	ArrayList<BasicEditorNode<?>> childrenInRenderOrder = new ArrayList<BasicEditorNode<?>>();

	public ComplexRenderNode(Complex peer) {
		super(peer);
		// we render in PCB order
		childrenInRenderOrder.add(peer.getForms());
//		childrenInRenderOrder.add(peer.getCables());
//		childrenInRenderOrder.add(peer.getCableGraph());
		childrenInRenderOrder.add(peer.getWires());
		childrenInRenderOrder.add(peer.getIo());
		childrenInRenderOrder.add(peer.getSubcomponents());
		assert peer.getChildren().containsAll(childrenInRenderOrder);
	}
	public double getDistance(Point2D p, Graphics2D g) { return Double.POSITIVE_INFINITY; }


	public List<? extends BasicEditorNode<?>> getChildrenInRenderOrder() {
		return childrenInRenderOrder;
	}
}
