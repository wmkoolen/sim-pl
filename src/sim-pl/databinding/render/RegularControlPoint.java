/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class RegularControlPoint implements ControlPoint {
	public static final double radius = 3;
	public static final float strokeWidth = 1;
	protected static final BasicStroke stroke = new BasicStroke(strokeWidth);
	private static final Color borderColor = Color.black;

	public static final Color defaultColor = new Color(0xfa,0xfa,0x3c/*,0xB0*/); // pastel yellow
	public static final Color hoverColor = Color.orange;
	public static final Color selectedColor = Color.red;


	// only create ellipse once, reset it each draw
	Ellipse2D e = new Ellipse2D.Double();

	//only create bounding rect once, reset it each query
	Rectangle2D b = new Rectangle2D.Double();

//	public RegularControlPoint(double x, double y) {
//		this(new Point2D.Double(x,y));
//	}

//	public RegularControlPoint(Point2D p) {
//		this.p = p;
//		this.e = new Ellipse2D.Double(p.getX()- radius, p.getY() - radius, 2*radius, 2*radius);
//	}

	public double distance(Point2D p) {
		return getPoint().distance(p);
	}

	private void render(Graphics2D g, Color interiorColor) {
		draw(g, getPoint(), e, interiorColor);
//		Point2D p = getPoint();
//		e.setFrame(p.getX()- radius, p.getY() - radius, 2*radius, 2*radius);
//
//		g.setColor(interiorColor);
//		g.fill(e);
//
//		g.setColor(borderColor);
//		g.setStroke(stroke);
//		g.draw(e);
	}

	public static Rectangle2D getBound(Point2D p, Rectangle2D r) {
		if (r == null) r = new Rectangle2D.Double();
		r.setFrame(p.getX() - radius-strokeWidth, p.getY() - radius-strokeWidth, 2*(radius+strokeWidth), 2*(radius+strokeWidth));
		return r;
	}

	public static void draw(Graphics2D g, Point2D p, Ellipse2D e, Color interior) {
		e.setFrame(p.getX()- radius, p.getY() - radius, 2*radius, 2*radius);

		g.setColor(interior);
		g.fill(e);

		g.setColor(borderColor);
		g.setStroke(stroke);
		g.draw(e);
	}

	public void render(Graphics2D g)         { render(g, defaultColor); }
	public void renderHover(Graphics2D g)    { render(g, hoverColor); }
	public void renderSelected(Graphics2D g) { render(g, selectedColor); }

	public Rectangle2D getBound() {
		Point2D p = getPoint();
		b.setFrame(p.getX()- radius, p.getY() - radius, 2*radius, 2*radius);
		GUIHelper.grow(stroke.getLineWidth()/2, b, b);
		return b;
	}

}
