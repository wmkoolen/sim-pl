/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import util.GUIHelper;
import databinding.myStroke;
import databinding.forms.Line;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class LineRenderNode extends RenderNode<Line>{

	public LineRenderNode(Line peer) {
		super(peer);
	}

	public void renderSelectionMark(Graphics2D g) {
		// bounding rectangle including line width
		Rectangle2D r = peer.getBounds(null);
		GUIHelper.grow(1, r, r);
		g.draw(r);
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		return GUIHelper.grow(1+((BasicStroke)g.getStroke()).getLineWidth()/2, peer.getBounds(null), null);
	}


	public void render(Graphics2D g) {
		myStroke stroke = peer.getStroke();
		g.setColor(peer.getColor());
		g.setStroke(stroke.getStroke());
		Point from = peer.getFrom();
		Point to = peer.getTo();

		g.drawLine(from.x, from.y, to.x, to.y);
	}

	public Rectangle2D getBound(Graphics2D g) {
		return peer.getBounds(g);
	}

	public double getDistance(Point2D p, Graphics2D g) {
		return Line2D.ptSegDist(peer.getFromX(), peer.getFromY(), peer.getToX(), peer.getToY(), p.getX(), p.getY());
	}

	public void collectControlPoints(Collection<ControlPoint> cps) {
		cps.add(new RegularControlPoint() {
			public Line getAffected() { return peer;}
			public Point2D getPoint() {
				return peer.getFrom();
			}

			public String getDescription() { return "endpoint"; }

			public void setPoint(Point2D p) {
				peer.setFromX((int)p.getX());
				peer.setFromY((int)p.getY());
				invalidate();
			}

		});
		cps.add(new RegularControlPoint() {
			public Line getAffected() { return peer;}
			public Point2D getPoint() {
				return peer.getTo();
			}
			public String getDescription() { return "endpoint"; }
			public void setPoint(Point2D p) {
				peer.setToX((int)p.getX());
				peer.setToY((int)p.getY());
				invalidate();
			}
		});
	}
}
