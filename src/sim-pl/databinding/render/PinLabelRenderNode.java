/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import util.GUIHelper.Alignment;
import databinding.PinLabel;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PinLabelRenderNode extends RenderNode<PinLabel> {
	public PinLabelRenderNode(PinLabel peer) {
		super(peer);
	}


	public static class Cache {
		// basic properties
		private Font f;
		private Point2D pos = new Point2D.Double();
		private String text;
		private HAlignment halign;
		private VAlignment valign;

		// derived, cached objects
		private TextLayout tl;
		private FontRenderContext frc;
		private Rectangle2D r;

		private float px, py;

		public void invalidate() {
			tl = null;
		}

		public void update(Graphics2D g, Font f, Point2D pos, String text, Alignment align) {
			update(g, f, pos, text, align.halign, align.valign);
		}
		public void update(Graphics2D g, Font f, Point2D pos, String text, HAlignment halign, VAlignment valign) {
			FontRenderContext curfrc = GUIHelper.getPureFRC(g);
			if (tl != null &&
				frc.equals(curfrc) &&
				halign == this.halign &&
				valign == this.valign &&
				f.equals(this.f) &&
				pos.equals(this.pos) &&
				text.equals(this.text)) return;
			// set cache state
			this.f = f;
			this.pos.setLocation(pos);
			this.text = text;
			this.halign = halign;
			this.valign = valign;
			// recompute derived objects
			frc = curfrc;
			tl = new TextLayout(text, f, frc);
			r = tl.getBounds();
			px = (float)pos.getX();
			py = (float)pos.getY();
			// (px, py) corresponds to drawing left-baseline

			if (valign == VAlignment.center) py += r.getHeight()/2;
			if (valign == VAlignment.top)    py += r.getHeight();

			if (halign == HAlignment.center) px -= r.getWidth()/2;
			if (halign == HAlignment.right)  px -= r.getWidth();

			GUIHelper.translate(r, px, py , r);

			// make room for cadre
			GUIHelper.grow(3, r, r);
		}

		public void render(Graphics2D g, Color c) {
			// fill text-box background
			g.setColor(c);
			g.fill(r);

			// draw text-box outline
			g.setColor(Color.black);
			g.draw(r);

			// render text in text-box
			if (false) {
				tl.draw(g, px, py);
			} else {
				g.translate(px, py);
				tl.draw(g, 0, 0);
				g.translate(-px, -py);
			}
		}

		public Rectangle2D getBound() {
			return r;
		}
	}

	Cache cache = new Cache();

	public void invalidate() {
		super.invalidate();
		cache.invalidate();
	}

	public double getDistance(Point2D p, Graphics2D g) {
		cache.update(g, g.getFont(), peer.getExternalPos(), peer.getParent().getName(), peer.getHalign(), peer.getValign());
		if (cache.r.contains(p)) return 0;
		return GUIHelper.minDist(cache.r, p);
	}

	public Rectangle2D getBound(Graphics2D g) {
		cache.update(g, g.getFont(), peer.getExternalPos(), peer.getParent().getName(), peer.getHalign(), peer.getValign());
		return cache.getBound();
	}

	public void render(Graphics2D g) {
		cache.update(g, g.getFont(), peer.getExternalPos(), peer.getParent().getName(), peer.getHalign(), peer.getValign());
		cache.render(g, PinRenderNode.getColor(peer.getParent()));
	}

	public void renderSelectionMark(Graphics2D g) {
		cache.update(g, g.getFont(), peer.getExternalPos(), peer.getParent().getName(), peer.getHalign(), peer.getValign());
		g.draw(GUIHelper.grow(1.5, cache.r, null));
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		cache.update(g, g.getFont(), peer.getExternalPos(), peer.getParent().getName(), peer.getHalign(), peer.getValign());
		return GUIHelper.grow(1.5 + ((BasicStroke)g.getStroke()).getLineWidth()/2, cache.r, null);
	}
}
