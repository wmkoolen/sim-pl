/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import util.GUIHelper.Alignment;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;
import databinding.wires.Probe;
import databinding.wires.Split;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ProbeRenderNode extends RenderNode<Probe>{

	public static final Color pinProbeColor = Color.pink;
	public static final Color wireProbeColor = Color.yellow;
	public static final Color probeOffsetColor = Color.orange;

	Rectangle2D.Double r = new Rectangle2D.Double();
	Line2D.Double      l = new Line2D.Double();
	Point2D.Double curPos = new Point2D.Double();


	private static final double radius = 4;


	public ProbeRenderNode(Probe peer) {
		super(peer);
	}

	public void invalidate() {
		getRealPos(curPos);
	}

	public void getRealPos(Point2D buf) {
		Point2D p1 = peer.getPos();
		Point2D p2 = peer.getParent().getPos();
		buf.setLocation(p1.getX() + p2.getX(),
						p1.getY() + p2.getY());
	}

	public double getDistance(Point2D p, Graphics2D g) {
		getRealPos(curPos);
		double dx = Math.max(0, Math.abs(curPos.x - p.getX())-radius);
		double dy = Math.max(0, Math.abs(curPos.y - p.getY())-radius);
		return Math.sqrt(dx*dx + dy*dy);
	}


	public static void getBound(Point2D realProbePos, Point2D parentPos, Rectangle2D r) {
		r.setFrame(realProbePos.getX()-radius, realProbePos.getY()-radius,2*radius,2*radius);
		r.add(parentPos); // include parent, we draw a line to it
		GUIHelper.grow(1, r, r);
	}

	private static Point2D getAlignmentPoint(Point2D probePos, Alignment a) {
		double dx =  a.halign == HAlignment.left  ? -radius
			      :  a.halign == HAlignment.right ? +radius
				  :                               0;
		double dy = a.valign == VAlignment.top    ? -radius
			      : a.valign == VAlignment.bottom ? +radius
				  :                               0;
		if (dx == 0 && dy == 0) return probePos;
		else return new Point2D.Double(probePos.getX() + dx, probePos.getY() + dy);
	}

	public static void render(Graphics2D g, Point2D realProbePos, Alignment a, Point2D parentPos, boolean wireProbe, Line2D l, Rectangle2D r) {
		if (!realProbePos.equals(parentPos)) {
			// draw line connecting probe to node
			Point2D alignmentPoint = getAlignmentPoint(realProbePos, a);
			l.setLine(parentPos, alignmentPoint);
			g.setStroke(GUIHelper.unitStroke);
			g.setColor(probeOffsetColor);
			g.draw(l);
		}

		r.setFrame(realProbePos.getX()-radius, realProbePos.getY()-radius,2*radius,2*radius);
		g.setColor(wireProbe ? wireProbeColor : pinProbeColor);
		g.fill(r);
		g.setColor(Color.black);
		g.setStroke(GUIHelper.unitStroke);
		g.draw(r);
	}

	public Rectangle2D getBound(Graphics2D g) {
		getRealPos(curPos);
		getBound(curPos, peer.getParent().getPos(), r);
		return r;
	}

	public void render(Graphics2D g) {
		getRealPos(curPos);
		render(g, curPos, new Alignment(peer.getHalign(), peer.getValign()), peer.getParent().getPos(), peer.getParent() instanceof Split, l, r);
	}

	public void renderSelectionMark(Graphics2D g) {
		getRealPos(curPos);
		r.setFrame(curPos.x-radius, curPos.y-radius,2*radius,2*radius);
		GUIHelper.grow(2, r, r);
		g.draw(r);
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		getRealPos(curPos);
		r.setFrame(curPos.x-radius, curPos.y-radius,2*radius,2*radius);
		return GUIHelper.grow(2 + ((BasicStroke)g.getStroke()).getLineWidth()/2, r, r);
	}
	
}
