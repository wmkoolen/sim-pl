/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;

import databinding.InternalNode.Mutator;
import databinding.forms.PPoint;
import databinding.forms.Polygon;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PolygonRenderNode extends ShapeRenderNode<Polygon> implements ControlPointContainer{

	public PolygonRenderNode(Polygon peer) {
		super(peer);
	}

	java.awt.Polygon polygon = null;

	public void invalidate() {
		polygon = null;
	}

	public java.awt.Polygon getShape() {
		if (polygon == null) {
			polygon = new java.awt.Polygon();
			for (PPoint p : peer.getChildren()) {
				// TODO: transform to doubles
				polygon.addPoint((int)Math.round(p.getX()), (int)Math.round(p.getY()));
			}
		}
		return polygon;
	}

	public double getDistance(Point2D p, Graphics2D g) {
		// handle border case: empty polygon
		if (peer.getChildren().isEmpty()) return Double.POSITIVE_INFINITY;

		if (peer.getFill() != null && getShape().contains(p)) return 0;


		double r2 = Double.POSITIVE_INFINITY;
		// go to the last point;
		PPoint prev = peer.getChildren().get(peer.getChildren().size()-1);
		for (PPoint cur : peer.getChildren()) {
			r2 = Math.min(r2, Line2D.ptSegDistSq(prev.getX(), prev.getY(), cur.getX(), cur.getY(), p.getX(), p.getY()));
			prev = cur;
		}

		return Math.sqrt(r2);
	}

	public void collectControlPoints(Collection<ControlPoint> cps) {
		for (int i = 0; i < peer.getChildren().size(); i++) {
			cps.add(new PolygonControlPoint(i));
		}
//		for (final PPoint p : peer.getChildren()) {
//			cps.add(new PolygonControlPoint() {
//				public Point2D getPoint() {
//					return new Point2D.Double(p.getX(), p.getY());
//				}
//				public void setPoint(Point2D t) {
//					p.setX((int)t.getX());
//					p.setY((int)t.getY());
//
//					invalidate();
//				}
//			});
//		}
	}

	private int bestIndex(Point2D p) {
		int besti = -1;
		double bestd = Double.POSITIVE_INFINITY;

		for (int i = 0; i < peer.getPoints().size(); i++) {
			PPoint left = i== 0
				? peer.getPoints().get(peer.getPoints().size()-1)
				: peer.getPoints().get(i-1);

			PPoint right = peer.getPoints().get(i);

			// compute differential wire length when adding point at index i
			double d = left.distance(p) + right.distance(p) - left.distance(right);
			if (d < bestd ) {
				bestd = d;
				besti = i;
			}
		}
		return besti;
	}

	public Mutator addControlPoint(Point2D p) {
		final int index = bestIndex(p);
		final PPoint pc = new PPoint(p);

		return new Mutator() {
			public void pre() {
				assert !peer.getPoints().contains(pc);
			}

			public void commit() {
				peer.getPoints().add(index, pc);
				invalidate();
			}
			public void post() {
				assert peer.getPoints().get(index).equals(pc);
			}
			public void rollback() {
				peer.getPoints().remove(index);
				invalidate();
			}
		}.perform();
	}

	public Mutator removeControlPoint(ControlPoint cp) {
		final PolygonControlPoint ecp = (PolygonControlPoint)cp;
		final PPoint p = peer.getPoints().get(ecp.index);

		return new Mutator() {
			public void pre() {
				assert peer.getPoints().get(ecp.index) == p;
			}

			public void commit() {
				peer.getPoints().remove(ecp.index);
				invalidate();
			}
			public void rollback() {
				peer.getPoints().add(ecp.index, p);
				invalidate();
			}
			public void post() {
				assert !peer.getPoints().contains(p);
			}
		}.perform();
	}


	private class PolygonControlPoint extends RegularControlPoint {
		public final int index;
		public PolygonControlPoint(int index) {
			this.index = index;
		}
		public String getDescription() { return null; }
		public Point2D getPoint() {
			return peer.getPoints().get(index).toPoint();
		}

		public void setPoint(Point2D pt) {
			peer.getPoints().set(index, new PPoint(pt));
			invalidate();
		}
		
		public Polygon getAffected() { return peer;}
	}

}
