/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.SolidFill;
import databinding.StrokedOutline;
import databinding.forms.AreaForm;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class ShapeRenderNode<T extends AreaForm> extends RenderNode<T> {

	public ShapeRenderNode(T peer) {
		super(peer);
	}

	public abstract Shape getShape();

//	public void renderSelectionMark(Graphics2D g) {
//		g.draw(getShape());
//	}


	public final void render(Graphics2D g) {
		Shape s = getShape();
		SolidFill fill = peer.getFill();
		if (fill != null) {
			g.setColor(fill.getColor());
			g.fill(s);
		}

		StrokedOutline outline = peer.getOutline();
		if (outline  != null) {
			g.setColor(outline.getColor());
			g.setStroke(outline.getStroke().getStroke());
			g.draw(s);
		}
	}

	public final Rectangle2D getBound(Graphics2D g) {
		Rectangle2D r = getShape().getBounds2D();
		StrokedOutline outline = peer.getOutline();
		if (outline != null) {
			float lw = outline.getStroke().getLineWidth();
			assert lw >= 0;
			r = GUIHelper.grow(lw/2, r, r);
		}
		return r;
	}
}
