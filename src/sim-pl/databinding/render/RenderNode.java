/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.List;

import util.GUIHelper;
import databinding.BasicEditorNode;
import databinding.InternalNode;
import databinding.compiled.Compiled.CompiledComponent;
import editor.RenderAdapter.NodeDist;
import editor.RenderAdapter.NodeRepresentantMapping;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

/* RenderNode
 * Graphics related information about a BasicEditorNode
 * - drawing
 * - distance computation (e.g. to mouse cursor)
 * - bound computation
 * - selection decoration
 * - obtaining control points
 *
 * Note that Swing tries very hard to make text look good.  For
 * example, it uses fancy algorithms for inter-letter spacing that
 * depend on the actual size (resolution/magnification) the text is
 * rendered at.  As a result, the _size_ of text depends on the
 * context. That is, the FontRenderContext. The easiest way to get
 * these FRCs is from Graphics2D. Hence in general, to compute the
 * bounding box of a BasicEditorNode, we need to supply a Graphics2D
 * object.
 */


public abstract class RenderNode<T extends BasicEditorNode<?>> {
	protected final T peer;

	public RenderNode(T peer) {
		this.peer = peer;
	}

	public void render(Graphics2D g, CompiledComponent<?> cc) {
		render(g);
	}

	public void render(Graphics2D g) {}
	public Rectangle2D getBound(Graphics2D g) { return null; }
	public abstract double getDistance(Point2D p, Graphics2D g);


	public void renderSelectionMark(Graphics2D g) {
		Rectangle2D r = getBound(g);
		if (r == null) return;

		g.draw(r);
	}

	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		Rectangle2D r = getBound(g);
		if (r != null) GUIHelper.grow(((BasicStroke)g.getStroke()).getLineWidth()/2, r, r);
		return r;
	}


	public <X> void getNode(Point2D p, Graphics2D g, NodeDist<X> nd, NodeRepresentantMapping<X> n2m) {
		double d = getDistance(p, g);
		assert d >= 0 : "distance from " + peer + " to " + p + " is " + d;
		if (d <= nd.d) { // also replace on equality, we want frontmost first
			nd.d = d;
			nd.node = n2m.getRepresentant(peer);
		}
	}



	/** invalidates LOCAL information, that is information that does not depend on subcomponents.
	**/
	public void invalidate() {}


	public void collectControlPoints(Collection<ControlPoint> p) {}


	// nodes which are returned FIRST will be rendered FIRST, and will
	// thus be at the deepest Z level, possibly overwritten later with
	// nodes that are returned LATER
	public List<? extends BasicEditorNode<?>> getChildrenInRenderOrder() {
		if (peer instanceof InternalNode) return ((InternalNode<?,?>)peer).getChildren();
		return null;
	}

	public static Rectangle2D union(Rectangle2D srcA, Rectangle2D srcB) {
		if (srcA == null) return srcB;
		if (srcB == null) return null;
		Rectangle2D.Double result = new Rectangle2D.Double();
		Rectangle2D.union(srcA, srcB, result);
		return result;
	}
}
