/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.Flip;
import databinding.forms.HAlignment;
import databinding.forms.Text;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class TextRenderNode extends RenderNode<Text> {

	public TextRenderNode(Text peer) {
		super(peer);
	}

	public void invalidate() {
		tlm = null;
	}


	TextLayoutMan tlm;

	public TextLayoutMan getTLM(Graphics2D g) {
		if (tlm == null) tlm = new TextLayoutMan(peer, g, peer.getFont());
		return tlm;
//		return new TextLayoutMan(peer, g, g.getFont());
	}

	public void render(Graphics2D g) {
		getTLM(g).draw(g);


//		int radius = 200;
//		Point p = peer.getPos();
//		for (int x = p.x-radius; x < p.x + radius; x += 2) {
//			for (int y = p.y-radius; y < p.y + radius; y += 2) {
//				double d = getDistance(new Point2D.Double(x,y));
//				if (d < 10) g.fillRect(x,y,1,1);
//			}
//		}
	}

	public Rectangle2D getBound(Graphics2D g) {
		return (Rectangle2D) getTLM(g).r.clone();
	}


	public double getDistance(Point2D p, Graphics2D g) {
		return getTLM(g).getDistance(p);
	}

	/*
	After some research, we conclude that LineMetrics and TextLayout are
   IDENTICAL. The only difference is that LineMetrics can return a NEGATIVE
  descent (which is weird), while TextLayout returns a descent of 0 in these cases,
  with a reduced leading. Only when LineMetric's decent is negative, and larger
  than the leading are LineMetrics and TextLayout different. This happens for a single font:
  LunaEclipsed.
	*/

	class TextLayoutMan {
		Font font;
//		String [] lines;
//		LineMetrics [] lms;
		float [] widths;
		double cx,cy; // center of flip
		float px, py; // origin (baseline)
		Rectangle2D r = null; // total bound
		float h_aligner = 0;
		TextLayout tls [];
		Color c;
		AffineTransform T;
		AffineTransform correction;

		public TextLayoutMan(Text t, Graphics2D g, Font _font) {
			this.font = _font;
			FontRenderContext frc = g.getFontRenderContext();

			c = t.getColor();

			T = g.getTransform();

			px = t.getPos().x;
			py = t.getPos().y;

			String [] lines = t.getText().split("(?<=\n)"); // leaves newline chars at ends
			assert lines.length >= 1;

			widths = new float[lines.length]; // width of line i
//			LineMetrics [] lms    = new LineMetrics[lines.length]; // height of line i
			tls    = new TextLayout[lines.length]; // height of line i


			int height = 0;
			for (int i = 0; i < lines.length; i++) {
//				lms[i] = this.font.getLineMetrics(lines[i], frc);
				tls[i] = new TextLayout(lines[i], this.font, frc);

				Rectangle2D ri = new Rectangle2D.Double(0, -tls[i].getAscent(),
															tls[i].getAdvance(),
															tls[i].getAscent() +
															tls[i].getDescent() +
															tls[i].getLeading());
				widths[i] = (float)ri.getWidth();
				ri.setRect(ri.getX(), ri.getY()+height, ri.getWidth(), ri.getHeight());
				if (r == null) r = ri; else Rectangle2D.union(r, ri, r);
				// goto next line
				height += tls[i].getAscent() + tls[i].getDescent() + tls[i].getLeading();
			}
//			assert r.getHeight() == height : r.getHeight() + " =/= " + height;
			height -= tls[tls.length-1].getLeading();
			// now height is the distance from the first ascender line to the last descender line

			HAlignment h_align = t.getHalign();
			VAlignment v_align = t.getValign();


			// for left   we plot left
			// for center we plot left + 0.5*(width-bbwidth)
			// for right  we plot left + width-bbwidth
			h_aligner = 0;

				 if (h_align == HAlignment.center)  h_aligner = 0.5f;
			else if (h_align == HAlignment.right)   h_aligner = 1;

			px -= h_aligner*r.getWidth();

			// ---- asc
			// ---- strikethrough
			// ---- baseline          <-- this is the (py) we need to compute
			// ---- desc
			// ++++ leading
			// ---- asc
			// ---- strikethrough
			// ---- baseline
			// ---- desc
			// ++++ leading
			// ---- asc
			// ---- strikethrough
			// ---- baseline
			// ---- desc

			// for top    we align the ascent line with py
			// for center we align halfway between the ascent and descent line with py
			// for bottom we align the descent line with py (this is the default)

				 if (v_align == VAlignment.bottom)  py += tls[0].getAscent() - height;
			else if (v_align == VAlignment.center)  py += tls[0].getAscent() - height/2; //-tls[0].getAscent()/2 -0.5*(height-tls[0].getAscent()-tls[0].getDescent());
//			else if (v_align == VAlignment.center)  py += lms[0].getAscent() -0.5*(+height);
			else if (v_align == VAlignment.top)     py += tls[0].getAscent();


			r.setRect(r.getX() + px, r.getY() + py, r.getWidth(), r.getHeight());

			// center position (for flipping)
			cx = px+r.getWidth()/2;
			// todo: check this (really figure out exactly what it should do first)
			cy = py-tls[0].getAscent() + 0.5*(lines.length*(tls[0].getAscent() + tls[0].getDescent() + tls[0].getLeading()));



			double[] m = new double[4];
			T.getMatrix(m); // strange indexing: a[1] = m10 a[2] = m01
			assert m[0] == T.getScaleX();
			assert m[3] == T.getScaleY();

			correction = new AffineTransform();

			if (m[0] < 0) {
				// undo horizontal flip
				correction.translate(cx, cy);
				correction.concatenate(Flip.horizontal.getTransform());
				correction.translate( -cx, -cy);
			}

			if (m[3] < 0) {
				// undo vertical flip
				correction.translate(cx, cy);
				correction.concatenate(Flip.vertical.getTransform());
				correction.translate( -cx, -cy);
			}
		}


		public double getDistance(Point2D p_orig) {
			try {
				Point2D p = correction.inverseTransform(p_orig, null);
				double d = Double.POSITIVE_INFINITY;

				float cpy = py;
				for (int i = 0; i < tls.length; i++) {
					Rectangle2D.Double ri = new Rectangle2D.Double(
										  px + h_aligner * ( (float)r.getWidth() - widths[i]),
										  cpy - tls[i].getAscent(),
										  widths[i],
										  tls[i].getAscent() + tls[i].getDescent() + tls[i].getLeading());
					if (ri.contains(p)) return 0;
					d = Math.min(d,GUIHelper.minDist(ri, p));
					cpy += tls[i].getAscent() + tls[i].getDescent() + tls[i].getLeading(); // go to next line
				}
				return d;
			} catch (NoninvertibleTransformException ex) { throw new Error(ex); }
		}



		public void draw(Graphics2D g) {
			AffineTransform old_t = g.getTransform();
			if (!( old_t.getScaleX() == T.getScaleX() &&
				   old_t.getScaleY() == T.getScaleY() &&
				   old_t.getShearX() == T.getShearX() &&
				   old_t.getShearY() == T.getShearY())) {
					   System.out.println("oepsie");
			}

			assert old_t.getScaleX() == T.getScaleX() &&
				   old_t.getScaleY() == T.getScaleY() &&
				   old_t.getShearX() == T.getShearX() &&
				   old_t.getShearY() == T.getShearY() : "transform at creation: " + T + "\n but at render: "+ old_t;

			g.transform(correction);

			g.setColor(c);

			float cpy = py;
			for (int i = 0; i < tls.length; i++) {
				if (false) {
					System.out.println(
									   " ascent: " + tls[i].getAscent() +
//									   " strike: " + tls[i].getStrikethroughOffset() +
									   " descent: "+ tls[i].getDescent() +
									   " leading: "+ tls[i].getLeading() +
//									   " height: " + tls[i].getHeight() +
									   " height: " + (tls[i].getLeading() + tls[i].getAscent() + tls[i].getDescent())
					);
					AffineTransform f = g.getTransform();
					float w = (float)Math.max(f.getScaleX(), f.getScaleY());
					Stroke s = g.getStroke();
					// base line
					g.setStroke(new BasicStroke(2/w));
					g.setColor(Color.orange);
					g.draw(new java.awt.geom.Line2D.Double(px, cpy,
						px + r.getWidth(), cpy));

					// ascent line
					g.setColor(Color.pink);
					g.draw(new java.awt.geom.Line2D.Double(px,
						cpy - tls[i].getAscent(), px + r.getWidth(),
						cpy - tls[i].getAscent()));


					// strikethrough line
					g.setColor(Color.green);
					double strikeoffset = -tls[i].getAscent()/2;
					g.draw(new java.awt.geom.Line2D.Double(px,
						cpy + strikeoffset, px + r.getWidth(),
						cpy + strikeoffset));


					// descent line
					g.setColor(Color.cyan);
					g.draw(new java.awt.geom.Line2D.Double(px,
						cpy + tls[i].getDescent(), px + r.getWidth(),
						cpy + tls[i].getDescent()));

					g.setColor(c);
					g.setStroke(s);
				}
				if (false) {
					tls[i].draw(g, px + h_aligner * ( (float)r.getWidth() - widths[i]), cpy);
				} else {
					g.translate(px + h_aligner * ( (float)r.getWidth() - widths[i]), cpy);
					tls[i].draw(g, 0, 0);
					g.translate(-(px + h_aligner * ( (float)r.getWidth() - widths[i])), -cpy);
				}

				if (i < tls.length-1) {
					cpy += tls[i+1].getAscent() + tls[i].getDescent() + tls[i].getLeading(); // go to next line
				}
			}

			g.setTransform(old_t);

		}

	}



}
