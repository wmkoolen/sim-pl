/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.SubPinStub;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SubPinStubRenderNode extends RenderNode<SubPinStub> {

	public SubPinStubRenderNode(SubPinStub peer) {
		super(peer);
		invalidate();
	}

	public double getDistance(Point2D p, Graphics2D g) {
		return !peer.isResolved()
			? peer.getExternalPos().distance(p)
			: Double.POSITIVE_INFINITY;
	}

	final Ellipse2D.Double e  = new Ellipse2D.Double();
	final Line2D.Double    l1 = new Line2D.Double();
	final Line2D.Double    l2 = new Line2D.Double();
	static final double r = 4;

	public void revalidateErrorDrawing() {
		Point2D p = peer.getExternalPos();
		e.setFrame(p.getX()-r, p.getY()-r, 2*r, 2*r);
		final double v = Math.sqrt(2)*r/2;
		l1.setLine(p.getX() - v, p.getY() - v,
				   p.getX() + v, p.getY() + v);
		l2.setLine(p.getX() - v, p.getY() + v,
				   p.getX() + v, p.getY() - v);
	}


	public void render(Graphics2D g) {
		if (!peer.isResolved()) {
			revalidateErrorDrawing();
			g.setColor(Color.red);
			g.setStroke(GUIHelper.unitStroke);
			g.draw(l1);
			g.draw(l2);
			g.draw(e);
		}
	}

	public Rectangle2D getBound(Graphics2D g) {
		if (!peer.isResolved()) {revalidateErrorDrawing(); return e.getBounds(); }
		return null;
	}
}
