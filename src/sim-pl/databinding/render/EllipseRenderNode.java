/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;

import databinding.forms.Ellipse;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class EllipseRenderNode extends ShapeRenderNode<Ellipse> {

	public EllipseRenderNode(Ellipse peer) {
		super(peer);
	}

	public Ellipse2D getShape() {
		Point2D pos = peer.getCenter();
		return new Ellipse2D.Double(pos.getX() - peer.getRadiusX(), pos.getY()-peer.getRadiusY(), 2*peer.getRadiusX(), 2*peer.getRadiusY());
	}

	public double getDistance(Point2D p, Graphics2D g) {
		if (peer.getFill() != null && getShape().contains(p)) return 0;

		// prevent division by zero with hack
		if (peer.getRadiusX() == 0 || peer.getRadiusY() == 0) return peer.getCenter().distance(p);

		return dist(p.getX(), p.getY(), peer.getCenter().getX(), peer.getCenter().getY(), peer.getRadiusX(), peer.getRadiusY());
	}

	public static double dist(double px, double py, double ctrx, double ctry, double rx, double ry) {
		assert rx >= 0 && ry >= 0;

		// cope with degenerate cases (points and flat ellipses)
		if (rx == 0 && ry == 0) return Point2D.distance(ctrx, ctry, px, py);
		if (rx == 0) return Line2D.ptSegDist(ctrx, ctry-ry, ctrx, ctry+ry, px, py);
		if (ry == 0) return Line2D.ptSegDist(ctrx-rx, ctry, ctrx+rx, ctry, px, py);

		assert rx > 0 && ry > 0;
		// it turns out in general to be quite hard to compute the exact
		// distance of a point to an ellipse
		// (this is a quartic equation)
		// so we use a simple approximation that works well for roughly round ellipses

		// we scale the field to unit circle
		double dx = (px-ctrx)/rx;
		double dy = (py-ctry)/ry;

		// compute distance to origin of circle
		double r = Math.sqrt(dx*dx+dy*dy);

		// the closest point on the circle is
		double cx = dx / r;
		double cy = dy / r;

		// we scale it back to the original ellipse, and compute distance
		return Point2D.distance(
				  cx*rx+ctrx,
				  cy*ry+ctry,
				  px, py);
	}

	public void collectControlPoints(Collection<ControlPoint> cps) {

	// bottom-right control point
		cps.add(new RegularControlPoint() {
			
			public Ellipse getAffected() { return peer;}
			
			public Point2D getPoint() {
				return new Point2D.Double(peer.getCenter().getX() + peer.getRadiusX(),
										  peer.getCenter().getY() + peer.getRadiusY());
			}
			public String getDescription() { return "bottom right"; }
			public void setPoint(Point2D t) {
				double fx = peer.getCenter().x - peer.getRadiusX();
				double fy = peer.getCenter().y - peer.getRadiusY();
				double tx = Math.max(fx, t.getX());
				double ty = Math.max(fy, t.getY());
				assert fx <= tx;
				assert fy <= ty;
				// now put ellipse in frame (px,py) and (tx,ty)
				peer.setRadiusX((tx-fx)/2);
				peer.setRadiusY((ty-fy)/2);
				peer.setX((tx+fx)/2);
				peer.setY((ty+fy)/2);

				invalidate();
			}
		});

		// top-left control point
		cps.add(new RegularControlPoint() {
			public Ellipse getAffected() { return peer;}
			public Point2D getPoint() {
				return new Point2D.Double(peer.getCenter().getX() - peer.getRadiusX(),
										  peer.getCenter().getY() - peer.getRadiusY());
			}
			public String getDescription() { return "top left"; }
			public void setPoint(Point2D t) {
				double tx = peer.getCenter().x + peer.getRadiusX();
				double ty = peer.getCenter().y + peer.getRadiusY();
				double fx = Math.min(tx, t.getX());
				double fy = Math.min(ty, t.getY());
				assert fx <= tx;
				assert fy <= ty;
				// now put ellipse in frame (fx,fy) and (tx,ty)
				peer.setRadiusX((tx-fx)/2);
				peer.setRadiusY((ty-fy)/2);
				peer.setX((tx+fx)/2);
				peer.setY((ty+fy)/2);

				invalidate();
			}
		});
	}

}
