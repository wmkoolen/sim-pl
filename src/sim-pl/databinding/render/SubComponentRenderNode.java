/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.SubComponent;
import databinding.compiled.Compiled.CompiledComplex;
import databinding.compiled.Compiled.CompiledComponent;
import editor.RenderAdapter.NodeDist;
import editor.RenderAdapter.NodeRepresentantMapping;
import editor.RenderContext;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class SubComponentRenderNode extends RenderNode<SubComponent>{
	final Rectangle2D.Double errorRect = new Rectangle2D.Double(-10,-10, 20, 20);

	public SubComponentRenderNode(SubComponent peer) {
		super(peer);
	}

	/** we currently just store it, but we should update it when necessary */
	RenderContext ra;
//	AffineTransform t;

	public void render(Graphics2D g, CompiledComponent<?> cc) {
		revalidate();
		assert cc != null;
		AffineTransform oldt = g.getTransform();
		g.transform(peer.getTransform());

		if (peer.isResolved()) {
			CompiledComponent<?> scc = ((CompiledComplex)cc).sub2csub.get(peer);
			ra.getRenderRoot().render(g, scc);
		} else {
			g.setColor(new Color(1,0,0,0.5f));
			g.fill(errorRect);
		}

		g.setTransform(oldt);
	}

	public void render(Graphics2D g) {
		render(g, null);
	}

	public <X> void getNode(Point2D p, Graphics2D g, NodeDist<X> nd, NodeRepresentantMapping<X> n2m) {
		try {
			AffineTransform t = peer.getTransform();
			// transform p to subcomponent space
			Point2D p2 = t.inverseTransform(p, null);

			// compute scale factor for distance
			assert Math.abs(t.getScaleX()) == Math.abs(t.getScaleY());
			double scale = Math.abs(t.getScaleX());

			if (peer.isResolved()) {
				revalidate();
				// compute best point over there
				AffineTransform oldt = g.getTransform();
				g.transform(t);
				NodeDist<X> nd2 = ra.getRenderRoot().getNode(p2, g, n2m.getSubComponentMapping(peer));
				g.setTransform(oldt);

				// see if that is good over here
				if (nd2.d * scale < nd.d) {
					nd.node = nd2.node;
					nd.d = nd2.d * scale;
				}
			} else {
				if (errorRect.contains(p2)) {
					nd.node = n2m.getRepresentant(peer);
					nd.d = 0;
				}
			}
		} catch (Exception ex) { throw new Error(ex); }
	}


	public double getDistance(Point2D p, Graphics2D g) {
		throw new Error("method superseded by getNode");
	}

	public void invalidate() {
		ra = null;
	}

	public void revalidate() {
		if (ra == null && peer.isResolved()) {
			ra = new RenderContext(peer.getSubComponent(), false, true);
		}
		assert ra == null || !ra.renderLabels;

	}

	public Rectangle2D getBound(Graphics2D g) {
		revalidate();

		if (!peer.isResolved()) {
			return GUIHelper.transform(peer.getTransform(), errorRect);
		} else {
			AffineTransform oldt = g.getTransform();
			AffineTransform t = peer.getTransform();
			g.transform(t);
			Rectangle2D r = ra.getRenderRoot().getDeepBound(g);
			g.setTransform(oldt);
			return r == null
				? null
				: GUIHelper.transform(t, r);
		}
	}
}
