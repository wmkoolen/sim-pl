/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import util.GUIHelper;
import databinding.Pin;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.compiled.Compiled.CompiledPin;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PinRenderNode extends RenderNode<Pin> {

	private static final int size = 2;
	// none, input, output, bidirectional
	private static final Color [] colors = {null, new Color(255, 230, 0), Color.green, Color.orange};

	public static Color getColor(Pin peer) { return getColor(peer.isInput(), peer.isOutput()); }
	public static Color getColor(boolean isInput, boolean isOutput) { return colors[(isInput ? 1 : 0) + (isOutput ? 2 : 0)]; }


	public PinRenderNode(Pin peer) {
		super(peer);
	}

	public void render(Graphics2D g, CompiledComponent<?> cc) {
		CompiledPin p = cc == null ? null : cc.pin2cpin.get(peer);
		if (p == null || !p.wired) render(g);
	}

	public static void render(Graphics2D g, Point2D pos, Color color, Ellipse2D e) {
		g.setColor(color);
		e.setFrame(pos.getX()-size, pos.getY()-size, 2*size, 2*size);
		g.fill(e);

		g.setStroke(GUIHelper.unitStroke);
		g.setColor(Color.black);
		g.draw(e);
	}

	public static Rectangle2D getBound(Point2D pos) {
		return new Rectangle2D.Double(pos.getX()-size, pos.getY()-size, 2*size, 2*size);

	}

	private Ellipse2D.Double e = new Ellipse2D.Double(); // render buffer

	public void render(Graphics2D g) {
		render(g, peer.getPos(), getColor(peer), e);
	}

	public Rectangle2D getBound(Graphics2D g) {
		return new Rectangle2D.Double(peer.getX()-size, peer.getY()-size, 2*size, 2*size);
	}

	public double getDistance(Point2D p, Graphics2D g) {
		return Math.max(0, peer.getPos().distance(p)-size);
	}

	public void renderSelectionMark(Graphics2D g) {
		int s = Math.max(5, size+1);
		g.draw(new Ellipse2D.Double(peer.getX() - s, peer.getY() - s, 2 * s, 2 * s));
	}
	
	public Rectangle2D getSelectionMarkBound(Graphics2D g) {
		int s = Math.max(5, size+1);
		Rectangle2D r = new Rectangle2D.Double(peer.getX() - s, peer.getY() - s, 2 * s, 2 * s);
		return GUIHelper.grow(((BasicStroke)g.getStroke()).getLineWidth()/2, r, r);
	}
	
}
