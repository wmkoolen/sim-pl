/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Event {
	public final String name;
	public final int index;

	private Event(int index, String name) {
		this.name = name;
		this.index = index;
	}

	public String toString() {
		return name;
	}

	public static final int         EVENT_INIT =            0;
	public static final int         EVENT_RESET =           1;
	public static final int         EVENT_CLOCK_RISING =    2;
	public static final int         EVENT_CLOCK_FALLING =   3;
	public static final int         EVENT_INPUT_CHANGE =    4;

	public static final Event [] events = {
		new Event(EVENT_INIT,          "INIT"),
		new Event(EVENT_RESET,         "RESET"),
		new Event(EVENT_CLOCK_RISING,  "CLOCK_RISING"),
		new Event(EVENT_CLOCK_FALLING, "CLOCK_FALLING"),
		new Event(EVENT_INPUT_CHANGE,  "INPUT_CHANGE"),
	};
//	public static final String []  events =        {"INIT", "RESET", "CLOCK_RISING", "CLOCK_FALLING", "INPUT_CHANGE"};


	public static int indexOf(String event) throws Exception	{
		for (int i = 0; i < events.length; i++)		{
			if (events[i].name.equals(event)) return i;
		}
		throw new Exception("Unknown event: " + event);
	}

	public static Event valueOf(String event) throws Exception	{
		return events[indexOf(event)];
	}
}
