/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


public abstract class AbstractNodeList<P extends BasicEditorNode, T extends BasicEditorNode<? extends BasicEditorNode>> extends EditorNode<P> implements InternalNode<P,T>  {

	public void wire(P parent) {
		super.wire(parent);
		for (BasicEditorNode child : getChildren()) {
			child.wire(this);
		}
   }

   public boolean isWellFormed() {
	   assert super.isWellFormed();
	   for (BasicEditorNode child : getChildren()) {
		   assert child.getParent() == this : child + ": parent " + child.getParent() + " should be " + this;
	   }
	   return true;
   }

   public final boolean isWellFormedRecursively() {
	   assert super.isWellFormedRecursively();
	   for (BasicEditorNode child : getChildren()) assert child.isWellFormedRecursively();
	   return true;
   }

   public final boolean isValidRecursively() {
	   assert super.isValidRecursively();
	   for (BasicEditorNode child : getChildren()) assert child.isValidRecursively();
	   return true;
   }
}
