/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import databinding.compiled.CompilationMessage;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class CompilationMessageBuffer extends ArrayList<CompilationMessage> {
	private int warnings = 0;
	private int errors = 0;


	Set<BasicEditorNode> errorNodes   = new HashSet<BasicEditorNode>();
	Set<BasicEditorNode> warningNodes = new HashSet<BasicEditorNode>();


	public int getErrorCount() { return errors;	}
	public int getWarningCount() { return warnings;}

	public void clear() {
		warnings = 0;
		errors = 0;
		errorNodes.clear();
		warningNodes.clear();
		super.clear();
	}

	public boolean add(CompilationMessage m) {
		if (m.error) errors++; else warnings++;
		(m.error ? errorNodes : warningNodes).add(m.genBy);
		return super.add(m);
	}

	public void add(BasicEditorNode genBy, String message, boolean error) {
		add(new CompilationMessage(genBy, message, error));
	}

	public void add(BasicEditorNode genBy, String message) {
		add(genBy, message, true);
	}

	public void add(BasicEditorNode genBy, Throwable message) {
		add(genBy, message, true);
	}

	public void add(BasicEditorNode genBy, Throwable message, boolean error) {
		add(genBy, message.getMessage(), error);
	}

	public boolean hasErrors(BasicEditorNode node) {
		return errorNodes.contains(node);
	}

	public boolean hasWarnings(BasicEditorNode node) {
		return warningNodes.contains(node);
	}

}
