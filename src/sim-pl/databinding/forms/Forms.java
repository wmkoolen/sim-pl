/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.util.ArrayList;

import databinding.Component;
import databinding.NodeList;







public class Forms extends NodeList<Component, Form> implements FormGroup<Component>
{
//	private Component parent;

	public Forms() {};

	public boolean canBeDetached() { return false; }

//	public Forms(Component parent)
//	{
//		this.parent = parent;
//	}

	public String toString() {
		return "Forms";
	}


//	public EditorNode getParent() {
//		return parent;
//	}

//	public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//	public <T,X> X handle(CTWalkerR<T,X> w, T param) { return w.handle(this,param); }


//	public void setParent(Component parent) {
//		assert this.parent == null;
//		this.parent = parent;
//	}


//	public void add(Form f) {
//		assert f.parent == null;
//		assert !children.contains(f);
//
//		children.add(f);
//		f.parent = this;
//
//		assert f.parent == this;
//		assert children.contains(f);
//	}



	public ArrayList<Form> getForms() { return children; }
	public void setForms(ArrayList<Form> forms) { this.children = forms; }

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}


//    public Forms(EditorGUIElement parent, XMLReader xml) throws Exception
//    {
//		super(parent);
//
//
//		while (xml.childrenLeft())
//		{
//			String childName = xml.getCurrentChildName();
//			     if (childName.equals("ELLIPSE"))     forms.add(new Ellipse(  this,   xml.nextChild()));
//    		else if (childName.equals("LINE"))        forms.add(new Line(     this,   xml.nextChild()));
//	    	else if (childName.equals("POLYGON"))     forms.add(new Polygon(  this,   xml.nextChild()));
//		    else if (childName.equals("RECTANGLE"))   forms.add(new Rectangle(this,   xml.nextChild()));
//		    else if (childName.equals("TEXT"))        forms.add(new Text(     this,   xml.nextChild()));
//			else throw new Exception("Unknown form: " + childName);
//		}
//    }
//
//	public String toString()
//	{
//		return "Forms";
//	}
//
//
//	public java.awt.Rectangle getBounds(Graphics2D g)
//	{
//		java.awt.Rectangle r = null;
//		for (Iterator i = forms.iterator(); i.hasNext();)
//	    {
//		    r = component.Component.myUnion(r, ((Form)i.next()).getBounds(g));
//	    }
//		return r;
//	}
//
//	public GUIElement getGUIElement(Point offset, Graphics2D g)
//	{
//		Form                    best =      null;
//		java.awt.Rectangle      bestBound = null;
//
//		for (Iterator i = forms.iterator(); i.hasNext(); )
//		{
//			Form e = (Form)(((Form)i.next()).getGUIElement(offset, g));
//			if (e != null)
//			{
//				if (best == null)
//				{
//					best = (Form)e;
//					bestBound = best.getBounds(g);
//				}
//				else
//				{
//					java.awt.Rectangle newBound = e.getBounds(g);
//					/* Manieren om overlap te behandelen
//					 - neem diegene die zich in de andere bevindt
//					   wisselen als:
//					   bestBound.contains(newBound)
//
//					 - neem diegene die de kleinste oppervlakte van de
//						boundign box heeft
//					   wisselen als:
//					   (bestBound.width * bestBound.height) > (newBound.width * newBound.height)
//					*/
//
//					if ((bestBound.width * bestBound.height) > (newBound.width * newBound.height))
//					{
//						best =      e;
//						bestBound = newBound;
//					}
//				}
//			}
//		}
//		return best;
//	}
//
//
//
//
//	public void draw(Point offset, Graphics2D g)
//	{
//		for (Iterator i = forms.iterator(); i.hasNext(); )
//		{
//			((Form)i.next()).draw(offset, g);
//		}
//	}
//
//	public void toXML(XMLWriter xml, File baseDirectory)
//	{
//		if (forms.size() > 0)
//		{
//		    xml.openTag("FORMS");
//		    xml.endTag();
//			for (Iterator i = forms.iterator(); i.hasNext(); ) ((Form)i.next()).toXML(xml, baseDirectory);
//			xml.closeTag("FORMS");
//		}
//	}
//
//	public boolean canBeDeleted() { return false; }
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TREE ---------------------
// */
//	//Returns true if the receiver allows children.
//	public boolean getAllowsChildren()          { return true;}
//
//	//Returns the child TreeNode at index childIndex.
//	public TreeNode getChildAt(int childIndex)  { return (TreeNode)forms.get(childIndex); }
//
//	// Returns the number of children TreeNodes the receiver contains.
//	public int getChildCount()                  { return forms.size(); }
//
//	// Returns the index of node in the receivers children.
//	public int getIndex(TreeNode node)          { return forms.indexOf(node);}
//
//	//Adds child to the receiver at index.
//	public void insert(MutableTreeNode child, int index) {forms.add(index, child);}
//
//	//Removes the child at index from the receiver.
//	public void remove(int index)               {forms.remove(index);}
//
//	//Removes node from the receiver.
//	public void remove(MutableTreeNode node)    {forms.remove(node);}
//
///*--------------- TREE REPRESENTATION ENDS HERE --------------------------------
// */
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TABLE --------------------
// */
//
///*--------------- TABLE REPRESENTATION ENDS HERE -------------------------------
// */
//
//
//
//
//
//
//
//
//
//
///*--------------- THIS OBJECT CAN GENERATE CHILDREN ----------------------------
// */
//
//
//	private static final Tool [] tools = {  new LineTool(),
//											new EllipseTool(),
//											new RectangleTool(),
//											new TextTool()};
//
//	public LinkedList getTools()
//	{
//		LinkedList l = super.getTools();
//		for (int i = 0; i < tools.length; i++)
//		{
//			tools[i].setParent(this);
//			l.add(tools[i]);
//		}
//		return l;
//	}
//
///*--------------- CHILD GENERATION ENDS HERE -----------------------------------
// */
//


}
