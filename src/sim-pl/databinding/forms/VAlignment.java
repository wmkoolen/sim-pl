/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

public class VAlignment
{
	public static final VAlignment top =     new VAlignment("TOP");
	public static final VAlignment center =  new VAlignment("CENTER");
	public static final VAlignment bottom =  new VAlignment("BOTTOM");

	public static final VAlignment [] alignments = {top,     center,     bottom};

	private String  alignmentString;

	private VAlignment(String alignmentString)
    {
		this.alignmentString = alignmentString;
    }

	public static VAlignment valueOf(String v_alignment) throws Exception {
		return parseVAlignment(v_alignment);
	}

    public static VAlignment parseVAlignment(String v_alignment) throws Exception
    {
		for (int i = 0; i < alignments.length; i++)
		{
			if (v_alignment.equals(alignments[i].alignmentString))
			{
				return alignments[i];
			}
		}
		throw new Exception("Invalid vertical alignment: " + v_alignment);
    }


	public String toString()
	{
		return alignmentString;
	}
}
