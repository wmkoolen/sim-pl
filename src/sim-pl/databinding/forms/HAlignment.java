/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

public class HAlignment
{
	public static final HAlignment left =    new HAlignment("LEFT");
	public static final HAlignment center =  new HAlignment("CENTER");
	public static final HAlignment right =   new HAlignment("RIGHT");

	public static final HAlignment [] alignments = {left,     center,     right};

	private String  alignmentString;

	private HAlignment(String alignmentString)
    {
		this.alignmentString = alignmentString;
    }

	public static HAlignment valueOf(String h_alignment) throws Exception {
		return parseHAlignment(h_alignment);
	}

    public static HAlignment parseHAlignment(String h_alignment) throws Exception
    {
		for (int i = 0; i < alignments.length; i++)
		{
			if (h_alignment.equals(alignments[i].alignmentString))
			{
				return alignments[i];
			}
		}
		throw new Exception("Invalid horizontal alignment: " + h_alignment);
    }


	public String toString()
	{
		return alignmentString;
	}
}
