/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.geom.RoundRectangle2D;

import databinding.IPositioned;
import databinding.SolidFill;
import databinding.StrokedOutline;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class RoundRect extends AreaForm implements IPositioned {
	private final RoundRectangle2D.Double r = new RoundRectangle2D.Double();

	public RoundRect() {
	}

	public RoundRect(StrokedOutline outline, SolidFill fill, double x, double y, double width, double height, double rx, double ry) {
		super(outline, fill);
		r.setRoundRect(x,y,width, height, rx, ry);
	}


	public void move(int dx, int dy) {
		r.setFrame(r.getX() + dx, r.getY() + dy, r.getWidth(), r.getHeight());
	}

	public RoundRectangle2D getShape() { return r; }

	public double getX() { return r.getX(); }
	public double getY() { return r.getY(); }
	public double getWidth() { return r.getWidth(); }
	public double getHeight() { return r.getHeight(); }
	public double getRadiusX() { return r.getArcWidth()/2; }
	public double getRadiusY() { return r.getArcHeight()/2; }

	public void setX(double x) { r.x = x; }
	public void setY(double y) { r.y = y; }
	public void setWidth(double width) { r.width = width; }
	public void setHeight(double height) { r.height = height; }
	public void setRadiusX(double rx) { r.arcwidth = 2*rx; }
	public void setRadiusY(double ry) { r.archeight = 2*ry; }

	public String toString() {
		return "Rectangle";
	}

}
