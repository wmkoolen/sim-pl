/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import databinding.IPositioned;
import databinding.SolidFill;
import databinding.StrokedOutline;

public class Ellipse extends AreaForm implements IPositioned
{
	public  Point2D.Double  pos = new Point2D.Double();
	public  double      radiusx;
	public  double      radiusy;

	public Ellipse() {
	}

	public Ellipse(StrokedOutline outline, SolidFill fill, Ellipse2D ellipse) {
		super(outline, fill);
		pos.setLocation(ellipse.getCenterX(), ellipse.getCenterY());
		radiusx = (int)ellipse.getWidth()/2;
		radiusy = (int)ellipse.getHeight()/2;
	}


	public void move(int dx, int dy) {
		pos.x += dx;
		pos.y += dy;
	}

//	public Ellipse(Point pos, int radiusx, int radiusy, Color c, int lineWidth, Color fillc, boolean  filled)
//	{
//		super(c,lineWidth, fillc,filled);
//		this.pos =      pos;
//		this.radiusx =  radiusx;
//		this.radiusy =  radiusy;
//	}
/*
    public Ellipse(FormGroup parent, Point pos, int radiusx, int radiusy, Color c)
    {
		super(parent);
		this.pos =      pos;
		this.radiusx =  radiusx;
		this.radiusy =  radiusy;
		this.c =        c;
    }
*/
//	public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//	public <T,X> X handle(CTWalkerR<T,X> w, T param) { return w.handle(this,param); }


	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert pos != null;
		assert radiusx >= 0;
		assert radiusy >= 0;
		return true;
	}

	public double getX() { return pos.getX(); }
	public void setX(double x) { pos.setLocation(x, pos.getY()); }

	public double getY() { return pos.getY(); }
	public void setY(double y) { pos.setLocation(pos.getX(), y); }

	public double getRadiusX() { return radiusx; }
	public void setRadiusX(double radiusx) { this.radiusx = radiusx; }

	public double getRadiusY() { return radiusy; }
	public void setRadiusY(double radiusy) { this.radiusy = radiusy; }

	public Point2D.Double getCenter() { return pos; }

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}



//	public Ellipse(EditorGUIElement parent, XMLReader xml) throws Exception
//	{
//		super(parent);
//
//		pos = new Point(
//			Integer.parseInt(xml.atts.getNamedItem("POSX").getNodeValue()),
//			Integer.parseInt(xml.atts.getNamedItem("POSY").getNodeValue()));
//		radiusx = Integer.parseInt(xml.atts.getNamedItem("RADIUSX").getNodeValue());
//		radiusy = Integer.parseInt(xml.atts.getNamedItem("RADIUSY").getNodeValue());
//
//		c =      Converter.toColor(xml.atts, "COLOR", Color.black);
//		filled = Converter.toBoolean(xml.atts, "FILLED", false);
//	}

	public String toString()
	{
		return "Ellipse";
	}

//    public void draw(Point offset, Graphics2D g)
//    {
//		g.setColor(fillc);
//		if (filled) g.fillOval(pos.x-radiusx + offset.x, pos.y-radiusy + offset.y, 2*radiusx, 2*radiusy);
//		else        g.drawOval(pos.x-radiusx + offset.x, pos.y-radiusy + offset.y, 2*radiusx, 2*radiusy);
//    }

//    public java.awt.Rectangle getBounds(Graphics2D g)
//    {
//		return new java.awt.Rectangle(pos.x-radiusx, pos.y-radiusy, 2*radiusx+1, 2*radiusy+1);
//    }
//
//	public GUIElement getGUIElement(Point pos, Graphics2D g)
//	{
//	    if (radiusy == 0) return null;
//		int difx =   this.pos.x - pos.x;
//	    int dify = ((this.pos.y - pos.y) * radiusx)/radiusy;
//	    if ((difx*difx) + (dify*dify) < (radiusx*radiusx))  return this;
//		else                                                return null;
//	}
//
	public void move(Point delta)
	{
		pos.x += delta.x;
		pos.y += delta.y;
	}

//	public RenderNode createRenderNode() {
//		return new RenderNode() {
//			public void render(Graphics2D g) {
//				g.setColor(getColor());
//				Point pos = getCenter();
//				int radiusx = getRadiusX();
//				int radiusy = getRadiusY();
//				if (isFilled())
//					g.fillOval(pos.x - radiusx, pos.y - radiusy, 2 * radiusx, 2 * radiusy);
//				else
//					g.drawOval(pos.x - radiusx, pos.y - radiusy, 2 * radiusx, 2 * radiusy);
//			}
//		};
//	}
	//	public void toXML(XMLWriter xml, File baseDirectory)
//	{
//		xml.openTag("ELLIPSE");
//		xml.addAttribute("POSX",    pos.x);
//		xml.addAttribute("POSY",    pos.y);
//		xml.addAttribute("RADIUSX", radiusx);
//		xml.addAttribute("RADIUSY", radiusy);
//
//		if (c != Color.black)   xml.addAttribute("COLOR", Converter.toString(c));
//		if (filled != false)    xml.addAttribute("FILLED", Converter.toString(filled));
//		xml.closeTag();
//	}
//
//	public Locator [] getLocators()
//	{
//		return new Locator [] {new Rad_Locator()};
//	}
//
//
//	public class Rad_Locator extends Locator
//	{
//		public void move(Point delta)
//		{
//			radiusx += delta.x;
//			radiusy += delta.y;
//			if (radiusx < 0) radiusx = 0;
//	    	if (radiusy < 0) radiusy = 0;
//		}
//
//		public void draw(Point offset, Graphics2D g)
//		{
//			g.setColor(Color.blue);
//			g.fill3DRect(offset.x + pos.x + radiusx - 2,
//						 offset.y + pos.y + radiusy - 2, 5, 5, true);
//		}
//
//		public java.awt.Rectangle getBounds(Graphics2D g)
//		{
//			return new java.awt.Rectangle(pos.x + radiusx - 2,
//										  pos.y + radiusy - 2, 5, 5);
//		}
//
//		public GUIElement getGUIElement(Point offset, Graphics2D g)
//		{
//			if (getBounds(g).contains(offset))  return this;
//			else                                return null;
//		}
//	}
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TREE ---------------------
// */
//
//
// /*--------------- TREE REPRESENTATION ENDS HERE --------------------------------
// */
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TABLE --------------------
// */
//
//	private static final String [] names = {"PosX",    "PosY",     "RadiusX",  "RadiusY",  "Color", "Filled"};
//
//	//Returns the number of rows in the model.
//	public int getRowCount()                                    {return names.length;}
//
//	//Returns the value for the cell at columnIndex and rowIndex.
//	public Object getValueAt(int rowIndex, int columnIndex)
//	{
//		if (columnIndex == 0) return names[rowIndex];
//		else
//		switch (rowIndex)
//		{
//			case 0: return new Integer(pos.x);
//			case 1: return new Integer(pos.y);
//			case 2: return new Integer(radiusx);
//			case 3: return new Integer(radiusy);
//			case 4: return c;
//			case 5: return new Boolean(filled);
//		}
//		return null;
//	}
//
//	//Sets the value in the cell at columnIndex and rowIndex to aValue.
//	public int setValue(Object value, int rowIndex, int columnIndex)
//	{
//		switch (rowIndex)
//		{
//			case 0: pos.x =       ((Integer)value).intValue();        break;
//			case 1: pos.y =       ((Integer)value).intValue();        break;
//			case 2: radiusx =     ((Integer)value).intValue();        break;
//			case 3: radiusy =     ((Integer)value).intValue();        break;
//			case 4: c =           ((Color)value);                     break;
//			case 5: filled =      ((Boolean)value).booleanValue();    break;
//		}
//		return UPDATE_DISPLAY;
//	}
//
///*--------------- TABLE REPRESENTATION ENDS HERE -------------------------------
// */

}
