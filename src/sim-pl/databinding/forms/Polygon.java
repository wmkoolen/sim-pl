/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.util.ArrayList;

import databinding.SolidFill;
import databinding.StrokedOutline;

public class Polygon extends AreaForm
{
	ArrayList<PPoint> points = new ArrayList<PPoint>();

//	public static class Point implements ComponentNode {
//		Polygon parent;
//		int index;
//
//		public ComponentNode getParent() {	return parent;	}

//		public boolean nodeValid(ComponentNode parent) {	}
//
//		public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//		public Iterator<? extends ComponentNode> getChildren() { return null; }
//	}


//	private static final String []              pointNames =    {"PosX",    "PosY"};

//	private java.awt.Polygon    polygon;

	public Polygon() {
//		polygon = new java.awt.Polygon();
	}

	public Polygon(StrokedOutline outline, SolidFill fill) {
		super(outline, fill);
	}


	public void move(int dx, int dy) {
		for (PPoint p : points) {
			p.move(dx, dy);
		}
	}


//	// from xml constructor
//	public Polygon(Color c, int lineWidth, Color fillc, boolean filled) {
//		super(c, lineWidth, fillc, filled);
//		this.polygon = new java.awt.Polygon();
//	}

//	public Polygon(FormGroup parent)
//    {
//		super(parent);
//    }

//	public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//	public <T,X> X handle(CTWalkerR<T,X> w, T param) { return w.handle(this,param); }
//	public Iterator<? extends EditorNode> getChildren() { return points.iterator(); }

//	public Shape getShape() { return polygon; }


//	public void add(Point p) {
//		polygon.addPoint(p.x, p.y);
//	}

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}

	public ArrayList<PPoint> getChildren() {
		return points;
	}

	public ArrayList<PPoint> getPoints() {
		return points;
	}

//	public Collection<Point> getPoints() {
//		return new Collection<Point>() {
//			public boolean add(Point p) {
//				polygon.addPoint(p.x, p.y);
//				return false;
//			}
//			public boolean addAll(Collection<? extends Point> c) {
//				throw new Error("Not implemented");
//			}
//			public boolean isEmpty() {
//				return polygon.npoints == 0;
//			}
//			public int size() {
//				return polygon.npoints;
//			}
//
//			public boolean contains(Object c) {
//				throw new Error("Not implemented");
//			}
//			public boolean containsAll(Collection<?> c) {
//				throw new Error("Not implemented");
//			}
//			public Object [] toArray() {
//				throw new Error("Not implemented");
//			}
//
//			public <T> T [] toArray(T [] ps) {
//				throw new Error("Not implemented");
//			}
//			public void clear() {
//				polygon.reset();
//			}
//
//			public boolean remove(Object p) {
//				throw new Error("Not implemented");
//			}
//			public boolean removeAll(Collection<?> c) {
//				throw new Error("Not implemented");
//			}
//
//			public boolean retainAll(Collection<?> c) {
//				throw new Error("Not implemented");
//			}
//
//			public Iterator<Point> iterator() {
//				return new Iterator<Point>() {
//					int i = 0;
//					public void remove() {
//						throw new Error("Not implemented");
//					}
//					public Point next() {
//						int cur = i++;
//						return new Point(polygon.xpoints[cur], polygon.ypoints[cur]);
//					}
//					public boolean hasNext() {
//						return i != polygon.npoints;
//					}
//				};
//			}
//		};
//	}



//	public Polygon(EditorGUIElement parent, XMLReader xml) throws Exception
//	{
//		super(parent);
//		LinkedList points = new LinkedList();
//
//
//		c =      Converter.toColor(xml.atts, "COLOR", Color.black);
//		filled = Converter.toBoolean(xml.atts, "FILLED", false);
//
//		while (xml.childrenLeft())
//		{
//			String childName = xml.getCurrentChildName();
//			if (childName.equals("POINT"))
//			{
//					 NamedNodeMap catts = xml.nextChild().atts;
//					 points.addLast(Converter.toPoint(catts, "POSX", "POSY", null));
////					 points.addLast(new Point(  Integer.parseInt(catts.getNamedItem("POSX").getNodeValue()),
////												Integer.parseInt(catts.getNamedItem("POSY").getNodeValue())));
//			}
//			else                                  throw new Exception("POINT expected, found: " + childName);
//		}
//
//		int [] pointsx = new int[points.size()];
//		int [] pointsy = new int[points.size()];
//
//		Iterator i = points.iterator();
//		int j = 0;
//
//		while (i.hasNext())
//		{
//			Point p = (Point) i.next();
//			pointsx[j] = p.x;
//			pointsy[j] = p.y;
//			j++;
//		}
//
//		polygon = new java.awt.Polygon(pointsx, pointsy, pointsx.length);
//	}

	public String toString()
	{
		return "Polygon";
	}


//	public void draw(Point offset, Graphics2D g)
//    {
//		g.setColor(fillc);
//		g.translate(offset.x, offset.y);
//		if (filled) g.fillPolygon(polygon);
//		else        g.drawPolygon(polygon);
//		g.translate(-offset.x, -offset.y);
//    }
//
    public java.awt.Rectangle getBounds(Graphics2D g)
    {
		return getShape().getBounds();
    }

//
//	public GUIElement getGUIElement(Point pos, Graphics2D g)
//	{
//		if (polygon.contains(pos)) return this;
//		else                       return null;
//	}

	public void move(Point delta)
	{
//		polygon.translate(delta.x, delta.y);
		for (PPoint p : points) {
			p.move(delta.x, delta.y);
		}
	}

	private Shape getShape() {
		java.awt.Polygon s = new java.awt.Polygon();
		for (PPoint p : points) {
			s.addPoint((int)Math.round(p.x), (int)Math.round(p.y));
		}
		return s;
	}

//	public RenderNode createRenderNode() {
//		return new RenderNode() {
//			public void render(Graphics2D g) {
//				g.setColor(getColor());
//				if (isFilled())   g.fill(getShape());
//				else              g.draw(getShape());
//
//			}
//		};
//	}

	//	public void toXML(XMLWriter xml, File baseDirectory)
//	{
//		xml.openTag("POLYGON");
//		if (c != Color.black)   xml.addAttribute("COLOR", Converter.toString(c));
//		if (filled != false)    xml.addAttribute("FILLED", Converter.toString(filled));
//		xml.endTag();
//
//		for (int i = 0; i < polygon.npoints; i++)
//		{
//			xml.openTag("POINT");
//			xml.addAttribute("POSX", polygon.xpoints[i]);
//			xml.addAttribute("POSY", polygon.ypoints[i]);
//			xml.endTag();
//			xml.closeTag("POINT");
//		}
//
//		xml.closeTag("POLYGON");
//	}
//
//
//
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TREE ---------------------
// */
//
//
// /*--------------- TREE REPRESENTATION ENDS HERE --------------------------------
// */
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TABLE --------------------
// */
//
//	private static final String []              names =         {"Color",   "Filled"};
//
//	//Returns the number of rows in the model.
//	public int getRowCount()                                    {return names.length;}
//
//	//Returns the value for the cell at columnIndex and rowIndex.
//	public Object getValueAt(int rowIndex, int columnIndex)
//	{
//		if (columnIndex == 0) return names[rowIndex];
//		else
//		switch (rowIndex)
//		{
//			case 0: return c;
//			case 1: return new Boolean(filled);
//		}
//		return null;
//	}
//
//	//Sets the value in the cell at columnIndex and rowIndex to aValue.
//	public int setValue(Object value, int rowIndex, int columnIndex)
//	{
//		switch (rowIndex)
//		{
//			case 0: c =                 ((Color)value);                     break;
//			case 1: filled =            ((Boolean)value).booleanValue();    break;
//		}
//		return UPDATE_DISPLAY;
//	}
//
///*--------------- TABLE REPRESENTATION ENDS HERE -------------------------------
// */
}
