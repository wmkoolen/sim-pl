/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.util.Set;

import databinding.BasicEditorNode;
import databinding.EditorNode;
import databinding.IMoveable;




public abstract class Form extends EditorNode<FormGroup> implements IMoveable
{
//	FormGroup parent;

	public Form() {}

//	public Form(FormGroup parent)
//	{
//		this.parent = parent;
//	}

//	public EditorNode getParent() {
//		return parent;
//	}

//	public abstract void                draw(Point offset, Graphics2D g);
//	public abstract GUIElement          getGUIElement(Point p, Graphics2D g);
//	public abstract java.awt.Rectangle  getBounds(Graphics2D g);
//	public abstract void                toXML(XMLWriter xml, File baseDirectory);


	public void collectAffected(Set<BasicEditorNode> nodes) {
		nodes.add(this); // no other nodes depend on our position 
	}
}
