/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.Color;

import util.GUIHelper;
import databinding.IColored;
import databinding.IStroked;
import databinding.StrokedOutline;
import databinding.myStroke;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class StrokedForm extends Form implements IColored, IStroked {
	StrokedOutline outline;

	public StrokedForm() {
		outline = new StrokedOutline(Color.pink, new myStroke(GUIHelper.unitStroke));
	}

	public StrokedForm(StrokedOutline outline) {
		this.outline = outline;
	};

	public StrokedForm(myStroke stroke, Color c) {
		this(new StrokedOutline(c,stroke));
	}


	final public StrokedOutline getOutline() {
		return outline;
	}


	final public myStroke getStroke() { return outline != null ? outline.getStroke() : null; }
	final public void setStroke(myStroke stroke) {
		if (outline == null)
			outline = new StrokedOutline(Color.pink, stroke);
		else
			outline.setStroke(stroke);
	}
	final public Color getColor() { return outline != null ? outline.getColor() : null; }

	final public void setColor(Color c) {
		if (outline == null)
			outline = new StrokedOutline(c, new myStroke(GUIHelper.unitStroke));
		else
			outline.setColor(c);
	}


	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert outline == null || outline.isWellFormed();
		return true;
	}
}
