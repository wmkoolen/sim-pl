/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.geom.Point2D;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.EditorNode;
import databinding.IMoveable;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class PPoint extends EditorNode<Polygon> implements IMoveable {
	double x;
	double y;

	public PPoint(){
		x=0;
		y=0;
	}

	public PPoint(Point2D p) {
		x = p.getX();
		y = p.getY();
	}

	public String toString() {
		return getX() + "," + getY();
	}


//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		return null;
//	}


	public void move(int dx, int dy) {
		x += dx;
		y += dy;
	}

	public double getX() { return x; }
	public double getY() { return y; }
	public void setX(double x) { this.x = x; }
	public void setY(double y) { this.y = y; }

	public void setLocation(Point2D p) {
		x = p.getX();
		y = p.getY();
	}

	public double distance(Point2D p) {
		return p.distance(x,y);
	}

	public double distance(PPoint p) {
		return Point2D.distance(x,y,p.x, p.y);
	}

	public Point2D toPoint() {
		return new Point2D.Double(x,y);
	}
	
	public void collectAffected(Set<BasicEditorNode> nodes) {
		nodes.add(this.getParent());
	}	
}
