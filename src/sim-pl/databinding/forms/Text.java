/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;

import databinding.IAligned;
import databinding.IColored;
import databinding.IPositioned;
import editor.FTEditor.FontText;

public class Text extends Form implements IColored, IAligned, IPositioned
{
	private Point       pos = new Point();
	private String      text;
	private Color       c;
	private HAlignment  h_align;
	private VAlignment  v_align;
	private Font        font = new Font("Dialog", Font.PLAIN, 12);

	public Text() {
		pos = new Point();
		c = Color.pink;
	}

	public Text(String text, Font font, Point2D pos, Color c, HAlignment h_align, VAlignment v_align) {
		this.text = text;
		this.font = font;
		this.pos.setLocation(pos);
		this.c = c;
		this.h_align = h_align;
		this.v_align = v_align;
	}


	public void move(int dx, int dy) {
		pos.translate(dx, dy);
	}

	public Font getFont() { return font; }
	public void setFont(Font font) { this.font = font; }

	public FontText getFontText() { return new FontText(getFont(), getText()); }
	public void setFontText(FontText ft) { setFont(ft.f); setText(ft.text); }


//	public Text(String text, Point pos, Color c, HAlignment h_align, VAlignment v_align)
//	{
//		this.text =     text;
//		this.pos =      pos;
//		this.c =        c;
//		this.h_align =  h_align;
//		this.v_align =  v_align;
//	}

//    public Text(FormGroup parent, Point pos, String text, Color c, HAlignment h_align, VAlignment v_align)
//    {
//		super(parent);
//		this.pos =      pos;
//		this.text =     text;
//		this.c =        c;
//		this.h_align =  h_align;
//		this.v_align =  v_align;
//    }

//	public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//	public <T,X> X handle(CTWalkerR<T,X> w, T param) { return w.handle(this,param); }

	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert pos != null;
		assert text != null;
		assert c != null;
		assert h_align != null;
		assert v_align != null;
		return true;
	}

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}


	public Color getColor() { return c;	}
	public void setColor(Color c) {	this.c = c;	}

	public String getText() { return text; }
	public void setText(String text) {	this.text = text; }

	public double getX() { return pos.getX(); }
	public void setX(double x) { pos.setLocation(x, pos.getY()); }

	public double getY() { return pos.getY(); }
	public void setY(double y) { pos.setLocation(pos.getX(), y); }

	public Point getPos() { return pos; }

	public HAlignment getHalign() { return h_align; }
	public void setHalign(HAlignment  h_align) { this.h_align = h_align; }

	public VAlignment getValign() { return v_align; }
	public void setValign(VAlignment v_align) { this.v_align = v_align; }

//	public Text(EditorGUIElement parent, XMLReader xml) throws Exception
//	{
//		super(parent);
//
//		pos = Converter.toPoint(xml.atts, "POSX", "POSY", null);
//
//		text = xml.getText();
//
//		c =         Converter.toColor(xml.atts, "COLOR", Color.black);
//		h_align =   Converter.toHAlignment(xml.atts, "HALIGN",      HAlignment.left);
//		v_align =   Converter.toVAlignment(xml.atts, "VALIGN",      VAlignment.top);
//	}

	public String toString()
	{
		return text;
	}


	public void draw(Point offset, Graphics2D g)
    {
		java.awt.Rectangle r;
		g.setColor(c);

		Point newPos = new Point(offset.x + pos.x, offset.y + pos.y);

		r = g.getFontMetrics().getStringBounds(text, g).getBounds();
		int ascent = g.getFontMetrics().getAscent();

		     if (h_align == HAlignment.center)  newPos.x -= r.width/2;
		else if (h_align == HAlignment.right)   newPos.x -= r.width;

		     if (v_align == VAlignment.center)  newPos.y += ascent/2;
		else if (v_align == VAlignment.top)     newPos.y += ascent;

		g.drawString(text, newPos.x, newPos.y);
    }

    public java.awt.Rectangle getBounds(Graphics2D g)
    {
		java.awt.Rectangle r = g.getFontMetrics().getStringBounds(text, g).getBounds();
		int ascent = g.getFontMetrics().getAscent();

		r.x += pos.x;
		r.y += pos.y;

		     if (h_align == HAlignment.center)  r.x -= r.width/2;
		else if (h_align == HAlignment.right)   r.x -= r.width;

		     if (v_align == VAlignment.center)  r.y += ascent/2;
		else if (v_align == VAlignment.top)     r.y += ascent;

		return r;
    }

//	public GUIElement getGUIElement(Point pos, Graphics2D g)
//	{
//		if (this.getBounds(g).contains(pos))    return this;
//		else                                    return null;
//	}

	public void move(Point delta)
	{
		pos.x += delta.x;
		pos.y += delta.y;
	}


	//
//
//	public void toXML(XMLWriter xml, File baseDirectory)
//	{
//		xml.openTag("TEXT");
//		xml.addAttribute("POSX",            pos.x);
//		xml.addAttribute("POSY",            pos.y);
//		xml.addAttribute("HALIGN",          h_align.toString());
//		xml.addAttribute("VALIGN",          v_align.toString());
//		xml.addAttribute("COLOR",           Converter.toString(c));
//		xml.endTag();
//		xml.addText(text);
//		xml.closeTag("TEXT");
//	}
//
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TREE ---------------------
// */
//
//
// /*--------------- TREE REPRESENTATION ENDS HERE --------------------------------
// */
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TABLE --------------------
// */
//
//	private static final String []              names = {"PosX", "PosY", "Text", "Color", "Horizontal Alignment", "Vertical Alignment"};
//
//	//Returns the number of rows in the model.
//	public int getRowCount()                                    {return names.length;}
//
//	//Returns the value for the cell at columnIndex and rowIndex.
//	public Object getValueAt(int rowIndex, int columnIndex)
//	{
//		if (columnIndex == 0) return names[rowIndex];
//		else
//		switch (rowIndex)
//		{
//			case 0: return new Integer(pos.x);
//			case 1: return new Integer(pos.y);
//			case 2: return text;
//			case 3: return c;
//			case 4: return h_align;
//			case 5: return v_align;
//		}
//		return null;
//	}
//
//	//Sets the value in the cell at columnIndex and rowIndex to aValue.
//	public int setValue(Object value, int rowIndex, int columnIndex)
//	{
//		switch (rowIndex)
//		{
//			case 0: pos.x =       ((Integer)value).intValue();                          break;
//			case 1: pos.y =       ((Integer)value).intValue();                          break;
//			case 2: text =        ((String)value);                                      return UPDATE_DISPLAY | UPDATE_TREE;
//			case 3: c =           ((Color)value);                                       break;
//			case 4: h_align =     ((HAlignment)value);                                  break;
//			case 5: v_align =     ((VAlignment)value);                                  break;
//		}
//		return UPDATE_DISPLAY;
//	}
//
///*--------------- TABLE REPRESENTATION ENDS HERE -------------------------------
// */
//

}
