/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.forms;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import databinding.myStroke;



public class Line extends StrokedForm
{
	public static int DEFAULT_LINE_WIDTH = 1;

	public Point from;
	public Point to;

	public Line() {
		from = new Point();
		to = new Point();
	}

	public Line(myStroke stroke, Color c, Point2D from, Point2D to) {
		super(stroke, c);
		this.from = new Point((int)from.getX(), (int)from.getY());
		this.to =   new Point((int)to.getX(), (int)to.getY());
	}


	public void move(int dx, int dy) {
		from.translate(dx, dy);
		to.translate(dx, dy);
	}


	// from XML constructor
//	public Line(Point from, Point to, Color c, int lineWidth)
//	{
//		super(c, lineWidth);
//		this.from = from;
//		this.to =   to;
//	}


//    public Line(FormGroup parent, Point from, Point to, Color c, int lineWidth)
//    {
//		super(parent);
//		this.from = from;
//		this.to =   to;
//		this.c =    c;
//		this.lineWidth = lineWidth;
//    }

//	public <T> void handle(CTWalker<T> w, T param) { w.handle(this,param); }
//	public <T,X> X handle(CTWalkerR<T,X> w, T param) { return w.handle(this,param); }


	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert from != null;
		assert to != null;
		return true;
	}


	public Point getFrom() { return from; }
	public Point getTo() { return to; }

	public int getFromX() { return from.x; }
	public void setFromX(int x) { from.x = x; }

	public int getFromY() { return from.y; }
	public void setFromY(int y) { from.y = y; }

	public int getToX() { return to.x; }
	public void setToX(int x) { to.x = x; }

	public int getToY() { return to.y; }
	public void setToY(int y) { to.y = y; }


//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}



//	public Line(EditorGUIElement parent, XMLReader xml) throws Exception
//	{
//		super(parent);
//		from = new Point(
//			Integer.parseInt(xml.atts.getNamedItem("FROMX").getNodeValue()),
//			Integer.parseInt(xml.atts.getNamedItem("FROMY").getNodeValue()));
//		to = new Point(
//			Integer.parseInt(xml.atts.getNamedItem("TOX").getNodeValue()),
//			Integer.parseInt(xml.atts.getNamedItem("TOY").getNodeValue()));
//
//		c =         Converter.toColor(xml.atts, "COLOR", Color.black);
//		lineWidth = Converter.toInteger(xml.atts, "LINEWIDTH", new Integer(DEFAULT_LINE_WIDTH));
//	}

	public String toString()
	{
		return "Line";
	}


//	public void draw(Point offset, Graphics2D g)
//    {
//		g.setColor(c);
//
//		Stroke old = g.getStroke();
//		g.setStroke(new BasicStroke(lineWidth));
//
//		g.drawLine(from.x + offset.x, from.y + offset.y, to.x + offset.x, to.y + offset.y);
//
//		g.setStroke(old);
//
//    }

    public Rectangle2D getBounds(Graphics2D g)
    {
		float lineWidth = getStroke().getLineWidth();
		return new Rectangle2D.Double(Math.min(from.x, to.x) - lineWidth/2, Math.min(from.y, to.y)-lineWidth/2, Math.abs(to.x-from.x)+lineWidth, Math.abs(to.y-from.y)+lineWidth);
    }


	public static double d_point2line(Point from, Point to, Point pos)
	{
		double dx = to.x - from.x;
		double dy = to.y - from.y;

		double d2x = pos.x - from.x;
		double d2y = pos.y - from.y;

		if (dx == 0 && dy == 0) // Lijn is een punt
		{
			return pos.distance(from);
		}

		double u = (d2x*dx + d2y*dy) / (dx*dx + dy*dy);

		     if (u < 0) return pos.distance(from);
		else if (u > 1) return pos.distance(to);
		else            return pos.distance(new java.awt.geom.Point2D.Double(from.x + u * dx, from.y + u * dy));
	}

//	public GUIElement getGUIElement(Point pos, Graphics2D g)
//	{
//		if (d_point2line(from, to, pos) < lineWidth+4)      return this;
//		else                                                return null;
//	}

	public void move(Point delta)
	{
		from.x += delta.x;
		from.y += delta.y;
		to.x +=   delta.x;
		to.y +=   delta.y;
	}


//	public RenderNode createRenderNode() {
//		return new RenderNode() {
//			public void render(Graphics2D g) {
//				g.setColor(getColor());
//
//				Stroke old = g.getStroke();
//				g.setStroke(new BasicStroke(getLineWidth()));
//				Point from = getFrom();
//				Point to = getTo();
//
//				g.drawLine(from.x, from.y, to.x, to.y);
//
//				g.setStroke(old);
//			}
//		};
//	}


//	public Locator [] getLocators()
//	{
//		return new Locator [] {new From_Locator(), new To_Locator()};
//	}


//
//	public void toXML(XMLWriter xml, File baseDirectory)
//	{
//		xml.openTag("LINE");
//		xml.addAttribute("FROMX",   from.x);
//		xml.addAttribute("FROMY",   from.y);
//		xml.addAttribute("TOX",     to.x);
//		xml.addAttribute("TOY",     to.y);
//		if (c != Color.black) xml.addAttribute("COLOR", Converter.toString(c));
//		xml.addAttribute("LINEWIDTH", lineWidth);
//		xml.closeTag();
//	}
//
//
//	public class From_Locator extends Locator
//	{
//		public void move(Point delta)
//		{
//			from.x += delta.x;
//			from.y += delta.y;
//		}
//
//		public void draw(Point offset, Graphics2D g)
//		{
//			g.setColor(Color.blue);
//			g.fill3DRect(offset.x + from.x - 2, offset.y + from.y - 2, 5, 5, true);
//		}
//
//		public java.awt.Rectangle getBounds(Graphics2D g)
//		{
//			return new java.awt.Rectangle(from.x - 2, from.y - 2, 5, 5);
//		}
//
//		public GUIElement getGUIElement(Point offset, Graphics2D g)
//		{
//			if (getBounds(g).contains(offset))  return this;
//			else                                return null;
//		}
//	}
//
//
//	public class To_Locator extends Locator
//	{
//		public void move(Point delta)
//		{
//			to.x += delta.x;
//			to.y += delta.y;
//		}
//
//		public void draw(Point offset, Graphics2D g)
//		{
//			g.setColor(Color.blue);
//			g.fill3DRect(offset.x + to.x - 2, offset.y + to.y - 2, 5, 5, true);
//		}
//
//		public java.awt.Rectangle getBounds(Graphics2D g)
//		{
//			return new java.awt.Rectangle(to.x - 2, to.y - 2, 5, 5);
//		}
//
//		public GUIElement getGUIElement(Point offset, Graphics2D g)
//		{
//			if (getBounds(g).contains(offset))  return this;
//			else                                return null;
//		}
//	}
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TREE ---------------------
// */
//
//
// /*--------------- TREE REPRESENTATION ENDS HERE --------------------------------
// */
//
//
///*--------------- THIS OBJECT CAN BE REPRESENTED IN A TABLE --------------------
// */
//
//	private static final String [] names = {"FromX",       "FromY",        "ToX",     "ToY",       "Color", "Line Width"};
//
//	//Returns the number of rows in the model.
//	public int getRowCount()                                    {return names.length;}
//
//	//Returns the value for the cell at columnIndex and rowIndex.
//	public Object getValueAt(int rowIndex, int columnIndex)
//	{
//		if (columnIndex == 0) return names[rowIndex];
//		else
//		switch (rowIndex)
//		{
//			case 0: return new Integer(from.x);
//			case 1: return new Integer(from.y);
//			case 2: return new Integer(to.x);
//			case 3: return new Integer(to.y);
//			case 4: return c;
//			case 5: return new Integer(lineWidth);
//		}
//		return null;
//	}
//
//	//Sets the value in the cell at columnIndex and rowIndex to aValue.
//	public int setValue(Object value, int rowIndex, int columnIndex)
//	{
//		switch (rowIndex)
//		{
//			case 0: from.x =    ((Integer)value).intValue();        break;
//			case 1: from.y =    ((Integer)value).intValue();        break;
//			case 2: to.x =      ((Integer)value).intValue();        break;
//			case 3: to.y =      ((Integer)value).intValue();        break;
//			case 4: c =         ((Color)value);                     break;
//			case 5: lineWidth = ((Integer)value).intValue();        break;
//		}
//		return UPDATE_DISPLAY;
//	}
//
///*--------------- TABLE REPRESENTATION ENDS HERE -------------------------------
// */

}
