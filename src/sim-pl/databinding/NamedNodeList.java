/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


public abstract class NamedNodeList<P extends BasicEditorNode, T extends BasicEditorNode<?> & NameDeclaringNode> extends NodeList<P,T> {
	public NamedNodeList() {}

	public T getByName(String name) {
		for (T c : children) {
			if (c.getName().equals(name)) return c;
		}
		return null;
	}

	public boolean isWellFormed() {
		assert super.isWellFormed();

		Set<String> names = new HashSet<String>();
		for (T c : children) {
			assert names.add(c.getName()) : "name \"" + c.getName() + "\" occurs twice";
		}
		return true;
	}


	public void collectForbidden(Set<String> forbidden) {
		for (T c : children) forbidden.add(c.getName());
	}

	public String createNewName(String basename) {
		int suffix = 2; // if basename is the name of some child, we want basename (2)
		boolean found = false;
		Pattern p = Pattern.compile("^" + Pattern.quote(basename) +" \\((\\d+)\\)$");
		for (T c : children) {
			Matcher m = p.matcher(c.getName());
			found = found | c.getName().equals(basename);
			if (m.matches()) {
				try {
					suffix = Math.max(suffix, 1+Integer.parseInt(m.group(1)));
				} catch (NumberFormatException ex) {}
			}
		}
		return found
			? basename + " (" + suffix + ")"
			: basename;
	}

}
