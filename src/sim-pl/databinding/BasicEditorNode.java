/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.Collection;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.Identifier;

public interface BasicEditorNode<T extends BasicEditorNode>  {
//	public List<? extends BasicEditorNode<?>> getChildren();
	public T getParent();
	public void setParent(T parent);
	public void collectUsedParameters(Set<String> params);

	public void collectDefs(String cfnContext, Collection<Identifier> ids);
	public void collectRefs(ComponentNamespace ns, Collection<Identifier> ids);

	public boolean canBeDetached();

	public void wire(T parent);


	/** Well-formed means: all required fields are set, all references are ok
	 * this basically verifies the integrity of the object graph; it is
	 * syntactic correctness
	 * */
	public boolean isWellFormed();

	/** Valid means: a node is valid if it is well-formedness and all semantic
	 * requirements are satisfied. This includes number of children, graph
	 * connectedness, etc. */
	public boolean isValid();

	// these two are as above, but recursively
	public boolean isWellFormedRecursively();
	public boolean isValidRecursively();

	public void collectInvolvedInMove(Set<BasicEditorNode> selection, Set<IMoveable> movers);
}
