/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import databinding.wires.MainPinStub;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public abstract class Pin extends FixedList<IO,PinLabel> implements NameDeclaringNode, IMoveable, IPositioned {
	String name;
	IntResolver bits;
	boolean signed;
	final Point2D.Double pos =   new Point2D.Double();

	// 'children' (always a single node)
	final PinLabel       label;
	public List<PinLabel> getChildren() { return Collections.singletonList(label); }

	// for internal reference only
	MainPinStub mps;


	public Pin() {
		 label = new PinLabel();
	}

	public Pin(String name, IntResolver bits, boolean signed, Point2D pos, PinLabel pinLabel) {
		this.name = name;
		this.bits = bits;
		this.signed = signed;
		this.pos.setLocation(pos);
		this.label = pinLabel;
		this.label.setParent(this);
	}

	// allow subclass default constructor to set label (they know where it should go)
	protected Pin(PinLabel pinLabel) {
		this.label = pinLabel;
	}

	public String toString() {
		return name;
	}

	public void move(int dx, int dy) {
		pos.x += dx;
		pos.y += dy;
	}

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Class getType() { return Pin.class; }

	public IntResolver getBits() { return bits; };
	public void setBits(IntResolver bits) { this.bits = bits; };

	public boolean isSigned() { return signed; }
	public void setSigned(boolean  signed) { this.signed = signed; }

	public double getX() { return pos.x; }
	public void setX(double x) { pos.x = x; }

	public double getY() { return pos.y; }
	public void setY(double y) { pos.y = y; }

	public Point2D.Double getPos() { return pos; }

	public PinLabel getLabel() { return label; }


	public void collectUsedParameters(Set<String> params) {
		bits.collectUsedParameters(params);
	}

	public boolean isOutput() {
		return this instanceof Output;
	}

	public boolean isInput() {
		return this instanceof Input;
	}

	public boolean isWellFormed() {
		assert super.isWellFormed();

		if (mps != null) {
			assert mps.getPin() == this;
			assert mps.getParent().getChildren().contains(mps);
		}

		return true;
	}

	public MainPinStub getNode() {
		return mps;
	}

	public void setNode(MainPinStub mps) {
		assert this.mps == null || mps == null;
		this.mps = mps;
	}
	
	public void collectAffected(Set<BasicEditorNode> nodes) {
		nodes.add(this); // no other nodes depend on our position
		nodes.add(label);
		if (getNode() != null) getNode().collectAffected(nodes);
	}
}
