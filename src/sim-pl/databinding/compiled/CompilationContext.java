/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.compiled;

import java.util.IdentityHashMap;

import databinding.BasicEditorNode;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class CompilationContext {
	IdentityHashMap<BasicEditorNode<? extends BasicEditorNode>, NodeCompilationResult> n2r = new IdentityHashMap<BasicEditorNode<? extends BasicEditorNode>,NodeCompilationResult>();

	public CompilationContext(BasicEditorNode<? extends BasicEditorNode> root) {
		init_walk(root);
	}

	private void init_walk(BasicEditorNode<? extends BasicEditorNode> root) {
		n2r.put(root, compile(root));
//      TODO; remove this class or fix these lines
//		for (BasicEditorNode<?> c : root.getChildren()) {
//			init_walk(c);
//		}
	}

	private NodeCompilationResult compile(BasicEditorNode node) {
		String name = CompiledNode.class.getPackage().getName()
			+ ".Compiled" + node.getClass().getSimpleName()
			+ "Node";

		CompiledNode o = null;
		try {
			Class c = getClass().getClassLoader().loadClass(name);
			o = (CompiledNode)c.getConstructor(node.getClass()).newInstance(node);
		} catch (ClassNotFoundException ex) {
		}
		catch (Exception ex) {
			throw new Error(ex);
		}

		NodeCompilationResult ncr = new NodeCompilationResult();
		ncr.source = node;
		ncr.result = o;

		recompile(node, ncr);

		return ncr;
	}


	private void recompile(BasicEditorNode node, NodeCompilationResult ncr) {
		/** remove all old defs, refs and messages */

		throw new Error("No compile implemented yet");

		/** add all new defs, refs and messages */
	}
}
