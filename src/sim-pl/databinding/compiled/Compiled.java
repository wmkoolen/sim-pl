/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.compiled;

import java.util.HashMap;
import java.util.Map;

import util.MyFormatter;

import compiler.wisent.Statement;
import compiler.wisent.Type_Struct;

import databinding.BasicEditorNode;
import databinding.Complex;
import databinding.Component;
import databinding.Event;
import databinding.Pin;
import databinding.Simple;
import databinding.Storage;
import databinding.SubComponent;

/** Light-weight classes that represent compiled component information
 *  Each such class depends on the context of compilation, and many compiled<X>
 *  classes can correspond to a single parametrized peer.
 */
public abstract class Compiled <T extends BasicEditorNode<?>> {
	public final T peer;

	public Compiled(T peer) {
		this.peer = peer;
	}

	public static class CompiledPin extends Compiled<Pin>{
		public final int bits;
		public final boolean wired;

		public CompiledPin(Pin peer, int bits, boolean wired) {
			super(peer);
			this.bits = bits;
			this.wired = wired;
		}

		public String toString() {
			return peer.getName() + " " + bits + " " + wired;
		}
	}

	public static class CompiledStorage extends Compiled<Storage>{
		public final int bits;
		public final int width;

		public CompiledStorage(Storage peer, int bits, int width ) {
			super(peer);
			this.bits = bits;
			this.width = width;
		}
	}

	public static class CompiledComponent<T extends Component> extends Compiled<T> {
		public final Map<Pin, CompiledPin> pin2cpin = new HashMap<Pin,CompiledPin>();

		public CompiledComponent(T peer) {
			super(peer);
		}

		public void addCompiledPin(Pin p, Integer width, boolean wired) {
			assert !pin2cpin.containsKey(p);
			pin2cpin.put(p, new CompiledPin(p, width, wired));
		}

		public String toString() {
			return peer.getName() + MyFormatter.join(" -> ", " ", pin2cpin);
		}

	}

	public static class CompiledSimple extends CompiledComponent<Simple> {
		public final Map<Storage, CompiledStorage> storage2cstorage = new HashMap<Storage,CompiledStorage>();
		public final Statement [] wcdl = new Statement[Event.events.length];
		public final Type_Struct type = new Type_Struct();

		public CompiledSimple(Simple peer) {
			super(peer);
		}

		public void setBitsNWidth(Storage s, int bits, int width) {
			assert !storage2cstorage.containsKey(s);
			storage2cstorage.put(s, new CompiledStorage(s, bits, width));
		}
	}

	public static class CompiledComplex extends CompiledComponent<Complex>{
		public final Map<SubComponent, CompiledComponent<?>> sub2csub = new HashMap<SubComponent,CompiledComponent<?>>();

		public CompiledComplex(Complex peer) {
			super(peer);
		}

		public String toString() {
			return super.toString() + " {" + MyFormatter.join(", ", sub2csub.keySet()) + "}\n";
		}
	}
}
