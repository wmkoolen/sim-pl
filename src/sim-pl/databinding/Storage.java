/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.Collection;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.Identifier;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Storage extends EditorNode<Memory> implements NameDeclaringNode {
	String name;
	IntResolver size;
	IntResolver bits;
	boolean signed;

	public Storage() {
	}

	public Storage(String name, IntResolver bits, IntResolver size, boolean signed) {
		this.name = name;
		this.bits = bits;
		this.size = size;
		this.signed = signed;
	}



	public String toString() {
		return name;
	}


	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	public Class getType() { return Storage.class; }

	public IntResolver getSize() { return size; }
	public void setSize(IntResolver size) { this.size = size; }

	public IntResolver getBits() { return bits; }
	public void setBits(IntResolver bits) { this.bits = bits; }

	public boolean isSigned() { return signed; }
	public void setSigned(boolean  signed) { this.signed = signed; }



	public void collectUsedParameters(Set<String> params) {
		bits.collectUsedParameters(params);
		size.collectUsedParameters(params);
	}

	public void collectRefs(ComponentNamespace ns, Collection<Identifier> ids) {

	}

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}
//
//	public RenderNode createRenderNode() {
//		return null;
//	}

}
