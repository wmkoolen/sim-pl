/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.EditorNode;
import databinding.IMoveable;
import databinding.myStroke;

/** A span is a maximal sequence of edges
 * (n1,n2)-(n2,n3)-(n3,n4)-...-(nk,n{k+1})
 * such that n2,....nk are binary nodes
 */

public class Span extends EditorNode<Spans>  {
	public final ArrayList<Edge> edges = new ArrayList<Edge>();

	public String toString() { return "Span"; }

	public boolean isValid() {
		assert super.isValid();

		assert !edges.isEmpty();

		// go through edges pair-wise
		Edge prev = null;

		for (Edge cur: edges) {
			if (prev == null) {
				// make sure this span begins with a valid first edge
				assert cur.n1.isSpanEnd();
			} else {
				// make sure this is a good mid-span edge
				assert !prev.n2.isSpanEnd();
			}
			prev = cur;
		}
		// make sure this span ends with a valid last edge
		assert prev.n2.isSpanEnd();

		return true;
	}

	public boolean isWellFormed() {
		assert super.isWellFormed();

		// go throuth edges pair-wise
		Edge prev = null;

		for (Edge cur: edges) {
			// checks for every edge
			assert cur.span == this;
			assert cur.n1 != cur.n2;
			assert cur.getNode1().incident.contains(cur);
			assert cur.getNode2().incident.contains(cur);

			// make sure edges are consecutive
			assert prev == null || prev.n2 == cur.n1;
			assert prev == null || prev.n2 instanceof Split : "non-split intermediate node";
			prev = cur;
		}

		return true;
	}

	public static class EdgeDistance {
		Edge   e = null;
		double d = Double.POSITIVE_INFINITY;
	}

	void updateClosestEdge(EdgeDistance ed, Point2D p) {
		for (Edge e : edges) {
			double cd = e.dist(p);
			if (cd < ed.d) {
				ed.e = e;
				ed.d = cd;
			}
		}
		assert ed.e != null;
	}

	public Edge getClosestEdge(Point2D p) {
		EdgeDistance ed = new EdgeDistance();
		updateClosestEdge(ed, p);
		return ed.e;
	}

	public void reverse() {
		assert isWellFormed();
		Collections.reverse(edges);
		for (Edge e : edges) {
			e.reverse();
		}
		assert isWellFormed();
	}

	public Node getNode1() { return edges.get(0).n1; }
	public Node getNode2() { return edges.get(edges.size()-1).n2; }

	/** Convenience method. Given some end node, finds the other end node
	 * @param n Node some end node
	 * @return Node the other end node
	 */
	public Node getOtherNode(Node n) {
		Node n1 = getNode1(), n2 = getNode2();
		assert n1 == n || n2 == n;
		return n1 == n ? n2 : n1;
	}
	
	
	public void collectInvolvedInMove(Set<BasicEditorNode> selection, Set<IMoveable> movers) {
		for (int i = 1; i < edges.size(); i++) {
			Split internal = (Split)edges.get(i).getNode1();
			assert !selection.contains(internal);
			movers.add(internal);
		}
	}
	


	public String getWireName() { return parent.getParent().getName(); }
	public void setWireName(String name) { parent.getParent().setName(name); }

	public Color getWireColor() { return parent.getParent().getColor(); }
	public void setWireColor(Color color) { parent.getParent().setColor(color); }

	public myStroke getWireStroke() { return parent.getParent().getStroke(); }
	public void setWireStroke(myStroke stroke) { parent.getParent().setStroke(stroke); }

}

