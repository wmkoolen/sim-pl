/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.IMoveable;
import databinding.IPositioned;
import databinding.forms.PPoint;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Split extends Node implements IMoveable, IPositioned {
	Point2D pos = new Point2D.Double();

	public Split() {}

	public Split(Point2D p) {
		this.pos.setLocation(p);
	}

	public String toString() { return "Split"; }

	public void move(int dx, int dy) {
		pos.setLocation(pos.getX() + dx, pos.getY() + dy);
	}

	public Point2D getPos() {return pos; }
	public void setPos(Point2D  p) { this.pos.setLocation(p); }

	public PPoint getPPos() {return new PPoint(pos); }
	public void setPPos(PPoint p) { this.pos.setLocation(p.getX(), p.getY()); }


	public double getX() { return pos.getX(); }
	public void setX(double x) { pos.setLocation(x, pos.getY()); }

	public double getY() { return pos.getY(); }
	public void setY(double y) { pos.setLocation(pos.getX(), y); }

	public void collectAffected(Set<BasicEditorNode> nodes) {
		if (isSpanEnd()) nodes.add(this);
		super.collectAffected(nodes);
	}
}
