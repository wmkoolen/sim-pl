/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import util.UnionFind;
import databinding.BasicEditorNode;
import databinding.Complex;
import databinding.Component;
import databinding.NamedNodeList;
import databinding.NodeList;
import databinding.Pin;
import databinding.SubComponent;
import databinding.SubPinStub;
import editor.tool.CurrentProperties;
import editor.tool.WireTool.NodeStub;
import editor.tool.WireTool.PotentialNode;
import editor.tool.WireTool.StubMainStub;
import editor.tool.WireTool.StubStub;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Wires extends NamedNodeList<Complex,Wire> {

   /** The global pool of nodes. At the moment, these are
	*   - split         (that is, an arbitrary point in the plane)
	*   - sub-pin-stub  (adapter; translates subpin into node)
	*   - main-pin-stub (adapter; tranlates main pin into node)
	*/

//	/** Maps main-pins to stubs */
//	Map<Pin,MainPinStub> pin2stub = new HashMap<Pin,MainPinStub>();


	public Wires() {}
	public String toString() { return "Wires"; }


	public Span connect(Node n1, Node n2, List<Point2D> points, MutatorList muts) {
		assert n1 != n2;
		Wire w1 = n1.getParent() != null ? n1.getParent().getParent() : null;
		Wire w2 = n2.getParent() != null ? n2.getParent().getParent() : null;

		// the joint wire that will contain n1 and n2
		Wire w = null;

		if (w1 == w2) {
			w = w1;
			if (w == null) {
				CurrentProperties cps = CurrentProperties.getProps();
				w = new Wire(createNewName("Wire"), cps.getColor(), cps.getStroke());
				muts.add(add(w));
				assert isWellFormedRecursively();
				muts.add(w.nodes.add(n1));
				muts.add(w.nodes.add(n2));
			}
		} else if (w2 == null) {
			w = w1; assert w1 != null;
			muts.add(w.nodes.add(n2));
		} else if (w1 == null) {
			w = w2; assert w2 != null;
			muts.add(w.nodes.add(n1));
		} else {
			assert w1 != null && w2 != null && w1 != w2;
			w = mergeWires(w1, w2, muts);
		}
		assert n1.getParent().getParent() == w;
		assert n2.getParent().getParent() == w;
		assert w.getParent() == this;

		// liberate pin 1 if necessary
		if (n1.incident.size() == 2 &&
			n1.incident.get(0).span == n1.incident.get(1).span) {
			muts.add(liberate(n1));
		}

		// liberate pin 2 if necessary
		if (n2.incident.size() == 2 &&
			n2.incident.get(0).span == n2.incident.get(1).span) {
			muts.add(liberate(n2));
		}

		// then connect
		return w.connect(n1, n2, points, muts);
	}




	/** Inserts the given node on the given edge
	 * @param edge Edge
	 * @param n Node
	 * @param muts MutatorList
	 * @return Node the new split node
	 */

	public Split splitEdge(final Edge edge, final Point2D p, MutatorList muts) {

		final Span s = edge.span;
		final Spans ss = s.getParent();
		final Wire w = ss.getParent();

		final Node n1 = edge.getNode1();
		final Node n2 = edge.getNode2();
		assert n1 != n2;

		final int i_edge = s.edges.indexOf(edge);
		final int in1 = n1.incident.indexOf(edge);
		final int in2 = n2.incident.indexOf(edge);

		// the new middle
		Split n = new Split(p);

		// the new edges, which replace edge
		final Edge el = new Edge(n1, n);
		final Edge er = new Edge(n, n2);
		n.incident.add(el);
		n.incident.add(er);
		el.span = s;
		er.span = s;

		// add the new node to the wire
		muts.add(w.nodes.add(n));

		muts.add(new Mutator() {
			public void pre() {
				assert n1.incident.get(in1) == edge;
				assert n2.incident.get(in2) == edge;
				assert s.edges.get(i_edge) == edge;
			}
			public void commit() {
				s.edges.set(i_edge, el);
				s.edges.add(i_edge+1,er);

				n1.incident.set(in1, el);
				n2.incident.set(in2, er);
			}
			public void post() {
				assert s.edges.get(i_edge) == el;
				assert s.edges.get(i_edge+1) == er;

				assert !s.edges.contains(edge);
				assert isValidRecursively();
				assert n1.incident.get(in1) == el;
				assert n2.incident.get(in2) == er;
				assert s.edges.get(i_edge) == el;
				assert s.edges.get(i_edge+1) == er;

			}

			public void rollback() {
				s.edges.set(i_edge, edge);
				s.edges.remove(i_edge+1);

				n1.incident.set(in1, edge);
				n2.incident.set(in2, edge);
			}
		}.perform());
		return n;
	}



	/** Inserts the given node on the given edge */

	public void dropNode(final Split n, MutatorList muts) {
		assert isValidRecursively();
		assert n.getParent() != null;
		assert !n.isSpanEnd();
		assert n.incident.size() == 2;

		Edge e1aux = n.incident.get(0),
			 e2aux = n.incident.get(1);
		final Edge el = e1aux.n2 == n ? e1aux : e2aux,
			       er = e1aux.n2 == n ? e2aux : e1aux;
	    final Node nl = el.n1,
		           nr = er.n2;
		assert nl != nr;
		assert el.n2 == n;
		assert er.n1 == n;

		final Span  s = el.span;
		final Spans ss = s.getParent();
		final Wire  w = ss.getParent();

		final int i_edge = s.edges.indexOf(el);
		assert s.edges.indexOf(er) == i_edge+1;

		final int in  = w.nodes.getChildren().indexOf(n);
		final int inl = nl.incident.indexOf(el);
		final int inr = nr.incident.indexOf(er);

		// the new edge, which replaces e1 and e2
		final Edge edge = new Edge(nl, nr);
		edge.span = s;

		muts.add(new Mutator() {
			public void pre() {
				assert nl.incident.get(inl) == el;
				assert nr.incident.get(inr) == er;
				assert s.edges.get(i_edge) == el;
				assert s.edges.get(i_edge+1) == er;
				assert w.nodes.getChildren().get(in) == n;
				assert isValidRecursively();
			}

			public void commit() {
				s.edges.set(i_edge, edge);
				s.edges.remove(i_edge+1);

				nl.incident.set(inl, edge);
				nr.incident.set(inr, edge);

				w.nodes.getChildren().remove(in);
			}

			public void post() {
				assert !w.nodes.getChildren().contains(n);
				assert s.edges.get(i_edge) == edge;
				assert isValidRecursively();
				assert nl.incident.get(inl) == edge;
				assert nr.incident.get(inr) == edge;

			}

			public void rollback() {
				s.edges.set(i_edge, el);
				s.edges.add(i_edge+1,er);

				nl.incident.set(inl, el);
				nr.incident.set(inr, er);

				w.nodes.getChildren().add(in, n);
			}
		}.perform());
	}





	public SubPinNode getPinStub(String pinref) {
		for (Wire wi: children) {
			for (Node n : wi.nodes.getChildren()) {
				if (!(n instanceof SubPinNode)) continue;
				SubPinNode ps = (SubPinNode)n;
				if (ps.getRef().equals(pinref)) return ps;
			}
		}
		return null;
	}

	public void addStubs(ArrayList<PotentialNode> pins, boolean includeNewStubs) {
		// get main stubs
		for (Pin p : parent.getIo().getChildren()) {
			if (p.getNode() != null) pins.add(new NodeStub(p.getNode()));
			else if (includeNewStubs) pins.add(new StubMainStub(p));
		}

		// get subcomponent stubs
		for (SubComponent s : parent.getSubcomponents().getChildren()) {
			if (!s.isResolved()) continue;
			Component c = s.getSubComponent();
			Set<String> seen = new HashSet<String>();
			for (SubPinStub sps : s.getSubPinStubs().getChildren()) {
				seen.add(sps.getPinRef());
				pins.add(new NodeStub(sps.getNode()));
			}

			if (includeNewStubs) {
				for (Pin p : c.getIo().getChildren()) {
					if (seen.add(p.getName())) {
						pins.add(new StubStub(s, p));
					}
				}
			}
		}
	}


//	public void wire(Complex parent) {
//		super.wire(parent);
//	}

//	public Mutator removeSpan(final Span span) {
//		assert nodeValid(getParent());
//
//		final Wire w = span.getParent().getParent();
//
//		// check whether wire becomes disconnected when removing span
//		MutatorList muts = new MutatorList();
//
//		muts.add(new Mutator() {
//			public void redo() {
//				for (Edge e : span.edges) {
//					assert e.getNode1().incident.contains(e);
//					assert e.getNode2().incident.contains(e);
//					e.getNode1().incident.remove(e);
//					e.getNode2().incident.remove(e);
//				}
//				w.spans.getChildren().remove(span);
//			}
//
//			public void undo() {
//				w.spans.getChildren().add(span);
//				for (Edge e : span.edges) {
//					assert !e.getNode1().incident.contains(e);
//					assert !e.getNode2().incident.contains(e);
//					e.getNode1().incident.add(e);
//					e.getNode2().incident.add(e);
//				}
//			}
//		}.perform());
//
//		return muts;
//	}






/** GRAPH HANDLING METHODS */
//	public boolean isValid() {
//		assert super.isValid();
////		System.out.println("Semantic check");
//
////		// check that all mainpin-stubs are necessary
////		for (Map.Entry<Pin,MainPinStub> e : pin2stub.entrySet()) {
////			assert parent.getIo().getChildren().contains(e.getKey()) : e.getKey() + " is not a pin in component";
////			assert e.getValue().getParent() != null : e.getValue() + " is not a node in graph";
////			assert !e.getValue().incident.isEmpty(): e.getValue() + " is useless";
////		}
//
////		// check that all subcomponent-pin stubs are necessary
////		for (Map.Entry<String,SubPinNode> e : ref2stub.entrySet()) {
////			assert e.getValue().getParent() != null : e.getValue() + " is not a node in graph";
////			assert !e.getValue().incident.isEmpty(): e.getValue() + " is useless";
////		}
//
//		for (Wire w: children) {
//			// we need nodes to exist
//			assert !w.nodes.getChildren().isEmpty() : "wire " + this + " has no nodes licensing existence";
//			// we need spans to exist
//			assert !w.spans.getChildren().isEmpty() : "wire " + this + " has no spans licensing existence";
//		}
//		return true;
//	}


/** EXTERNAL METHODS */



	// - removes all incident edges,
	// - removes this node
	// - then collapses binary nodes
	// EXTERNAL
	public Mutator removeNode(final Node node) {
		assert node.getParent() != null;
		assert new HashSet<Edge>(node.incident).size() == node.incident.size();

		// if it's not an end node, we can do with removing the edge through it
		if (!node.isSpanEnd()) {
			assert node instanceof Split;
			assert node.incident.size() == 2;
			assert node.incident.get(0).span == node.incident.get(1).span;
			return removeEdge(node.incident.get(0).span);
		}


		assert node.isSpanEnd();

		final Wire w = node.getParent().getParent();

		MutatorList muts = new MutatorList() {
			public void pre()  {
				assert isValidRecursively(); super.pre();
			}
			public void post() {
				super.post();  isValidRecursively();
			}
		};
		muts.pre();

		Set<Node> collapseCheck = new HashSet<Node>();
		for (int i = node.incident.size()-1; i >= 0; --i) {
			assert node.incident.size() == i+1;
			Span s = node.incident.get(i).span;
			collapseCheck.add(s.getOtherNode(node));
			muts.add(removeEdgePure(s));
		}

		assert children.contains(w);
		removeUnconnectedNode(node, muts);

		postProcess(w, muts, collapseCheck);

		muts.post();

		return muts;
	}




	/* removes an edge, and prunes the remaining graph */
	// EXTERNAL METHOD
	public Mutator removeEdge(final Span span) {
		assert span.getParent() != null;
		assert span.getParent().getChildren().contains(span);
		assert isValidRecursively();
		Node n1 = span.getNode1();
		Node n2 = span.getNode2();
		Wire w = span.getParent().getParent();

		MutatorList muts = new MutatorList();

		muts.add(removeEdgePure(span));
		// prune unwanted stuff
		postProcess(w, muts, Arrays.asList(n1, n2));

		assert isValidRecursively();
		return muts;
	}



/** PRIVATE METHODS */

	private void removeUnconnectedNode(final Node node, MutatorList muts) {
		assert isWellFormedRecursively();
		assert node.getParent() != null;
		Wire w = node.getParent().getParent();

		assert w.nodes.getChildren().contains(node);
		assert node.incident.isEmpty();

		muts.add(node.getParent().removePure(node));

		if (node instanceof SubPinNode) {
			SubPinStub sps = ((SubPinNode)node).getStub();
			muts.add(sps.getParent().removePure(sps));
		}
		if (node instanceof MainPinStub) {
			final MainPinStub mps = (MainPinStub)node;
			muts.add(new Mutator() {
				protected void commit()     { mps.p.setNode(null);}
				protected void rollback()   { mps.p.setNode(mps); }
				protected void pre()        { assert mps.p.getNode() == mps; }
				protected void post()       { assert mps.p.getNode() == null; }
			}.perform());
		}

	}


	private Mutator removeEdgePure(final Span span) {
		assert isWellFormedRecursively();

		final Edge e1 = span.edges.get(0),
			       e2 = span.edges.get(span.edges.size()-1);

	    final Node n1 = span.getNode1();
		final Node n2 = span.getNode2();
		assert e1.n1 == n1;
		assert e2.n2 == n2;
		final Wire w = span.getParent().getParent();

		final int n1index = n1.incident.indexOf(e1);
		final int n2index = n2.incident.indexOf(e2);
		final int cindex = w.spans.getChildren().indexOf(span);

		MutatorList muts  = new MutatorList();
		// ruthlessly kick out all intermediate nodes
		for (int i = 1; i < span.edges.size(); i++) {
			final Node n = span.edges.get(i).getNode1();
			assert n != n1 && n != n2;
//			final int ni = w.nodes.getChildren().indexOf(n);
			muts.add(w.nodes.removePure(n));
//			muts.add(new Mutator() {
//				public void redo() {
//					assert w.nodes.getChildren().get(ni) == n;
//					w.nodes.getChildren().remove(ni);
//				}
//
//				public void undo() {
//					assert !w.nodes.getChildren().contains(n);
//					w.nodes.getChildren().add(ni,n);
//				}
//			}.perform());
		}

		// then take care of the first and last node
		muts.add(new Mutator() {
			public void pre() {
				assert n1 != n2;
				assert n1.incident.get(n1index) == e1;
				assert n2.incident.get(n2index) == e2;
				assert w.spans.getChildren().get(cindex) == span;
			}

			public void post() {
				assert n1 != n2;
				assert !n1.incident.contains(e1);
				assert !n2.incident.contains(e2);
				assert !w.spans.getChildren().contains(span);
				assert isWellFormedRecursively();
			}

			public void commit() {
				n1.incident.remove(n1index);
				n2.incident.remove(n2index);
				w.spans.getChildren().remove(cindex);
			}

			public void rollback() {
				n2.incident.add(n2index, e2); // reverse order
				n1.incident.add(n1index, e1);
				w.spans.getChildren().add(cindex, span);
			}
		}.perform());
		return muts;
	}



	private Wire mergeWires(Wire w1, Wire w2, MutatorList muts) {
		assert isWellFormedRecursively();
		assert w1.getParent() == this;
		assert w2.getParent() == this;

		// TODO: ask the user which properties to give w
		Wire w = new Wire(w1.getName(), w1.getColor(), w1.getStroke());

		muts.add(remove(w1));
		muts.add(remove(w2));
		muts.add(add(w));

		// use toArray to prevent concurrentmodification exception
		for (Node n : w1.nodes.getChildren().toArray(new Node [0])) muts.add(new MoveOver(w1.nodes, w.nodes, n).perform());
		for (Node n : w2.nodes.getChildren().toArray(new Node [0])) muts.add(new MoveOver(w2.nodes, w.nodes, n).perform());
		for (Span s : w1.spans.getChildren().toArray(new Span [0])) muts.add(new MoveOver(w1.spans, w.spans, s).perform());
		for (Span s : w2.spans.getChildren().toArray(new Span [0])) muts.add(new MoveOver(w2.spans, w.spans, s).perform());

		assert isWellFormedRecursively();

		return w;
	}








	/** Decides whether node can be collapsed without forming a cycle. If it can,
	 * return the offending node.
	 */

	private Node getCollapseCycleNode(Node n) {
		assert n.incident.size() == 2;
		Span s1 = n.incident.get(0).span;
		Span s2 = n.incident.get(1).span;
		assert s1 != s2;

		Node nl = s1.getOtherNode(n);
		Node nr = s2.getOtherNode(n);
		assert nl != n;
		assert nr != n;
		return nl == nr ? nl : null;
	}


	private void postProcess(final Wire w, MutatorList muts, Collection<Node> nodes)  {
		assert isWellFormedRecursively();

		LinkedList<Node> ns = new LinkedList<Node>(nodes);
		// remove cruft
		while (!ns.isEmpty()) {
			Node n = ns.removeFirst();

			// we don't want no dangling splits
			if (n instanceof Split && n.incident.size() == 1) {
				Span s = n.incident.get(0).span;
				muts.add(removeEdgePure(s));
				removeUnconnectedNode(n, muts);
				ns.addLast(s.getOtherNode(n));
			}

			// collapse nodes if they become binary
			if (n instanceof Split && n.incident.size() == 2) {
				Node cycleNode = getCollapseCycleNode(n);
				if (cycleNode == null) { // safe to collapse
					muts.add(collapseNode((Split)n));
				} else {
					// kick out edges
					muts.add(removeEdgePure(n.incident.get(1).span));
					muts.add(removeEdgePure(n.incident.get(0).span));
					// and this node
					removeUnconnectedNode(n, muts);
					// and continue from the cycle node
					ns.addLast(cycleNode);
				}
			}
			// remove PinNodes if they become empty
			if ((n instanceof SubPinNode || n instanceof MainPinStub) && n.incident.isEmpty()) {
				removeUnconnectedNode(n, muts);
			}
		}

		assert isWellFormedRecursively();
		assert children.contains(w);

		// union-find remainders (nodes and spans)
		UnionFind<Object> uf = new UnionFind<Object> ();
		for (Node n : w.nodes.getChildren())   uf.add(n);
		for (Span s : w.spans.getChildren()) {
			uf.add(s);
			for (Edge e : s.edges) {
				uf.union(s, e.getNode1(), e.getNode2());
			}
		}


		if (uf.getEqvClasses().size() != 1) {
			// oh oh, multiple parts; create new wiring
			muts.add(remove(w));

			// create collection of new wires
//			final ArrayList<Wire> nws = new ArrayList<Wire> ();
			for (Collection<Object> eqvcl : uf.getEqvClasses()) {
				final Wire wi = new Wire(createNewName(w.getName()), w.getColor(), w.getStroke());
				muts.add(add(wi));
				Detangler d = new Detangler(eqvcl);
				for (Node n : d.nodes) muts.add(new MoveOver(w.nodes, wi.nodes, n).perform());
				for (Span s : d.spans) muts.add(new MoveOver(w.spans, wi.spans, s).perform());
			}
		}
		assert isWellFormedRecursively();
	}

	class MoveOver<T extends BasicEditorNode<?>> extends Mutator {
		final NodeList<?,T> from, to;
		final T node;
		final int fi, ti;

		public MoveOver(NodeList<?,T> from, NodeList<?,T> to, T node) {
			this.from = from;
			this.to = to;
			this.node = node;
			this.fi = from.getChildren().indexOf(node);
			this.ti = to.getChildren().size();
		}

		public void pre() {
			assert node.getParent() == from;
			assert from.getChildren().get(fi) == node;
			assert !to.getChildren().contains(node);
			assert to.getChildren().size() == ti;
		}

		public void commit() {
			from.getChildren().remove(fi);
			node.setParent(null);
			((BasicEditorNode)node).setParent(to);
			to.getChildren().add(ti, node);
		}

		public void post() {
			assert node.getParent() == to;
			assert !from.getChildren().contains(node);
			assert to.getChildren().get(ti) == node;
			assert to.getChildren().size() == ti+1;
		}

		public void rollback() {
			to.getChildren().remove(ti);
			node.setParent(null);
			((BasicEditorNode)node).setParent(from);
			from.getChildren().add(fi, node);
		}
	}

	private static class Detangler {
		final ArrayList<Node> nodes = new ArrayList<Node>();
		final ArrayList<Span> spans = new ArrayList<Span>();

		public Detangler(Collection objs) {
			for (Object o : objs) {
				if (o instanceof Node)       nodes.add((Node)o);
				else if (o instanceof Span)  spans.add((Span)o);
				else throw new Error("What? "+o.getClass());
			}
		}
	}


	private Mutator collapseNode(final Split s) {
		assert isWellFormedRecursively();
		assert s.incident.size() == 2;
		assert s.getParent() != null;

		final Wire w = s.getParent().getParent();
		assert w.nodes.getChildren().contains(s);
		Edge e1 = s.incident.get(0);
		Edge e2 = s.incident.get(1);
		final Span s1 = e1.span;
		final Span s2 = e2.span;

		assert w.spans.getChildren().contains(s1);
		assert w.spans.getChildren().contains(s2);
		assert s1.getOtherNode(s) != s2.getOtherNode(s) : "collapsing into cycle";

		final int i2 = w.spans.getChildren().indexOf(s2);


		// we need to consider these four situations
		// --> o -->
		// --> o <--
		// <-- o -->
		// <-- o <--
		// We reverse edge directions to always use the first case

		final boolean reverse1 = e1.n1 == s;
		final boolean reverse2 = e2.n2 == s;

		final int ne1 = s1.edges.size();

		return new Mutator() {
			public void pre() {
				assert isWellFormedRecursively();
				assert children.contains(w);
				assert w.spans.getChildren().get(i2) == s2;
			}

			public void commit() {

				if (reverse1) s1.reverse();
				if (reverse2) s2.reverse();

				// dump all edges of span 2 into span 1
				for (Edge e: s2.edges) {
					s1.edges.add(e);
					e.span = s1;
				}

				// then remove span 2
				w.spans.getChildren().remove(i2);
			}

			public void post() {
				assert isWellFormedRecursively();
				for (int i = s1.edges.size()-1; i >= ne1; --i) {
					assert s1.edges.get(i) == s2.edges.get(i - ne1);
				}
			}

			public void rollback() {
				for (int i = s1.edges.size()-1; i >= ne1; --i) {
					s1.edges.get(i).span = s2;
					s1.edges.remove(i);
				}

				w.spans.getChildren().add(i2, s2);

				if (reverse1) s1.reverse();
				if (reverse2) s2.reverse();
			}
		}.perform();
	}


	private Mutator liberate(final Node n) {
		final Span s = n.incident.get(0).span;
		final int li = Math.min(s.edges.indexOf(n.incident.get(0)),
								s.edges.indexOf(n.incident.get(1)));
		final Edge l = s.edges.get(li);
		final Edge r = s.edges.get(li+1);
		assert l.span == r.span;
		assert n.incident.contains(l);
		assert n.incident.contains(r);
		assert l.n2 == n;
		assert r.n1 == n;
		final Spans ss = s.getParent();

		// new span that we will put to the right
		final Span sr = new Span();

		return new Mutator() {
			public void pre() {
				assert n.incident.size() == 2;
				assert n.incident.contains(l) && n.incident.contains(r);
				assert l.n2 == n;
				assert r.n1 == n;
				assert l.span == s;
				assert r.span == s;
				assert !ss.getChildren().contains(sr);
				for (Edge e  : s.edges) assert e.span == s;
			}

			public void commit() {
				for (int i = s.edges.size()-1; i > li; --i) {
					assert s.edges.size() == i+1;
					Edge e = s.edges.remove(i);
					e.reverse();
					sr.edges.add(e);
					e.span = sr;
				}
				sr.setParent(ss);
				ss.getChildren().add(sr);
			}

			public void post() {
				assert n.incident.size() == 2;
				assert n.incident.contains(l) && n.incident.contains(r);
				assert s.getNode2() == n;
				assert sr.getNode2() == n;
				assert l.span == s;
				assert r.span == sr;

				for (Edge e  : s .edges) assert e.span == s;
				for (Edge e  : sr.edges) assert e.span == sr;
				assert ss.getChildren().get(ss.getChildren().size()-1) == sr;
			}

			public void rollback() {
				s.getParent().getChildren().remove(sr);
				sr.setParent(null);
				assert s.edges.size() == li+1;
				for (int i = sr.edges.size()-1; i >= 0; --i) {
					assert sr.edges.size() == i+1;
					Edge e = sr.edges.remove(i);
					e.reverse();
					s.edges.add(e);
					e.span = s;
				}
			}

		}.perform();

	}
}


