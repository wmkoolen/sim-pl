/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import databinding.BasicEditorNode;
import databinding.FixedList;
import databinding.IColored;
import databinding.IStroked;
import databinding.NameDeclaringNode;
import databinding.myStroke;
import databinding.wires.Span.EdgeDistance;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Wire extends FixedList<Wires, BasicEditorNode<?>> implements NameDeclaringNode, IColored, IStroked {
	String   name;
//	String   description;
	Color    c;
	myStroke stroke;

	// default graph representation. Set of nodes and set of edges
	final Nodes nodes = new Nodes();


	/** representation for drawing: set of spans. */
	final Spans spans = new Spans();


	private final List<BasicEditorNode<?>> children = Arrays.<BasicEditorNode<?>>asList(nodes, spans);
	public final List<BasicEditorNode<?>> getChildren() { return children; }

	public Wire() {}

	public Wire(String name, Color c, myStroke stroke) {
		nodes.setParent(this);
		spans.setParent(this);
		this.name = name;
//		this.description = description;
		this.c = c;
		this.stroke = stroke;
	}


	public String toString() { return name; }
	public Class getType() { return Wire.class; }


	public void setName(String name) { this.name = name; }
	public String getName() { return name; }

//	public void setDescription(String description) { this.description = description; }
//	public String getDescription() { return description; }

	public void setColor(Color c) { this.c = c; }
	public Color getColor() { return c; }

	public myStroke getStroke() { return stroke; }
	public void setStroke(myStroke stroke) { this.stroke = stroke; }

	public Spans getSpans() { return spans; }
	public Nodes getNodes() { return nodes; }



	public Span connect(final Node n1, final Node n2, List<Point2D> points, MutatorList muts) {
		assert n1 != n2;
		assert nodes.getChildren().contains(n1);
		assert nodes.getChildren().contains(n2);

		// ensure that n1 is not in the middle of a span
		assert
			n1.incident.size() != 2 ||
			n1.incident.get(0).span != n1.incident.get(1).span;

	    assert
			n2.incident.size() != 2 ||
			n2.incident.get(0).span != n2.incident.get(1).span;

		Node [] ns = new Node[points.size()+2];
		ns[0] = n1;
		ns[ns.length-1] = n2;
		for (int i = 0; i < points.size(); i++) {
			Split s = new Split(points.get(i));
			ns[i+1] = s;
			muts.add(nodes.add(s));
		}

		final Span s = new Span();
		final Edge [] es = new Edge[ns.length-1];
		for (int i = 0; i < es.length; i++) {
			Edge e = new Edge(ns[i], ns[i+1]);
			es[i] = e;
			s.edges.add(e);
			e.span = s;
			if (i > 0)           ns[i  ].incident.add(e);
			if (i+1 < es.length) ns[i+1].incident.add(e);
		}
		muts.add(spans.add(s));

		final Edge ef = es[0], el = es[es.length-1];

		muts.add(new Mutator() {
			public void pre(){
				assert !n1.incident.contains(ef);
				assert !n2.incident.contains(el);
			}
			public void commit() {
				n1.incident.add(ef);
				n2.incident.add(el);
			}
			public void post() {
				assert n1.getParent() == nodes && n1.isWellFormedRecursively();
				assert n2.getParent() == nodes && n2.isWellFormedRecursively();
				assert s .getParent() == spans && s. isWellFormedRecursively();
				assert n1.incident.contains(ef);
				assert n2.incident.contains(el);
			}

			public void rollback() {
				n1.incident.remove(ef);
				n2.incident.remove(el);
			}
		}.perform());

		return s;
	}


//	public Mutator add(final Node n) {
//		MutatorList muts = new MutatorList();
//		muts.add(nodes.add(n));
//		assert nodes.getChildren().contains(n);
//		muts.add(new Mutator() {
//			public void redo() {
//				assert !parent.node2wire.containsKey(n);
//				parent.node2wire.put(n, Wire.this);
//			}
//
//			public void undo() {
//				assert parent.node2wire.get(n) == Wire.this;
//				parent.node2wire.remove(n);
//			}
//		}.perform());
//		return muts;
//	}

	public Mutator remove(final Node n) {
		// check whether this wire is split in two

		throw new Error("Implement");
	}


	public boolean isWellFormed() {
		assert super.isWellFormed();

		// cross check references
		for (Node n : nodes.getChildren()) {
			for (Edge e : n.incident) {
				Span s = e.span;
				assert s.edges.contains(e);
				assert s.getParent().getChildren().contains(s);
			}
		}

		for (Span s : spans.getChildren()) {
			for (Edge e : s.edges) {
				assert nodes.getChildren().contains(e.getNode1());
				assert nodes.getChildren().contains(e.getNode2());
			}
		}

		return true;
	}

	public boolean isValid() {
		// we need nodes to exist
		assert !nodes.getChildren().isEmpty() : "wire " + this + " has no nodes licensing existence";
		// we need spans to exist
		assert !spans.getChildren().isEmpty() : "wire " + this + " has no spans licensing existence";
		return true;
	}



	// to XML method
	public NodeSequencer getNS() {
		NodeSequencer nsr = new NodeSequencer();
		for (Span s : spans.getChildren()) {
			NodeSequence ns = new NodeSequence();
			ns.nodes.add(new SpanNode(s.edges.get(0).getNode1()));
			for (Edge e : s.edges) {
				ns.nodes.add(new SpanNode(e.getNode2()));
			}
			nsr.nss.add(ns);
		}
		return nsr;
	}

	// from XML method
	public void setNS(NodeSequencer nsr) {
		for (NodeSequence ns : nsr.nss) {
			Span s = new Span();
			Edge [] es = new Edge[ns.nodes.size()-1];
			for (int i = 0; i < es.length; i++) {
				Edge e = new Edge(ns.nodes.get(i).node, ns.nodes.get(i+1).node);
				e.getNode1().incident.add(e);
				e.getNode2().incident.add(e);
				es[i] = e;
				e.span = s;
				s.edges.add(e);
			}
			spans.getChildren().add(s);
		}
	}

	public Edge getClosestEdge(Point2D p) {
		EdgeDistance ed = new EdgeDistance();
		for (Span s : spans.getChildren()) {
			s.updateClosestEdge(ed, p);
		}
		return ed.e;
	}

	public static class NodeSequencer {
		public ArrayList<NodeSequence> nss = new ArrayList<NodeSequence>();
	}

	public static class NodeSequence {
		public ArrayList<SpanNode> nodes = new ArrayList<SpanNode>();
	}

	public static class SpanNode {
		public Node node;
		public SpanNode() {}
		public SpanNode(Node node) { this.node = node; }
	}



}



