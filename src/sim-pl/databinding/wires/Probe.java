/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;
import java.util.Set;

import util.GUIHelper.Alignment;
import databinding.BasicEditorNode;
import databinding.EditorNode;
import databinding.IAligned;
import databinding.IMoveable;
import databinding.IPositioned;
import databinding.forms.HAlignment;
import databinding.forms.VAlignment;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Probe extends EditorNode<Node> implements IAligned, IMoveable, IPositioned {


	// provide sane defaults, so that we can omit values in XML
	HAlignment halign = HAlignment.center;
	VAlignment valign = VAlignment.center;


	// offset from parent point
	Point2D.Double pos = new Point2D.Double();

	public Probe() {} // from XML constructor

	public Probe(Point2D pos) {
		this(pos, HAlignment.center, VAlignment.center);
	}

	public Probe(Point2D pos, Alignment align) {
		this(pos, align.halign, align.valign);
	}

	public Probe(Point2D pos, HAlignment halign, VAlignment valign) {
		this.pos.setLocation(pos);
		this.valign = valign;
		this.halign = halign;
	}

	public String toString() { return "Probe"; }
	
	
	private boolean isFakeSplit() {
		return pos.x == 0 && pos.y == 0;
	}
	
	
	public void collectInvolvedInMove(Set<BasicEditorNode> selection, Set<IMoveable> movers) {
		// filter out cases where we do not move.
		if (selection.contains(parent)) return;
		if (!parent.isSpanEnd() && // we're a split
			selection.contains(parent.getEdges().get(0).getSpan())) return;

		if (parent instanceof SubPinNode &&
			selection.contains(((SubPinNode)parent).getSubComponent())) return;

		if (parent instanceof MainPinStub &&
			selection.contains(((MainPinStub)parent).getPin())) return;
		
		// prevent user from unintentionally moving probe off of wire
		if (isFakeSplit()) {
			getParent().collectInvolvedInMove(selection, movers);
		} else {
			movers.add(this);
		}
	}
	
	public void collectAffected(Set<BasicEditorNode> nodes) {
		nodes.add(this);	
		if (isFakeSplit()) nodes.add(getParent());
	}
	

	public void move(int dx, int dy) {
		// drag probe pure mode
		pos.x += dx;
		pos.y += dy;
	}

	public double getX() { return pos.x; }
	public void setX(double x) { pos.x = x; }
	public double getY() { return pos.y; }
	public void setY(double y) { pos.y = y; }
	public Point2D getPos() { return pos; }

	public Point2D getTruePos() {
		Point2D.Double tp = new Point2D.Double();
		tp.setLocation(parent.getPos());
		tp.x += pos.x;
		tp.y += pos.y;
		return tp;
	}
	public HAlignment getHalign() { return halign; }
	public void setHalign(HAlignment h_align) { this.halign = h_align; }

	public VAlignment getValign() { return valign; }
	public void setValign(VAlignment v_align) { this.valign = v_align; }



	
	
	
}
