/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;

import databinding.SubComponent;
import databinding.SubPinStub;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


/** Adapts SubPinStub into a Node (for graphs) */

public class SubPinNode extends Node {
	SubPinStub sps;


	String subref; // used during XML construction
	String pinref; // used during XML construction

	public SubPinNode() { }

	public SubPinNode(SubPinStub sps) {
		this.sps = sps;
	}

	public boolean isResolved() {
		return  sps.isResolved();
	}

	public boolean isSpanEnd() { return true; }

	public boolean isWellFormed() {
		assert super.isWellFormed();

		assert pinref == null && subref == null;

		assert sps != null;
		assert sps.getNode() == this;


		assert sps.getSubComponent().getSubPinStubs().getChildren().contains(sps);
		assert sps.getSubComponent().getParent() != null : sps.getSubComponent() + " not in graph";
		assert sps.getSubComponent().getParent().getChildren().contains(sps.getSubComponent());

		return true;
	}


	// to XML method
	public String getSubRef() {  return sps == null ? subref : sps.getSubComponent().getName(); }
	public String getPinRef() {  return sps == null ? pinref : sps.getPinRef(); }

	// from XML method
	public void setSubRef(String subref) {
		assert sps == null; // only used during XML construction
		this.subref = subref;
	}

	public void setPinRef(String pinref) {
		assert sps == null; // only used during XML construction
		this.pinref = pinref;
	}

	public String toString() { return getSubRef() + ":" + getPinRef(); }

	public Point2D getPos() { return sps.getExternalPos(); }


	public void wire(Nodes parent) {
		super.wire(parent);
		assert sps == null;
		assert subref != null && pinref != null;

		SubComponent sc = parent.getParent().getParent().getParent().getSubcomponents().getByName(subref);
		assert sc != null;

		for (SubPinStub sps : sc.getSubPinStubs().getChildren()) {
			if (sps.getPinRef().equals(pinref)) {
				this.sps = sps;
				break;
			}
		}
		assert sps != null : "pin " + subref + ":" + pinref + " not found";
		assert sps.getNode() == null : subref + ":" + pinref + " already wired";
		sps.setNode(this);

		subref = null;
		pinref = null;
	}

	public SubComponent getSubComponent() {
		return sps.getSubComponent();
	}

	public SubPinStub getStub() {
		return sps;
	}
}

