/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;

import databinding.Pin;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */


public class MainPinStub extends Node {
	Pin p;

	public MainPinStub() { }

	public MainPinStub(Pin p) {
		assert p != null;
		this.p = p;
	}

	public boolean isSpanEnd() { return true; }

	public String toString() { return p.getName(); }

	// from/to XML methods. We use XML id/reference to find the pin
	public Pin getPin() { return p; }
	public void setPin(Pin p) { assert this.p == null; this.p = p; }

	public Point2D getPos() {
		return p.getPos();
	}

	public void wire(Nodes parent) {
		super.wire(parent);
		assert p.getNode() == null : "two stubs for " + p;
		p.setNode(this);
	}

	public boolean isWellFormed() {
		assert super.isWellFormed();
		assert p != null;
		assert p.getNode() == this;
		assert p.getParent().getChildren().contains(p);
		return true;
	}
}

