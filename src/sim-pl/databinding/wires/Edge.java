/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

public class Edge  {
	Node n1, n2;
	Span span;

	public Edge(Node n1, Node n2) {
		assert n1 != n2;
		this.n1 = n1;
		this.n2 = n2;
	}


	public Node getNode1() { return n1; }
	public void setNode1(Node n1) { assert n1 != n2; this.n1 = n1; }
	public Node getNode2() { return n2; }
	public void setNode2(Node n2) { assert n1 != n2; this.n2 = n2; }

	public Span getSpan() {
		return span;
	}

	public double dist(Point2D p) {
		Point2D l1 = n1.getPos(), l2 = n2.getPos();
		return Line2D.ptSegDist(l1.getX(), l1.getY(), l2.getX(), l2.getY(), p.getX(), p.getY());
	}

	public void reverse() {
		Node nt = n1;
		n1 = n2;
		n2 = nt;
	}
}

