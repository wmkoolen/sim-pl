/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.NodeList;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */



public abstract class Node extends NodeList<Nodes, Probe>  {

	final ArrayList<Edge> incident = new ArrayList<Edge>();

	public static class IDs {
		private static final  Random r = new Random();
		public  static String getNewID() { return Long.toHexString(r.nextLong()); }
	}

	String _ref = IDs.getNewID();
	public String getRef() { return _ref; }
	public void setRef(String ref) { this._ref = ref; }

	public abstract Point2D getPos();


	public boolean isWellFormed() {
		assert super.isWellFormed();

		for (Edge e : incident) {
			assert e.getNode1() == this || e.getNode2() == this;
		}

		return true;
	}

	public boolean isSpanEnd() { return incident.size() != 2; }


	public boolean isValid() {
		assert super.isValid();
		assert !incident.isEmpty() : "node " + this + " has no edges licensing existence";

		// collect all spans incident to n
		Set<Span> ss = new HashSet<Span> ();
		for (Edge e : incident) ss.add(e.span);

		// if n is not a span-end, all connected spans must be the same
		assert isSpanEnd() || ss.size() == 1;

		// and if we're a span end, then all connected spans must be different
		assert!isSpanEnd() || ss.size() == incident.size();
		return true;
	}

	public ArrayList<Edge> getEdges() {
		return incident;
	}
	
	public void collectAffected(Set<BasicEditorNode> nodes) {
		for (Edge e : incident) nodes.add(e.getSpan());
		nodes.addAll(getChildren()); // probes move with us
	}
	
}


