/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.ComponentNamespace.PinInfo;
import namespace.Namespace;
import namespace.ParameterContext;
import util.MyFormatter;
import util.UnionFind;
import databinding.cablegraph.CG_legacy;
import databinding.cablegraph.CG_legacy.CGEdge;
import databinding.cablegraph.CG_legacy.CGNode;
import databinding.cablegraph.CG_legacy.CGProbe;
import databinding.compiled.Compiled;
import databinding.forms.PPoint;
import databinding.wires.Edge;
import databinding.wires.MainPinStub;
import databinding.wires.Node;
import databinding.wires.Probe;
import databinding.wires.Span;
import databinding.wires.SubPinNode;
import databinding.wires.Wire;
import databinding.wires.Wires;


/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Complex extends Component {
	SubComponents subcomponents = new SubComponents();
//	Cables cables = new Cables();
//	CableGraph cg = new CableGraph();
	Wires wires = new Wires();

	public CG_legacy cableGraph; // used in XML construction of legagacy components

	{
		children.add(subcomponents);
		children.add(wires);
	}

	public Complex() {}

	public Complex(String name) {
		super(name);
		subcomponents.setParent(this);
		wires.setParent(this);
	}

	public SubComponents getSubcomponents() { return subcomponents; }
//	public Cables getCables() { return cables; }
//	public CableGraph getCableGraph() { return cg; }
	public Wires getWires() { return wires; }


	public void collectDefs(ComponentNamespace cns) {
		super.collectDefs(cns);
		subcomponents.collectDefs(cns.sub);
	}

//	public void dispatch(ComponentCallback callback) { callback.callback(this); }

	public Compiled.CompiledComplex compile(ParameterContext<String, Integer> pc, Set<String> wiredPins, ComponentNamespace cns, CompilationMessageBuffer cmb) {
		Compiled.CompiledComplex tt = new Compiled.CompiledComplex(this);
		super.compile(tt, pc, wiredPins, cns, cmb);

		Map<String, Set<String>> sub2wired = new HashMap<String,Set<String>>();

		for (Wire w : wires.getChildren())
		for (Node n : w.getNodes().getChildren()) {
			if (n instanceof SubPinNode) {
				SubPinNode pn = (SubPinNode)n;
				try {
					Set<String> wired = sub2wired.get(pn.getSubComponent().getName());
					if (wired == null) {
						wired = new HashSet<String> ();
						sub2wired.put(pn.getSubComponent().getName(), wired);
					}
					wired.add(pn.getPinRef());
				} catch (Exception ex) {} // ignore here, we flag it below
			}
		}


		// compile subcomponents (applying parameters along the way)
		for (Map.Entry<String, Namespace.RefDef<SubComponent, BasicEditorNode>> pe : cns.sub.entrySet())  {
			ArrayList<SubComponent> pdef = pe.getValue().def;
			assert!pdef.isEmpty();
			// only import successfully defined SubComponents
			if (pdef.size() == 1) {
				SubComponent p = pdef.get(0);
				assert subcomponents.getChildren().contains(p);

				// check name
				if (p.getName().contains(".") || p.getName().contains(":")) {
					cmb.add(p, "'.' and ':' not allowed in name: " + p.getName(), true);
				}

				// resolve all parameters simultaneously in the outer scope
				Namespace<String, ParameterVal, Object> ns = new Namespace<String,ParameterVal,Object>();
				Map<ParameterVal, Integer> pv2i = new IdentityHashMap<ParameterVal,Integer>();
				for (ParameterVal pv : p.getParameterVals().getChildren()) {
					ns.addDefiner(pv.getName(), pv);
					Integer i = pv.getValue().getValue(pc);
					pv2i.put(pv, i);
					if (i == null) {
						cmb.add(pv, "unable to determine value of " + pv.getValue(), false);
					}
				}

				// then enter new scope to compile subcomponent
				pc.enterScope(this);
				try {
					// enter all parameter-vals
					for (Map.Entry<String, Namespace.RefDef<ParameterVal, Object>> pve : ns.entrySet())  {
						ArrayList<ParameterVal> pvdef = pve.getValue().def;
						assert!pvdef.isEmpty();
						// only import successfully defined parameter-vals
						if (pvdef.size() == 1) {
							ParameterVal pv = pvdef.get(0);
							Integer i = pv2i.get(pv);
							pc.put(pve.getKey(), i);
						} else {
							for (ParameterVal pv : pvdef) {
								cmb.add(pv, pve.getKey() + " multiply defined");
							}
						}
					}
					// compile subcomponent
					if (p.isResolved())
					try {
						CompilationMessageBuffer sub_cmb = new CompilationMessageBuffer();
						ComponentNamespace sub_cns = cns.sub2cns.get(p);
						if (sub_cns == null) {
							sub_cns = new ComponentNamespace(p.getSubComponent());
							cns.sub2cns.put(p, sub_cns);
						}

						Set<String> subwired = sub2wired.get(p.getName());
						if (subwired == null) subwired = Collections.emptySet();

						Compiled.CompiledComponent subtt = sub_cns.compile(pc, subwired, sub_cmb);
//						tt.addKid(p.getName(), subtt);
//						ComponentNamespace sub_cns = p.getSubComponent().compile(pc, sub_cmb);
						if (sub_cmb.getErrorCount() > 0) throw new Exception("subcomponent " + p.getName() + " has errors");
						if (sub_cmb.getWarningCount() > 0) {
							cmb.add(p, "subcomponent has warnings", false);
						}
						tt.sub2csub.put(p, subtt);
//						cns.sub2cns.put(p, sub_cns);
					} catch (Exception ex) {
						ex.printStackTrace();
						cmb.add(p, ex.getMessage(), true);
					}
				} finally {
					pc.exitScope(this);
				}

			} else {
				for (SubComponent p : pdef) {
					cmb.add(p, pe.getKey() + " multiply defined");
				}
			}
		}


		Set<Pin> unconnectedPins = new HashSet<Pin> ();
		for (Pin p : io.getPins()) {
			unconnectedPins.add(p);
		}

		// resolve cables
		UnionFind<Node> f = new UnionFind<Node>();

		for (Wire w : wires.getChildren()) {
			for (Node n : w.getNodes().getChildren()) {
				f.add(n);
				if (n instanceof MainPinStub) {
					MainPinStub pn = (MainPinStub)n;
					try {
						Pin p = cns.pin.getUniqueDef(pn.getPin().getName());
						unconnectedPins.remove(p);
					} catch (Exception ex) {
						cmb.add(pn.getPin(), ex);
					}
				}
			}
			for (Span s : w.getSpans().getChildren())
			for (Edge e : s.edges) {
				f.union(e.getNode1(), e.getNode2());
			}
		}

		for (Pin uo : unconnectedPins) {
			cmb.add(uo, "unconnected pin", false);
		}



		for (Collection<Node> eqvcl : f.getEqvClasses()) {
			// extract pinref nodes
			ArrayList<Node> pins      = new ArrayList<Node>();
			ArrayList<Node> outputs   = new ArrayList<Node>();
			Set<Integer>   bitWidths  = new HashSet<Integer>();

			for (Node n : eqvcl) {
				if (n instanceof SubPinNode) {
					SubPinNode pn = (SubPinNode)n;
					try {
						PinInfo pi = cns.getPinInfo(pn.getStub());
						pins.add(pn);
						bitWidths.add(pi.bitWidth);
						if (pi.isSender) outputs.add(pn);
					} catch (Exception ex) {
						cmb.add(pn, ex);
					}
				}
				if (n instanceof MainPinStub) {
					MainPinStub pn = (MainPinStub)n;
					try {
						PinInfo pi = cns.getPinInfo(pn);
						pins.add(pn);
						bitWidths.add(pi.bitWidth);
						if (pi.isSender) outputs.add(pn);
					} catch (Exception ex) {
						cmb.add(pn, ex);
					}
				}
			}

			if (bitWidths.size() > 1) {
				for (Node pn : pins) {
					cmb.add(pn, "Conflicting bit widths: " + MyFormatter.join(", ", bitWidths), true);
				}
			}

			if (outputs.size() > 1) {
				for (Node on : outputs) {
					cmb.add(on, "Multiple outputs on wire", false);
				}
			}

			if (eqvcl.size() == 1) {
				cmb.add(eqvcl.iterator().next(), "Dangling node", false);
			}
		}

		// properly connect senders to receivers
		// at most one cable per receiving pin
		// matching bit widths

		/**
		for (Cable c : cables.children) {
			try {
				String source_pinref = c.getSource();
				cns.pinref2cable.addDefiner(source_pinref, c);
				ComponentNamespace.PinInfo sourcePinInfo = cns.getPinInfo(source_pinref);

				if (!sourcePinInfo.isSender) {
					throw new Exception(source_pinref + ": invalid cable source");
				}

				ArrayList<PinTarget> result = new ArrayList<PinTarget>();
				c.collectTargetPins(result);

				if (result.isEmpty()) {
					throw new Exception("Cable has no target");
				}

				for (PinTarget t : result) {
					try {
						String target_pinref = t.getPin();
						cns.pinref2cable.addDefiner(target_pinref, c);
						ComponentNamespace.PinInfo targetPinInfo = cns.getPinInfo(target_pinref);

						if (targetPinInfo.isSender) {
							cmb.add(t, target_pinref + ": invalid cable target");
						}

						if (targetPinInfo.isCabled) {
							cmb.add(t, target_pinref + ": already cabled");
						}

						if (sourcePinInfo.bitWidth != targetPinInfo.bitWidth) {
							cmb.add(t, "source bit width (" + sourcePinInfo.bitWidth +") does not match target bit width (" + targetPinInfo.bitWidth + ")");
						}
					} catch (Exception ex) {
						cmb.add(t, ex);
					}
				}
			} catch (Exception ex) {
//				ex.printStackTrace();
				cmb.add(c, ex);
			}
		*/
		// TODO: compile cable graph
		return tt;
	}




//	public RenderNode createRenderNode() {
//		final RenderNode rforms = forms.createRenderNode();
//		final RenderNode rio = io.createRenderNode();
//		final RenderNode rsubs = subcomponents.createRenderNode();
//		final RenderNode rcables = cables.createRenderNode();
//
//		return new RenderNode() {
//			public void render(Graphics2D g) {
//				// 'PCB with ICs' order
//				rforms.render(g);
//				rcables.render(g);
//				rio.render(g);
//				rsubs.render(g);
//			}
//		};
//	}


//	public Point2D getPinPos(int subi, String [] subs, String name) throws Exception {
//		if (subi == subs.length) {
//			return super.getPinPos(subi, subs, name);
//		}
//
//		for (SubComponent s : subcomponents.getChildren()) {
//			if (s.name.equals(subs[subi])) {
//				try {
//					Component c = s.getSubComponent();
//					Point2D p = c.getPinPos(subi+1, subs, name);
//					AffineTransform t = s.getTransform();
//					return t.transform(p, null); // create a new point
//
//				} catch (Exception ex) {
//					throw new Exception("Error in subcomponent " + s.getName(), ex);
//				}
//			}
//		}
//		throw new Exception("subcomponent not found: " + subs[subi]);
//	}

	/**
	 * rewire: refreshes all subcomponent references, to point to the
	 * current version of the subcomponent in the ComponentTable
	 */
	public void rewire() {
		for (SubComponent sc : subcomponents.getChildren()) sc.rewire();
	}



	public void wire(BasicEditorNode parent) {
		super.wire(parent);
		if (cableGraph != null) {
			MutatorList muts = new MutatorList();

			Map<CGNode, Node> n2n = new HashMap<CGNode,Node>();
			// add edges
			for (CGNode n : cableGraph.nodes) {
				try { n2n.put(n, n.realise(this)); }
				catch (Exception ex) { ex.printStackTrace(); } // just ingore
			}

			// add wires
			for (CGEdge e : cableGraph.edges) {
				ArrayList<Point2D> pts = new ArrayList<Point2D>();
				if (e.pPoints != null)
					for (PPoint pp : e.pPoints)
						pts.add(new Point2D.Double(pp.getX(), pp.getY()));
				Node n1 = n2n.get(e.node1),
					 n2 = n2n.get(e.node2);
				// legacy components can have n1 == n2. This was never a
				// correct statement, but it more or less worked. In SIM-PL v2
				// this is explicitly disallowed. In this legacy component
				// translation we just ignore such self-loops
				if (n1 != null && n2 != null && n1 != n2) {
					Span s = wires.connect(n1, n2, pts, muts);
					Wire w = s.getParent().getParent();
					w.setColor(e.color);
					w.setStroke(e.stroke);
				}
			}

//			// add probes
			for (CGProbe p : cableGraph.probes) {
				Wire w = n2n.get(p.node).getEdges().get(0).getSpan().getParent().getParent();
				Edge e = w.getClosestEdge(p.pos);
				Node n = getWires().splitEdge(e, p.pos, muts);
				n.add(new Probe(new Point2D.Double(0,0), p.halign, p.valign));
			}

			cableGraph = null;
		}
		assert cableGraph == null;
	}
}
