/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import namespace.ComponentNamespace;
import namespace.Namespace;
import namespace.ParameterContext;
import nbit.NumberFormat;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.forms.Forms;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

 /* Components are NOT name-declaring */
public abstract class Component extends NodeList<BasicEditorNode,BasicEditorNode<?>>  {
	protected String name;
	protected Forms forms = new Forms();
	protected IO io = new IO();
	protected Parameters params = new Parameters();

	protected int preferredCycleLength = 10;
	protected NumberFormat preferredNumberFormat = NumberFormat.HEXADECIMAL;

	{
		children.add(forms);
		children.add(params);
		children.add(io);
	}

	public Component() {} // XML construction

	public Component(String name) {
		forms.setParent(this);
		params.setParent(this);
		io.setParent(this);
		this.name = name;
	}


	public boolean canBeDetached() { return false; }

	public String toString() {
		return name;
	}


	public String getName() { return name; }
	public void setName(String name) {this.name = name; }

	public Parameters getParameters() { return params; }
	public Forms getForms() { return forms; }
	public IO getIo() { return io; }


	public  int getPreferredCycleLength() { return preferredCycleLength; }
	public  void setPreferredCycleLength(int preferredCycleLength) { this.preferredCycleLength = preferredCycleLength; }

	public NumberFormat getPreferredNumberFormat() { return preferredNumberFormat; }
	public void setPreferredNumberFormat(NumberFormat preferredNumberFormat) { this.preferredNumberFormat = preferredNumberFormat; }

//	public Iterator<? extends EditorNode> getChildren() { return new Tuple<EditorNode>(forms, io).iterator(); }

//	public CompiledNode compile(CompilationMessageBuffer cmb) {
//		cmb.add(new CompilationMessage(this, "Not implemented", true));
//		return null;
//	}

//	public Point2D getPinPos(String pinref) throws Exception {
//		String [] ab = pinref.split(":");
//		String [] subs;
//		String name;
//		if (ab.length == 1) {
//			subs = new String[]{};
//			name = ab[0];
//		} else if (ab.length == 2) {
//			name = ab[1];
//			subs = ab[0].split("\\.");
//		} else throw new Error("Invalid pin reference: " + pinref);
//
//		return getPinPos(0, subs, name);
//	}

//	public Point2D getPinPos(int subi, String [] subs, String name) throws Exception {
//		assert subi == subs.length;
//		for (Pin p : io.getChildren()) {
//			if (p.name.equals(name)) return p.getPos();
//		}
//		throw new Exception("pin not found: " + name + " in " + this.name);
//	}



	public void collectDefs(ComponentNamespace cns) {
		io.collectDefs(cns.pin);
		params.collectDefs(cns.par);
	}

	public abstract CompiledComponent compile(ParameterContext<String, Integer> pc, Set<String> wiredPins, ComponentNamespace cns, CompilationMessageBuffer cmb);

	protected void compile(CompiledComponent tt, ParameterContext<String, Integer> pc, Set<String> wiredPins, ComponentNamespace cns, CompilationMessageBuffer cmb) {
		// check name
		if (name.contains(".") || name.contains(":")) {
			cmb.add(this, "'.' and ':' not allowed in name: " + name, true);
		}

		if (preferredCycleLength <= 0) {
			cmb.add(this, "Impossible preferred cycle length: " + preferredCycleLength, true);
		}

		// evaluate parameters values simultaneously in outer scope
		Map<ParameterDecl, Integer> pv2i = new IdentityHashMap<ParameterDecl,Integer>();
		for (ParameterDecl p : params.children) {
			pv2i.put(p, p.getDefault().getValue(pc));
		}

		// enter parameter definitions into scope
		for (Map.Entry<String, Namespace.RefDef<ParameterDecl, BasicEditorNode>> pe : cns.par.entrySet())  {
			ArrayList<ParameterDecl> pdef = pe.getValue().def;
			assert !pdef.isEmpty();
			// only import successfully compiled parameters
			if (pdef.size() == 1) {
				ParameterDecl p = pdef.get(0);
				assert params.getChildren().contains(p);

				// check name
				if (p.getName().contains(".") || p.getName().contains(":")) {
					cmb.add(p, "'.' and ':' not allowed in name: " + p.getName(), true);
				}

				// note, the parameters here are only defaults
				// we only use them when they aren't set already
				if (!pc.containsKey(p.getName())) {
					Integer val = pv2i.get(p);
					if (val != null) {
						pc.put(p.getName(), val);
					} else {
						cmb.add(p, "unable to resolve " + p.getDefault(), false);
					}
				}
			} else {
				for (ParameterDecl p : pdef) {
					cmb.add(p, pe.getKey() + " multiply defined", true);
				}
			}
		}



		// compile io pins (resolving parameters along the way)
		for (Map.Entry<String, Namespace.RefDef<Pin, BasicEditorNode>> pe : cns.pin.entrySet())  {
			ArrayList<Pin> pdef = pe.getValue().def;
			assert!pdef.isEmpty();
			// only import successfully compiled pins
			if (pdef.size() == 1) {
				Pin p = pdef.get(0);
				assert io.getChildren().contains(p);

				// check name
				if (p.getName().contains(".") || p.getName().contains(":")) {
					cmb.add(p, "'.' and ':' not allowed in name: " + p.getName(), true);
				}


				Integer width = p.bits.getValue(pc);
				if (width != null) {
					cns.pin2bits.put(p, width);
					tt.addCompiledPin(p, width, wiredPins.contains(p.getName()));
				} else {
					cmb.add(p, "unable to determine bit width from " + p.bits, true);
				}
			} else {
				for (Pin p : pdef) {
					cmb.add(p, pe.getKey() + " multiply defined", true);
				}
			}
		}
	}

//	public abstract void dispatch(ComponentCallback componentCallback);

	//	final public ComponentNamespace compile(ParameterContext<String, Integer> pc, CompilationMessageBuffer cmb) {
//
//		System.out.println("Compilation triggered for: " + this);
//
//		// enter new scope
//		pc.enterScope(this);
//
//		// run a round of definitions (to check  for duplicates)
//		ComponentNamespace cns = new ComponentNamespace(this);
//		collectDefs(cns);
//
//		compile(pc, cns, cmb);
//
//		pc.exitScope(this);
//
//		System.out.println("Compilation result: " + cns);
//
//		return cns;
//	}
}
