/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;

import java.awt.geom.AffineTransform;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class Flip {

	public static final Flip none =       new Flip("NONE",       new AffineTransform( 1,0,0, 1,0,0));
	public static final Flip vertical =   new Flip("VERTICAL",   new AffineTransform( 1,0,0,-1,0,0));
	public static final Flip horizontal = new Flip("HORIZONTAL", new AffineTransform(-1,0,0, 1,0,0));
	public static final Flip diagonal =   new Flip("DIAGONAL",   new AffineTransform(-1,0,0,-1,0,0));


	public static final Flip[] flips = {none, vertical, horizontal, diagonal};

	private String flipString;
	private AffineTransform t;

	private Flip(String flipString, AffineTransform t) {
		this.flipString = flipString;
		this.t = t;
	}

	public static Flip valueOf(String flip_str) throws Exception {
		return parseFlip(flip_str);
	}

	public static Flip parseFlip(String flip_str) throws
		Exception {
		for (int i = 0; i < flips.length; i++) {
			if (flip_str.equals(flips[i].flipString)) {
				return flips[i];
			}
		}
		throw new Exception("Invalid flip: " + flip_str);
	}

	public AffineTransform getTransform() {
		return t;
	}

	public String toString() {
		return flipString;
	}
}
