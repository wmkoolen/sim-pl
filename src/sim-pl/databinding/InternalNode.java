/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;


import java.util.ArrayList;
import java.util.List;



/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public interface InternalNode<P extends BasicEditorNode, T extends BasicEditorNode<? extends BasicEditorNode>> extends BasicEditorNode<P> {
	public void addSubtreeListener   (SubTreeListener l);
	public void removeSubtreeListener(SubTreeListener l);
	public List<T> getChildren();


	static abstract class Mutator  {
		public final void redo() { pre();  commit();   post(); }
		public final void undo() { post(); rollback(); pre();  }

		protected abstract void commit();
		protected abstract void rollback();
		protected abstract void pre();
		protected abstract void post();

		// convenience for returning
		public Mutator perform() {
			redo();
			return this;
		}
	};


	static class MutatorList extends Mutator {
		List<Mutator> muts = new ArrayList<Mutator>();
		public void pre() {};
		public void post() {};

		public void commit() {
			// forward order in redo
			for (Mutator m : muts) m.redo();
		}

		public void rollback() {
			// reverse order in undo
			for (int i = muts.size()-1; i >= 0; --i) muts.get(i).undo();
		}

		public void add(databinding.InternalNode.Mutator mutator) {
			muts.add(mutator);
		}
	}

	/** All methods that return a Mutator should return in a state equivalent to
	 * having called commit on the Mutator*/
	public Mutator add(T n);
	public Mutator remove(T n);
}
