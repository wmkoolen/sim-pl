/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding;


import java.awt.Font;
import java.util.Arrays;

import org.exolab.castor.mapping.GeneralizedFieldHandler;


/**
 * The FieldHandler for the Font class
 *
 */
public class FontConverter extends GeneralizedFieldHandler
{
	/**
	 * Creates a new MyDateHandler instance
	 */
	public FontConverter() {
		super();
	}

	/**
	 * This method is used to convert the value when the
	 * getValue method is called. The getValue method will
	 * obtain the actual field value from given 'parent' object.
	 * This convert method is then invoked with the field's
	 * value. The value returned from this method will be
	 * the actual value returned by getValue method.
	 *
	 * @param value the object value to convert after
	 *  performing a get operation
	 * @return the converted value.
	 */
	public final static String [] styles = {"plain", "bold", "italic", "bold+italic"};

	public Object convertUponGet(Object value) {
		if (value == null) return null;

		Font f = (Font)value;
		return f.getFamily() + "," + styles[f.getStyle()] + "," + f.getSize2D();
	}


	/**
	 * This method is used to convert the value when the
	 * setValue method is called. The setValue method will
	 * call this method to obtain the converted value.
	 * The converted value will then be used as the value to
	 * set for the field.
	 *
	 * @param value the object value to convert before
	 *  performing a set operation
	 * @return the converted value.
	 */
	public Object convertUponSet(Object value)  {
		try {
			String [] parts = ((String)value).split(",");
			if (parts.length != 3) return null;

			int style = Arrays.asList(styles).indexOf(parts[1]);
			if (style == -1) return null;

			return new Font(
						 parts[0],
						 style,
						 1).deriveFont(Float.valueOf(parts[2]));
		} catch (NumberFormatException ex) {
			return null;
		}
	}

	/**
	 * Returns the class type for the field that this
	 * GeneralizedFieldHandler converts to and from. This
	 * should be the type that is used in the
	 * object model.
	 *
	 * @return the class type of of the field
	 */
	public Class getFieldType() {
		return Font.class;
	}



	/**
	 * Creates a new instance of the object described by
	 * this field.
	 *
	 * @param parent The object for which the field is created
	 * @return A new instance of the field's value
	 * @throws IllegalStateException This field is a simple
	 *  type and cannot be instantiated
	 */
	public Object newInstance( Object parent )
		throws IllegalStateException
	{
		//-- Since it's marked as a string...just return null,
		//-- it's not needed.
		return null;
	}

}

