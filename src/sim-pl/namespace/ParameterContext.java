/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package namespace;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class ParameterContext<tN, tV>  {

	public static final boolean DEBUG = false;

	/** Map names to values */
	Map<tN,tV> name2val = new HashMap<tN,tV>();

	/** Stack of scopes */
	Stack<Scope<tN,tV>> scopes = new Stack<Scope<tN,tV>>();

	static class Scope<tN, tV> {
		Object scopeID;
		Map<tN,tV> name2oldval = new HashMap<tN,tV>();

		public Scope(Object scopeID) {
			this.scopeID = scopeID;
		}
	}

	public void enterScope(Object scopeID) {
		if (DEBUG) System.out.println("Enter scope: " + scopeID);
		scopes.push(new Scope<tN,tV>(scopeID));
	}

	public void exitScope(Object scopeID) {
		if (DEBUG) System.out.println("Exit  scope: " + scopeID);
		Scope<tN,tV> s = scopes.pop();
		assert s.scopeID == scopeID : "expected " + scopeID + " but found " + s.scopeID;

		for (Map.Entry<tN,tV> e : s.name2oldval.entrySet()) {
			if (e.getValue() == null) {
				name2val.remove(e.getKey());
			} else {
				name2val.put(e.getKey(), e.getValue());
			}
		}
	}


	public void put(tN name, tV value) {
		assert !scopes.peek().name2oldval.containsKey(name);
		scopes.peek().name2oldval.put(name, name2val.put(name, value));
	}

	public tV get(tN name) {
		return name2val.get(name);
	}


	public boolean containsKey(tN name) {
		return name2val.containsKey(name);
	}

}
