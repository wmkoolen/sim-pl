/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package namespace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.SwingUtilities;

import org.castor.util.ConfigKeys;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.util.Configuration;
import org.exolab.castor.util.LocalConfiguration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.XMLMappingLoader;

import databinding.BasicEditorNode;
import databinding.Complex;
import databinding.Component;
import databinding.EditorNode;
import editor.EventDispatchManager;
import editor.EventDispatcher;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */

/** Provides a thread-safe global mapping from files to components */

public class ComponentTable {

	private static final boolean DEBUG = false;
	private static final boolean DEBUG_CASTOR = false;

	/* ------------ Event dispatch mechanism */
	public static interface ComponentFileChangeListener extends EventListener{
		public void fileChanged(ComponentFileChangedEvent cfce);
	}

	public static class ComponentFileChangedEvent {
		final File f;
		final Component c;
		public ComponentFileChangedEvent(File f, Component c) {
			this.f = f;
			this.c = c;
		}
	}

	static final EventDispatchManager<ComponentFileChangedEvent, ComponentFileChangeListener> edm = new EventDispatchManager<ComponentFileChangedEvent, ComponentFileChangeListener>(){
		protected void dispatchEvent(ComponentFileChangeListener listener, ComponentFileChangedEvent event) {
			listener.fileChanged(event);
		}
	};

	public static EventDispatcher<ComponentFileChangeListener> getEDM() { return edm; }
	/* ------------ Event dispatch mechanism */




	static class StatRecord {
		final File f;

		/** The last accessed time
		 *  using File.lastTime() convention
		 *  0L means the file does not exist, or an IO error occurs
		 * */
		long time;

		/** The component, or null if unparseable */
		Component c;

		/** The parse problem, or null if component is set */
		Exception ex;

		public StatRecord(File f) {
			this.f = f;
			this.time = -1; // File.getLastModified never returns -1
			this.c = null;
			this.ex = null;
		}

//		public synchronized void refresh() {
//			assert f.exists();
//			if (f.lastModified() > time) {
//				time = f.lastModified();
//			}
//		}
	}



//	static Object lock = new Object();

	/** Maps absolute path names to stat recods */
	private static final Map<String, StatRecord> fn2c = new HashMap<String,StatRecord>();

	/** The (global) castor xml mapping  */
	private static Mapping map = new Mapping();

	/** local variable that is only set during component reading or writing */
	static Map<Thread,StatRecord> context = new HashMap<Thread,StatRecord>();


	// static initialiser
	static {
		try {
//			Properties p_org_castor = org.castor.util.Configuration.getInstance().getProperties();
//			p_org_castor.setProperty(ConfigKeys.MAPPING_LOADER_FACTORIES, XMLMappingLoaderFactory.class.getCanonicalName());

			Properties p_org_castor = org.castor.util.Configuration.getInstance().getProperties();
			p_org_castor.setProperty(ConfigKeys.MAPPING_LOADERS, XMLMappingLoader.class.getCanonicalName());


			Properties p_org_exolab = LocalConfiguration.getInstance().getProperties();
			p_org_exolab.setProperty(Configuration.Property.Indent,"true");

			map.loadMapping(EditorNode.class.getResource("mapping.xml"));
		} catch (Exception ex) {
			throw new Error(ex);
		}
	}


	private ComponentTable() { assert false : "static component"; }


//	public static File reverseLookup(Component c) {
//		synchronized (lock) {
//			return new File(fn2c.getF(c));
//		}
//	}

	/** removes all cached file -> component mappings */
	public static void clear() {
		if (DEBUG) System.out.println(ComponentTable.class.getName() + ": clear()");
		// TODO: make this function superfluous by implementing a smart file-time comparison
		synchronized (fn2c) {
			fn2c.clear();
		}
	}


	public static Component load(File faux) throws Exception  {
		File f = faux.getCanonicalFile();
		if (DEBUG) System.out.println(ComponentTable.class.getName() + ": read(" + f + ")");
		StatRecord sr = getSR(f);

		synchronized(sr) {
			long curt = f.lastModified();
			if (curt == sr.time) {  // no change
				if (sr.c == null) throw sr.ex; // rethrow old exception
				return sr.c;
			} else { // file changed
				try {
					if (DEBUG) System.out.println(ComponentTable.class.getName() +
									   ": File " + sr.f + " changed (" +
									   sr.time + " -> " + curt);

					sr.time = curt;
					try {
						if (curt != 0) { // file got content
							// make sure we are NOT there when successful
							// loading of subcomponents causes fire() to be called
							sr.c = null;
							Component c = unmarshall(sr);
							wireParents(c, null);
							// only if we survive this do we become a present component
							sr.c = c;
							sr.ex = null;
						} else { // can not read file for unknown reason
							throw new Exception("Can not get modification time of file " + f);
						}
					} catch (Exception ex) {
						sr.c = null;
						sr.ex = ex; // store exception; one might later try to re-load this file unchanged
						ex.printStackTrace();
						throw ex;
					} catch (Error er) {
						er.printStackTrace();
						throw er;
					}
					return sr.c;
				} finally {
					assert sr.time == curt;
					assert (sr.c != null) == (sr.ex == null);
					fire(new ComponentFileChangedEvent(sr.f, sr.c));
				}
			}
		}
	}


	private static void fire(final ComponentFileChangedEvent cce) {
		if (DEBUG) System.out.println(ComponentTable.class.getName() + ": fire(" + cce.f + ")");
		synchronized(fn2c) {
			// prevent concurrentModificationException
			ArrayList<StatRecord> srs = new ArrayList<StatRecord>(fn2c.values());
			for (StatRecord sr : srs) {
				synchronized (sr) {
					if (sr.c != null && sr.c instanceof Complex) {
						( (Complex)sr.c).rewire();
					}
				}
			}
		}
		if (SwingUtilities.isEventDispatchThread()) {
			edm.fireEvent(cce);
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					edm.fireEvent(cce);
				}
			});
		}
	}

	private static StatRecord getSR(File f) throws IOException {
		String name  = f.getCanonicalPath();
		synchronized (fn2c) {
			StatRecord sr = fn2c.get(name);
			if (sr == null) {
				sr = new StatRecord(f);
				fn2c.put(name, sr);
			}
			assert sr.f.equals(f.getCanonicalFile());
			return sr;
		}
	}


	/** Writes component to file, then stores the component in the cache.
	 *
	 * @param component Component
	 * @param saveTo File
	 * @return Component
	 */

	public static Component write(File faux, Component c)  throws Exception {
		File f = faux.getCanonicalFile();
		if (DEBUG) System.out.println(ComponentTable.class.getName() + ": write(" + f + ")");
		StatRecord sr = getSR(f);

		synchronized(sr) {
			try {

				sr.c = null;  // good value for when component fails to load
				marshall(sr, c);
				sr.c = c; // only sets sr.c if no exception occurs
				return sr.c;
			} finally {
				sr.time = f.lastModified(); // read new time from disk
				fire(new ComponentFileChangedEvent(sr.f, sr.c));
			}
		}
	}


//	public static Component load(Component base, String name) throws Exception {
//		synchronized (lock) {
//			File basef = reverseLookup(base);
//			assert basef != null;
//			File newf = new File(name).isAbsolute()
//				? new File(name)
//				: new File(basef.getParentFile(), name);
//			return load(newf);
//		}
//	}

//	public static void save(final Component c, File f) throws Exception {
//		write(c, f.getAbsoluteFile());
//	}

	public static final String encoding = "ISO-8859-1";


//	private static void write(final Component c, File f) throws Exception {
//		System.out.println("Request to write to " + f);
//
//		// atomically create new file if necessary
//		if (f.createNewFile()) {
//			System.out.println("New file created: " + f + " becomes " + f.getCanonicalFile());
//		}
//		f = f.getCanonicalFile();
//		assert f.isAbsolute();
//
//		Writer w = new OutputStreamWriter(new FileOutputStream(f), encoding);
//		File parentDir = f.getCanonicalFile().getParentFile();
//
//		synchronized(lock) { // protect data structures
//			try {
//				context = parentDir; // set context only during component write
//				Marshaller marshaller = new Marshaller(w);
//				marshaller.setEncoding(encoding);
//				marshaller.setMapping(map);
//				marshaller.setMarshalExtendedType(true);
//				marshaller.setMarshalAsDocument(true);
//				marshaller.setDebug(true);
//				marshaller.setLogWriter(new PrintWriter(System.err));
//				marshaller.marshal(c);
//			} finally {
//				w.close();
//				assert context == parentDir;
//				context = null;
//				fn2c.put(f.getCanonicalPath(), c);
//			}
//		}
//		System.out.println("Change notification for " + f);
//		edm.fireEvent(new ComponentFileChangedEvent(f, c));
//	}



//	/** Request to reconsider the given file */
//	public static Component load(final File f) throws Exception {
//		assert f.isAbsolute();
//		// TODO: implement per-entry locking instead of a big global lock
//		synchronized (lock) {
//			String name = f.getCanonicalPath();
//			Component c = fn2c.get(name);
//
//			if (c == null) {
//				c = unmarshall(f);
//				wireParents(c, null);
//				fn2c.put(name, c);
//			}
//			assert fn2c.getS(name) == c;
//			assert fn2c.getF(c).equals(name);
//
//			return c;
//		}
//	}




	private static void marshall(StatRecord sr, final Component c) throws Exception {
		if (DEBUG) System.out.println("Marhshall to " + sr.f);
		assert Thread.holdsLock(sr);
		assert !context.containsKey(Thread.currentThread());
		context.put(Thread.currentThread(), sr);

		Writer w = new OutputStreamWriter(new FileOutputStream(sr.f), encoding);
		try {
			Marshaller marshaller = new Marshaller(w);
			marshaller.setEncoding(encoding);
			marshaller.setMapping(map);
			marshaller.setMarshalExtendedType(true);
			marshaller.setMarshalAsDocument(true);
			marshaller.setDebug(DEBUG_CASTOR);
			marshaller.setLogWriter(new PrintWriter(System.err));
			marshaller.marshal(c);
		} finally {
			w.close();
			assert context.get(Thread.currentThread()) == sr;
			context.remove(Thread.currentThread());
		}
	}


	private static Component unmarshall(StatRecord sr) throws Exception {
		if (DEBUG) System.out.println("Unmarhshall from " + sr.f);
		assert Thread.holdsLock(sr);
		assert !context.containsKey(Thread.currentThread());
		context.put(Thread.currentThread(), sr);


		Reader reader = new FileReader(sr.f);
		try {
			Unmarshaller unmarshaller = new Unmarshaller(map);
			unmarshaller.setClearCollections(true);
			unmarshaller.setDebug(DEBUG_CASTOR);
			// TODO: set to false to be really strict
			unmarshaller.setIgnoreExtraAttributes(true);
			unmarshaller.setIgnoreExtraElements(false);
			unmarshaller.setWhitespacePreserve(true);
			Component c = (Component)unmarshaller.unmarshal(reader);
			// NEED TO INVOKE WIREPARENTS OUTSIDE OF context LOCK
			return c;
		} finally {
			reader.close();
			assert context.containsKey(Thread.currentThread());
			assert context.get(Thread.currentThread()) == sr;
			context.remove(Thread.currentThread());
		}
	}





	public static Component expensiveCopy(Component orig) throws Exception {
		if (DEBUG) System.out.println("expensiveCopy");
		StatRecord sr = new StatRecord(new File("nofile").getAbsoluteFile()); // just some file used as the base for resolution
		synchronized(sr) {
			assert Thread.holdsLock(sr);
			assert!context.containsKey(Thread.currentThread());
			context.put(Thread.currentThread(), sr);

			StringWriter w = new StringWriter();

			Marshaller marshaller = new Marshaller(w);
			marshaller.setEncoding(encoding);
			marshaller.setMapping(map);
			marshaller.setMarshalExtendedType(true);
			marshaller.setMarshalAsDocument(true);
			marshaller.setDebug(DEBUG_CASTOR);
			marshaller.setLogWriter(new PrintWriter(System.err));
			marshaller.marshal(orig);


			StringReader r = new StringReader(w.toString());

			Unmarshaller unmarshaller = new Unmarshaller(map);
			unmarshaller.setClearCollections(true);
			unmarshaller.setDebug(DEBUG_CASTOR);
			unmarshaller.setIgnoreExtraAttributes(false);
			unmarshaller.setIgnoreExtraElements(false);
			unmarshaller.setWhitespacePreserve(true);
			Component copy = (Component)unmarshaller.unmarshal(r);
			assert copy != null;

			assert context.containsKey(Thread.currentThread());
			assert context.get(Thread.currentThread()) == sr;
			context.remove(Thread.currentThread());

			wireParents(copy, null);
			if (copy instanceof Complex) ((Complex)copy).rewire();
			return copy;
		}
	}





	private static void wireParents(BasicEditorNode node, BasicEditorNode parent) {
//		assert Thread.holdsLock(lock);
		node.wire(parent);
		assert node.getParent() == null;
		assert node.isValidRecursively();
	}
}


