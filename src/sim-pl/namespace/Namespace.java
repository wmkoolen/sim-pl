/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package namespace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: Digital Component Simulator</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Wouter Koolen-Wijkstra</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class  Namespace<nT,dT,rT> {
	/** maps identifiers to sets of def and ref nodes */
	private final Map<nT, RefDef<dT, rT>> id2nodedefref = new HashMap<nT, RefDef<dT, rT>> ();

	/** maps nodes to sets of def and ref identifiers */
	private final Map<dT, Set<nT>> node2iddef = new HashMap<dT, Set<nT>>();
	private final Map<rT, Set<nT>> node2idref = new HashMap<rT, Set<nT>>();

	/** Declare and define a certain indentifier
	 * @param name nT the identifier to define
	 * @param def dT  the node that declared the identifier
	 */
	public void addDefiner(nT name, dT def) {
		RefDef<dT,rT> e = id2nodedefref.get(name);
		if (e == null) {
			e = new RefDef<dT,rT>();
			id2nodedefref.put(name, e);
		}
		assert !e.def.contains(def);
		e.def.add(def);

		Set<nT> iddefs = node2iddef.get(def);
		if (iddefs == null) {
			iddefs = new HashSet<nT>();
			node2iddef.put(def, iddefs);
		}
		assert !iddefs.contains(name);
		iddefs.add(name);
	}

	public void addReferencer(nT name, rT ref) {
		RefDef<dT,rT> e = id2nodedefref.get(name);
		if (e == null) {
			e = new RefDef<dT,rT>();
			id2nodedefref.put(name, e);
		}
		assert !e.ref.contains(ref);
		e.ref.add(ref);

		Set<nT> idrefs = node2idref.get(ref);
		if (idrefs == null) {
			idrefs = new HashSet<nT> ();
			node2idref.put(ref, idrefs);
		}
		assert!idrefs.contains(name);
		idrefs.add(name);

	}

	public RefDef<dT,rT> getRefDef(nT name) {
		assert id2nodedefref.containsKey(name);
		return id2nodedefref.get(name);
	}

	public Set<nT> keySet() {
		return id2nodedefref.keySet();
	}

	public Set<Map.Entry<nT, RefDef<dT,rT>>> entrySet() {
		return id2nodedefref.entrySet();
	}

	public dT getUniqueDef(nT name) throws Exception {
		RefDef<dT,rT> refdef = id2nodedefref.get(name);
		if (refdef == null) throw new Exception(name + " undefined");
		assert !refdef.def.isEmpty();
		if (refdef.def.size() > 1) throw new Exception(name + " multiply defined");
		return refdef.def.get(0);
	}


	public boolean isDefined(nT name) {
		assert !id2nodedefref.containsKey(name) || !id2nodedefref.get(name).def.isEmpty();
		return id2nodedefref.containsKey(name);
	}


	public Set<nT> getDefIDs(dT node) {
		assert node2iddef.containsKey(node);
		return Collections.unmodifiableSet(node2iddef.get(node));
	}

	public Set<nT> getRefIDs(rT node) {
		assert node2idref.containsKey(node);
		return Collections.unmodifiableSet(node2idref.get(node));
	}


	public String toString() {
		return id2nodedefref.toString();
	}


	public static class RefDef<dT,rT> {
		public final ArrayList<dT> def = new ArrayList<dT>();
		public final ArrayList<rT> ref = new ArrayList<rT>();

		public String toString() {
			return "[def: " + def + " | ref: " + ref + "]";
		}
	}

	public void clear() {
		id2nodedefref.clear();
		node2iddef.clear();
		node2idref.clear();
	}
}
