/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package namespace;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import databinding.BasicEditorNode;
import databinding.CompilationMessageBuffer;
import databinding.Component;
import databinding.Input;
import databinding.ParameterDecl;
import databinding.Pin;
import databinding.Storage;
import databinding.SubComponent;
import databinding.SubPinStub;
import databinding.compiled.Compiled.CompiledComponent;
import databinding.wires.MainPinStub;
import databinding.wires.Wire;

/** A ComponentNamespace is a part of the complete dependency graph. SIM-PL uses this graph to recompile and redraw only (possibly) changed parts.
 *  The dependency graph is organised as a nested sequence of namespaces.
 *  - the root ComponentTable is a namespace that maps file names to ComponentNamespace instances
 *  - For each Component, there is a ComponentNamespace. This class contains separate namespaces for
 *     - parameters
 *     - pins
 *     - subcomponents
 *     - memory
 *    as one can see, we do not distinguish simple/complex components here.
 */
public class ComponentNamespace {
	final Component c;

	// the following namespaces map identifiers to def/ref node sets and reverse

	// parameter related
	public final Namespace<String, ParameterDecl, BasicEditorNode> par = new Namespace<String,ParameterDecl,BasicEditorNode>();

	// pin related
	public final Map<Pin, Integer> pin2bits = new IdentityHashMap<Pin,Integer>();
	public final Namespace<String, Pin, BasicEditorNode>           pin = new Namespace<String,Pin,BasicEditorNode>();

	// complex: subcomponents and cables
	public final Map<SubComponent, ComponentNamespace> sub2cns = new IdentityHashMap<SubComponent,ComponentNamespace>();
//	public final Map<String, Collection<Wire>> pinref2cable = new HashMap<String,Collection<Wire>>();
	public final Namespace<String, Wire, BasicEditorNode> pinref2cable = new Namespace<String,Wire,BasicEditorNode>();
	public final Namespace<String, SubComponent, BasicEditorNode>  sub = new Namespace<String,SubComponent,BasicEditorNode>();

	// simple: storages
	public final Namespace<String, Storage, BasicEditorNode>       mem = new Namespace<String,Storage,BasicEditorNode>();



	public ComponentNamespace(Component c) {
		this.c = c;
	}

//	public Namespace<String, ? extends BasicEditorNode<?>, BasicEditorNode> getSubspace(Class classIdent) {
//		if (classIdent == Pin.class)           return pin;
//		if (classIdent == SubComponent.class)  return sub;
//		if (classIdent == ParameterDecl.class) return par;
//		if (classIdent == Storage.class)       return mem;
//		throw new Error("Invalid subspace: " + classIdent);
//	}


	public static class PinInfo {
		public int     bitWidth;
		public boolean isCabled;
		public boolean isSender;

		PinInfo(int bitWidth, boolean isCabled, boolean isSender) {
			this.bitWidth = bitWidth;
			this.isCabled = isCabled;
			this.isSender = isSender;
		}

		public String toString() {
			return "bits: " + bitWidth + ", cabled: " + isCabled + ", sender: " + isSender;
		}
	}



	public PinInfo getPinInfo(SubPinStub dpr) throws Exception {
		ComponentNamespace sub_cns = sub2cns.get(dpr.getParent().getParent());
		if (sub_cns == null) throw new Exception("unable to resolve subcomponent");

		Pin p = sub_cns.pin.getUniqueDef(dpr.getPinRef());
		assert p != null;
		Integer bits = sub_cns.pin2bits.get(p);
		if (bits == null) throw new Exception("unable to determine bit width");
		PinInfo pi = new PinInfo(bits, false, p instanceof Input);

		// embedding flips sender/receiver state
		pi.isSender = !pi.isSender;

		// handle cabled state
		// only when we're not the root
		if (!pi.isCabled) {
			String pinref = dpr.getSubComponent().getName() + ":" + dpr.getPinRef();
			pi.isCabled |= pinref2cable.isDefined(pinref);
		}
		return pi;
	}


	public PinInfo getPinInfo(MainPinStub dpr) throws Exception {
		Pin p = pin.getUniqueDef(dpr.getPin().getName());
		assert p != null;
		Integer bits = pin2bits.get(p);
		if (bits == null) throw new Exception("unable to determine bit width");
		return new PinInfo(bits, false, p instanceof Input);
	}




	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("Pin: " + pin + "\n");
		b.append("Par: " + par + "\n");
		b.append("Mem: " + mem + "\n");
		b.append("Sub: " + sub + "\n");
		b.append("pinref2cable: " + pinref2cable + "\n");
		b.append("pin2bits: " + pin2bits + "\n");
		return b.toString();
	}

	static final boolean DEBUG = false;

	final public CompiledComponent compile(ParameterContext<String, Integer> pc, Set<String> wiredPins, CompilationMessageBuffer cmb) {
		if (DEBUG) System.out.println("Compilation triggered for: " + c);

		// TODO: remove us from all reffed and deffed ids
		// TODO: the following is just a hack
		pin.clear();
		pin2bits.clear();
		pinref2cable.clear();
		mem.clear();
		par.clear();
		sub.clear();
		sub2cns.clear();



		// enter new scope
		pc.enterScope(this);

		// collect definitions (to check for duplicates later)
		c.collectDefs(this);

		CompiledComponent tt = c.compile(pc, wiredPins, this, cmb);

		pc.exitScope(this);

		if (DEBUG) System.out.println("Compilation result: \n" + this + "\n"+ "TypeTree: " + tt);

		return tt;
	}
}
