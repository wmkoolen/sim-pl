/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package about;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.lang.reflect.Method;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;

import util.JAALabel;
import util.VersionManager;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SplashScreen extends JWindow {

	String head;

	// jbuilder happy constructor
	public SplashScreen() {
		this("Test");
	}

	public SplashScreen(String head) {
		this.head = head;
		jbInit();
	}

	private void jbInit()  {
		JComponent cp = ((JComponent)this.getContentPane());
		cp.setBackground(Color.white);
		cp.setBorder(BorderFactory.createRaisedBevelBorder());
		top.setHorizontalAlignment(SwingConstants.CENTER);
		top.setText(head);
		bottom.setText(VersionManager.currentVersion);
		this.getContentPane().add(center, java.awt.BorderLayout.CENTER);
		this.getContentPane().add(top, java.awt.BorderLayout.NORTH);
		this.getContentPane().add(bottom, java.awt.BorderLayout.SOUTH);

		int logoScaleTo = 500;

		ImageIcon simpl = new ImageIcon(SplashScreen.class.getResource("SIM-PL2_big.png"));
		simpl.setImage(simpl.getImage().getScaledInstance(logoScaleTo, -1, Image.SCALE_SMOOTH));

		Font huge = new java.awt.Font("Serif", Font.ITALIC, 50);
		top.setFont(huge);
//		center.setText(VersionManager.currentVersion);
		center.setIcon(simpl);
//		jLabel1.setVerticalAlignment(SwingConstants.BOTTOM);
//		jLabel1.setHorizontalAlignment(SwingConstants.CENTER);NTER);
		center.setHorizontalTextPosition(SwingConstants.CENTER);
		center.setVerticalTextPosition(SwingConstants.BOTTOM);
		pack();
	}

	JLabel center = new JLabel();
	JLabel top = new JAALabel();
	JLabel bottom = new JAALabel();


	/** Note: the main method of className is responsible for the setVisible call
	 *  we want NO hard reference to the main class, for we want to to show the
	 *  splash screen as soon as possible. Hence we do not want any other classes
	 * to be loaded automatically before we call setVisible.
	 *
	 * @param title String
	 * @param className String
	 * @param args String[]
	 */

	public static void launch(String title, String className, String [] args) throws Exception {
		SplashScreen ss = new SplashScreen(title);
		ss.setLocationRelativeTo(null);
		ss.setVisible(true);
		Class e = Thread.currentThread().getContextClassLoader().loadClass(className);
		Method m = e.getMethod("main", new Class[] {args.getClass()});
		m.invoke(null, new Object [] {args});

		Thread.sleep(1000);

		ss.setVisible(false);
		ss.dispose();
	}
}
