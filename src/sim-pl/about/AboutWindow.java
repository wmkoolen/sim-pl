/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package about;


import icons.IconManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import util.FileSlurper;
import util.JAALabel;
import util.VersionManager;

import com.centerkey.utils.BareBonesBrowserLaunch;

public class AboutWindow extends JDialog
{
    JScrollPane spLicense = new JScrollPane();
    BorderLayout borderLayout1 = new BorderLayout();


	static final String LICENSE = "LICENSE";
	static final String LICENSE_TEXT = FileSlurper.slurp(AboutWindow.class, LICENSE);

	static final String LICENSE_NOTICE = "LICENSE_NOTICE";
	static final String LICENSE_NOTICE_TEXT = FileSlurper.slurp(AboutWindow.class, LICENSE_NOTICE);



    JTextArea text = new JTextArea(LICENSE_TEXT);
	JTextArea text_notice = new JTextArea(LICENSE_NOTICE_TEXT);

    JTabbedPane jTabbedPane1 = new JTabbedPane();
	JPanel Producers = new JPanel();
    JLabel jLabel2 = new JLabel();
    JLabel jLabel6 = new JLabel();
    BorderLayout borderLayout2 = new BorderLayout();
    JLabel jLabel1 = new JLabel();
    JLabel jLabel3 = new JLabel();
//    JPanel jPanel2 = new JPanel();
    JLabel jLabel7 = new JLabel();
    JLabel jLabel5 = new JLabel();
    JLabel jLabel8 = new JAALabel();
    JPanel People = new JPanel();
    JLabel jLabel9 = new JLabel();
    JLabel jLabel10 = new JLabel();
	JLabel jLabel4 = new JLabel();
    JLabel jLabel11 = new JLabel();
    JScrollPane spPeople = new JScrollPane();
    JScrollPane spProducers = new JScrollPane();
	JScrollPane spNotice = new JScrollPane();
    JLabel jLabel12 = new JLabel();
    JLabel jLabel13 = new JLabel();
    JLabel jLabel14 = new JLabel();
    JLabel jLabel15 = new JLabel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	GridBagLayout gridBagLayout2 = new GridBagLayout();
	JLabel jLabel16 = new JLabel();
	JLabel jLabel17 = new JLabel();
	JButton jButton1 = new JButton();
//	JScrollPane Libraries = new JScrollPane();
//	JTextArea text_libraries = new JTextArea();

	public AboutWindow(Frame owner, String component)
    {
		super(owner, "About - " + VersionManager.currentVersion, true);

		jbInit();

		jLabel8.setText("SIM-PL " + component);

		this.pack();
    }

	public void setVisible(boolean visible)
	{
	    if (visible) this.setLocationRelativeTo(this.getOwner());
	    super.setVisible(visible);
	}

    public AboutWindow()
    {
		this(null, "TEST");
    }


    private void jbInit()
    {
        this.getContentPane().setLayout(borderLayout1);
        text.       setEditable(false);
		text_notice.setEditable(false);
//		text_libraries.setEditable(false);

		text.       setDisabledTextColor(Color.black);
		text_notice.setDisabledTextColor(Color.black);
//		text_libraries.setDisabledTextColor(Color.black);

//		text.       setBackground(Color.lightGray.ligh);
//		text_notice.setBackground(Color.lightGray);


		int producerScaleto = 75;
		int peopleScaleTo = 150;
		int logoScaleTo = 150;

		Font big =  new java.awt.Font("Dialog", Font.BOLD, 16);
		Font huge = new java.awt.Font("Serif", Font.ITALIC, 80);
		Font mono = new java.awt.Font("MonoSpaced", Font.PLAIN, 13);

		text.setFont(mono);
		text_notice.setFont(mono);
//		text_libraries.setFont(mono);

		text.setRows(26);
		text_notice.setRows(26);
//		text_libraries.setRows(26);

		ImageIcon uva = IconManager.load(AboutWindow.class, "uva.gif");
		uva.setImage(uva.getImage().getScaledInstance(-1, producerScaleto, Image.SCALE_SMOOTH));

		ImageIcon du =  IconManager.load(AboutWindow.class, "du.gif");
		du.setImage(du.getImage().getScaledInstance(-1, producerScaleto,Image.SCALE_SMOOTH));

		ImageIcon amstel = IconManager.load(AboutWindow.class, "amstel.gif");
		// amstel image height is fine, no scale
//		amstel.setImage(amstel.getImage().getScaledInstance(-1, producerScaleto,Image.SCALE_SMOOTH));

		ImageIcon pgexp = IconManager.load(AboutWindow.class, "e-xperimenteren.gif");
		// pgexp image height is fine, no scale
//		pgexp.setImage(pgexp.getImage().getScaledInstance(-1, producerScaleto,Image.SCALE_SMOOTH));


		ImageIcon wouter = IconManager.load(AboutWindow.class, "wouter.jpg");
		wouter.setImage(wouter.getImage().getScaledInstance(-1, peopleScaleTo,Image.SCALE_SMOOTH));

		ImageIcon ben = 	IconManager.load(AboutWindow.class, "ben.jpg");
		ben.setImage(ben.getImage().getScaledInstance(-1, peopleScaleTo,Image.SCALE_SMOOTH));

		ImageIcon toto = 	IconManager.load(AboutWindow.class, "toto.jpg");
		toto.setImage(toto.getImage().getScaledInstance(-1, peopleScaleTo,Image.SCALE_SMOOTH));

		ImageIcon simpl = IconManager.load(AboutWindow.class, "SIM-PL2_small.png");
//		simpl.setImage(simpl.getImage().getScaledInstance(-1, logoScaleTo,Image.SCALE_SMOOTH));

		ImageIcon edict =  IconManager.load(AboutWindow.class, "edict.gif");
		edict.setImage(edict.getImage().getScaledInstance(-1, producerScaleto,Image.SCALE_SMOOTH));

		jLabel12.setIcon(pgexp);
        jLabel4.setIcon(ben);

        jLabel6.setText("Digitale Universiteit");
		Producers.setLayout(borderLayout2);

		jLabel1.setIcon(uva);
        jLabel2.setIcon(du);
        jLabel3.setIcon(amstel);
        Producers.setLayout(gridBagLayout2);
        jLabel7.setText("Amstel Instituut");

        jLabel5.setText("Universiteit van Amsterdam");



		jLabel6.setFont(big);
		jLabel7.setFont(big);
		jLabel5.setFont(big);
		jLabel13.setFont(big);
		jLabel9.setFont(big);
		jLabel11.setFont(big);
		jLabel14.setFont(big);
		jLabel17.setFont(big);

		jLabel10.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel3.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel8.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel4.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel12.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel15.setHorizontalAlignment(SwingConstants.CENTER);

		jLabel8.setFont(huge);
		jLabel8.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		jLabel8.setIcon(simpl);
		jLabel8.setText("<About>");
		People.setLayout(gridBagLayout1);

        jLabel10.setIcon(wouter);
		jLabel10.setHorizontalTextPosition(SwingConstants.CENTER);
        jLabel10.setVerticalTextPosition(SwingConstants.BOTTOM);
        jLabel9.setText("Wouter Koolen-Wijkstra");

        jLabel11.setText("Ben Bruidegom");
        jLabel13.setText("Projectgroep e-xperimenteren");


        this.getContentPane().setBackground(Color.white);
		jLabel14.setText("Toto van Inge");

        jLabel15.setIcon(toto);
		jLabel16.setIcon(edict);
		jLabel17.setText("Stichting Edict");
		jButton1.setText("SIM-PL Web Page");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BareBonesBrowserLaunch.openURL(VersionManager.currentURL);
			}
		});
//		text_libraries.setText("jTextArea1");

		this.getContentPane().add(jTabbedPane1,  BorderLayout.CENTER);

		spLicense.getViewport().add(text, null);
		spNotice.getViewport().add(text_notice, null);
		spPeople.getViewport().add(People);
        jTabbedPane1.add(spPeople,  "People");
		jTabbedPane1.add(spProducers,  "Producers");
        jTabbedPane1.add(spNotice,  "Notice");
		jTabbedPane1.add(spLicense,  "License");
//		jTabbedPane1.add(Libraries, "Libraries");
//		Libraries.getViewport().add(text_libraries);
		spProducers.getViewport().add(Producers);
//        Producers.add(jPanel2, BorderLayout.CENTER);
		People.add(jLabel10, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 260, 56));
		People.add(jLabel9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 1), 173, 186));
		People.add(jLabel4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 234, 56));
		People.add(jLabel11, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 1), 241, 186));
		People.add(jLabel15, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 1, 0), 263, 56));
		People.add(jLabel14, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 1, 1), 253, 186));
		Producers.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 318, 74));
		Producers.add(jLabel5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 162, 129));
		Producers.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 226, 74));
		Producers.add(jLabel6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 229, 129));
		Producers.add(jLabel7, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 259, 129));
		Producers.add(jLabel12, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 3, 0), 89, 107));
		Producers.add(jLabel13, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 3, 0), 144, 129));
		Producers.add(jLabel16, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
			, GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		Producers.add(jLabel17, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(jButton1, java.awt.BorderLayout.SOUTH);
		this.getContentPane().add(jLabel8, java.awt.BorderLayout.NORTH);
		Producers.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
			, GridBagConstraints.WEST, GridBagConstraints.NONE,
			new Insets(0, 0, 0, 20), 0, 92));
		//		jTabbedPane1.add(Libraries, "Libraries");
		util.DialogHelper.performOnEscapeKey(this, new AbstractAction() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
    }

}
