/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package about;

import icons.IconManager;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class LogoPanel extends JPanel
{

//import java.awt.*;
//import java.awt.event.*;
//import java.awt.image.*;
//import javax.swing.*;
//import component.Instance;
//import Simulator.Instance_Pin;
//import component.Component;
//import util.ExceptionHandler;
//import about.AboutWindow;

	static final private    Image backlogo = IconManager.loadImage(LogoPanel.class, "uvalogo.jpg");;
	static final private 	Image uva =      IconManager.loadImage(LogoPanel.class, "UvaBump.png");

    public LogoPanel()
	{
		this.setBackground(Color.lightGray);
    }


	public void paint(Graphics g)
	{
		super.paint(g);

		// draw a diagonally tiled logo pattern
		int realw = uva.getWidth(this), realh = uva.getHeight(this);
		int tilew = 200, tileh = 75;

		for (int yi = 0; yi <= getHeight()/tileh; yi++) {
			for (int xi = 0; xi <= getWidth()/tilew; xi++) {
				g.drawImage(uva, ((2*xi+1-yi%2)*tilew-realw)/2, ((2*yi+1)*tileh-realh)/2, this);
			}
		}

		// center banner image over it
		int x = (this.getWidth() -  backlogo.getWidth(this))/2;
		int y = (this.getHeight() - backlogo.getHeight(this))/2;

		g.drawImage(backlogo, x,y, this);
		g.drawImage(backlogo, 0,y, x,y+backlogo.getHeight(this), 0,0, 1,backlogo.getHeight(this),this);
		g.drawImage(backlogo, x+backlogo.getWidth(this),y, getWidth(),y+backlogo.getHeight(this), backlogo.getWidth(this)-1,0, backlogo.getWidth(this),backlogo.getHeight(this),this);
	}

}
