# Author: Wouter Koolen-Wijkstra
# This file contains the 16 bit Bus WASM instruction format definition

.memory REGISTERS
.setting  {
  targetMemory = "16 bit bus architectuur.Registers:Registers"
  addressType = "register"
}



.processor BUS
.setting {
	targetMemory = "16 bit bus architectuur.Mem:Memory"
	addressType = "address"
}

.highlight {
	[ "16 bit bus architectuur.PC" | "Out" | "#E0E0FF"]
}

.autocast {
        label -> address
}

.constant {
   $0  =	0/register
   $1  =  	1/register 
   $2  =  	2/register
   $3  =  	3/register
   $4  =  	4/register
   $5  =  	5/register
   $6  =  	6/register
   $7  =  	7/register
}


# Meaning of OP
# 0: Reg-Reg computation
# 1: Reg-Imm computation
# 2: Branch
#    Meaing of INF
#    0: branch on zero ALU output
#    1: branch on non-zero ALU output
# 3: Memory access
#    Meaing of INF
#    0: load
#    1: store

.opcodepartition {
	DEFAULT =	[UNUSED:1, INF:1, OP:2, ALU:3, RS:3, RT:3, RD:3 | "UNUSED=0;"]
	CONSTANT =	[UNUSED:1, INF:1, OP:2, ALU:3, RS:3, RT:3, RD:3 ; VAL:16  | "UNUSED=0;"]
}


.instructionformat {
	ARITH1 =	[DEFAULT  | rd:4/register,                  rt:4/register | "OP=0; INF=0; RS= 0; RT=rt; RD=rd;" ]
	ARITH2 =	[DEFAULT  | rd:4/register, rs:4/register, rt:4/register | "OP=0; INF=0; RS=rs; RT=rt; RD=rd;" ]

	ARITH1I =	[CONSTANT | rd:4/register,                  imm:16/number | "OP=1; INF=0; RS= 0; RT= 0; RD=rd; VAL=imm;" ]
	ARITH2I =	[CONSTANT | rd:4/register, rs:4/register, imm:16/number | "OP=1; INF=0; RS=rs; RT= 0; RD=rd; VAL=imm;" ]

	BRANCH0 =	[CONSTANT |                                   offs:16/label | "OP=2; RS= 0; RD= 0; RT=0; VAL=offs;"]
	BRANCH1 =	[CONSTANT | rt:4/register,                  offs:16/label | "OP=2; RS= 0; RD= 0; RT=rt; VAL=offs;"]
	BRANCH2 =	[CONSTANT | rt:4/register, rs:4/register, offs:16/label | "OP=2; RS=rs; RD= 0; RT=rt; VAL=offs;"]

	LOAD =		[CONSTANT | rt:4/register, rs:4/register, offs:16/address | "OP=3; RS=rs; RD= rt; RT=  0; VAL=offs;"]
	STORE =		[CONSTANT | rt:4/register, rs:4/register, offs:16/address | "OP=3; RS=rs; RD=  0; RT= rt; VAL=offs;"]

	HALT =		[CONSTANT |  | "OP=2; RS= 0; RD= 0; RT=0; VAL=self;"]
}

.instructiondefinition {
	ADD =	[ARITH2  | "ALU = 0;"]
	SUB =	[ARITH2  | "ALU = 1;"]
	AND =	[ARITH2  | "ALU = 2;"]
	COPY =	[ARITH1  | "ALU = 3;"]
	OR =	[ARITH2  | "ALU = 4;"]
	XOR =	[ARITH2  | "ALU = 5;"]
	SHL =	[ARITH2  | "ALU = 6;"]
	SHR =	[ARITH2  | "ALU = 7;"]

	ADDI =	[ARITH2I  | "ALU = 0;"]
	SUBI =	[ARITH2I  | "ALU = 1;"]
	ANDI =	[ARITH2I  | "ALU = 2;"]
	LOADI =	[ARITH1I  | "ALU = 3;"]
	ORI =	[ARITH2I  | "ALU = 4;"]
	XORI =	[ARITH2I  | "ALU = 5;"]
	SHLI =	[ARITH2I  | "ALU = 6;"]
	SHRI =	[ARITH2I  | "ALU = 7;"]

	# use 5 (XOR) and 3 (COPY) for branches
	BRA =	[BRANCH0 | "INF=0; ALU=5;"]
	BZ =	[BRANCH1 | "INF=0; ALU=3;"]
	BNZ =	[BRANCH1 | "INF=1; ALU=3;"]
	BEQ =	[BRANCH2 | "INF=0; ALU=5;"]
	BNE =	[BRANCH2 | "INF=1; ALU=5;"]

	LW =	[LOAD  | "INF=0; ALU=0;"]
	SW =	[STORE | "INF=1; ALU=0;"]

	# use 1 (SUB) for halt
	HALT = [HALT | "INF=0; ALU=1;"]
}


