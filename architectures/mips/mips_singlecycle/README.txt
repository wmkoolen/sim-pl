Singlecycle MIPS architecture.
==============================
This architecture conforms as most as possible the layout as used in Patterson and Hennessy,
Computer Organization and Design (5th edition), chapter 4. A few small changes are added in the graphics:a jal
structure is added to allow the execution of function calls. Of course the number of control lines
is slightly different. 


Instruction set
===============
The following instruction are implemented:
1. add
2. addi
3. sub

...

Register names
==============
supported names and values

Hexadecimal values of instructions
==================================
tbd

Not supported
=============
Floating point

Changes
=======
aug 2015. TW: "official" MIPS register names added to mips.wasm
aug 2015. TW: starting bug fix SLL to conform to SLL, $r1, $r2, shamt instead of 3 regs. arg
aug. 2016 TW: fix mult high word result 
aug. 2016 TW: support for MIPS registernames $a0 etc.
