# these variables are passed in by ant through build.xml
ifndef SRC
$(error SRC required)
endif

ifndef TRG
$(error TRG required)
endif

.DELETE_ON_ERROR:
.SECONDEXPANSION:

# compute list of parent directories: a/b/c/ results in a/b/c a/b a
tc = $(foreach parent,$(filter-out .,$(patsubst %/,%,$(dir $(1)))), $(call tc, $(parent)) $(parent))


# find all .wasms that do not contain "include"
BIS := $(shell find $(SRC) -name '*.bi')
JAVABIS := $(patsubst $(SRC)/%.bi,$(TRG)/%BeanInfo.java,$(BIS))
DIRS  := $(sort $(call tc, $(JAVABIS)))

.PHONY : bi2java clean
bi2java : $(JAVABIS)


$(JAVABIS) : $(TRG)/%BeanInfo.java : $(SRC)/%.bi scripts/beaninfo.pl | $$(@D)
	scripts/beaninfo.pl $(subst /,.,$*) $< > $@

$(DIRS) : | $$(@D)
	mkdir $@

clean : 
	rm -rf $(JAVABIS)