/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

import junit.framework.TestCase;


public class TestNumberFormat extends TestCase
{
  public TestNumberFormat(String s)
  {
    super(s);
  }

  Object [][] a;

  protected void setUp() throws Exception
  {
	    a = new Object [][]
		{   {"0xF",     new Hexadecimal(),  new nBit(10, 0xF)},
		    {"0xf",     new Hexadecimal(),  new nBit(10, 0xF)},
			{"0b1",     new Binary(),       new nBit(10, 1)},
			{"0d9",     new Decimal(),      new nBit(10, 9)},
			{"0000",    null,               new nBit(10, 0)},
		};
  }

  protected void tearDown()
  {
  }

  public void testParse()throws Exception
  {
	NumberFormat.parse(new Binary().toString()).equals(new Binary());
	NumberFormat.parse(new Decimal().toString()).equals(new Decimal());
	NumberFormat.parse(new Hexadecimal().toString()).equals(new Hexadecimal());
  }

  public void testIsSigned()
  {
	assertTrue(NumberFormat.isSigned("-10",     new Decimal()));
	assertTrue(NumberFormat.isSigned("0d-10",   new Binary()));
  }


  public void testGetRepresentation()
  {
	for (int i = 0; i < a.length; i++)
	{
		assertEquals(a[i][1], NumberFormat.getRepresentation((String)a[i][0], null));
	}
  }

  public void testParse_nBit()
  {
	for (int i = 0; i < a.length; i++)
	{
		assertTrue(a[i][2].equals(NumberFormat.parse_nBit((String)a[i][0], new Decimal())));
	}
  }


}
