/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package nbit;

import junit.framework.TestCase;

public class TestnBit extends TestCase
{

	public TestnBit(String s)
    {
        super(s);
    }

	private int compare(nBit v1, nBit v2)
	{
		int result = v1.compareTo(v2);
//		System.out.println("Comparing: " + v1 + " to " + v2 + " gives " + result);
		return result;
	}




	public void testCompareTo() throws Exception
    {
	    nBit [] a = {   new nBit(1, 0),
		    			new nBit(3, 1),
			    		new nBit(15, 0x7FFF),
				    	new nBit(16, 0x8000),
						new nBit(32, 0xAF71),
	    				new nBit(32, 0xFFFF),
		    			new nBit(32, 0x7FFFFFFF),
			    		new nBit(32, 0x80000000),
				    	new nBit(32, 0xAF711234),
					    new nBit(32, 0xFFFFFFFF),
						new nBit(33, 0x1).SHIFT_LEFT(new nBit(32, 32)),
	    				new nBit(33, 0xFFFFFFFF).SHIFT_LEFT(nBit.one),
		    			new nBit(34, 0x1).SHIFT_LEFT(new nBit(32, 33)),
			    		};

	    for (int i = 0; i < a.length; i++)
	    {
		    assertTrue(compare(a[i], a[i]) == 0);

		    for (int j = i+1; j < a.length; j++)
		    {
			    assertTrue(compare(a[i], a[j]) < 0);
			    assertTrue(compare(a[j], a[i]) > 0);
		    }
	    }
	}



	private nBit [] divide(nBit v1, nBit v2)
	{
		nBit [] result = v1.Division(v2);
//		System.out.println("Dividing: " + v1 + " by " + v2 + " gives " + result[0] + " rem " + result[1]);
		return result;
	}


	public void testDivide() throws Exception
	{
	    nBit [][] a =   {   {new nBit(32, 0xA), new nBit(32, 0x2), new nBit(32, 0x5), new nBit(32, 0x0) },
							{new nBit(32, 0xFFFFFFFF), new nBit(32, 0xF), new nBit(32, 0x11111111), new nBit(32, 0x0)},
							{new nBit(32, 0xFFFFFFFF), new nBit(32, 0xA), new nBit(32, 0x19999999), new nBit(32, 0x5)},
							{new nBit(36, 0xFFFFFFFF).SHIFT_LEFT(new nBit(32,4)), new nBit(32, 0xA0), new nBit(32, 0x19999999), new nBit(32, 0x50)},
			    		};

		for (int i = 0; i < a.length; i++)
		{
			nBit [] r = divide(a[i][0], a[i][1]);
			assertTrue(r[0].equals(a[i][2]));
			assertTrue(r[1].equals(a[i][3]));
		}
	}

	public void testBitsUsed() throws Exception {
		assertEquals(nBit.bitsUsed(0), 1); // this is a little strange of course
		assertEquals(nBit.bitsUsed(1), 1);
		assertEquals(nBit.bitsUsed(2), 2);
	}

	public void testShift() throws Exception
	{
		assertEquals(new nBit(0x20,0x00000002),
					 new nBit(0x20,0x00000001).SHIFT_LEFT(new nBit(10,1)));
		assertEquals(new nBit(0x20,0x00000001),
					 new nBit(0x20,0x00000002).SHIFT_RIGHT(new nBit(10,1)));

		assertEquals(new nBit(0x20,0xE0000000),
					 new nBit(0x20,0xF0000000).SHIFT_LEFT(new nBit(10,1)));
		assertEquals(new nBit(0x20,0x78000000),
					 new nBit(0x20,0xF0000000).SHIFT_RIGHT(new nBit(10,1)));


	assertEquals(new nBit(0x40,0x0000000200000000L),
				 new nBit(0x40,0x0000000100000000L).SHIFT_LEFT(new nBit(10,1)));
	assertEquals(new nBit(0x40,0x0000000200000000L),
				 new nBit(0x40,0x0000000080000000L).SHIFT_LEFT(new nBit(10,2)));

	assertEquals(new nBit(0x40,0x0000000100000000L),
				 new nBit(0x40,0x0000000200000000L).SHIFT_RIGHT(new nBit(10,1)));

	assertEquals(new nBit(0x40,0x0000000080000000L),
				 new nBit(0x40,0x0000000200000000L).SHIFT_RIGHT(new nBit(10,2)));


	assertEquals(new nBit(0x40,0xE000000000000000L),
				 new nBit(0x40,0xF000000000000000L).SHIFT_LEFT(new nBit(10,1)));
	assertEquals(new nBit(0x40,0x7800000000000000L),
				 new nBit(0x40,0xF000000000000000L).SHIFT_RIGHT(new nBit(10,1)));


	assertEquals(new nBit(0x40,0x0000000200000000L),
				 new nBit(0x40,0x0000000100000000L).SHIFT_LEFT(new nBit(10,1)));
	assertEquals(new nBit(0x40,0x0000000200000000L),
				 new nBit(0x40,0x0000000080000000L).SHIFT_LEFT(new nBit(10,2)));


	// and now with parsing
	assertEquals(nBit.parse_nBit("0x000000010000000000000000"),
				 nBit.parse_nBit("0x000000020000000000000000").SHIFT_RIGHT(new nBit(10,1)));

	assertEquals(nBit.parse_nBit("0x000000008000000000000000"),
				 nBit.parse_nBit("0x000000020000000000000000").SHIFT_RIGHT(new nBit(10,2)));


	assertEquals(nBit.parse_nBit("0xE00000000000000000000000"),
				 nBit.parse_nBit("0xF00000000000000000000000").SHIFT_LEFT(new nBit(10,1)));
	assertEquals(nBit.parse_nBit("0x780000000000000000000000"),
				 nBit.parse_nBit("0xF00000000000000000000000").SHIFT_RIGHT(new nBit(10,1)));


	assertEquals(nBit.parse_nBit("0xAAAAAAAAA78BBBBBBB000000000000000"),
				 nBit.parse_nBit("0x00000000AAAAAAAAA78BBBBBBB0000000").SHIFT_LEFT(new nBit(10,32)));


	assertEquals(nBit.parse_nBit("0xAAAAAAAAA78BBBBBBB000000000000000").SHIFT_RIGHT(new nBit(10,32)),
				 nBit.parse_nBit("0x00000000AAAAAAAAA78BBBBBBB0000000"));


		for (int i = 0; i < 10000; i++)
		{
			long a = (long)(Math.random()* Long.MAX_VALUE);

			int b = 0;//(int)(Math.random() * 65);

			long ans1 = (b >= 0 && b < 64 ? a << b : 0);
			long ans2 = (b >= 0 && b < 64 ? a >>> b : 0);

			nBit na = new nBit(0x40, a);
			nBit nb = new nBit(0x20, b);
			nBit ans1b = na.SHIFT_LEFT (nb);
			nBit ans2b = na.SHIFT_RIGHT(nb);

//			System.out.println("a: " + a + ", b: " + b + ", a<<b: " + ans1 + ", a>>b: " + ans2);
//			System.out.println("a: " + na + ", b: " + nb + ", a<<b: " + ans1b + ", a>>b: " + ans2b);

			assertEquals(new nBit(0x40, ans1), ans1b);
			assertEquals(new nBit(0x40, ans2), ans2b);
		}
	}
}
