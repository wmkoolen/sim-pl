/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package editor;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.RepaintManager;

import namespace.ComponentTable;
import databinding.Complex;
import databinding.Component;
import editor.tool.EllipseTool;
import editor.tool.LineTool;
import editor.tool.PinTool;
import editor.tool.PolygonTool;
import editor.tool.ProbeTool;
import editor.tool.RectangleTool;
import editor.tool.Select;
import editor.tool.SubComponentTool;
import editor.tool.TextTool;
import editor.tool.Tool;
import editor.tool.WireTool;

// Smarter than your average user

/* TODO:
 * - check thoroughly with anti-aliasing enabled
 * - include right mouse button presses
 * - tests to verify that everyone renders within their getBound()s 
 */

public class Fuzzer {
	
	private static final long SEED = 1;
	
	// small enough to be fast
	// large enough to find all mistakes
	private static final int USER_WIDTH  = 300;
	private static final int USER_HEIGHT = 300;
	
	public static class RectanglePane extends JPanel {
		Rectangle r;
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.setColor(Color.green);
			
			if (r != null) ((Graphics2D) g).fill(r);
		}
		
		public void setRectangle(Rectangle r) {
			this.r = r;
			repaint();
		}
	}
	
	
	public static class BufferingComponentPanel extends ComponentPanel {
		
		BufferedImage bi = new BufferedImage(USER_WIDTH, USER_HEIGHT,BufferedImage.TYPE_INT_RGB);

		public BufferingComponentPanel(EditorDocumentContext edc, Editor editor) {
			super(edc, editor);
		}
		
		public void paintComponent(Graphics _g) {
			Graphics2D g = (Graphics2D)_g;
			Graphics2D big = bi.createGraphics();
			
			big.setClip(g.getClip());
			
			// we DON't set the transform on big, so that it always is in component pixels
			// (g's transform may actually transform to the double buffering ancestor's pixels
			// check that g's transform does nothing else
			assert g.getTransform().getType() == AffineTransform.TYPE_TRANSLATION ||
					g.getTransform().getType() == AffineTransform.TYPE_IDENTITY 
					: g.getTransform() + " is not a translation";
			
			big.setColor(g.getColor());
			big.setFont(g.getFont());
			big.setRenderingHints(g.getRenderingHints());
			big.setStroke(g.getStroke());
			big.setBackground(g.getBackground());
			
			super.paintComponent(big);
			
			Shape clip = g.getClip();
			g.setClip(null);

			g.drawImage(bi, 0, 0, null);
			big.dispose();
		}
	}
	
	
	static class UserEnvironment {
		Component c;
		EditorDocumentContext edc;
		BufferingComponentPanel bcp;
		
		Tool [] tools = {
				new Select(),
				new RectangleTool(),
				new EllipseTool(),
				new PolygonTool(),
				new LineTool(),
				new TextTool(),
				new PinTool(true),
				new PinTool(false),
				new SubComponentTool(),
				new WireTool(),
				new ProbeTool(),
		};

		public UserEnvironment(File load) throws Exception {
			if (load != null) {
				ComponentTable.clear();
				c = ComponentTable.load(load);
			} else {
				c = new Complex("FUZZ TEST FAST");
			}
			edc = new EditorDocumentContext(c, true);
			bcp = new BufferingComponentPanel(edc, null);
			bcp.setTool(tools[3]);
		}
		
		public boolean isToolSupported(int i) {
			return !(i >=5 && i <= 8) && // requires user input
					tools[i].canHandle(c);
		}
		
		public void setTool(int i) {
			assert isToolSupported(i);
			bcp.setTool(tools[i]);
		}
	}
	
	
	public static void main(String [] args) throws Exception {
		
		File load = new File("test/editor/Hard.sim-pl");
		
		final UserEnvironment fast = new UserEnvironment(load), 
							  slow = new UserEnvironment(load);
		
		JFrame f = new JFrame("Fuzz Test");
		
		JSplitPane lp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, fast.bcp, slow.bcp);
		lp.setResizeWeight(.5);
		
		final RectanglePane rp = new RectanglePane();
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, rp, lp);
		sp.setResizeWeight(1.0/3);
		
		
		// ignore mouse events
		MouseAdapter ignore = new MouseAdapter() {};
		f.getGlassPane().setVisible(true);
		f.getGlassPane().addMouseListener(ignore);
		f.getGlassPane().addMouseMotionListener(ignore);
		
		
		f.getContentPane().add(sp);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(3*USER_WIDTH+20, USER_HEIGHT+20);
		f.setVisible(true);

		// same sequence every time
		final Random det_rand = new Random(SEED);
		
		final UserEnvironment [] ues = new UserEnvironment [] {slow, fast};
		
		 Runnable mousing_user = new Runnable() {
			int mx = 250, my = 250;
			boolean down = false;
			
			public void run() {
				
				double u = det_rand.nextDouble();
				
				if (u < 0.01) {
					int tool = det_rand.nextInt(11);
					assert slow.isToolSupported(tool) == fast.isToolSupported(tool);
					if (slow.isToolSupported(tool)) {
						System.out.println("set tool " + tool);
						for (UserEnvironment ue : ues) {
							ue.setTool(tool);
						}
					}
				} else if (u < .1) {
					assert slow.edc.getUndoManager().canUndo() == fast.edc.getUndoManager().canUndo();
					assert slow.edc.getUndoManager().canRedo() == fast.edc.getUndoManager().canRedo();
					if (u < .05) {
						if (slow.edc.getUndoManager().canUndo()) {
							System.out.println("undo " + slow.edc.getUndoManager().getUndoPresentationName());
							for (UserEnvironment ue : ues) {
								ue.edc.undo(); 
								ue.bcp.doToolReset();	
							}
						}
					} else {
						if (slow.edc.getUndoManager().canRedo()) {
							System.out.println("redo " + slow.edc.getUndoManager().getRedoPresentationName());
							for (UserEnvironment ue : ues) {
								ue.edc.redo(); 
								ue.bcp.doToolReset();	
							}
						}
					}
				} else if (u < .8) {
					// mouse teleport
					mx = det_rand.nextInt(USER_WIDTH); 
					my = det_rand.nextInt(USER_HEIGHT);
					System.out.println((down ? "drag" : "move") + " to " + mx + ", " + my);
					int mod = down ? MouseEvent.BUTTON1_DOWN_MASK : 0;
					for (UserEnvironment ue : ues) {
						ue.bcp.dispatchEvent(new MouseEvent(ue.bcp, down ? MouseEvent.MOUSE_DRAGGED : MouseEvent.MOUSE_MOVED, 0, mod, mx, my, 0, 0, 0, false, 0));
					}
				} else {
					// press or release
					System.out.println(down ? "release" : "press");
					int mod = down ? MouseEvent.BUTTON1_DOWN_MASK : 0;
					for (UserEnvironment ue : ues) {
						ue.bcp.dispatchEvent(new MouseEvent(ue.bcp, down ? MouseEvent.MOUSE_RELEASED : MouseEvent.MOUSE_PRESSED, 0, mod, mx, my, 0, 0, 0, false, MouseEvent.BUTTON1));
					}
					down = !down;
				}
			
				// completely update right panel
				slow.bcp.setZoom(slow.bcp.getZoom());
				
				// show repaint region of left panel in rp panel
				rp.setRectangle(RepaintManager.currentManager(fast.bcp).getDirtyRegion(fast.bcp));
			}
		};
		
	
		// Fake a user
		for(int iter = 0; ; ++iter) {
			System.out.println("iteration: " + iter);
			EventQueue.invokeAndWait(mousing_user);
			
			

			EventQueue.invokeAndWait(new Runnable() {
				public void run() {
					// force all rendering to settle.
					for (UserEnvironment ue : ues) {
						while (!RepaintManager.currentManager(ue.bcp).getDirtyRegion(ue.bcp).isEmpty()) {
							RepaintManager.currentManager(ue.bcp).paintDirtyRegions();
				
						}
					}
					
					// Then compare two images
					int [] fast_pixels = fast.bcp.bi.getData().getPixels(0, 0, USER_WIDTH, USER_HEIGHT, new int [3*USER_WIDTH*USER_HEIGHT]);
					int [] slow_pixels = slow.bcp.bi.getData().getPixels(0, 0, USER_WIDTH, USER_HEIGHT, new int [3*USER_WIDTH*USER_HEIGHT]);
					
				
					if (!Arrays.equals(fast_pixels, slow_pixels)) {
						BufferedImage diff = new BufferedImage(USER_WIDTH, USER_HEIGHT, BufferedImage.TYPE_BYTE_BINARY);
						int [] b1 = new int[3], 
							   b2 = new int[3], 
							   b3 = new int[1];
						boolean [] bad_rows = new boolean[USER_HEIGHT],
								   bad_cols = new boolean[USER_WIDTH];
						
						for (int x = 0; x < diff.getWidth(); ++x) {
							for (int y = 0; y < diff.getWidth(); ++y) {
								fast.bcp.bi.getRaster().getPixel(x, y, b1);
								slow.bcp.bi.getRaster().getPixel(x, y, b2);
								boolean same = Arrays.equals(b1, b2); 
								b3[0] = same  ? 0 : 1;
								diff.getRaster().setPixel(x, y, b3);
								if (same) continue;
								if (false) {
									// print the difference
									System.out.println("fast has :" + b1[0] + " " + b1[1] + " " + b1[2]);
									System.out.println("slow has :" + b2[0] + " " + b2[1] + " " + b2[2]);
								}
								bad_rows[y] = true;
								bad_cols[x] = true;
							}
						}
						Color error = new Color(1, 0, 0, 0.5f);
						for (BufferedImage bi : new BufferedImage [] {fast.bcp.bi, slow.bcp.bi}) {
							Graphics2D g = bi.createGraphics();
							g.setColor(error);
							for (int x = 0; x < diff.getWidth(); ++x) {
								if (bad_cols[x]) g.drawLine(x, 0, x, USER_HEIGHT);
							}
							for (int y = 0; y < diff.getWidth(); ++y) {
								if (bad_rows[y]) g.drawLine(0, y, USER_WIDTH, y);
							}
							g.dispose();
						}
						
						System.err.println("Looks different!!!");
						JOptionPane.showMessageDialog(null,
								new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
										new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, 
												new JLabel(new ImageIcon(fast.bcp.bi)), 
												new JLabel(new ImageIcon(slow.bcp.bi))),
										new JLabel(new ImageIcon(diff))));
						System.exit(0);
					}
				}
			});
		}
	}
}
