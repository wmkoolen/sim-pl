/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package databinding.wires;

import java.util.Random;

import junit.framework.TestCase;
import databinding.Complex;
import editor.EditorDocumentContext;

/**
 * <p>Title: SIM-PL</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: WoCoWa</p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 2.0
 */
public class TestWires extends TestCase {
	public TestWires(String name) {
		super(name);
	}

	interface RandomWireOp {
		public void perform(EditorDocumentContext edc);
	}

	static class AddSubComponent implements RandomWireOp {
		public void perform(EditorDocumentContext edc) {
		}
	}

	static class RemoveSubComponent implements RandomWireOp {
		public void perform(EditorDocumentContext edc) {
		}
	}

	static class AddSpan implements RandomWireOp {
		public void perform(EditorDocumentContext edc) {
		}
	}

	static class RemoveSpan implements RandomWireOp {
		public void perform(EditorDocumentContext edc) {
		}
	}

	static class RemoveNode implements RandomWireOp {
		public void perform(EditorDocumentContext edc) {
		}
	}


	public void testRunTheMadWirer() {
		Random r = new Random(0);

		Complex c = new Complex("Mad Wires");
		EditorDocumentContext edc = new EditorDocumentContext(c, false);


		RandomWireOp [] rwo = {
			new AddSpan(),
			new AddSubComponent(),
			new RemoveNode(),
			new RemoveSpan(),
			new RemoveSubComponent()
		};

		for (int i = 0; i < 10000; i++) {
			rwo[r.nextInt(rwo.length)].perform(edc);
		}
	}
}
