/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package component;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import namespace.ComponentTable;
import databinding.Component;
import executer.Simulator;
import executer.timetable.TimeTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TestComponentParsing extends TestSuite {

	public TestComponentParsing() throws Exception {
		work(new File("archive/Components"));
	}

	public void work(final File f) throws Exception {
		if (f.isDirectory()) {
			File [] fs = f.listFiles();
			for (int i = 0; i < fs.length; i++) work(fs[i]);
			ComponentTable.clear();
		} else {
			if (f.getName().endsWith(".xml")) {
				addTest(new ParserTest(f));
			}
		}
	}
	public static Test suite() throws Exception {
	  return new TestComponentParsing();
	}
}

class ParserTest extends TestCase {
	File f;

	public ParserTest(File f) {
		super(f.getPath());
		this.f = f;
	}

	public void runBare() throws Exception {
		Component c = ComponentTable.load(f);
		Simulator s = new Simulator(c, f);
		TimeTable tt= new TimeTable(s);
		tt.init();
		tt.cycle(10);
	}
}
