/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package util.graphics;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p> </p>
 *
 * @author Wouter Koolen-Wijkstra
 * @version 1.0
 */
public class TestGraphicsFileFilters extends TestSuite {


	private static final String [] sourceFiles =
		{
		"archive/Components/TextTest/TextTest.sim-pl",
		"archive/Components/TextTest/TextTest2.sim-pl",
		"archive/Components/TextTest/TextTest3.sim-pl",
		"archive/Components/Boek/H6Complex/Multiplexing/Multiplexing.sim-pl",
		};
	private static final String [] formats = {"EPS", "JPEG", "PDF", "PNG", "SVG"};

	public static Test suite() throws Exception {
	  return new TestGraphicsFileFilters();
	}

	public TestGraphicsFileFilters() {
		for (final String sourceFile : sourceFiles) {
			for (final String format : formats) {
				final File f = new File(sourceFile);
				addTest(new TestCase(sourceFile + " as " + format) {
					public void runBare() throws Exception {
					    ImageOutputter.go(new String [] {sourceFile, "-format", format, "-output", File.createTempFile(f.getName(), "." + format).toString()});
					}
				});
			}
		}
	}
}
