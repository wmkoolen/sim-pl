package compiler.wasm;

import java.io.File;
import java.io.IOException;

import util.graphics.TestGraphicsFileFilters;

import compiler.wasm.Archs.Arch;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import log.Logger;
import log.StreamLog;


public class InstructionSetFormatterTest extends TestSuite {

	public InstructionSetFormatterTest() {
		for (Arch a : Archs.archs) {
			super.addTest(new TestFormat(a));
		}
	}
	
	public static Test suite() throws Exception {
	  return new InstructionSetFormatterTest();
	}	
	
	
	static class TestFormat extends TestCase {
		final Arch a;
		public TestFormat(Arch a) {
			super(a.component.toString());
			this.a = a;
		}
		
		public void runBare() throws Exception  {
			File outFile = File.createTempFile(a.component.getName(), ".xhtml");
			Logger logger = new Logger(new StreamLog());
			InstructionSetFormatter.main(a.wasm, a.component,  outFile, logger);
		}
		
	}
}
