/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wasm;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestSuite;
import nbit.nBit;

import compiler.ComponentProgram;
import compiler.wisent.DataSource_Direct;
import compiler.wisent.Instance_Array;
import compiler.wisent.Instance_Simple;

import databinding.HPL;
import databinding.InvalidHPLException;
import executer.Simulator;
import executer.timetable.AliasResolver;
import executer.timetable.HighlightManager;
import executer.timetable.InstanceAliasResolver;
import executer.timetable.TimeTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */


class ArchDetails {
	public final String harvardXML;
	public final HPL regComp, regStore, dataMemComp, dataMemStore, memOut;
	public final nBit haltCond;

	ArchDetails(String harvardXML, String regComp, String regStore, String dataMemComp, String dataMemStore, String memOut, nBit haltCond) throws InvalidHPLException {
		this.harvardXML = harvardXML;
		this.regComp = new HPL(regComp);
		this.regStore = new HPL(this.regComp, regStore);
		this.dataMemComp = new HPL(dataMemComp);
		this.dataMemStore = new HPL(this.dataMemComp, dataMemStore);
		this.memOut = new HPL(memOut);
		this.haltCond = haltCond;
	}
}

public class TestWASM extends TestSuite {
	
	
	final ArchDetails harvard = new ArchDetails(
			"archive/Components/16bitHarvard/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));
		
		
		
		final ArchDetails harvard2 = new ArchDetails(
			"archive/Components/16bitHarvard2/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x290000000"));
		
		final ArchDetails harvard_connect4 = new ArchDetails(
			"archive/Components/16bitHarvard_Connect4/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));
	
		
		final ArchDetails coachlab = new ArchDetails(
			"archive/Components/Coachlab/Harvard/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));
	
		
		
		final ArchDetails harvard_boek = new ArchDetails(
			"archive/Components/Boek/H8HarvardArchitecture/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));

		final ArchDetails v2h8 = new ArchDetails(
			"archive/Components/ComponentenSecEdComSyst/H8HarvardArchitecture/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0490000000"));
		
		
		final ArchDetails harvard_cache = new ArchDetails(
			"archive/Components/16bitCached/16bitCached.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.MM",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0320000000"));

		final ArchDetails v2h11 = new ArchDetails(
			"archive/Components/ComponentenSecEdComSyst/H11Caches/16bitHarvardProcessorDirectMappedCache.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.MM",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0320000000"));

		final ArchDetails v2h11set = new ArchDetails(
			"archive/Components/ComponentenSecEdComSyst/H11Caches/16bitHarvardProcessor2SetAssociativeCache.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.MM",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0320000000"));

		
		
		final ArchDetails harvard_predicated = new ArchDetails(
			"archive/Components/16bitPredicated/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x25000000000"));		
		

		final ArchDetails bus = new ArchDetails(
			"archive/Components/16bitBus/16bitBus.sim-pl",
			"16 bit bus architectuur.Registers",
			"Registers",
			"16 bit bus architectuur.Mem",
			"Memory",
			"16 bit bus architectuur.IR:Out",
			nBit.parse_nBit("0x2200"));

		final ArchDetails pipeline = new ArchDetails(
			"archive/Components/16bitPipeline/16bitHarvard.sim-pl",
			"16 bit Harvard Processor.Registers",
			"Registers",
			"16 bit Harvard Processor.Data Memory",
			"Memory",
			"16 bit Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x290000000"));
		
		
		final ArchDetails new_pipeline = new ArchDetails(
			"archive/Components/Pipelined16bitHarvardArchitecture/PipelinedHarvard.sim-pl",
			"Pipelined Harvard Processor.Registers",
			"Registers",
			"Pipelined Harvard Processor.Data Memory",
			"Memory",
			"Pipelined Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));

		final ArchDetails forwarding = new ArchDetails(
			"archive/Components/Pipelined16bitHarvardArchitectureForwarding/PipelinedProcessorwithForwarding.sim-pl",
			"Pipelined Processor with Forwarding.Registers",
			"Registers",
			"Pipelined Processor with Forwarding.Data Memory",
			"Memory",
			"Pipelined Processor with Forwarding.Instruction Memory:Data",
			nBit.parse_nBit("0x150000000"));

				
		final ArchDetails v2h9fw = new ArchDetails(
			"archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/PipelinedForwarding.sim-pl",
			"Pipelined Forwarding Processor.Registers",
			"Registers",
			"Pipelined Forwarding Processor.Data Memory",
			"Memory",
			"Pipelined Forwarding Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0490000000"));

		final ArchDetails v2h9 = new ArchDetails(
			"archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/PipelinedHarvard.sim-pl",			
			"Pipelined Harvard Processor.Registers",
			"Registers",
			"Pipelined Harvard Processor.Data Memory",
			"Memory",
			"Pipelined Harvard Processor.Instruction Memory:Data",
			nBit.parse_nBit("0x0490000000"));

		final ArchDetails v2h9bp = new ArchDetails(
				"archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/PipelinedBranchPredict.sim-pl",			
				"Pipelined Harvard Processor.Registers",
				"Registers",
				"Pipelined Harvard Processor.Data Memory",
				"Memory",
				"Pipelined Harvard Processor.Instruction Memory:Data",
				nBit.parse_nBit("0x0490000000"));
		


		final ArchDetails mips_single = new ArchDetails(
			"archive/Components/Mips/SingleCycle/Mips.sim-pl",
			"Mips Processor.Registers",
			"Registers",
			"Mips Processor.Data Memory",
			"Memory",
			"Mips Processor.Instruction Memory:Data",
			nBit.parse_nBit("0xFFFFFFFF"));

		final ArchDetails mips_multi =  new ArchDetails(
			"archive/Components/Mips/MultiCycle/MultiCycle.sim-pl",
			"MultiCycle Mips Processor.Registers",
			"Registers",
			"MultiCycle Mips Processor.Memory",
			"Memory",
			"MultiCycle Mips Processor.Instruction Memory:Address",
			nBit.parse_nBit("0x20"));

		final ArchDetails mips_pipld =  new ArchDetails(
			"archive/Components/Mips/Pipelined/Mips.sim-pl",
			"Mips Processor.Registers",
			"Registers",
			"Mips Processor.Data Memory",
			"Memory",
			"Mips Processor.Instruction Memory:Data",
			nBit.parse_nBit("0xFFFFFFFF"));

		
		final ArchDetails jumper =  new ArchDetails(
				"archive/Components/16bitJumper/16bitJumper.sim-pl",
				"16 bit Harvard with Procedure Calls.Registers",
				"Registers",
				"16 bit Harvard with Procedure Calls.Data Memory",
				"Memory",
				"16 bit Harvard with Procedure Calls.Instruction Memory:Data",
				nBit.parse_nBit("0x0320000000"));
		
		final ArchDetails regstack =  new ArchDetails(
				"archive/Components/16bitRegStack/16bitRegStack.sim-pl",
				"16 bit Register Stack Machine.Registers",
				"Registers",
				"16 bit Register Stack Machine.Data Memory",
				"Memory",
				"16 bit Register Stack Machine.Instruction Memory:Data",
				nBit.parse_nBit("0x072000FFFF"));
		
		
	

	public static junit.framework.Test suite() throws Exception {
		return new TestWASM();
	}

	public TestWASM() throws Exception {

		addTest(new EratosthenesTest("archive/Components/16bitCached/Eratosthenes.wasm", harvard_cache));
		addTest(new EratosthenesTest("archive/Components/16bitPipeline/Eratosthenes.wasm", pipeline));
		addTest(new EratosthenesTest("archive/Components/16bitPredicated/Eratosthenes.wasm", harvard_predicated));
		addTest(new EratosthenesTest("archive/Components/16bitHarvard2/Eratosthenes.wasm", harvard2));
		addTest(new EratosthenesTest("archive/Components/16bitBus/Eratosthenes.wasm", bus));
		addTest(new EratosthenesTest("archive/Components/16bitHarvard/Eratosthenes.wasm", harvard));
		addTest(new EratosthenesTest("archive/Components/Pipelined16bitHarvardArchitecture/Eratosthenes.wasm", new_pipeline));
		addTest(new EratosthenesTest("archive/Components/Pipelined16bitHarvardArchitectureForwarding/Eratosthenes.wasm", forwarding));

		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H8HarvardArchitecture/Eratosthenes.wasm", v2h8));
		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/EratosthenesForwarding.wasm", v2h9fw));
		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/Eratosthenes.wasm", v2h9));
		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/Eratosthenes.wasm", v2h9bp));
		
//		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H11Caches/Eratosthenes.wasm", v2h11));
//		addTest(new EratosthenesTest("archive/Components/ComponentenSecEdComSyst/H11Caches/Eratosthenes.wasm", v2h11set));
		

		addTest(new AdderSuite("archive/Components/Pipelined16bitHarvardArchitecture/Opdracht2.wasm", new_pipeline));
		addTest(new AdderSuite("archive/Components/Pipelined16bitHarvardArchitectureForwarding/Opdracht2.wasm", forwarding));
		addTest(new AdderSuite("archive/Components/Coachlab/Harvard/Opdracht2.wasm", coachlab));
		addTest(new AdderSuite("archive/Components/Boek/H8HarvardArchitecture/Opdracht2.wasm", harvard_boek));
		addTest(new AdderSuite("archive/Components/16bitCached/DemoLoopje.wasm", harvard_cache));
		addTest(new AdderSuite("archive/Components/16bitPipeline/DemoLoopje.wasm", pipeline)); 
		addTest(new AdderSuite("archive/Components/16bitPredicated/DemoLoopje.wasm", harvard_predicated));
		addTest(new AdderSuite("archive/Components/16bitHarvard2/DemoLoopje.wasm", harvard2));
		addTest(new AdderSuite("archive/Components/16bitHarvard/DemoLoopje.wasm", harvard));
		

		addTest(new MultiplierSuite("archive/Components/16bitCached/Vermenigvuldiger.wasm", harvard_cache));
		addTest(new MultiplierSuite("archive/Components/16bitPipeline/Vermenigvuldiger.wasm", pipeline));
		addTest(new MultiplierSuite("archive/Components/16bitPredicated/Vermenigvuldiger.wasm", harvard_predicated));
		addTest(new MultiplierSuite("archive/Components/16bitHarvard2/Vermenigvuldiger.wasm", harvard2));
		addTest(new MultiplierSuite("archive/Components/16bitHarvard_Connect4/Vermenigvuldiger.wasm", harvard_connect4)); 
		addTest(new MultiplierSuite("archive/Components/16bitBus/Vermenigvuldiger.wasm", bus));
		addTest(new MultiplierSuite("archive/Components/16bitHarvard/Vermenigvuldiger.wasm", harvard));	


		addTest(new SquaresTest("archive/Components/Mips/SingleCycle/Demo.wasm", mips_single));
		addTest(new SquaresTest("archive/Components/Mips/MultiCycle/Demo.wasm",  mips_multi));
		addTest(new SquaresTest("archive/Components/Mips/Pipelined/Demo.wasm",   mips_pipld));

		
		addTest(new GaussSumSuite("archive/Components/16bitJumper/GaussSum.wasm", jumper, 3, 1));
		addTest(new GaussSumSuite("archive/Components/16bitRegStack/GaussSum.wasm", regstack,1,1));
		addTest(new JumperSuite_QuickSort());
		addTest(new CachedSuite());

		
	}

	/** Considerably speeds up test execution, by determining exactly when
	 *  to stop simulating. This works because of the HALT isntruction.
	 */

//	public void runToHalt(TimeTable tt, int maxcycles, String toplevel) throws Exception {
//		int batch = 30;
//		nBit halt = nBit.parse_nBit("0x72000FFFF");
//		DataSource opcode = (DataSource) tt.getEffector().getInstance().resolve(new HPL(toplevel+".Instruction Memory:Data"));
//
//		int i;
//		for (i = 0; !((Instance_Simple)opcode.getValue()).getValue().equals(halt) && i < maxcycles/batch; i++) {
//			tt.cycle(batch); // small batch
//		}
//
//	}


/*
	public void testCompileRegStack() throws Exception {
		TimeTable tt = performCompilation(
			  new File("archive/Components/16bitRegStack/GaussSum.wasm"),
			  new File("archive/Components/16bitRegStack/16bitRegStack.sim-pl")
			);
		tt.cycle();

		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("16 bit Register Stack Machine.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		// secure the input(it is destroyed)
		nBit n = new nBit(((Instance_Simple) ia.getElement(1)).getValue());

		runToHalt(tt, 100, "16 bit Register Stack Machine");

		nBit res =  ((Instance_Simple) ia.getElement(1)).getValue();

		nBit one = new nBit(16,1);
		// it's a gaussian sum program, right?
		assertEquals(n.MUL(n.ADD(one)).SHIFT_RIGHT(one), res);


	}
*/



   public class GaussSumSuite extends TestSuite {

	   public GaussSumSuite(String gaussSrc, ArchDetails d, int inReg, int outReg) throws Exception {
		   super("GaussSum on " + d.harvardXML + " using " + gaussSrc);
		   TimeTable tt = ExecutionCase.performCompilation(
			  new File(gaussSrc),
			  new File(d.harvardXML)
				   );

		   for (int i = 0; i < 10; i++) {
			   addTest(new GaussSumTest(tt, i, d, inReg, outReg));
		   }
	   }
   }


   public class JumperSuite_QuickSort extends TestSuite {

	   public JumperSuite_QuickSort() throws Exception {
		   super("Jumper");
		   TimeTable tt = ExecutionCase.performCompilation(
			  new File("archive/Components/16bitJumper/QuickSort.wasm"),
			  new File("archive/Components/16bitJumper/16bitJumper.sim-pl")
			   );

		   for (int i = 1; i < 10; i++) {
			   int [] numbs = new int[i];
			   for (int j = 0; j < numbs.length; j++)
				   numbs[j] = (int)(Math.random()*255);
			   int offset = (int)(Math.random()*20);

			   addTest(new JumperTest_Quicksort(tt, numbs, offset));
		   }
	   }
   }


   public class CachedSuite extends TestSuite {

	   public CachedSuite() throws Exception {
		   super("Cached");
		   TimeTable tt = ExecutionCase.performCompilation(
			  new File("archive/Components/16bitCached/DemoLoopje.wasm"),
			  new File("archive/Components/16bitCached/16bitCached.sim-pl")
			   );

		   addTest(new CachedTest(tt));
	   }


	public class CachedTest extends ExecutionCase {
		CachedTest(TimeTable tt) {
			super(tt, "DemoLoopje");
		}

		public void runBare() throws Exception {
			tt.init();

			DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().
				resolve(new HPL("16 bit Harvard Processor.MM:Memory"));
			Instance_Array ia_mem = (Instance_Array)mem.getValue();

			// pre-compute sum
			int sum = 0;
			// get numbers from memory
			for (int i = 5; i <= 7; i++) {
				sum += ((Instance_Simple)ia_mem.getElement(i)).getValue().toInt();
			}

			runToCondition(1000,
				new HPL("16 bit Harvard Processor.Instruction Memory:Data"),
						   nBit.parse_nBit("0x0320000000"));

			assertEquals(sum, ( (Instance_Simple)ia_mem.getElement(8)).getValue().toInt());
		}
	}

   }




   public class GaussSumTest extends ExecutionCase {
	   final int n;
	   final ArchDetails d;
	   final int inReg, outReg;
	   
	   GaussSumTest(TimeTable tt, int n, ArchDetails d, int inReg, int outReg) {
		   super(tt, "n: " + n);
		   this.n = n;
		   this.inReg = inReg;
		   this.outReg = outReg;
		   this.d = d;
	   }

	   public void runBare() throws Exception {
		   tt.init();
		   tt.cycle(); // execute first instruction: load parameter into register

		   // note: we're working in pure registers here
		   DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.regStore);
		   Instance_Array ia = (Instance_Array)mem.getValue();

		   // secure the input(it is destroyed) (it is in $a0)
		   nBit v = new nBit(16, n);
		   ( (Instance_Simple)ia.getElement(inReg)).setValue(new nBit(v));

		   runToCondition(1000, d.memOut, d.haltCond);

		   // result is in $v0
		   nBit res = ( (Instance_Simple)ia.getElement(outReg)).getValue();

		   nBit one = new nBit(16, 1);
		   // it's a gaussian sum program, right?
		   assertEquals(v.MUL(v.ADD(one)).SHIFT_RIGHT(one), res);
	   }
   }

   private String join(String s, int [] is) {
	   String r = "";
	   for (int i = 0; i < is.length; i++){
		   if (i > 0) r += s;
		   r += is[i];
	   }
	   return r;
   }


   public class JumperTest_Quicksort extends ExecutionCase {
	   int [] numbers;
	   int offset;
	   JumperTest_Quicksort(TimeTable tt, int [] numbers, int offset) {
		   super(tt, "nbs: " + join(", ", numbers) + " @ " + offset);
		   this.numbers = numbers;
		   this.offset = offset;
	   }

	   public void runBare() throws Exception {
		   tt.init();
		   tt.cycle(2); // set first & second to a0, a1

		   DataSource_Direct regs = (DataSource_Direct)tt.getEffector().getInstance().
			   resolve(new HPL("16 bit Harvard with Procedure Calls.Registers:Registers"));
		   Instance_Array ia_reg = (Instance_Array)regs.getValue();

		   DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().
			   resolve(new HPL("16 bit Harvard with Procedure Calls.Data Memory:Memory"));
		   Instance_Array ia_mem = (Instance_Array)mem.getValue();

		   // write numbers to memory
		   for (int i = 0; i < numbers.length; i++) {
			( (Instance_Simple)ia_mem.getElement(offset+i)).setValue(new nBit(16, numbers[i]));
		   }

		   // write first/last address to registers a0, a1
		   ( (Instance_Simple)ia_reg.getElement(3)).setValue(new nBit(16, offset));
		   ( (Instance_Simple)ia_reg.getElement(4)).setValue(new nBit(16, offset + numbers.length-1));


		   runToCondition(1000,
			   new HPL("16 bit Harvard with Procedure Calls.Instruction Memory:Data"),
						  nBit.parse_nBit("0x0320000000"));

	       Arrays.sort(numbers);
		   for (int i = 0; i < numbers.length; i++) {
			   assertEquals(numbers[i],
							( (Instance_Simple)ia_mem.getElement(offset+i)).getValue().toInt());
		   }
	   }
   }



/*
    public int fac(int n) {
	if (n < 2) return 1;
	return n*fac(n-1);
    }


	public void testCompileRegStack_Fac() throws Exception {
		TimeTable tt = performCompilation(
			  new File("archive/Components/16bitRegStack/Factorial.wasm"),
			  new File("archive/Components/16bitRegStack/16bitRegStack.sim-pl")
			);
		tt.cycle();

		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("16 bit Register Stack Machine.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		tt.cycle(); // the first cycle puts n into register one
		// secure the input(it is destroyed)
		nBit n = new nBit(((Instance_Simple) ia.getElement(1)).getValue());

		runToHalt(tt, 1000, "16 bit Register Stack Machine");

		nBit res =  ((Instance_Simple) ia.getElement(1)).getValue();

		nBit real = new nBit(16, fac(n.toInt()));
		// it's a factorial program, right?
		assertEquals(real, res);


	}




    public int fib(int n) {
	if (n < 2) return n;
	return fib(n-1) + fib(n-2);
    }



	public void testCompileRegStack_Fib() throws Exception {
		TimeTable tt = performCompilation(
			  new File("archive/Components/16bitRegStack/Fibonacci.wasm"),
			  new File("archive/Components/16bitRegStack/16bitRegStack.sim-pl")
			);
		tt.cycle();

		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("16 bit Register Stack Machine.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		tt.cycle(); // the first cycle puts n into register one
		// secure the input(it is destroyed)
		nBit n = new nBit(((Instance_Simple) ia.getElement(1)).getValue());

		runToHalt(tt, 1000, "16 bit Register Stack Machine");

		nBit res =  ((Instance_Simple) ia.getElement(1)).getValue();

		nBit real = new nBit(16, fib(n.toInt()));
		// it's a factorial program, right?
		assertEquals(real, res);


	}



	public void testCompileJumper_Fib() throws Exception {
		TimeTable tt = performCompilation(
			  new File("archive/Components/16bitJumper/Fibonacci.wasm"),
			  new File("archive/Components/16bitJumper/16bitJumper.sim-pl")
			);
		tt.cycle();

		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("16 bit Harvard with Procedure Calls.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		tt.cycle(); // the first cycle puts n into register one
		// secure the input(it is destroyed) (it is in $a0)
		nBit n = new nBit(((Instance_Simple) ia.getElement(3)).getValue());

		runToHalt(tt, 1000,"16 bit Harvard with Procedure Calls");

		// retrieve result from $v0
		nBit res =  ((Instance_Simple) ia.getElement(1)).getValue();

		nBit real = new nBit(16, fib(n.toInt()));
		// it's a factorial program, right?
		assertEquals(real, res);
	}



	public void testCompileCalculator1() throws Exception {
		TimeTable tt = performCompilation(
			  new File("test/compiler/wasm/test1.wasm"),
			  new File("archive/Components/16bitCalculator/16bitCalculator.xml")
			);
		tt.cycle();
	}

	public void testCompileCalculator2() throws Exception {
		TimeTable tt = performCompilation(
		  new File("archive/Components/16bitCalculator/DemoprogramCal.wasm"),
		  new File("archive/Components/16bitCalculator/16bitCalculator.xml")
		);
		tt.cycle();
	}

	public void testCompileCalculator3() throws Exception {
		TimeTable tt = performCompilation(
		  new File("archive/Components/16bitCalculator/DemoMultiplication.wasm"),
		  new File("archive/Components/16bitCalculator/16bitCalculator.xml")
		);

		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("16 bit Calculator.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		// secure the input(it is destroyed)
		nBit a = new nBit(((Instance_Simple) ia.getElement(0)).getValue());
		nBit b = new nBit(((Instance_Simple) ia.getElement(1)).getValue());

		tt.cycle(24);

		nBit res =  ((Instance_Simple) ia.getElement(2)).getValue();

		// it's a multiplication program, right?
		assertEquals(a.MUL(b), res);
	}


	public void testCompileMIPS1() throws Exception {
		TimeTable tt = performCompilation(
			  new File("test/compiler/wasm/test2.wasm"),
			  new File("archive/Components/Mips/Pipelined/Mips.sim-pl")
			);
		tt.cycle();
	}

	public void testCompileMIPS2() throws Exception {
		TimeTable tt = performCompilation(
			new File("archive/Components/Mips/Pipelined/Demo.wasm"),
			new File("archive/Components/Mips/Pipelined/Mips.sim-pl")
			);
		tt.cycle(130);
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("Mips Processor.Data Memory:Memory"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		for (int i = 0; i < 10; i++) {
			// note: Pipelined mips still uses BYTE addressing
			assertEquals(new nBit(32, i*i), ( (Instance_Simple) ia.getElement(1+i)).getValue());
		}
	}


	public void testCompileVLIW() throws Exception {
		TimeTable tt = performCompilation(
			new File("archive/Components/16bitVLIW/Demo_SquareRoot.wasm"),
			new File("archive/Components/16bitVLIW/VLIW.xml")
			);
		// note: we're working in pure registers here
		DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(new HPL("VLIW Processor.Registers:Registers"));
		Instance_Array ia = (Instance_Array)mem.getValue();

		// secure the input(it is destroyed)
		nBit orig = new nBit(((Instance_Simple) ia.getElement(0)).getValue());

		tt.cycle(1050);

		nBit res =  ((Instance_Simple) ia.getElement(1)).getValue();

		// it's a square root program, right?
		assertEquals(res.MUL(res), orig);
	}

	*/
   
   
	static int resolve(InstanceAliasResolver iar, String name) {
		assert iar != null;
		for (int i = 0; ; ++i) {
			List<String> als = iar.resolveAliases(i);
			if (als != null && als.contains(name)) return i;
		}
	}
   

   
   
	public class AdderSuite extends TestSuite {

		public AdderSuite(String adderSrc, ArchDetails d) throws Exception {
			super("Adder on " + d.harvardXML + " using " + adderSrc);
			TimeTable tt = ExecutionCase.performCompilation(
				new File(adderSrc),
				new File(d.harvardXML)
				);

			for (int i = 0; i < 25; i++) {
				final short as = (short) (Math.random() * 255);
				final short bs = (short) (Math.random() * 255);
				final short cs = (short) (Math.random() * 255);

				addTest(new AdderTest(tt, as, bs, cs, d));
			}
		}
	}

	
	public class AdderTest extends ExecutionCase {
		final short as, bs, cs;
		final ArchDetails d;

		AdderTest(TimeTable tt, short as, short bs, short cs, ArchDetails d) {
			super(tt, as + " + " + bs + " + " + cs);
			this.as = as;
			this.bs = bs;
			this.cs = cs;
			this.d = d;
		}

		public void runBare() throws Exception {
			final ComponentProgram oldProgram = tt.getInitialisationProgram();

			final Simulator.Instance_Simple datamem = (Simulator.Instance_Simple)tt.getEffector().getInstance().resolve(d.dataMemComp);

			ComponentProgram newProgram = new ComponentProgram() {
				public void execute(TimeTable tt) throws Exception {
					oldProgram.execute(tt);
					InstanceAliasResolver iar = oldProgram.getAliasResolver().getAliases(datamem);
					assert iar != null : "Failed to look up " + datamem;
					DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.dataMemStore);
					Instance_Array ia = (Instance_Array)mem.getValue();
					((Instance_Simple)ia.getElement(resolve(iar, "base")+0)).setValue(new nBit(16, as));
					((Instance_Simple)ia.getElement(resolve(iar, "base")+1)).setValue(new nBit(16, bs));
					((Instance_Simple)ia.getElement(resolve(iar, "base")+2)).setValue(new nBit(16, cs));
				}

				public AliasResolver getAliasResolver() {
					return oldProgram.getAliasResolver();
				}

				public HighlightManager getHighlightManager() {
					return oldProgram.getHighlightManager();
				}
			};

			tt.setInitialisationProgram(newProgram);

			tt.init();
			InstanceAliasResolver iar = tt.getAliasResolver().getAliases(datamem);
			assert iar != null : "Failed to look up " + datamem; 
			DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.dataMemStore);
			Instance_Array ia = (Instance_Array)mem.getValue();

			runToCondition(10000, d.memOut, d.haltCond);

			// it's an addition, right?
			assertEquals(as+bs+cs, ((Instance_Simple) ia.getElement(resolve(iar,"base")+3)).getValue().toInt());
		}
	}
	
	
	



	public class MultiplierSuite extends TestSuite {

		public MultiplierSuite(String vermenigvuldigerSrc, ArchDetails d) throws Exception {
			super("Multiplier on " + d.harvardXML + " using " + vermenigvuldigerSrc);
			TimeTable tt = ExecutionCase.performCompilation(
				new File(vermenigvuldigerSrc),
				new File(d.harvardXML)
				);

			for (int i = 0; i < 25; i++) {
				final short as = (short) (Math.random() * 255);
				final short bs = (short) (Math.random() * 255);

				addTest(new MultiplierTest(tt, as, bs, d));
			}
		}
	}

	
	

	public class MultiplierTest extends ExecutionCase {
		final short as, bs;
		final ArchDetails d;

	MultiplierTest(TimeTable tt, short as, short bs, ArchDetails d) {
			super(tt, as + " x " + bs);
			this.as = as;
			this.bs = bs;
			this.d = d;
		}

		public void runBare() throws Exception {
			final ComponentProgram oldProgram = tt.getInitialisationProgram();

			final Simulator.Instance_Simple regs = (Simulator.Instance_Simple)tt.getEffector().getInstance().resolve(d.regComp);

			ComponentProgram newProgram = new ComponentProgram() {
				public void execute(TimeTable tt) throws Exception {
					oldProgram.execute(tt);
					InstanceAliasResolver iar = oldProgram.getAliasResolver().getAliases(regs);
					assert iar != null : "Failed to look up " + regs;
					DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.regStore);
					Instance_Array ia = (Instance_Array)mem.getValue();
					((Instance_Simple)ia.getElement(resolve(iar, "a")))         .setValue(new nBit(16, as));
					((Instance_Simple)ia.getElement(resolve(iar, "b")))         .setValue(new nBit(16, bs));
					((Instance_Simple)ia.getElement(resolve(iar, "nr_of_bits"))).setValue(new nBit(16, 8));
				}

				public AliasResolver getAliasResolver() {
					return oldProgram.getAliasResolver();
				}

				public HighlightManager getHighlightManager() {
					return oldProgram.getHighlightManager();
				}
			};

			tt.setInitialisationProgram(newProgram);

			tt.init();
			InstanceAliasResolver iar = tt.getAliasResolver().getAliases(regs);
			assert iar != null : "Failed to look up " + regs;
			DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.regStore);
			Instance_Array ia = (Instance_Array)mem.getValue();

			runToCondition(1000, d.memOut, d.haltCond);

			// it's a multiplication program, right?
			assertEquals(as*bs, ((Instance_Simple) ia.getElement(resolve(iar,"result"))).getValue().toInt());
		}
	}



	public class EratosthenesTest extends ExecutionCase {
		final ArchDetails d;

	EratosthenesTest(String eratosthenesSrc, ArchDetails d) throws Exception {
			super(ExecutionCase.performCompilation(
				new File(eratosthenesSrc),
				new File(d.harvardXML)
				), "Run " + eratosthenesSrc + " on " + d.harvardXML);
			this.d = d;
		}

		public void runBare() throws Exception {
			final ComponentProgram oldProgram = tt.getInitialisationProgram();

			final Simulator.Instance_Simple regs = (Simulator.Instance_Simple)tt.getEffector().getInstance().resolve(d.regComp);
			final Simulator.Instance_Simple dmem = (Simulator.Instance_Simple)tt.getEffector().getInstance().resolve(d.dataMemComp);

			ComponentProgram newProgram = new ComponentProgram() {
				public void execute(TimeTable tt) throws Exception {
					oldProgram.execute(tt);
					InstanceAliasResolver iar = oldProgram.getAliasResolver().getAliases(regs);
					assert iar != null : "Failed to look up " + regs;
					DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.regStore);
					Instance_Array ia = (Instance_Array)mem.getValue();
					((Instance_Simple)ia.getElement(resolve(iar, "max")))         .setValue(new nBit(16, 100));
					((Instance_Simple)ia.getElement(resolve(iar, "maxstr")))      .setValue(new nBit(16, 10));
				}

				public AliasResolver getAliasResolver() {
					return oldProgram.getAliasResolver();
				}

				public HighlightManager getHighlightManager() {
					return oldProgram.getHighlightManager();
				}
			};

			tt.setInitialisationProgram(newProgram);

			tt.init();
			InstanceAliasResolver iar = tt.getAliasResolver().getAliases(dmem);
			assert iar != null : "Failed to look up " + dmem;
			DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.dataMemStore);
			Instance_Array ia = (Instance_Array)mem.getValue();

			runToCondition(10000, d.memOut, d.haltCond);

			// it's an Eratosthenes program, right?
			int [] primes = {2,3,5,7,11,97};
			int [] nprimes = {4,6,9,99};
			for (int i : primes) {
				assertEquals(i + " is prime", 1, ((Instance_Simple) ia.getElement(resolve(iar,"space")+i)).getValue().toInt());
			}
			for (int i : nprimes) {
				assertEquals(i + " is not prime", 0, ((Instance_Simple) ia.getElement(resolve(iar,"space")+i)).getValue().toInt());
			}
		}
	}





	public class SquaresTest extends ExecutionCase {
		final ArchDetails d;

	SquaresTest(String programSrc, ArchDetails d) throws Exception {
			super(ExecutionCase.performCompilation(
				new File(programSrc),
				new File(d.harvardXML)
				), "Run " + programSrc + " on " + d.harvardXML);
			this.d = d;
		}

		public void runBare() throws Exception {
			final Simulator.Instance_Simple dmem = (Simulator.Instance_Simple)tt.getEffector().getInstance().resolve(d.dataMemComp);

			tt.init();
			InstanceAliasResolver iar = tt.getAliasResolver().getAliases(dmem);
			assert iar != null : "Failed to look up " + dmem;
			DataSource_Direct mem = (DataSource_Direct)tt.getEffector().getInstance().resolve(d.dataMemStore);
			Instance_Array ia = (Instance_Array)mem.getValue();

			tt.cycle(1000); // Fails if this is not enough cycles

			// it's an Squares program, right?
			for (int i = 0; i < 10 ; ++i) {
				assertEquals(i + " squared is " + i*i, i*i, ((Instance_Simple) ia.getElement(resolve(iar,"result")+1+i)).getValue().toInt());
			}
		}
	}


/*

	public void testCompileOK() throws Exception {
		File componentFile = new File("archive/Components/16bitCalculator/16bitCalculator.xml");
		File headerFile = new File("archive/Components/16bitCalculator/16bitCalculator.wasm");
		File includeOK = new File("test/compiler/wasm/includeOK.wasm");

		Component c = ComponentTable.load(componentFile);
		Effector e = new Simulator(c, componentFile);

		String source =
			"@include \"" + headerFile.toString() + "\" \n" +
			"@include \"" + includeOK + "\" \n" +
			" .code MyCode : CALCULATOR, REGISTERS\n" +
			" ADD $0, $1, $2 \n";
		ComponentProgram cp = wASM.compile(source, new File("."), e.getInstance(), new Logger(new StreamLog()));

		TimeTable tt = new TimeTable();
		tt.setEffector(e);
		tt.init();

		HighlightManager hlm = cp.getHighlightManager();
		assertNotNull(hlm);

		hlm.recomputeHighlights(tt);
		Collection hlts = hlm.getHighlights();

		assertEquals(1, hlts.size());

		Highlight h = (Highlight)hlts.iterator().next();

		assertNull(h.ltr.file);
	}


	public void testCompileFailInText() throws Exception {
		File componentFile = new File("archive/Components/16bitCalculator/16bitCalculator.xml");
		File headerFile = new File("archive/Components/16bitCalculator/16bitCalculator.wasm");
		File includeOK = new File("test/compiler/wasm/includeOK.wasm");

		Component c = ComponentTable.load(componentFile);
		Effector e = new Simulator(c, componentFile);

		String source =
			"@include \"" + headerFile.toString() + "\" \n" +
			"@include \"" + includeOK + "\" \n" +
			" bogus";

		try {
			ComponentProgram cp = wASM.compile(source, new File("."), e.getInstance(), new Logger(new StreamLog()));
			fail("Compiler should have choked");
		} catch (Exception ex) {
			assertEquals(ComponentCompiler.INPUT_TEXT_NAME + ":3:2: expecting EOF, found 'bogus'", ex.toString());
		}
	}

	public void testCompileFailInInclude() throws Exception {
		File componentFile = new File("archive/Components/16bitCalculator/16bitCalculator.xml");
		File headerFile = new File("archive/Components/16bitCalculator/16bitCalculator.wasm");
		File includeFail = new File("test/compiler/wasm/includeFail.wasm");

		Component c = ComponentTable.load(componentFile);
		Effector e = new Simulator(c, componentFile);

		String source =
			"@include \"" + headerFile.toString() + "\" \n" +
			"@include \"" + includeFail + "\" \n" +
			" flogus";

		try {
			ComponentProgram cp = wASM.compile(source, new File("."), e.getInstance(), new Logger(new StreamLog()));
			fail("Compiler should have choked");
		} catch (Exception ex) {
			assertEquals(new File(new File("."), includeFail.toString()).toString() + ":1:1: expecting EOF, found 'bogus'", ex.toString());
		}
	}
	*/
}
