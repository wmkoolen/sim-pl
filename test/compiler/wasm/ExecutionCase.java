package compiler.wasm;

import java.io.File;

import antlr.RecognitionException;
import junit.framework.TestCase;
import log.Log;
import log.Logger;
import log.StreamLog;
import namespace.ComponentTable;
import nbit.nBit;

import compiler.ComponentProgram;
import compiler.wisent.DataSource;
import compiler.wisent.Instance_Simple;

import databinding.Component;
import databinding.HPL;
import executer.Effector;
import executer.Simulator;
import executer.timetable.TimeTable;

abstract class ExecutionCase extends TestCase {
	
	final TimeTable tt;

	ExecutionCase(TimeTable tt, String descr) {
		super(descr);
		this.tt = tt;
	}


	public void runToCondition(int maxcycles, HPL hpl,
							   nBit halt) throws Exception {
		// non-pipelined architectures simply freeze on HALT, but
		// pipelined architectures tend to oscillate
		// to ensure that we detect HALT (albeit somewhat later), the number
		// below must be prime relative to the period of oscillation
		int batch = 31;
		DataSource opcode = (DataSource)tt.getEffector().getInstance().
			resolve(hpl);

		for (int i = 0; i < maxcycles; i+=batch) {
			if (((Instance_Simple)opcode.getValue()).getValue().equals(halt)) {
				System.out.println("Condition reached after: <=" + i + " cycles");
				return;
			}
			tt.cycle(batch); // small batch
		}
		throw new Exception("HALT condition not reached after " + maxcycles + " cycles\nValue is " + ((Instance_Simple)opcode.getValue()).getValue() + "\nLooking for " + halt);
	}


	public static TimeTable performCompilation(File sourceFile, File componentFile) throws Exception {
		Component c = ComponentTable.load(componentFile);
		Effector effector = new Simulator(c, null);
	
		Log log = new StreamLog();
		Logger logger = new Logger(log);
	
	    logger.addLine("Compiling " + sourceFile);
	
		ComponentProgram p;
		try {
			p = new WASM().compile(sourceFile, effector.getInstance(), logger);
		} catch (RecognitionException ex) {
			logger.addParagraph(ex.toString()); // toString includes position information in ANTLR errors
			throw ex;
		} catch (Exception ex) {
			logger.addParagraph(ex.getMessage());
			throw ex;
		}
	
		TimeTable tt = new TimeTable(effector);
		logger.addDoing("Executing");
		try {
			tt.setInitialisationProgram(p);
			tt.init();
			logger.addSuccess();
		} catch (Exception ex) {
			throw logger.addFailure(ex);
		}
		return tt;
	}

}