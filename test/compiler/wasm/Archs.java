package compiler.wasm;

import java.io.File;

public class Archs {
	
	static class Arch {
		public final File component;
		public final File wasm;
		
		public Arch(File component, File wasm) {
			this.component = component;
			this.wasm = wasm;
			assert(this.component.exists());
			assert(this.wasm.exists());
		}
		
		public Arch(String stem) {
			File try1 = new File(stem + ".sim-pl");
			if (!try1.exists()) try1 = new File(stem + ".xml");
			this.component = try1;
			this.wasm = new File(stem + ".wasm");
			assert this.component.exists();
			assert this.wasm.exists();
		}
		
		public Arch(File directory, String component, String wasm) {
			this.component = new File(directory, component);
			this.wasm = new File(directory, wasm);
			assert(this.component.exists());
			assert(this.wasm.exists());
		}
		
		
	};
	
	public static Arch [] archs = {
		new Arch("archive/Components/Mips/MultiCycle/MultiCycle"),
		new Arch("archive/Components/Rekenmachine_I/Rekenmachine_I"),
		new Arch("archive/Components/Rekenmachine_II/Rekenmachine_II"),
		new Arch("archive/Components/Rekenmachine_II/Rekenmachine_II"),
		new Arch("archive/Components/Rekenmachine/Rekenmachine"),
		new Arch("archive/Components/RekenmachineCLOCK/Rekenmachine"),
		new Arch("archive/Components/Pipelined16bitHarvardArchitecture/PipelinedHarvard"),
		new Arch(new File("archive/Components/Pipelined16bitHarvardArchitectureForwarding"), "PipelinedProcessorwithForwarding.sim-pl", "PipelinedHarvard.wasm"),

		// boek editie 1
		new Arch("archive/Components/Boek/H8HarvardArchitecture/16bitHarvard"),
		new Arch("archive/Components/Boek/Rekenmachine_I/Rekenmachine_I"),
		new Arch("archive/Components/Boek/Rekenmachine_II/Rekenmachine_II"),
		new Arch(new File("archive/Components/Boek/H9ProcedureCalls"), "ProcedureCalls.sim-pl", "16bitJumper.wasm"),
		new Arch("archive/Components/Boek/H7CalculatorWithLoops/CalculatorWithLoops"),
		
		// boek editie 2
		new Arch("archive/Components/ComponentenSecEdComSyst/H3Rekenmachines/Rekenmachine_I"),
		new Arch("archive/Components/ComponentenSecEdComSyst/H3Rekenmachines/Rekenmachine_II"),
		new Arch("archive/Components/ComponentenSecEdComSyst/H7CalculatorWithLoops/CalculatorWithLoops"),
		new Arch("archive/Components/ComponentenSecEdComSyst/H8HarvardArchitecture/16bitHarvard"),
		new Arch("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/PipelinedHarvard"),
		new Arch("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture/PipelinedForwarding"),
		new Arch(new File("archive/Components/ComponentenSecEdComSyst/H9PipelinedHarvardArchitecture"), "PipelinedBranchPredict.sim-pl", "PipelinedHarvard.wasm"),
		new Arch(new File("archive/Components/ComponentenSecEdComSyst/H10ProcedureCalls"), "ProcedureCalls.sim-pl", "16bitJumper.wasm"),		
		new Arch(new File("archive/Components/ComponentenSecEdComSyst/H11Caches"), "16bitHarvardProcessorDirectMappedCache.sim-pl", "16bitCached.wasm"),
		new Arch(new File("archive/Components/ComponentenSecEdComSyst/H11Caches"), "16bitHarvardProcessor2SetAssociativeCache.sim-pl",  "16bitCached.wasm"),
		
		new Arch("archive/Components/16bitCached/16bitCached"),
		new Arch("archive/Components/Coachlab/CWL/CalculatorWithLoops"),
		new Arch("archive/Components/Coachlab/Harvard/16bitHarvard"),
		new Arch("archive/Components/16bitJumper/16bitJumper"),
		new Arch("archive/Components/16bitRegStack/16bitRegStack"),
		new Arch("archive/Components/16bitPipeline/16bitHarvard"),
		new Arch("archive/Components/16bitPredicated/16bitHarvard"),
		new Arch("archive/Components/16bitHarvard2/16bitHarvard"),
		new Arch("archive/Components/16bitHarvard_Connect4/16bitHarvard"),
		new Arch("archive/Components/16bitBus/16bitBus"),
		new Arch("archive/Components/Mips/SingleCycle/Mips"),
		new Arch("archive/Components/Mips/MultiCycle/MultiCycle"),
		new Arch("archive/Components/Mips/Pipelined/Mips"),
		new Arch("archive/Components/Harvards/16bitHarvard"),
		new Arch("archive/Components/PIC10F20X/PIC10F206"),
		new Arch("archive/Components/16bitHarvard/16bitHarvard"),	
		};		
}
