package compiler.wasm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import util.FileSlurper;

import compiler.wasm.Archs.Arch;

import junit.framework.TestSuite;
import namespace.ComponentTable;


public class TestCompileAndRun extends TestSuite {

	public static junit.framework.Test suite() throws Exception {
		return new TestCompileAndRun();
	}
	
	
	// we map path strings and WASM filenames to a list of their SIM-PL components
	// (we may have several architectures in one folder sharing the same .wasm)
	static Map<String, Map<String, List<File>>> dir2wasm2component = new HashMap<String, Map<String, List<File>>>();
	static Set<String> blacklist = new HashSet<String>();
	
	static {
		for (Arch f : Archs.archs) {
			assert f.component.getParent().equals(f.wasm.getParent());
			String key = f.component.getParent();
			Map<String,List<File>> m = dir2wasm2component.get(key);
			if (m==null) dir2wasm2component.put(key, m = new HashMap<String,List<File>>());
			List<File> l = m.get(f.wasm.getName());
			if (l==null) m.put(f.wasm.getName(), l = new ArrayList<File>());
			l.add(f.component);
		}
		
		blacklist.add("archive/Components/Boek/H9ProcedureCalls/register_constants.wasm");
		blacklist.add("archive/Components/Boek/H9ProcedureCalls/register_constantsLeaf.wasm");
		blacklist.add("archive/Components/ComponentenSecEdComSyst/H10ProcedureCalls/register_constants.wasm");
		blacklist.add("archive/Components/ComponentenSecEdComSyst/H10ProcedureCalls/register_constantsLeaf.wasm");
		blacklist.add("archive/Components/16bitCached/memconstants.wasm");
		blacklist.add("archive/Components/16bitJumper/register_constants.wasm");
		blacklist.add("archive/Components/ComponentenSecEdComSyst/H11Caches/Eratosthenes_all.wasm");
	}
	
	
	public void work(final File f) throws Exception {
		if (f.isDirectory()) {
			File [] fs = f.listFiles();
			for (int i = 0; i < fs.length; i++) work(fs[i]);
			ComponentTable.clear();
		} else {
			if (!f.getName().endsWith(".wasm")) return;
			if (blacklist.contains(f.getPath())) return;
			Map<String, List<File>> m = dir2wasm2component.get(f.getParent());
			if (m==null) throw new Exception("No architecture record for " + f.getParent());
			
			// skip architecture definition WASM files for SIM-PL components in this directory
			if (m.containsKey(f.getName())) return;
			
			// puzzle out the @include
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			try {
				do { 
					line = br.readLine().trim();
					int comment = line.indexOf('#');
					if (comment != -1) line = line.substring(0, comment).trim();
				} while(line.isEmpty());
			} finally {
				br.close();
			}
			
			if (!line.startsWith("@include \"") || !line.endsWith("\""))
				throw new Error("Can not figure out @include line for " + f.getPath() +"\n" + "I got " + line);
			
			String include = line.substring("@include \"".length(), line.length()-1);
			List<File> componentFiles = m.get(include);
			if (componentFiles == null) throw new Exception("No architecture record for " + new File(f.getParentFile(), include).getPath());
			for (File componentFile : componentFiles) {
				addTest(new SimpleCompileRunTest(f, componentFile, 1000));
			}
		}
	}
	


	
	public TestCompileAndRun() throws Exception {
		work(new File("archive/Components"));
	}
	
	
	
	public class SimpleCompileRunTest extends ExecutionCase {
		final int ncycles;

		SimpleCompileRunTest(File program, File component, int ncycles) throws Exception {
			super(
				 performCompilation(program, component),
				 "Run " + program + " on " + component + " for " + ncycles + " cycles");

			this.ncycles = ncycles;
		}

		public void runBare() throws Exception {
			tt.init();
			tt.cycle(ncycles);
		}
	}

}


