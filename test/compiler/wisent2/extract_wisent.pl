#!/usr/bin/perl -w

##########################################################################
#  SIM-PL 2                                                              #
#  Digital Component Discrete Event Simulator                            #
#  Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                        #
#                                                                        #
#                                                                        #
#  This file is part of SIM-PL.                                          #
#                                                                        #
#                                                                        #
#  SIM-PL is free software; you can redistribute it and/or modify        #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation; either version 2 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  SIM-PL is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#                                                                        #
#  You should have received a copy of the GNU General Public License     #
#  along with SIM-PL; if not, write to the Free Software                 #
#  Foundation, Inc., 51 Franklin St, Fifth Floor,                        #
#  Boston, MA  02110-1301  USA                                           #
#                                                                        #
#                                                                        #
#  Contact:                                                              #
#  Wouter Koolen-Wijkstra                                                #
#  wmkoolen@gmail.com                                               #
#                                                                        #
##########################################################################


use strict;

my $dir = "../../../Components";

my @files = `cd $dir; grep ACTION * -R --exclude-dir=.svn -l`;

print "{\n";

for my $f (sort @files) {
    chomp $f;
    open my $fh, "<", "$dir/$f" or die $!;
    local $/; # enable localized slurp mode
    my $content = <$fh>;
    close $fh;


    $content =~ s/&(lt|#60);/</g;
    $content =~ s/&(gt|#62);/>/g;
    $content =~ s/&(amp|#38);/&/g;
    $content =~ s/&#9;/\t/g;
    $content =~ s/&#10;/\n/g;

    print "/***** begin file $f **************/\n\n";

    print "{\n";

    my %params;

    while ($content =~ /<PARAMETERDECL\s+([^>]*)>/sg) {
	my $param = $1;
	$param =~ /NAME\s*=\s*"([^"]*)"/ or die "no name";
	my $name = $1;
	$param =~ /DEFAULT\s*=\s*"([^"]*)"/ or die "no default";
	my $default = $1;
	$params{$name}=$default;
    }


    while ($content =~ /<(INPUT|OUTPUT)\s+([^>]*)>/sg) {
	my $pin = $2;
	$pin =~ /NAME\s*=\s*"([^"]*)"/ or die "no name";
	my $name = $1;
	$pin =~ /BITS\s*=\s*"([^"]*)"/ or die "no bits";
	my $bits = $1;

	$bits = $params{$1} if $bits =~ /\${(.*)}/;

	print "$bits bit $name;\n";
    }


    while ($content =~ /<STORAGE\s+([^>]*)>/sg) {
	my $mem = $1;
	$mem =~ /NAME\s*=\s*"([^"]*)"/ or die "no name";
	my $name = $1;

	$mem =~ /SIZE\s*=\s*"([^"]*)"/ or die "no size";
	my $size = $1;
	$size = $params{$1} if $size =~ /\${(.*)}/;

	$mem =~ /BITS\s*=\s*"([^"]*)"/ or die "no bits";
	my $bits = $1;
	$bits = $params{$1} if $bits =~ /\${(.*)}/;

	print "[$size] $bits bit $name;\n";
    }




    while ($content =~ /<ACTION[^>]*>(.*?)<\/ACTION>/sg) {
	print "{\n",  $1, "\n}\n";
    }

    print "}\n";

    print "\n/***** end file $f **************/\n\n\n\n\n\n";
}

print "}\n";
