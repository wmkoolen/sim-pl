/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent2;


import java.io.IOException;
import java.util.Random;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import compiler.LocatedException;
import compiler.wisent.Expression;
import compiler.wisent.Statement;
import compiler.wisent.Statement_Block;
import compiler.wisent.Type_Struct;
import compiler.wisent.Wisent;
import compiler.wisent.TestWisent.ExpStr;
import compiler.wisent.TestWisent.TypeStr;
import compiler.wisent.Type;

public class TestWisent extends TestSuite {
	
	public TestWisent() throws Exception {
		addTest(new TestCase("garbage") {
			public void runBare() throws RecognitionException, IOException, LocatedException {
				
				// a HUGE hunk of Wisent code, obtained by
				// extracting the code from all component files. 
				String codeFile = "test/compiler/wisent2/garbage";
				WisentLexer lex = new WisentLexer(new ANTLRFileStream(codeFile));
				CommonTokenStream tokens = new CommonTokenStream(lex);
				WisentParser prs = new WisentParser(tokens);
				
				Statement [] stms = prs.translation_unit();
				assertEquals(0, prs.getNumberOfSyntaxErrors());
				
				Statement stms2 = Wisent.compiler.parse(new Type_Struct(), util.FileSlurper.slurp(codeFile));
				
				assertEquals(stms[0].toString(), stms2.toString());
			}
		});
		
		Random rnd = new Random(0);
		for (int i = 0; i < 0; ++i) {
			final ExpStr expr = compiler.wisent.TestWisent.ExpStr.getDogFood(rnd, i);
			addTest(new TestCase(expr.s) {
			
				public void runBare() throws Exception {
					WisentLexer lex = new WisentLexer(new ANTLRStringStream(expr.s));
					CommonTokenStream tokens = new CommonTokenStream(lex);
					WisentParser prs = new WisentParser(tokens);
					
					Expression e = prs.expression();
					assertEquals(expr.e.toString(), e.toString()); // string level comparison, Expression does not have equals(...)
				}
			});
		}
		
		
		for (int i = 0; i < 100; ++i) {
			final TypeStr type = compiler.wisent.TestWisent.TypeStr.getDogFood(rnd, 1+i);
			addTest(new TestCase(type.s) {
			
				public void runBare() throws Exception {
					WisentLexer lex = new WisentLexer(new ANTLRStringStream(type.s));
					CommonTokenStream tokens = new CommonTokenStream(lex);
					WisentParser prs = new WisentParser(tokens);
					
					Type t = prs.type_specifier();
					assertEquals(type.t.toString(), t.toString()); // string level comparison, Type does not have equals(...)
				}
			});
		}
		
	}
	
	public static Test suite() throws Exception {
		return new TestWisent();
	}

	

}
