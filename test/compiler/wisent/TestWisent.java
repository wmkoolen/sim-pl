/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**\
*   SIM-PL 2                                                               *
*   Digital Component Discrete Event Simulator                             *
*   Copyright (C) 2002-2012 Wouter Koolen-Wijkstra                         *
*                                                                          *
*                                                                          *
*   This file is part of SIM-PL.                                           *
*                                                                          *
*                                                                          *
*   SIM-PL is free software; you can redistribute it and/or modify         *
*   it under the terms of the GNU General Public License as published by   *
*   the Free Software Foundation; either version 2 of the License, or      *
*   (at your option) any later version.                                    *
*                                                                          *
*   SIM-PL is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU General Public License for more details.                           *
*                                                                          *
*   You should have received a copy of the GNU General Public License      *
*   along with SIM-PL; if not, write to the Free Software                  *
*   Foundation, Inc., 51 Franklin St, Fifth Floor,                         *
*   Boston, MA  02110-1301  USA                                            *
*                                                                          *
*                                                                          *
*   Contact:                                                               *
*   Wouter Koolen-Wijkstra                                                 *
*   wmkoolen@gmail.com                                                *
*                                                                          *
\**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


package compiler.wisent;

import java.util.Random;

import junit.framework.TestCase;

import compiler.wisent.Type.Access;

public class TestWisent extends TestCase {

	Type_Struct namespace;
	
	protected void setUp() throws Exception {
		super.setUp();
		namespace = new Type_Struct();
		Type_Simple tsimple = new Type_Simple(4, false);
		Access       simple = new Access(true, true, tsimple);
		
		namespace.set("a", simple);
		namespace.set("b", simple);
		namespace.set("c", simple);
	}
	
	// multiplication binds stronger than addition
	public void testMul() throws Exception {
		// test parsing of operator precedence
		String code = "a+b*c";
		Expression e = Wisent.compiler.parseExpression(namespace, code);
		assertEquals("(a + (b * c))", e.toString());
	}
	
	
	// equal precedence -> bind in input order 
	public void testSeq() throws Exception {
		String code = "a/b/c";
		Expression e = Wisent.compiler.parseExpression(namespace, code);
		assertEquals("((a / b) / c)", e.toString());
	}

	// postfix binds stronger
	public void testPrePost() throws Exception {
		String code = "++a++";
		Expression e = Wisent.compiler.parseExpression(namespace, code);
		assertEquals("(++ (a ++))", e.toString());
	}

	// declarations are expressions
	public void testDeclaration() throws Exception {
		String code = "1 bit d + 1 bit e + 3 bit f";
		Expression e = Wisent.compiler.parseExpression(namespace, code);
//		assertEquals("(++ (a ++))", e.toString());
	}

	
	// eat our own dog food
	public void testExpressions() throws Exception {
		Random rnd = new Random(0);
		for (int i = 0; i < 500; ++i) {
			ExpStr in = ExpStr.getDogFood(rnd, i);
			Expression out = Wisent.compiler.parseExpression(namespace, in.s);
			assertEquals(in.e.toString(), out.toString());
		}
	}

	
	// eat our own dog food
	public void testTypes() throws Exception {
		Random rnd = new Random(0);
		for (int i = 0; i < 100; ++i) {
			TypeStr in = TypeStr.getDogFood(rnd, 1+i);
			Type out = Type.parse(Wisent.compiler.tokenize(in.s));
			assertEquals(in.t.toString(), out.toString());
		}
	}
	
	// eat our own dog food
	public void testDecimalDefault() throws Exception {
		Type out = Type.parse(Wisent.compiler.tokenize("unsigned 10 bit"));
		assertTrue(out instanceof Type_Simple);
		Type_Simple t = (Type_Simple)out; 
		assertEquals(t.bits, 10);
		assertEquals(t.signed, false);
	}	
	
	
	
	public static class ExpStr {
		public final Expression e;
		public final String s;
		public final int precedence;
		
		ExpStr(Expression e, String s, int precedence) {
			assert(e != null);
			assert(s != null);
			this.e = e;
			this.s = s;
			this.precedence = precedence;
		}
	
		
		public static ExpStr getDogFood(Random rnd, int length) throws Exception {		
			
			if (length == 0) {
				return  (rnd.nextDouble() < .5) 
						? new ExpStr(new Number("0"), "0", 0) 
						: new ExpStr(new Identifier("a"), "a", 0);
			}
			
			if (rnd.nextDouble() < .7) {
				
				int left = rnd.nextInt(length);
				int right = length-1-left;
				assert 0 <= left  && left  < length;
				assert 0 <= right && right < length;
				
				// binary
				ExpStr e1 = getDogFood(rnd, left);
				ExpStr e2 = getDogFood(rnd, right);
				
				Operator_Infix [] os = {
						OperatorSymbol_And_Bin.infix,
						OperatorSymbol_And_Bit.infix,
						OperatorSymbol_Divide.infix,
						OperatorSymbol_Divide_Signed.infix,
						OperatorSymbol_Equals.infix,
						OperatorSymbol_Larger.infix,
						OperatorSymbol_LargerEqual.infix,
						OperatorSymbol_Multiply.infix,
						OperatorSymbol_Multiply_Signed.infix,
						OperatorSymbol_Nequals.infix,
						OperatorSymbol_Or_Bin.infix,
						OperatorSymbol_Or_Bit.infix,
						OperatorSymbol_Plus.infix,
						OperatorSymbol_Remainder.infix,
						OperatorSymbol_Remainder_Signed.infix,
						OperatorSymbol_ShiftLeft.infix,
						OperatorSymbol_ShiftRight.infix,
						OperatorSymbol_Smaller.infix,
						OperatorSymbol_SmallerEqual.infix,
						OperatorSymbol_Xor_Bit.infix,					
						};
				
				Operator_Infix o = os[rnd.nextInt(os.length)];
				
				boolean brl = o.asso == Operator_Infix.YFX 
						? o.precedence <  e1.precedence 
						: o.precedence <= e1.precedence;
				boolean brr = o.asso == Operator_Infix.XFY 
						? o.precedence <  e2.precedence 
						: o.precedence <= e2.precedence;
				
				return new ExpStr(new Expression_Operator_Infix(e1.e, o, e2.e),
							(brl ? "(" + e1.s + ")" : e1.s)
							+ " " + o + " " +
							(brr ? "(" + e2.s + ")" : e2.s),
							o.precedence);
			} else {
				// unary
				ExpStr e = getDogFood(rnd, length-1);
				
				if (rnd.nextDouble() < 1) {
					Operator_Prefix os[] = {
							OperatorSymbol_Minus.prefix,
							OperatorSymbol_Not_Bin.prefix,
							OperatorSymbol_Not_Bit.prefix,
							OperatorSymbol_Plus.prefix,
	//						OperatorSymbol_MinMin.prefix,
	//						OperatorSymbol_PlusPlus.prefix,						
							};
					
					Operator_Prefix o = os[rnd.nextInt(os.length)];
					
					boolean br = o.asso == Operator_Prefix.FY
							? o.precedence <  e.precedence 
							: o.precedence <= e.precedence;				
					
					return new ExpStr(new Expression_Operator_Prefix(o, e.e),
							o + " " +
						    (br ? "(" + e.s + ")" : e.s),
							o.precedence);
				} else {
					Operator_Postfix os[] = {
							OperatorSymbol_MinMin.postfix,
							OperatorSymbol_PlusPlus.postfix,
							};
					Operator_Postfix o = os[rnd.nextInt(os.length)];
					
					boolean br = o.asso == Operator_Postfix.YF
							? o.precedence <  e.precedence 
							: o.precedence <= e.precedence;				
					
					
					return new ExpStr(new Expression_Operator_Postfix(e.e, o),
							(br ? "(" + e.s + ")" : e.s) + " " + o,
							o.precedence);						
				}
			}
		}
	}	

	
	public static class TypeStr {
		public final Type t;
		public final String s;

		
		TypeStr(Type t, String s) {
			assert(t != null);
			assert(s != null);
			this.t = t;
			this.s = s;
		}
	
		
		public static TypeStr getDogFood(Random rnd, int length) throws Exception {
			
			assert length > 0;
			
			if (length == 1) {
				Type_Simple t = new Type_Simple(1+rnd.nextInt(128), rnd.nextBoolean());
				return  new TypeStr(t, t.toString());
			}
			
			if (rnd.nextDouble() < .5) {
				TypeStr ts = getDogFood(rnd, length-1);
				Type_Array t = new Type_Array(rnd.nextInt(10), ts.t);
				return new TypeStr(t, t.toString());
			} else {
				Type_Struct t = new Type_Struct();
				for(int i = 0; length != 0; ++i) {
					int cur = 1+rnd.nextInt(length);
					length -= cur;
					String id = "id" + i;
					TypeStr ts = getDogFood(rnd, cur);
					Access a = new Access(rnd.nextBoolean(),  rnd.nextBoolean(),  ts.t);
					t.set(id, a);
				}
				return new TypeStr(t, t.toString());
			}
		}
	}	
	
	
	
}
