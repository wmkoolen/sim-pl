there are two stages:
- compilation 
- instantiation


Compilation is performed 
  a) during the edit cycle
  b) before instantiation

Instantiation is performed just before simulation. It is required for each (nested) instantiation of each component. O.t.o.h: compilation is only necessary for each different 'parametercontext'. This can be at most 1-1, but also substantially less often.


In principle, instantiation should only use entities that have passed compilation. That is
- unique pins/memories
- pins/momories which parameters resolve correctly