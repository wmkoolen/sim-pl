- probes can not exist without wires
- probes are placed on wires
- probes are deleted when the wire they are placed on disappears
- probes move with the wire they are placed on



1 probes as nodes
  PRO: easy
  CON: probes can exist without wires
  CON: need conversion between probes and splits or points


2 probes as pairs (wire ref, position)
  PRO: nice and general
  CON: probe is not tied to wire (solution: show connecting thread)


3 probes as pairs (wire ref, length)
  PRO: probe always on wire
  CON: hard to keep track of lengths


4 probes as pairs (node ref, position)
  PRO: nice and general (every wire has at least a node)
  PRO: probes can not exists without node (yeah)
  CON: some work needs to be done to keep track of probes
  PRO: easy to build on top of current situation



---------
We choose option 4