#!/bin/bash -e


ARG=$1 # only viable one is -r to remove licenses

function die {
    echo "ERROR"
}

trap die ERR;

LICENSE=aux/LICENSE_NOTICE
#CONFIG=aux/headache.config
SOURCES=`find src test -type f | egrep -v '(\.svn|(\.(gif|jpg|svg|jpeg|png|xcf|dot|eps|wasm|bi|xml)|~)$|garbage$)'`;

./scripts/licenseify.pl $LICENSE $SOURCES

