#!/usr/bin/perl -w

use strict;

# name, export super, image, [exposed props]

my $class = shift;
my $file = shift;

#my $file = "../src/sim-pl/databinding/Component.bi";

my $data;
{local $/; open FH, $file; $data = <FH>; close FH;}

#print STDERR $data, "\n";

my @dp = split "\n", $data;

my @bi = ($class,
	  @dp[0..1], 
	  [map {[split(/\s*,\s*/, $_)]}  @dp[2..@dp-1]]);



# my @bis = (["databinding.Component",
# 	    1,
# 	    "null",
# 	    [["name", "getName", "setName"],
# 	     ["preferredCycleLength"],
# 	     ["preferredNumberFormat"]
# 	     ]
# 	    ]
# 	   );
	   
         
export(@bi);


sub export {
    my ($cname, $super, $image, $props) = @_;
    my $bi = shift;

    my @ppath = split /\./, $cname;

    my $class = $ppath[-1];

#    print STDERR "Exporting $class\n";

    my ($image_large, $image_small) =  $image 
	? ("IconManager.loadImage($class.class, \"${image}_large.png\")", 
	   "IconManager.loadImage($class.class, \"${image}_small.png\")")
	: ("null", "null");

    print 
	"package ", join(".", @ppath[0..@ppath-2]), ";\n",
	"\n",
	"import java.beans.*;\n",
	"import java.awt.*;\n",
	"import icons.*;\n",
	"\n",
	"public class ${class}BeanInfo extends java.beans.SimpleBeanInfo {\n",
	"\tprivate final Class beanClass = ${class}.class;\n",
	"\tprivate final Image iconColor16x16 = ${image_small};\n",
	"\tprivate final Image iconColor32x32 = ${image_large};\n",
	"\tprivate final Image iconMono16x16 = null;\n",
	"\tprivate final Image iconMono32x32 = null;\n",
	"\n",
	"\tpublic PropertyDescriptor[] getPropertyDescriptors() {\n";

    if (defined $props && @$props) {
	print 
	    "\t\ttry{\n";

	my @decls;
	my @uses;

	for (@$props) {
	    my ($name, $getm, $setm) = @$_;
	    my $capname = uc(substr($name,0,1)) . substr($name,1);
	    $getm = "get$capname" unless defined $getm; 
	    $setm = "set$capname" unless defined $setm;

	    push @decls, "\t\t\tPropertyDescriptor _$name = new PropertyDescriptor(\"$name\", beanClass, \"$getm\", \"$setm\");\n";
	    push @uses, "_$name";
	}

	print 
	    join("\n", @decls), "\n",
	    "\t\t\tPropertyDescriptor[] pds = new PropertyDescriptor[] {\n",
	    "\t\t\t\t", join(",\n\t\t\t\t", @uses), "};\n",
	    "\t\t\treturn pds;\n";


	print 
	    "\t\t} catch(IntrospectionException ex) {\n",
            "\t\t\tex.printStackTrace();\n",
            "\t\t\treturn null;\n",
	    "\t\t}\n";
    } else {
	print "\t\treturn new PropertyDescriptor [] {};\n";
    }

    print 
	"\t}\n";


    print "\n\t", 
    join("\n\t", 
	 "public java.awt.Image getIcon(int iconKind) {",
	 "\tswitch (iconKind) {",
	 "\tcase BeanInfo.ICON_COLOR_16x16:",
	 "\t\treturn iconColor16x16;",
	 "\tcase BeanInfo.ICON_COLOR_32x32:",
	 "\t\treturn iconColor32x32;",
	 "\tcase BeanInfo.ICON_MONO_16x16:",
	 "\t\treturn iconMono16x16;",
	 "\tcase BeanInfo.ICON_MONO_32x32:",
	 "\t\treturn iconMono32x32;",
	 "\t}",
	 "\treturn null;",
	 "}"
	 ), "\n";


    if ($super) {
	print "\n\t",
	join("\n\t",
	     "public BeanInfo[] getAdditionalBeanInfo() {",
	     "\tClass superclass = beanClass.getSuperclass();",
	     "\ttry {",
	     "\t\tBeanInfo superBeanInfo = Introspector.getBeanInfo(superclass);",
	     "\t\treturn new BeanInfo[] { superBeanInfo };",
	     "\t}",
	     "\tcatch(IntrospectionException ex) {",
	     "\t\tex.printStackTrace();",
	     "\t\treturn null;",
	     "\t}",
	     "}"), "\n";
    }

    print
	"}\n";
}
