#!/bin/bash
# execute in the root directory

IFS='
'

# these are the components with .xml.sim-pl includes
for file in `git grep -l  '\.xml\.sim-pl' Components/ test/`; do
    sed -i 's/\.xml\.sim-pl/.sim-pl/' "$file"
done

for file in `git ls-files  '*.xml.sim-pl'`; do
    DIR=`dirname "$file"`
    STEM=`basename "$file" .xml.sim-pl`
    git mv "$DIR/$STEM.xml.sim-pl" "$DIR/$STEM.sim-pl"
done


# these are the components with .xml file names
for file in `git ls-files  'Components/*.xml' `; do
    DIR=`dirname "$file"`
    FILE=`basename "$file"`
    STEM=`basename "$file" .xml`
    for user in `git grep -l --word-regexp --fixed-strings "$FILE"`; do
	sed -i -r 's/\b'$STEM'\.xml\b/'$STEM'.sim-pl/' "$user"
    done
    git mv "$DIR/$STEM.xml" "$DIR/$STEM.sim-pl"
done
