#!/usr/bin/perl -w

my $noticefile = shift or die "Please specify the notice file\n";
@ARGV or die "Specify at least one file to process after the notice file\n";



# read in notice file, and compute its width

my @notice;
open FH, "<", $noticefile or die "Could not open notice file $noticefile\n";

my $noticespan = 0;
while(<FH>) {
    chomp;
    push @notice, $_;
    my $l = length $_;
    $noticespan = $l if $l > $noticespan;
}
close FH or die "Shit\n";



# define some nice commenting styles
my @STYLES = 
    (
     ["/*", "*" , "*/" , "*/", "*/", "*" , "/*" , "/*"],
     ["/*", "-*", "*\\", " *", "*/", "*-", "\\*", "* "],
     ["#" , "#" , "#"  , "#" , "#" , "#" , "#"  , "#" ]
     );


# notice cache (helps when called upon many files)
my %style2noticetxt;


for (@ARGV) {
    process($_); 
}



# splits text into shebang line, notice and rest, 
# with notice in any style give above

sub splitc {
    my $content = shift;

    $content =~ m!^(?:(\#\!.*?)\n)?\s*(.*)$!s;
    
    my ($shebang,$text) = ($1, $2);

    for my $style (@STYLES) {

	my ($ul, $u, $ur, $r, $dr, $d, $dl, $l) = 
	    map { my $t = $_; $t =~ s/(\*|\\|\?|\.)/\\$1/; $t } @$style;

	return ($shebang, $1, $2) if ($text =~ m!^($ul(?:$u)*$ur\s*.*?$dl(?:$d)*$dr)\s*(.*)$!s);
	
    }

    return ($shebang, undef, $text);
}



# updates a file if this is necessary 
# i.e. notice or style changed

sub process {
    my ($file) = @_;

    my $style;

    if ($file =~ /\.(java|g|h)$/) {
	$style = 1;
    } elsif ($file =~ /Makefile$|\.(asm|txt|deps|pl|sh)$/) {
	$style = 2;
    } else {
	die "unknown file type: $file\n";
    }

    my ($ul, $u, $ur, $r, $dr, $d, $dl, $l) = @{$STYLES[$style]};

    # retrieve notice text from cache, if it has already been computed
    my $noticetxt = $style2noticetxt{$style};
    unless ($noticetxt) {
	$noticetxt = 
	    join "\n", (
			$ul . ($u x (($noticespan+4)/length($u))) . $ur,
			(map { $l . "  " . $_ . (" " x ($noticespan - length($_))) . "  " . $r} @notice),
			$dl . ($d x (($noticespan+4)/length($d))) . $dr
			);
	$style2noticetxt{$style} = $noticetxt;
    } 
	
    open FH, "<", $file;
    my $data; {local $/=undef; $data=<FH>;}
    close FH;

    my ($shebang, $comment, $rest) = splitc($data);

    if (defined $comment && $comment eq $noticetxt) {
#	print STDERR "$file up to date\n";
    } else {
	print STDERR "$file out of date\n";

	open FH, ">", $file or die "Could not write to $file\n";
	print FH 
	    $shebang ? "$shebang\n\n" : "",
	    $noticetxt ? "$noticetxt\n\n\n" : "",
	    "$rest";
	close FH or die "Merde\n";
    }
}
