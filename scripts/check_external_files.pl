#!/usr/bin/perl -w

use strict;
use File::Copy qw(copy);

my %fn2sr = (
    "./aux/LICENSE" => "/usr/share/common-licenses/GPL-2",
    "./lib/antlr3.jar" => "/usr/share/java/antlr3.jar",
    "./lib/antlr3-runtime.jar" => "/usr/share/java/antlr3-runtime.jar",
    "./lib/antlr.jar" => "/usr/share/java/antlr.jar",
    "./lib/batik.jar" => "/usr/share/java/batik.jar",
    "./lib/castor-commons.jar" => "/home/wouter/lib/castor-1.0.3/castor-1.0.3-commons.jar",
    "./lib/castor-xml.jar" => "/home/wouter/lib/castor-1.0.3/castor-1.0.3-xml.jar",
    "./lib/commons-codec.jar" => "/usr/share/java/commons-codec.jar",
    "./lib/commons-logging.jar" => "/usr/share/java/commons-logging.jar",
    "./lib/epsgraphics.jar" => "/usr/share/java/net.sourceforge.jlibeps.jar",
    "./lib/idw-gpl.jar" => "/usr/share/java/idw-1.6.1.jar",
    "./lib/itext.jar" => "/usr/share/java/itext.jar",
    "./lib/stringtemplate.jar" => "/usr/share/java/stringtemplate4.jar",
    "./lib/xercesImpl.jar" => "/usr/share/java/xercesImpl.jar",
    "./lib/xmlParserAPIs.jar" => "/usr/share/java/xmlParserAPIs.jar",
#    "./src/sim-pl/databinding/SubPinNode.svg" => "wires/SubPinNode.svg",
    "./src/sim-pl/executer/programeditor/16x16_failure.png" => "/usr/share/icons/oxygen/base/16x16/actions/dialog-cancel.png",
    "./src/sim-pl/executer/programeditor/16x16_success.png" => "/usr/share/icons/oxygen/base/16x16/actions/dialog-ok.png",
    "./src/sim-pl/executer/programeditor/22x22_failure.png" => "/usr/share/icons/oxygen/base/22x22/actions/dialog-cancel.png",
    "./src/sim-pl/executer/programeditor/22x22_success.png" => "/usr/share/icons/oxygen/base/22x22/actions/dialog-ok.png",
    "./src/sim-pl/icons/16x16_about.png" => "/usr/share/icons/oxygen/base/16x16/actions/help-about.png",
    "./src/sim-pl/icons/16x16_copy.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-copy.png",
    "./src/sim-pl/icons/16x16_cut.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-cut.png",
    "./src/sim-pl/icons/16x16_delete.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-delete.png",
    "./src/sim-pl/icons/16x16_exit.png" => "/usr/share/icons/oxygen/base/16x16/actions/application-exit.png",
    "./src/sim-pl/icons/16x16_fileclose.png" => "/usr/share/icons/oxygen/base/16x16/actions/dialog-close.png",
    "./src/sim-pl/icons/16x16_fileexport.png" => "/usr/share/icons/oxygen/base/16x16/actions/document-export.png",
    "./src/sim-pl/icons/16x16_filenew.png" => "/usr/share/icons/oxygen/base/16x16/actions/document-new.png",
    "./src/sim-pl/icons/16x16_fileopen.png" => "/usr/share/icons/oxygen/base/16x16/actions/document-open.png",
    "./src/sim-pl/icons/16x16_filesaveas.png" => "/usr/share/icons/oxygen/base/16x16/actions/document-save-as.png",
    "./src/sim-pl/icons/16x16_filesave.png" => "/usr/share/icons/oxygen/base/16x16/actions/document-save.png",
    "./src/sim-pl/icons/16x16_paste.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-paste.png",
    "./src/sim-pl/icons/16x16_redo.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-redo.png",
    "./src/sim-pl/icons/16x16_undo.png" => "/usr/share/icons/oxygen/base/16x16/actions/edit-undo.png",
    "./src/sim-pl/icons/16x16_zoom_100.png" => "/usr/share/icons/oxygen/base/16x16/actions/zoom-original.png",
    "./src/sim-pl/icons/16x16_zoom_in.png" => "/usr/share/icons/oxygen/base/16x16/actions/zoom-in.png",
    "./src/sim-pl/icons/16x16_zoom_out.png" => "/usr/share/icons/oxygen/base/16x16/actions/zoom-out.png",
    "./src/sim-pl/icons/22x22_about.png" => "/usr/share/icons/oxygen/base/22x22/actions/help-about.png",
    "./src/sim-pl/icons/22x22_copy.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-copy.png",
    "./src/sim-pl/icons/22x22_cut.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-cut.png",
    "./src/sim-pl/icons/22x22_delete.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-delete.png",
    "./src/sim-pl/icons/22x22_exit.png" => "/usr/share/icons/oxygen/base/22x22/actions/application-exit.png",
    "./src/sim-pl/icons/22x22_fileclose.png" => "/usr/share/icons/oxygen/base/22x22/actions/dialog-close.png",
    "./src/sim-pl/icons/22x22_fileexport.png" => "/usr/share/icons/oxygen/base/22x22/actions/document-export.png",
    "./src/sim-pl/icons/22x22_filenew.png" => "/usr/share/icons/oxygen/base/22x22/actions/document-new.png",
    "./src/sim-pl/icons/22x22_fileopen.png" => "/usr/share/icons/oxygen/base/22x22/actions/document-open.png",
    "./src/sim-pl/icons/22x22_filesaveas.png" => "/usr/share/icons/oxygen/base/22x22/actions/document-save-as.png",
    "./src/sim-pl/icons/22x22_filesave.png" => "/usr/share/icons/oxygen/base/22x22/actions/document-save.png",
    "./src/sim-pl/icons/22x22_paste.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-paste.png",
    "./src/sim-pl/icons/22x22_redo.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-redo.png",
    "./src/sim-pl/icons/22x22_undo.png" => "/usr/share/icons/oxygen/base/22x22/actions/edit-undo.png",
    "./src/sim-pl/icons/22x22_zoom_100.png" => "/usr/share/icons/oxygen/base/22x22/actions/zoom-original.png",
    "./src/sim-pl/icons/22x22_zoom_in.png" => "/usr/share/icons/oxygen/base/22x22/actions/zoom-in.png",
    "./src/sim-pl/icons/22x22_zoom_out.png" => "/usr/share/icons/oxygen/base/22x22/actions/zoom-out.png",
#    "./src/sim-pl/util/version.properties" => "../../../version.properties",
    );

die "usage $0 [fix]" unless @ARGV <= 1;

my $fix = 0;

if (@ARGV==1) {
    if ($ARGV[0] eq "fix") {
	$fix = 1;
    } else {
	die "usage $0 [fix]";
    }
}




while (my ($fn, $sr) = each %fn2sr) {
    my @srs = stat($sr) or die "failed to stat $sr\n";
    my @fns = stat($fn) or die "failed to stat $fn\n";
    if ($srs[9] > $fns[9]) {
	print "source ", $sr, " is newer than ", $fn, "\n";
	if ($fix) {
	    copy($sr, $fn) or die "failed to copy $sr to $fn";
	}
    }
}
