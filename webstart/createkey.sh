#!/bin/sh
keytool -genkeypair \
    -dname "CN=Wouter Koolen, OU=SIM-PL Team, O=SIM-PL Developers, L=Amsterdam, ST=Unknown, C=NL" \
    -alias wouter  \
    -keypass SIM-PL \
    -keystore SIM-PL_KeyStore \
    -storepass SIM-PL \
    -validity 365
